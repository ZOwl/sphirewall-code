/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include "Api/RequestService.h"
#include "Api/RequestHandler.h"

using boost::asio::ip::tcp;
using namespace std;

void RequestService::run() {
	boost::asio::io_service io_service;
	tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));
	for (;;) {
		socket_ptr sock(new tcp::socket(io_service));
		a.accept(*sock);

		boost::thread(boost::bind(&RequestService::handleConnection, this, sock));
	}
}

void RequestService::handleConnection(socket_ptr sock) {
	try {
		boost::asio::streambuf response;
		boost::asio::read_until(*sock, response, "\n");
		string buffer = string((istreambuf_iterator<char>(&response)),
				istreambuf_iterator<char>());

		string res = handler->handle(buffer);

		boost::asio::streambuf request;
		std::ostream request_stream(&request);

		request_stream << res << "\n";
		boost::asio::write(*sock, request);

		(*sock).close();
	} catch (boost::exception_detail::clone_impl<boost::exception_detail::error_info_injector<boost::system::system_error> > e) {
		cout << "some error was caught:" << e.what() << endl;
	}
}

