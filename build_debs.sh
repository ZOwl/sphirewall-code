#!/bin/bash
#
# Loose script to build the debian packages
#
#
VERSION=0.9.9.7
TEMP_DIR=debian_build
RESULT_DIR=debs
ARCH=i386

rm -Rf $TEMP_DIR
rm -Rf $RESULT_DIR

mkdir $TEMP_DIR
mkdir $RESULT_DIR

build_kernel_module() {
	cp sphirewall-kernel/ $TEMP_DIR/sphirewall-kernel-$VERSION -Rf
	cd $TEMP_DIR
	tar zcf sphirewall-kernel-$VERSION.orig.tar.gz sphirewall-kernel-$VERSION/
	cd sphirewall-kernel-$VERSION
	pwd
	echo "" | dh_make -f ../sphirewall-kernel-$VERSION.orig.tar.gz -k -c gpl
	cp .debian/* debian/

	echo "building kernel packages"
	dpkg-buildpackage
	cd ../../
}

build_wmi(){
	cd sphirewall-wmi/www/
	python setup.py sdist
	cp dist/sphirewallwmi-$VERSION.tar.gz ../../$TEMP_DIR 
	cp .debian ../../$TEMP_DIR -Rf
	cd ../../$TEMP_DIR
	tar zxvf sphirewallwmi-$VERSION.tar.gz
	cd sphirewallwmi-$VERSION
	python setup.py --command-packages=stdeb.command debianize
	rm -rf debian/patches
	echo 'sphirewallwmi.egg-info/*' > debian/clean
	cp ../sphirewallwmi-$VERSION.tar.gz ../sphirewallwmi_$VERSION.orig.tar.gz
	cp ../.debian/* debian -Rf
	dpkg-buildpackage
	cd ../../
}

build_core(){
	cp sphirewall-core $TEMP_DIR/sphirewall-core-$VERSION -Rf
	cd $TEMP_DIR/sphirewall-core-$VERSION

	sh autogen.sh
	cd ../
	tar zcf sphirewall-core-$VERSION.tar.gz sphirewall-core-$VERSION/
	cd sphirewall-core-$VERSION
	echo "" | dh_make -f ../sphirewall-core-$VERSION.tar.gz -l -c gpl
	cp .debian/* debian/

	echo "building sphirewall-core"
	dpkg-buildpackage
	cd ../../
}

build_ana(){
	cp sphirewall-ana $TEMP_DIR/sphirewall-ana-$VERSION -Rf
	cd $TEMP_DIR/sphirewall-ana-$VERSION
	sh autogen.sh
	cd ../
        tar zcf sphirewall-ana-$VERSION.tar.gz sphirewall-ana-$VERSION/
        cd sphirewall-ana-$VERSION

	echo "" | dh_make -f ../sphirewall-ana-$VERSION.tar.gz -m --copyright gpl3
	cp .debian/* debian
	dpkg-buildpackage

	cd ../../
}

build_libs(){
	cp sphirewall-libs/ $TEMP_DIR/libsphirewall-$VERSION -Rf
	cd $TEMP_DIR/libsphirewall-$VERSION
	sh autogen.sh
	cd ../
	tar zcf libsphirewall-$VERSION.tar.gz libsphirewall-$VERSION/
	cd libsphirewall-$VERSION
    
	echo "setting up"
	echo "" | dh_make -f ../libsphirewall-$VERSION.tar.gz -l -c gpl
	cp .debian/* debian/
    
	echo "building libsphirewall"
	dpkg-buildpackage -d

	cd ../	
	dpkg -i libsphirewall1_$VERSION-1_$ARCH.deb
	dpkg -i libsphirewall-dev_$VERSION-1_$ARCH.deb
	cd ../
}

deploy(){
	scp *.deb root@192.168.57.88:/var/lib/debarchiver/incoming/stable
}

build_libs
build_wmi
build_kernel_module
build_core
build_ana

#deploy
