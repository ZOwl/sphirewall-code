#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

class QuotaInfo:
    dailyQuota = False
    dailyQuotaLimit = 0
    weeklyQuota = False
    weeklyQuotaLimit = 0
    monthQuota = False
    monthQuotaLimit = 0
    monthQuotaBillingDay = 0
    monthQuotaIsSmart = False
    totalQuota = False
    totalQuotaLimit = 0
    timeQuota = False
    timeQuotaLimit = 0


class GeneralSettings:
    connection = None

    def __init__(self, connection):
        self.connection = connection

    def events_list(self):
        return self.connection.request("events/list", None)["events"]

    def events_size(self):
        return self.connection.request("events/size", None)["size"]

    def hosts_list(self):
        return self.connection.request("network/arp/list", None)["hosts"]

    def hosts_size(self):
        return self.connection.request("network/arp/size", None)["size"]

    def active_users_list(self):
        return self.connection.request("auth/sessions/list", None)["sessions"]

    def configuration(self, key, value=None):
        if value is not None:
            arr = {}
            arr["key"] = key
            arr["value"] = value
            self.connection.request("general/config/set", arr)
        else:
            arr = {}
            arr["key"] = key
            arr["hideExceptions"] = True
            retval = self.connection.request("general/config/get", arr)

            if retval is not None:
                return retval["value"]
            return False

    def event_handler(self, eventType=None, handler=None):
        if eventType is not None and handler is not None:
            handlers = self.connection.request("events/config/list", None)["handlers"]
            for h in handlers:
                if h["key"] == eventType and h["handler"] == handler:
                    return h
        else:
            return self.connection.request("events/config/list", None)["handlers"]

    def event_handler_add(self, eventType, handlerType, data=None):
        args = {}

        if data is not None:
            for dkey in data.iterkeys():
                k = dkey
                v = data[k]
                args[k] = v

        args["key"] = eventType
        args["handler"] = handlerType

        return self.connection.request("events/config/add", args)

    def event_handler_delete(self, eventType, handlerType):
        args = {}
        args["key"] = eventType
        args["handler"] = handlerType

        return self.connection.request("events/config/del", args)

    def event_handler_exists(self, eventType, handlerType):
        handlers = self.event_handler()
        for handler in handlers:
            if handler["key"] == eventType and handler["handler"] == handlerType:
                return True

        return False

    def groups(self, name=None):
        if name is not None:
            allGroups = self.groups()
            for group in allGroups:
                if group["id"] == int(name):
                    return group

        return self.connection.request("auth/groups/list", None)["groups"]

    def users(self, username=None):
        if username is not None:
            allUsers = self.users()
            for user in allUsers:
                if user["username"] == username:
                    return user

        return self.connection.request("auth/users/list", None)["users"]

    def users_add(self, username, firstName, lastName):
        args = {}
        args["username"] = username
        self.connection.request("auth/users/add", args)

    def users_delete(self, username):
        args = {}
        args["username"] = username
        self.connection.request("auth/users/del", args)

    def users_save(self, username, fname, lname, email, quota):
        args = {}
        args["username"] = username
        args["fname"] = fname
        args["lname"] = lname
        args["email"] = email
        args["usePam"] = False
        args["quota"] = 0

        args["dailyQuota"] = quota.dailyQuota
        args["dailyQuotaLimit"] = quota.dailyQuotaLimit
        args["weeklyQuota"] = quota.weeklyQuota
        args["weeklyQuotaLimit"] = quota.weeklyQuotaLimit
        args["monthQuota"] = quota.monthQuota
        args["monthQuotaLimit"] = quota.monthQuotaLimit
        args["monthQuotaBillingDay"] = quota.monthQuotaBillingDay
        args["monthQuotaIsSmart"] = quota.monthQuotaIsSmart
        args["totalQuota"] = quota.totalQuota
        args["totalQuotaLimit"] = quota.totalQuotaLimit
        args["timeQuota"] = quota.timeQuota
        args["timeQuotaLimit"] = quota.timeQuotaLimit

        self.connection.request("auth/users/save", args)

    def user_set_password(self, username, password):
        args = {}
        args["username"] = username
        args["password"] = password
        self.connection.request("auth/users/setpassword", args)

    def group_add(self, name):
        args = {}
        args["name"] = name
        self.connection.request("auth/groups/create", args)

    def groups_delete(self, name):
        args = {}
        args["id"] = int(name)
        self.connection.request("auth/groups/del", args)

    def groups_save(self, groupId, manager, isAdmin, description, quota):
        args = {}
        args["id"] = int(groupId)
        args["desc"] = description
        args["manager"] = manager
        args["mui"] = isAdmin

        args["dailyQuota"] = quota.dailyQuota
        args["dailyQuotaLimit"] = quota.dailyQuotaLimit
        args["weeklyQuota"] = quota.weeklyQuota
        args["weeklyQuotaLimit"] = quota.weeklyQuotaLimit
        args["monthQuota"] = quota.monthQuota
        args["monthQuotaLimit"] = quota.monthQuotaLimit
        args["monthQuotaBillingDay"] = quota.monthQuotaBillingDay
        args["monthQuotaIsSmart"] = quota.monthQuotaIsSmart
        args["totalQuota"] = quota.totalQuota
        args["totalQuotaLimit"] = quota.totalQuotaLimit
        args["timeQuota"] = quota.timeQuota
        args["timeQuotaLimit"] = quota.timeQuotaLimit

        self.connection.request("auth/groups/save", args)

    def disconnect_session(self, address):
        args = {}
        args["ipaddress"] = address
        self.connection.request("auth/logout", args)

    def events_purge(self):
        self.connection.request("events/purge", None)

    def smtp_test(self):
        self.connection.request("general/smtp/testconnection", None)

    def smtp_publish(self):
        self.connection.request("general/smtp/publish", None)

    def users_groups_merge(self, username, groups):
        args = {"username":username, "groups":groups}
        self.connection.request("auth/groups/mergeoveruser", args)

    def user_groups_add(self, username, groupid):
        args = {}
        args["username"] = username
        args["group"] = int(groupid)
        self.connection.request("auth/users/groups/add", args)

    def user_groups_remove(self, username, groupid):
        args = {}
        args["username"] = username
        args["group"] = int(groupid)
        self.connection.request("auth/users/groups/del", args)

    def user_quotas(self, username):
        args = {}
        args["username"] = username
        return self.connection.request("auth/users/quotas/list", args)["items"]

    def user_quotas_add(self, username, period, limit):
        args = {}
        args["username"] = username
        args["period"] = period
        args["quotaLimit"] = int(limit)
        self.connection.request("auth/users/quotas/set", args)

    def user_quotas_del(self, username, period):
        args = {}
        args["username"] = username
        args["period"] = period
        self.connection.request("auth/users/quotas/del", args)


    def user_enable(self, username):
        args = {}
        args["username"] = username
        self.connection.request("auth/users/enable", args)

    def user_disable(self, username):
        args = {}
        args["username"] = username
        self.connection.request("auth/users/disable", args)

    def advanced(self, key=None, value=None):
        if key is None and value is None:
            args = {}
            return self.connection.request("general/runtime/list", args)["keys"]
        else:
            args = {}
            args["key"] = key
            args["value"] = value
            self.connection.request("general/runtime/set", args)

    def advanced_value(self, key):
        all_items = self.advanced()
        for item in all_items:
            if item["key"] == key:
                return item["value"]

        return None

    def quotas(self):
        args = {}
        return self.connection.request("general/quotas", args)

    def quotas_set(self, dailyQuota, dailyQuotaLimit, weeklyQuota, weeklyQuotaLimit, monthQuota, monthQuotaBillingDay,
                   monthQuotaIsSmart, monthQuotaLimit):
        args = {}
        args["dailyQuota"] = dailyQuota
        args["dailyQuotaLimit"] = dailyQuotaLimit
        args["weeklyQuota"] = weeklyQuota
        args["weeklyQuotaLimit"] = weeklyQuotaLimit

        args["monthQuota"] = monthQuota
        args["monthQuotaBillingDay"] = monthQuotaBillingDay
        args["monthQuotaIsSmart"] = monthQuotaIsSmart
        args["monthQuotaLimit"] = monthQuotaLimit

        return self.connection.request("general/quotas/set", args)

    def logs(self, filter=None):
        args = {}
        if filter is not None:
            args["filter"] = filter
        return self.connection.request("general/logs", args)["log"]

    def defaultpasswordset(self):
        return self.connection.request("auth/users/defaultpasswordset", None)["value"]

    def ldap(self):
        return self.connection.request("auth/ldap", None)

    def ldap_sync_groups(self):
        return self.connection.request("auth/ldap/sync", None)