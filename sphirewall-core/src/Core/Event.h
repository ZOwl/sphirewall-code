/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_EVENT_H_INCLUDED
#define SPHIREWALL_EVENT_H_INCLUDED

#include <list>
#include <vector>
#include <map>
#include <sstream>
#include "Utils/Lock.h"
#include "Core/Cron.h"

class ConfigurationManager;

namespace SLogger{
    class Logger;
};

using namespace std;

extern const char* EVENT_PURGE;
extern const char* IDS_EVENT;
extern const char* IDS_PORTSCAN;
extern const char* IDS_SSHD_BRUTEFORCE;
extern const char* SYSTEM_MONITOR ;
extern const char* IDS_FLOOD_ATTACK;
extern const char* IDS_SNORT_ALERT;
extern const char* IDS_SNORT_ALERT_1;
extern const char* IDS_SNORT_ALERT_2;
extern const char* IDS_SNORT_ALERT_3;
extern const char* IDS_SNORT_ALERT_4;
extern const char* IDS_SNORT_ALERT_5;
extern const char* USERDB_LOGIN_FAILED;
extern const char* USERDB_LOGIN_SUCCESS;
extern const char* USERDB_QUOTA_EXCEEDED;
extern const char* USERDB_QUOTA_OK;
extern const char* USERDB_SESSION_CREATE;
extern const char* USERDB_SESSION_TIMEOUT;
extern const char* USERDB_LOGOUT;
extern const char* GLOBAL_QUOTA_EXCEEDED;
extern const char* GLOBAL_QUOTA_OK;

extern const char* AUDIT_EVENT;
extern const char* AUDIT_RPC_CALL;
extern const char* AUDIT_CONFIGURATION_CHANGED;
extern const char* AUDIT_CONFIGURATION_IDS_EXCEPTION_ADDED;
extern const char* AUDIT_CONFIGURATION_IDS_EXCEPTION_REMOVED;
extern const char* AUDIT_CONFIGURATION_NETWORK_DEVICES_TOGGLE;
extern const char* AUDIT_CONFIGURATION_NETWORK_DEVICES_SETIP;
extern const char* AUDIT_CONFIGURATION_NETWORK_DNS_SET;
extern const char* AUDIT_CONFIGURATION_NETWORK_ROUTES_ADD;
extern const char* AUDIT_CONFIGURATION_NETWORK_ROUTES_DEL;
extern const char* AUDIT_CONFIGURATION_NETWORK_DHCP_SET;
extern const char* AUDIT_CONFIGURATION_NETWORK_DHCP_PUBLISH;
extern const char* AUDIT_CONFIGURATION_NETWORK_DHCP_START;
extern const char* AUDIT_CONFIGURATION_NETWORK_DHCP_STOP;
extern const char* AUDIT_CONFIGURATION_NETWORK_CONNECTION_START;
extern const char* AUDIT_CONFIGURATION_NETWORK_CONNECTION_STOP;
extern const char* AUDIT_CONFIGURATION_GROUPDB_ADD;
extern const char* AUDIT_CONFIGURATION_GROUPDB_DEL;
extern const char* AUDIT_CONFIGURATION_USERDB_ADD;
extern const char* AUDIT_CONFIGURATION_USERDB_DEL;
extern const char* AUDIT_CONFIGURATION_USERDB_MODIFIED;
extern const char* AUDIT_CONFIGURATION_USERDB_SETPASSWORD;
extern const char* AUDIT_CONFIGURATION_USERDB_GROUPS_ADD;
extern const char* AUDIT_CONFIGURATION_USERDB_GROUPS_DEL;
extern const char* AUDIT_CONFIGURATION_USERDB_ENABLE;
extern const char* AUDIT_CONFIGURATION_USERDB_DISABLE;
extern const char* AUDIT_FIREWALL_CONNECTIONS_TERMINATE;
extern const char* AUDIT_CONFIGURATION_GROUPDB_MODIFIED;

extern const char* RUNTIME_METRIC;
extern const char* START;

extern const char* NETWORK_DEVICES_ADDED;
extern const char* NETWORK_DEVICES_CHANGED;
extern const char* NETWORK_DEVICES_REMOVED;
extern const char* NETWORK_ARP_DISCOVERED;
extern const char* NETWORK_ARP_TIMEDOUT;

extern const char* FIREWALL_WEBFILTER_HIT;
extern const char* WATCHDOG_INTERNET_CONNECTION_UP;
extern const char* WATCHDOG_INTERNET_CONNECTION_DOWN;

class Param {
	public:
		Param(){}
		Param(std::string value){
			s = true;
			n = false;
		}

		Param(double value){
			s = false;
			n = true;
		}

		Param& operator=(std::string stringValue){
			s = true;
			n = false;
			this->stringValue = stringValue;
		}

		Param& operator=(double numberValue){
			s = false;
			n = true;
			this->numberValue= numberValue;
		}


		std::string string() const {
			return stringValue;
		}

		double number() const {
			return numberValue;
		}

		bool isNumber() const {
			return n;
		}		

		bool isString() const {
			return s;
		}
	private:
		std::string stringValue;
		double numberValue;

		bool s;
		bool n;
};

class EventParams {
	public:
		void addParam(std::string key, Param param){
			params[key] = param;	
		}

		Param& operator[](const std::string& input){
			return params[input];
		}			

		Param* get(const std::string input){
			if(params.find(input) == params.end()){
				return NULL;
			}	

			return &params[input];
		}

		map<string, Param>& getParams() {
			return params;
		}

		std::string toString() {
			stringstream ss;
			map<string, Param>::iterator iter;
			for(iter = params.begin(); iter != params.end(); iter++){
				Param param = iter->second;
				ss << "{'" << iter->first << "':";	
				if(param.isString()){
					ss << "'" << param.string() << "'";
				}else{
					ss << param.number();
				}	

				ss << "},";
			}	
			
			return ss.str();
		}
	private:
		map<string, Param> params;	
};

class Event{
	public:
		Event(std::string key, EventParams params){
			this->t = time(NULL);
			this->key = key;
			this->params = params;	
		}

		int t;
		string key;
		EventParams params;

		std::string toString(){
			stringstream ss;
			ss << "Event " << key << " with details " << params.toString();
			return ss.str();
		}
};

class EventHandler{
	public:
		virtual void handle(Event* event) {};
		virtual void unhandle(Event* event){};

		virtual string name(){return "";};
		virtual int getId() {return -1;};
		virtual std::string key() const {return "";};

		EventHandler(){}
		EventHandler(const EventHandler& h) {
		}

		virtual EventHandler* Clone(){
			return new EventHandler(*this);
		}
};

class EventDb : public CronJob {
	public:
		EventDb(SLogger::Logger* logger);
		EventDb(SLogger::Logger* logger, Config* config);

		//Method as part of the CronJob class
                void run(){
			purgeOldEvents();
		}

		int sendEmail(Event event);
		void add(Event*);
		void runhandlers(Event* e);
		int size();

		void load();
		void save();
		void list(vector<Event*>& _return);
		void purgeEvents();
		void purgeOldEvents();

		void addHandler(std::string key, EventHandler* handler);
		void removeHandler(std::string key, EventHandler* handler);
		void clearHandlers();
		multimap<string, EventHandler*>& getHandlers(){
			return handlers;
		}
		
		void setAsync(bool value){
			this->async = value;
		}
                void holdLock();
                void releaseLock();
        private:
                Lock* lock;
		vector<Event*> events; 					
		multimap<string, EventHandler*> handlers;
		map<std::string, std::string> descriptors;

		SLogger::Logger* logger;
		Config* config;
		bool async;

		static int EVENT_DB_PURGE_THRESHOLD;
};

#endif
