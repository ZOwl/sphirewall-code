#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester

class AuthenticationTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login("fw", "admin", "admin")

    def gotoAliases(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Firewall')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Pools')]").click()

    def test_create_IPRangeListAlias(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_name("name").send_keys("test-alias1")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='IP Range List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'test-alias1')]//ancestor::tr/td[contains(text(), 'Ip range alias')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'test-alias1')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'test-alias1')]"))

    def test_create_IPSubnetListAlias(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_name("name").send_keys("test-subnetalias")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='IP Subnet List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'test-subnetalias')]//ancestor::tr/td[contains(text(), 'Ip subnet alias')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'test-subnetalias')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'test-subnetalias')]"))

    def test_create_WebsiteListAlias(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_name("name").send_keys("test-websitelist")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='Website List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'test-websitelist')]//ancestor::tr/td[contains(text(), 'Custom website list')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'test-websitelist')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'test-websitelist')]"))


    def test_create_IPRangeListAlias_ManuallyManage(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_name("name").send_keys("test-iprangelist")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='IP Range List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'test-iprangelist')]").click()
        self.tester.driver.find_element_by_name("value").send_keys("10.1.1.1-10.1.1.10")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_name("value").send_keys("10.0.0.1-10.0.0.10")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.1.1.1-10.1.1.10')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), '10.1.1.1-10.1.1.10')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.1.1.1-10.1.1.10')]"))

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.0.0.1-10.0.0.10')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), '10.0.0.1-10.0.0.10')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.0.0.1-10.0.0.10')]"))

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Aliases')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'test-iprangelist')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()

    def test_create_IPSubnetListAlias_ManuallyManage(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_name("name").send_keys("test-ipsubnetlist")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='IP Subnet List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'test-ipsubnetlist')]").click()
        self.tester.driver.find_element_by_name("value").send_keys("10.1.1.0/255.255.255.240")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_name("value").send_keys("10.0.0.0/255.0.0.0")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.1.1.0/255.255.255.240')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), '10.1.1.0/255.255.255.240')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.1.1.0/255.255.255.240')]"))

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.0.0.0/255.0.0.0')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), '10.0.0.0/255.0.0.0')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.0.0.0/255.0.0.0')]"))

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Aliases')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'test-ipsubnetlist')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()

    def test_create_WebsiteList_ManuallyManage(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_name("name").send_keys("test-websitelist")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='Website List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'test-websitelist')]").click()
        self.tester.driver.find_element_by_name("value").send_keys("yahoo.com")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_name("value").send_keys("google.com")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), 'yahoo.com')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), 'yahoo.com')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), 'yahoo.com')]"))

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), 'google.com')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), 'google.com')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), 'google.com')]"))

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Aliases')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'test-websitelist')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()

    def test_create_WebsiteList_ManuallyManage_InsertMultiple(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_name("name").send_keys("test-websitelist")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='Website List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'test-websitelist')]").click()
        self.tester.driver.find_element_by_name("value").send_keys("google.com boobs.com")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), 'boobs.com')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), 'boobs.com')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), 'boobs.com')]"))

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), 'google.com')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), 'google.com')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), 'google.com')]"))

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Aliases')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'test-websitelist')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()


    def test_createDnsList(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_name("name").send_keys("localhost")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='IP Subnet List']").click()
        self.tester.driver.find_element_by_xpath("//select[@name='source']/option[text()='Dns Hostname']").click()
        self.tester.driver.find_element_by_name("detail").send_keys("localhost")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'localhost')]").click()
        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '127.0.0.1')]//ancestor::tr/td[contains(text(), '255.255.255.255')]"))

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Aliases')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'localhost')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()

    def test_createRemoteList(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_name("name").send_keys("remotelist")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='IP Subnet List']").click()
        self.tester.driver.find_element_by_xpath("//select[@name='source']/option[text()='Remote list from url']").click()
        self.tester.driver.find_element_by_name("detail").send_keys("http://mirror.sphirewall.net/resources/list.txt")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'remotelist')]").click()
        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.1.1.1')]//ancestor::tr/td[contains(text(), '255.255.255.255')]"))
        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.1.1.2')]//ancestor::tr/td[contains(text(), '255.255.255.255')]"))
        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.1.1.3')]//ancestor::tr/td[contains(text(), '255.255.255.255')]"))

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Aliases')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'remotelist')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()

    def test_createGeoIpList(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_name("name").send_keys("nz")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='IP Range List']").click()
        self.tester.driver.find_element_by_xpath("//select[@name='source']/option[text()='Geoip MaxMind Country']").click()
        self.tester.driver.find_element_by_name("detail").send_keys("New Zealand")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'nz')]").click()
        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '96.8.113.160-96.8.113.167')]"))

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Aliases')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'nz')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()

    def tearDown(self):
        self.tester.disconnect()
