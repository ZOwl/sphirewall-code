/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;

#include "Core/System.h"
#include "Api/JsonManagementService.h"
#include "Json/JSON.h"
#include "SFwallCore/Alias.h"
#include "Utils/IP4Addr.h"
#include "SFwallCore/Firewall.h"
#include "Utils/NetDevice.h"
#include "SFwallCore/ApplicationLevelFilter.h"
#include "Core/Event.h"

using namespace SFwallCore;

JSONObject FirewallDelegate::process(std::string uri, JSONObject obj) {
	if (uri.compare("firewall/aliases/list") == 0) {
		return aliases_list(obj);
	} else if (uri.compare("firewall/aliases/add") == 0) {
		return aliases_add(obj);
	} else if (uri.compare("firewall/aliases/del") == 0) {
		return aliases_del(obj);
	} else if (uri.compare("firewall/aliases/del/bulk") == 0) {
		return aliases_del_bulk(obj);
	} else if (uri.compare("firewall/aliases/alias/list") == 0) {
		return aliases_alias_list(obj);
	} else if (uri.compare("firewall/aliases/alias/add") == 0) {
		return aliases_alias_add(obj);
	} else if (uri.compare("firewall/aliases/alias/del") == 0) {
		return aliases_alias_del(obj);
	} else if (uri.compare("firewall/aliases/get") == 0) {
		return aliases_get(obj);
	} else if (uri.compare("firewall/aliases/load") == 0) {
		return aliases_load(obj);
	} else if (uri.compare("firewall/tracker/list") == 0) {
		return tracker_list(obj);
	} else if (uri.compare("firewall/tracker/terminate") == 0) {
		return tracker_terminate(obj);
	} else if (uri.compare("firewall/acls/filter/up") == 0) {
		return acls_filter_up(obj);
	} else if (uri.compare("firewall/acls/filter/down") == 0) {
		return acls_filter_down(obj);
	} else if (uri.compare("firewall/acls/filter/delete") == 0) {
		return acls_filter_delete(obj);
	} else if (uri.compare("firewall/acls/nat/delete") == 0) {
		return acls_nat_delete(obj);
	} else if (uri.compare("firewall/acls/list") == 0) {
		return acls_list(obj);
	} else if (uri.compare("firewall/acls/add") == 0) {
		return acls_add(obj);
	} else if (uri.compare("firewall/acls/list/trafficshaper") == 0) {
		return acls_list_trafficshaper(obj);
	} else if (uri.compare("firewall/acls/add/trafficshaper") == 0) {
		return acls_add_trafficshaper(obj);
	} else if (uri.compare("firewall/acls/del/trafficshaper") == 0) {
		return acls_list_del_trafficshaper(obj);
	}else if(uri.compare("firewall/acls/enable") == 0){
		return acls_enable(obj);
	}else if(uri.compare("firewall/acls/disable") == 0){
		return acls_disable(obj);
	}else if(uri.compare("firewall/acls/status") == 0){
		return acls_status(obj);
	}else if(uri.compare("firewall/webfilter/rules/list") == 0){
		return acls_webfilter_rules_list(obj);
	}else if(uri.compare("firewall/webfilter/rules/deleteall") == 0){
		return acls_webfilter_rules_deleteall(obj);
	}else if(uri.compare("firewall/webfilter/rules/add") == 0){
		return acls_webfilter_rules_add(obj);
	}else if(uri.compare("firewall/webfilter/rules/remove") == 0){
		return acls_webfilter_rules_remove(obj);
	}else if(uri.compare("firewall/deny/list") == 0){
		return deny_list(obj);
	}else if(uri.compare("firewall/deny/del") == 0){
		return deny_del(obj);
	}else if(uri.compare("firewall/rewrite/remove") == 0){
		return rewrite_remove(obj);
	}else if(uri.compare("firewall/tracker/size") == 0){
		return tracker_size();
	}else if(uri.compare("firewall/webfilter/rules/moveup") == 0){
		return acls_webfilter_rules_moveup(obj);
	}else if(uri.compare("firewall/webfilter/rules/movedown") == 0){
		return acls_webfilter_rules_movedown(obj);
        }else if(uri.compare("firewall/webfilter/rules/enable") == 0){
                return acls_webfilter_rules_enable(obj);
        }else if(uri.compare("firewall/webfilter/rules/disable") == 0){
                return acls_webfilter_rules_disable(obj);
	}else if(uri.compare("firewall/rewrite/forceauthranges") == 0){
		return acls_rewrite_forceauthranges();
	}else if(uri.compare("firewall/rewrite/forceauthranges/set") == 0){
		return acls_rewrite_forceauthranges_set(obj);
	} else {
		throw new DelegateNotFoundException(uri);
	}
}

JSONObject FirewallDelegate::acls_rewrite_forceauthranges(){
	RewriteEngine* engine = firewall->rewriteEngine;
	JSONObject ret;
	JSONArray ranges;
	JSONArray websites;
	for(list<Alias*>::iterator iter = engine->getForceAuthRanges().begin();
		iter != engine->getForceAuthRanges().end();
		iter++){
		
		Alias* target = (*iter);
		JSONObject o;
		o.put(L"aliasId", new JSONValue((string) target->id));	
		o.put(L"aliasName", new JSONValue((string) target->name));	
		
		ranges.push_back(new JSONValue(o));
	}

	for(list<Alias*>::iterator iter = engine->getForceAuthWebsiteExceptions().begin();
		iter != engine->getForceAuthWebsiteExceptions().end();
		iter++){
		
		Alias* target = (*iter);
		JSONObject o;
		o.put(L"aliasId", new JSONValue((string) target->id));	
		o.put(L"aliasName", new JSONValue((string) target->name));	
		
		websites.push_back(new JSONValue(o));
	}


	ret.put(L"ranges", new JSONValue(ranges));
	ret.put(L"websites", new JSONValue(websites));
	return ret;
}

JSONObject FirewallDelegate::acls_rewrite_forceauthranges_set(JSONObject obj){
	RewriteEngine* engine = firewall->rewriteEngine;
	AliasDb* aliases = firewall->aliases;
	engine->getForceAuthRanges().clear();
	engine->getForceAuthWebsiteExceptions().clear();

	//Ranges
	JSONArray ranges = obj[L"ranges"]->AsArray();
	for(int x= 0; x < ranges.size(); x++){
		string range = ranges[x]->String();
		Alias* alias = aliases->get(range);
		if(alias){
			engine->getForceAuthRanges().push_back(alias);
		}			
	}	

	//Websites
        JSONArray websites = obj[L"websites"]->AsArray();
        for(int x= 0; x < websites.size(); x++){
                string range = websites[x]->String();
                Alias* alias = aliases->get(range);
                if(alias){
                        engine->getForceAuthWebsiteExceptions().push_back(alias);
                }
        }

	engine->save();	
	return JSONObject();
}

JSONObject FirewallDelegate::rewrite_remove(JSONObject obj){
	std::string id = obj[L"id"]->String();
	RewriteEngine* engine = firewall->rewriteEngine;
	RewriteRequired* rule = engine->get(id);	
	engine->clear(rule);

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_list(JSONObject) {
	JSONObject ret;
	JSONArray array;

	map<string, Alias*>::iterator iter;
	for (iter = firewall->aliases->aliases.begin(); iter != firewall->aliases->aliases.end(); iter++) {
		Alias* alias = iter->second;
		JSONObject o;
		o.put(L"id", new JSONValue((string) iter->first));
		o.put(L"name", new JSONValue((string) alias->name));
		o.put(L"type", new JSONValue((double) alias->type()));

		stringstream ss; ss << alias->description();
		if(alias->source){
			ss << " from " << alias->source->description();		
		}
		o.put(L"description", new JSONValue((string) ss.str()));
		array.push_back(new JSONValue(o));
	}

	ret.put(L"aliases", new JSONValue(array));
	return ret;
}

JSONObject FirewallDelegate::aliases_get(JSONObject object) {
	Alias* alias = firewall->aliases->get(object[L"id"]->String());
	if (alias) {
		JSONObject o;
		o.put(L"id", new JSONValue((string) alias->id));
		o.put(L"name", new JSONValue((string) alias->name));
		o.put(L"description", new JSONValue((string) alias->description()));

		return o;
	}
	throw new DelegateGeneralException("Could not find alias");
}

JSONObject FirewallDelegate::aliases_load(JSONObject object){
	Alias* alias = firewall->aliases->get(object[L"id"]->String());
	int err = -1;
	if (alias) {
		err = alias->load();
	}

	JSONObject ret;
	ret.put(L"loadstate", new JSONValue((double) err));
	return ret; 
}

JSONObject FirewallDelegate::aliases_add(JSONObject object) {
	if(firewall->aliases->getByName(object[L"name"]->String())){
		return JSONObject();	
	}

	SFwallCore::AliasType type = (SFwallCore::AliasType) object[L"type"]->AsNumber();
	int ret = 0;
	Alias* alias = NULL;
	switch(type){
		case IP_RANGE:{
				      alias = new IpRangeAlias();
				      alias->name = object[L"name"]->String();
				      break;
			      }

		case IP_SUBNET: {
					alias = new IpSubnetAlias();
					alias->name = object[L"name"]->String();
					break;
				}

		case WEBSITE_LIST : {
					    alias = new WebsiteListAlias();
					    alias->name = object[L"name"]->String();
					    break;
				    }
	}		

	if(object.has(L"source") && object[L"source"]->AsNumber() != -1){
		AliasListSourceType source = (AliasListSourceType) object[L"source"]->AsNumber();
		switch(source){
			case DNS:{
					 alias->source = new DnsListSource();
					 alias->source->detail = object[L"detail"]->String();
					 break;
				 }
			case HTTP_FILE:{
					       alias->source = new HttpFileSource();
					       alias->source->detail = object[L"detail"]->String();
					       break;
				       }
			case MAX_MIND:{
					      alias->source = new MaxMindSource();
					      alias->source->detail = object[L"detail"]->String();
					      break;
				      }
		};		

	}else{
		alias->source = NULL;
		//Can I already add entries:
		if(object.has(L"items")){
			JSONArray items = object[L"items"]->AsArray();
			for(int x= 0; x < items.size(); x++){
				alias->addEntry(items[x]->String());
			}
		}	
	}

	ret = firewall->aliases->create(alias);

	JSONObject jret;
	jret.put(L"loadstate", new JSONValue((double) ret));
	return jret;
}


JSONObject FirewallDelegate::aliases_del(JSONObject object) {
	Alias* target = firewall->aliases->get(object[L"id"]->String());
	if(target){
		int ret = firewall->aliases->del(target);
		if(ret < 0){
			throw new DelegateGeneralException("Could not delete alias, it is being used");
		}
	}else{
		throw new DelegateGeneralException("Could not find alias");
	}
	return JSONObject();
}

JSONObject FirewallDelegate::aliases_del_bulk(JSONObject object) {
	JSONArray ids = object[L"ids"]->AsArray();
	for(int x= 0; x < ids.size(); x++){
		Alias* target = firewall->aliases->get(ids[x]->String());
		if(target){
			int ret = firewall->aliases->del(target);
			if(ret < 0){
				throw new DelegateGeneralException("Could not delete alias, it is being used");
			}
		}	
	}

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_alias_list(JSONObject object) {
	JSONObject ret;
	JSONArray items;

	Alias* alias = firewall->aliases->get(object[L"id"]->String());
	if (alias != NULL) {
		std::list<std::string> entries = alias->listEntries();
		for (std::list<std::string>::iterator iter = entries.begin();
				iter != entries.end();
				iter++) {

			items.push_back(new JSONValue((string) (*iter)));
		}
		ret.put(L"items", new JSONValue(items));
		ret.put(L"type", new JSONValue((double) alias->type()));
		return ret;
	}

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_alias_add(JSONObject object) {
	JSONObject ret;

	Alias* alias = firewall->aliases->get(object[L"id"]->String());
	if (alias) {
		alias->addEntry(object[L"value"]->String());
		firewall->aliases->save();
	}

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_alias_del(JSONObject object) {
	JSONObject ret;

	Alias* alias = firewall->aliases->get(object[L"id"]->String());
	if (alias) {
		alias->removeEntry(object[L"value"]->String());
		firewall->aliases->save();
	}
	return JSONObject();
}

JSONObject FirewallDelegate::tracker_size(){
	firewall->connectionTracker->lock.lock();
	int size = firewall->connectionTracker->listConnections().size();
	firewall->connectionTracker->lock.unlock();

	JSONObject ret;
	ret.put(L"size", new JSONValue((double) size));
	return ret;
}

JSONObject FirewallDelegate::tracker_list(JSONObject object) {
	JSONObject ret;
	JSONArray conns;

	firewall->connectionTracker->lock.lock();
	vector<SFwallCore::Connection*> connections = firewall->connectionTracker->listConnections();
	for (int x = 0; x < connections.size(); x++) {
		SFwallCore::Connection* connection = connections[x];
		if (!connection->hasExpired()) {
			if(object.has(L"filterTime")){
				int filterTime = object[L"filterTime"]->AsNumber();
				if(connection->getTime() < filterTime){
					continue;
				}
			}

			JSONObject o;

			o.put(L"sourceIp", new JSONValue((string) IP4Addr::ip4AddrToString(connection->getSrcIp())));
			o.put(L"destIp", new JSONValue((string) IP4Addr::ip4AddrToString(connection->getDstIp())));
			o.put(L"bytes", new JSONValue((double) connection->getUpload() + connection->getDownload()));
			o.put(L"protocol", new JSONValue((double) connection->getProtocol()));

			Session* session = connection->getSession();
			if (session) {
				o.put(L"user", new JSONValue((string) session->getUser()->getUserName()));
			}

			if (connection->getProtocol() == SFwallCore::TCP) {
				SFwallCore::TcpConnection* tcp = dynamic_cast<SFwallCore::TcpConnection*> (connection);
				o.put(L"sourcePort", new JSONValue((double) tcp->getSourcePort()));
				o.put(L"destPort", new JSONValue((double) tcp->getDestinationPort()));

				o.put(L"state", new JSONValue((string) tcp->getState()->echo()));
			}

			if (connection->getProtocol() == SFwallCore::UDP) {
				SFwallCore::UdpConnection* udp = dynamic_cast<SFwallCore::UdpConnection*> (connection);
				o.put(L"sourcePort", new JSONValue((double) udp->getSourcePort()));
				o.put(L"destPort", new JSONValue((double) udp->getDestinationPort()));
			}

			if (connection->idle() > (60 * 2)) {
				o.put(L"isIdle", new JSONValue(true));
			}

			o.put(L"website", new JSONValue((string) connection->getHttpHost()));

			conns.push_back(new JSONValue(o));
		}
	}

	firewall->connectionTracker->lock.unlock();
	ret.put(L"connections", new JSONValue(conns));
	return ret;
}

JSONObject FirewallDelegate::tracker_terminate(JSONObject obj) {
	SFwallCore::PlainConnTracker* connTracker = firewall->connectionTracker;

	SFwallCore::ConnectionCriteria targetCriteria;
	targetCriteria.setSource(IP4Addr::stringToIP4Addr(obj[L"source"]->String()));
	targetCriteria.setDest(IP4Addr::stringToIP4Addr(obj[L"dest"]->String()));
	targetCriteria.setSourcePort(obj[L"sourcePort"]->AsNumber());
	targetCriteria.setDestPort(obj[L"destPort"]->AsNumber());

	if(eventDb){
		EventParams params;
		params["source"] = obj[L"source"]->String();
		params["sourcePort"] = obj[L"sourcePort"]->AsNumber();
		params["dest"] = obj[L"dest"]->String();
		params["destPort"] = obj[L"destPort"]->AsNumber();
		eventDb->add(new Event(AUDIT_FIREWALL_CONNECTIONS_TERMINATE, params));
	}

	list<SFwallCore::Connection*> connections = connTracker->listConnections(targetCriteria);
	for (list<SFwallCore::Connection*>::iterator iter = connections.begin(); iter != connections.end(); iter++) {
		SFwallCore::Connection* conn = (*iter);
		firewall->connectionTracker->terminate(conn);
	}

	return JSONObject();
}

JSONObject FirewallDelegate::acls_filter_up(JSONObject args) {
	SFwallCore::ACLStore* store = firewall->acls;
	std::string id= args[L"id"]->String();
	Rule* rule = store->getRuleById(id);	
	if(rule){
		store->moveup(rule);
	}else{
		throw new DelegateGeneralException("could not find rule");
	}

	return JSONObject();
}

JSONObject FirewallDelegate::acls_filter_down(JSONObject args) {
	SFwallCore::ACLStore* store = firewall->acls;
	std::string id= args[L"id"]->String();
	Rule* rule = store->getRuleById(id);
	if(rule){
		store->movedown(rule);
	}else{
		throw new DelegateGeneralException("could not find rule");
	}

	return JSONObject();
}

JSONObject FirewallDelegate::acls_filter_delete(JSONObject args) {
	std::string id = args[L"id"]->String();

	SFwallCore::ACLStore* store = firewall->acls;
	SFwallCore::Rule* target = firewall->acls->getRuleById(id);
	if (target) {
		store->deleteAclEntry(target);
	}else {
		throw new DelegateGeneralException("rule position was not valid");
	}

	store->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_nat_delete(JSONObject args) {
	return acls_filter_delete(args);
}

void FirewallDelegate::serializeRule(JSONObject& rule, SFwallCore::Rule* target){
	rule.put(L"enabled", new JSONValue((bool) target->enabled));

	if(target->sport_start != -1 && target->sport_end != -1){
		rule.put(L"sport_start", new JSONValue((double) target->sport_start));
		rule.put(L"sport_end", new JSONValue((double) target->sport_end));
	}

	if(target->dport_start != -1 && target->dport_end != -1){
		rule.put(L"dport_start", new JSONValue((double) target->dport_start));
		rule.put(L"dport_end", new JSONValue((double) target->dport_end));
	}

	if (target->source != -1) {
		rule.put(L"sourceAddress", new JSONValue((string) IP4Addr::ip4AddrToString(target->source)));
		rule.put(L"sourceSubnet", new JSONValue((string) IP4Addr::ip4AddrToString(target->sourceMask)));
	}

	if (target->sportList.size() != 0) {
		stringstream res;
		for(set<int>::iterator iter = target->sportList.begin();
				iter != target->sportList.end();
				iter++){
			res << (*iter) << ",";
		}

		rule.put(L"sourcePort", new JSONValue((string) res.str()));
	}
	if (target->getSourceDevice()!= NULL) {
		rule.put(L"sourceInterface", new JSONValue((string) target->getSourceDevice()->getInterface()));
	}

	if (target->getDestDevice() != NULL) {
		rule.put(L"destInterface", new JSONValue((string) target->getDestDevice()->getInterface()));
	}

	if (target->sourceAlias) {
		rule.put(L"sourceAlias", new JSONValue((std::string) target->sourceAlias->name));
		rule.put(L"sourceAliasId", new JSONValue((std::string) target->sourceAlias->id));
	}

	if (target->dest != -1) {
		rule.put(L"destAddress", new JSONValue((string) IP4Addr::ip4AddrToString(target->dest)));
		rule.put(L"destSubnet", new JSONValue((string) IP4Addr::ip4AddrToString(target->destMask)));
	}

	if (target->dportList.size() != 0) {
		stringstream res;
		for(set<int>::iterator iter = target->dportList.begin();
				iter != target->dportList.end();
				iter++){
			res << (*iter) << ",";
		}

		rule.put(L"destPort", new JSONValue((string) res.str()));
	}
	if (target->destAlias) {
		rule.put(L"destAlias", new JSONValue((string) target->destAlias->name));
		rule.put(L"destAliasId", new JSONValue((string) target->destAlias->id));
	}

	if (target->comment.size() > 0) {
		rule.put(L"comment", new JSONValue((string) target->comment));
	}

	rule.put(L"hwAddress", new JSONValue((string) target->hwAddr));

	if (target->type != -1) {
		switch (target->type) {
			case TCP:
				rule.put(L"protocol", new JSONValue((string) "TCP"));
				break;

			case (SFwallCore::proto)UDP:
				rule.put(L"protocol", new JSONValue((string) "UDP"));
				break;

			case ICMP:
				rule.put(L"protocol", new JSONValue((string) "ICMP"));
				break;

			case IGMP:
				rule.put(L"protocol", new JSONValue((string) "IGMP"));
				break;
		};
	}

}

JSONObject FirewallDelegate::acls_list(JSONObject) {
	JSONObject ret;
	JSONArray filter;
	JSONArray nat;

	SFwallCore::ACLStore* store = firewall->acls;
	vector<SFwallCore::NatRule*> natRules = store->listNatRules();
	for (int x = 0; x < natRules.size(); x++) {
		JSONObject rule;

		SFwallCore::NatRule* target = natRules[x];
		if(target->valid){
			serializeRule(rule, target);

			if (target->natType == SFwallCore::SNAT) {
				if(target->natTarget != -1){
					rule.put(L"masqueradeTarget", new JSONValue((string) IP4Addr::ip4AddrToString(target->natTarget)));
				}else{
					rule.put(L"masqueradeTargetDevice", new JSONValue((string) target->natTargetDevicePtr->getInterface()));
				}	

				rule.put(L"action", new JSONValue((double) 2));
			} else if (target->natType == SFwallCore::DNAT) {
				rule.put(L"action", new JSONValue((double) 3));
				rule.put(L"forwardTarget", new JSONValue((string) IP4Addr::ip4AddrToString(target->natTarget)));
				rule.put(L"forwardPort", new JSONValue((double) target->natPort));
			}

			if (target->groupid != -1) {
				Group* group = groupDb->getGroup(target->groupid);
				if (group != NULL) {
					rule.put(L"group", new JSONValue((string) group->getName()));
				}
			}

			rule.put(L"pos", new JSONValue((double) x));
			rule.put(L"hits", new JSONValue((double) target->count));
			rule.put(L"id", new JSONValue((string) target->id));
			nat.push_back(new JSONValue(rule));
		}
	}

	vector<SFwallCore::FilterRule*> rules = store->listFilterRules();
	for (int x = 0; x < rules.size(); x++) {
		JSONObject rule;

		SFwallCore::FilterRule* target = rules[x];
		if(target->valid){
			serializeRule(rule, target);

			rule.put(L"action", new JSONValue((double) target->action));
			rule.put(L"ignoreconntrack", new JSONValue((bool) target->ignoreconntrack));

			if (target->groupid != -1) {
				Group* group = groupDb->getGroup(target->groupid);
				if (group != NULL) {
					rule.put(L"group", new JSONValue((string) group->getName()));
				}
			}

			rule.put(L"hits", new JSONValue((double) target->count));
			rule.put(L"pos", new JSONValue((double) x));
			rule.put(L"id", new JSONValue((string) target->id));
			rule.put(L"log", new JSONValue((bool) target->log));			

			if(target->startHour != -1 && target->finishHour != -1){
				rule.put(L"startHour", new JSONValue((double) target->startHour));
				rule.put(L"finishHour", new JSONValue((double) target->finishHour));
			}
			filter.push_back(new JSONValue(rule));
		}
	}

	JSONArray priority;
	vector<SFwallCore::PriorityRule*> priorityRules = store->listPriorityRules();
	for(int x =0; x < priorityRules.size(); x++){
		JSONObject rule;

		SFwallCore::PriorityRule* target = priorityRules[x];
		if(target->valid){
			serializeRule(rule, target);
			rule.put(L"nice", new JSONValue((double) target->nice));	
			rule.put(L"hits", new JSONValue((double) target->count));
			rule.put(L"pos", new JSONValue((double) x));
			rule.put(L"id", new JSONValue((string) target->id));

			priority.push_back(new JSONValue(rule));
		}

	}	

	ret.put(L"priority", new JSONValue(priority));
	ret.put(L"nat", new JSONValue(nat));
	ret.put(L"normal", new JSONValue(filter));
	return ret;
}

JSONObject FirewallDelegate::acls_add(JSONObject args) {
	//Work out what kind of rule we are dealing with first:
	int action = args[L"action"]->AsNumber();
	SFwallCore::Rule* rule = NULL;
	if (action == 0 || action == 1) {
		rule = new SFwallCore::FilterRule();
		rule->enabled = false;
	} else if (action == 2 || action == 3){
		rule = new SFwallCore::NatRule();
		rule->enabled = false;
	}else if(action == 4){
		rule = new SFwallCore::PriorityRule();
		rule->enabled = false;
	}

	if(args.has(L"id")){
		rule->id = args[L"id"]->String();
	}

	if (args.hasString(L"sourceAddress")) {
		rule->source = IP4Addr::stringToIP4Addr(args[L"sourceAddress"]->String());
		rule->sourceMask = IP4Addr::stringToIP4Addr(args[L"sourceSubnet"]->String());
	} else if (args.has(L"sourceAlias")) {
		SFwallCore::Alias* sourceAlias = firewall->aliases->get(args[L"sourceAlias"]->String());
		if (sourceAlias) {
			rule->sourceAlias = sourceAlias;
		}
	}

	if (args.hasString(L"destAddress")) {
		rule->dest = IP4Addr::stringToIP4Addr(args[L"destAddress"]->String());
		rule->destMask = IP4Addr::stringToIP4Addr(args[L"destSubnet"]->String());
	} else if (args.has(L"destAlias")) {
		SFwallCore::Alias* destAlias = firewall->aliases->get(args[L"destAlias"]->String());
		if (destAlias) {
			rule->destAlias = destAlias;
		}
	}

	if(args.has(L"dport_start") && args.has(L"dport_end")){
		rule->dport_start = args[L"dport_start"]->AsNumber();
		rule->dport_end = args[L"dport_end"]->AsNumber();
	}

	if(args.has(L"sport_start") && args.has(L"sport_end")){
		rule->sport_start = args[L"sport_start"]->AsNumber();
		rule->sport_end = args[L"sport_end"]->AsNumber();
	}

	if (args.has(L"destPort")) {
		rule->dportList.clear();

		string temp = args[L"destPort"]->String();
		vector<string> pieces;
		split(temp, ',', pieces);
		for(int y = 0; y < pieces.size(); y++){
			rule->dportList.insert(atoi(pieces[y].c_str()));
		}
	}

	if (args.has(L"sourcePort")) {
		rule->sportList.clear();
		string temp = args[L"sourcePort"]->String();
		vector<string> pieces;
		split(temp, ',', pieces);
		for(int y = 0; y < pieces.size(); y++){
			rule->sportList.insert(atoi(pieces[y].c_str()));
		}
	}

	if (args.has(L"destDev")) {
		rule->destDevName = args[L"destDev"]->String();
	}

	if (args.has(L"sourceDev")) {
		rule->sourceDevName = args[L"sourceDev"]->String();
	}

	if (args.has(L"protocol")) {
		string proto = args[L"protocol"]->String();
		if (proto.compare("TCP") == 0) {
			rule->type = TCP;
		} else if (proto.compare("UDP") == 0) {
			rule->type = UDP;
		} else if (proto.compare("ICMP") == 0) {
			rule->type = ICMP;
		} else if (proto.compare("IGMP") == 0) {
			rule->type = IGMP;
		}
	}

	if (args.has(L"hwAddress")) {
		rule->hwAddr = args[L"hwAddress"]->String();
	}

	if (args.has(L"comment")) {
		rule->comment = args[L"comment"]->String();
	}

	if (args.has(L"groupid")) {
		rule->groupid = args[L"groupid"]->AsNumber();
	}

	if (args.has(L"log")) {
		if(action == 0 || action == 1){
			SFwallCore::FilterRule* crule = (SFwallCore::FilterRule*) rule;
			crule->log = args[L"log"]->AsBool();
		}
	}

	if(args.has(L"startHour") && args.has(L"finishHour")){
		rule->startHour = args[L"startHour"]->AsNumber();
		rule->finishHour = args[L"finishHour"]->AsNumber();
	}

	if(args.has(L"ignoreconntrack")){
		rule->ignoreconntrack = args[L"ignoreconntrack"]->AsBool();
	}

	if (action == 0 || action == 1) {
		rule->action = action;
	} else if (action == 2) {
		rule->setNatType(SFwallCore::SNAT);
		if(args.has(L"masqueradeTarget")){
			rule->setNatTarget(IP4Addr::stringToIP4Addr(args[L"masqueradeTarget"]->String()));
		}else if(args.has(L"masqueradeTargetDevice")){
			rule->setNatTargetDevice(interfaceManager->get(args[L"masqueradeTargetDevice"]->String()));
		}else{
			throw new DelegateGeneralException("no snat target specified");
		}

		rule->action = 2;
	} else if (action == 3) {
		rule->setNatType(SFwallCore::DNAT);
		rule->setNatTarget(IP4Addr::stringToIP4Addr(args[L"forwardTarget"]->String()));
		rule->setNatPort(args[L"forwardPort"]->AsNumber());
		rule->action = 3;
	}else if(action == 4){
		PriorityRule* pr = (PriorityRule*) rule;
		pr->nice = args[L"nice"]->AsNumber();
	}

	firewall->acls->createAclEntry(rule);
	return JSONObject();
}

JSONObject FirewallDelegate::acls_add_trafficshaper(JSONObject args) {
	TsRule* rule = new TsRule(++firewall->trafficShaper->store->count);
	//Destination:
	if (args.has(L"destinationBlock")) {
		bool isBlock = args[L"destinationBlock"]->AsBool();
		if (isBlock) {
			if (args.has(L"destination")) {
				rule->filter.block_dest = IP4Addr::stringToIP4Addr(args[L"destination"]->String());
				rule->filter.block_destMask = IP4Addr::stringToIP4Addr(args[L"destinationMask"]->String());
				rule->filter.dest = IP4Addr::stringToIP4Addr(args[L"destination"]->String());
				rule->filter.destMask = IP4Addr::stringToIP4Addr(args[L"destinationMask"]->String());

			}

		} else {
			if (args.has(L"destination")) {
				rule->filter.dest = IP4Addr::stringToIP4Addr(args[L"destination"]->String());
				rule->filter.destMask = IP4Addr::stringToIP4Addr(args[L"destinationMask"]->String());
			}
		}
	}

	if (args.has(L"sourceBlock")) {
		bool isBlock = args[L"sourceBlock"]->AsBool();
		if (isBlock) {
			if (args.has(L"source")) {
				rule->filter.block_source = IP4Addr::stringToIP4Addr(args[L"source"]->String());
				rule->filter.block_sourceMask = IP4Addr::stringToIP4Addr(args[L"sourceMask"]->String());
				rule->filter.source = IP4Addr::stringToIP4Addr(args[L"source"]->String());
				rule->filter.sourceMask = IP4Addr::stringToIP4Addr(args[L"sourceMask"]->String());

			}

		} else {
			if (args.has(L"source")) {
				rule->filter.source = IP4Addr::stringToIP4Addr(args[L"source"]->String());
				rule->filter.sourceMask = IP4Addr::stringToIP4Addr(args[L"sourceMask"]->String());
			}
		}
	}

	if (args.has(L"sourcePort")) {
		rule->filter.sport = args[L"sourcePort"]->AsNumber();
	}

	if (args.has(L"destinationPort")) {
		rule->filter.dport = args[L"destinationPort"]->AsNumber();
	}

	if (args.has(L"groupid")) {
		rule->filter.block_groupid = args[L"groupid"]->AsNumber();
		// check group ID's existance.  
		Group* group = groupDb->getGroup(rule->filter.block_groupid);

		if (group == NULL) { // Group does not exist so bag the command.
			throw new DelegateGeneralException("Group not found");
		}
	}

	rule->uploadRate = args[L"upload"]->AsNumber();
	rule->downloadRate = args[L"download"]->AsNumber();

	//Check thats this rule isnt a complete wildcard with tiny values:
	if (rule->uploadRate < 1024 || rule->uploadRate < 1024) {
		//Dont create a rule, the values are way to small - this is madness
		throw new DelegateGeneralException("values are to small, this is madness");
	} else {
		firewall->trafficShaper->store->add(rule);
	}
	return JSONObject();
}

JSONObject FirewallDelegate::acls_list_trafficshaper(JSONObject args) {
	JSONObject ret;
	JSONArray arr;

	vector<TsRule*> filter = firewall->trafficShaper->store->list();
	for (int x = 0; x < filter.size(); x++) {
		TokenFilter* target = &filter[x]->filter;
		JSONObject o;
		//Destination:
		if (target->block_dest != -1) {
			o.put(L"destinationBlock", new JSONValue((bool) true));
			o.put(L"destination", new JSONValue((string) IP4Addr::ip4AddrToString(target->block_dest)));
			o.put(L"destinationMask", new JSONValue((string) IP4Addr::ip4AddrToString(target->block_destMask)));
		} else if (target->dest != -1) {
			o.put(L"destinationBlock", new JSONValue((bool) false));
			o.put(L"destination", new JSONValue((string) IP4Addr::ip4AddrToString(target->dest)));
			o.put(L"destinationMask", new JSONValue((string) IP4Addr::ip4AddrToString(target->destMask)));
		}

		if (target->dport != 0) {
			o.put(L"destinationPort", new JSONValue((double) target->dport));
		}

		//Source
		if (target->block_source != -1) {
			o.put(L"sourceBlock", new JSONValue((bool) true));
			o.put(L"source", new JSONValue((string) IP4Addr::ip4AddrToString(target->block_source)));
			o.put(L"sourceMask", new JSONValue((string) IP4Addr::ip4AddrToString(target->block_sourceMask)));
		} else if (target->source != -1) {
			o.put(L"sourceBlock", new JSONValue((bool) false));
			o.put(L"source", new JSONValue((string) IP4Addr::ip4AddrToString(target->source)));
			o.put(L"sourceMask", new JSONValue((string) IP4Addr::ip4AddrToString(target->sourceMask)));
		}

		if (target->sport != 0) {
			o.put(L"sourcePort", new JSONValue((double) target->sport));
		}

		//Group
		if (target->block_groupid != -1) {
			o.put(L"groupid", new JSONValue((double) target->block_groupid));
			o.put(L"groupname", new JSONValue((string) groupDb->getGroup(target->block_groupid)->getName()));
		}

		o.put(L"upload", new JSONValue((double) filter[x]->getUploadRate()));
		o.put(L"download", new JSONValue((double) filter[x]->downloadRate));
		o.put(L"id", new JSONValue((double) filter[x]->id));

		o.put(L"buckets", new JSONValue((double) filter[x]->getNoActiveBuckets()));
		o.put(L"hits", new JSONValue((double) filter[x]->getHits()));

		arr.push_back(new JSONValue(o));
	}

	ret.put(L"items", new JSONValue(arr));
	return ret;
}

JSONObject FirewallDelegate::acls_list_del_trafficshaper(JSONObject args) {
	TsRule* rule = firewall->trafficShaper->store->get(args[L"id"]->AsNumber());
	if (rule) {
		firewall->trafficShaper->store->del(rule);
	}else{
		throw new DelegateGeneralException("could not find rule");
	}

	return JSONObject();
}

JSONObject FirewallDelegate::acls_enable(JSONObject obj){
	SFwallCore::ACLStore* store = firewall->acls;
	std::string id = obj[L"id"]->String();
	Rule* target = store->getRuleById(id);	
	if(target){
		target->enabled = true;
	}else{
		throw new DelegateGeneralException("could not find rule");
	}
	store->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_disable(JSONObject obj){
	SFwallCore::ACLStore* store = firewall->acls;
	std::string id = obj[L"id"]->String();
	Rule* target = store->getRuleById(id);
	if(target){
		target->enabled = false;
	}else{
		throw new DelegateGeneralException("could not find rule");
	}
	store->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_status(JSONObject obj){
	JSONObject ret;
	SFwallCore::ACLStore* store = firewall->acls;
	std::string id = obj[L"id"]->String();
	Rule* target = store->getRuleById(id);
	if(target){
		ret.put(L"status", new JSONValue((bool) target->enabled));
	}else{
		throw new DelegateGeneralException("could not find rule");
	}
	return ret; 
}

JSONObject FirewallDelegate::acls_webfilter_rules_list(JSONObject obj){
	getHttpApplicationFilter()->lock();
	JSONObject ret;
	JSONArray array;

	list<SFwallCore::FilterCriteria*> rules = getHttpApplicationFilter()->getRules();
	for(list<SFwallCore::FilterCriteria*>::iterator iter = rules.begin(); iter != rules.end(); iter++){
		JSONObject rule;
		SFwallCore::FilterCriteria* target = (*iter);
		rule.put(L"id", new JSONValue((string) target->id));		

		if(target->sourceIp != -1){
			rule.put(L"sourceIp", new JSONValue((string) IP4Addr::ip4AddrToString(target->sourceIp)));
			rule.put(L"sourceMask", new JSONValue((string) IP4Addr::ip4AddrToString(target->sourceMask)));
		}

		JSONArray groups;
		for(list<Group*>::iterator giter = target->groups.begin(); giter != target->groups.end(); giter++){
			JSONObject group;
			group.put(L"group", new JSONValue((double) (*giter)->getId()));	
			group.put(L"groupName", new JSONValue((*giter)->getName()));	
			groups.push_back(new JSONValue(group));
		}	

		rule.put(L"groups", new JSONValue(groups));

		JSONArray lists;
		for(list<WebsiteListAlias*>::iterator aiter = target->lists.begin(); aiter != target->lists.end(); aiter++){
			JSONObject l;
			l.put(L"list", new JSONValue((string) (*aiter)->id));
			l.put(L"listName", new JSONValue((string) (*aiter)->name));
			lists.push_back(new JSONValue(l));
		}

		rule.put(L"lists", new JSONValue(lists));

		rule.put(L"action", new JSONValue((double) target->action));
		rule.put(L"fireEvent", new JSONValue((bool) target->fireEvent));
		rule.put(L"redirect", new JSONValue((bool) target->redirect));
		rule.put(L"redirectUrl", new JSONValue((string) target->redirectUrl));
		rule.put(L"startTime", new JSONValue((double) target->startTime));
		rule.put(L"endTime", new JSONValue((double) target->endTime));
		rule.put(L"exclusive", new JSONValue((bool) target->exclusive));

		rule.put(L"startHour", new JSONValue((double) target->startTime / 100));
		rule.put(L"startMin", new JSONValue((double) target->startTime - (target->startTime / 100)));
		rule.put(L"endHour", new JSONValue((double) (int) (target->endTime / 100)));
		rule.put(L"endMin", new JSONValue((double) (int) target->endTime - (target->endTime / 100)));
		//days:
		rule.put(L"mon", new JSONValue((bool) target->mon));
		rule.put(L"tues", new JSONValue((bool) target->tues));
		rule.put(L"wed", new JSONValue((bool) target->wed));
		rule.put(L"thurs", new JSONValue((bool) target->thurs));
		rule.put(L"fri", new JSONValue((bool) target->fri));
		rule.put(L"sat", new JSONValue((bool) target->sat));
		rule.put(L"sun", new JSONValue((bool) target->sun));
		rule.put(L"enabled", new JSONValue((bool) target->enabled));

		if(target->sourceAlias.size() > 0){
			JSONArray sourceArray;
			for(list<Alias*>::iterator siter = target->sourceAlias.begin(); siter != target->sourceAlias.end(); siter++){
				JSONObject s;
				s.put(L"sourceAlias", new JSONValue((string) (*siter)->id));
				s.put(L"sourceAliasName", new JSONValue((string) (*siter)->name));
				sourceArray.push_back(new JSONValue(s));
			}
			rule.put(L"sourceAlias", new JSONValue(sourceArray));
		}

		array.push_back(new JSONValue(rule));	
	}

	ret.put(L"rules", new JSONValue(array));
	getHttpApplicationFilter()->unlock();
	return ret; 
}

JSONObject FirewallDelegate::acls_webfilter_rules_add(JSONObject obj){
	bool newRule = true;
	FilterCriteria* rule = NULL; 
	if(obj.has(L"id")){
		rule = getHttpApplicationFilter()->get(obj[L"id"]->String());	
		newRule = false;
	}		

	if(!rule){
		rule = new FilterCriteria();
		rule->id = StringUtils::genRandom();
	}

	//Must get the correct list:
	if(obj.has(L"sourceIp") && obj.has(L"sourceMask")){
		if(obj[L"sourceIp"]->String().size() > 0){
			rule->sourceIp = IP4Addr::stringToIP4Addr(obj[L"sourceIp"]->String());
			rule->sourceMask = IP4Addr::stringToIP4Addr(obj[L"sourceMask"]->String());
		}
	}

	if(obj.has(L"group")){
		rule->groups.clear();
		JSONArray groupArray = obj[L"group"]->AsArray();
		for(int x= 0;x < groupArray.size(); x++){
			Group* g = groupDb->getGroup(groupArray[x]->AsNumber());
			if(g){
				rule->groups.push_back(g);
			}
		}
	}

	if(obj.has(L"exclusive")){
		rule->exclusive = obj[L"exclusive"]->AsBool();
	}

	rule->action = (ApplicationFilterAction) obj[L"action"]->AsNumber();
	rule->fireEvent = obj[L"fireEvent"]->AsBool();
	rule->redirect = obj[L"redirect"]->AsBool();
	rule->redirectUrl = obj[L"redirectUrl"]->String();
	rule->startTime= obj[L"startTime"]->AsNumber();
	rule->endTime = obj[L"endTime"]->AsNumber();

	//Days:
	rule->mon = obj[L"mon"]->AsBool();
	rule->tues= obj[L"tues"]->AsBool();
	rule->wed = obj[L"wed"]->AsBool();
	rule->thurs= obj[L"thurs"]->AsBool();
	rule->fri= obj[L"fri"]->AsBool();
	rule->sat= obj[L"sat"]->AsBool();
	rule->sun= obj[L"sun"]->AsBool();


	JSONArray lists = obj[L"list"]->AsArray();
	rule->lists.clear();
	for(int x= 0; x < lists.size(); x++){
		Alias* alias = getHttpApplicationFilter()->getAliasDb()->get(lists[x]->String());
		if(alias && alias->type() == WEBSITE_LIST){
			rule->lists.push_back(((WebsiteListAlias*) alias));
		}
	}

	if(obj.has(L"sourceAlias")){
		rule->sourceAlias.clear();
		JSONArray groupArray = obj[L"sourceAlias"]->AsArray();
		for(int x= 0;x < groupArray.size(); x++){
			rule->sourceAlias.push_back(getHttpApplicationFilter()->getAliasDb()->get(groupArray[x]->String()));
		}
	}

	if(obj.has(L"enabled")){
		rule->enabled = obj[L"enabled"]->AsBool();
	}

	getHttpApplicationFilter()->lock();
	if(newRule){
		httpApplicationFilter->addRule(rule);
	}
	getHttpApplicationFilter()->save();
	getHttpApplicationFilter()->unlock();
	return JSONObject();
}	

JSONObject FirewallDelegate::acls_webfilter_rules_deleteall(JSONObject obj){
	getHttpApplicationFilter()->lock();
	list<FilterCriteria*>& lists = getHttpApplicationFilter()->getRules();
	for(list<FilterCriteria*>::iterator iter = lists.begin(); iter != lists.end(); iter++){
		FilterCriteria* list = (*iter);
		delete list;
	}	
	
	lists.clear();
	getHttpApplicationFilter()->save();
	getHttpApplicationFilter()->unlock();
	return JSONObject();
}
JSONObject FirewallDelegate::acls_webfilter_rules_remove(JSONObject obj){
	getHttpApplicationFilter()->lock();
	list<FilterCriteria*> lists = getHttpApplicationFilter()->getRules();
	for(list<FilterCriteria*>::iterator iter = lists.begin(); iter != lists.end(); iter++){
		FilterCriteria* list = (*iter);
		if(list->id.compare(obj[L"id"]->String()) == 0){
			getHttpApplicationFilter()->removeRule(list);
			break;
		}
	}	
	getHttpApplicationFilter()->save();
	getHttpApplicationFilter()->unlock();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_moveup(JSONObject obj){
	getHttpApplicationFilter()->lock();
	FilterCriteria* criteria = getHttpApplicationFilter()->get(obj[L"id"]->String());
	getHttpApplicationFilter()->moveup(criteria);	

	getHttpApplicationFilter()->save();
	getHttpApplicationFilter()->unlock();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_movedown(JSONObject obj){
	getHttpApplicationFilter()->lock();
	FilterCriteria* criteria = getHttpApplicationFilter()->get(obj[L"id"]->String());     
	getHttpApplicationFilter()->movedown(criteria);

	getHttpApplicationFilter()->save();
	getHttpApplicationFilter()->unlock();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_enable(JSONObject obj){
	getHttpApplicationFilter()->lock();
	FilterCriteria* criteria = getHttpApplicationFilter()->get(obj[L"id"]->String());
	criteria->enabled = true;
	getHttpApplicationFilter()->save();
	getHttpApplicationFilter()->unlock();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_disable(JSONObject obj){
	getHttpApplicationFilter()->lock();
	FilterCriteria* criteria = getHttpApplicationFilter()->get(obj[L"id"]->String());     
	criteria->enabled = false;

	getHttpApplicationFilter()->save();
	getHttpApplicationFilter()->unlock();
	return JSONObject();
}


JSONObject FirewallDelegate::deny_list(JSONObject obj){
	JSONObject ret;
	JSONArray arr;
	for(set<unsigned int>::iterator iter = firewall->acls->getDenyList().begin(); iter != firewall->acls->getDenyList().end(); iter++){
		arr.push_back(new JSONValue((string) IP4Addr::ip4AddrToString((*iter))));
	}	

	ret.put(L"list", new JSONValue(arr));
	return ret;	
}

JSONObject FirewallDelegate::deny_del(JSONObject obj){
	firewall->acls->removeFromDenyList(IP4Addr::stringToIP4Addr(obj[L"ip"]->String()));	
	return JSONObject();
}
