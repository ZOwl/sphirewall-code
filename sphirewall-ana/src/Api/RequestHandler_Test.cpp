/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <gtest/gtest.h>

using namespace std;

#include "Api/RequestHandler.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Common/sphirewall_bandwidth_types.h"
#include "Database/BandwidthDb.h"
#include "Utils/IP4Addr.h"
#include "Database/TrafficAnalyticsDb.h"
#include "Database/MonitoringDb.h"
#include "Utils/TimeWrapper.h"

class Tester{
	public:
		Tester(){
			system("rm /tmp/bwDb*");
			SLogger::Logger* logger = new SLogger::Logger(); 
			this->bwDb = new BandwidthDb("/tmp/bwDb", logger->getDefault());

			this->handler = new RequestHandler();
			this->handler->setBandwidthDb(this->bwDb);
		}

		JSONObject create(	std::string sourceIp, 
					std::string destIp, 
					std::string user, 
					double sourcePort, double destPort, 
					double t, double upload, double download, 
					std::string inputDev, std::string outputDev, 
					double protocol, std::string httpHost){

			JSONObject i;
			i.put(L"sourceIp", new JSONValue((string) sourceIp));
			i.put(L"hwAddress", new JSONValue((string) sourceIp + "-macaddress"));
			i.put(L"destIp", new JSONValue((string) destIp));
			i.put(L"user", new JSONValue((string) user));
			i.put(L"sourcePort", new JSONValue((double) sourcePort));
			i.put(L"destPort", new JSONValue((double) destPort));
			i.put(L"time", new JSONValue((double) t));
			i.put(L"upload", new JSONValue((double) upload));
			i.put(L"download", new JSONValue((double) download));
			i.put(L"inputDev", new JSONValue((string) inputDev));
			i.put(L"outputDev", new JSONValue((string) outputDev));
			i.put(L"protocol", new JSONValue((double) protocol));
			i.put(L"httpHost", new JSONValue((string) httpHost));

			return i;
		}

		void insertToDb(JSONObject i){
			JSONObject obj;
			JSONArray arr;
			arr.push_back(new JSONValue(i));
			obj.put(L"items", new JSONValue(arr));
			handler->process("stats/bandwidth/insert", obj);
		}

		RequestHandler* handler;
		BandwidthDb* bwDb;
};

TEST(Ta, reduceurl){
	EXPECT_TRUE(TrafficAnalyticsDb::reduceurl("google.com").compare("google.com") == 0);	
	EXPECT_TRUE(TrafficAnalyticsDb::reduceurl("blah.testing.google.com").compare("google.com") == 0);	
	EXPECT_TRUE(TrafficAnalyticsDb::reduceurl("blah.testing.google.co.nz").compare("google.co.nz") == 0);	
	EXPECT_TRUE(TrafficAnalyticsDb::reduceurl("blah.testing.google.com.au").compare("google.com.au") == 0);	
}

TEST(RequestHandler, web_basic_over_long_period){
	//analytics/stats/bandwidth/query/web
	Tester* tester = new Tester();
        tester->insertToDb(tester->create("192.168.1.1", "224.12.33.4", "unknown", 33344, 80, time(NULL), 10, 0, "etho0", "etho1", 0, "google.com"));
        tester->insertToDb(tester->create("192.168.1.1", "224.12.33.4", "unknown", 33344, 80, time(NULL), 11, 3, "etho0", "etho1", 0, "google.com"));
        tester->insertToDb(tester->create("192.168.1.1", "224.12.33.2", "unknown", 33344, 80, time(NULL), 5,0, "etho0", "etho1", 0, "yahoo.com"));
        tester->insertToDb(tester->create("192.168.1.1", "224.12.33.1", "unknown", 33344, 80, time(NULL), 2,0, "etho0", "etho1", 0, "hotmail.com"));

        JSONObject a;
	std::string t;
	Time startTime = Time(0);
	Time today = Time(time(NULL));

	a.put(L"limit", new JSONValue((double) 10));
        a.put(L"startTime", new JSONValue((string) startTime.format("%Y-%m-%d")));
        a.put(L"endTime", new JSONValue((string) today.format("%Y-%m-%d")));
        a.put(L"metric", new JSONValue((string) "mbyte"));

	JSONObject request = tester->handler->process("analytics/stats/bandwidth/query/web", a);
        JSONArray rarr = request[L"items"]->AsArray();
        
	JSONObject real = rarr[0]->AsObject();
	cout << "real:" << real[L"httpHost"]->String() << endl;

        EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 21);
        EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 3);
        EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 0);
        EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"httpHost"]->String().compare("google.com") == 0);

        JSONObject real2 = rarr[1]->AsObject();
	cout << "real:" << real2[L"httpHost"]->String() << endl;
        EXPECT_TRUE(real2[L"tcpUp"]->AsNumber() == 5);
        EXPECT_TRUE(real2[L"tcpDown"]->AsNumber() == 0);
        EXPECT_TRUE(real2[L"udpUp"]->AsNumber() == 0);
        EXPECT_TRUE(real2[L"udpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real2[L"httpHost"]->String().compare("yahoo.com") == 0);

        JSONObject real1 = rarr[2]->AsObject();
	cout << "real:" << real1[L"httpHost"]->String() << endl;
        EXPECT_TRUE(real1[L"tcpUp"]->AsNumber() == 2);
        EXPECT_TRUE(real1[L"tcpDown"]->AsNumber() == 0);
        EXPECT_TRUE(real1[L"udpUp"]->AsNumber() == 0);
        EXPECT_TRUE(real1[L"udpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real1[L"httpHost"]->String().compare("hotmail.com") == 0);
}

TEST(RequestHandler, web_basic_small_period){
	//analytics/stats/bandwidth/query/web
	Tester* tester = new Tester();
        tester->insertToDb(tester->create("192.168.1.1", "224.12.33.4", "unknown", 33344, 80, time(NULL), 10, 0, "etho0", "etho1", 0, "google.com"));
        tester->insertToDb(tester->create("192.168.1.1", "224.12.33.4", "unknown", 33344, 80, time(NULL), 11,3, "etho0", "etho1", 0, "google.com"));
        tester->insertToDb(tester->create("192.168.1.1", "224.12.33.2", "unknown", 33344, 80, time(NULL), 5,0, "etho0", "etho1", 0, "yahoo.com"));

        JSONObject a;
	std::string t;
	Time startTime = Time(time(NULL));
	Time today = Time(time(NULL));

	a.put(L"limit", new JSONValue((double) 10));
        a.put(L"startTime", new JSONValue((string) startTime.format("%Y-%m-%d")));
        a.put(L"endTime", new JSONValue((string) today.format("%Y-%m-%d")));
        a.put(L"metric", new JSONValue((string) "bytes"));

	JSONObject request = tester->handler->process("analytics/stats/bandwidth/query/web", a);
        JSONArray rarr = request[L"items"]->AsArray();
        
	JSONObject real = rarr[0]->AsObject();
        EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 21);
        EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 3);
        EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 0);
        EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"httpHost"]->String().compare("google.com") == 0);

        JSONObject real1 = rarr[1]->AsObject();
        EXPECT_TRUE(real1[L"tcpUp"]->AsNumber() == 5);
        EXPECT_TRUE(real1[L"tcpDown"]->AsNumber() == 0);
        EXPECT_TRUE(real1[L"udpUp"]->AsNumber() == 0);
        EXPECT_TRUE(real1[L"udpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real1[L"httpHost"]->String().compare("yahoo.com") == 0);
}


TEST(RequestHandler, web_basic_filteringonsource){
	//analytics/stats/bandwidth/query/web
	Tester* tester = new Tester();
        tester->insertToDb(tester->create("192.168.1.1", "224.12.33.4", "unknown", 33344, 80, time(NULL), 10,0, "etho0", "etho1", 0, "google.com"));
        tester->insertToDb(tester->create("192.168.1.1", "224.12.33.4", "unknown", 33344, 80, time(NULL), 11,3, "etho0", "etho1", 0, "google.com"));
        tester->insertToDb(tester->create("192.168.1.2", "224.12.33.2", "unknown", 33344, 80, time(NULL), 5,0, "etho0", "etho1", 0, "yahoo.com"));
        tester->insertToDb(tester->create("192.168.1.2", "224.12.33.1", "unknown", 33344, 80, time(NULL), 2,0, "etho0", "etho1", 0, "hotmail.com"));

        JSONObject a;
	std::string t;
	Time oTime = Time(time(NULL));
	t = oTime.format("%Y-%m-%d");
	a.put(L"limit", new JSONValue((double) 10));
        a.put(L"startTime", new JSONValue((string) t));
        a.put(L"endTime", new JSONValue((string) t));
        a.put(L"metric", new JSONValue((string) "bytes"));
	a.put(L"filter_sourceIp", new JSONValue((string) "192.168.1.1"));

	JSONObject request = tester->handler->process("analytics/stats/bandwidth/query/web", a);
        JSONArray rarr = request[L"items"]->AsArray();
        
	JSONObject real = rarr[0]->AsObject();
        EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 21);
        EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 3);
        EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 0);
        EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"httpHost"]->String().compare("google.com") == 0);

	EXPECT_TRUE(rarr.size() == 1);
}

TEST(RequestHandler, web_basic_filteringonuser){
	//analytics/stats/bandwidth/query/web
	Tester* tester = new Tester();
        tester->insertToDb(tester->create("192.168.1.1", "224.12.33.4", "michael", 33344, 80, time(NULL), 10,0, "etho0", "etho1", 0, "google.com"));
        tester->insertToDb(tester->create("192.168.1.1", "224.12.33.4", "michael", 33344, 80, time(NULL), 11,3, "etho0", "etho1", 0, "google.com"));
        tester->insertToDb(tester->create("192.168.1.2", "224.12.33.2", "john", 33344, 80, time(NULL), 5,0, "etho0", "etho1", 0, "yahoo.com"));
        tester->insertToDb(tester->create("192.168.1.2", "224.12.33.1", "bill", 33344, 80, time(NULL), 2,0, "etho0", "etho1", 0, "hotmail.com"));

        JSONObject a;
	std::string t;
	Time oTime = Time(0);
	t = oTime.format("%Y-%m-%d");
	Time eTime = Time(time(NULL));
	a.put(L"limit", new JSONValue((double) 10));
        a.put(L"startTime", new JSONValue((string) t));
        a.put(L"endTime", new JSONValue((string) eTime.format("%Y-%m-%d")));
        a.put(L"metric", new JSONValue((string) "bytes"));
	a.put(L"filter_user", new JSONValue((string) "michael"));

	JSONObject request = tester->handler->process("analytics/stats/bandwidth/query/web", a);
        JSONArray rarr = request[L"items"]->AsArray();
        
	JSONObject real = rarr[0]->AsObject();
        EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 21);
        EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 3);
        EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 0);
        EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"httpHost"]->String().compare("google.com") == 0);

	EXPECT_TRUE(rarr.size() == 1);
}

TEST(RequestHandler, web_address_reduction){
	//analytics/stats/bandwidth/query/web/address
        Tester* tester = new Tester();
        tester->insertToDb(tester->create("10.1.1.1", "224.12.33.4", "michael", 33344, 80, time(NULL), 10,0, "etho0", "etho1", 0, "blah.google.com"));
        tester->insertToDb(tester->create("10.1.1.1", "224.12.33.4", "michael", 33344, 80, time(NULL), 10,0, "etho0", "etho1", 0, "blah.horse.google.com"));
        tester->insertToDb(tester->create("10.1.1.1", "224.12.33.4", "michael", 33344, 80, time(NULL), 10,0, "etho0", "etho1", 0, "boobs.com"));

        JSONObject a;
        std::string t;
        Time oTime = Time(time(NULL));
        t = oTime.format("%Y-%m-%d");
        a.put(L"limit", new JSONValue((double) 10));
        a.put(L"startTime", new JSONValue((string) t));
        a.put(L"endTime", new JSONValue((string) t));
        a.put(L"metric", new JSONValue((string) "bytes"));
        a.put(L"httpHost", new JSONValue((string) "google.com"));

        JSONObject request = tester->handler->process("analytics/stats/bandwidth/query/web/address", a);
        JSONArray rarr = request[L"items"]->AsArray();
        EXPECT_TRUE(rarr.size() == 1);

        JSONObject real1 = rarr[0]->AsObject();
        EXPECT_TRUE(real1[L"address"]->String().compare("10.1.1.1") == 0);
        EXPECT_TRUE(real1[L"hw"]->String().compare("10.1.1.1-macaddress") == 0);
        EXPECT_TRUE(real1[L"tcpUp"]->AsNumber() == 20);
        EXPECT_TRUE(real1[L"tcpDown"]->AsNumber() == 0);
        EXPECT_TRUE(real1[L"udpUp"]->AsNumber() == 0);
        EXPECT_TRUE(real1[L"udpDown"]->AsNumber() == 0);
}

TEST(RequestHandler, web_address){
	//analytics/stats/bandwidth/query/web/address
        Tester* tester = new Tester();
        tester->insertToDb(tester->create("192.168.1.1", "224.12.33.4", "michael", 33344, 80, time(NULL), 11,0, "etho0", "etho1", 0, "google.com"));
        tester->insertToDb(tester->create("10.1.1.1", "224.12.33.4", "michael", 33344, 80, time(NULL), 10,0, "etho0", "etho1", 0, "google.com"));
        tester->insertToDb(tester->create("10.1.1.1", "224.12.33.4", "michael", 33344, 80, time(NULL), 10,0, "etho0", "etho1", 0, "yahoo.com"));
        tester->insertToDb(tester->create("10.1.1.1", "224.12.33.4", "michael", 33344, 80, time(NULL), 10,0, "etho0", "etho1", 0, "boobs.com"));

        JSONObject a;
        std::string t;
        Time oTime = Time(time(NULL));
        t = oTime.format("%Y-%m-%d");
        a.put(L"limit", new JSONValue((double) 10));
        a.put(L"startTime", new JSONValue((string) t));
        a.put(L"endTime", new JSONValue((string) t));
        a.put(L"metric", new JSONValue((string) "bytes"));
        a.put(L"httpHost", new JSONValue((string) "google.com"));

        JSONObject request = tester->handler->process("analytics/stats/bandwidth/query/web/address", a);
        JSONArray rarr = request[L"items"]->AsArray();
        EXPECT_TRUE(rarr.size() == 2);

        JSONObject real = rarr[0]->AsObject();
        EXPECT_TRUE(real[L"address"]->String().compare("192.168.1.1") == 0);
        EXPECT_TRUE(real[L"hw"]->String().compare("192.168.1.1-macaddress") == 0);
        EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 11);
        EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 0);
        EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 0);
        EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);

        JSONObject real1 = rarr[1]->AsObject();
        EXPECT_TRUE(real1[L"address"]->String().compare("10.1.1.1") == 0);
        EXPECT_TRUE(real1[L"hw"]->String().compare("10.1.1.1-macaddress") == 0);
        EXPECT_TRUE(real1[L"tcpUp"]->AsNumber() == 10);
        EXPECT_TRUE(real1[L"tcpDown"]->AsNumber() == 0);
        EXPECT_TRUE(real1[L"udpUp"]->AsNumber() == 0);
        EXPECT_TRUE(real1[L"udpDown"]->AsNumber() == 0);
}

TEST(RequestHandler, web_address_largeperiod){
	//analytics/stats/bandwidth/query/web/address
        Tester* tester = new Tester();
        tester->insertToDb(tester->create("192.168.1.1", "224.12.33.4", "michael", 33344, 80, time(NULL), 11,0, "etho0", "etho1", 0, "google.com"));
        tester->insertToDb(tester->create("10.1.1.1", "224.12.33.4", "michael", 33344, 80, time(NULL), 10,0, "etho0", "etho1", 0, "google.com"));
        tester->insertToDb(tester->create("10.1.1.1", "224.12.33.4", "michael", 33344, 80, time(NULL), 10,0, "etho0", "etho1", 0, "yahoo.com"));
        tester->insertToDb(tester->create("10.1.1.1", "224.12.33.4", "michael", 33344, 80, time(NULL), 10,0, "etho0", "etho1", 0, "boobs.com"));

        JSONObject a;
        std::string t;
        Time oTime = Time(0);
	Time eTime = Time(time(NULL));
        a.put(L"limit", new JSONValue((double) 10));
        a.put(L"startTime", new JSONValue((string) oTime.format("%Y-%m-%d")));
        a.put(L"endTime", new JSONValue((string) eTime.format("%Y-%m-%d")));
        a.put(L"metric", new JSONValue((string) "bytes"));
        a.put(L"httpHost", new JSONValue((string) "google.com"));

        JSONObject request = tester->handler->process("analytics/stats/bandwidth/query/web/address", a);
        JSONArray rarr = request[L"items"]->AsArray();
        EXPECT_TRUE(rarr.size() == 2);

        JSONObject real = rarr[0]->AsObject();
        EXPECT_TRUE(real[L"address"]->String().compare("192.168.1.1") == 0);
        EXPECT_TRUE(real[L"hw"]->String().compare("192.168.1.1-macaddress") == 0);
        EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 11);
        EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 0);
        EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 0);
        EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);

        JSONObject real1 = rarr[1]->AsObject();
        EXPECT_TRUE(real1[L"address"]->String().compare("10.1.1.1") == 0);
        EXPECT_TRUE(real1[L"hw"]->String().compare("10.1.1.1-macaddress") == 0);
        EXPECT_TRUE(real1[L"tcpUp"]->AsNumber() == 10);
        EXPECT_TRUE(real1[L"tcpDown"]->AsNumber() == 0);
        EXPECT_TRUE(real1[L"udpUp"]->AsNumber() == 0);
        EXPECT_TRUE(real1[L"udpDown"]->AsNumber() == 0);
}

TEST(RequestHandler, address) {
	Tester* tester = new Tester();
	tester->insertToDb(tester->create("10.1.1.2", "192.168.2.2", "unknown", 33344, 80, time(NULL), 1024,5, "eth1", "eth0", 1, ""));

	std::string t;
	Time oTime = Time(time(NULL));
	t = oTime.format("%Y-%m-%d");

	JSONObject a;
	a.put(L"startTime", new JSONValue((string) t));
	a.put(L"endTime", new JSONValue((string) t));
	a.put(L"metric", new JSONValue((string) "bytes"));
	a.put(L"filter_sourceIp", new JSONValue((string) "10.1.1.2"));

	JSONObject r2 = tester->handler->process("analytics/stats/bandwidth/query/transfer", a);
	JSONArray rarr = r2[L"items"]->AsArray();
	JSONObject real = rarr[0]->AsObject();

	EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 1024);
	EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 5);
}

TEST(RequestHandler, addres_generals) {
	Tester* tester = new Tester();
	tester->insertToDb(tester->create("10.1.1.2", "244.12.33.4", "unknown", 33344, 80, time(NULL), 1024,0, "eth1", "eth0", 1, ""));
	std::string t;
	Time oTime = Time(time(NULL));
	t = oTime.format("%Y-%m-%d");

	JSONObject a;
	a.put(L"startTime", new JSONValue((string) t));
	a.put(L"endTime", new JSONValue((string) t));
	a.put(L"metric", new JSONValue((string) "bytes"));
	a.put(L"filter_sourceIp", new JSONValue((string) "10.1.1.2"));

	JSONObject r2 = tester->handler->process("analytics/stats/bandwidth/query/transfer", a);
	JSONArray rarr = r2[L"items"]->AsArray();
	JSONObject real = rarr[0]->AsObject();

	EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 1024);
	EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);
}

TEST(RequestHandler, address_large) {
	Tester* tester = new Tester();
	for(int x= 0; x < 100; x++){
		tester->insertToDb(tester->create("10.1.1.2", "244.12.33.4", "unknown", 33344, 80, time(NULL), 536870912, 0, "eth1", "eth0", 1, ""));
	}

	std::string t;
	Time oTime = Time(time(NULL));
	t = oTime.format("%Y-%m-%d");

	JSONObject a;
	a.put(L"startTime", new JSONValue((string) t));
	a.put(L"endTime", new JSONValue((string) t));
	a.put(L"metric", new JSONValue((string) "mbytes"));
	a.put(L"filter_sourceIp", new JSONValue((string) "10.1.1.2"));

	JSONObject r2 = tester->handler->process("analytics/stats/bandwidth/query/transfer", a);
	JSONArray rarr = r2[L"items"]->AsArray();
	JSONObject real = rarr[0]->AsObject();

	EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 51200);
	EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);
}


TEST(RequestHandler, address_largeTimeRange) {
	Tester* tester = new Tester();
	tester->insertToDb(tester->create("10.1.1.2", "244.12.33.4", "unknown", 33344, 80, time(NULL), 1024, 0,"eth1", "eth0", 1, ""));
	std::string t;
	Time oTime = Time(time(NULL));
	t = oTime.format("%Y-%m-%d");

	JSONObject a;
        a.put(L"filter_sourceIp", new JSONValue((string) "10.1.1.2"));
	a.put(L"startTime", new JSONValue((string) (new Time(0))->format("%Y-%m-%d")));
	a.put(L"endTime", new JSONValue((string) (new Time(time(NULL)))->format("%Y-%m-%d")));
	a.put(L"metric", new JSONValue((string) "bytes"));
	JSONObject r2 = tester->handler->process("analytics/stats/bandwidth/query/transfer", a);
	JSONArray rarr = r2[L"items"]->AsArray();
	JSONObject real = rarr[0]->AsObject();

	EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 1024);
	EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);
}


TEST(RequestHandler, users) {
	Tester* tester = new Tester();
	tester->insertToDb(tester->create("10.1.1.2", "224.12.33.4", "admin", IP4Addr::CLIENT_PORT_MIN + 10, 80, time(NULL), 1024,1023, "eth0", "eth1", 0, ""));

	std::string t;
	Time oTime = Time(time(NULL));
	t = oTime.format("%Y-%m-%d");

	JSONObject a;
	a.put(L"filter_user", new JSONValue((string) "admin"));
	a.put(L"startTime", new JSONValue((string) t));
	a.put(L"endTime", new JSONValue((string) t));
	a.put(L"metric", new JSONValue((string) "bytes"));
	JSONObject r2 = tester->handler->process("analytics/stats/bandwidth/query/transfer", a);
	JSONArray rarr = r2[L"items"]->AsArray();
	JSONObject real = rarr[0]->AsObject();

	EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 1024);
	EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 1023);
	EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);
}

TEST(RequestHandler, users_top) {
        Tester* tester = new Tester();
        tester->insertToDb(tester->create("10.1.1.2", "224.12.33.4", "admin", 40000, 80, time(NULL), 1024,1023, "eth0", "eth1", 0, ""));

        std::string t;
        Time oTime = Time(time(NULL));
        t = oTime.format("%Y-%m-%d");

        JSONObject a;
        a.put(L"startDate", new JSONValue((string) t));
        a.put(L"endDate", new JSONValue((string) t));
        a.put(L"metric", new JSONValue((string) "bytes"));
        a.put(L"limit", new JSONValue((double) 10));
        JSONObject r2 = tester->handler->process("analytics/stats/bandwidth/query/top/users", a);
        JSONArray rarr = r2[L"users"]->AsArray();
        JSONObject real = rarr[0]->AsObject();

        EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 1024);
        EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 1023);
        EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 0);
        EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);
}

TEST(RequestHandler, port_user) {
	Tester* tester = new Tester();
	tester->insertToDb(tester->create("10.1.1.1", "224.12.33.4", "admin", 40000, 23, time(NULL), 1024,0, "eth0", "eth1", 0, ""));
	tester->insertToDb(tester->create("10.1.1.1", "223.122.13.1", "admin", 40000, 80, time(NULL), 1029,0, "eth1", "eth0", 0, ""));
	tester->insertToDb(tester->create("10.1.1.4", "223.122.13.1", "michael", 40000, 80, time(NULL), 1029,0, "eth1", "eth0", 0, ""));

	std::string t;
	Time oTime = Time(time(NULL));
	t = oTime.format("%Y-%m-%d");

	JSONObject a;
	a.put(L"filter_user", new JSONValue((string) "admin"));
	a.put(L"startTime", new JSONValue((string) t));
	a.put(L"endTime", new JSONValue((string) t));
	a.put(L"metric", new JSONValue((string) "bytes"));
	JSONObject r2 = tester->handler->process("analytics/stats/bandwidth/query/port", a);
	JSONArray rarr = r2[L"items"]->AsArray();
	
	JSONObject real = rarr[0]->AsObject();
	EXPECT_TRUE(real[L"port"]->AsNumber() == 23);
	EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 1024);
	EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);

	JSONObject real1 = rarr[1]->AsObject();

	EXPECT_TRUE(real1[L"port"]->AsNumber() == 80);
	EXPECT_TRUE(real1[L"tcpUp"]->AsNumber() == 1029);
	EXPECT_TRUE(real1[L"tcpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real1[L"udpUp"]->AsNumber() == 0);
	EXPECT_TRUE(real1[L"udpDown"]->AsNumber() == 0);
}

TEST(RequestHandler, port_address) {
	Tester* tester = new Tester();
	tester->insertToDb(tester->create("10.1.1.1", "224.12.33.4", "admin", 42311, 23, time(NULL), 1024,0, "eth0", "eth1", 0, ""));
	tester->insertToDb(tester->create("10.1.1.1", "223.122.13.1", "admin", 42311, 80, time(NULL), 1029,0, "eth1", "eth0", 0, ""));
	tester->insertToDb(tester->create("10.1.1.4", "223.122.13.1", "admin", 43422, 80, time(NULL), 1029,0, "eth1", "eth0", 0, ""));

	std::string t;
	Time oTime = Time(time(NULL));
	t = oTime.format("%Y-%m-%d");

	JSONObject a;
	a.put(L"filter_sourceIp", new JSONValue((string) "10.1.1.1"));
	a.put(L"startTime", new JSONValue((string) t));
	a.put(L"endTime", new JSONValue((string) t));
	a.put(L"metric", new JSONValue((string) "bytes"));
	JSONObject r2 = tester->handler->process("analytics/stats/bandwidth/query/port", a);
	JSONArray rarr = r2[L"items"]->AsArray();
	
	JSONObject real = rarr[0]->AsObject();
	EXPECT_TRUE(real[L"port"]->AsNumber() == 23);
	EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 1024);
	EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);

	JSONObject real1 = rarr[1]->AsObject();

	EXPECT_TRUE(real1[L"port"]->AsNumber() == 80);
	EXPECT_TRUE(real1[L"tcpUp"]->AsNumber() == 1029);
	EXPECT_TRUE(real1[L"tcpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real1[L"udpUp"]->AsNumber() == 0);
	EXPECT_TRUE(real1[L"udpDown"]->AsNumber() == 0);
}

TEST(RequestHandler, port_address_sum) {
	Tester* tester = new Tester();
	tester->insertToDb(tester->create("10.1.1.1", "224.12.33.4", "admin", 42311, 23, time(NULL), 1024,0, "eth0", "eth1", 0, ""));
	tester->insertToDb(tester->create("10.1.1.1", "223.122.13.1", "admin", 42311, 80, time(NULL) - 70 * 60, 1,0, "eth1", "eth0", 0, ""));
	tester->insertToDb(tester->create("10.1.1.1", "223.122.13.1", "admin", 43422, 80, time(NULL), 1,0, "eth1", "eth0", 0, ""));

	std::string t;
	Time oTime = Time(time(NULL));
	t = oTime.format("%Y-%m-%d");

	JSONObject a;
	a.put(L"filter_sourceIp", new JSONValue((string) "10.1.1.1"));
	a.put(L"startTime", new JSONValue((string) t));
	a.put(L"endTime", new JSONValue((string) t));
	a.put(L"metric", new JSONValue((string) "bytes"));
	JSONObject r2 = tester->handler->process("analytics/stats/bandwidth/query/port/sum", a);
	JSONArray rarr = r2[L"items"]->AsArray();

	EXPECT_TRUE(rarr.size() == 2);
	
	JSONObject real = rarr[0]->AsObject();
	EXPECT_TRUE(real[L"port"]->AsNumber() == 23);
	EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 1024);
	EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);

	JSONObject real1 = rarr[1]->AsObject();

	cout << "was:" << real1[L"tcpUp"]->AsNumber() << " expected 2\n";

	EXPECT_TRUE(real1[L"port"]->AsNumber() == 80);
	EXPECT_TRUE(real1[L"tcpUp"]->AsNumber() == 2);
	EXPECT_TRUE(real1[L"tcpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real1[L"udpUp"]->AsNumber() == 0);
	EXPECT_TRUE(real1[L"udpDown"]->AsNumber() == 0);
}

TEST(RequestHandler, port_address_largeRange) {
	Tester* tester = new Tester();
	tester->insertToDb(tester->create("10.1.1.1", "224.12.33.4", "admin", 42311, 23, time(NULL), 1024,0, "eth0", "eth1", 0, ""));
	tester->insertToDb(tester->create("10.1.1.1", "223.122.13.1", "admin", 42311, 80, time(NULL), 1029,0, "eth1", "eth0", 0, ""));
	tester->insertToDb(tester->create("10.1.1.4", "223.122.13.1", "admin", 43422, 80, time(NULL), 1029,0, "eth1", "eth0", 0, ""));

	JSONObject a;
	a.put(L"filter_sourceIp", new JSONValue((string) "10.1.1.1"));
	a.put(L"startTime", new JSONValue((string) (new Time(0))->format("%Y-%m-%d")));
	a.put(L"endTime", new JSONValue((string) (new Time(time(NULL)))->format("%Y-%m-%d")));
	a.put(L"metric", new JSONValue((string) "bytes"));
	JSONObject r2 = tester->handler->process("analytics/stats/bandwidth/query/port", a);
	JSONArray rarr = r2[L"items"]->AsArray();
	
	JSONObject real = rarr[0]->AsObject();
	EXPECT_TRUE(real[L"port"]->AsNumber() == 23);
	EXPECT_TRUE(real[L"tcpUp"]->AsNumber() == 1024);
	EXPECT_TRUE(real[L"tcpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"udpUp"]->AsNumber() == 0);
	EXPECT_TRUE(real[L"udpDown"]->AsNumber() == 0);

	JSONObject real1 = rarr[1]->AsObject();

	EXPECT_TRUE(real1[L"port"]->AsNumber() == 80);
	EXPECT_TRUE(real1[L"tcpUp"]->AsNumber() == 1029);
	EXPECT_TRUE(real1[L"tcpDown"]->AsNumber() == 0);
	EXPECT_TRUE(real1[L"udpUp"]->AsNumber() == 0);
	EXPECT_TRUE(real1[L"udpDown"]->AsNumber() == 0);
}

TEST(RequestHandler, device) {
	Tester* tester = new Tester();
	tester->insertToDb(tester->create("10.1.1.2", "224.12.33.4", "admin", IP4Addr::CLIENT_PORT_MIN + 10, 80, time(NULL), 1024,0, "eth0", "eth1", 0, ""));
	
	std::string t;
	Time oTime = Time(time(NULL));
	t = oTime.format("%Y-%m-%d");

	JSONObject a;
	a.put(L"device", new JSONValue((string) "eth0"));
	a.put(L"startTime", new JSONValue((string) t));
	a.put(L"endTime", new JSONValue((string) t));
	a.put(L"metric", new JSONValue((string) "bytes"));

	JSONObject result = tester->handler->process("analytics/stats/bandwidth/query/device", a);
	JSONArray rarr = result[L"items"]->AsArray();

	EXPECT_TRUE(rarr.size() == 1);
	JSONObject r1 = rarr[0]->AsObject();

	EXPECT_TRUE(r1[L"tcpUp"]->AsNumber() == 0);
	EXPECT_TRUE(r1[L"tcpDown"]->AsNumber() == 1024);
	EXPECT_TRUE(r1[L"udpUp"]->AsNumber() == 0);
	EXPECT_TRUE(r1[L"udpDown"]->AsNumber() == 0);
}

TEST(RequestHandler, metrics_insert) {
	Tester* tester = new Tester();

	JSONObject obj;
	JSONArray arr;
	JSONObject i;
	i.put(L"key", new JSONValue((string) "testing.metric"));
	i.put(L"value", new JSONValue((double) 100));

	arr.push_back(new JSONValue(i));
	obj.put(L"items", new JSONValue(arr));
	tester->handler->process("stats/metrics/insert", obj);

	JSONObject obj2;
	obj2.put(L"key", new JSONValue((string) "testing.metric"));
	obj2.put(L"startDate", new JSONValue((string)"1970-01-01"));
	obj2.put(L"endDate", new JSONValue((string)"2020-01-01"));

	JSONObject response = tester->handler->process("analytics/stats/metrics/get", obj2);
	JSONArray responseItems = response[L"items"]->AsArray();
	EXPECT_TRUE(responseItems.size() == 1);

	JSONObject r1 = responseItems[0]->AsObject();
	EXPECT_TRUE(r1[L"value"]->AsNumber() == 100);
}

TEST(RequestHandler, metrics_hourly_get) {
	Tester* tester = new Tester();	
	tester->bwDb->getMonitoringDb()->addMetric("testing.one", 10);
	sleep(2);
	tester->bwDb->getMonitoringDb()->addMetric("testing.one", 20);
	tester->bwDb->getMonitoringDb()->updateMetricsHour("");

	JSONObject args;
	args.put(L"key", new JSONValue((string) "testing.one"));
	args.put(L"startDate", new JSONValue((string)"1970-01-01"));
	args.put(L"endDate", new JSONValue((string)"2020-01-01"));
	JSONObject response = tester->handler->process("analytics/stats/metrics/get", args);
	JSONArray responseItems = response[L"items"]->AsArray();

	EXPECT_TRUE(responseItems.size() == 1);
	JSONObject r1 = responseItems[0]->AsObject();
	EXPECT_TRUE(r1[L"value"]->AsNumber() == 15);
}

