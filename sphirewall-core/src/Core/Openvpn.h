/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPENVPN_H
#define OPENVPN_H

#include <list>
#include "Core/Process.h"

class ConfigurationManager;
class OpenvpnClientConf {
	public:
		std::string file;
		std::string name;
};

class Openvpn : public Process {
	public:
		Openvpn(){
			active = false;
			remoteport = 1194;
		}		

		void installbase();
		void uninstallbase();

		void initalise();
                void destroy();

		void start();
		void stop();
		void publish();
		bool status();

		OpenvpnClientConf* createClient(std::string name);
		void deleteClient(OpenvpnClientConf*);
		std::list<OpenvpnClientConf*>& getClients();

		std::string generateUserFile(std::string name);			
		std::string log();

		bool isStatic(){
			return this->staticAuthentication;
		}

		//Configuration Values:
		std::string name;
		std::string serverip;
		std::string clientip;
		std::string servermask;
		std::string remoteserverip;
		std::string customopts;
		int remoteport;
		bool compression;		

		//Certification Details
		std::string certcountry;
                std::string certprovince;
                std::string certcity;
                std::string certorg;
                std::string certemail;
		
		//Other
		bool staticAuthentication;	
		bool active;
	private:
		std::list<OpenvpnClientConf*> clients;
		std::string getPath();
		std::string getPidFile();
		std::string getLogFile();
		
};

class OpenvpnManager : public Process {
	public:
		void initalise();
		void destroy();

		void save();

		void start();
		void stop();
		
		Openvpn* getInstance(std::string name);
		void addInstance(Openvpn* instance);	
		void removeInstance(Openvpn* instance);
        
	        void setConfigurationManager(ConfigurationManager* c ){
                        this->config = c;
                }

		std::list<Openvpn*>& listInstances(){
			return instances;
		}

		bool daemonExists();
	private:
		std::list<Openvpn*> instances;
                void load();
		ConfigurationManager* config;
};

#endif
