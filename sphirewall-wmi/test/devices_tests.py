#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester

class NetDevicesTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login("fw", "admin", "admin")

    def gotoDevices(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Networking')]").click()

    def test_DisplayedCorrectly(self):
        self.gotoDevices()
        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td/a[contains(text(), 'lo')]//ancestor::tr/td[contains(text(), '127.0.0.1')]//ancestor::tr/td[contains(text(), '255.0.0.0')]//ancestor::tr/td[contains(text(), 'UP')]"))

    def test_SavingInterface(self):
        self.gotoDevices()
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'eth1')]").click()

        self.tester.driver.find_element_by_id("name").clear()
        self.tester.driver.find_element_by_id("name").send_keys("int")

        self.tester.driver.find_element_by_id("ip").clear()
        self.tester.driver.find_element_by_id("ip").send_keys("10.1.1.10")

        self.tester.driver.find_element_by_id("mask").clear()
        self.tester.driver.find_element_by_id("mask").send_keys("255.255.255.0")

        self.tester.driver.find_element_by_id("gateway").clear()
        self.tester.driver.find_element_by_id("gateway").send_keys("10.1.1.1")

        self.tester.driver.find_element_by_id("submit-main").click()
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'eth1')]").click()

        self.assertEqual(self.tester.driver.find_element_by_id("ip").get_attribute("value"), "10.1.1.10")
        self.assertEquals(self.tester.driver.find_element_by_id("mask").get_attribute("value"), "255.255.255.0")
        self.assertEquals(self.tester.driver.find_element_by_id("name").get_attribute("value"), "int")
        self.assertEquals(self.tester.driver.find_element_by_id("gateway").get_attribute("value"), "10.1.1.1")

    def test_SavingInterfaceDhcp(self):
        self.gotoDevices()
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'eth1')]").click()

        self.tester.driver.find_element_by_id("dhcp").click()
        self.tester.driver.find_element_by_id("submit-main").click()
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'eth1')]").click()

        self.assertEquals(self.tester.driver.find_element_by_id("dhcp").get_attribute("value"), "on")

    def test_SavingAliases(self):
        self.gotoDevices()
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'eth1')]").click()

        self.tester.driver.find_element_by_id("aliases").clear()
        self.tester.driver.find_element_by_id("aliases").send_keys("10.1.1.1 10.1.1.10/255.255.255.255")

        self.tester.driver.find_element_by_id("submit-main").click()
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'eth1')]").click()

        self.assertEquals(self.tester.driver.find_element_by_id("aliases").get_attribute("value"), "10.1.1.1 10.1.1.10/255.255.255.255 ")

    def test_SettingAsBridge(self):
        self.gotoDevices()
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'eth1')]").click()

        self.tester.driver.find_element_by_id("bridge").click()

        self.tester.driver.find_element_by_id("submit-main").click()
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'eth1')]").click()

        self.assertEquals(self.tester.driver.find_element_by_id("bridge").get_attribute("value"), "on")

    def test_BringDownUp(self):
        self.gotoDevices()
        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td/a[contains(text(), 'lo')]//ancestor::tr/td[contains(text(), '127.0.0.1')]//ancestor::tr/td[contains(text(), '255.0.0.0')]//ancestor::tr/td[contains(text(), 'UP')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'lo')]//ancestor::tr/td/a[contains(text(), 'Bring Down')]").click()
        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td/a[contains(text(), 'lo')]//ancestor::tr/td[contains(text(), '127.0.0.1')]//ancestor::tr/td[contains(text(), '255.0.0.0')]//ancestor::tr/td[contains(text(), 'DOWN')]"))

        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'lo')]//ancestor::tr/td/a[contains(text(), 'Bring Up')]").click()
        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td/a[contains(text(), 'lo')]//ancestor::tr/td[contains(text(), '127.0.0.1')]//ancestor::tr/td[contains(text(), '255.0.0.0')]//ancestor::tr/td[contains(text(), 'UP')]"))

    def tearDown(self):
        self.tester.disconnect()
