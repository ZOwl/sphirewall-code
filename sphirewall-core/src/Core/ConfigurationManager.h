/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONFIGURATION_MANAGER_H
#define CONFIGURATION_MANAGER_H

#include "Json/JSON.h"
#include "Json/JSONValue.h"

#include <vector>
#include <map>
#include <sstream>
class Lock;

enum ObjectContainerType {
	CREL, CARRAY
};

class ObjectWrapper;
class ObjectContainer;
class Serialiser {
	public:
		virtual ~Serialiser() {}
		virtual std::string serialize(ObjectWrapper* input) = 0;
		virtual ObjectWrapper* parse(std::string input) = 0;
};

class JsonSerialiser : public virtual Serialiser {
	public:
		~JsonSerialiser(){
		}

		JsonSerialiser(){
			c = -1;
			format = false;
		}

		static ObjectWrapper* parseJsonString(std::string input);
		static std::string serializeToJsonString(ObjectWrapper* input, bool format = false);

                std::string serialize(ObjectWrapper* input);
                ObjectWrapper* parse(std::string input);
		
		void setFormat(bool format){
			this->format = format;
		}
	private:
		void recurseObject(std::stringstream& root, ObjectContainer* input);
		ObjectWrapper* parseRecurse(JSONValue* value);
		
		bool format;
		int c;
};

class ObjectContainer {
	public:
		ObjectContainer(ObjectContainerType type) :
			type(type){
			}

		~ObjectContainer();

		void put(std::string key, ObjectWrapper* input);
		void put(ObjectWrapper* input);

		ObjectWrapper* get(std::string key);
		ObjectWrapper* get(int index);

		bool has(std::string key);
		int size();

		ObjectContainerType getType() const {
			return this->type;
		}

		std::map<std::string, ObjectWrapper*>& getKeyObjects(){
			return keyObjects;
		}
	private:
		ObjectContainerType type;	

		std::map<std::string, ObjectWrapper*> keyObjects;
		std::vector<ObjectWrapper*>  listObjects;

};

enum ObjectWrapperType {
	OBJECT, STRING, DOUBLE, BOOL
};

class ObjectWrapper {
	public:

		~ObjectWrapper();
		ObjectWrapper(ObjectContainer* container){
			this->type = OBJECT;
			this->containerValue = container;	
		}

		ObjectWrapper(std::string value) {
			this->type = STRING;
			this->stringValue = value;
			this->containerValue = NULL;
		}

		ObjectWrapper(double value) {
			this->type = DOUBLE;
			this->numberValue = value;
			this->containerValue = NULL;
		}

		ObjectWrapper(bool value) {
			this->type = BOOL;
			this->boolValue = value;
			this->containerValue = NULL;
		}

		std::string string() {
			return this->stringValue;
		}

		ObjectContainer* container(){
			return this->containerValue;
		}

		double number() const {
			return this->numberValue;
		}

		bool boolean() const {
			return this->boolValue;
		}

		bool isString() const {
			return this->type == STRING;
		}

		bool isNumber() const {
			return this->type == DOUBLE;
		}

		bool isBoolean() const {
			return this->type == BOOL;
		}

		bool isContainer() const {
			return this->type == OBJECT;
		}	

		std::string toString() const {
			std::stringstream ss;
			ss << " ObjectWrapper [";
			if(isContainer()){
				ss << " container ";
			}else if(isNumber()){
				ss << " " << numberValue << " ";
			}else if(isBoolean()){
				ss << " " << boolValue << " ";
			}else if(isString()){
				ss << " " << stringValue << " ";
			}

			ss << " ]\n";

			return ss.str();
		}
	private:
		std::string stringValue;
		bool boolValue;
		double numberValue;
		ObjectContainer* containerValue;
		ObjectWrapperType type;
};

class ConfigurationException : public std::exception {
	public:

		ConfigurationException(std::string message) {
			w = message;
		}

		~ConfigurationException() throw () {
		}

		virtual const char* what() const throw () {
			return w.c_str();
		}

		std::string message() {
			return w;
		}
	private:
		std::string w;
};

class ConfigurationManager {
	public:
		ConfigurationManager();		
		~ConfigurationManager();

		int put(std::string path, std::string value);
		int put(std::string path, bool value);
		int put(std::string path, double number);

		ObjectWrapper* get(std::string path);
		bool hasByPath(std::string path);

		std::string dump();
		void save();
		void load(std::string filename);

		bool has(std::string);
		ObjectContainer* getElement(std::string);
		void setElement(std::string key, ObjectContainer* object);
	private:
		int put(std::string path, ObjectWrapper* value);
		ObjectWrapper* rootValue;
		std::string filename;
		Lock* lock;
};

#endif
