/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <signal.h>
   
using namespace std;
using std::string;

#include "Core/System.h"
#include "Core/SignalHandler.h"
#include "Utils/Utils.h"
#include "Core/Cloud.h"

void processRequest(string command, map<char, string> args);
void usage();
int main(int argc, char* argv[])
{	
	string configFile;
	//bool silent = false;
	
	signal(SIGPIPE, sighandler);
	signal(SIGTERM, sighandler);
	signal(SIGHUP, sighandler);

//	signal(SIGABRT, sighandler);
//	signal(SIGSEGV, sighandler);
	signal(SIGINT, sighandler);	

	//System sys;
	int u_id = getuid();

	if(u_id == 0)
	{	
		std::map<char, string> vars;
		string instruction;

		if(argc >= 2){
			instruction = argv[1];

			if(argc >= 3){
				for (int i = 2; i < argc; i++)
				{			
					if(argv[i][0] == '-' && argv[i +1][0] != '-'){
						vars[argv[i][1]] = argv[i +1];
					}else if(argv[i][0] == '-'){
						vars[argv[i][1]] = " ";
					}else if(argv[i][0] == '?'){
						vars['?'] == " ";
					}

				}
			}

			processRequest(instruction, vars);
		}else{
			usage();
		}
	}
	else
	{
		cout << "Please run sphirewall with root " << endl;
	}

	System::sysRelease();

	return 0;
}


void usage_header();
void usage_footer();

void usage(){
	usage_header();
	/*General Usage Information:*/

	cout << "\nInstructions:\n";
	cout << "\tstart - Launch Sphirewall\n";
	cout << "\tstop  - Stop Sphirewall\n";

	cout << "For details on any commands - sphirewalld <instruction> ?\n\n";

	usage_footer();
}

void usage_header(){
	cout << "\nSphirewall Usage: sphirewalld <instruction> <args.....>\n";
}

void usage_footer(){
}

/*End of general usage functions*/

/*Beginning of command usage functions*/
void start_usage(){
	usage_header();

	cout << "\nsphirewalld start ?:\n";
	cout << "\t default          : Run Sphirewall as a process\n";
	cout << "\t -c <config_file> : Specify a configuration file to load\n";
	cout << "\t -m <mode>        : Specify a runtime mode\n";
	cout << "\t\tbind    : Bind sphirewall to the console\n";

	usage_footer();
}

void stop_usage(){
	usage_header();
	cout << "\nsphirewalld stop ?:\n";
	cout << "\t default          : Stop the sphirewall process\n";
	cout << "\t -f               : Stop sphirewall with brute force\n";
	cout << "\t -p <PID>         : Stop sphirewall with the specified process id\n";

	usage_footer(); 
}


/*End of usage functions*/

void start(map<char, string> args){
	if(args.find('?') != args.end()){	
		start_usage();
	}else{
		string configFile;
		runlevel_t level = RUNLEVEL_NORMAL;
		bool doFork = true;

		/*Get the configuration*/
		if(args.find('c') != args.end()){
			configFile = args['c'];
		}else{
			configFile = "/etc/sphirewall.conf";
		}

		/*Get the mode*/
		if(args.find('m') != args.end()){
			string mode = args['m'];
			if(mode.compare("bind") == 0){
				level = RUNLEVEL_NORMAL;
				doFork = false;
			}	
		}


		/*Now we can actually run*/
		if(doFork){
			pid_t pid;
			if((pid = fork()) == -1){
				perror("fork");
			}else{
				if(pid == 0){
					System::getInstance()->init(configFile, level);
				}
			}
		}else{
			System::getInstance()->init(configFile, level);
		}
	}
}

void stop(map<char, string> args){
	if(args.find('?') != args.end()){
		stop_usage();
	}else{
		//Read the pid file:
		ifstream lockfileFp("/var/lock/sphirewall.lock");
		if(!lockfileFp.is_open()){
			cout << "Could not find lockfile, unable to find pid" << endl;
			exit(-1);
		}

		string pidString;
		getline(lockfileFp, pidString);

		pid_t PID = atoi(pidString.c_str()); 

		if(args.find('p') != args.end()){
			PID = atoi(args['p'].c_str());
		}

		if(args.find('f') != args.end()){
			if(PID != -1 && PID != 0){
				cout << "Using extreme force to stop sphirewall running on PID: " << PID << endl;
				kill(PID, SIGKILL);
			}else{
				cout << "I am sorry, but I cannot find a PID to kill, perhaps you can help me ?\n";
			}
		}else{
			if(PID != -1 && PID != 0){
				cout << "Attempting to stop sphirewall running on PID: " << PID << endl;
				kill(PID, SIGTERM);
			}else{
				cout << "I am sorry, but I cannot find a PID to kill, perhaps you can help me ?\n";
			}
		}
	}
}

void processRequest(string command, map<char, string> args){
	/*Split into types of request*/	
	if(command.compare("start") == 0){
		start(args);
	}

	else if(command.compare("stop") == 0){
		stop(args);
	}

	else{
		usage();
	}
}
