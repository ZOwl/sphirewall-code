/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PACKET_H
#define PACKET_H

/*Packet.h*/
#include <netinet/ip.h>

#include "Utils/IP4Addr.h"
#include "sphirewall_queue.h"
#include "Auth/User.h"

class NetDevice;
namespace SFwallCore {
	class Connection;

	enum proto {
		NDEF = -1,
		TCP = 0,
		UDP = 1,
		ICMP = 2,
		IGMP = 3
	};

	enum pDirection {
		n,
		UP,
		DOWN
	};

	enum PacketDirection {
		DIR_SAME = 1,
		DIR_OPPOSITE,
		DIR_ANY
	};

	string stringProtocol(proto p);

	class SNatConnTracker;
	class DNatConnTracker;
	class SNatConn;
	class DNatConn;
	class NatRule;

	class Packet {
	public:

		Packet() {
			fakeData = false;
		}
		Packet(const struct sq_packet* sqp);
		virtual ~Packet();
		static Packet* parsePacket(const struct sq_packet* sqp);
		static struct sq_packet* cloneSqp(struct sq_packet* sqp);
		void setData(std::string d);

		/*Getters*/
		std::string getData();
		std::string getDevice() ;
		virtual std::string toString() const;

		in_addr_t getSrcIp() const;
		in_addr_t getDstIp() const;

		int getLen() const;
		int getHeaderLen() const;
		int getPayloadLen() const;
		int getCheckSum() const;
		int getSourceDev() ;
		int getDestDev() ;

		User* getUser() ;
		std::string getUsername() const;
		std::string getHw() ;

		long hash() const;
		proto getProtocol() const;

		NetDevice* getSourceNetDevice();
		NetDevice* getDestNetDevice(); 
		Connection* getConnection() const {
			return connection;
		}

		void setConnection(Connection* connection) {
			this->connection = connection;
		}

		void setInternalSqp(struct sq_packet* sqp){
			this->sqp = sqp;
		}

		struct sq_packet* getInternalSqp(){
			return sqp;
		}

		int getSrcPort() const;
                int getDstPort() const;
		unsigned short getIcmpId() const;

		void setUser(User* user) {
			this->userPtr = user;
		}

		unsigned char* getCData();
		bool isBroadcast();
		bool isMulticast();

		void setPayloadLen(int len){
			this->payloadlen = len;
		}
	protected:
		bool hasHw;
		u_int8_t hw[6];

		int len;
		int headerlen;
		int payloadlen;

		int check;
		int sport;
		int icmpId;

		//proto protocol;
		User* userPtr;
		std::string data;
		bool fakeData;
		Connection* connection;

		//Root information:
		struct sq_packet* sqp;
	};

	class TcpUdpPacket : public Packet {
		public:

			TcpUdpPacket() {
			}

			TcpUdpPacket(const struct sq_packet* sqp) : Packet(sqp) {
			}

			virtual ~TcpUdpPacket() {
			}
	};

	class TcpPacket : public TcpUdpPacket {
		public:

			TcpPacket() : TcpUdpPacket() {
			}
			TcpPacket(const struct sq_packet* sqp);

			~TcpPacket() {
			}

			std::string echo() const;
			bool finSet() const;
			bool ackSet() const;
			bool synSet() const;
			bool rstSet() const;

			unsigned long getAckSeq() const;
			unsigned long getSeq() const;

			std::string toString() const;
	};

	/*Helper, not part of the hierachy*/
        class HttpsTcpPacket{
                public:
                        HttpsTcpPacket(TcpUdpPacket* packet) :
                                packet(packet){}

                        std::string getHost();
			static bool matchPort(int target){
				return target == 443;
			}

			bool isTls();
                private:
                        TcpUdpPacket* packet;
        };

	class HttpProxyPacket{
		public:
			HttpProxyPacket(TcpUdpPacket* packet) :
				packet(packet){}

			std::string getHost();
			static bool matchPort(int target){
				return target == 8080 || target == 3128 || target == 1080;
			}
		private:
			TcpUdpPacket* packet;
	};

	class HttpTcpPacket {
		public:
			HttpTcpPacket(TcpUdpPacket* packet) : 
				packet(packet){} 

			std::string getHost();
                        static bool matchPort(int target){
                                return target == 80;
                        }
		private:
			TcpUdpPacket* packet;
	};

	class UdpPacket : public TcpUdpPacket {
		public:

			UdpPacket() : TcpUdpPacket() {
			}
			UdpPacket(const struct sq_packet* sqp);

			~UdpPacket() {
			}

			std::string echo() const;

			std::string toString() const;
	};

	class IcmpPacket : public Packet {
		public:
			long hash() const;

			IcmpPacket() : Packet() {
			}
			IcmpPacket(const struct sq_packet* sqp);

			~IcmpPacket() {
			}

			std::string toString() const;
	};

}

#endif
