/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;
using std::string;

#include "Core/Config.h"
#include "Core/System.h"
#include "Utils/Utils.h"
#include "Core/Logger.h"

const string Config::timeSlotName = "ta_timeslot";

void Config::save() {
	configurationManager->save();
}

int Config::loadConfig() {

	if (!configurationManager->has("general")) {
		//Set the raw default values here:
		configurationManager->put("general:admin_group", (double) 0);
		configurationManager->put("general:config_dir", (string) "/etc/sphirewall");
		configurationManager->put("general:gid", (double) 6001);
		configurationManager->put("general:redirect_to_auth", true);
		configurationManager->put("general:session_timeout", (double) 600);
		configurationManager->put("general:bandwidth_hostname", (string) "localhost");
		configurationManager->put("general:bandwidth_port", (double) 8002);
		configurationManager->put("general:ssl_port", (double) 8003);
		configurationManager->put("general:ssl_password", (string) "FenderBass1");
		configurationManager->put("general:snort_enabled", (bool) false);
		configurationManager->put("general:snort_file", (string) "/var/log/snort/alert");
		configurationManager->put("general:bandwidth_threshold", (double) 1500);
		configurationManager->put("general:bandwidth_retension", (double) 60 * 60 * 24 * 5);
		configurationManager->put("general:bandwidth_day_switch_period", (double) 0);
		configurationManager->put("general:hashpasswords", (bool) true);
		
		configurationManager->put("general:remote_user_mode", (double) 0);
		configurationManager->put("general:requires_initial_config", (bool) false);
		configurationManager->put("general:force_login", (bool) false);
		configurationManager->put("general:initial_config_url", (string) "");	
		configurationManager->put("general:rpc", (bool) false);	
		configurationManager->put("general:eventPurgeThreshold", (double) 60 * 60 * 24);	
		
		save();
	}
}

int LoggingConfiguration::getLevel(std::string context) {
	SLogger::LogContext* c = rootLogger->getContext(context);
	if (c) {
		return c->level;
	}

	return -1;
}

int LoggingConfiguration::setLevel(std::string context, int level) {
	SLogger::LogContext* c = rootLogger->getContext(context);
	if (c && level >= 0 &&  level <5){
		c->level = (SLogger::Priority) level;
		save();
		return 0;
	}else{
		return -1;
	}
}

void LoggingConfiguration::save() {
	ObjectContainer* root = new ObjectContainer(CREL);
	ObjectContainer* items = new ObjectContainer(CARRAY);

	list<string> names = rootLogger->getAvailableContexts();
	for (list<string>::iterator iter = names.begin();
			iter != names.end();
			iter++) {

		string cName = (*iter);
		SLogger::LogContext* c = rootLogger->getContext(cName);

		ObjectContainer* o = new ObjectContainer(CREL);
		o->put("context", new ObjectWrapper((string) cName));
		o->put("level", new ObjectWrapper((double) c->level));

		items->put(new ObjectWrapper(o));
	}

	root->put("levels", new ObjectWrapper(items));
	configManager->setElement("logging", root);
	configManager->save();
}

void LoggingConfiguration::load() {
	if (configManager->has("logging")) {
		ObjectContainer* root = configManager->getElement("logging");
		ObjectContainer* v = root->get("levels")->container();
		for (int x = 0; x < v->size(); x++) {
			ObjectContainer* p = v->get(x)->container();
			SLogger::LogContext* c = rootLogger->getContext(p->get("context")->string());
			if(c){
				c->level = (SLogger::Priority) p->get("level")->number();
			}
		}
	}
}

std::list<std::string> LoggingConfiguration::getContexts() {
	return rootLogger->getAvailableContexts();
}

int RuntimeConfiguration::get(std::string key) {
	if (contains(key))
		return *configMap[key];
	return -1;
}

int RuntimeConfiguration::set(std::string key, int newvalue) {
	if (configMap.find(key) != configMap.end()) {
		int* v = configMap[key];
		*v = newvalue;
		if(configurationManager != NULL){
			configurationManager->put("runtime:" + key, (double) *v);
			configurationManager->save();
		}
		return 0;
	}
	return -1;
}

void RuntimeConfiguration::put(std::string key, int* value) {
	configMap[key] = value;
}

void RuntimeConfiguration::loadOrPut(std::string key, int* ptr){
	if(configurationManager != NULL && configurationManager->hasByPath("runtime:" + key)){
		put(key, ptr);
		set(key, configurationManager->get("runtime:" + key)->number());	
	}else{
		put(key, ptr);
		set(key, *ptr);
	}
}

void RuntimeConfiguration::listAll(std::list<string>& l) {
	for (map<string, int*>::iterator iter = configMap.begin();
			iter != configMap.end();
			++iter) {
		l.push_back(iter->first);
	}
}

bool RuntimeConfiguration::contains(std::string key) {
	return configMap.find(key) != configMap.end();
}

