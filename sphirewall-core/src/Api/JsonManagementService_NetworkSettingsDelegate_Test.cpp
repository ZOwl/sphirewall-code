/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <iostream>

using namespace std;

#include "Utils/NetDevice.h"
#include "Api/JsonManagementService.h"

//TEST(NetworkSettingsDelegate, devices_list) {
//	InterfaceManager* manager = new InterfaceManager();
//	manager->load();
//
//	NetworkSettingsDelegate* del = new NetworkSettingsDelegate(manager, NULL, NULL, NULL, NULL);
//	JSONObject ret = del->process("network/devices/list", JSONObject());
//
//	JSONArray arr = ret[L"devices"]->AsArray();
//	EXPECT_TRUE(arr.size() == manager->devices.size());
//}
//
//TEST(NetworkSettingsDelegate, devices_get) {
//	InterfaceManager* manager = new InterfaceManager();
//	manager->load();
//
//	NetworkSettingsDelegate* del = new NetworkSettingsDelegate(manager, NULL, NULL, NULL, NULL);
//	JSONObject pass;
//	pass.put(L"interface", new JSONValue((string) "lo"));
//	JSONObject ret = del->process("network/devices/get", pass);
//
//	EXPECT_TRUE(ret[L"interface"]->String().compare("lo") == 0);
//	EXPECT_TRUE(ret[L"ip"]->String().compare("127.0.0.1") == 0);
//	EXPECT_TRUE(ret[L"mask"]->String().compare("255.0.0.0") == 0);
//}
//
//TEST(NetworkSettingsDelegate, dns_get) {
//	DNSConfig config;
//
//	NetworkSettingsDelegate* del = new NetworkSettingsDelegate(NULL, &config, NULL, NULL, NULL);
//	JSONObject ret = del->process("network/dns/get", JSONObject());
//
//	EXPECT_TRUE(ret[L"ns1"]->String().compare(config.getNS1()) == 0);
//	EXPECT_TRUE(ret[L"ns2"]->String().compare(config.getNS2()) == 0);
//	EXPECT_TRUE(ret[L"domain"]->String().compare(config.getDomain()) == 0);
//	EXPECT_TRUE(ret[L"search"]->String().compare(config.getSearch()) == 0);
//}
//
//TEST(NetworkSettingsDelegate, dns_set) {
//	/*	DNSConfig config;
//
//		NetworkSettingsDelegate* del = new NetworkSettingsDelegate(NULL, &config, NULL, NULL);
//		JSONObject pass;
//		pass.put(L"ns1", new JSONValue((string) "ns1"));
//			pass.put(L"ns2", new JSONValue((string) "ns2"));
//			pass.put(L"domain", new JSONValue((string) "domain"));
//			pass.put(L"search", new JSONValue((string) "search"));
//
//		JSONObject ret = del->process("network/dns/set",pass); 
//
//		EXPECT_TRUE(config.getNS1().compare("ns1") == 0);	
//		EXPECT_TRUE(config.getNS2().compare("ns2") == 0);	
//		EXPECT_TRUE(config.getSearch().compare("search") == 0);	
//		EXPECT_TRUE(config.getDomain().compare("domain") == 0);	
//	 */
//}
//
//TEST(NetworkSettingsDelegate, routes_list) {
//	Route::RouteTable* routeTable = new Route::RouteTable();
//	NetworkSettingsDelegate* del = new NetworkSettingsDelegate(NULL, NULL, routeTable, NULL, NULL);
//
//	Route::Route_Entry r1;
//	r1.setDest("10.1.1.1");
//	r1.setGw("10.1.2.3");
//	Route::Route_Entry r2;
//	r2.setDest("10.1.1.2");
//	r2.setGw("10.1.2.4");
//
//	routeTable->routes.clear();
//	routeTable->routes.push_back(r1);
//	routeTable->routes.push_back(r2);
//
//	JSONObject ret = del->process("network/routes/list", JSONObject());
//	JSONArray items = ret[L"routes"]->AsArray();
//
//	EXPECT_TRUE(items.size() == 2);
//}
//
//TEST(NetworkSettingsDelegate, routes_add) {
//	Route::RouteTable* routeTable = new Route::RouteTable();
//	NetworkSettingsDelegate* del = new NetworkSettingsDelegate(NULL, NULL, routeTable, NULL, NULL);
//
//	routeTable->routes.clear();
//
//	JSONObject pass;
//	pass.put(L"dest", new JSONValue((string) "171.2.2.0"));
//	pass.put(L"gw", new JSONValue((string) "171.2.2.22"));
//	JSONObject ret = del->process("network/routes/add", pass);
//
//	//	EXPECT_TRUE(routeTable->routes.size() == 1);
//}
//
