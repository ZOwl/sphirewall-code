/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <pthread.h>
#include <string>
#include <vector>
#include <queue>

using namespace std;

#include "Database/BandwidthDb.h"
#include "Utils/Utils.h"
#include "Utils/IP4Addr.h"
#include "Database/TrafficAnalyticsDb.h"
#include "Database/MonitoringDb.h"
#include "Database/BaseDb.h"

/*Database constructor - this should only be run once. Sets variables, these can be adjusted to alter performance*/
BandwidthDb::BandwidthDb(std::string f, SLogger::LogContext* l) {
	taDb = new TrafficAnalyticsDb(f + "_ta", l);
	taDb->initDb();

	monDb = new MonitoringDb(f + "_mon", l);
	monDb->initDb();
}

BandwidthDb::~BandwidthDb() {
}

void BandwidthDb::lock() {
	taDb->lock();
	monDb->lock();
}

void BandwidthDb::unlock() {
	taDb->unlock();
	monDb->unlock();
}

