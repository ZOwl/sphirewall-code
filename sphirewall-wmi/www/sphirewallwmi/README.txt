Sphirewall-wmi is the web based management interface for Sphirewall. This is the second iteration of this interface, written in python using the Flask Framework. http://flask.pocoo.org/docs/quickstart/

To get started, you must have flask and python installed
> apt-get install python python-flask

Then you can start the interface
> python main.py

Open it in a browser
> firefox 127.0.0.1 5000
