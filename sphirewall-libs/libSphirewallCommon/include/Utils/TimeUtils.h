/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TIMEUTILS_H
#define TIMEUTILS_H
#include <iostream>

class TimeUtils {
public:
	static const int HOUR_SECS = 3600;
	static const int DAY_SECONDS = 86400;
	static const int WEEK_SECS = 604800;
	static const int MONTH_SECS = 2629743;

	static void convertToString(time_t, const char* format, std::string& _return);
	static int extractHour(time_t);
	static std::string dateNow();
private:

};

#endif
