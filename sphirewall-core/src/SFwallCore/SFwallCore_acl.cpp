/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream> 
#include <vector>
#include <time.h>
#include <sys/types.h>
#include <sstream>
#include <map>
#include <fstream>
#include <string>

using namespace std;

#include "Utils/IP4Addr.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Acl.h"
#include "SFwallCore/Alias.h"
#include "Utils/Utils.h"
#include "Utils/StringUtils.h"
#include "Json//JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Utils/TimeWrapper.h"
#include "Core/System.h"

const string SFwallCore::ACLStore::FIELD_DELIM = ",";
const string SFwallCore::ACLStore::tagFieldDelim = "<delim>";

SFwallCore::ACLStore::ACLStore(SLogger::LogContext* logger,
		UserDb* userDb,
		GroupDb* groupDb,
		SessionDb* sessionDb,
		AliasDb* aliases) : logger(logger),
userDb(userDb),
groupDb(groupDb),
sessionDb(sessionDb),
aliases(aliases) {
	lock = new Lock();
}

SFwallCore::ACLStore::~ACLStore() {
	for (int x = 0; x < entries.size(); x++) {
		delete entries[x];
	}

	for (int x = 0; x < natEntries.size(); x++) {
		delete natEntries[x];
	}
}

SFwallCore::Rule::Rule() {
	source = -1, sourceMask = -1, dest = -1, destMask = -1;
	sourceAlias = NULL;
	destAlias = NULL;
	sourceDevPtr = NULL;
	destDevPtr = NULL;
	groupid = -1;
	type = NDEF;
	count = 0;
	enabled = true;
	valid = true;

	sport_start = -1; sport_end = -1;
	dport_start = -1; dport_end = -1;
	startHour = -1; finishHour = -1;
	ignoreconntrack = false;
}

SFwallCore::PriorityRule* SFwallCore::ACLStore::matchPriorityQos(Packet* packet) {
	bool capturePortal = false;
	for (int x = 0; x < (int) priorityQosRules.size(); x++) {
		bool ruleMatch = true; /*Rule match default to true, so we check to see if any conditions break that*/

		/*Packet should never be null: TODO: Find why its sometimes null and resolve*/
		if (packet == NULL) {
			return NULL;
		}

		if(priorityQosRules[x]->supermatch(packet, capturePortal, sessionDb, groupDb)){
			priorityQosRules[x]->count++;
			return priorityQosRules[x];
		}
	}
	return NULL;
}

SFwallCore::FilterRule* SFwallCore::ACLStore::matchFilterAcls(Packet* packet, bool& capturePortal) {
	for (int x = 0; x < (int) entries.size(); x++) {
		bool ruleMatch = true; /*Rule match default to true, so we check to see if any conditions break that*/

		/*Packet should never be null: TODO: Find why its sometimes null and resolve*/
		if (packet == NULL) {
			return NULL;
		}

		if(entries[x]->supermatch(packet, capturePortal, sessionDb, groupDb)){
			entries[x]->count++;
			return entries[x];
		}
	}
	return NULL;
}


bool SFwallCore::Rule::supermatch(Packet* packet, bool& capturePortal, SessionDb* sessionDb, GroupDb* groupDb){
	bool ruleMatch = true;

	if(sport_start != -1 && sport_end != -1){
		if (packet->getProtocol() == TCP || packet->getProtocol() == UDP) {
			const TcpUdpPacket* tcpudp = (const TcpUdpPacket*) packet;
			if(!(tcpudp->getSrcPort() >= sport_start && tcpudp->getSrcPort() <= sport_end)){
				ruleMatch = false;
			}
		}else {
			ruleMatch = false;
		}
	}

	if(dport_start != -1 && dport_end != -1){
                if (packet->getProtocol() == TCP || packet->getProtocol() == UDP) {
                        const TcpUdpPacket* tcpudp = (const TcpUdpPacket*) packet;
                        if(!(tcpudp->getDstPort() >= dport_start && tcpudp->getDstPort() <= dport_end)){
                                ruleMatch = false;
                        }
                }else{
			ruleMatch = false;
		}
	}

	if(dportList.size() != 0){
		if (packet->getProtocol() == TCP || packet->getProtocol() == UDP) {
			const TcpUdpPacket* tcpudp = (const TcpUdpPacket*) packet;
			if(dportList.find(tcpudp->getDstPort()) == dportList.end()){
				ruleMatch = false;
			}
		}else{
			ruleMatch = false;
		}	
	}

	if(sportList.size() != 0){
		if (packet->getProtocol() == TCP || packet->getProtocol() == UDP) {
			const TcpUdpPacket* tcpudp = (const TcpUdpPacket*) packet;
			if(sportList.find(tcpudp->getSrcPort()) == sportList.end()){
				ruleMatch = false;
			}
		}else{
			ruleMatch = false;
		}
	}

	if (sourceAlias != NULL) {
		if (!sourceAlias->searchForNetworkMatch(packet->getSrcIp())) {
			ruleMatch = false;
		}
	} else {
		if (source != -1) {
			if (IP4Addr::matchNetwork(packet->getSrcIp(), source, sourceMask) == -1) {
				ruleMatch = false;
			}
		}
	}

	if (destAlias != NULL) {
		if (!destAlias->searchForNetworkMatch(packet->getDstIp())) {
			ruleMatch = false;
		}
	} else {
		if (dest != -1) {
			if (IP4Addr::matchNetwork(packet->getDstIp(), dest, destMask) == -1) {
				ruleMatch = false;
			}
		}
	}

	if (getSourceDevice() != NULL) {
		if (packet->getSourceDev() != getSourceDevice()->getId()) {
			ruleMatch = false;
		}
	}

	if (getDestDevice() != NULL) {
		if (packet->getDestDev() != getDestDevice()->getId()) {
			ruleMatch = false;
		}
	}


	if (hwAddr.size() > 1) {
		if (packet->getHw().compare(hwAddr) != 0) {
			ruleMatch = false;
		}
	}

	if (type != NDEF) {
		if (type != packet->getProtocol()) {
			ruleMatch = false;
		}
	}

	/*Check Group Now*/
	if (groupid != -1 && ruleMatch != false) {
		bool gMatch = false;
		
		Session* session = sessionDb->get(packet->getHw(), IP4Addr::ip4AddrToString(packet->getSrcIp()));
		if (session != NULL) {
			User* user = session->getUser();
			if (user != NULL) {
				Group* group = groupDb->getGroup(groupid);
				if (group != NULL) {
					if (user->checkForGroup(group)) {
						gMatch = true;
						session->touch();
					}
				}
			}
		}

		if(gMatch == false){
			if(!session && (HttpTcpPacket::matchPort(packet->getDstPort()) || HttpProxyPacket::matchPort(packet->getDstPort()))){
				if(isActive()){
					capturePortal = true;
					return true;
				}
			}else{
				return false;
			}
		}
	}

	if(startHour != -1 && finishHour != -1){
		Time t(time(NULL));
		if(!(t.extractHour() >= startHour && t.extractHour() <= finishHour)){
			ruleMatch = false;
		}
	}

	/*If the rule matched - lets return the action*/
	if (ruleMatch && isActive()) {
		return true;
	}

	return false;
}

SFwallCore::NatRule* SFwallCore::ACLStore::matchNatAcls(Packet* packet, NAT_TYPE natType) {
	for (int x = 0; x < (int) natEntries.size(); x++) {
		bool b;
		bool ruleMatch = natEntries[x]->supermatch(packet, b, sessionDb, groupDb);
		if (natEntries[x]->natType != natType) {
			ruleMatch = false;
		}

		if (ruleMatch && natEntries[x]->isActive()) {
			natEntries[x]->count++;
			return natEntries[x];
		}
	}

	return NULL;
}

std::string SFwallCore::NatRule::stringAction() {
	return "NAT";
}

std::string SFwallCore::NatRule::toString() {
	stringstream ss;

	ss << "source ";
	if(source != -1){
		ss << IP4Addr::ip4AddrToString(source) << "/" << IP4Addr::ip4AddrToString(sourceMask);
	}else{
		ss << "any";
	}

	ss << " destination ";
	if(dest != -1){
		ss << IP4Addr::ip4AddrToString(dest) << "/" << IP4Addr::ip4AddrToString(destMask);
	}else{
		ss << "any";
	}

	ss << " protocol: " << stringProtocol(type) << "; ";
	ss << " action: " << this->stringAction();

	return ss.str();
}

void SFwallCore::ACLStore::deserializeRule(ObjectContainer* source, SFwallCore::Rule* entry){
	if(source->has("id")){
		entry->id = source->get("id")->string();
	}

	if(source->has("enabled")){
		entry->enabled = source->get("enabled")->boolean();
	}

	if (source->has("source")) {
		entry->source = IP4Addr::stringToIP4Addr(source->get("source")->string());
	}

	if (source->has("sourceMask")) {
		entry->sourceMask = IP4Addr::stringToIP4Addr(source->get("sourceMask")->string());
	}

	if (source->has("dest")) {
		entry->dest = IP4Addr::stringToIP4Addr(source->get("dest")->string());
	}

	if (source->has("destMask")) {
		entry->destMask = IP4Addr::stringToIP4Addr(source->get("destMask")->string());
	}

	if(source->has("dport_start") && source->has("dport_end")){
		entry->dport_start = source->get("dport_start")->number();
		entry->dport_end = source->get("dport_end")->number();
	}

	if(source->has("sport_start") && source->has("sport_end")){
		entry->sport_start = source->get("sport_start")->number();
		entry->sport_end = source->get("sport_end")->number();
	}

	if (source->has("dport")) {
		entry->setDportList(source->get("dport")->string());
	}

	if (source->has("sport")) {
		entry->setSportList(source->get("sport")->string());
	}

	if (source->has("type")) {
		int type = source->get("type")->number();

		switch (type) {
			case 1:
				entry->type = TCP;
				break;

			case 2:
				entry->type = UDP;
				break;

			case 3:
				entry->type = ICMP;
				break;

			case 4:
				entry->type = IGMP;
				break;
		};
	}

	if (source->has("groupid")) {
		entry->groupid = source->get("groupid")->number();
	}

	if (source->has("source_device")) {
		entry->sourceDevName= source->get("source_device")->string();
	}

	if (source->has("dest_device")) {
		entry->destDevName = source->get("dest_device")->string();
	}

	if (source->has("destAlias")) {
		entry->destAlias = aliases->get(source->get("destAlias")->string());
	}

	if (source->has("sourceAlias")) {
		entry->sourceAlias = aliases->get(source->get("sourceAlias")->string());
	}

	if (source->has("hwAddr")) {
		entry->hwAddr = source->get("hwAddr")->number();
	}

	if (source->has("comment")) {
		entry->comment = source->get("comment")->string();
	}
}

void SFwallCore::ACLStore::loadPriorityFile(){
	ObjectContainer* root;
	if (configurationManager->has("priorityAcls")) {
		root = configurationManager->getElement("priorityAcls");
		ObjectContainer* rules = root->get("rules")->container();

		for (int x = 0; x < rules->size(); x++) {
			PriorityRule* entry = new PriorityRule();

			ObjectContainer* source = rules->get(x)->container();
			deserializeRule(source, entry);

			entry->nice = source->get("nice")->number();
			loadAclEntry(entry);
		}
	} else {
		logger->log(SLogger::INFO, "Could not find any nat rules in configuration");
	}
}

void SFwallCore::ACLStore::loadNatFile() {
        ObjectContainer* root;
	if (configurationManager->has("natAcls")) {
		root = configurationManager->getElement("natAcls");
                ObjectContainer* rules = root->get("rules")->container();

		for (int x = 0; x < rules->size(); x++) {
			NatRule* entry = new NatRule();

			ObjectContainer* source = rules->get(x)->container();
			deserializeRule(source, entry);

			entry->natPort = source->get("natPort")->number();
			entry->natType = (SFwallCore::NAT_TYPE)source->get("natType")->number();

			if (source->has("natTarget")) {
				entry->natTarget = IP4Addr::stringToIP4Addr(source->get("natTarget")->string());
			}

			if (source->has("natTargetDevice")) {
				entry->natTargetDevice = source->get("natTargetDevice")->string();
			}

			loadAclEntry(entry);
		}
	} else {
		logger->log(SLogger::INFO, "Could not find any nat rules in configuration");
	}
}

void SFwallCore::ACLStore::loadFilterFile() {
	int loaded = 0;

	ObjectContainer* root;
	if (configurationManager->has("filterAcls")) {
		root = configurationManager->getElement("filterAcls");
		ObjectContainer* rules = root->get("rules")->container();

		for (int x = 0; x < rules->size(); x++) {
			FilterRule* entry = new FilterRule();
			ObjectContainer* source = rules->get(x)->container();

			deserializeRule(source, entry);
			if(source->has("log")){
				entry->log = source->get("log")->boolean();
			}

			if (source->has("action")) {
				entry->action = source->get("action")->number();
			}

			if (source->has("startHour") && source->has("finishHour")) {
				entry->startHour = source->get("startHour")->number();
				entry->finishHour = source->get("finishHour")->number();
			}

			if (source->has("comment")) {
				entry->comment = source->get("comment")->string();
			}

			if(source->has("ignoreconntrack")){
				entry->ignoreconntrack = source->get("ignoreconntrack")->boolean();
			}

			loadAclEntry(entry);
			loaded++;
		}
	}

	if (loaded == 0) {
		cout << "configuring default ruleset\n";
	
		logger->log(SLogger::ERROR, "No filter nat rules found, loading default accept all ruleset");
		FilterRule* ssh = new FilterRule();
		ssh->id = "default1"; 
		ssh->action = 1;
		ssh->dportList.insert(22);		
		ssh->dportList.insert(5000);		
		ssh->dportList.insert(8001);		
		ssh->enabled = true;

		FilterRule* def = new FilterRule();
		def->action = 0;
		def->id = "default2"; 
		def->enabled = true;

		loadAclEntry(ssh);
		loadAclEntry(def);
	}
}

void SFwallCore::ACLStore::serializeRule(ObjectContainer* rule, SFwallCore::Rule* target){
	if (target->dportList.size() != 0) {
		rule->put("dport", new ObjectWrapper((string) target->getDportList()));
	}

	if (target->sportList.size() == 0) {
		rule->put("sport", new ObjectWrapper((string) target->getSportList()));
	}

	if(target->sport_start != -1 && target->sport_end != -1){
		rule->put("sport_start", new ObjectWrapper((double) target->sport_start));
		rule->put("sport_end", new ObjectWrapper((double) target->sport_end));
	}

	if(target->dport_start != -1 && target->dport_end != -1){
		rule->put("dport_start", new ObjectWrapper((double) target->dport_start));
		rule->put("dport_end", new ObjectWrapper((double) target->dport_end));
	}

	if (target->source != -1)
		rule->put("source", new ObjectWrapper(IP4Addr::ip4AddrToString(target->source)));

	if (target->sourceMask != -1)
		rule->put("sourceMask", new ObjectWrapper(IP4Addr::ip4AddrToString(target->sourceMask)));

	if (target->dest != -1)
		rule->put("dest", new ObjectWrapper(IP4Addr::ip4AddrToString(target->dest)));

	if (target->destMask != -1)
		rule->put("destMask", new ObjectWrapper(IP4Addr::ip4AddrToString(target->destMask)));

	if (target->type != NDEF) {
		rule->put("type", new ObjectWrapper((double) target->type));
	}

	if (target->groupid != -1) {
		rule->put("groupid", new ObjectWrapper((double) target->groupid));
	}

	if (target->getSourceDevice() != NULL) {
		rule->put("source_device", new ObjectWrapper((string) target->getSourceDevice()->getInterface()));
	}

	if (target->getDestDevice() != NULL) {
		rule->put("dest_device", new ObjectWrapper((string) target->getDestDevice()->getInterface()));
	}
	if (target->destAlias != NULL) {
		rule->put("destAlias", new ObjectWrapper((string) target->destAlias->id));
	}

	if (target->sourceAlias != NULL) {
		rule->put("sourceAlias", new ObjectWrapper((string) target->sourceAlias->id));
	}

	if (target->hwAddr.size() > 0)
		rule->put("hwAddr", new ObjectWrapper((string) target->hwAddr));

	/*This method of ending the line is hax!! we shouldnt have to add a space character, but strings are gay*/
	if (target->comment.size() > 0) {
		rule->put("comment", new ObjectWrapper((string) target->comment));
	}

	rule->put("id", new ObjectWrapper((string) target->id));
}

void SFwallCore::ACLStore::savePriorityFile() {
	int loaded = 0;

	ObjectContainer* root = new ObjectContainer(CREL);
	ObjectContainer* rules = new ObjectContainer(CARRAY);
	for (int x = 0; x < (int) priorityQosRules.size(); x++) {
		ObjectContainer* rule = new ObjectContainer(CREL);
		serializeRule(rule, priorityQosRules[x]);
		rule->put("id", new ObjectWrapper((string) priorityQosRules[x]->id));
		rule->put("enabled", new ObjectWrapper((bool) priorityQosRules[x]->enabled));
		rule->put("nice", new ObjectWrapper((double) priorityQosRules[x]->nice));
		loaded++;

		rules->put(new ObjectWrapper(rule));
	}

	root->put("rules", new ObjectWrapper(rules));
	configurationManager->setElement("priorityAcls", root);
	configurationManager->save();
}

void SFwallCore::ACLStore::saveFilterFile() {
	int loaded = 0;

        ObjectContainer* root = new ObjectContainer(CREL);
        ObjectContainer* rules = new ObjectContainer(CARRAY);
	for (int x = 0; x < (int) entries.size(); x++) {
                ObjectContainer* rule = new ObjectContainer(CREL);
		serializeRule(rule, entries[x]);
		rule->put("id", new ObjectWrapper((string) entries[x]->id));
		rule->put("enabled", new ObjectWrapper((bool) entries[x]->enabled));
		rule->put("action", new ObjectWrapper((double) entries[x]->action));

		rule->put("ignoreconntrack", new ObjectWrapper((bool) entries[x]->ignoreconntrack));
		if(entries[x]->startHour != -1 && entries[x]->finishHour != -1){
			rule->put("startHour", new ObjectWrapper((double) entries[x]->startHour));
			rule->put("finishHour", new ObjectWrapper((double) entries[x]->finishHour));
		}

		rule->put("log", new ObjectWrapper((bool) entries[x]->log));
		loaded++;

		rules->put(new ObjectWrapper(rule));
	}

	root->put("rules", new ObjectWrapper(rules));
	configurationManager->setElement("filterAcls", root);
	configurationManager->save();
}

void SFwallCore::ACLStore::saveNatFile() {
        ObjectContainer* root = new ObjectContainer(CREL);
        ObjectContainer* rules = new ObjectContainer(CARRAY);

	for (int x = 0; x < (int) natEntries.size(); x++) {
                ObjectContainer* rule = new ObjectContainer(CREL);
		serializeRule(rule, natEntries[x]);

		if (natEntries[x]->natTarget != (in_addr_t) - 1) {
			rule->put("natTarget", new ObjectWrapper(IP4Addr::ip4AddrToString(natEntries[x]->natTarget)));
		}

		if(natEntries[x]->natTargetDevicePtr != NULL){
			rule->put("natTargetDevice", new ObjectWrapper((string) natEntries[x]->natTargetDevicePtr->getInterface()));
		}

		rule->put("natPort", new ObjectWrapper((double) natEntries[x]->natPort));
		rule->put("natType", new ObjectWrapper((double) natEntries[x]->natType));
		rules->put(new ObjectWrapper(rule));
	}

	root->put("rules", new ObjectWrapper(rules));
	configurationManager->setElement("natAcls", root);
	configurationManager->save();
}

void SFwallCore::ACLStore::loadDenyList(){
        ObjectContainer* root = new ObjectContainer(CREL);
	if (configurationManager->has("denyList")) {
		root = configurationManager->getElement("denyList");
		ObjectContainer* rules = root->get("items")->container();

		for (int x = 0; x < rules->size(); x++) {
			addToDenyList(IP4Addr::stringToIP4Addr(rules->get(x)->string()));
		}
	}	
}

void SFwallCore::ACLStore::saveDenyList(){
        ObjectContainer* root = new ObjectContainer(CREL);
        ObjectContainer* arr = new ObjectContainer(CARRAY);

	for(set<unsigned int>::iterator iter = denyList.begin(); iter != denyList.end(); iter++){
		arr->put(new ObjectWrapper((string) IP4Addr::ip4AddrToString((*iter))));
	}		

	root->put("items", new ObjectWrapper(arr));
	configurationManager->setElement("denyList", root);
	configurationManager->save();
}

std::string SFwallCore::FilterRule::stringAction() {
	string s;
	switch (action) {
		case SQ_ACCEPT:
			s = "allow";
			break;
		case SQ_DENY:
			s = "deny";
			break;
	}

	return s;
}

std::string SFwallCore::FilterRule::toString() {
	stringstream ss;

	ss << "source ";
	if(source != -1){
		ss << IP4Addr::ip4AddrToString(source) << "/" << IP4Addr::ip4AddrToString(sourceMask);
	}else{
		ss << "any";
	}

	ss << " destination ";
	if(dest != -1){
		ss << IP4Addr::ip4AddrToString(dest) << "/" << IP4Addr::ip4AddrToString(destMask);
	}else{
		ss << "any";
	}

	ss << " protocol: " << stringProtocol(type) << "; ";
	ss << " action: " << this->stringAction();

	return ss.str();
}

void SFwallCore::ACLStore::initLazyObjects(Rule* rule){
	rule->valid = true;
	if(logger){
		logger->log(SLogger::INFO, "ACLStore", "Init lazy interface objects");	
	}

	if(rule->sourceDevName.size() > 0){
		rule->sourceDevPtr = interfaceManager->get((string) rule->sourceDevName);
		if(!rule->sourceDevPtr){
			rule->valid = false;	
		}
	}

	if(rule->destDevName.size() > 0){
		rule->destDevPtr = interfaceManager->get((string) rule->destDevName);
		if(!rule->destDevPtr){
			rule->valid = false;
		}
	}

	if(NatRule * r = dynamic_cast<NatRule*> (rule)){
		if(r->natTargetDevice.size() > 0){
			r->setNatTargetDevice(interfaceManager->get((string) r->natTargetDevice));
			if(!r->natTargetDevicePtr){
				rule->valid = false;
			}
		}
	}	
}

int SFwallCore::ACLStore::loadAclEntry(Rule* rule) {
	initLazyObjects(rule);

	if (FilterRule * r = dynamic_cast<FilterRule*> (rule)) {
		for(int x = 0; x < entries.size(); x++){
			if(entries[x]->id == rule->id){
				entries[x] = r;
				return 0;
			}
		}

		entries.push_back(r);
	} else if (NatRule * r = dynamic_cast<NatRule*> (rule)) {
                for(int x = 0; x < natEntries.size(); x++){
                        if(natEntries[x]->id == rule->id){
                                natEntries[x] = r;
                                return 0;
                        }
                }

		natEntries.push_back(r);
	}else if(PriorityRule* r = dynamic_cast<PriorityRule*>(rule)){
                for(int x = 0; x < priorityQosRules.size(); x++){
                        if(priorityQosRules[x]->id == rule->id){
                                priorityQosRules[x] = r;
                                return 0;
                        }
                }

		priorityQosRules.push_back(r);
	}
	return 0;
}

int SFwallCore::ACLStore::unloadAclEntry(Rule* rule) {
	if (FilterRule * e = dynamic_cast<FilterRule*> (rule)) {
		for (int x = 0; x < (int) entries.size(); x++) {
			if (e == entries[x]) {
				entries.erase(entries.begin() + x);

				delete e;
				break;
			}
		}

	} else if (NatRule * e = dynamic_cast<NatRule*> (rule)) {
		for (int x = 0; x < (int) natEntries.size(); x++) {
			if (e == natEntries[x]) {
				natEntries.erase(natEntries.begin() + x);

				delete e;
				break;
			}
		}

	}else if(PriorityRule* e = dynamic_cast<PriorityRule*>(rule)){
		for (int x = 0; x < (int) priorityQosRules.size(); x++) {
			if (e == priorityQosRules[x]) {
				priorityQosRules.erase(priorityQosRules.begin() + x);

				delete e;
				break;
			}
		}

	}
	return 0;
}

int SFwallCore::ACLStore::createAclEntry(Rule* e) {
	if(e->id.size() == 0){
		e->id = StringUtils::genRandom(); 
	}

	lock->lock();
	loadAclEntry(e);
	save();
	lock->unlock();
	return 0;
}

int SFwallCore::ACLStore::deleteAclEntry(Rule* e) {
	lock->lock();
	unloadAclEntry(e);
	save();
	lock->unlock();
	return 0;
}

int SFwallCore::ACLStore::deleteAclEntryByPos(int rulePos) {
	lock->lock();
	Rule* rule;
	int   result;


	if ((rulePos >= 0) && (rulePos < entries.size())) {
		rule = entries[rulePos];
		lock->unlock(); // deleteAclEntry locks ACLs so must unlock before call.
		result = this->deleteAclEntry(rule);
	} else {
		lock->unlock(); // Did not call deleteAclEntry so we must unlock.
		result = -1;
	}

	return result;
}

void SFwallCore::ACLStore::save() {
	saveNatFile();
	saveFilterFile();
	saveDenyList();
	savePriorityFile();
}

void SFwallCore::ACLStore::movedownByPos(int rulePos) {
	lock->lock();
	if ((rulePos + 1) < (int) entries.size()) {
		FilterRule* a = entries[rulePos];
		FilterRule* b = entries[rulePos + 1];

		entries[rulePos + 1] = a;
		entries[rulePos]     = b;
	}

	save();
	lock->unlock();
}

void SFwallCore::ACLStore::movedown(Rule* rule) {
	int x = findPos(rule);
	if(FilterRule* e = dynamic_cast<FilterRule*>(rule)){
		this->movedownByPos(x);
	}else{
		lock->lock();
		if ((x + 1) < (int) priorityQosRules.size()) {
			PriorityRule* a = priorityQosRules[x];
			PriorityRule* b = priorityQosRules[x+ 1];

			priorityQosRules[x+ 1] = a;
			priorityQosRules[x]     = b;
		}
		lock->unlock();
	}
	save();
}

void SFwallCore::ACLStore::moveupByPos(int rulePos) {
	lock->lock();
	if ((rulePos - 1) >= 0) {
		FilterRule* a = entries[rulePos];
		FilterRule* b = entries[rulePos - 1];

		entries[rulePos - 1] = a;
		entries[rulePos]     = b;
	}

	save();
	lock->unlock();
}

void SFwallCore::ACLStore::moveup(Rule* rule) {
	int x = findPos(rule);
	if(FilterRule* e = dynamic_cast<FilterRule*>(rule)){
		this->moveupByPos(x);
	}else{
		lock->lock();
		if ((x - 1) >= 0) {
			PriorityRule* a = priorityQosRules[x];
			PriorityRule* b = priorityQosRules[x- 1];

			priorityQosRules[x- 1] = a;
			priorityQosRules[x]     = b;
		}
		lock->unlock();

	}

	save();
}

int SFwallCore::ACLStore::findPos(Rule* rule){
	lock->lock();
	for(int x= 0; x < entries.size(); x++){
		Rule* target = entries[x];
		if(target == rule){
			lock->unlock();
			return x;
		}
	}       

	for(int x= 0; x < priorityQosRules.size(); x++){
		Rule* target = priorityQosRules[x];
		if(target == rule){
			lock->unlock();
			return x;
		}
	}

	lock->unlock();
	return -1;
}

SFwallCore::Rule* SFwallCore::ACLStore::getRuleById(std::string id){
	lock->lock();
	for(int x= 0; x < entries.size(); x++){
		Rule* target = entries[x];
		if(target->id.compare(id) == 0){
			lock->unlock();
			return target;
		}
	}	

	for(int x= 0; x < natEntries.size(); x++){
		Rule* target = natEntries[x];
		if(target->id.compare(id) == 0){
			lock->unlock();
			return target;
		}
	}

	for(int x= 0; x < priorityQosRules.size(); x++){
		Rule* target = priorityQosRules[x];
		if(target->id.compare(id) == 0){
			lock->unlock();
			return target;
		}
	}

	lock->unlock();
	return NULL;
}

void SFwallCore::ACLStore::InterfaceChangeListener::event(){
	store->getlock();
	for (int x = 0; x < (int) store->entries.size(); x++) {
		store->initLazyObjects(store->entries[x]);
	}

	for (int x = 0; x < (int) store->natEntries.size(); x++) {
		store->initLazyObjects(store->natEntries[x]);
	}	
	store->releaselock();
}

void SFwallCore::ACLStore::aliasRemoved(Alias* alias){
	for(int x =0; x < entries.size(); x++){
		Rule* rule = entries[x];
		if((rule->destAlias && rule->destAlias == alias) || rule->sourceAlias && rule->sourceAlias == alias){
			deleteAclEntry(rule);
			aliasRemoved(alias);
			return;
		}
	}

	for(int x =0; x < natEntries.size(); x++){
		Rule* rule = natEntries[x];
		if((rule->destAlias && rule->destAlias == alias) || rule->sourceAlias && rule->sourceAlias == alias){
			deleteAclEntry(rule);
			aliasRemoved(alias);
			return;
		}
	}
}

void SFwallCore::ACLStore::addToDenyList(unsigned int i){
	lock->lock();
	denyList.insert(i);
	saveDenyList();
	lock->unlock();
}

void SFwallCore::ACLStore::removeFromDenyList(unsigned int i){
	lock->lock();
	denyList.erase(i);
	saveDenyList();
	lock->unlock();
}

bool SFwallCore::ACLStore::existsInDenyList(SFwallCore::Packet* packet){
	lock->lock();
	if(System::getInstance()->getFirewall()->BLOCK_LIST_ENABLED == 1){
		if(denyList.find(packet->getSrcIp()) != denyList.end() || denyList.find(packet->getDstIp()) != denyList.end()){
			lock->unlock();
			return true;	
		}
	}
	lock->unlock();
	return false; 
}


void SFwallCore::Rule::setDportList(std::string temp){
	vector<string> pieces;
	split(temp, ',', pieces);
	for(int y = 0; y < pieces.size(); y++){
		dportList.insert(atoi(pieces[y].c_str()));
	}
}

void SFwallCore::Rule::setSportList(std::string temp){
	vector<string> pieces; 
	split(temp, ',', pieces);
	for(int y = 0; y < pieces.size(); y++){
		sportList.insert(atoi(pieces[y].c_str()));
	}       
}

std::string SFwallCore::Rule::getDportList(){
	stringstream res;
	for(set<int>::iterator iter = dportList.begin();
			iter != dportList.end();
			iter++){
		res << (*iter) << ",";
	}
	return res.str();
}
std::string SFwallCore::Rule::getSportList(){
	stringstream res;
	for(set<int>::iterator iter = sportList.begin();
			iter != sportList.end();
			iter++){
		res << (*iter) << ",";
	}
	return res.str();
}


