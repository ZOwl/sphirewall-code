/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <sphirewall/Utils/StringUtils.h>

using namespace std;

#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Auth/AuthenticationHandler.h"
#include "Core/System.h"
#include "Utils/Lock.h"

ObjectWrapper::~ObjectWrapper(){
	if(isContainer()){
		delete containerValue;
	}
}

ObjectContainer::~ObjectContainer(){
	for(map<string, ObjectWrapper*>::iterator iter = keyObjects.begin();
			iter != keyObjects.end();
			iter++){
		delete iter->second;
	}

	for(vector<ObjectWrapper*>::iterator iter = listObjects.begin();
			iter != listObjects.end();
			iter++){
		delete (*iter);
	}
}

ObjectWrapper* ObjectContainer::get(std::string key){
	if(type == CREL){
		return keyObjects[key];
	}

	return NULL;
}


void ObjectContainer::put(std::string key, ObjectWrapper* input){
	if(type == CREL){
		if(has(key)){
			delete keyObjects[key];
		}

		keyObjects[key] = input;
	}
}

void ObjectContainer::put(ObjectWrapper* input){
	if(type == CARRAY){
		listObjects.push_back(input);
	}
}

bool ObjectContainer::has(std::string key){
	if(type == CREL){
		return keyObjects.find(key) != keyObjects.end();
	}

	return false;
}

int ObjectContainer::size(){
	if(type == CARRAY){
		return listObjects.size();
	}

	return 0;
}

ObjectWrapper* ObjectContainer::get(int pos){
	if(pos < listObjects.size()){
		return listObjects[pos];
	}

	return NULL;
}


ConfigurationManager::ConfigurationManager(){
	lock = new Lock();
	rootValue = new ObjectWrapper(new ObjectContainer(CREL));
}

ConfigurationManager::~ConfigurationManager(){
	delete lock;
	delete rootValue;
}

std::string ConfigurationManager::dump(){
	if (rootValue == NULL) {
		throw ConfigurationException("rootValue is null");
	}

	stringstream ss;
	ss << "# sphirewall.conf -- Generated by Sphirewall for the Core Sphirewall Daemon\n";
	ss << "# For support, see http://sphirewall.net\n";
	ss << JsonSerialiser::serializeToJsonString(rootValue, true);

	return ss.str();
}

void ConfigurationManager::save() {
	lock->lock();
	ofstream fp;
	fp.open(filename.c_str());
	fp << dump();
	fp.close();
	lock->unlock();
}

void ConfigurationManager::load(string filename) {
	this->filename = filename;
	ifstream fp(filename.c_str());
	if (fp.is_open()) {
		stringstream inputBuffer;
		while ( fp.good() )
		{
			string line;
			getline (fp,line);

			if(line[0] != '#'){
				inputBuffer << line;
			}
		}

		rootValue = JsonSerialiser::parseJsonString(inputBuffer.str());
		if(!rootValue){
			throw new ConfigurationException("Configuration file is corrupted");
		}
	}else{
		rootValue = new ObjectWrapper(new ObjectContainer(CREL));
	}
}

ObjectContainer* ConfigurationManager::getElement(std::string key) {
	if (rootValue == NULL){
		throw ConfigurationException("rootValue is null");
	}
	return rootValue->container()->get(key)->container();
}

void ConfigurationManager::setElement(std::string key, ObjectContainer* container) {
	if (rootValue == NULL) {
		throw ConfigurationException("rootValue is null");
	}
	lock->lock();
	rootValue->container()->put(key, new ObjectWrapper(container));
	lock->unlock();
}

bool ConfigurationManager::has(std::string key) {
	if (rootValue == NULL){
		throw ConfigurationException("rootValue is null");
	}
	return rootValue->container()->has(key);
}

int ConfigurationManager::put(std::string path, ObjectWrapper* value) {
	vector<string> pathElements;
	split(path, ':', pathElements);

	if (!rootValue){
		throw ConfigurationException("rootValue is null");
	}

	ObjectContainer* curser = rootValue->container();
	for (int x = 0; x < pathElements.size(); x++) {

		if (x == pathElements.size() - 1) {
			curser->put(pathElements[x], value);

			save();
			return 0;
		} else {

			if (curser->has(pathElements[x])) {
				curser = curser->get(pathElements[x])->container();
			} else {
				ObjectContainer* o = new ObjectContainer(CREL);
				curser->put((pathElements[x]), new ObjectWrapper(o));

				return put(path, value);
			}
		}
	}
}

int ConfigurationManager::put(std::string path, std::string value) {
	return put(path, new ObjectWrapper((std::string) value));
}

int ConfigurationManager::put(std::string path, bool value) {
	return put(path, new ObjectWrapper((bool) value));
}

int ConfigurationManager::put(std::string path, double number) {
	return put(path, new ObjectWrapper((double) number));
}

ObjectWrapper* ConfigurationManager::get(std::string path) {
	if (!hasByPath(path)) {
		throw new ConfigurationException("Could not find value");
	}

	vector<string> pathElements;
	split(path, ':', pathElements);

	if (!rootValue){
		throw new ConfigurationException("rootValue is null");
	}

	ObjectContainer* base = rootValue->container();
	ObjectContainer* curser = base;
	for (int x = 0; x < pathElements.size(); x++) {
		if (x == pathElements.size() - 1) {
			if (curser) {
				return curser->get(pathElements[x]);
			}

		} else {
			if (curser == NULL) {
				curser = base->get(pathElements[x])->container();
			} else {
				curser = curser->get(pathElements[x])->container();
			}
		}
	}
}

bool ConfigurationManager::hasByPath(std::string path) {
	vector<string> pathElements;
	split(path, ':', pathElements);

	if(!rootValue){
		throw ConfigurationException("rootValue is null");
	}

	ObjectContainer* base = rootValue->container();
	ObjectContainer* curser = NULL;

	for (int x = 0; x < pathElements.size(); x++) {
		if (x == pathElements.size() - 1) {
			return curser->has(pathElements[x]);

		} else {
			if (curser == NULL) {
				if (!base->has(pathElements[x])) {
					return false;
				}

				curser = base->get(pathElements[x])->container();
			} else {
				if (!curser->get(pathElements[x])) {
					return false;
				}
				curser = curser->get(pathElements[x])->container();
			}
		}
	}
}

std::string StringifyString(std::string str)
{
	std::string str_out = "";

	std::string::iterator iter = str.begin();
	while (iter != str.end())
	{
		wchar_t chr = *iter;

		if (chr == '"' || chr == '\\' || chr == '/')
		{
			str_out += '\\';
			str_out += chr;
		}
		else if (chr == '\b')
		{
			str_out += "\\b";
		}
		else if (chr == '\f')
		{
			str_out += "\\f";
		}
		else if (chr == '\n')
		{
			str_out += "\\n";
		}
		else if (chr == '\r')
		{
			str_out += "\\r";
		}
		else if (chr == '\t')
		{
			str_out += "\\t";
		}
		else if (chr < ' ')
		{
			str_out += "\\u";
			for (int i = 0; i < 4; i++)
			{
				int value = (chr >> 12) & 0xf;
				if (value >= 0 && value <= 9)
					str_out += (wchar_t)('0' + value);
				else if (value >= 10 && value <= 15)
					str_out += (wchar_t)('A' + (value - 10));
				chr <<= 4;
			}
		}
		else
		{
			str_out += chr;
		}

		iter++;
	}

	return str_out;
}

void JsonSerialiser::recurseObject(std::stringstream& root, ObjectContainer* input){
	c++;
	if(input->getType() == CREL){
		if(format){
			root << "\n";
			for(int xx = 0; xx < c; xx++){
				root << "\t";
			}
			root << "{\n";
		}else{
			root << "{";
		}
		map<string, ObjectWrapper*>& crelObjects = input->getKeyObjects();
		for(map<string, ObjectWrapper*>::iterator iter = crelObjects.begin();
				iter != crelObjects.end();
				iter++){

			if(iter != crelObjects.begin()){
				root << ",";
				if(format){
					root << "\n";
				}
			}

			std::string key = iter->first;
			ObjectWrapper* potentialValue = iter->second;
			if(potentialValue == NULL){
				throw new ConfigurationException("Value was null" + key);
			}

			if(format){
				for(int xx = 0; xx < c; xx++){
					root << "\t";
				}
			}

			root << "\"" << key << "\":";	
			if(potentialValue->isContainer()){
				recurseObject(root, potentialValue->container());
			}else if(potentialValue->isBoolean()){
				if(potentialValue->boolean()){
					root << "true";
				}else{
					root << "false";
				}	
			}else if(potentialValue->isNumber()){
				root << potentialValue->number();
			}else if(potentialValue->isString()){
				root << "\"" << StringifyString(potentialValue->string()) << "\"";
			}
		}
		if(format){
			root << "\n";
			for(int xx = 0; xx < c; xx++){
				root << "\t";
			}
		}
		root << "}";

	}else if(input->getType() == CARRAY){
		if(format){
			root << "\n";
			for(int xx = 0; xx < c; xx++){
				root << "\t";
			}
			root << "[\n";
		}else{
			root << "[";
		}
		for(int x = 0; x < input->size(); x++){
			if(format){
				if(x != 0){
					root << ",\n";
				}		

				for(int xx = 0; xx < c; xx++){
					root << "\t";
				}
			}else{
				if(x != 0){
					root << ",";
				}
			}

			ObjectWrapper* potentialValue = input->get(x);
			if(potentialValue->isContainer()){
				ObjectContainer* deeperContainer = potentialValue->container();
				if(deeperContainer->getType() == CREL){
					recurseObject(root, deeperContainer);
				}else if(deeperContainer->getType() == CARRAY){
					recurseObject(root, deeperContainer);
				}
			}else if(potentialValue->isBoolean()){
				if(potentialValue->boolean()){
					root << "true";
				}else{
					root << "false";
				}
			}else if(potentialValue->isNumber()){
				root << potentialValue->number();
			}else if(potentialValue->isString()){
				root << "\"" << StringifyString(potentialValue->string()) << "\"";
			}
		}		

		if(format){
			root << "\n";
			for(int xx = 0; xx < c; xx++){
				root << "\t";
			}
		}
		root << "]";
	}
	c--;
}

std::string JsonSerialiser::serialize(ObjectWrapper* input){
	stringstream stream;
	recurseObject(stream, input->container());	
	return stream.str();
}	

ObjectWrapper* JsonSerialiser::parseRecurse(JSONValue* value){
	if(value->IsString()){
		return new ObjectWrapper(value->String());	
	}else if(value->IsBool()){
		return new ObjectWrapper(value->AsBool());	
	}else if(value->IsNumber()){
		return new ObjectWrapper(value->AsNumber());
	}else if(value->IsObject()){
		ObjectContainer* container = new ObjectContainer(CREL);

		JSONObject& obj = value->AsObject();
		for(std::map<std::wstring, JSONValue*>::iterator iter = obj.values.begin();
				iter != obj.values.end();
				iter++){

			container->put(WStringToString(iter->first), parseRecurse(iter->second));	
		}		

		return new ObjectWrapper(container);	
	}else if(value->IsArray()){
		ObjectContainer* container = new ObjectContainer(CARRAY);

		JSONArray obj = value->AsArray();
		for(int x = 0; x < obj.size(); x++){
			container->put(parseRecurse(obj[x]));
		}
		return new ObjectWrapper(container);	
	}

	return NULL;
}

ObjectWrapper* JsonSerialiser::parse(std::string input){
	JSONValue* value = JSON::Parse(input.c_str());
	if (!value) {
		throw new ConfigurationException("Could not parse json");                
	}

	ObjectWrapper* ret = parseRecurse(value);	
	delete value;
	return ret;
}

ObjectWrapper* JsonSerialiser::parseJsonString(std::string input){
	JsonSerialiser* s = new JsonSerialiser();
	ObjectWrapper* ret = s->parse(input);
	delete s;
	return ret;
}

std::string JsonSerialiser::serializeToJsonString(ObjectWrapper* input, bool format){
	JsonSerialiser* s = new JsonSerialiser();
	s->setFormat(format);
	string ret = s->serialize(input);
	delete s;
	return ret;
}

