/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TA_H
#define TA_H

#include "Core/Logger.h"
#include "Utils/Utils.h"
#include "Common/sphirewall_bandwidth_types.h"
#include "Database/BaseDb.h"
#include "Utils/TimeWrapper.h"

class AddressPair {
	public:
		AddressPair(unsigned int address, std::string hw) :
			address(address), hw(hw){

		}
		unsigned int address;
		std::string hw;
};

class Filter {
	public:
		Filter(){
			this->sourceIp_set = false;
			this->hw_set = false;
			this->user_set = false;
			this->limit = -1;
		}

		Time startTime;
		Time endTime;
		
		bool sourceIp_set;
		bool hw_set;
		bool user_set;

		void setUser(std::string user){
			this->user = user;
			user_set = true;
		}	

		void setHw(std::string hw){
			this->hw = hw;
			hw_set = true;
		}	

		void setSourceIp(unsigned int sourceIp){
			this->sourceIp = sourceIp;
			sourceIp_set = true;
		}
	
		void setLimit(int limit){
		    this->limit = limit;
		}

		std::string getUser(){
			return this->user;
		}

		std::string getHw(){
			return this->hw;
		}

		unsigned int getSourceIp(){
			return this->sourceIp;
		}

		unsigned int getLimit(){
			return this->limit;
		}

	private:
        std::string user;
        std::string hw;
        unsigned int sourceIp;
        int  limit;
};

class TrafficAnalyticsDb : public BaseDb {
	public:

		TrafficAnalyticsDb(std::string dbFileName, SLogger::LogContext* logger) :
			dbName(dbFileName), logger(logger) {
				this->byteThreshold = 0;
				this->retensionPeriod = 60 * 60 * 24 * 5;
				this->reductionCounter = 0;
				this->daySwitchPeriod = 60 * 60 * 24 * 2;
				this->insertSinceLastVacume = 0;
			}

		sqlite3* connection() {
			return dbConn;
		}

		int prepareStatements();
		void createTables();
		void initDb();
		void createPrepStatements();

		void add(taInputSlot slot);
		void flush();

		void addPostHook();
		void addTransfer(int upload, int download);

		void getTopLocalAddresses(std::vector<taSummaryUnit>& _return, Time startDate,Time endDate, int limit);
		void getTopUsers(std::vector<taSummaryUnit>& _return, Time startDate,Time stopDate, int limit);
		
		void getTransfer(std::vector<taSummaryUnit>& _return, Filter filter);
		void getPortTransfer(std::vector<taSummaryUnit>& _return, Filter filter);	
		void getWebsiteTransfer(std::vector<taWebsiteSummaryUnit>& _return, Filter filter);

		void getDeviceSummaryByHour(std::vector<taSummaryUnit>& _return,std::string device, Time startDate, Time endDate);
		void getDateSummary(std::vector<taSummaryUnit>& _return, Time startDate,Time endDate);

		void getWebsiteByAddress(std::vector<taWebsiteSummaryUnit>& _return,std::string httpHost, Time startDate,Time endDate);
		
		int getRetensionPeriod() const {
			return this->retensionPeriod;
		}

		int getByteThreshold() const {
			return this->byteThreshold;
		}	

		void setRetensionPeriod(int value) {
			this->retensionPeriod = value;
		}

		void setByteThreshold(int value){
			this->byteThreshold = value;
		}

		void setDaySwitchPeriod(int value){
			this->daySwitchPeriod = value;
		}
		void dump(std::vector<taSummaryUnit>& _return , Filter filter);
                int reductionCounter;

		static std::string reduceurl(std::string input);

                std::string getTopWebsite(Filter filter);
                std::string getTopUser(Filter filter);
                std::string getTopIp(Filter filter);
                std::string getTopDevice(Filter filter);

	private:
		map<long, list<taInputSlot> > flushCache;

		sqlite3 *dbConn;
		std::map<sqlite3_stmt**, std::string> PREP_STATEMENTS;
		std::vector<std::string> TABLES;
		std::string dbName;
		SLogger::LogContext* logger;

		void updateTaMasterTable(int tcpUp, int tcpDown, int udpUp,int udpDown, unsigned int address, int time, std::string user, int port, std::string website, std::string hw);
		void updateTaMasterDayTable(int tcpUp, int tcpDown, int udpUp,int udpDown, unsigned int address, int time, std::string user, int port, std::string website, std::string hw);
		void updateDeviceByHour(int tcpUp, int tcpDown, int udpUp, int udpDown,	std::string device, int time);
		void updateDateSummary(int tcpUp, int tcpDown, int udpUp, int udpDown, int time);
		void updateDeviceByDay();
		void updateDateSummary();
		void deleteUsingDate(string tableName, string datem);

		const static int HTTP_PORT = 80;

		int retensionPeriod;
		int daySwitchPeriod;
		int byteThreshold;
		int insertSinceLastVacume;
};

#endif
