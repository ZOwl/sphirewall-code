/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <list>

using namespace std;

#include "Api/JsonManagementService.h"
#include "Core/Event.h"
#include "BandwidthDb/AnalyticsClient.h"

JSONObject AnalyticsDelegate::process(string uri, JSONObject object) {
	JSONObject wrapper;
	wrapper.put(L"request", new JSONValue((string) uri));
	wrapper.put(L"args", new JSONValue((object)));

	JSONValue *responseValue = new JSONValue(wrapper);

	std::string sMsg = WStringToString(responseValue->Stringify());
	string resp;
	try {
		resp = client->sendAndRecv(sMsg);
	} catch (exception& e) {
		throw new ServiceNotAvailableException("service currently unavailable");
	}

	JSONValue* value = JSON::Parse(resp.c_str());
	if (!value) {
		throw new DelegateGeneralException("could not parse json response");
	}

	JSONObject responseRoot = value->AsObject();
	if (responseRoot[L"code"]->AsNumber() == -1) {
		throw new DelegateGeneralException(responseRoot[L"message"]->String());
	}

	return responseRoot[L"response"]->AsObject();
}
