#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester

class ConfigurationTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester();
        self.tester.connect();
        self.tester.login("fw", "admin", "admin")

    def test_setUserTimeout(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Configure')]").click();

        name = self.tester.driver.find_element_by_name("session_timeout")
        name.clear()
        name.send_keys("900")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        name = self.tester.driver.find_element_by_name("session_timeout")
        self.assertEquals(name.get_attribute("value"), "900")

    def tearDown(self):
        self.tester.disconnect()
