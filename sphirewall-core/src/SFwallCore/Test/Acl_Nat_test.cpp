/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <gtest/gtest.h>
#include "SFwallCore/Test/TestFactory.h"
#include "SFwallCore/Acl.h"

using namespace SFwallCore;

TEST(AclStore_Nat, ignoreDisabledRules){
	ACLStore* store = new ACLStore();
	NatRule* filter = new NatRule();
	filter->enabled = false;
	store->listNatRules().push_back(filter);
	
	Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
	EXPECT_TRUE(store->matchNatAcls(packet, DNAT) == NULL);
}

TEST(AclStore_Nat, acceptEnabledRules){
	ACLStore* store = new ACLStore();
	NatRule* filter = new NatRule();
	filter->enabled = true;
	store->listNatRules().push_back(filter);
	
	Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
	EXPECT_TRUE(store->matchNatAcls(packet, DNAT) != NULL);
}

TEST(AclStore_Nat, reject_destinationIp){
	ACLStore* store = new ACLStore();
	NatRule* filter = new NatRule();
	filter->dest = IP4Addr::stringToIP4Addr("10.22.3.1");	
	store->listNatRules().push_back(filter);
	
	Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
	EXPECT_TRUE(store->matchNatAcls(packet, DNAT) == NULL);
}

TEST(AclStore_Nat, reject_sourceIp){
	ACLStore* store = new ACLStore();
	NatRule* filter = new NatRule();
	filter->source= IP4Addr::stringToIP4Addr("10.22.3.1");	
	store->listNatRules().push_back(filter);
	
	Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
	EXPECT_TRUE(store->matchNatAcls(packet, DNAT) == NULL);
}

TEST(AclStore_Nat, accept_ipNats){
	ACLStore* store = new ACLStore();
	NatRule* filter = new NatRule();
	filter->source= IP4Addr::stringToIP4Addr("10.1.1.1");	
	filter->dest = IP4Addr::stringToIP4Addr("10.1.1.2");	

	printf("filter->sourceDevPtr: %p\n", filter->sourceDevPtr);
	printf("filter->destDevPtr: %p\n", filter->destDevPtr);
	
	store->listNatRules().push_back(filter);
	
	Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
	EXPECT_TRUE(store->matchNatAcls(packet, DNAT) != NULL);
}

TEST(AclStore_Nat, reject_type){
        ACLStore* store = new ACLStore();
        NatRule* filter = new NatRule();
	filter->type = UDP;
        store->listNatRules().push_back(filter);

        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        EXPECT_TRUE(store->matchNatAcls(packet, DNAT) == NULL);
}

TEST(AclStore_Nat, accept_type){
        ACLStore* store = new ACLStore();
        NatRule* filter = new NatRule();
	filter->type = TCP;
        store->listNatRules().push_back(filter);

        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        EXPECT_TRUE(store->matchNatAcls(packet, DNAT) != NULL);
}

TEST(AclStore_Nat, reject_destDev){
	NetDevice* device = new NetDevice();
	device->setId(10);

        ACLStore* store = new ACLStore();
        NatRule* filter = new NatRule();
	filter->destDevPtr = device;
        store->listNatRules().push_back(filter);

        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setDestDev(packet, 100);
        EXPECT_TRUE(store->matchNatAcls(packet, DNAT) == NULL);
}

TEST(AclStore_Nat, accept_destDev){
        NetDevice* device = new NetDevice();
        device->setId(1);

        ACLStore* store = new ACLStore();
        NatRule* filter = new NatRule();
        filter->destDevPtr = device;
        store->listNatRules().push_back(filter);

        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setDestDev(packet, 1);
        EXPECT_TRUE(store->matchNatAcls(packet, DNAT) != NULL);
}

TEST(AclStore_Nat, reject_sourceDev){
	NetDevice* device = new NetDevice(); device->setId(10);

        ACLStore* store = new ACLStore();
        NatRule* filter = new NatRule();

	filter->sourceDevPtr = device;
        store->listNatRules().push_back(filter);

        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchNatAcls(packet, DNAT) == NULL);
}

TEST(AclStore_Nat, accept_sourceDev){
        NetDevice* device = new NetDevice(); device->setId(1);
        ACLStore* store = new ACLStore();
        NatRule* filter = new NatRule();
        filter->sourceDevPtr = device;
        store->listNatRules().push_back(filter);

        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setSourceDev(packet, 1);
        EXPECT_TRUE(store->matchNatAcls(packet, DNAT) != NULL);
}

TEST(AclStore_Nat, return_correct_rule_pointer){
	NetDevice* device1 = new NetDevice(); device1->setId(100);
        NetDevice* device2 = new NetDevice(); device2->setId(1);

	ACLStore* store = new ACLStore();
        NatRule* filter = new NatRule();
        NatRule* filter2 = new NatRule();
	filter2->sourceDevPtr = device1;
        filter->sourceDevPtr = device2;
        store->listNatRules().push_back(filter);
	store->listNatRules().push_back(filter2);

        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setSourceDev(packet, 1);
        EXPECT_TRUE(store->matchNatAcls(packet, DNAT) == filter);
}

TEST(AclStore_Nat, deny_natType_SNAT){
        NetDevice* device = new NetDevice(); device->setId(1);

        ACLStore* store = new ACLStore();
        NatRule* filter = new NatRule();
	filter->setNatType(SNAT);
        filter->sourceDevPtr = device;
        store->listNatRules().push_back(filter);

        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setSourceDev(packet, 1);
        EXPECT_TRUE(store->matchNatAcls(packet, DNAT) == NULL);
}

TEST(AclStore_Nat, deny_natType_DNAT){
        NetDevice* device = new NetDevice(); device->setId(1);

        ACLStore* store = new ACLStore();
        NatRule* filter = new NatRule();
	filter->setNatType(DNAT);
        filter->sourceDevPtr = device;
        store->listNatRules().push_back(filter);

        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setSourceDev(packet, 1);
        EXPECT_TRUE(store->matchNatAcls(packet, SNAT) == NULL);
}

TEST(AclStore_Nat, incrementcount){
        ACLStore* store = new ACLStore();
        NatRule* filter = new NatRule();
	filter->setNatType(DNAT);
        store->listNatRules().push_back(filter);

        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
	EXPECT_TRUE(store->matchNatAcls(packet, DNAT)->count == 1);
}

