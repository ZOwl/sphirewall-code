#Copyright Michael Lawson, John Deal
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

from _socket import AF_INET, SOCK_STREAM
import json
import ssl
import ssl, pprint
import sys
from socket import socket
import readline
from os.path import expanduser
import ConnectionDetail
from Node import Node
from Set import set_inTestMode, set_config_runtime, set_config_system, set_firewall_acls_add, set_firewall_acls_move, set_firewall_acls_delete, set_firewall_qos_add, set_firewall_qos_delete, set_network_device_update, set_network_device_toggle, set_network_device_alias_add, set_network_device_alias_delete, set_config_loglevel, set_user_add, set_user_delete, set_user_update, set_user_password, set_group_add, set_group_delete, set_group_update, set_user_quota_add, set_user_quota_delete, set_group_quota_add, set_group_quota_delete,set_user_disconnect

from ShowHandlers import show_session_token, show_session_connection, show_metrics, show_runtime, show_connections, show_devices, show_acls, show_config_system, show_config_loglevel, show_users, show_groups, show_qos, show_user_quotas, show_group_quotas, show_status_users, show_status_clients, show_status_devices, show_status_websites

class Cli:
    token            = None
    hostname         = None
    s                = None;
    handlers         = []
    root             = None
    connectionDetail = None
    testMode         = None

    def show_token(self):
        print "Token";

    def show_connection(self):
        print "Connection";


    def __init__(self, username, password, hostname, port, doSSL, testMode):
        msg = "Connecting to " + hostname + " on " + port

        if doSSL == True:
            msg += " (ssl)"

        print msg
        tokenRequest = {'request': 'auth', 'username': username, 'password': password}

        s = socket(AF_INET, SOCK_STREAM)
        s.connect((hostname, int(port)))

        tokenRequest = {'request': 'auth', 'username': username, 'password': password}

        if doSSL == True:
            sslSocket = ssl.wrap_socket(s)
            # print repr(sslSocket.getpeername())
            # print sslSocket.cipher()
            # print pprint.pformat(sslSocket.getpeercert())
            sslSocket.sendall(json.dumps(tokenRequest) + "\n")
            data = sslSocket.recv(1024)
        else:
            s.sendall(json.dumps(tokenRequest) + "\n")
            data = s.recv(1024)

        self.testMode = testMode
        set_inTestMode(self.testMode)
        response      = json.loads(data)

        if response['code'] == 0:
            token = response['token']
            self.connectionDetail = ConnectionDetail.ConnectionDetail(token, \
                hostname, port, username, doSSL);

            print "Authenticated with token: " + token

            self.root = Node(None, None, None);
            set = Node(self.root, None, "set");
            show = Node(self.root, None, "show");

            #Show:
            #Root show Config
            showConfig = Node(show, None, "show config");
            showRuntime = Node(showConfig, show_runtime, "show config runtime");
            showSystemConfig = Node(showConfig, show_config_system, "show config system");
            showLogLevels = Node(showConfig, show_config_loglevel, "show config loglevel");

            #Root show Firewall
            showFirewall = Node(show, None, "show firewall");
            showAcls = Node(showFirewall, show_acls, "show firewall acls");
            showQos = Node(showFirewall, show_qos, "show firewall qos")
            
            #Root show Group
            showGroup = Node(show, None, "show group");
            showGroupList = Node(showGroup, show_groups, "show group list")
            showGroupQuotas = Node(showGroup, show_group_quotas, "show group quota");

            #Root show Network
            showNetwork = Node(show, None, "show network");
            showDevices = Node(showNetwork, show_devices, "show network devices");
            
            #Root show Session
            showSession = Node(show, None, "show session");
            showToken = Node(showSession, show_session_token, "show session token");
            showConnection = Node(showSession, show_session_connection, "show session connection");

            #Root show Status:
            showStatus = Node(show, None, "show status");
            showStatusClients = Node(showStatus, show_status_clients, "show status clients");
            showConnections = Node(showStatus, show_connections, "show status connections");
            showStatusUsers = Node(showStatus, show_status_users, "show status users");
            showStatusDevices = Node(showStatus, show_status_devices, "show status devices");
            showStatusWebsites = Node(showStatus, show_status_websites, "show status websites");
            showMetrics = Node(showStatus, show_metrics, "show status metrics");

            #Root show User
            showUsers = Node(show, None, "show user");
            showUsersList = Node(showUsers, show_users, "show user list")
            showUserQuotas = Node(showUsers, show_user_quotas, "show user quota")


            #Set:
            #Root Set Config
            setConfig = Node(set, None, "set config");
            setRuntime = Node(setConfig, set_config_runtime, "set config runtime");
            setSystem = Node(setConfig, set_config_system, "set config system");
            setLoglevel = Node(setConfig, set_config_loglevel, "set config loglevel");

            #Root Set Firewall
            setFirewall = Node(set, None, "set firewall");
            setFirewallAclsAdd = Node(setFirewall, set_firewall_acls_add, "set firewall acls add");
            setFirewallAclsMove = Node(setFirewall, set_firewall_acls_move, "set firewall acls move");
            setFirewallAclsDelete = Node(setFirewall, set_firewall_acls_delete, "set firewall acls delete");
            setFirewallQosAdd = Node(setFirewall, set_firewall_qos_add, "set firewall qos add");
            setFirewallQosDelete = Node(setFirewall, set_firewall_qos_delete, "set firewall qos delete");
            
            #Root Set group
            setGroup = Node(set, None, "set group");
            setGroupAdd = Node(setGroup, set_group_add, "set group add");
            setGroupDelete = Node(setGroup, set_group_delete, "set group delete");
            setGroupUpdate = Node(setGroup, set_group_update, "set group update");
            setGroupQuotaAdd = Node(setGroup, set_group_quota_add, "set group quota add")
            setGroupQuotaDelete = Node(setGroup, set_group_quota_delete, "set group quota delete")
                
            #Root Set Network
            setNetwork = Node(set,None, "set network");
            setNetworkDeviceUpdate = Node(setNetwork, set_network_device_update, "set network device update");
            setNetworkDeviceState = Node(setNetwork, set_network_device_toggle, "set network device state");
            setNetworkDeviceAliasAdd = Node(setNetwork, set_network_device_alias_add, "set network device alias add");
            setNetworkDeviceAliasDelete = Node(setNetwork, set_network_device_alias_delete, "set network device alias delete");

            #Root Set user
            setUser = Node(set, None, "set user");
            setUserAdd = Node(setUser, set_user_add, "set user add");
            setUserDelete = Node(setUser, set_user_delete, "set user delete");
            setUserUpdate = Node(setUser, set_user_update, "set user update");
            setUserPassword = Node(setUser, set_user_password, "set user password");
            setUserQuotaAdd = Node(setUser, set_user_quota_add, "set user quota add");
            setUserQuotaDelete = Node(setUser, set_user_quota_delete, "set user quota delete");
            setUserDisconnect = Node(setUser, set_user_disconnect, "set user disconnect")
            
        else:
            print "Failed to authenticate user";
            exit()
           
		
    def process(self):
        # Auto-complete class and functions
        class Completer:
            def __init__(self,commands):
                self.commands = commands

            def traverse(self,tokens,tree):
                
                if tree is None:
                    return []
                elif len(tokens) == 0:
                    return []
                elif len(tokens) == 1:
                    # obtaining matches from commands
                    return [x + ' ' for x in tree if x.startswith(tokens[0])]
                else:
                    # second match from commands
                    if tokens[0] in tree.keys():
                        return self.traverse(tokens[1:],tree[tokens[0]])
                    else:
                        return
                return []

            def complete(self,text,state):
                try:
                    tokens = readline.get_line_buffer().split()
                    if not tokens or readline.get_line_buffer()[-1] == ' ':
                        # append needs a value
                        tokens.append(text)

                    results = self.traverse(tokens,self.commands) + [None]
                    return results[state]
                    
                except Exception,e:
                    print e 
         
        # Define me some colours
        GREEN   = "\033[32m"
        ENDC    = "\033[0m"
        
        # Define CLI prompt
        prompt = GREEN + "\nsphirewall> " + ENDC;
        
        # Commands available for tab complete
        commands = {
                    'show':
                        {
                        'config':
                                {
                                'runtime':None,
                                'system':None, 
                                'loglevel':None,
                                }, 
                        'firewall':
                                {
                                'acls':None,
                                'qos':None,
                                }, 
                        'group':
                                {
                                'list':None,
                                'quota':None,
                                }, 
                        'network':
                                {
                                'devices':None,
                                }, 
                        'session':
                                {
                                'token':None,
                                'connection':None,
                                }, 
                        'status':
                                {
                                'clients':None,
                                'connections':None,
                                'users':None,
                                'websites':None,
                                'metrics':None,
                                }, 
                        'user':
                                {
                                'list':None,
                                'quota':None,
                                },
                        },
                 'set':
                        {
                        'config':
                                {
                                'runtime':None,
                                'system':None, 
                                'loglevel':None,
                                }, 
                        'firewall':
                                {
                                'acls':
                                        {
                                        'add':None,
                                        'move':None,
                                        'delete':None,
                                        },
                                'qos':
                                        {
                                        'add':None,
                                        'delete':None,
                                        },
                                },
                        'group':
                                {
                                'add':None,
                                'delete':None,
                                'update':None,
                                'quota':
                                        {
                                        'add':None,
                                        'delete':None,
                                        },
                                },
                        'network':
                                {
                                'device':
                                        {
                                        'update':None,
                                        'state':None,
                                        'alias':
                                                {
                                                'add':None,
                                                'delete':None,
                                                },
                                        },
                                },
                        'user':
                                {
                                'add':None,
                                'delete':None,
                                'update':None,
                                'password':None,
                                'quota':
                                        {
                                        'add':None,
                                        'delete':None,
                                        },
                                'disconnect':None,
                                },
                        }
                }
        # Complete and history binding
        readline.parse_and_bind("tab: complete")
        completer = Completer(commands)
        readline.set_completer(completer.complete)        
        history_file = expanduser("~/.scli.log")
        readline.parse_and_bind('"^[[A": history-search-backward')
        
        # Make sure we have a history file
        try:
            readline.read_history_file(history_file)
        except IOError: 
            # Likely to be first run and file doesn't exist yet
            pass
        
        try:
            # Loop our prompt and save the history
            while 1:
                try:
                    if self.testMode == False:
                        foo = raw_input(prompt) # Normal prompt.
                    else:
                        # Must split sperate stdin and stdout I/O for tests.
                        print prompt
                        sys.stdout.flush()
                        foo = raw_input()

                    if foo == 'exit' or foo == 'exit ':
                        print "Exiting...."
                        exit()
                    elif foo != '':
                        readline.write_history_file(history_file)
                        node = self.handle(self.root, foo)
                        if node != None:
                            node.function(self.connectionDetail, foo.split(node.c)[1])
                            
                except KeyboardInterrupt:
                    print ''
                    
        except EOFError:
            print "\nExiting...."

    def handle(self, node, input):
        for l in node.leaves:
            if l.matches(input) == 0:
                return self.handle(l, input)

        if node.function != None:
            return node;

        l.usage();
        return None;
