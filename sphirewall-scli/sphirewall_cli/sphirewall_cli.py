#!/usr/bin/python
#Copyright Michael Lawson, John Deal
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python

#Sphirewall SCLI Python
#Copyright Michael Lawson 2012
import sys
import getpass
from os import chmod
from os.path import expanduser

from Cli import Cli

def usage():
    print "Usage:"
    print "./sphirewall_cli.py [-test]"
    print "./sphirewall_cli.py <username> {<password> | -p} [-s] [-ssl] [-test]"
    print "./sphirewall_cli.py <username> {<password> | -p} <hostname> <port> [-s] [-ssl] [-test]"
    print "./sphirewall_cli.py --help [-test]\n"
 
def help(): 
    usage()
    print "Optional Arguments:"
    print "   -p               Prompt for password without echo."
    print "   -s, --save       Save authentication details for current user."
    print "   -ssl             Use SSL encryption"
    print "   -test            Use test mode.  Affects prompt display."
    print "       --help       Display this help and exit.\n"
    
def useAuthFile():
    try:
        user=[];passw=[];server=[];port=[];ssl=[];test=[]
        file = open(sppass)
    except IOError as e:
        print "No Sphirewall authentication file exists for this user\n"
        usage()
        exit()

    for line in file:
        try:
            user, passw, server, port, ssl, test = line.split(':')
        except:
            print "Sphirewall authentication file improperly formatted.\n"
            usage()
            exit()

        if ssl == "-ssl":
            sslMode = True
        else:
            sslMode = False 

        if test == "-test":
            testMode = True
        else:
            testMode = False;

        cli = Cli(user, passw, server, port, sslMode, testMode)
        file.close()
        cli.process()

def getPassword(param):
    if param == "-p":
        return getpass.getpass("Password: ")
    else:
        return param

def writeAuthFile(fileName, details):
    file = open(fileName, "w");
    s = ':';
    file.write(details)
    file.close()
    chmod(sppass, 0600)

if __name__ == "__main__":
    print "\nSphirewall Interactive Cli\n"
    
    sppass = expanduser("~/.sppass")
    
    #Find configuration:    
    if sys.argv[len(sys.argv) - 1] == "-test":
        numArgs  = len(sys.argv) -1
        testMode = True
    else:
        numArgs  = len(sys.argv)
        testMode = False

    if numArgs == 1:
        useAuthFile()
    elif numArgs == 2:
        if sys.argv[1] == "--help":
            help();
            exit();
        else:
            help();
            exit();
    elif numArgs == 3:
        # cli = Cli(sys.argv[1], sys.argv[2], "localhost", "8001", False, \
        cli = Cli(sys.argv[1], getPassword(sys.argv[2]), "localhost", "8001", \
            False, testMode)
        cli.process()
    elif numArgs == 4:
        if sys.argv[3] == "-s":
            s = ':'
            writeAuthFile(sppass, sys.argv[1] + s + sys.argv[2] + s + \
                "localhost" + s + "8001")
            cli = Cli(sys.argv[1], sys.argv[2], "localhost", "8001", \
                False, testMode)
            cli.process()
        elif sys.argv[3] == "-ssl":
            cli = Cli(sys.argv[1], getPassword(sys.argv[2]), "localhost", \
                "8003", True)
        else:
            usage();
            exit();
    elif numArgs == 5:
        if sys.argv[4] == "-s" and sys.argv[5] == "-ssl":
            s = ':'
            writeAuthFile(sppass, sys.argv[1] + s + sys.argv[2] + s + \
                "localhost" + s + "8003")
            cli = Cli(sys.argv[1], sys.argv[2], "localhost", "8003", True, \
                testMode)
            cli.process()
        else:
            cli = Cli(sys.argv[1], getPassword(sys.argv[2]), sys.argv[3], \
                sys.argv[4], False, testMode)
            cli.process()
    elif numArgs == 6:
        if sys.argv[5] == "-s":
            s = ':'
            writeAuthFile(sppass, sys.argv[1] + s + sys.argv[2] + s + \
                sys.argv[3] + s + sys.argv[4])
            cli = Cli(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], \
                False, testMode)
            cli.process()
        elif sys.argv[5] == "-ssl":
            cli = Cli(sys.argv[1], getPassword(sys.argv[2]), sys.argv[3], \
                sys.argv[4], True, testMode)
            cli.process()
        else:
            usage();
            exit();
    elif numArgs == 7:
        if sys.argv[5] == "-s" and sys.argv[6] == "-ssl":
            s = ':';
            writeAuthFile(sppass, sys.argv[1] + s + sys.argv[2] + s + \
                sys.argv[3] + s + sys.argv[4] + s + "-ssl")
            cli = Cli(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], \
                True, testMode)
            cli.process()
        else:
            usage()
            exit()
    else:
        usage()
        exit()
