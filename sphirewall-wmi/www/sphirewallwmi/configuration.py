#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

from flask.globals import request
from flask.templating import render_template
import operator
from werkzeug.utils import redirect
from sphirewallwmi import app
import service
from sphirewallwmi.api.sphirewall_api_general import QuotaInfo
from sphirewallwmi.network import text_value

@app.route("/configuration/general", methods=["POST", "GET"])
def configuration_general():
    if request.method == "POST":
        service.get_sphirewallapi().general().configuration("general:disable_remote_management",
            checkbox_value("disable_remote_management"))
        service.get_sphirewallapi().general().configuration("general:session_timeout",
            nint(request.form["session_timeout"]))
        service.get_sphirewallapi().general().configuration("general:snort_enabled",
            checkbox_value("snort_enabled"))
        service.get_sphirewallapi().general().configuration("general:snort_file",
            request.form["snort_file"])
        service.get_sphirewallapi().general().configuration("general:bandwidth_hostname",
            request.form["bandwidth_hostname"])
        service.get_sphirewallapi().general().configuration("general:bandwidth_port",
            nint(request.form["bandwidth_port"]))
        service.get_sphirewallapi().general().configuration("general:bandwidth_threshold",
            nint(request.form["threshold"]))
        service.get_sphirewallapi().general().configuration("general:bandwidth_retension",
            nint(request.form["retension"]))
        service.get_sphirewallapi().general().configuration("general:remote_user_mode",
            nint(request.form["remote_user_mode"]))
        service.get_sphirewallapi().general().configuration("general:cp_url",
            request.form["cp_url"])
        service.get_sphirewallapi().general().configuration("general:remote_authentication_group",
            nint(request.form["remote_authentication_group"]))
        service.get_sphirewallapi().general().configuration("ldapSettings:enabled",
            checkbox_value("ldap_enabled"))
        service.get_sphirewallapi().general().configuration("pamSettings:enabled",
            checkbox_value("pam_enabled"))
        service.get_sphirewallapi().general().configuration("basicHttpAuthHandler:enabled",
            checkbox_value("http_enabled"))
        service.get_sphirewallapi().general().configuration("basicHttpAuthHandler:baseurl",
            request.form["http_baseurl"])

        force_login_setting = 1 if checkbox_value("force_login") else 0
        service.get_sphirewallapi().general().advanced(key="CAPTURE_PORTAL_FORCE", value=force_login_setting)
        service.get_sphirewallapi().general().configuration("general:rpc", checkbox_value("enable_rpc"))

        service.get_sphirewallapi().general().configuration("cloud:enabled", checkbox_value("cloud_enabled"))
        service.get_sphirewallapi().general().configuration("cloud:hostname", request.form["cloud_hostname"])
        service.get_sphirewallapi().general().configuration("cloud:port", nint(request.form["cloud_port"]))
        service.get_sphirewallapi().general().configuration("cloud:key", request.form["cloud_key"])
        service.get_sphirewallapi().general().configuration("general:bandwidth_day_switch_period",
            int(request.form["bandwidth_day_switch_period"]))

        force_auth_source = request.form.getlist('force_auth_sources')
        force_auth_website_exceptions = request.form.getlist('force_auth_website_exceptions')
        service.get_sphirewallapi().firewall().force_auth_settings(force_auth_source, force_auth_website_exceptions)

    drm = service.get_sphirewallapi().general().configuration("general:disable_remote_management")
    session_timeout = service.get_sphirewallapi().general().configuration("general:session_timeout")
    snort_enabled = service.get_sphirewallapi().general().configuration("general:snort_enabled")
    snort_file = service.get_sphirewallapi().general().configuration("general:snort_file")
    ana_host = service.get_sphirewallapi().general().configuration("general:bandwidth_hostname")
    ana_port = service.get_sphirewallapi().general().configuration("general:bandwidth_port")
    ana_threshold = service.get_sphirewallapi().general().configuration("general:bandwidth_threshold")
    ana_retension = service.get_sphirewallapi().general().configuration("general:bandwidth_retension")
    remote_user_mode = service.get_sphirewallapi().general().configuration("general:remote_user_mode")
    cp_url = service.get_sphirewallapi().general().configuration("general:cp_url")
    groups = service.get_sphirewallapi().general().groups()
    remote_authentication_group = service.get_sphirewallapi().general().configuration(
        "general:remote_authentication_group")
    ldap_enabled = service.get_sphirewallapi().general().configuration("ldapSettings:enabled")
    pam_enabled = service.get_sphirewallapi().general().configuration("pamSettings:enabled")
    http_enabled = service.get_sphirewallapi().general().configuration("basicHttpAuthHandler:enabled")
    http_baseurl = service.get_sphirewallapi().general().configuration("basicHttpAuthHandler:baseurl")

    #Force authentication stuff
    force_login = True if service.get_sphirewallapi().general().advanced_value(
        key="CAPTURE_PORTAL_FORCE") == 1 else False

    enable_rpc = service.get_sphirewallapi().general().configuration("general:rpc")

    cloud_enabled = service.get_sphirewallapi().general().configuration("cloud:enabled")
    cloud_hostname = service.get_sphirewallapi().general().configuration("cloud:hostname")
    cloud_port = service.get_sphirewallapi().general().configuration("cloud:port")
    cloud_key = service.get_sphirewallapi().general().configuration("cloud:key")
    bandwidth_day_switch_period = service.get_sphirewallapi().general().configuration(
        "general:bandwidth_day_switch_period")

    return render_template("configuration_general.html", disable_remote_management=drm,
        cp_url=cp_url, session_timeout=session_timeout,
        snort_enabled=snort_enabled, snort_file=snort_file, ana_host=ana_host, ana_port=ana_port,
        ana_threshold=ana_threshold, ana_retension=ana_retension, remote_user_mode=remote_user_mode, groups=groups,
        remote_authentication_group=remote_authentication_group, ldap_enabled=ldap_enabled, pam_enabled=pam_enabled,
        http_enabled=http_enabled, http_baseurl=http_baseurl, force_login=force_login, enable_rpc=enable_rpc,
        cloud_enabled=cloud_enabled,
        cloud_hostname=cloud_hostname, cloud_port=cloud_port, cloud_key=cloud_key,
        bandwidth_day_switch_period=bandwidth_day_switch_period,
        force_auth_settings=service.get_sphirewallapi().firewall().force_auth_settings(),
        pools=service.get_sphirewallapi().firewall().aliases())


@app.route("/configuration/general/ldap", methods=["POST", "GET"])
def configuration_general_ldap():
    if request.method == "POST":
        service.get_sphirewallapi().general().configuration("ldapSettings:hostname", request.form["hostname"])
        service.get_sphirewallapi().general().configuration("ldapSettings:port", nint(request.form["port"]))
        service.get_sphirewallapi().general().configuration("ldapSettings:basedn", request.form["basedn"])
        service.get_sphirewallapi().general().configuration("ldapSettings:ldapusername", request.form["username"])
        service.get_sphirewallapi().general().configuration("ldapSettings:ldappassword", request.form["password"])
        service.get_sphirewallapi().general().ldap_sync_groups()

    return render_template("configuration_general_ldap.html",
        server=service.get_sphirewallapi().general().configuration("ldapSettings:hostname"),
        port=service.get_sphirewallapi().general().configuration("ldapSettings:port"),
        baseDn=service.get_sphirewallapi().general().configuration("ldapSettings:basedn"),
        bindUsername=service.get_sphirewallapi().general().configuration("ldapSettings:ldapusername"),
        bindPassword=service.get_sphirewallapi().general().configuration("ldapSettings:ldappassword"),
        ldap=service.get_sphirewallapi().general().ldap())


@app.route("/configuration/smtp", methods=["POST", "GET"])
def configuration_smtp():
    if request.method == "POST":
        service.get_sphirewallapi().general().configuration("smtp:hostname",
            request.form["hostname"])
        service.get_sphirewallapi().general().configuration("smtp:port",
            nint(request.form["port"]))
        service.get_sphirewallapi().general().configuration("smtp:username",
            request.form["username"])
        service.get_sphirewallapi().general().configuration("smtp:password",
            request.form["password"])
        service.get_sphirewallapi().general().configuration("smtp:default",
            request.form["default"])
        service.get_sphirewallapi().general().configuration("smtp:tls",
            checkbox_value("tls"))
        service.get_sphirewallapi().general().smtp_publish()

    hostname = service.get_sphirewallapi().general().configuration("smtp:hostname")
    port = service.get_sphirewallapi().general().configuration("smtp:port")
    username = service.get_sphirewallapi().general().configuration("smtp:username")
    password = service.get_sphirewallapi().general().configuration("smtp:password")
    default = service.get_sphirewallapi().general().configuration("smtp:default")
    tls = service.get_sphirewallapi().general().configuration("smtp:tls")

    return render_template("configuration_smtp.html", hostname=hostname, port=port, username=username, password=password
        , default=default, tls=tls)


@app.route("/configuration/smtp/test")
def configuration_smtp_test():
    service.get_sphirewallapi().general().smtp_test()
    return redirect("/configuration/smtp")


@app.route("/configuration/events", methods=["POST", "GET"])
def configuration_events():
    if request.method == "POST":
        if checkbox_value("log"):
            service.get_sphirewallapi().general().event_handler_add("event", "handler.log")
        else:
            service.get_sphirewallapi().general().event_handler_delete("event", "handler.log")

        if checkbox_value("console"):
            service.get_sphirewallapi().general().event_handler_add("event", "handler.stdout")
        else:
            service.get_sphirewallapi().general().event_handler_delete("event", "handler.stdout")

        if checkbox_value("quotas"):
            service.get_sphirewallapi().general().event_handler_add("event.userdb.quota.exceeded",
                "handler.users.disable")
        else:
            service.get_sphirewallapi().general().event_handler_delete("event.userdb.quota.exceeded",
                "handler.users.disable")

        if checkbox_value("ids"):
            service.get_sphirewallapi().general().event_handler_add("event.ids", "handler.firewall.block")
        else:
            service.get_sphirewallapi().general().event_handler_delete("event.ids", "handler.firewall.block")

        if checkbox_value("email"):
            arr = {}
            arr["email"] = request.form["emailAddr"]
            service.get_sphirewallapi().general().event_handler_add("event", "handler.email", arr)
        else:
            service.get_sphirewallapi().general().event_handler_delete("event", "handler.email")

    logging = service.get_sphirewallapi().general().event_handler_exists("event", "handler.log")
    console = service.get_sphirewallapi().general().event_handler_exists("event", "handler.stdout")
    quotas = service.get_sphirewallapi().general().event_handler_exists("event.userdb.quota.exceeded",
        "handler.users.disable")
    ids = service.get_sphirewallapi().general().event_handler_exists("event.ids", "handler.firewall.block")
    email = service.get_sphirewallapi().general().event_handler_exists("event", "handler.email")
    if email:
        emailAddr = service.get_sphirewallapi().general().event_handler("event", "handler.email")["email"]
    else:
        emailAddr = ""
    return render_template("configuration_events.html", logging=logging, console=console, quotas=quotas, ids=ids,
        email=email, emailAddr=emailAddr, handlers=service.get_sphirewallapi().general().event_handler())


@app.route("/configuration/events/delete/<eventType>/<handler>")
def configuration_events_delete(eventType, handler):
    service.get_sphirewallapi().general().event_handler_delete(eventType, handler)
    return redirect("/configuration/events")

#/configuration/events/purge
@app.route("/configuration/events/purge")
def configuration_events_purge():
    service.get_sphirewallapi().general().events_purge()
    return redirect("/status/events")


@app.route("/configuration/events/add", methods=["POST"])
def configuration_events_add():
    service.get_sphirewallapi().general().event_handler_add(request.form["eventType"], request.form["handler"])
    return redirect("/configuration/events")


@app.route("/configuration/events/manage/<eventType>/<handler>", methods=["GET", "POST"])
def configuration_events_manage(eventType, handler):
    if request.method == "POST":
        args = {}
        if handler == "handler.email":
            args["email"] = request.form["email"]
        if handler == "handler.groups.add" or handler == "handler.groups.remove":
            args["groupid"] = nint(request.form["groupid"])
        service.get_sphirewallapi().general().event_handler_add(eventType, handler, args)

    return render_template("configuration_events_manage.html",
        handler=service.get_sphirewallapi().general().event_handler(eventType, handler),
        groups=service.get_sphirewallapi().general().groups())


@app.route("/configuration/users", methods=["GET", "POST"])
def configuration_users():
    if request.method == "POST":
        service.get_sphirewallapi().general().users_add(request.form["username"], "", "")

    unsorted_users = service.get_sphirewallapi().general().users()
    users = sorted(unsorted_users, key=operator.itemgetter("username"))
    return render_template("configuration_users.html", users=users,
        groups=service.get_sphirewallapi().general().groups())


@app.route("/configuration/users/delete/<username>")
def configuration_users_delete(username):
    service.get_sphirewallapi().general().users_delete(username)
    return redirect("/configuration/users")


@app.route("/configuration/users/<username>", methods=["GET", "POST"])
def configuration_users_edit(username):
    if request.method == "POST":
        quota = QuotaInfo()
        quota.dailyQuota = checkbox_value("dailyQuota")
        quota.dailyQuotaLimit = nint(text_value("dailyQuotaLimit"))

        quota.weeklyQuota = checkbox_value("weeklyQuota")
        quota.weeklyQuotaLimit = nint(text_value("weeklyQuotaLimit"))

        quota.monthQuota = checkbox_value("monthQuota")
        quota.monthQuotaLimit = nint(text_value("monthQuotaLimit"))
        quota.monthQuotaBillingDay = nint(text_value("monthQuotaBillingDay"))
        quota.monthQuotaIsSmart = checkbox_value("monthQuotaIsSmart")

        quota.timeQuota = checkbox_value("timeQuota")
        quota.timeQuotaLimit = nint(text_value("timeQuotaLimit"))

        quota.totalQuota = checkbox_value("totalQuota")
        quota.totalQuotaLimit = nint(text_value("totalQuotaLimit"))

        #configure groups:
        group_list = request.form.getlist('groups')
        groups = []
        for id in group_list:
            groups.append(id)

        service.get_sphirewallapi().general().users_groups_merge(username, groups)
        service.get_sphirewallapi().general().users_save(username, request.form["fname"], request.form["lname"],
            request.form["email"], quota)

        if len(request.form["password"]) > 0 and request.form["password"] == request.form["repassword"]:
            service.get_sphirewallapi().general().user_set_password(username, request.form["password"])

    return render_template("configuration_users_edit.html",
        user=service.get_sphirewallapi().general().users(username),
        groups=service.get_sphirewallapi().general().groups())


@app.route("/configuration/users/<username>/disable")
def configuration_users_disable(username):
    service.get_sphirewallapi().general().user_disable(username)
    return redirect("/configuration/users/" + username)


@app.route("/configuration/users/<username>/enable")
def configuration_users_enable(username):
    service.get_sphirewallapi().general().user_enable(username)
    return redirect("/configuration/users/" + username)


@app.route("/configuration/users/<username>/groups/remove/<groupid>", methods=["GET", "POST"])
def configuration_users_remove_group(username, groupid):
    service.get_sphirewallapi().general().user_groups_remove(username, groupid)
    return redirect("/configuration/users/" + username)


@app.route("/configuration/users/<username>/groups/add", methods=["POST"])
def configuration_users_add_group(username):
    service.get_sphirewallapi().general().user_groups_add(username, request.form["group"])
    return redirect("/configuration/users/" + username)


@app.route("/configuration/groups", methods=["GET", "POST"])
def configuration_groups():
    if request.method == "POST":
        service.get_sphirewallapi().general().group_add(request.form["name"])

    unsorted_groups = service.get_sphirewallapi().general().groups()
    groups = sorted(unsorted_groups, key=operator.itemgetter("name"))
    return render_template("configuration_groups.html", groups=groups)


@app.route("/configuration/groups/delete/<id>")
def configuration_groups_delete(id):
    service.get_sphirewallapi().general().groups_delete(id)
    return redirect("/configuration/groups")


@app.route("/configuration/groups/<id>", methods=["GET", "POST"])
def configuration_groups_edit(id):
    if request.method == "POST":
        quota = QuotaInfo()

        quota.dailyQuota = checkbox_value("dailyQuota")
        quota.dailyQuotaLimit = nint(text_value("dailyQuotaLimit"))

        quota.weeklyQuota = checkbox_value("weeklyQuota")
        quota.weeklyQuotaLimit = nint(text_value("weeklyQuotaLimit"))

        quota.monthQuota = checkbox_value("monthQuota")
        quota.monthQuotaLimit = nint(text_value("monthQuotaLimit"))
        quota.monthQuotaBillingDay = nint(text_value("monthQuotaBillingDay"))
        quota.monthQuotaIsSmart = checkbox_value("monthQuotaIsSmart")

        quota.timeQuota = checkbox_value("timeQuota")
        quota.timeQuotaLimit = nint(text_value("timeQuotaLimit"))

        quota.totalQuota = checkbox_value("totalQuota")
        quota.totalQuotaLimit = nint(text_value("totalQuotaLimit"))

        service.get_sphirewallapi().general().groups_save(id, request.form["manager"], checkbox_value("mui"),
            request.form["desc"], quota)

    return render_template("configuration_groups_edit.html",
        group=service.get_sphirewallapi().general().groups(id))


@app.route("/configuration/advanced", methods=["GET", "POST"])
def configuration_advanced():
    if request.method == "POST":
        for fieldname, value in request.form.items():
            service.get_sphirewallapi().general().advanced(fieldname, nint(value))

    return render_template("configuration_advanced.html", vars=service.get_sphirewallapi().general().advanced())


@app.route("/configuration/quotas", methods=["GET", "POST"])
def configuration_quotas():
    if request.method == "POST":
        dailyQuotaLimit = nint(text_value("dailyQuotaLimit"))
        weeklyQuotaLimit = nint(text_value("weeklyQuotaLimit"))
        monthQuotaBillingDay = nint(text_value("monthQuotaBillingDay"))
        monthQuotaLimit = nint(text_value("monthQuotaLimit"))

        service.get_sphirewallapi().general().quotas_set(checkbox_value("dailyQuota"),
            dailyQuotaLimit, checkbox_value("weeklyQuota"),
            weeklyQuotaLimit, checkbox_value("monthQuota"),
            monthQuotaBillingDay,
            checkbox_value("monthQuotaIsSmart"), monthQuotaLimit)

    return render_template("configuration_quotas.html", settings=service.get_sphirewallapi().general().quotas())


@app.route("/configuration/watchdog", methods=["GET", "POST"])
def configuration_watchdog():
    if request.method == "POST":
        service.get_sphirewallapi().general().configuration("watchdog:monitorGoogle", checkbox_value("monitorGoogle"))

    monitorGoogle = service.get_sphirewallapi().general().configuration("watchdog:monitorGoogle")
    return render_template("configuration_watchdog.html", monitorGoogle=monitorGoogle)


def nint(value):
    if value is None or len(value) == 0:
        return 0
    else:
        return int(value)


def checkbox_value(key):
    if key in request.form:
        return True
    return False

