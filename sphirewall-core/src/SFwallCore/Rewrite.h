/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REWRITE_RULE_H
#define REWRITE_RULE_H

#include "Core/Logger.h"
#include <list>
#include "SFwallCore/Alias.h"

class ConfigurationManager;
namespace SFwallCore {
	class Packet;
	class Connection;

	class RewriteRule {
		public:
			virtual ~RewriteRule(){}
			virtual int type() = 0;
			virtual void rewrite(Connection* conn, Packet* packet) = 0;
			virtual bool match(Packet* packet){
				return false;
			}
	};

	class HttpRedirectRewrite : public RewriteRule {
		public:
			HttpRedirectRewrite(std::string url) : 
				url(url){}

			HttpRedirectRewrite(std::string id, std::string url) : 
				id(id), url(url){}
			~HttpRedirectRewrite(){}
			int type();
			void rewrite(Connection* conn, Packet* packet);
			bool match(Packet* packet);

		private:
			std::string id;
			std::string url;
	};


	//Rewrite rules are temporary rules for notifying users to something, they are populated by the event system
	class RewriteRequired{
		public:
			RewriteRequired(std::string id, std::string url) :
				id(id),url(url){}

			bool match(Connection* conn, Packet* packet);	
			RewriteRule* get();	
			std::string getId(){
				return id;
			}
			
			std::string getUrl(){
				return url;
			}
		private:
			std::string url;	
			std::string id;
	};
	
	class RewriteEngine {
		public:
			RewriteEngine(ConfigurationManager* config, SLogger::LogContext* logger, AliasDb* aliases); 

			RewriteRule* process(Connection* conn, Packet* packet);		
			void clear(RewriteRequired* rewrite);	
			RewriteRequired* get(std::string id);
			void add(RewriteRequired*);			

			static int CAPTURE_PORTAL_MODE;
			static int CAPTURE_PORTAL_FORCE;
			static int CAPTURE_PORTAL_REQUIRES_HOSTNAME;

			std::list<Alias*>& getForceAuthRanges(){
				return forceAuthSources;
			}

			std::list<Alias*>& getForceAuthWebsiteExceptions(){
				return forceAuthWebsiteExceptions;
			}

			void save();
			void load();
		private:
			ConfigurationManager* config;
			SLogger::LogContext* logger;
			AliasDb* aliases;
			std::list<RewriteRequired*> rewrites;

			//Force auth rules:
			std::list<Alias*> forceAuthSources;
			std::list<Alias*> forceAuthWebsiteExceptions;
	};
};
#endif
