/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

#include "Event.h"
#include "System.h"
#include "Auth/UserDb.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Rewrite.h"

#include "Utils/EmailSender.h"

class AddGroupEventHandler : public EventHandler {
public:

        AddGroupEventHandler* Clone() {
                return new AddGroupEventHandler(*this);
        }

        AddGroupEventHandler() {
		this->groupId = -1;	
	}
        AddGroupEventHandler(const AddGroupEventHandler&h) : EventHandler(h) {}
        void handle(Event* event) {
                if(this->groupId == -1){
                        return;
                }

                Group* targetGroup = System::getInstance()->getGroupDb()->getGroup(this->groupId);
                if (targetGroup) {
                        Param* param = event->params.get("username");
                        if(!param){
				param = event->params.get("user");	
                        }

			if(param){
				UserDb* userDb = System::getInstance()->getUserDb();
				User* user = userDb->getUser(param->string());
				if(user){
					user->addGroup(targetGroup);
					System::getInstance()->getUserDb()->save();
				}
			}
		}
	}

	string name() {
		return "Add user to group";
	}

	int getId() {
		return 6;
	}

	std::string key() const {
		return "handler.groups.add";
	}

	int getGroupId() const {
		return groupId;
	}

	void setGroupId(int id){
		this->groupId = id;	
	}
private:
	int groupId;
};

class RemoveGroupEventHandler : public EventHandler {
	public:

		RemoveGroupEventHandler* Clone() {
			return new RemoveGroupEventHandler(*this);
		}

		RemoveGroupEventHandler() {
			this->groupId = -1;
		}

		RemoveGroupEventHandler(const RemoveGroupEventHandler&h) : EventHandler(h) {
		}

		void handle(Event* event) {
			if(this->groupId == -1){
				return;
			}

			Group* targetGroup = System::getInstance()->getGroupDb()->getGroup(this->groupId);
			if (targetGroup) {
				Param* param = event->params.get("username");
				if(!param){
					param = event->params.get("user");
				}

				if(param){
					UserDb* userDb = System::getInstance()->getUserDb();
					User* user = userDb->getUser(param->string());
					if(user){
						user->removeGroup(targetGroup);
						System::getInstance()->getUserDb()->save();
					}
				}
			}
		}

		string name() {
			return "Remove user from group";
		}

		int getId() {
			return 7;
		}

		std::string key() const {
			return "handler.groups.remove";
		}

		int getGroupId() const {
			return groupId;
		}

		void setGroupId(int id){
			this->groupId = id;
		}

	private:
		int groupId;
};

class EmailEventHandler : public EventHandler {
	public:

		EmailEventHandler* Clone() {
			return new EmailEventHandler(*this);
		}

		EmailEventHandler() {
		}

		EmailEventHandler(const EmailEventHandler &h) : EventHandler(h) {
		}

		void handle(Event* event) {
			EmailSender* emailService = System::getInstance()->getEmailSender();
			if(email.size() == 0){
				email = emailService->getDefaultEmail();
			}

			emailService->send(email, "Sphirewall Event Alert", event->toString());
		}

		string name() {
			return "Email event handler";
		}

		int getId() {
			return 5;
		}

		std::string key() const {
			return "handler.email";
		}

		void setEmail(std::string email){
			this->email = email;
		}

		std::string getEmail(){
			return email;
		}
	private:
		std::string email;
};

class DisableUserEventHandler : public EventHandler {
	public:

		DisableUserEventHandler* Clone() {
			return new DisableUserEventHandler(*this);
		}

		DisableUserEventHandler() {
		}

		DisableUserEventHandler(const DisableUserEventHandler& h) : EventHandler(h) {
		}

		void handle(Event* event) {
                        Param* param = event->params.get("username");
                        if(!param){
                                param = event->params.get("user");
                        }

			if(param){
				UserDb* userDb = System::getInstance()->getUserDb();
				User* user = userDb->getUser(param->string());
				if(user){
					userDb->disableUser(user);	
				}
			}
		}

		string name() {
			return "Disable user";
		}

		int getId() {
			return 4;
		}

		std::string key() const {
			return "handler.users.disable";
		}

};

class DeleteUserEventHandler : public EventHandler {
	public:

		DeleteUserEventHandler* Clone() {
			return new DeleteUserEventHandler(*this);
		}

		DeleteUserEventHandler() {
		}

		DeleteUserEventHandler(const DeleteUserEventHandler& h) : EventHandler(h) {
		}

		void handle(Event* event) {
                        Param* param = event->params.get("username");
                        if(!param){
                                param = event->params.get("user");
                        }

			if(param){
				UserDb* userDb = System::getInstance()->getUserDb();
				User* user = userDb->getUser(param->string());
				if(user){
					userDb->delUser(user);	
				}
			}
		}

		string name() {
			return "Delete user";
		}

		int getId() {
			return 5;
		}

		std::string key() const {
			return "handler.users.delete";
		}

};


class LogEventHandler : public EventHandler {
	public:

		LogEventHandler* Clone() {
			return new LogEventHandler(*this);
		}

		LogEventHandler() {
		}

		LogEventHandler(const LogEventHandler& h) : EventHandler(h) {
		}

		void handle(Event* event) {
			std::stringstream ss;
			ss << "Event Caught: " + event->key;
			ss << " data = [" << event->params.toString() << "]";
			System::getInstance()->logger.log(SLogger::INFO, ss.str());
		}

		string name() {
			return "Log Event Handler";
		}

		int getId() {
			return 1;
		}

		std::string key() const {
			return "handler.log";
		}
};

class StdOutEventHandler : public EventHandler {
	public:

		StdOutEventHandler* Clone() {
			return new StdOutEventHandler(*this);
		}

		StdOutEventHandler() {
		}

		StdOutEventHandler(const StdOutEventHandler& h) : EventHandler(h) {
		}

		void handle(Event* event) {
			std::stringstream ss;
			ss << "Event Caught: " + event->key;
			ss << " data = [" << event->params.toString() << "]";
			std::cout << ss << std::endl;
		}

		string name() {
			return "Stdout Event Handler";
		}

		int getId() {
			return 2;
		}

		std::string key() const {
			return "handler.stdout";
		}

};

class FirewallBlockSourceAddressHandler : public EventHandler {
	public:

		FirewallBlockSourceAddressHandler() {
		}

		FirewallBlockSourceAddressHandler(const FirewallBlockSourceAddressHandler& h) : EventHandler(h) {
		}

		FirewallBlockSourceAddressHandler* Clone() {
			return new FirewallBlockSourceAddressHandler(*this);
		}

		void handle(Event* event) {
			Param* param = event->params.get("source");
			if(param){
				System::getInstance()->getFirewall()->acls->addToDenyList(IP4Addr::stringToIP4Addr(param->string()));
			}
		}

		string name() {
			return "Block source address";
		}

		int getId() {
			return 3;
		}

		std::string key() const {
			return "handler.firewall.block";
		}

};

class RedirectToUrl : public EventHandler {
	public:

		RedirectToUrl() {
		}

		RedirectToUrl(const RedirectToUrl& h) : EventHandler(h) {
		}

		RedirectToUrl* Clone() {
			return new RedirectToUrl(*this);
		}

		void handle(Event* event) {
			SFwallCore::RewriteRequired* rewrite = new SFwallCore::RewriteRequired(StringUtils::genRandom(), url);
			System::getInstance()->getFirewall()->rewriteEngine->add(rewrite);
		}

		string name() {
			return "Rewrite http requests";
		}

		int getId() {
			return 4;
		}

		std::string key() const {
			return "handler.firewall.rewrite";
		}

		void setUrl(std::string url){
			this->url = url;
		}

		std::string getUrl(){
			return this->url;
		}
	private:
		std::string url;
};

#endif
