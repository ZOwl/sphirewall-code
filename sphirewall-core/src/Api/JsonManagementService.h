/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef JSON_MANAGEMENT_SERVICE
#define JSON_MANAGEMENT_SERVICE

#include <list>
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/ConnTracker.h"
#include "Auth/UserDb.h"

class AnalyticsClient;
class InterfaceManager;
class Config;
class SysMonitor;
class EventDb;
class IDS;
class DNSConfig;
class GroupDb;
class SessionDb;
class LoggingConfiguration;
class HostDiscoveryService;
class AuthManager;
class Dhcp3ConfigurationManager;
class ConnectionManager;
class OpenvpnManager;
class AuthenticationManager;

namespace SFwallCore {
	class HttpUrlApplicationFilter;
};

class ServiceNotAvailableException : public std::exception {
	public:

		ServiceNotAvailableException(std::string message) {
			w = message;
		}

		~ServiceNotAvailableException() throw () {
		}

		virtual const char* what() const throw () {
			return w.c_str();
		}

		std::string message() {
			return w;
		}
	private:
		std::string w;
};

class DelegateGeneralException : public std::exception {
	public:

		DelegateGeneralException(std::string message) {
			w = message;
		}

		~DelegateGeneralException() throw () {
		}

		virtual const char* what() const throw () {
			return w.c_str();
		}

		std::string message() {
			return w;
		}

	private:
		std::string w;
};

class DelegateNotFoundException : public std::exception {
	public:

		DelegateNotFoundException(std::string message) {
			w = message;
		}

		~DelegateNotFoundException() throw () {
		}

		virtual const char* what() const throw () {
			return w.c_str();
		}

		std::string message() {
			return w;
		}

	private:
		std::string w;
};

class AuthenticationException: public std::exception {
	public:

		AuthenticationException(std::string message) {
			w = message;
		}

		~AuthenticationException() throw () {
		}

		virtual const char* what() const throw () {
			return w.c_str();
		}

		std::string message() {
			return w;
		}

	private:
		std::string w;
};


class Delegate {
	public:
		virtual std::string rexpression() = 0;
		virtual JSONObject process(std::string uri, JSONObject input) = 0;
		bool matches(std::string uri);
};

class NetworkSettingsDelegate : public virtual Delegate {
	public:
		NetworkSettingsDelegate(InterfaceManager* interfaceManager, DNSConfig* dnsConfig, HostDiscoveryService* arpTable, Dhcp3ConfigurationManager* dhcp) :
			interfaceManager(interfaceManager), dnsConfig(dnsConfig), arp(arpTable), dhcpConfiguration(dhcp) {
				eventDb = NULL;
			}

		JSONObject process(std::string uri, JSONObject object);

		std::string rexpression() {
			return "network/(.*)";
		}

		void setConnectionManager(ConnectionManager* connectionManager){
			this->connectionManager = connectionManager;
		}

		void setEventDb(EventDb* eventDb){
			this->eventDb = eventDb;
		}		
		
		void setOpenvpnManager(OpenvpnManager* o){
			this->openvpn =o;
		}	
	
	private:
		InterfaceManager* interfaceManager;
		DNSConfig* dnsConfig;
		HostDiscoveryService* arp;
		Dhcp3ConfigurationManager* dhcpConfiguration;
		ConnectionManager* connectionManager;
		EventDb* eventDb;	
		OpenvpnManager* openvpn;		

		JSONObject devices_list(JSONObject o);
		JSONObject devices_publish(JSONObject);
		JSONObject devices_set(JSONObject);
		JSONObject devices_dynamicdns_set(JSONObject);
		JSONObject dns_get(JSONObject);
		JSONObject dns_set(JSONObject);
		JSONObject devices_synced(JSONObject obj);
		JSONObject routes_list(JSONObject);
		JSONObject routes_add(JSONObject);
		JSONObject routes_del(JSONObject);
		JSONObject arp_list(JSONObject);
		JSONObject arp_size();

		//Dhcp control:
		JSONObject dhcp_globals(JSONObject);
		JSONObject dhcp_globals_set(JSONObject);
		JSONObject dhcp_publish(JSONObject);

		JSONObject dhcp_start(JSONObject);
		JSONObject dhcp_stop(JSONObject);
		JSONObject dhcp_running(JSONObject);
		JSONObject dhcp_log(JSONObject);
		JSONObject dhcp_static_add(JSONObject o);
		JSONObject dhcp_static_del(JSONObject o);
		JSONObject dhcp_static(JSONObject o);

		JSONObject devices_alias_list(JSONObject obj);
		JSONObject devices_alias_delete(JSONObject obj);
		JSONObject devices_alias_add(JSONObject obj);
		JSONObject devices_up(JSONObject obj);
		JSONObject devices_toggle(JSONObject obj);
		JSONObject devices_toggledhcp(JSONObject obj);

		JSONObject connections_list(JSONObject obj);
		JSONObject connectons_add(JSONObject obj);
		JSONObject connections_del(JSONObject obj);
		JSONObject connections_save(JSONObject obj);
		JSONObject connections_connect(JSONObject o);
		JSONObject connections_disconnect(JSONObject o);

		JSONObject openvpn_start(JSONObject obj);
		JSONObject openvpn_stop(JSONObject obj);
		JSONObject openvpn_publish(JSONObject obj);
		JSONObject openvpn_status(JSONObject obj);
		JSONObject openvpn_log(JSONObject obl);
		JSONObject openvpn_clients_list(JSONObject obl);
		JSONObject openvpn_clients_add(JSONObject obl);
		JSONObject openvpn_clients_delete(JSONObject obl);
		JSONObject openvpn_install(JSONObject obj);
		JSONObject openvpn_uninstall(JSONObject obj);
		JSONObject openvpn_checkinstall(JSONObject obj);
		JSONObject openvpn_client_static(JSONObject obj);
		JSONObject openvpn_instances_list(JSONObject obj);
		JSONObject openvpn_instance_create(JSONObject obj);
		JSONObject openvpn_instance_remove(JSONObject obj);
		JSONObject openvpn_instance_setoption(JSONObject);
		JSONObject openvpn_daemonexists(JSONObject obj);
		JSONObject wireless_start(JSONObject obj);
		JSONObject wireless_stop(JSONObject obj);
		JSONObject wireless_online(JSONObject obj);
		JSONObject devices_delete(JSONObject obj);		

		JSONObject dyndns();
		JSONObject dyndns_set(JSONObject obj);	
		void devices_serialize_configured(JSONObject& interface, NetDeviceConfiguration* configuredDevice);
};

class AnalyticsDelegate : public virtual Delegate {
	public:

		AnalyticsDelegate(AnalyticsClient* client) :
			client(client) {
			}

		JSONObject process(std::string uri, JSONObject object);

		std::string rexpression() {
			return "analytics/(.*)";
		}

		JSONObject pass_through(JSONObject object);

	private:
		AnalyticsClient* client;
};

class IdsDelegate : public virtual Delegate {
	public:

		IdsDelegate(IDS* ids) :
			ids(ids) {
				eventDb = NULL;
			}

		JSONObject process(std::string uri, JSONObject object);

		std::string rexpression() {
			return "ids/(.*)";
		}

		JSONObject exceptions_list(JSONObject object);
		JSONObject exceptions_add(JSONObject object);
		JSONObject exceptions_del(JSONObject object);

		void setEventDb(EventDb* eventDb){
			this->eventDb = eventDb;
		}

	private:
		IDS* ids;
		EventDb* eventDb;
};

class FirewallDelegate : public virtual Delegate {
	public:
		FirewallDelegate(SFwallCore::Firewall* firewall, InterfaceManager* interfaceManager, GroupDb* groupDb) :
			firewall(firewall), interfaceManager(interfaceManager), groupDb(groupDb) {
				this->httpApplicationFilter = NULL;
				this->eventDb = NULL;
			}

		JSONObject process(std::string uri, JSONObject object);

		std::string rexpression() {
			return "firewall/(.*)";
		}

		void setHttpApplicationFilter(SFwallCore::HttpUrlApplicationFilter* filter) {
			this->httpApplicationFilter = filter;
		}

		SFwallCore::HttpUrlApplicationFilter* getHttpApplicationFilter(){
			if(this->httpApplicationFilter == NULL){
				this->httpApplicationFilter = firewall->httpFilter;
			}

			return this->httpApplicationFilter;
		}

		void setEventDb(EventDb* eventDb){
			this->eventDb = eventDb;
		}
	private:
		SFwallCore::Firewall* firewall;
		InterfaceManager* interfaceManager;
		GroupDb* groupDb;
		SFwallCore::HttpUrlApplicationFilter* httpApplicationFilter;
		EventDb* eventDb;

		JSONObject aliases_list(JSONObject);
		JSONObject aliases_add(JSONObject);
		JSONObject aliases_del(JSONObject);
		JSONObject aliases_del_bulk(JSONObject);
		JSONObject aliases_alias_list(JSONObject);
		JSONObject aliases_alias_add(JSONObject);
		JSONObject aliases_get(JSONObject);
		JSONObject aliases_alias_del(JSONObject);
		JSONObject aliases_load(JSONObject obj);

		JSONObject tracker_list(JSONObject);
		JSONObject tracker_terminate(JSONObject obj);
		JSONObject tracker_size();
		JSONObject blocklist_list(JSONObject);
		JSONObject blocklist_del(JSONObject);
		JSONObject blocklist_add(JSONObject);
		JSONObject blocklist_getLists(JSONObject);
		JSONObject blocklist_addList(JSONObject);
		JSONObject blocklist_delList(JSONObject);
		JSONObject blocklistList_addList(JSONObject);
		JSONObject blocklistList_delList(JSONObject);
		JSONObject acls_filter_up(JSONObject);
		JSONObject acls_filter_up_pos(JSONObject);
		JSONObject acls_filter_down(JSONObject);
		JSONObject acls_filter_down_pos(JSONObject);
		JSONObject acls_filter_delete(JSONObject);
		JSONObject acls_filter_delete_pos(JSONObject);
		JSONObject acls_nat_delete(JSONObject);
		JSONObject acls_list(JSONObject);
		JSONObject acls_add(JSONObject);
		JSONObject acls_add_trafficshaper(JSONObject);
		JSONObject acls_list_trafficshaper(JSONObject args);
		JSONObject acls_list_del_trafficshaper(JSONObject args);

		JSONObject acls_enable(JSONObject obj);
		JSONObject acls_disable(JSONObject obj);
		JSONObject acls_status(JSONObject obj);

		JSONObject acls_webfilter_rules_list(JSONObject obj);
		JSONObject acls_webfilter_lists_list(JSONObject obj);
		JSONObject acls_webfilter_lists_add(JSONObject obj);
		JSONObject acls_webfilter_rules_add(JSONObject obj);
		JSONObject acls_webfilter_lists_remove(JSONObject obj);
		JSONObject acls_webfilter_rules_remove(JSONObject obj);
		JSONObject acls_webfilter_lists_addurl(JSONObject obj);
		JSONObject acls_webfilter_lists_removeurl(JSONObject obj);
		JSONObject deny_list(JSONObject obj);
		JSONObject deny_del(JSONObject obj);
		JSONObject rewrite_remove(JSONObject obj);
		JSONObject acls_webfilter_rules_movedown(JSONObject obj);
		JSONObject acls_webfilter_rules_moveup(JSONObject obj);
		JSONObject acls_webfilter_rules_deleteall(JSONObject obj);

		JSONObject acls_webfilter_rules_enable(JSONObject obj);
		JSONObject acls_webfilter_rules_disable(JSONObject obj);
		void serializeRule(JSONObject& rule, SFwallCore::Rule* target);
		JSONObject acls_rewrite_forceauthranges();
		JSONObject acls_rewrite_forceauthranges_set(JSONObject obj);
};

class AuthDelegate : public virtual Delegate {
	public:

		AuthDelegate(GroupDb* groupDb, UserDb* userDb, SFwallCore::Firewall* firewall, SessionDb* sessionDb, HostDiscoveryService* arp, AuthenticationManager* authManager) :
			groupDb(groupDb), userDb(userDb), firewall(firewall), sessionDb(sessionDb), arp(arp), authManager(authManager) {
				this->eventDb = NULL;
			}

		JSONObject process(std::string uri, JSONObject object);

		std::string rexpression() {
			return "auth/(.*)";
		}

		void setEventDb(EventDb* eventDb){
			this->eventDb = eventDb;
		}

	private:
		GroupDb* groupDb;
		UserDb* userDb;
		SFwallCore::Firewall* firewall;
		SessionDb* sessionDb;
		HostDiscoveryService* arp;
		AuthenticationManager* authManager;
		EventDb* eventDb;

		JSONObject groups_list(JSONObject);
		JSONObject groups_create(JSONObject);
		JSONObject groups_get(JSONObject);
		JSONObject groups_del(JSONObject);
		JSONObject groups_save(JSONObject);
		JSONObject users_list(JSONObject);
		JSONObject users_add(JSONObject);
		JSONObject users_del(JSONObject);
		JSONObject users_get(JSONObject);
		JSONObject users_save(JSONObject);
		JSONObject user_setpassword(JSONObject);
		JSONObject users_groups_add(JSONObject);
		JSONObject users_groups_del(JSONObject);
		JSONObject users_enable(JSONObject);
		JSONObject users_disable(JSONObject);
		JSONObject sessions_list(JSONObject);
		JSONObject login(JSONObject);
		JSONObject logout(JSONObject);

		JSONObject users_quotas_list(JSONObject args);
		JSONObject users_quotas_set(JSONObject args);
		JSONObject users_quotas_del(JSONObject args);
		JSONObject groups_quotas_list(JSONObject args);
		JSONObject groups_quotas_set(JSONObject args);
		JSONObject groups_quotas_del(JSONObject args);
		JSONObject groups_mergeoveruser(JSONObject);				

		JSONObject settings_ldap(JSONObject args);
		JSONObject settings_ldap_set(JSONObject args);
		JSONObject ldap();
		JSONObject ldap_sync();

		JSONObject defaultpasswordset();
		JSONObject createsession(JSONObject input);
		JSONObject groups_create_bulk(JSONObject args);
};

class EventDelegate : public virtual Delegate {
	public:

		EventDelegate(EventDb* eventDb) :
			eventDb(eventDb) {
			}

		JSONObject process(std::string uri, JSONObject object);

		std::string rexpression() {
			return "events/(.*)";
		}

		JSONObject list(JSONObject object);
		JSONObject size();
		JSONObject config_handlers_available(JSONObject object);
		JSONObject config_type_available(JSONObject);
		JSONObject config_add(JSONObject object);
		JSONObject config_del(JSONObject object);
		JSONObject config_list(JSONObject object);
		JSONObject config_handlers_config(JSONObject object);
		JSONObject config_handlers_config_list(JSONObject);
		JSONObject purge(JSONObject);
	private:
		EventDb* eventDb;
};

class GeneralDelegate : public virtual Delegate {
	public:

		GeneralDelegate(Config* config, SysMonitor* sysMonitor, LoggingConfiguration* loggingConfiguration, AuthenticationManager* authenticationManager) :
			config(config), sysMonitor(sysMonitor), loggingConfiguration(loggingConfiguration), authenticationManager(authenticationManager) {
				this->eventDb = NULL;
			}

		GeneralDelegate(Config* config) :
			config(config) {
				this->eventDb = NULL;
			}

		JSONObject process(std::string uri, JSONObject object);

		std::string rexpression() {
			return "general/(.*)";
		}

		void setEventDb(EventDb* eventDb){
			this->eventDb = eventDb;
		}
	private:
		Config* config;
		SysMonitor* sysMonitor;
		LoggingConfiguration* loggingConfiguration;
		EventDb* eventDb;
		AuthenticationManager* authenticationManager;		

		JSONObject config_dump();
		JSONObject config_get(JSONObject object);
		JSONObject config_set(JSONObject object);
		JSONObject runtime_list(JSONObject object);
		JSONObject runtime_get(JSONObject object);
		JSONObject runtime_set(JSONObject object);
		JSONObject metrics(JSONObject object);
		JSONObject version(JSONObject o);
		JSONObject logging_list(JSONObject o);
		JSONObject logging_set(JSONObject o);
		JSONObject ldap_testconnection(JSONObject o);
		JSONObject smtp_testconnection(JSONObject object);
		JSONObject smtp_publish(JSONObject object);
		JSONObject cloud_connected();
		JSONObject rpc(JSONObject);
		JSONObject quotas();
		JSONObject quotas_set(JSONObject obj);
		JSONObject logs(JSONObject obj);		
		JSONObject config_set_bulk(JSONObject obj);
		JSONObject info(JSONObject obj);
};

class ApiSession {
	public:
		ApiSession(User* user, std::string uuid) : user(user), uuid(uuid) {
			lastSeenTime = time(NULL);
		}

		User* user;
		int lastSeenTime;	
		string uuid;
};

class JsonManagementService : public DeleteUserListener {
	public:
		JsonManagementService();

		void initDelegate();
		std::string process(std::string input);


		void setConfig(Config* config) {
			this->config = config;
		}

		void setSysMonitor(SysMonitor* sysMonitor) {
			this->sysMonitor = sysMonitor;
		}

		void setEventDb(EventDb* eventDb) {
			this->eventDb = eventDb;
		}

		void setIds(IDS* ids) {
			this->ids = ids;
		}

		void setInterfaceManager(InterfaceManager* i) {
			this->interfaceManager = i;
		}

		void setDnsConfig(DNSConfig* dnsConfig) {
			this->dnsConfig = dnsConfig;
		}

		void setFirewall(SFwallCore::Firewall* firewall) {
			this->firewall = firewall;
		}

		void setGroupDb(GroupDb* groupDb) {
			this->groupDb = groupDb;
		}

		void setUserDb(UserDb* userDb) {
			this->userDb = userDb;
			this->userDb->registerDeleteListener(this);
		}

		void setSessionDb(SessionDb* sessionDb) {
			this->sessionDb = sessionDb;
		}

		void setHostDiscoveryService(HostDiscoveryService* arpTable) {
			this->arpTable = arpTable;
		}

		void registerDelegate(Delegate* d) {
			registeredDelegates.push_back(d);
		}

		void ignoreAuthentication(bool value) {
			ignoreAuth = value;
		}

		void setLoggingConfiguration(LoggingConfiguration* loggingConfiguration) {
			this->loggingConfiguration = loggingConfiguration;
		}

		void setDhcp(Dhcp3ConfigurationManager* dhcpConfigurationManager) {
			this->dhcpConfigurationManager = dhcpConfigurationManager;
		}

		void setOpenvpn(OpenvpnManager* target) {
			this->openvpn= target;
		}

		void setConnectionManager(ConnectionManager* connectionManager){
			this->connectionManager = connectionManager;
		}

		void setAuthenticationManager(AuthenticationManager* authenticationManager){
			this->authenticationManager = authenticationManager;
		}

		void handleDeleteUser(User* user);
	private:
		std::list<Delegate*> registeredDelegates;
		std::list<ApiSession*> sessions;

		Config* config;
		SysMonitor* sysMonitor;
		EventDb* eventDb;
		IDS* ids;
		InterfaceManager* interfaceManager;
		DNSConfig* dnsConfig;
		SFwallCore::Firewall* firewall;
		GroupDb* groupDb;
		UserDb* userDb;
		SessionDb* sessionDb;
		HostDiscoveryService* arpTable;
		LoggingConfiguration* loggingConfiguration;
		Dhcp3ConfigurationManager* dhcpConfigurationManager;
		OpenvpnManager* openvpn;
		bool ignoreAuth;
		ConnectionManager* connectionManager;
		AuthenticationManager* authenticationManager;

		ApiSession* getSession(std::string token);
		bool isValidSession(std::string token);
		std::string createSession(User* user);
		std::set<std::string> requestsWithoutAuth;

		//Internal request handlers:
		std::string handleAuthentication(JSONObject& obj);
		std::string handleRequest(std::string, JSONObject& obj);
		Lock* lock;

		bool noSessionAuth(string requesturi, string token);
		bool noAuthRequired(std::string requestUrl);

};

#endif
