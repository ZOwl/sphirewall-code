/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <poll.h>
#include "NetworkChangeNotifier.h"

using namespace std;

int NetworkChangeNotifier::pollfirst(){
	struct nlmsghdr* n;
	struct rtgenmsg *gen;
	uint8_t req[1024];
	
	memset(&req, 0, sizeof(req));
	n = (struct nlmsghdr*) req;
	n->nlmsg_len = NLMSG_LENGTH(sizeof(struct rtgenmsg));
	n->nlmsg_type = RTM_GETLINK;
	n->nlmsg_flags = NLM_F_ROOT|NLM_F_MATCH|NLM_F_REQUEST;
	n->nlmsg_pid = 0;

	gen = (struct rtgenmsg*) NLMSG_DATA(n);
	memset(gen, 0, sizeof(struct rtgenmsg));
	gen->rtgen_family = AF_UNSPEC;

	if (send(fd, n, n->nlmsg_len, 0) < 0) {
		return -1;
	}
}


int NetworkChangeNotifier::connect(){
	const int on = 1;
	struct sockaddr_nl addr;

	if ((fd = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE)) < 0) {
		return -1;
	}

	memset(&addr, 0, sizeof(addr));
	addr.nl_family = AF_NETLINK;
	addr.nl_groups = RTMGRP_LINK|RTMGRP_IPV4_IFADDR|RTMGRP_IPV6_IFADDR;
	addr.nl_pid = getpid();

	if (bind(fd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		return -1;
	}

	if (setsockopt(fd, SOL_SOCKET, SO_PASSCRED, &on, sizeof(on)) < 0) {
		return -1;
	}
	return 0;
}

int NetworkChangeNotifier::waitForEvent(){
	struct pollfd* fds = (struct pollfd*) malloc(sizeof(struct pollfd));
        fds[0].events = POLLIN;
        fds[0].fd = fd;
	return poll(fds, 1,-1);
}

int NetworkChangeNotifier::handleEvent(std::list<NetworkChange>& ret){
	int len;
	char buf[8192];
	struct nlmsghdr *nh = (struct nlmsghdr *) &buf;
	nh->nlmsg_pid = getpid();
	nh->nlmsg_flags = 0;
	nh->nlmsg_len = 8192;

	struct sockaddr_nl sa;
	memset(&sa, 0, sizeof(sa));
	sa.nl_family = AF_NETLINK;

	struct iovec iov = {(void *) nh, nh->nlmsg_len};
	struct msghdr msg = { (void *)&sa, sizeof(sa), &iov, 1, NULL, 0, 0 };

	len = recvmsg(fd, &msg, 0);
	if(len < 0){
		return -1;
	}
	while (NLMSG_OK(nh, len)) {
		if (nh->nlmsg_type == RTM_NEWLINK || nh->nlmsg_type == RTM_DELLINK || nh->nlmsg_type == RTM_GETLINK) {
			struct ifinfomsg* info =(struct ifinfomsg*) NLMSG_DATA(nh);
			struct rtattr *a = IFLA_RTA(info);
			size_t l = NLMSG_PAYLOAD(nh, sizeof(struct ifaddrmsg));

			while (RTA_OK(a, l)) {
				switch(a->rta_type) {
					case IFLA_IFNAME:{
								 NetworkChange ch;
								 ch.ifid = info->ifi_index;	
								 ch.ifname = string((char*) RTA_DATA(a));

								 if(nh->nlmsg_type == RTM_NEWLINK){
									 ch.eventType = DEVICE_ADD_EXISTS;
								 }else if(nh->nlmsg_type == RTM_DELLINK){
									 ch.eventType = DEVICE_REMOVAL;
								 }										

								 ret.push_back(ch);
								 break;
							 }
					default:
							 break;
				}
				a = RTA_NEXT(a, l);
			}
		}else if(nh->nlmsg_type == RTM_NEWADDR || nh->nlmsg_type == RTM_DELADDR || nh->nlmsg_type == RTM_GETADDR){
			struct ifaddrmsg* info =(struct ifaddrmsg*) NLMSG_DATA(nh);
			struct rtattr *a = IFLA_RTA(info);
			size_t l = NLMSG_PAYLOAD(nh, sizeof(struct ifaddrmsg));

			while (RTA_OK(a, l)) {
				switch(a->rta_type) {
					case IFLA_IFNAME:{
								 NetworkChange ch;
								 ch.ifid = info->ifa_index; 
								 ch.ifname = string((char*) RTA_DATA(a));
								 ch.eventType = DEVICE_CHANGE;
								 ret.push_back(ch);
								 break;
							 }
					default:
							 break;
				}
				a = RTA_NEXT(a, l);
			}
		}	

		nh = NLMSG_NEXT(nh, len);
	}
	return 0;
}

int NetworkChangeNotifier::disconnect(){
	close(fd);
}
