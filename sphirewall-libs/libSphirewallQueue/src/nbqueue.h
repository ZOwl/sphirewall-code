/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QQ_QUEUE
#define QQ_QUEUE

#define DO_YIELD() pthread_yield() 
#include <pthread.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <time.h>

extern int send_nbqueue_configfd;

pthread_mutex_t queue_recv_lock;
pthread_cond_t queue_recv_lock_wait_cond = PTHREAD_COND_INITIALIZER;

struct meta {
        volatile int head;
        volatile int tail;
        volatile int capacity;
        volatile int elem_size;
        volatile int offset;
	volatile int closing;
	volatile int pid;
};

struct meta* queue_connect(void* memory_block){
        struct meta* queue = (struct meta*) memory_block;
	queue->pid = getpid();
        return queue;
}

int queue_enqueue(void* elem, struct meta* q){
        while ((q->tail + 1) % (q->capacity + 1) == q->head)
		DO_YIELD();

	struct message* sourcePtr = (struct message*) elem;
	void* targetPtr = ((char*) q) + (q->elem_size * q->tail) + q->offset;

	memcpy(targetPtr,elem, sizeof(struct message));
	if(sourcePtr->modified == 1){
		memcpy(((char*) targetPtr) + sizeof(struct message), sourcePtr->packet.raw_packet, RAWSIZE);
	}

	int notify = 0;
	if(q->tail == q->head){
		notify = 1;
	}
        q->tail = (q->tail + 1) % (q->capacity + 1);
	char buf[0];
	ioctl(send_nbqueue_configfd, 100, buf);
	
	return 0;
}

void queue_pop(struct meta* queue){
	queue->head = (queue->head + 1) % (queue->capacity + 1);
}

struct message* queue_peek(struct meta* queue){
	static int deSleepTimer = 0;
	while (queue->head == queue->tail && queue->closing != 1){
		if(deSleepTimer++ == *SqClient::yieldCountSetting){
			pthread_mutex_lock(&queue_recv_lock);

			struct timespec ts;
			clock_gettime(CLOCK_REALTIME, &ts);
			ts.tv_sec += 1;
			pthread_cond_timedwait(&queue_recv_lock_wait_cond, &queue_recv_lock, &ts);
			pthread_mutex_unlock(&queue_recv_lock);

			deSleepTimer = 0;
		}else{
			DO_YIELD();
		}

	}

	if(queue->closing == 1){
		return NULL;
	}

	void* addr = ((char*)queue) + (queue->elem_size * queue->head) + queue->offset;
	struct message* m = (struct message*) addr;
	m->packet.raw_packet = (unsigned char*) ((char*) addr) + sizeof(struct message);
	return m;
}

#endif

