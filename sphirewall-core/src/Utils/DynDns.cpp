/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>

#include "Core/System.h"
#include "Core/Logger.h"
#include "Utils/DynDns.h"
#include "Utils/HttpRequestWrapper.h"
#include "Core/ConfigurationManager.h"
#include "Utils/Hash.h"
#include "Utils/StringUtils.h"

bool DynamicDnsClient::update(){
	static int attempts = 0;
	if(type == DYNDNS){	
		stringstream ss; ss << "Updating dynamic dns domain for " << domain;
		System::getInstance()->logger.getDefault()->log(SLogger::INFO, "DynamicDnsClient", ss.str(), false);

		stringstream url;
		url << "http://" << username << ":" << password << "@members.dyndns.org/nic/update?hostname=" << domain; 
		HttpRequestWrapper* request = new HttpRequestWrapper(url.str(), GET);
		request->useragent = "Sphirewall Labs - Sphirewall - 0.9.9.4";

		try {
			string buffer = request->execute();	
			delete request;

			stringstream debug; debug << "Dyndns responded with message : [" << buffer << "]"; 
			System::getInstance()->logger.getDefault()->log(SLogger::DEBUG, "DynamicDnsClient", debug.str(), false);

			if(buffer.find("good") != -1){ 
				active= true; 

				stringstream success; success << "Dyndns update succeeded";
				System::getInstance()->logger.getDefault()->log(SLogger::INFO, "DynamicDnsClient", success.str(), false);

				return true;
			}else{  
				stringstream failed; failed<< "Dyndns update failed with response : [" << buffer << "]"; 
				System::getInstance()->logger.getDefault()->log(SLogger::INFO, "DynamicDnsClient", failed.str(), false);
			}       

		}catch(HttpRequestWrapperException* exception){
			stringstream err; err << "Updating device for dynamic dns failed with a connection error";
			System::getInstance()->logger.getDefault()->log(SLogger::ERROR, "DynamicDnsClient", err.str(), false);

			active = false;
			attempts++;
			if(attempts++ < 5){
				return update();
			}       
		}

	}else if(type == FREEDNS){
		stringstream us; us << username << "|" << password;
		Hash h; string sha1Password = h.create_hash(us.str(), us.str().size());

		stringstream ss; ss << "http://freedns.afraid.org/api/?action=getdyndns&sha=" << sha1Password;
		HttpRequestWrapper* request = new HttpRequestWrapper(ss.str(), GET);
		try {
			string info = request->execute();	
			vector<string> domains;
			split(info, '\n', domains);

			for(int x = 0; x < domains.size(); x++){
				vector<string> pieces;
				split(domains[x], '|', pieces);

				if(pieces.size() == 3){
					if(pieces[0].compare(domain) == 0){
						HttpRequestWrapper* updateRequest = new HttpRequestWrapper(pieces[2], GET);
						updateRequest->execute();
						active = true;

						stringstream msg; msg << "Updated freedns service domain " << domain;
						System::getInstance()->logger.getDefault()->log(SLogger::INFO, "DynamicDnsClient", msg.str(), false);

						delete updateRequest;
						return true;
					}
				}
			}		

			stringstream err; err << "Could not update freedns service, domain was not found";
			System::getInstance()->logger.getDefault()->log(SLogger::ERROR, "DynamicDnsClient", err.str(), false);
		}catch(HttpRequestWrapperException* exception){
			active = false;

			stringstream err; err << "Could not update freedns service, an error was found";
			System::getInstance()->logger.getDefault()->log(SLogger::ERROR, "DynamicDnsClient", err.str(), false);
		}
		delete request;
	}

	attempts = 0;
	return false;
}

void GlobalDynDns::run(){
	run(false);
}

void GlobalDynDns::run(bool force){
	if(isEnabled()){
		client->update();
	}
}

void GlobalDynDns::load(){
	if(config->has("dyndns")){
		ObjectContainer* root = config->getElement("dyndns");	
		setUsername(root->get("username")->string());
		setPassword(root->get("password")->string());
		setDomain(root->get("domain")->string());
		setEnabled(root->get("enabled")->boolean());
		setType((DynamicDnsClientProvider) root->get("type")->number());
	}
}

void GlobalDynDns::save(){
	ObjectContainer* root = new ObjectContainer(CREL);
	root->put("username", new ObjectWrapper((string) getUsername()));
	root->put("password", new ObjectWrapper((string) getPassword()));
	root->put("domain", new ObjectWrapper((string) getDomain()));
	root->put("enabled", new ObjectWrapper((bool) isEnabled()));
	root->put("type", new ObjectWrapper((double) getType()));	

	config->setElement("dyndns", root);
	config->save();
}

