/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALLD_LOGGER_H_INCLUDED
#define SPHIREWALLD_LOGGER_H_INCLUDED

#include <map>
#include <queue>
#include <list>
#include <fstream>
#include <syslog.h>
#include <boost/thread.hpp>

namespace SLogger{

typedef enum{CRITICAL_ERROR=0,ERROR=1,EVENT=2,INFO=3,DEBUG=4} Priority;

void* logProcessorThread(void*);

class Log;

class SyslogPriority {
	public:
		static int getSyslogPriority(Priority p){
			switch(p){
				case CRITICAL_ERROR:
					return LOG_CRIT;
			
				case ERROR:
					return LOG_ERR;
			
				case EVENT:
					return LOG_INFO;
	
				case INFO:
					return LOG_INFO;

				case DEBUG:
					return LOG_DEBUG;
		
				default:
					return LOG_DEBUG;
			};
		}
};

class LogContext{
public:
	LogContext(Priority level, std::string name, std::string filename);
	LogContext(){}
	Priority level;
	std::string name;

	void log(Priority priority, std::string str);
	void log(Priority priority, std::string str, std::string message);
	void log(Priority priority, std::string function, std::string str, bool consoleOut);
	void flush();
private:
	std::string filename;
	std::list<Log> logs;
	std::ofstream fp;
};

class Log{
public:
        bool syslog;
	
	Priority priority;
	std::string location;
	std::string message;
	int timestamp;	
	
	std::string getPriority();
};

class Logger{
public:
	Logger();

	void registerContext(LogContext* context){
		contexts[context->name] = context;
	}

	void unregisterContext(LogContext* context){
		contexts.erase(contexts.find(context->name));
	}
	
	void start(){
		new boost::thread(boost::bind(&Logger::process, this));
	}
	
	void log(Priority priority, bool syslog, std::string location, std::string str);
	void log(Priority priority, std::string location, std::string str);
	void log(Priority priority, std::string str);
	void log(std::string context, Priority priority, std::string msg);
	
	void process();
	void flush();	
	LogContext* getDefault(){
		return defaultContext;
	}	

	std::list<std::string> getAvailableContexts();
	LogContext* getContext(std::string);

private:
	LogContext* defaultContext;
	std::map<std::string, LogContext*> contexts;
};

}
#endif

