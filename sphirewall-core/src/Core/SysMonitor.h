/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_SYSMONITOR_H_INCLUDED
#define SPHIREWALL_SYSMONITOR_H_INCLUDED

#include <vector>
#include <set>
#include <map>

#include "Core/Cron.h"

class UserDb;
class GroupDb;
class User;
class BandwidthDbPrimer;
class Sampler;
class EventDb;
class QuotaInfo;
class ConfigurationManager;
namespace SFwallCore {
	class Firewall;
};

namespace SLogger {
	class Logger;
}

class Watch {
public:
	virtual void poll() = 0;

	void setEventDb(EventDb* eventDb) {
		this->eventDb = eventDb;
	}

	void setLogger(SLogger::Logger* logger) {
		this->logger = logger;
	}

protected:
	EventDb* eventDb;
	SLogger::Logger* logger;
};

class MetricsWatch : public Watch {
	public:
		MetricsWatch(SFwallCore::Firewall* firewall, BandwidthDbPrimer* bandwidthDb) : 
			firewall(firewall), bandwidthDb(bandwidthDb){}

		void poll();
		std::map<std::string, long> getSnapshot();

	private:
		SFwallCore::Firewall* firewall;
		BandwidthDbPrimer* bandwidthDb;
		map<string, Sampler*> registeredSamplers;
};

class QuotaWatch : public Watch {
	public:

		QuotaWatch(UserDb* userDb, GroupDb* groupDb, ConfigurationManager* config);
		void poll();
		bool evaluateQuotaForUser(User* user);
		bool evaluateQuotaForUser(int baseTime, User* user);
		bool evaluateGlobalQuotas();

		void load();
		void save();
	
		QuotaInfo* getQuota(){
			return quota;
		}	
	protected:
		UserDb* userDb;
		GroupDb* groupDb;
		QuotaInfo* quota;		
		ConfigurationManager* config;		

		virtual long getTransfer(User* user, long start, long end);
		virtual long getTransfer(long start, long end);
		void alertOnQuota( User* user, bool exceeded);
		void alertOnQuota( bool exceeded);
};

class InternetConnectionWatch : public Watch {
	public:
		InternetConnectionWatch(ConfigurationManager* config) : Watch() {
			this->config = config;
			this->state = false;
		}

		void poll();
	private:
		ConfigurationManager* config;
		bool state;
};

class MetricSampler {
	public:
		virtual void sample(std::map<string, long>& input) = 0;
};

class SysMonitor {
	public:
		SysMonitor() {} 
		SysMonitor(EventDb* eventDb, SLogger::Logger* logger, 
			CronManager* cronManager, SFwallCore::Firewall* firewall, BandwidthDbPrimer* bandwidthDb, QuotaWatch* quotaWatch);

		void registerMetric(MetricSampler* sampler);	
	private:
		EventDb* eventDb;
		SLogger::Logger* logger;
		std::list<Watch*> registeredWatches;
		MetricsWatch* metrics;
		SFwallCore::Firewall* firewall;

		list<MetricSampler*> registeredSamplers;
		class SysMonitorCron : public CronJob {
			public:

				SysMonitorCron(SysMonitor* monitor) : CronJob(60, "SYSMONITOR_CRON", true), monitor(monitor) {
				}

				void run() {
					monitor->pollall();
				}

			private:
				SysMonitor* monitor;
		};

		void pollall();
};

#endif
