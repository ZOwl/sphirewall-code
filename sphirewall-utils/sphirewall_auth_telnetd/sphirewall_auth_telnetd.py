# This file is part of Sphirewall, it comes with a copy of the license in LICENSE.txt, which is also available at http://sphirewall.net/LICENSE.txt
#!/usr/bin/python

import json
from socket import *
import sys
import SocketServer

SERVICE_HOSTNAME = "localhost"
SERVICE_PORT = 8001

class MyTCPHandler(SocketServer.BaseRequestHandler):
	def handle(self):

		self.request.sendall("Sphirewall Authentication Daemon\n")	
		self.request.sendall("1. Login\n2. Logout\n0. Exit\nInput your choice:")
		choice = self.request.recv(1024).strip()
		if choice == "1":
			self.request.sendall("Username:")
			username = self.request.recv(1024).strip()
			self.request.sendall("Password:")
			password = self.request.recv(1024).strip()

			request = {'request':'auth/login', 'args':{'username':username, 'password':password, 'ipaddress':self.client_address[0]}}
			s = socket(AF_INET, SOCK_STREAM)
			s.connect((SERVICE_HOSTNAME, SERVICE_PORT))
			s.send(json.dumps(request))
			s.send('\n')
			data = s.recv(1024)

			response = json.loads(data)
			if response['response']['response'] == 0:
					s.close();
					self.request.sendall("Login Success\n")
			else:
					s.close();
					self.request.sendall("Login Failed: " + response['response']['message'] + "\n")

		if choice == "2":
			request = {'request':'auth/logout', 'args':{'ipaddress':self.client_address[0]}}
			s = socket(AF_INET, SOCK_STREAM)
			s.connect((SERVICE_HOSTNAME, SERVICE_PORT))
			s.send(json.dumps(request))
			s.send('\n')
			data = s.recv(1024)

			response = json.loads(data)
			if response['response']['response'] == 0:
					s.close();
					self.request.sendall("Logout Success\n")
			else:
					s.close();
					self.request.sendall("Logout Failed: " + response['response']['message'] + "\n")
		else:
			self.request.sendall("Exiting\n");	

if __name__ == "__main__":
	if len(sys.argv) == 5:
		HOST, PORT, SERVICE_HOSTNAME, SERVICE_PORT = sys.argv[1], int(sys.argv[2]), sys.argv[3], int(sys.argv[4])
	
		server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)
		server.serve_forever()
	else:
		print "Missing required arguments: <local address> <bind telnet to> <service address> <service port>"
