#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

from flask.globals import request
from flask.templating import render_template
from werkzeug.utils import redirect
from sphirewallwmi import app
import service

@app.route("/firewall/aliases", methods=["POST", "GET"])
def firewall_aliases():
    if request.method == "POST":
        service.get_sphirewallapi().firewall().aliases_create(text_value("name"), text_value("type"),
            text_value("source"), text_value("detail"))

    return render_template("firewall_aliases.html", aliases=service.get_sphirewallapi().firewall().aliases())


@app.route("/firewall/aliases/<id>/delete")
def firewall_aliases_delete(id):
    service.get_sphirewallapi().firewall().aliases_delete(id)
    return redirect("/firewall/aliases")


@app.route("/firewall/aliases/<id>/sync")
def firewall_aliases_sync(id):
    service.get_sphirewallapi().firewall().aliases_sync(id)
    return redirect("/firewall/aliases")


@app.route("/firewall/aliases/<id>", methods=["POST", "GET"])
def firewall_aliases_get(id):
    if request.method == "POST":
        values = text_value("value")
        value_list = values.split(" ")
        for value in value_list:
            service.get_sphirewallapi().firewall().aliases_list_add(id, value)
    return render_template("firewall_aliases_edit.html", alias=service.get_sphirewallapi().firewall().aliases(id),
        items=service.get_sphirewallapi().firewall().aliases_list(id))


@app.route("/firewall/aliases/<id>/deletevalue")
def firewall_aliases_del(id):
    service.get_sphirewallapi().firewall().aliases_list_del(id, request.args["value"])
    return redirect("/firewall/aliases/" + id)


@app.route("/firewall/webfilter", methods=["GET", "POST"])
def firewall_webfilter():
    return render_template("firewall_webfilter.html", pools=service.get_sphirewallapi().firewall().aliases(),
        groups=service.get_sphirewallapi().general().groups(), rules=service.get_sphirewallapi().firewall().webfilter())


@app.route("/firewall/webfilter/add", methods=["GET", "POST"])
@app.route("/firewall/webfilter/add/<id>", methods=["GET", "POST"])
def firewall_webfilter_add(id=None):
    if request.method == "POST":
        service.get_sphirewallapi().firewall().webfilter_add(id, text_value("sourceIp"), text_value("sourceMask"),
            request.form.getlist('group'), request.form.getlist('list'), text_value("action"),
            checkbox_value("fireEvent"),
            checkbox_value("redirect"), text_value("redirectUrl"), text_value("startTime"), text_value("endTime"),
            checkbox_value("mon"), checkbox_value("tues"), checkbox_value("wed"), checkbox_value("thurs"),
            checkbox_value("fri"),
            checkbox_value("sat"), checkbox_value("sun"), False, request.form.getlist('sourceAlias')
        )
        return redirect("/firewall/webfilter")

    return render_template("firewall_webfilter_add.html", rule=service.get_sphirewallapi().firewall().webfilter_get(id),
        pools=service.get_sphirewallapi().firewall().aliases(),
        groups=service.get_sphirewallapi().general().groups(), rules=service.get_sphirewallapi().firewall().webfilter())


@app.route("/firewall/webfilter/<id>/delete")
def firewall_webfilter_delete(id):
    service.get_sphirewallapi().firewall().webfilter_delete(id)
    return redirect("/firewall/webfilter")


@app.route("/firewall/webfilter/<id>/moveup")
def firewall_webfilter_moveup(id):
    service.get_sphirewallapi().firewall().webfilter_moveup(id)
    return redirect("/firewall/webfilter")


@app.route("/firewall/webfilter/<id>/movedown")
def firewall_webfilter_movedown(id):
    service.get_sphirewallapi().firewall().webfilter_movedown(id)
    return redirect("/firewall/webfilter")


@app.route("/firewall/webfilter/disable/<id>", methods=["POST", "GET"])
def firewall_webfilter_disable(id):
    service.get_sphirewallapi().firewall().webfilter_disable(id)
    return redirect("/firewall/webfilter")


@app.route("/firewall/webfilter/enable/<id>", methods=["POST", "GET"])
def firewall_webfilter_enable(id):
    service.get_sphirewallapi().firewall().webfilter_enable(id)
    return redirect("/firewall/webfilter")


@app.route("/firewall/filtering")
def firewall_acls():
    return render_template("firewall_filtering.html",
        filter=service.get_sphirewallapi().firewall().acls(),
        groups=service.get_sphirewallapi().general().groups())


@app.route("/firewall/priority")
def firewall_priority():
    return render_template("firewall_priorityqos.html",
        filter=service.get_sphirewallapi().firewall().priority(),
        groups=service.get_sphirewallapi().general().groups(),
        enabled=service.get_sphirewallapi().general().advanced_value("QOS_ENABLED"))


@app.route("/firewall/qos")
def firewall_qos():
    return render_template("firewall_limiterqos.html",
        qos=service.get_sphirewallapi().firewall().qos(),
        groups=service.get_sphirewallapi().general().groups(),
        enabled=service.get_sphirewallapi().general().advanced_value("QOS_ENABLED"))


@app.route("/firewall/nat")
def firewall_nat():
    return render_template("firewall_nat.html",
        nat=service.get_sphirewallapi().firewall().nat(),
        groups=service.get_sphirewallapi().general().groups())


@app.route("/firewall/acls/denylist/<host>/delete")
def firewall_denylist_delete(host):
    service.get_sphirewallapi().firewall().denylist_delete(host)
    return redirect("/firewall/acls")


@app.route("/firewall/acls/qos/add", methods=["GET", "POST"])
def firewall_acls_qos_add():
    if request.method == "POST":
        service.get_sphirewallapi().firewall().qos_add(checkbox_value("sourceBlock"), text_value("source"),
            text_value("sourceMask"), text_value("sourcePort"), text_value("groupid"),
            checkbox_value("destinationBlock"), text_value("destination"),
            text_value("destinationMask"), text_value("destinationPort"), text_value("upload"), text_value("download"))
        return redirect("/firewall/qos")
    return render_template("firewall_acls_qos_add.html",
        groups=service.get_sphirewallapi().general().groups())


@app.route("/firewall/acls/qos/<id>/delete")
def firewall_acls_qos_delete(id):
    service.get_sphirewallapi().firewall().qos_del(id)
    return redirect("/firewall/qos")


@app.route("/firewall/acls/normal/add", methods=["GET", "POST"])
@app.route("/firewall/acls/normal/add/<id>", methods=["GET", "POST"])
def firewall_acls_normal_add(id=None):
    if request.method == "POST":
        service.get_sphirewallapi().firewall().normal_add(id, text_value("source"), text_value("source_mask"),
            text_value("source_alias"), text_value("sport_start"), text_value("sport_end"), text_value("dport_start"),
            text_value("dport_end"), text_value("sport"), text_value("dport"), text_value("source_dev"),
            text_value("dest"), text_value("dest_mask"), text_value("dest_alias"),
            text_value("startHour"), text_value("finishHour"), text_value("dest_dev"), checkbox_value("ignoreconntrack")
            , text_value("groupid"), text_value("mac"), text_value("comment"), text_value("protocol"),
            text_value("action"), text_value("snat_target"), text_value("snat_device_target"), text_value("dnat_target")
            , text_value("dnat_port"), checkbox_value("log"), text_value("nice"))

        if text_value("action") == "ACCEPT" or text_value("action") == "DROP":
            return redirect("/firewall/filtering")
        if text_value("action") == "MASQUERADE" or text_value("action") == "FORWARD":
            return redirect("/firewall/nat")
        else:
            return redirect("/firewall/priority")

    return render_template("firewall_acls_normal_add.html", rule=service.get_sphirewallapi().firewall().get(id),
        groups=service.get_sphirewallapi().general().groups(),
        devices=service.get_sphirewallapi().network().devices(),
        aliases=service.get_sphirewallapi().firewall().aliases(), mode=arg("mode"))


@app.route("/firewall/acls/up/<id>")
def firewall_acls_up(id):
    service.get_sphirewallapi().firewall().normal_up(id)
    if "caller" in request.args:
        return redirect(request.args["caller"])

    return redirect("/firewall/filtering")


@app.route("/firewall/acls/down/<id>")
def firewall_acls_down(id):
    service.get_sphirewallapi().firewall().normal_down(id)
    if "caller" in request.args:
        return redirect(request.args["caller"])

    return redirect("/firewall/filtering")


@app.route("/firewall/acls/delete/<id>")
def firewall_acls_delete(id):
    service.get_sphirewallapi().firewall().normal_delete(id)
    if "caller" in request.args:
        return redirect(request.args["caller"])
    return redirect("/firewall/filtering")


@app.route("/firewall/acls/enable/<id>", methods=["GET", "POST"])
def firewall_acls_enable(id):
    service.get_sphirewallapi().firewall().normal_enable(id)
    if "caller" in request.args:
        return redirect(request.args["caller"])

    return redirect("/firewall/nat")


@app.route("/firewall/acls/disable/<id>", methods=["GET", "POST"])
def firewall_acls_acls_disable(id):
    service.get_sphirewallapi().firewall().normal_disable(id)
    if "caller" in request.args:
        return redirect(request.args["caller"])

    return redirect("/firewall/nat")


@app.route("/firewall/priority/disable")
def firewall_priority_disable():
    service.get_sphirewallapi().general().advanced("QOS_ENABLED", 0)
    return redirect("/firewall/priority")


@app.route("/firewall/priority/enable")
def firewall_priority_enable():
    service.get_sphirewallapi().general().advanced("QOS_ENABLED", 1)
    return redirect("/firewall/priority")


@app.route("/firewall/qos/disable")
def firewall_qos_disable():
    service.get_sphirewallapi().general().advanced("QOS_ENABLED", 0)
    return redirect("/firewall/qos")


@app.route("/firewall/qos/enable")
def firewall_qos_enable():
    service.get_sphirewallapi().general().advanced("QOS_ENABLED", 1)
    return redirect("/firewall/qos")


def checkbox_value(key):
    if key in request.form:
        return True
    return False


def text_value(key):
    if key in request.form:
        return request.form[key]
    return ""


def arg(key):
    if key in request.args:
        return request.args[key]
    return ""