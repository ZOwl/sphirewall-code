#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester

class FirewallAclsTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login("fw", "admin", "admin")

    def gotoRules(self):
        self.clickFirewall()
        self.clickAcl()

    def clickAcl(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Rate Limiting Qos')]").click()

    def clickFirewall(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Firewall')]").click()

    def test_addQos_Basic(self):
        self.gotoRules()
        self.tester.clickOptions("Create QOS Rule")

        self.tester.driver.find_element_by_name("source").send_keys("10.1.1.0")
        self.tester.driver.find_element_by_name("sourceMask").send_keys("255.255.255.240")
        self.tester.driver.find_element_by_name("sourcePort").send_keys("4433")

        self.tester.driver.find_element_by_name("destination").send_keys("10.1.2.0")
        self.tester.driver.find_element_by_name("destinationMask").send_keys("255.255.255.0")
        self.tester.driver.find_element_by_name("destinationPort").send_keys("80")

        self.tester.driver.find_element_by_name("upload").send_keys("512000")
        self.tester.driver.find_element_by_name("download").send_keys("1024000")

        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'10.1.1.0/255.255.255.240 port 4433')]//ancestor::tr/td[contains(normalize-space(), '10.1.2.0/255.255.255.0 port 80')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(),'10.1.1.0/255.255.255.240 port 4433')]//ancestor::tr/td/a[contains(normalize-space(), 'Delete')]").click()

        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'10.1.1.0/255.255.255.240 port 4433')]"))

    def tearDown(self):
        self.tester.disconnect()
