#!/bin/bash

#Properties
SOURCE_DIR=.
TEMP_DIR=tmp_build_dir
VERSION=0.9.9.7

#Execution
rm -Rf $TEMP_DIR

cd $SOURCE_DIR
python setup.py sdist 
mkdir $TEMP_DIR
cp dist/sphirewallwmi-$VERSION.tar.gz $TEMP_DIR
cp .debian $TEMP_DIR -Rf
cd $TEMP_DIR
tar zxvf sphirewallwmi-$VERSION.tar.gz
cd sphirewallwmi-$VERSION
python setup.py --command-packages=stdeb.command debianize
rm -rf debian/patches
echo 'sphirewallwmi.egg-info/*' > debian/clean
cp ../sphirewallwmi-$VERSION.tar.gz ../sphirewallwmi_$VERSION.orig.tar.gz
cp ../.debian/* debian -Rf
dpkg-buildpackage
