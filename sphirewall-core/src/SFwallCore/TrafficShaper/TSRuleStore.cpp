/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <map>
#include <vector>
#include <fstream>

#include "SFwallCore/TrafficShaper/TSRuleStore.h"
#include "Core/System.h"

using namespace std;

static const char FIELD_DELIM = ':';

TSRuleStore::TSRuleStore(SLogger::LogContext* logger,
		ConfigurationManager* config,
		TrafficShaper* shaper,
		GroupDb* groupDb,
		SessionDb* sessionDb) : logger(logger),
configurationManager(config),
shaper(shaper),
groupDb(groupDb),
sessionDb(sessionDb) {
	count = 0;

	if(configurationManager != NULL){
		load();
	}
}

TSRuleStore::~TSRuleStore() {
	for (std::map<int, TsRule*>::iterator iter = rules.begin();
			iter != rules.end();
			iter++) {
		delete iter->second;
	}
}

TokenBucket* TSRuleStore::match(SFwallCore::Packet* packet) {

	for (std::map<int, TsRule*>::iterator iter = rules.begin();
			iter != rules.end();
			iter++) {

		if (iter->second->filter.check(packet)) {
			TsRule* rule = iter->second;
			TokenFilter* filter = NULL;

			/*Check for extra required criteria*/
			if (rule->filter.block_groupid != -1) {
				Group* group = System::getInstance()->getGroupDb()->getGroup(rule->filter.block_groupid);
				Session* session = System::getInstance()->getSessionDb()->get(packet->getHw(), IP4Addr::ip4AddrToString(packet->getSrcIp()));
				if (session != NULL) {
					if (session->getUser() != NULL) {
						session->getUser()->checkForGroup(group);

						filter = new TokenFilter(rule->filter);
						filter->userPtr = session->getUser();

						logger->log(SLogger::INFO, "TSRuleStore::match()", "Creating a bucket based on group criteria");
					}
				}
			} else if (rule->filter.block_dest != -1 && rule->filter.block_destMask != -1) {
				if (IP4Addr::matchNetwork(packet->getDstIp(), rule->filter.block_dest, rule->filter.block_destMask) == 0) {
					filter = new TokenFilter(rule->filter);
					filter->dest = packet->getDstIp();
					filter->destMask = IP4Addr::SUBNET_SINGLE_IP;
					logger->log(SLogger::INFO, "TSRuleStore::match()", "Creating a bucket based on dest criteria");

				}

			} else if (rule->filter.block_source != -1 && rule->filter.block_sourceMask != -1) {
				if (IP4Addr::matchNetwork(packet->getSrcIp(), rule->filter.block_source, rule->filter.block_sourceMask) == 0) {

					filter = new TokenFilter(rule->filter);
					filter->source = packet->getSrcIp();
					filter->sourceMask = IP4Addr::SUBNET_SINGLE_IP;
					logger->log(SLogger::INFO, "TSRuleStore::match()", "Creating a bucket based on source criteria");

				}
			} else {
				logger->log(SLogger::INFO, "TSRuleStore::match()", "Creating a bucket based on generic filter criteria");
				filter = &rule->filter;
			}

			/*So ---> If we now have a filter lets add a bucket for it*/
			if (filter != NULL) {
				TokenBucket* bucket = new TokenBucket(rule->uploadRate, rule->downloadRate, filter);

				/*Lets create a new bucket*/
				shaper->addBucket(bucket);
				iter->second->buckets.push_back(bucket);

				rule->incrementHits();
				return bucket;
			}
		}
	}

	logger->log(SLogger::DEBUG, "TSRuleStore::match()", "Could not match new connection with a TSRule");
	return NULL;
}

void TSRuleStore::add(TsRule* rule) {
	logger->log(SLogger::EVENT, "TSRuleStore::add()", "Added a new TSRule");
	rules[rule->id] = rule;

	save();
}

TsRule* TSRuleStore::get(int id) {
	if (rules.find(id) != rules.end()) {
		return rules[id];
	}

	return NULL;
}

void TSRuleStore::del(TsRule* rule) {
	logger->log(SLogger::EVENT, "TSRuleStore::del()", "Deleting a TSRule");
	if (rules.find(rule->id) != rules.end()) {
		for (int x = 0; x < rule->buckets.size(); x++) {
			shaper->del(rule->buckets[x]);
		}

		rules.erase(rule->id);
		delete rule;
	}

	save();
}

vector<TsRule*> TSRuleStore::list() {
	vector<TsRule*> ret;

	for (map<int, TsRule*>::iterator iter = rules.begin();
			iter != rules.end();
			iter++) {
		ret.push_back(iter->second);
	}

	return ret;
}

void TSRuleStore::save() {
	logger->log(SLogger::INFO, "TSRuleStore::save()", "Saving TSRules to: " + ruleFile);
	if(configurationManager == NULL){
		return;
	}

	ObjectContainer* root = new ObjectContainer(CREL);	
	ObjectContainer* arr = new ObjectContainer(CARRAY);	
	
	for (map<int, TsRule*>::iterator iter = rules.begin();
			iter != rules.end();
			iter++) {

		TsRule* b = iter->second;

		ObjectContainer* o = new ObjectContainer(CREL);
		o->put("dport", new ObjectWrapper((double) b->filter.dport));
		o->put("sport", new ObjectWrapper((double) b->filter.sport));
		o->put("source", new ObjectWrapper((string) IP4Addr::ip4AddrToString(b->filter.source)));
		o->put("sourceMask", new ObjectWrapper((string) IP4Addr::ip4AddrToString(b->filter.sourceMask)));
		o->put("dest", new ObjectWrapper((string) IP4Addr::ip4AddrToString(b->filter.dest)));
		o->put("destMask", new ObjectWrapper((string) IP4Addr::ip4AddrToString(b->filter.destMask)));
		o->put("block_dest", new ObjectWrapper((string) IP4Addr::ip4AddrToString(b->filter.block_dest)));
		o->put("block_destMask", new ObjectWrapper((string) IP4Addr::ip4AddrToString(b->filter.block_destMask)));
		o->put("block_source", new ObjectWrapper((string) IP4Addr::ip4AddrToString(b->filter.block_source)));
		o->put("block_sourceMask", new ObjectWrapper((string) IP4Addr::ip4AddrToString(b->filter.block_sourceMask)));
		o->put("block_groupid", new ObjectWrapper((double) b->filter.block_groupid));
		o->put("upload", new ObjectWrapper((double) b->uploadRate));
		o->put("download", new ObjectWrapper((double) b->downloadRate));
		o->put("id", new ObjectWrapper((double) b->id));

		arr->put(new ObjectWrapper(o));
	}

	root->put("rules", new ObjectWrapper(arr));
	configurationManager->setElement("qos", root);
	configurationManager->save();
}

void TSRuleStore::load() {
	logger->log(SLogger::EVENT,
			"SFwallCore::TSRuleStore",
			"Loading Rules from file: " +
			ruleFile);

	count = 0;
	ObjectContainer* root;
	if (configurationManager->has("qos")) {
		root = configurationManager->getElement("qos");
		ObjectContainer* arr = root->get("rules")->container();

		for (int x = 0; x < arr->size(); x++) {
			ObjectContainer* source = arr->get(x)->container();

			TsRule* target = new TsRule(source->get("id")->number());
			target->uploadRate = source->get("upload")->number();
			target->downloadRate = source->get("download")->number();

			//Blocks
			target->filter.block_dest = IP4Addr::stringToIP4Addr(source->get("block_dest")->string());
			target->filter.block_destMask = IP4Addr::stringToIP4Addr(source->get("block_destMask")->string());
			target->filter.block_source = IP4Addr::stringToIP4Addr(source->get("block_source")->string());
			target->filter.block_sourceMask = IP4Addr::stringToIP4Addr(source->get("block_sourceMask")->string());
			target->filter.block_groupid = source->get("block_groupid")->number();

			target->filter.dest = IP4Addr::stringToIP4Addr(source->get("dest")->string());
			target->filter.destMask = IP4Addr::stringToIP4Addr(source->get("destMask")->string());
			target->filter.source = IP4Addr::stringToIP4Addr(source->get("source")->string());
			target->filter.sourceMask = IP4Addr::stringToIP4Addr(source->get("sourceMask")->string());

			target->filter.dport = source->get("dport")->number();
			target->filter.sport = source->get("sport")->number();

			rules[target->id] = target;

			if (target->id > count) {
				count = target->id + 1;
			}
		}
	}
}
