/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <map>
#include <sstream>
#include <vector>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <syslog.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in_systm.h>
#include <netinet/in.h>

#define __FAVOR_BSD
#if (linux)
#define __BSD_SOURCE
#endif

#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

using namespace std;

#include "Core/System.h"
#include "Ids/Ids.h"
#include "Core/Event.h"
#include "Ids/SnortLogSensor.h"
#include "Ids/PortScanSensor.h"
#include "Ids/SshLogSensor.h"
#include "Ids/FloodIdsSensor.h"

void IDS::start() {
	logger->log(SLogger::EVENT, "Starting IDS");

	sensors.push_back(new PortScanSensor(this));
	sensors.push_back(new FloodSensor(this));
	logSensors.push_back(new SshLogSensor(this));
	logSensors.push_back(new SnortLogSensor(this));
	logger->log(SLogger::INFO, "Finished Starting IDS");
}

void IDS::stop() {
	for (list<IdsSensor*>::iterator iter = sensors.begin(); iter != sensors.end(); iter++) {
		IdsSensor* sensor = *iter;
		delete sensor;

		iter = sensors.erase(iter);
	}
}

void IDS::handle(SFwallCore::Packet* packet) {
	if(!packet){
		return;
	}

	for (list<IdsSensor*>::iterator iter = sensors.begin(); iter != sensors.end(); iter++) {
		(*iter)->handle(packet);
	}
}

void IdsSensor::alert(unsigned int source, unsigned int dest) {
	if (!idsPtr->checkExceptions(source) && !idsPtr->checkExceptions(dest)) {
		EventParams params;
		params["source"] = IP4Addr::ip4AddrToString(source); 
		params["destination"] = IP4Addr::ip4AddrToString(dest); 
		params["desc"] =  description(); 
		idsPtr->getEventDb()->add(new Event(type(), params));
	}
}

void IdsLogSensor::roll() {
	if(active()){

		if (!stream.is_open()) {
			stream.open(filename().c_str());
		}

		if (stream.is_open()) {
			while (!stream.eof()) {
				std::string line;
				getline(stream, line);
				parseIn(line);
			}
			stream.clear();
		}
	}
}

void IdsLogSensor::alert(unsigned int source, unsigned int dest) {
	EventParams params;
	params["source"] = IP4Addr::ip4AddrToString(source);
	params["destination"] = IP4Addr::ip4AddrToString(dest);
	params["desc"] =  description();
	idsPtr->getEventDb()->add(new Event(type(), params));
}

IDS::IDS(EventDb* eventDb, SLogger::Logger* loggerRoot) : CronJob(5), eventDb(eventDb), loggerRoot(loggerRoot) {
	logger = new SLogger::LogContext(SLogger::INFO, "IDS", "sphirewalld-ids");
	loggerRoot->registerContext(logger);
}

IDS::~IDS() {
	loggerRoot->unregisterContext(logger);
	delete logger;
}

void IDS::listExceptions(vector<AddressPair>& e) {
	e = exceptions;
}

void IDS::addException(AddressPair pair) {
	exceptions.push_back(pair);
}

void IDS::delException(AddressPair pair) {
	for (vector<AddressPair>::iterator iter = exceptions.begin(); iter != exceptions.end(); iter++) {
		AddressPair target = *iter;
		if (target.ip == pair.ip && target.mask == pair.mask) {
			exceptions.erase(iter);
			return;
		}
	}
}

bool IDS::checkExceptions(unsigned int address) {
	for (int x = 0; x < exceptions.size(); x++) {
		if (IP4Addr::matchNetwork(address, exceptions[x].ip, exceptions[x].mask) == 0) {
			return true;
		}
	}
	return false;
}

/*Method for the CronJob: This runs all the log sensors*/
void IDS::run() {
	for (list<IdsLogSensor*>::const_iterator iter = logSensors.begin(); iter != logSensors.end(); iter++) {
		(*iter)->roll();
	}
}

