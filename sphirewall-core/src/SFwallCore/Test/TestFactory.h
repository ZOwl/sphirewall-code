/* This file is part of Sphirewall, it comes with a copy of the license in LICENSE.txt, which is also available at http://sphirewall.net/LICENSE.txt */

#ifndef TESTFACTORY_H
#define TESTFACTORY_H

namespace SFwallCore {
class TcpPacket;
class UdpPacket;
class IcmpPacket;
class Packet;
class PacketBuilder {
        public:
                static TcpPacket* createTcp(std::string sourceIp, std::string destIp, int sourcePort, int destPort);
		static UdpPacket* createUdp(std::string sourceIp, std::string destIp, int sourcePort, int destPort);
		static IcmpPacket* createIcmp(std::string sourceIp, std::string destIp, int icmpid);

                static void setAck(TcpPacket* tcpPacket);
		static void setSyn(TcpPacket* tcpPacket);

		static void setSourceDev(Packet* packet, int device);
		static void setDestDev(Packet* packet, int device);
};
};
#endif
