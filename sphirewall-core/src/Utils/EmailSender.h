/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EMAILSENDER_H
#define EMAILSENDER_H

class ConfigurationManager;
namespace SLogger{
	class LogContext;
};

class EmailSender {
	public:
		EmailSender(ConfigurationManager* config, SLogger::LogContext* logger) : 
			config(config), logger(logger){}

		int send(std::string recipient, std::string subject, std::string message);
		void publish();

		std::string getServer();
		int getPort(); 
		std::string getUsername();
		std::string getPassword();		
		bool getUseTls();		
		std::string getDefaultEmail();

	private:
		ConfigurationManager* config;
		SLogger::LogContext* logger;
};

#endif
