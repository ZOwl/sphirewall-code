#Copyright Michael Lawson, John Deal
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import re

class Node:
    root = None;
    leaves = None;
    function = None;
    c = None;

    def __init__(self, root, function, c):
        self.leaves = [];
        self.root = root;
        self.function = function;
        self.c = c;
        if self.root != None:
            self.root.leaves.append(self);

    def matches(self, input):
        if input == None:
            return -1;

        p = re.compile(self.c)

        if p.match(input):
            return 0
        else:
            return -1

    def usage(self):
        print "command not found, available options are:\n";
        for l in self.root.leaves:
            print l.c
