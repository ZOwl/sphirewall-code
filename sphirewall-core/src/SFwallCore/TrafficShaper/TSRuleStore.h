/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TSRULESTORE_H
#define TSRULESTORE_H

#include "TSRule.h"
#include "Core/Logger.h"
#include "Auth/GroupDb.h"
#include "Auth/UserDb.h"
#include "TrafficShaper.h"
#include "Auth/SessionDb.h"

class TrafficShaper;

class TSRuleStore {
public:

	void add(TsRule*);
	TsRule* get(int id);
	void del(TsRule*);

	void setConfigurationManager(ConfigurationManager* c) {
		this->configurationManager = c;
	}

	void setLogger(SLogger::LogContext* logger) {
		this->logger = logger;
	}

	std::vector<TsRule*> list();

	string ruleFile;

	TSRuleStore(SLogger::LogContext* logger, ConfigurationManager* config, TrafficShaper* shaper, GroupDb* groupDb, SessionDb* sessionDb);
	~TSRuleStore();

	int count;
	TokenBucket* match(SFwallCore::Packet*);
private:
	std::map<int, TsRule*> rules;

	void load();
	void save();

	SLogger::LogContext* logger;
	TrafficShaper* shaper;
	GroupDb* groupDb;
	SessionDb* sessionDb;
	ConfigurationManager* configurationManager;
};

#endif
