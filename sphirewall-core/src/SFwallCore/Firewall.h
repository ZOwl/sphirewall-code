/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FIREWALL_H
#define FIREWALL_H

#include <map>
#include <list>
#include <vector>
#include <queue>

#include "Core/Logger.h"
#include "Core/Config.h"
#include "Acl.h"
#include "Alias.h"
#include "client.h"
#include "TrafficShaper/TrafficShaper.h"
#include "PostProcessor.h"
#include "Core/Cron.h"
#include "Core/SysMonitor.h"

class ConfigurationManager;
class Sampler;
namespace SFwallCore {
	class RewriteEngine;
	class PlainConnTracker;
	class SNatConnTracker;
	class DNatConnTracker;
	class HttpUrlApplicationFilter;

	class GarbageCollectorCron : public CronJob {
	public:

		GarbageCollectorCron(PlainConnTracker* connectionTracker,	
				SLogger::LogContext* logger) :
		CronJob(60 * 5, "CONNTRACKER_GARBAGECOLLECTOR_CRON", true),
		connectionTracker(connectionTracker),
		logger(logger) {
			waiting = false;
		}

		void run();
		bool waiting;

	private:
		PlainConnTracker* connectionTracker;
		SLogger::LogContext* logger;
	};

	class Firewall : public MetricSampler {
	public:
		static int TCP_CONNECTION_TIMEOUT_THESHOLD;
		static int UDP_CONNECTION_TIMEOUT_THESHOLD;
		static int ICMP_CONNECTION_TIMEOUT_THESHOLD;

		static int SYN_SYNACK_WAIT_TIMEOUT;
		static int SYN_ACKACK_TIMEOUT;
		static int FIN_FINACK_WAIT_TIMEOUT;
		static int FIN_WAIT_TIMEOUT;
		static int TIME_WAIT_TIMEOUT;
		static int CLOSE_WAIT_TIMEOUT;
		static int LAST_ACK_TIMEOUT;
		static int CACHE_TTL_MAX;
		static int SQUEUE_DEQUEUE_YIELD_TIMEOUT;
		static int SQUEUE_DEQUEUE_YIELD_SLEEP_INTERVAL;

		static int POST_PROCESSOR_ENABLED;
		static int BLOCK_LIST_ENABLED;
		static int PREROUTING_ENABLED;
		static int POSTROUTING_ENABLED;
		static int QOS_ENABLED;
		static int CONNECTIONTRACKER_CACHE_ENABLED;		
		static int FIREWALL_FORCE_AUTHENTICATION;	
		static int BANDWIDTHDB_ENABLED;	
		static int REWRITE_ENABLED;
		static int REWRITE_PROXY_REQUESTS_ENABLED;
		static int REQUIRE_TLS;

		static const int PLAIN_CONN_HASH_MAX = 200000;

		PostProcessor* post;

		/*Api for the capture portal stuff*/
		TrafficShaper* trafficShaper;

		SqClient sqClient;

		/*!
		 * 	System for holding/managing ACLS - Magic monkeys live here.
		 */
		ACLStore* acls;

		/*!
		 * 	Super class Connection Tracker.
		 *
		 * 	Magic fish live here and in all its children.
		 *
		 */
		PlainConnTracker* connectionTracker;

		GarbageCollectorCron* garbageCron;

		Firewall(Config* config, SLogger::Logger* logger, ConfigurationManager* configurationManager);
		Firewall() {}
		~Firewall();
		/*!
		 * 	Kicks everything off.
		 *
		 * 	This function runs, loads the acls, loads the iptable rules --> see below. And starts the processing threads.
		 */
		void start(bool debug);

		/*
		 *
		 */

		void stop();

		/*!
		 * 	ACL configuration file - all acls are stored here
		 */
		string filterFile;
		string natFile;

		/*!
		 * 	Configuration file where aliases are stored
		 */
		string aliasFilename;

		/*!
		 *	Default action: If a match cannot be found for a new packet, then we return this action. Default is deny
		 */
		int defaultAction;

		/*!
		 * 	Lets keep track of how many packets are served with the default action - useful for debugging hells angels
		 */
		int defaultCount;

		/*!
		 * 	Store and manage all those nice little aliases
		 */
		AliasDb* aliases;

		ConfigurationManager* configurationManager;

		int totalTcpUp;
		int totalTcpDown;
		int totalUdpUp;
		int totalUdpDown;

		bool debug;

		long totalTcpPackets;
		long totalUdpPackets;
		long totalIcmpPackets;
		long totalDefaultAction;

		Sampler* avgPacketProcess;

		SLogger::LogContext* getLogContext() const {
			return logContext;
		}

		void setLogContext(SLogger::LogContext* logger) {
			this->logContext = logger;
		}


		//Getters:

		PlainConnTracker* getPlainConnTracker() const {
			return connectionTracker;
		}

		void setInterfaceManager(InterfaceManager* i){
			this->interfaceManager = i;
		}
		
		HttpUrlApplicationFilter* httpFilter;
		RewriteEngine* rewriteEngine;

		Sampler* transferSampler;
		Sampler* noPacketsSampler;
		Sampler* noAcceptsSampler;
		Sampler* noDropsSampler;
		Sampler* noRstSampler;	

		void sample(map<string, long>& input);
	private:
		SLogger::LogContext* logContext;
		SLogger::Logger* logger;
		Config* config;
		InterfaceManager* interfaceManager;
	};
}
#endif
