/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;

#include "Core/Cron.h"
#include "Core/System.h"
#include "Core/Logger.h"

void CronManager::registerJob(CronJob* job) {
	config->getRuntime()->loadOrPut(job->name, &job->cron);
	jobs.push_back(job);
}

void CronManager::roll() {
	lock->lock();
	for (std::list<CronJob*>::const_iterator iter = jobs.begin(); iter != jobs.end(); iter++) {
		CronJob* job = *iter;
		if (job->canRun()) {
			if (job->log) {
				logger->log(SLogger::DEBUG, "CronManager::roll()", "Starting cron job: " + job->name);
			}

			job->run();
			job->setLastRun(time(NULL));

			if (job->log) {
				logger->log(SLogger::DEBUG, "CronManager::roll()", "Finishing cron job: " + job->name);
			}
		}
	}
	lock->unlock();
}

void CronManager::start() {
	new boost::thread(boost::bind(&CronManager::run, this));
}

void CronManager::run(){
	while(true){
		roll();
		sleep(5);
	}
}

bool CronJob::canRun() const {
	if (((time(NULL) - lastRun) > cron)) {
		return true;
	}
	return false;
}

void CronJob::setLastRun(int t) {
	lastRun = t;
}

