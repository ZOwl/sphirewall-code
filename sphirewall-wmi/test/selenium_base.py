#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver

class SeleniumTester:
    url = "http://localhost:5000/logout"

    def connect(self):
        self.driver = webdriver.Firefox();
        self.driver.implicitly_wait(5)
        self.driver.get(self.url)

    def disconnect(self):
        self.driver.quit()

    def login(self, host, username, password):
        usernameField = self.driver.find_element_by_name("username")
        passwordField = self.driver.find_element_by_name("password")
        hostnameField = self.driver.find_element_by_name("host")
        submitButton = self.driver.find_element_by_name("login")

        usernameField.send_keys(username)
        passwordField.send_keys(password)
        hostnameField.clear()
        hostnameField.send_keys(host)
        submitButton.click()

    def check_exists_by_id(self, id):
        try:
            self.driver.find_element_by_id(id)
        except NoSuchElementException:
            return False
        return True

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True

    def clickOptions(self, option):
        self.driver.find_element_by_xpath("//a[contains(text(), 'Actions')]").click()
        self.driver.find_element_by_xpath("//a[contains(text(), '"+option+"')]").click()

    def select(self, name):
        if self.driver.find_element_by_name(name).get_attribute("checked"):
            self.driver.find_element_by_name(name).click()

        self.driver.find_element_by_name(name).click()

    def check_selected(self, name):
        return self.driver.find_element_by_name(name).get_attribute(name)

    def setValue(self, name, value):
        self.driver.find_element_by_name(name).clear()
        self.driver.find_element_by_name(name).send_keys(value)