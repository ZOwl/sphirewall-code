#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester

class AuthenticationTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester();
        self.tester.connect();
        self.tester.login("fw", "admin", "admin")

    def gotoUsersAndPeople(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Configure')]").click();
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Users and People')]").click();

    def gotoGroupsAndRoles(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Configure')]").click();
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Groups and Roles')]").click();

    def test_createNewUser(self):
        self.gotoUsersAndPeople()
        name = self.tester.driver.find_element_by_name("username")
        name.send_keys("michaell")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath("//table//a[contains(text(), 'michaell')]"))
        self.deleteUser("michaell")

    def test_deleteUser(self):
        self.gotoUsersAndPeople()
        name = self.tester.driver.find_element_by_name("username")
        name.send_keys("michael2")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()
        self.deleteUser("michael2")

        self.assertFalse(self.tester.check_exists_by_xpath("//table//a[contains(text(), 'michael2')]"))

    def test_modifyUserDetails(self):
        self.gotoUsersAndPeople()
        name = self.tester.driver.find_element_by_name("username")
        name.send_keys("michael3")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath("//table//a[contains(text(), 'michael3')]"))
        self.tester.driver.find_element_by_xpath("//table//a[contains(text(), 'michael3')]").click();

        self.tester.driver.find_element_by_name("fname").send_keys("Michael")
        self.tester.driver.find_element_by_name("lname").send_keys("Lawson")
        self.tester.driver.find_element_by_name("email").send_keys("michael@sphinix.com")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertEquals(self.tester.driver.find_element_by_name("fname").get_attribute("value"), "Michael")
        self.assertEquals(self.tester.driver.find_element_by_name("lname").get_attribute("value"), "Lawson")
        self.assertEquals(self.tester.driver.find_element_by_name("email").get_attribute("value"),
            "michael@sphinix.com")

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Users and People')]").click();
        self.deleteUser("michael3")

#   Not possible due to some func we are doing using js
#    def test_addRemoveUserToGroup(self):
#        self.gotoUsersAndPeople()
#        name = self.tester.driver.find_element_by_name("username")
#        name.send_keys("groupuser")
#        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()
#        self.tester.driver.find_element_by_xpath("//table//a[contains(text(), 'groupuser')]").click();
#
#        self.tester.driver.find_element_by_xpath("//select[@name='groups']/option[text()='defaultadmin']").click()
#        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()
#
#        self.assertTrue(self.tester.driver.execute_script("$('#groups').val()") == "[\"defaultadmin\"]")
#
#        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Users and People')]").click();
#        self.deleteUser("groupuser")

    def test_addQuota(self):
        self.gotoUsersAndPeople()

        name = self.tester.driver.find_element_by_name("username")
        name.send_keys("quotauser")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()
        self.tester.driver.find_element_by_xpath("//table//a[contains(text(), 'quotauser')]").click();

        self.tester.driver.find_element_by_name("dailyQuota").click()
        self.tester.driver.find_element_by_name("dailyQuotaLimit").clear()
        self.tester.driver.find_element_by_name("dailyQuotaLimit").send_keys("10")

        self.tester.driver.find_element_by_name("weeklyQuota").click()
        self.tester.driver.find_element_by_name("weeklyQuotaLimit").clear()
        self.tester.driver.find_element_by_name("weeklyQuotaLimit").send_keys("20")

        self.tester.driver.find_element_by_name("monthQuota").click()
        self.tester.driver.find_element_by_name("monthQuotaIsSmart").click()
        self.tester.driver.find_element_by_name("monthQuotaLimit").clear()
        self.tester.driver.find_element_by_name("monthQuotaLimit").send_keys("30")
        self.tester.driver.find_element_by_name("monthQuotaBillingDay").clear()
        self.tester.driver.find_element_by_name("monthQuotaBillingDay").send_keys("1")

        self.tester.driver.find_element_by_name("totalQuota").click()
        self.tester.driver.find_element_by_name("totalQuotaLimit").clear()
        self.tester.driver.find_element_by_name("totalQuotaLimit").send_keys("40")

        self.tester.driver.find_element_by_name("timeQuota").click()
        self.tester.driver.find_element_by_name("timeQuotaLimit").clear()
        self.tester.driver.find_element_by_name("timeQuotaLimit").send_keys("50")
        self.tester.driver.find_element_by_name("save").click()

        #check the values:
        self.assertEquals(self.tester.driver.find_element_by_name("dailyQuotaLimit").get_attribute("value"), "10")
        self.assertEquals(self.tester.driver.find_element_by_name("weeklyQuotaLimit").get_attribute("value"), "20")
        self.assertEquals(self.tester.driver.find_element_by_name("monthQuotaLimit").get_attribute("value"), "30")
        self.assertEquals(self.tester.driver.find_element_by_name("monthQuotaBillingDay").get_attribute("value"), "1")
        self.assertEquals(self.tester.driver.find_element_by_name("totalQuotaLimit").get_attribute("value"), "40")
        self.assertEquals(self.tester.driver.find_element_by_name("timeQuotaLimit").get_attribute("value"), "50")

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Users and People')]").click()
        self.deleteUser("quotauser")

    def test_addGroup(self):
        self.gotoGroupsAndRoles()
        name = self.tester.driver.find_element_by_name("name")
        name.send_keys("group1")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'group1')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'group1')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'group1')]"))

    def deleteUser(self, username):
        self.tester.driver.find_element_by_xpath(
            "//table//a[contains(text(), '" + username + "')]//ancestor::tr/td[last()]//a[contains(text(), 'Delete')]").click()

    def tearDown(self):
        self.tester.disconnect()
