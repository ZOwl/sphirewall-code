/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_CONN_H
#define SPHIREWALL_CONN_H

#include "State.h"
#include "Packet.h"
#include "Acl.h"
#include "Utils/Lock.h"
#include "Auth/User.h"
#include "TrafficShaper/TokenBucket.h"
#include "Rewrite.h"
namespace SFwallCore {

	class Connection {
	public:

		/*Constructors and Destructors*/
		Connection() : pleaseTerminate(-1) {
		}
		Connection(Packet*);

		static Connection* parseConnection(Packet* packet);
		virtual ~Connection();

		/*Getters*/
		NetDevice* getSourceNetDevice();
		NetDevice* getDestNetDevice(); 

		int getDestDev() const;
		int getSourceDev() const;
		in_addr_t getSrcIp() const;
		in_addr_t getDstIp() const;
		string getHwAddress() const;

		int hash() const;
		int getTime() const;

		Session* getSession() const;
		TokenBucket* getQosBucket() const;
		virtual std::string toString() = 0;

		/*Setters*/
		void increment(const Packet* packet);
		void setQosBucket(TokenBucket* bucket);
		void setSession(Session* session);

		int idle() const;
		int getNoPackets() const;
		long getDownload() const;
		long getUpload() const;
		int getUpTransfer() const;
		int getDownTransfer() const;
		void setMustLog(bool i) {
			this->log = i;
		}
		/*Other*/
		bool capturePortal;

		enum PacketDirection {
			DIR_SAME = 1, DIR_OPPOSITE, DIR_ANY
		};

		typedef unsigned short NatId;

		bool checkPacket(const Packet* packet, PacketDirection filter) const;
		bool hasExpired();

		void terminateNext();
		void terminate();
		bool isTerminating();

		//Information to help the analytics engine out
		void setHttpHost(std::string host){
			this->httpHost = host;
		}	

		std::string getHttpHost(){
			return httpHost;
		}

		proto getProtocol() const {
			return this->protocol;
		}

		int getDestinationPort() const {
			return this->m_dport;
		}

		int getSourcePort() const {
			return this->m_sport;
		}

		static int hash(Packet* tcpPacket);
		bool mustLog(){
			return log;
		}

		bool hasHttpHost() const {
			return httpHostSet;
		}
	
		void setRewriteRule(RewriteRule* rewriteRule){
			if(this->rewriteRule){
				delete this->rewriteRule;
			}

			this->rewriteRule = rewriteRule;
		}
	
		RewriteRule* getRewriteRule() const {
			return rewriteRule;	
		}
	
		int nice;

		void setRewriteVerdictApplied(){
			rewriteVerdictApplied = true;
		}
		bool isRewriteVerdictApplied(){
			return rewriteVerdictApplied;
		}
	protected:
		bool rewriteVerdictApplied;
		RewriteRule* rewriteRule;
		TokenBucket* qos_bucket;

		void updateTransferRate(const SFwallCore::Packet*);

		int sourceDev;
		int destDev;

		string hwAddress;
		long upload, download;
		unsigned int m_sourceIp;
		unsigned int m_destIp;
		int m_sport;
		int m_dport;		

		int m_hash;
		Session* sessionPtr;

		long m_last_packet;
		int m_time_stamp;
		int noPackets;
		int m_icmpId;

		int pleaseTerminate;

		//Information to help the analytics engine out: Not required by firewall
		std::string httpHost;
		proto protocol;
		bool log;
		bool httpHostSet;
	};

	class TcpConnection : public Connection {
		public:
			TcpConnection(TcpPacket* tcpPacket);
			~TcpConnection() {
				delete tcpTrackable;
			}

			TcpTrackable* getTcpTrackable() const {
				return this->tcpTrackable;
			}

			TcpState* getState();

			std::string toString();
			TcpTrackable* tcpTrackable;
	};

	class UdpConnection : public Connection {
		public:
			UdpConnection(UdpPacket* udpPacket);

			~UdpConnection() {
			}

			std::string toString();

	};

	class IcmpConnection : public Connection {
		public:
			IcmpConnection(IcmpPacket* icmpPacket);

			~IcmpConnection() {
			}

			std::string toString();
	};
};

#endif
