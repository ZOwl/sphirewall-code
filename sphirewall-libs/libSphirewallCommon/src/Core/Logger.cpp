/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <queue>
#include <syslog.h>
#include <sstream>

#include "Core/Logger.h"
#include "Utils/Utils.h"
#include "Utils/TimeUtils.h"
using namespace std;

SLogger::LogContext::LogContext(Priority level, std::string name, std::string filename)
: level(level), name(name), filename(filename) {
}

void SLogger::LogContext::log(Priority p, std::string msg) {
	log(p, "unknown", msg, false);
}

void SLogger::LogContext::log(Priority p, std::string function, std::string msg) {
	log(p, function, msg, false);
}

void SLogger::LogContext::log(Priority p, std::string function, std::string msg, bool out){
	if(out){
		std::stringstream o;
                o<< name << "::" << function << " ";
                o<< msg;
		cout << o.str() << endl;
	}

	if (p <= level) {
                Log nLog;
                nLog.priority = p;
                nLog.location = function;
                nLog.message = msg;
                nLog.timestamp = time(NULL);

                logs.push_back(nLog);
        }
}

void SLogger::LogContext::flush() {
	openlog("sphirewall", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);

	for (std::list<Log>::const_iterator iter = logs.begin(); iter != logs.end(); iter++) {
		Log l = *iter;
		std::stringstream msg;
		msg << name << "::" << l.location << " ";
		msg << l.message;

		int syslogPriority = SyslogPriority::getSyslogPriority(l.priority);
		syslog(syslogPriority, msg.str().c_str());

	}
	logs.clear();
	closelog();
}

void SLogger::Logger::log(std::string context, Priority p, std::string msg) {
	//Find context to log this to:
	map<string, LogContext*>::iterator iter = contexts.find(context);
	if (iter != contexts.end()) {
		iter->second->log(p, msg);
	} else {
		//Must use the default context:
		defaultContext->log(p, msg);
	}
}

void SLogger::Logger::log(Priority priority, std::string str) {
	log(priority, false, "unknown", str);
}

void SLogger::Logger::log(Priority priority, std::string location, std::string str) {
	log(priority, false, location, str);
}

void SLogger::Logger::log(Priority priority, bool syslog, std::string location, std::string str) {
	//Old function that will now use the default context:
	log("DEFAULT", priority, str);
}

std::string SLogger::Log::getPriority() {
	switch (priority) {
		case DEBUG:
			return "DEBUG";

		case INFO:
			return "INFO";

		case EVENT:
			return "EVENT";

		case ERROR:
			return "ERROR";

		case CRITICAL_ERROR:
			return "CRITICAL_ERROR";

		default:
			return "UNKNOWN";
	};

	return "UNKNOWN";
}

/*All logging is delayed to prevent slowing down main threads*/
void SLogger::Logger::process() {
	while (true) {
		flush();
		sleep(1);
	}
}

void SLogger::Logger::flush(){
	for (map<string, LogContext*>::iterator iter = contexts.begin(); iter != contexts.end(); ++iter) {
		iter->second->flush();
	}
}

SLogger::Logger::Logger() {
	defaultContext = new LogContext(EVENT, "DEFAULT", "sphirewalld");
	contexts["DEFAULT"] = defaultContext;
}

std::list<std::string> SLogger::Logger::getAvailableContexts() {
	list<string> ret;

	map<string, LogContext*>::iterator iter;
	for (iter = contexts.begin();
			iter != contexts.end();
			iter++) {

		ret.push_back(iter->first);
	}

	return ret;
}

SLogger::LogContext* SLogger::Logger::getContext(std::string c) {
	return contexts[c];
}

