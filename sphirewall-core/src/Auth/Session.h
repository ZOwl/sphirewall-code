/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_SESSIONS_H_INCLUDED
#define SPHIREWALL_SESSIONS_H_INCLUDED

#include <vector>

class User;
class Session {
public:
	Session();
	Session(User* user, string str2, string ipaddress);

	string getMac() const;
	string getUserName() const;
	string getIp() const ;
	int getStartTime() const;
	int getSessionLast() const;

	void touch();
	User* getUser() const; 

private:
	string m_macAddr;
	string m_ip;
	int m_session_start_time;
	int m_last_packet_time;

	User *m_user;

};

#endif
