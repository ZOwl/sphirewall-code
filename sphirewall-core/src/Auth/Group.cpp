/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "Auth/Group.h"
#include "Auth/Quota.h"
using namespace std;

Group::Group() {
	allow_mui = false;
	quota = new QuotaInfo();
}

int Group::getId() const {
	return m_id;
}

std::string Group::getName() const {
	return m_name;
}

void Group::setId(int id){
	this->m_id = id;
}

QuotaInfo* Group::getQuota() const {
	return quota;
}

std::string Group::getDesc() const{
	return this->m_desc;
}

/*
        std::string m_name;
        std::string m_file;
        std::string m_desc;
        int m_createTime;
        std::string m_manager;

*/
std::string Group::getManager() const{
	return this->m_manager;
}

bool Group::isAllowMui() const{
	return this->allow_mui;
}

void Group::setDesc(std::string desc){
	this->m_desc = desc;
}

void Group::setManager(std::string manager){
	this->m_manager = manager;
}

void Group::setAllowMui(bool a){
	this->allow_mui = a;
}

