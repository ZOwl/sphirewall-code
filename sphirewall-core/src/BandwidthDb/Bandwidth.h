/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <map>
#include <list>
#include <vector>
#include <queue>

#ifndef SPHIREWALL_BANDWIDTH_H_INCLUDED
#define SPHIREWALL_BANDWIDTH_H_INCLUDED

#include "SFwallCore/Firewall.h"
#include "SFwallCore/Conn.h"
#include "SFwallCore/ConnTracker.h"

class AnalyticsClient;
class Lock;

class ConnectionStatistics {
	public:
		long upload, download;
		unsigned int m_sourceIp;
		unsigned int m_destIp;
		int m_sport;
		int m_dport;
		std::string httpHost;
		SFwallCore::proto protocol;
		std::string sourceDev;
		std::string destDev;
		std::string user;
		int startTime;
		std::string hwAddress;
};

class BandwidthDbPrimer : public SFwallCore::DeleteConnectionHook, public CronJob {
	public:
		void close();
		void setMaxBufferSize(int i);
		void run();

		BandwidthDbPrimer(UserDb* userDb, Config* config, SLogger::Logger* log)
			: transferLock(new Lock()), transferToAnaLock(new Lock()), CronJob(60 * 15, "BANDWIDTHDB_FLUSH_CRON", true), userDb(userDb), conf(config), logger(log) {
				EXCLUDE_NON_WEB = 0; //If this was static, we would not have to init it
				MIN_BANDWIDTH_TRANSFER_THRESHOLD = 100; //If this was static, we would not have to init it

				config->getRuntime()->loadOrPut("BANDWIDTHDB_PRIMER_MAX_BUFSIZE", &maxBufferSize);
				config->getRuntime()->loadOrPut("EXCLUDE_NON_WEB", &EXCLUDE_NON_WEB);
				config->getRuntime()->loadOrPut("MIN_BANDWIDTH_TRANSFER_THRESHOLD", &MIN_BANDWIDTH_TRANSFER_THRESHOLD);
			}

		~BandwidthDbPrimer() {
		}

		void hook(SFwallCore::Connection* connection);
		AnalyticsClient* db();

        private:
		//Performance configuration items
                int maxBufferSize;
		int EXCLUDE_NON_WEB;
		int MIN_BANDWIDTH_TRANSFER_THRESHOLD;

                UserDb* userDb;
                Config* conf;
                SLogger::Logger* logger;
                Lock* transferLock;
		Lock* transferToAnaLock;
                std::vector<ConnectionStatistics*> bufferedConnections;
                void push(ConnectionStatistics* statistic);
};
#endif
