/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Utils.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include "Core/Cloud.h"
#include "SFwallCore/Conn.h"

using namespace std;

int SFwallCore::RewriteEngine::CAPTURE_PORTAL_MODE = 1;
int SFwallCore::RewriteEngine::CAPTURE_PORTAL_FORCE = 0;
int SFwallCore::RewriteEngine::CAPTURE_PORTAL_REQUIRES_HOSTNAME= 0;

SFwallCore::RewriteEngine::RewriteEngine(ConfigurationManager* config, SLogger::LogContext* logger,SFwallCore::AliasDb* aliases) :
	config(config), logger(logger), aliases(aliases){
		System::getInstance()->config.getRuntime()->loadOrPut("CAPTURE_PORTAL_MODE", &CAPTURE_PORTAL_MODE);
		System::getInstance()->config.getRuntime()->loadOrPut("CAPTURE_PORTAL_FORCE", &CAPTURE_PORTAL_FORCE);
		System::getInstance()->config.getRuntime()->loadOrPut("CAPTURE_PORTAL_REQUIRES_HOSTNAME", &CAPTURE_PORTAL_REQUIRES_HOSTNAME);
	}


int SFwallCore::HttpRedirectRewrite::type(){
	return 0;
}

void SFwallCore::HttpRedirectRewrite::rewrite(SFwallCore::Connection* connection, SFwallCore::Packet* packet){
	stringstream finalUrl;
	finalUrl << url;

	ConfigurationManager* config = &System::getInstance()->configurationManager;
	if(config->hasByPath("general:localhostname")){
		finalUrl << "?host=" << config->get("general:localhostname")->string();	
	}else{
		finalUrl << "?host=";
	}

	finalUrl << "&port=8001";
	finalUrl << "&url=" << connection->getHttpHost();
	finalUrl << "&id=" << id;
	finalUrl << "&deviceid=" << System::getInstance()->getCloudConnection()->getDeviceId();
	finalUrl << "&ip=" << IP4Addr::ip4AddrToString(connection->getSrcIp());
	SFwallCore::Utils::redirectToHTTPAuthPortal(packet->getInternalSqp()->raw_packet, packet->getInternalSqp()->len, finalUrl.str());
}

bool SFwallCore::HttpRedirectRewrite::match(SFwallCore::Packet* packet){
	return HttpTcpPacket::matchPort(packet->getSrcPort()) || HttpProxyPacket::matchPort(packet->getSrcPort());
}

void SFwallCore::RewriteEngine::save(){
	ObjectContainer* root = new ObjectContainer(CREL);
	ObjectContainer* websites = new ObjectContainer(CARRAY);
	ObjectContainer* sources = new ObjectContainer(CARRAY);

	for(list<Alias*>::iterator iter = forceAuthSources.begin();
			iter != forceAuthSources.end();
			iter++){
		sources->put(new ObjectWrapper((string) (*iter)->id));
	}

	for(list<Alias*>::iterator witer = forceAuthWebsiteExceptions.begin();
			witer != forceAuthWebsiteExceptions.end();
			witer++){
		websites->put(new ObjectWrapper((string) (*witer)->id));
	}

	root->put("websites", new ObjectWrapper(websites));
	root->put("sources", new ObjectWrapper(sources));
	config->setElement("forceAuthSettings", root);
	config->save();
}

void SFwallCore::RewriteEngine::load(){
	ObjectContainer* root;
	if (config->has("forceAuthSettings")) {
		root = config->getElement("forceAuthSettings");
		ObjectContainer* websites = root->get("websites")->container();
		for(int x= 0; x < websites->size(); x++){
			string id = websites->get(x)->string();
			if(aliases->get(id)){
				forceAuthWebsiteExceptions.push_back(aliases->get(id));
			}
		}

		ObjectContainer* sources = root->get("sources")->container();
		for(int x= 0; x < sources->size(); x++){
			string id = sources->get(x)->string();
			if(aliases->get(id)){
				forceAuthSources.push_back(aliases->get(id));
			}
		}
	}
}

SFwallCore::RewriteRule* SFwallCore::RewriteEngine::process(SFwallCore::Connection* conn, SFwallCore::Packet* packet){
	if(conn->getSession() == NULL){
		if(conn->getProtocol() != TCP || ((TcpConnection*) conn)->getState()->state != TCPUP){
			return NULL;
		}

		if(CAPTURE_PORTAL_FORCE == 1){
			if(CAPTURE_PORTAL_MODE == 1){
				//Lets check the forceAuth ip range
				for(list<Alias*>::iterator iter = forceAuthSources.begin();
						iter != forceAuthSources.end();
						iter++){

					Alias* target = (*iter);
					if(target->searchForNetworkMatch(conn->getSrcIp())){
						if(forceAuthWebsiteExceptions.size() > 0){
							if(conn->hasHttpHost()){
								for(list<Alias*>::iterator witer = forceAuthWebsiteExceptions.begin();
										witer != forceAuthWebsiteExceptions.end();
										witer++){

									WebsiteListAlias* websiteTarget = (WebsiteListAlias*) (*witer);
									if(websiteTarget->checkUrl(conn->getHttpHost())){
										conn->setRewriteVerdictApplied();
										return NULL;
									}
								}		
							}else{
								return NULL;
							}
						}

						conn->setRewriteVerdictApplied();
						stringstream ss; ss << "rewriting http connection for forced login to " << config->get("general:cp_url")->string();
						logger->log(SLogger::EVENT, "SFwallCore::RewriteEngine::process", ss.str());
						return new HttpRedirectRewrite(config->get("general:cp_url")->string());
					}
				}				
			}
		}

		if(conn->capturePortal){
			if(CAPTURE_PORTAL_MODE == 1){
				return new HttpRedirectRewrite(config->get("general:cp_url")->string());
			}
		}
	}

	//Iterate over dynamic rules:
	list<RewriteRequired*>::iterator iter;
	for(iter = rewrites.begin();
			iter != rewrites.end();
			iter++){

		RewriteRequired* rule = (*iter);
		if(rule->match(conn, packet)){
			RewriteRule* ret = rule->get();
			return ret;
		}	
	}

	return NULL;
}

bool SFwallCore::RewriteRequired::match(SFwallCore::Connection*, SFwallCore::Packet*){
	return true;
}

SFwallCore::RewriteRule* SFwallCore::RewriteRequired::get(){
	return new HttpRedirectRewrite(id, url);
}

void SFwallCore::RewriteEngine::clear(RewriteRequired* rewrite){
	list<RewriteRequired*>::iterator iter;
	for(iter = rewrites.begin();
			iter != rewrites.end();
			iter++){

		RewriteRequired* rule = (*iter);
		if(rule == rewrite){
			stringstream ss; ss << "Clearing rewrite engine rule, url is " << rule->getUrl() << " with id " << rule->getId();
			logger->log(SLogger::INFO, ss.str());

			rewrites.erase(iter);
			delete rule;
			return;
		}
	}
}

SFwallCore::RewriteRequired* SFwallCore::RewriteEngine::get(std::string id){
	list<RewriteRequired*>::iterator iter;
	for(iter = rewrites.begin();
			iter != rewrites.end();
			iter++){

		RewriteRequired* rule = (*iter);
		if(rule->getId().compare(id) == 0){
			return rule;
		}
	}
	return NULL;
}

void SFwallCore::RewriteEngine::add(RewriteRequired* rule){
	stringstream ss; ss << "Adding rewrite engine rule for requests, url is " << rule->getUrl()<< " with id " << rule->getId();
	logger->log(SLogger::INFO, ss.str());

	rewrites.push_back(rule);
}

