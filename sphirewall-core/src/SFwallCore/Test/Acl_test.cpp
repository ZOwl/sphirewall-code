/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <gtest/gtest.h>
#include "SFwallCore/Test/TestFactory.h"
#include "SFwallCore/Acl.h"

using namespace SFwallCore;

TEST(AclStore_Filter, rejectMatchIfWithPortButNotTcpOrUdp_source){
	ACLStore* store = new ACLStore();
	FilterRule* filter = new FilterRule();
	filter->enabled = true;
	filter->sport_start = 10;
	filter->sport_end = 10;
	store->listFilterRules().push_back(filter);
	
	bool a;
	Packet* packet = PacketBuilder::createIcmp("10.1.1.1", "10.1.1.2", 10);
	EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}

TEST(AclStore_Filter, rejectMatchIfWithPortButNotTcpOrUdp_dest){
	ACLStore* store = new ACLStore();
	FilterRule* filter = new FilterRule();
	filter->enabled = true;
	filter->dport_start = 10;
	filter->dport_end = 10;
	store->listFilterRules().push_back(filter);
	
	bool a;
	Packet* packet = PacketBuilder::createIcmp("10.1.1.1", "10.1.1.2", 10);
	EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}

TEST(AclStore_Filter, ignoreDisabledRules){
	ACLStore* store = new ACLStore();
	FilterRule* filter = new FilterRule();
	filter->enabled = false;
	store->listFilterRules().push_back(filter);
	
	bool a;
	Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
	EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}

TEST(AclStore_Filter, acceptEnabledRules){
	ACLStore* store = new ACLStore();
	FilterRule* filter = new FilterRule();
	filter->enabled = true;
	store->listFilterRules().push_back(filter);
	
	bool a;
	Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
	EXPECT_TRUE(store->matchFilterAcls(packet, a) != NULL);
}

TEST(AclStore_Filter, reject_destinationIp){
	ACLStore* store = new ACLStore();
	FilterRule* filter = new FilterRule();
	filter->dest = IP4Addr::stringToIP4Addr("10.22.3.1");	
	store->listFilterRules().push_back(filter);
	
	bool a;
	Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
	EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}

TEST(AclStore_Filter, reject_sourceIp){
	ACLStore* store = new ACLStore();
	FilterRule* filter = new FilterRule();
	filter->source= IP4Addr::stringToIP4Addr("10.22.3.1");	
	store->listFilterRules().push_back(filter);
	
	bool a;
	Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
	EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}

TEST(AclStore_Filter, accept_ipFilters){
	ACLStore* store = new ACLStore();
	FilterRule* filter = new FilterRule();
	filter->source= IP4Addr::stringToIP4Addr("10.1.1.1");	
	filter->dest = IP4Addr::stringToIP4Addr("10.1.1.2");	
	store->listFilterRules().push_back(filter);
	
	bool a;
	Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
	EXPECT_TRUE(store->matchFilterAcls(packet, a) != NULL);
}

TEST(AclStore_Filter, reject_type){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
	filter->type = UDP;
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}

TEST(AclStore_Filter, accept_type){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
	filter->type = TCP;
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) != NULL);
}

TEST(AclStore_Filter, reject_destDev){
        NetDevice* device = new NetDevice();
	device->setId(10);

        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
        filter->destDevPtr = device;

        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setDestDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}

TEST(AclStore_Filter, accept_destDev){
	NetDevice* device = new NetDevice();	
	device->setId(1);

        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
	filter->destDevPtr = device; 
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setDestDev(packet, 1);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) != NULL);
}

TEST(AclStore_Filter, reject_sourceDev){
	NetDevice* device = new NetDevice();
        device->setId(10);

        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
        filter->sourceDevPtr = device;
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}

TEST(AclStore_Filter, accept_sourceDev){
        NetDevice* device = new NetDevice();
        device->setId(1);

        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
        filter->sourceDevPtr = device;
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setSourceDev(packet, 1);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) != NULL);
}

TEST(AclStore_Filter, deny_group){
        SLogger::Logger* logger = new SLogger::Logger();

        GroupDb* groupDb = new GroupDb(logger->getDefault(), NULL);
        SessionDb* sessionDb = new SessionDb(logger->getDefault(), NULL, NULL);
	UserDb* userDb = new UserDb(NULL, groupDb, logger->getDefault(), NULL, sessionDb);

	Group* group= groupDb->createGroup("testinggroup");

	User* user = userDb->createUser("user");	
	user->addGroup(groupDb->getGroup("testinggroup"));

	ACLStore* store = new ACLStore(logger->getDefault(), userDb, groupDb, sessionDb, NULL);
        FilterRule* filter = new FilterRule();
	filter->groupid = group->getId();
        store->listFilterRules().push_back(filter);

	bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}

TEST(AclStore_Filter, allow_group){
        SLogger::Logger* logger = new SLogger::Logger();

        GroupDb* groupDb = new GroupDb(logger->getDefault(), NULL);
        SessionDb* sessionDb = new SessionDb(logger->getDefault(), NULL, NULL);
	UserDb* userDb = new UserDb(NULL, groupDb, logger->getDefault(), NULL, sessionDb);

        Group* group= groupDb->createGroup("testinggroup");

	User* user = userDb->createUser("user");	
	user->addGroup(groupDb->getGroup("testinggroup"));

	sessionDb->create(user, "","10.1.1.1");
	ACLStore* store = new ACLStore(logger->getDefault(), userDb, groupDb, sessionDb, NULL);
        FilterRule* filter = new FilterRule();
	filter->groupid = group->getId();
        store->listFilterRules().push_back(filter);
        
	Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
	bool a;
        EXPECT_TRUE(store->matchFilterAcls(packet, a) != NULL);
}

TEST(AclStore_Filter, return_correct_rule_pointer){
	NetDevice* device1 = new NetDevice(); device1->setId(100);
	NetDevice* device2 = new NetDevice(); device2->setId(1);

	ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
        FilterRule* filter2 = new FilterRule();
	filter2->sourceDevPtr= device1;
        filter->sourceDevPtr= device2;
        store->listFilterRules().push_back(filter);
	store->listFilterRules().push_back(filter2);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setSourceDev(packet, 1);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) == filter);
}

TEST(AclStore_Filter, when_matched_increment){
        NetDevice* device1 = new NetDevice(); device1->setId(100);
        NetDevice* device2 = new NetDevice(); device2->setId(1);

        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
        FilterRule* filter2 = new FilterRule();
        filter2->sourceDevPtr= device1;
        filter->sourceDevPtr= device2;

        store->listFilterRules().push_back(filter);
        store->listFilterRules().push_back(filter2);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setSourceDev(packet, 1);
	EXPECT_TRUE(store->matchFilterAcls(packet, a)->count == 1);
}

TEST(AclStore_Filter, accept_geoipaliaschecks){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
	filter->sourceAlias = new IpRangeAlias();
	filter->sourceAlias->addEntry("10.1.1.1-10.1.1.255");
	filter->sourceAlias->addEntry("7.1.1.1-7.1.1.5");
	
        store->listFilterRules().push_back(filter);

        bool a;
        EXPECT_TRUE(store->matchFilterAcls(PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20), a) != NULL);
        EXPECT_TRUE(store->matchFilterAcls(PacketBuilder::createTcp("10.1.1.2", "10.1.1.2", 10, 20), a) != NULL);
        EXPECT_TRUE(store->matchFilterAcls(PacketBuilder::createTcp("10.1.1.254", "10.1.1.2", 10, 20), a) != NULL);
        EXPECT_TRUE(store->matchFilterAcls(PacketBuilder::createTcp("7.1.1.2", "10.1.1.2", 10, 20), a) != NULL);
        EXPECT_FALSE(store->matchFilterAcls(PacketBuilder::createTcp("10.1.10.2", "10.1.1.2", 10, 20), a) != NULL);
}

TEST(AclStore_Filter, accept_sourcealias){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
	filter->sourceAlias = new IpSubnetAlias();
        filter->sourceAlias->addEntry("10.1.1.1/255.255.255.255");
	
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) != NULL);
}

TEST(AclStore_Filter, reject_sourcealias){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
        filter->sourceAlias = new IpSubnetAlias();
        filter->sourceAlias->addEntry("10.1.1.1/255.255.255.255");
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.2", "10.1.1.2", 10, 20);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}

TEST(AclStore_Filter, accept_destalias){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
        filter->destAlias= new IpSubnetAlias();
        filter->destAlias->addEntry("10.1.1.2/255.255.255.255");
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) != NULL);
}

TEST(AclStore_Filter, reject_destalias){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
        filter->destAlias= new IpSubnetAlias();
        filter->destAlias->addEntry("22.3.1.1/255.255.255.255");
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 20);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}

TEST(AclStore_Filter, accept_dst_portrange){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
	filter->dportList.insert(10);
	filter->dportList.insert(100);
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 1223, 10);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) != NULL);
}

TEST(AclStore_Filter, reject_dst_portrange){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
	filter->dportList.insert(20);
	filter->dportList.insert(100);
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 1223, 10);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}

TEST(AclStore_Filter, accept_src_portrange){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
	filter->sportList.insert(10);
	filter->sportList.insert(1223);
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 1223, 10);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) != NULL);
}

TEST(AclStore_Filter, reject_src_portrange){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
	filter->sportList.insert(20);
	filter->sportList.insert(100);
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 1223, 10);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}

TEST(AclStore_Filter, accept_srcport_between){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
	filter->sport_start = 100;
	filter->sport_end = 150;
	
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 110, 10);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) != NULL);
}

TEST(AclStore_Filter, reject_srcport_between){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
        filter->sport_start = 100;
        filter->sport_end = 150;
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 151, 10);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}

TEST(AclStore_Filter, accept_dstport_between){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
	filter->dport_start = 100;
	filter->dport_end = 150;
	
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 110);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) != NULL);
}

TEST(AclStore_Filter, reject_dport_between){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
        filter->dport_start = 100;
        filter->dport_end = 150;
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 10);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}
/*
TEST(AclStore_Filter, rejectTimeframe){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
        filter->startHour = 1;
        filter->finishHour= 2;
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 10);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) == NULL);
}
*/
TEST(AclStore_Filter, acceptTimeframe){
        ACLStore* store = new ACLStore();
        FilterRule* filter = new FilterRule();
        filter->startHour = 0;
        filter->finishHour= 23;
        store->listFilterRules().push_back(filter);

        bool a;
        Packet* packet = PacketBuilder::createTcp("10.1.1.1", "10.1.1.2", 10, 10);
        PacketBuilder::setSourceDev(packet, 100);
        EXPECT_TRUE(store->matchFilterAcls(packet, a) != NULL);
}


