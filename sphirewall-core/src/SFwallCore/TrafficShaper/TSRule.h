/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TS_RULE_H
#define TS_RULE_H

#define RECURSIVE 1;
#define BLOCK 2;

#include "TokenFilter.h"
#include "TokenBucket.h"

class TsRule {
public:
	TsRule(int id);

	int id;
	int downloadRate;
	int uploadRate;

	int getUploadRate() const {
		return uploadRate;
	}

	int getDownloadRate() const {
		return downloadRate;
	}

	int getNoActiveBuckets() {
		return buckets.size();
	}

	int getHits() {
		return hits;
	}

	TokenFilter filter;
	vector<TokenBucket*> buckets;

	void incrementHits() {
		hits++;
	}

private:
	int hits;
};

#endif
