/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ALIAS_H_INCLUDED
#define ALIAS_H_INCLUDED

#include <vector>
#include <set>
#include <string>
#include <map>
#include <boost/unordered_set.hpp>

#include "Core/Logger.h"
#include "Utils/Lock.h"
#include "Utils/IP4Addr.h"

using boost::unordered_set;
class ConfigurationManager;

namespace SFwallCore {
	class ACLStore;
	class Alias;

	enum AliasListSourceType {
		DNS = 0,
		HTTP_FILE = 1,
		MAX_MIND = 2	
	};

	enum AliasType {
		IP_RANGE = 0,
		IP_SUBNET = 1,
		WEBSITE_LIST= 2
	};

        class ListSource {
                public:
			std::string detail;
                        virtual int load(Alias*) = 0;
			virtual AliasListSourceType type() = 0;
			virtual std::string description() = 0;
        };

	class DnsListSource : public ListSource {
		public:
			int load(Alias*);

			AliasListSourceType type() {
				return DNS;
			}

                        std::string description(){
				return "Dns Hostname Source: " + detail;
			}

	};

	class HttpFileSource : public ListSource {
		public:
                        int load(Alias*);
			AliasListSourceType type(){
				return HTTP_FILE;
			}

                        std::string description(){
                                return "Http File: " + detail;
                        }
	};

	class MaxMindSource : public ListSource {
		public:
                        int load(Alias*);
			AliasListSourceType type(){
				return MAX_MIND;
			}

                        std::string description(){
                                return "Maxmind Country: " + detail;
                        }
	};

	class Range {
		public:
			Range(){
				start = -1;
				end = -1;
			}

			unsigned int start;
			unsigned int end;
	};

	struct IpRangeOperator : public std::binary_function<Range, Range, bool>
        {
                bool operator() (Range const& n1, Range const& n2) const;
	};

	struct IpSubnetOperator : public std::binary_function<Range, Range, bool>
        {
                bool operator() (Range const& n1, Range const& n2) const;
	};

	class Alias {
		public:
			std::string id;
			std::string name;
			ListSource* source;		

			Alias() {
				lock = new Lock();
				source = NULL;
			}

			virtual ~Alias(){
				delete lock;
			}

			virtual bool searchForNetworkMatch(unsigned int ip) = 0;
			virtual void addEntry(std::string entry) = 0;
			virtual std::list<std::string> listEntries() = 0;
			virtual void removeEntry(std::string entry) = 0;
			virtual AliasType type() = 0;

			int load(){
				if(source != NULL){
					clear();
					return source->load(this);
				}

				return 0;
			}
			virtual std::string description() = 0;
			void clear();			

		protected:
			Lock* lock;
	};

	class IpRangeAlias : public Alias {
		public:
			~IpRangeAlias(){}			

			bool searchForNetworkMatch(unsigned int ip);
			void addEntry(std::string entry);
			std::list<std::string> listEntries();
			void removeEntry(std::string entry);

			AliasType type(){
				return IP_RANGE;
			}
			std::string description(){
				return "Ip range alias";
			}

		private:
			std::multiset<Range, IpRangeOperator> items;
	};

	class IpSubnetAlias : public Alias {
		public:
			~IpSubnetAlias(){}
			bool searchForNetworkMatch(unsigned int ip);
			void addEntry(std::string entry);
			std::list<std::string> listEntries();
			void removeEntry(std::string entry);

			AliasType type(){
				return IP_SUBNET;
			}
			std::string description(){
				return "Ip subnet alias";
			}

		private:   
			std::multiset<Range, IpSubnetOperator> items;

	};

	class WebsiteListAlias : public Alias {
		public:
			~WebsiteListAlias(){}
			AliasType type() {
				return WEBSITE_LIST;
			}

			std::string description(){
				return "Custom website list";
			}

			bool checkUrl(std::string url);
			bool searchForNetworkMatch(unsigned int ip);
			void addEntry(std::string entry);
			std::list<std::string> listEntries();
			void removeEntry(std::string entry);

		private:
			unordered_set<std::string> items;
	};

	class AliasRemovedListener {
		public:
			virtual void aliasRemoved(Alias* alias) = 0;
	};

	class AliasDb {
		public:
			AliasDb(SLogger::LogContext* logger, ConfigurationManager* configurationManager) :
				logger(logger), configurationManager(configurationManager) {
					resolve = true;
				}

			AliasDb() {
			}

			Alias* get(std::string);
			Alias* getByName(string id);
			int create(Alias* alias);
			int del(Alias* target);
			void load();
			void save();

			std::map<std::string, Alias*> aliases;

			void setAclStore(ACLStore* store) {
				this->store = store;
			}

			void setResolve(bool r) {
				this->resolve = r;
			}

			void registerRemovedListeners(AliasRemovedListener* target){
				this->removedListeners.push_back(target);
			}	
		private:
			SLogger::LogContext* logger;
			ACLStore* store;
			ConfigurationManager* configurationManager;
			bool resolve;

			std::list<AliasRemovedListener*> removedListeners;
	};
}

#endif
