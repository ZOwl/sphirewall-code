/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
using namespace std;

#include "Api/RequestHandler.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Common/sphirewall_bandwidth_types.h"
#include "Database/BandwidthDb.h"
#include "Utils/IP4Addr.h"
#include "Database/TrafficAnalyticsDb.h"
#include "Database/MonitoringDb.h"
#include "Utils/TimeWrapper.h"

double RequestHandler::convertMetric(std::string metric, long long input) {
	if (metric.compare("bytes") == 0) {
		return input;
	} else if (metric.compare("kbytes") == 0) {
		return input / 1024;
	} else if (metric.compare("mbytes") == 0) {
		return input / 1048576;
	} else if (metric.compare("gbytes") == 0) {
		return input / 1073741824;
	}

	return input;
}

static const string TA_PREFIX_KEY = "stats/bandwidth";
static const string TA_INSERT_KEY = "stats/bandwidth/insert";

void RequestHandler::lock(string requestUri){
	if(requestUri.find(TA_PREFIX_KEY) != -1){
		//Need to lock the _ta database
		if(requestUri.compare(TA_INSERT_KEY) == 0){
			bandwidthDb->getTaDb()->lock();
		}else{
			if(!bandwidthDb->getTaDb()->tryLock()){
				throw new LockHeldException();
			}
		}
	}else{
		//Need to lock the metrics db
		bandwidthDb->getMonitoringDb()->lock();
	}
}

void RequestHandler::unlock(string requestUri){
	if(requestUri.find(TA_PREFIX_KEY) != -1){ 
		bandwidthDb->getTaDb()->unlock();
	}else{  
		bandwidthDb->getMonitoringDb()->unlock();
	}       

}



string RequestHandler::handle(string input) {
	JSONValue* value = JSON::Parse(input.c_str());
	if (!value) {
		logger->log(SLogger::ERROR, "handle()", "Could not parse a json request");
		FileUtils::write(StringUtils::genRandom().c_str(),input);
		return "{'code':'-1','message':'could not parse json request'}";
	}

	JSONObject root = value->AsObject();
	try {
		std::string requestUri = root[L"request"]->String();
		lock(requestUri);

		Timer* requestTimer = new Timer();
		requestTimer->start();

		JSONObject outer;
		JSONObject response = process(requestUri, root[L"args"]->AsObject());

		outer.put(L"code", new JSONValue((double) 0));
		outer.put(L"response", new JSONValue(response));

		JSONValue *responseValue = new JSONValue(outer);
		std::string sMsg = WStringToString(responseValue->Stringify());

		requestTimer->stop();
		stringstream logss;
		logss << "Request '" << requestUri << "' took " << requestTimer->value() << "ms";
		logger->log(SLogger::INFO, "handle()", logss.str());	

		unlock(requestUri);
		delete requestTimer;
		delete responseValue;
		delete value;
			
		return sMsg;
	} catch (JSONFieldNotFoundException* e) {
		stringstream estring; estring << "Could not process request, missing field in request: " << e->what();
		logger->log(SLogger::ERROR, "handle()", estring.str());

		bandwidthDb->unlock();
		stringstream ss;
		ss << "{\"code\":-1,\"message\":\"failed to pass request, missing field '" << e->what() << "'\"}";
		return ss.str();
	} catch (DelegateNotFoundException* e) {
		logger->log(SLogger::ERROR, "handle()", "Could not process request, could not find delegate");

		bandwidthDb->unlock();
		return "{\"code\":-1,\"message\":\"could not find delegate\"}";
	} catch (DelegateGeneralException* e) {
		stringstream estring; estring << "Could not process request, " << e->what();
		logger->log(SLogger::ERROR, "handle()", estring.str());

		bandwidthDb->unlock();
		stringstream ss;
		ss << "{\"code\":-1,\"message\":\"" << e->what() << "\"}";
		return ss.str();
	} catch(LockHeldException* e){
		logger->log(SLogger::ERROR, "handle()", "could not process request, lock held");

		bandwidthDb->unlock();
		stringstream ss;
		ss << "{\"code\":-1,\"message\":\"" << "lock currently help on db" << "\"}";
		return ss.str();
	}

	return "";
}

JSONObject RequestHandler::process(string request, JSONObject args) {
	if (request.compare("analytics/version") == 0) {
		JSONObject response;
		response.put(L"version", new JSONValue((string) "0.9.9.7"));
		return response;
	} else if (request.compare("stats/bandwidth/insert") == 0) {
		bandwidthDb->getTaDb()->beginTransaction();
		if(args.has(L"byteThreshold") && args.has(L"retensionPeriod") && args.has(L"daySwitchPeriod")){
			bandwidthDb->getTaDb()->setByteThreshold(args[L"byteThreshold"]->AsNumber());
			bandwidthDb->getTaDb()->setRetensionPeriod(args[L"retensionPeriod"]->AsNumber());
			bandwidthDb->getTaDb()->setDaySwitchPeriod(args[L"daySwitchPeriod"]->AsNumber());
		}

		Timer* addTimer = new Timer();
		addTimer->start();

		JSONArray items = args[L"items"]->AsArray();
		for (unsigned int x = 0; x < items.size(); x++) {
			JSONObject i = items[x]->AsObject();

			taInputSlot in;
			in.sourceIp = IP4Addr::stringToIP4Addr(i[L"sourceIp"]->String());
			in.sourceHw = i[L"hwAddress"]->String();
			in.destIp = IP4Addr::stringToIP4Addr(i[L"destIp"]->String());
			in.user = i[L"user"]->String();
			in.sourcePort = i[L"sourcePort"]->AsNumber();
			in.destPort = i[L"destPort"]->AsNumber();
			in.time = i[L"time"]->AsNumber();
			in.inputDev = i[L"inputDev"]->String();
			in.outputDev = i[L"outputDev"]->String();
			in.protocol = i[L"protocol"]->AsNumber();
			in.httpHost = i[L"httpHost"]->String();			
			in.upload = i[L"upload"]->AsNumber();
			in.download = i[L"download"]->AsNumber();

			bandwidthDb->getTaDb()->add(in);
		}
		addTimer->stop();		

		int reductionCounter = bandwidthDb->getTaDb()->reductionCounter;
		Timer* flushTimer = new Timer();
		flushTimer->start();
		bandwidthDb->getTaDb()->flush();
		flushTimer->stop();

		Timer* postTimer = new Timer();
		postTimer->start();
		bandwidthDb->getTaDb()->addPostHook();
		postTimer->stop();

		if(logger){
			stringstream stats;
			stats << "Import finished, noItems:" << items.size() << " addTimer: " << addTimer->value() << " flushTimer: " << flushTimer->value() << " postTimer:" << postTimer->value() << " reductionCounter:" << reductionCounter;	
			logger->log(SLogger::INFO, "process()", stats.str());
		}

		delete flushTimer;
		delete addTimer;
		delete postTimer;
		bandwidthDb->getTaDb()->endTransaction();
		return JSONObject();

	} else if(request.compare("analytics/stats/bandwidth/dump") == 0){
		Filter filter;
		filter.startTime = Time(args[L"startTime"]->String());
		filter.endTime = Time(args[L"endTime"]->String());

		vector<taSummaryUnit> units;
		bandwidthDb->getTaDb()->dump(units, filter);

		string m = args[L"metric"]->String();
		JSONObject response;
		JSONArray arr;
		for (unsigned int x = 0; x < units.size(); x++) {
			JSONObject u;
			u.put(L"time", new JSONValue((string) units[x].displayTime));
			u.put(L"tcpUp", new JSONValue((double) convertMetric(m, units[x].tcpUp)));
			u.put(L"tcpDown", new JSONValue((double) convertMetric(m, units[x].tcpDown)));
			u.put(L"udpUp", new JSONValue((double) convertMetric(m, units[x].udpUp)));
			u.put(L"udpDown", new JSONValue((double) convertMetric(m, units[x].udpDown)));
			u.put(L"port", new JSONValue((double) units[x].port));
			u.put(L"httpHost", new JSONValue((string) units[x].httpHost));
			u.put(L"username", new JSONValue((string) units[x].username));
			u.put(L"hw", new JSONValue((string) units[x].hw));
			u.put(L"sourceIp", new JSONValue((string) IP4Addr::ip4AddrToString(units[x].sourceIp)));	

			arr.push_back(new JSONValue(u));
		}

		response.put(L"items", new JSONValue(arr));
		return response;

	} else if (request.compare("analytics/stats/bandwidth/query/transfer") == 0){
		vector<taSummaryUnit> units;
		Filter filter;
		filter.startTime = Time(args[L"startTime"]->String());
		filter.endTime = Time(args[L"endTime"]->String());
		if(args.has(L"filter_sourceIp")){
			filter.setSourceIp(IP4Addr::stringToIP4Addr(args[L"filter_sourceIp"]->String()));
		}else if(args.has(L"filter_user")){
			filter.setUser( args[L"filter_user"]->String());
		}else if(args.has(L"filter_hw")){
			filter.setHw(args[L"filter_hw"]->String());
		}

		bandwidthDb->getTaDb()->getTransfer(units, filter);

		string m = args[L"metric"]->String();
		JSONObject response;
		JSONArray arr;
		for (unsigned int x = 0; x < units.size(); x++) {
			JSONObject u;

			u.put(L"time", new JSONValue((string) units[x].displayTime));
			u.put(L"tcpUp", new JSONValue((double) convertMetric(m, units[x].tcpUp)));
			u.put(L"tcpDown", new JSONValue((double) convertMetric(m, units[x].tcpDown)));
			u.put(L"udpUp", new JSONValue((double) convertMetric(m, units[x].udpUp)));
			u.put(L"udpDown", new JSONValue((double) convertMetric(m, units[x].udpDown)));
			arr.push_back(new JSONValue(u));
		}

		response.put(L"items", new JSONValue(arr));
		return response;

	}else if(request.compare("analytics/stats/bandwidth/top") == 0){
		Filter filter;
                filter.startTime = Time(args[L"startTime"]->String());
                filter.endTime = Time(args[L"endTime"]->String());
                if(args.has(L"filter_sourceIp")){
                        filter.setSourceIp(IP4Addr::stringToIP4Addr(args[L"filter_sourceIp"]->String()));
                }else if(args.has(L"filter_user")){
                        filter.setUser( args[L"filter_user"]->String());
                }else if(args.has(L"filter_hw")){
                        filter.setHw(args[L"filter_hw"]->String());
                }
		
		JSONObject response;
		response.put(L"website", new JSONValue((std::string) bandwidthDb->getTaDb()->getTopWebsite(filter)));
		response.put(L"user", new JSONValue((std::string) bandwidthDb->getTaDb()->getTopUser(filter)));
		response.put(L"ip", new JSONValue((std::string) bandwidthDb->getTaDb()->getTopIp(filter)));
		response.put(L"device", new JSONValue((std::string) bandwidthDb->getTaDb()->getTopDevice(filter)));
			
		return response;		

	}else if (request.compare("analytics/stats/bandwidth/query/transfer/sum") == 0) {
		JSONObject response;
		vector<taSummaryUnit> units;
		Filter filter;
		filter.startTime = Time(args[L"startTime"]->String());
		filter.endTime = Time(args[L"endTime"]->String());
		if(args.has(L"filter_sourceIp")){
			filter.setSourceIp(IP4Addr::stringToIP4Addr(args[L"filter_sourceIp"]->String()));
		}else if(args.has(L"filter_user")){
			filter.setUser( args[L"filter_user"]->String());
		}else if(args.has(L"filter_hw")){
			filter.setHw(args[L"filter_hw"]->String());
		}

		bandwidthDb->getTaDb()->getTransfer(units, filter);

		JSONArray arr;
		string m = args[L"metric"]->String();

		long long tcpUp = 0;
		long long tcpDown = 0;
		long long udpUp = 0;
		long long udpDown = 0;

		for (int x = 0; x < units.size(); x++) {
			tcpUp += units[x].tcpUp;
			tcpDown += units[x].tcpDown;
			udpUp += units[x].udpUp;
			udpDown += units[x].udpDown;
		}

		response.put(L"tcpUp", new JSONValue((double) convertMetric(m, tcpUp)));
		response.put(L"tcpDown", new JSONValue((double) convertMetric(m, tcpDown)));
		response.put(L"udpUp", new JSONValue((double) convertMetric(m, udpUp)));
		response.put(L"udpDown", new JSONValue((double) convertMetric(m, udpDown)));

		return response;

	} else if (request.compare("analytics/stats/bandwidth/query/web") == 0) {
		vector<taWebsiteSummaryUnit> units;
		//Check for filters:
		Filter filter;
		filter.startTime = Time(args[L"startTime"]->String());
		filter.endTime = Time(args[L"endTime"]->String());
		if(args.has(L"filter_sourceIp")){
			filter.setSourceIp(IP4Addr::stringToIP4Addr(args[L"filter_sourceIp"]->String()));
		}else if(args.has(L"filter_user")){
			filter.setUser( args[L"filter_user"]->String());
		}else if(args.has(L"filter_hw")){
			filter.setHw(args[L"filter_hw"]->String());
		}else if(args.has(L"limit")){
			filter.setLimit(args[L"limit"]->AsNumber());
		}

		bandwidthDb->getTaDb()->getWebsiteTransfer(units, filter);

		JSONObject response;
		JSONArray  websiteData;
		string m = args[L"metric"]->String();
		for (unsigned int siteIndex = 0; siteIndex < units.size(); siteIndex++)
		{
			JSONObject jObj;

			jObj.put(L"tcpUp", new JSONValue(convertMetric(m, (double) units[siteIndex].tcpUp)));
			jObj.put(L"tcpDown", new JSONValue(convertMetric(m, (double) units[siteIndex].tcpDown)));
			jObj.put(L"udpUp", new JSONValue(convertMetric(m, (double) units[siteIndex].udpUp)));
			jObj.put(L"udpDown", new JSONValue(convertMetric(m, (double) units[siteIndex].udpDown)));

			// Remove invalid characters from host name.
			for (unsigned int index = 0;
					index < units[siteIndex].httpHost.size(); index++)
			{
				if ((units[siteIndex].httpHost[index] < ' ') ||
						(units[siteIndex].httpHost[index] > '~'))
				{
					units[siteIndex].httpHost[index] = '?'; // It's garbage.
				}
			}

			jObj.put(L"httpHost", new JSONValue((string) units[siteIndex].httpHost));
			websiteData.push_back(new JSONValue(jObj));
		}

		response.put(L"items", new JSONValue(websiteData));
		return response;
	}else if(request.compare("analytics/stats/bandwidth/query/web/address") == 0){
		vector<taWebsiteSummaryUnit> units;
		bandwidthDb->getTaDb()->getWebsiteByAddress(units,args[L"httpHost"]->String(), Time(args[L"startTime"]->String()),Time(args[L"endTime"]->String()));

		JSONObject response;
		JSONArray  websiteData;
		string m = args[L"metric"]->String();

		for (unsigned int siteIndex = 0; siteIndex < units.size(); siteIndex++)
		{
			JSONObject jObj;

			jObj.put(L"address", new JSONValue((string)IP4Addr::ip4AddrToString(units[siteIndex].ip)));
			jObj.put(L"user", new JSONValue((string)units[siteIndex].username));
			jObj.put(L"hw", new JSONValue((string)units[siteIndex].hw));
			jObj.put(L"tcpUp", new JSONValue(convertMetric(m, (double) units[siteIndex].tcpUp)));
			jObj.put(L"tcpDown", new JSONValue(convertMetric(m, (double) units[siteIndex].tcpDown)));
			jObj.put(L"udpUp", new JSONValue(convertMetric(m, (double) units[siteIndex].udpUp)));
			jObj.put(L"udpDown", new JSONValue(convertMetric(m, (double) units[siteIndex].udpDown)));
			websiteData.push_back(new JSONValue(jObj));
		}

		response.put(L"items", new JSONValue(websiteData));
		return response;

	} else if (request.compare("analytics/stats/bandwidth/query/device") == 0) {

		vector<taSummaryUnit> units;
		bandwidthDb->getTaDb()->getDeviceSummaryByHour(units, args[L"device"]->String(), args[L"startTime"]->String(),
				args[L"endTime"]->String());
		string m = args[L"metric"]->String();
		JSONObject response;
		JSONArray arr;
		for (unsigned int x = 0; x < units.size(); x++) {
			JSONObject u;

			u.put(L"time", new JSONValue((string) units[x].displayTime));
			u.put(L"tcpUp", new JSONValue((double) convertMetric(m, units[x].tcpUp)));
			u.put(L"tcpDown", new JSONValue((double) convertMetric(m, units[x].tcpDown)));
			u.put(L"udpUp", new JSONValue((double) convertMetric(m, units[x].udpUp)));
			u.put(L"udpDown", new JSONValue((double) convertMetric(m, units[x].udpDown)));
			arr.push_back(new JSONValue(u));
		}

		response.put(L"items", new JSONValue(arr));
		return response;

	} else if (request.compare("analytics/stats/bandwidth/query/device/sum") == 0) {

		vector<taSummaryUnit> units;
		bandwidthDb->getTaDb()->getDeviceSummaryByHour(units, args[L"device"]->String(), args[L"startTime"]->String(),
				args[L"endTime"]->String());
		string m = args[L"metric"]->String();
		JSONObject response;
		double tcpUp = 0, tcpDown = 0, udpUp = 0, udpDown = 0;

		for (unsigned int x = 0; x < units.size(); x++) {
			tcpUp += units[x].tcpUp;
			tcpDown += units[x].tcpDown;
			udpUp += units[x].udpUp;
			udpDown += units[x].udpDown;
		}

		response.put(L"tcpUp", new JSONValue((double) convertMetric(m, tcpUp)));
		response.put(L"tcpDown", new JSONValue((double) convertMetric(m, tcpDown)));
		response.put(L"udpUp", new JSONValue((double) convertMetric(m, udpUp)));
		response.put(L"udpDown", new JSONValue((double) convertMetric(m, udpDown)));

		return response;
	} else if (request.compare("analytics/stats/bandwidth/query/port") == 0) {
		vector<taSummaryUnit> units;
		Filter filter;
		filter.startTime = Time(args[L"startTime"]->String());
		filter.endTime = Time(args[L"endTime"]->String());
		if(args.has(L"filter_sourceIp")){
			filter.setSourceIp(IP4Addr::stringToIP4Addr(args[L"filter_sourceIp"]->String()));
		}else if(args.has(L"filter_user")){
			filter.setUser(args[L"filter_user"]->String());
		}else if(args.has(L"filter_hw")){
			filter.setHw(args[L"filter_hw"]->String());
		}
		bandwidthDb->getTaDb()->getPortTransfer(units, filter);

		string m = args[L"metric"]->String();
		JSONObject response;
		JSONArray arr;
		for (unsigned int x = 0; x < units.size(); x++) {
			JSONObject u;

			u.put(L"time", new JSONValue((string) units[x].displayTime));
			u.put(L"tcpUp", new JSONValue((double) convertMetric(m, units[x].tcpUp)));
			u.put(L"tcpDown", new JSONValue((double) convertMetric(m, units[x].tcpDown)));
			u.put(L"udpUp", new JSONValue((double) convertMetric(m, units[x].udpUp)));
			u.put(L"udpDown", new JSONValue((double) convertMetric(m, units[x].udpDown)));
			u.put(L"port", new JSONValue((double) units[x].port));

			arr.push_back(new JSONValue(u));
		}

		response.put(L"items", new JSONValue(arr));
		return response;

	} else if (request.compare("analytics/stats/bandwidth/query/port/sum") == 0) {
		vector<taSummaryUnit> units;
		Filter filter;
		filter.startTime = Time(args[L"startTime"]->String());
		filter.endTime = Time(args[L"endTime"]->String());
		if(args.has(L"filter_sourceIp")){
			filter.setSourceIp(IP4Addr::stringToIP4Addr(args[L"filter_sourceIp"]->String()));
		}else if(args.has(L"filter_user")){
			filter.setUser(args[L"filter_user"]->String());
		}else if(args.has(L"filter_hw")){
			filter.setHw(args[L"filter_hw"]->String());
		}
		bandwidthDb->getTaDb()->getPortTransfer(units, filter);

		map<int, taSummaryUnit> u;
		for (unsigned int x = 0; x < units.size(); x++) {
			taSummaryUnit* au = &u[units[x].port];

			au->tcpUp = au->tcpUp + units[x].tcpUp;
			au->tcpDown = au->tcpDown + units[x].tcpDown;
			au->udpUp = au->udpUp + units[x].udpUp;
			au->udpDown = au->udpDown + units[x].udpDown;
		}

		string m = args[L"metric"]->String();
		JSONArray arr;
		for (map<int, taSummaryUnit>::iterator iter = u.begin();
				iter != u.end();
				iter++) {

			JSONObject o;
			o.put(L"port", new JSONValue((double) iter->first));
			o.put(L"tcpUp", new JSONValue((double) convertMetric(m, iter->second.tcpUp)));
			o.put(L"tcpDown", new JSONValue((double) convertMetric(m, iter->second.tcpDown)));
			o.put(L"udpUp", new JSONValue((double) convertMetric(m, iter->second.udpUp)));
			o.put(L"udpDown", new JSONValue((double) convertMetric(m, iter->second.udpDown)));

			arr.push_back(new JSONValue(o));
		}

		JSONObject response;
		response.put(L"items", new JSONValue(arr));
		return response;
	} else if (request.compare("analytics/stats/bandwidth/query") == 0) {
		vector<taSummaryUnit> units;
		bandwidthDb->getTaDb()->getDateSummary(units, args[L"startTime"]->String(),
				args[L"endTime"]->String());

		string m = args[L"metric"]->String();
		JSONObject response;
		JSONArray arr;
		for (unsigned int x = 0; x < units.size(); x++) {
			JSONObject u;

			u.put(L"time", new JSONValue((string) units[x].displayTime));
			u.put(L"tcpUp", new JSONValue((double) convertMetric(m, units[x].tcpUp)));
			u.put(L"tcpDown", new JSONValue((double) convertMetric(m, units[x].tcpDown)));
			u.put(L"udpUp", new JSONValue((double) convertMetric(m, units[x].udpUp)));
			u.put(L"udpDown", new JSONValue((double) convertMetric(m, units[x].udpDown)));

			arr.push_back(new JSONValue(u));
		}

		response.put(L"items", new JSONValue(arr));
		return response;
	} else if (request.compare("stats/metrics/insert") == 0) {

		bandwidthDb->getMonitoringDb()->beginTransaction();
		bandwidthDb->getMonitoringDb()->cleanupMetrics();

		JSONArray arr = args[L"items"]->AsArray();
		for (unsigned int x = 0; x < arr.size(); x++) {
			JSONObject o = arr[x]->AsObject();
			bandwidthDb->getMonitoringDb()->addMetric(o[L"key"]->String(), o[L"value"]->AsNumber());
		}

		bandwidthDb->getMonitoringDb()->updateMetricsHour("dude");
		bandwidthDb->getMonitoringDb()->endTransaction();
		return JSONObject();
	}else if(request.compare("analytics/stats/metrics") == 0){
		bandwidthDb->getMonitoringDb()->beginTransaction();
		bandwidthDb->getMonitoringDb()->cleanupMetrics();

		JSONArray arr;
		list<string> avail = bandwidthDb->getMonitoringDb()->availableMetrics();
		for(list<string>::iterator iter = avail.begin();
				iter != avail.end();
				iter++){
			arr.push_back(new JSONValue((string) (*iter)));
		}

		bandwidthDb->getMonitoringDb()->updateMetricsHour("dude");
		bandwidthDb->getMonitoringDb()->endTransaction();

		JSONObject ret;
		ret.put(L"available", new JSONValue((arr)));
		return ret;
	} else if (request.compare("analytics/stats/metrics/get") == 0) {
		std::vector<std::pair<std::string, int> > items;
		bandwidthDb->getMonitoringDb()->getMetrics(items, args[L"key"]->String(),
				Time(args[L"startDate"]->String()),
				Time(args[L"endDate"]->String()));

		JSONObject response;
		JSONArray arr;

		for (unsigned int x = 0; x < items.size(); x++) {
			JSONObject u;

			u.put(L"time", new JSONValue((string) items[x].first));
			u.put(L"value", new JSONValue((double) items[x].second));
			arr.push_back(new JSONValue(u));
		}

		response.put(L"items", new JSONValue(arr));
		return response;
	} else if (request.compare("analytics/stats/metrics/getAllTotals") == 0) {
		std::vector<std::pair<std::string, int> > items;
		bandwidthDb->getMonitoringDb()->getAllMetricsTotals(items, 
				Time(args[L"startDate"]->String()),
				Time(args[L"endDate"]->String()));

		JSONObject response;
		JSONArray arr;

		for (unsigned int x = 0; x < items.size(); x++) {
			JSONObject u;

			u.put(L"name", new JSONValue((string) items[x].first));
			u.put(L"value", new JSONValue((double) items[x].second));
			arr.push_back(new JSONValue(u));
		}

		response.put(L"items", new JSONValue(arr));
		return response;
	} else if (request.compare("analytics/stats/bandwidth/query/top/users") == 0) {
		vector<taSummaryUnit> users;
		bandwidthDb->getTaDb()->getTopUsers(users, args[L"startDate"]->String(), args[L"endDate"]->String(), args[L"limit"]->AsNumber());
		JSONObject ret;
		JSONArray arr;
		for (unsigned int x = 0; x < users.size(); x++) {
			JSONObject u;
			u.put(L"user", new JSONValue((string) users[x].username));
			string m = args[L"metric"]->String();
			u.put(L"tcpUp", new JSONValue((double) convertMetric(m, users[x].tcpUp)));
			u.put(L"tcpDown", new JSONValue((double) convertMetric(m, users[x].tcpDown)));
			u.put(L"udpUp", new JSONValue((double) convertMetric(m, users[x].udpUp)));
			u.put(L"udpDown", new JSONValue((double) convertMetric(m, users[x].udpDown)));

			arr.push_back(new JSONValue(u));
		}

		ret.put(L"users", new JSONValue(arr));

		return ret;
	} else if (request.compare("analytics/stats/bandwidth/query/top/addresses") == 0) {
		vector<taSummaryUnit> users;
		bandwidthDb->getTaDb()->getTopLocalAddresses(users, args[L"startDate"]->String(), args[L"endDate"]->String(), args[L"limit"]->AsNumber());

		JSONObject ret;
		JSONArray arr;
		for (unsigned int x = 0; x < users.size(); x++) {
			JSONObject u;
			u.put(L"address", new JSONValue((string) IP4Addr::ip4AddrToString(users[x].sourceIp)));
			u.put(L"hw", new JSONValue((string) users[x].hw));
			string m = args[L"metric"]->String();

			u.put(L"tcpUp", new JSONValue((double) convertMetric(m, users[x].tcpUp)));
			u.put(L"tcpDown", new JSONValue((double) convertMetric(m, users[x].tcpDown)));
			u.put(L"udpUp", new JSONValue((double) convertMetric(m, users[x].udpUp)));
			u.put(L"udpDown", new JSONValue((double) convertMetric(m, users[x].udpDown)));

			arr.push_back(new JSONValue(u));
		}

		ret.put(L"addresses", new JSONValue(arr));

		return ret;
	} else {
		throw new DelegateNotFoundException(request);
	}
}
