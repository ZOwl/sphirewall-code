#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester

class FirewallAclsTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login("fw", "admin", "admin")

    def gotoRules(self):
        self.clickFirewall()
        self.clickAcl()

    def clickAcl(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Filtering')]").click()

    def clickFirewall(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Firewall')]").click()

    def test_BasicCriteria_Accept(self):
        self.gotoRules()
        self.tester.clickOptions("Create Rule")

        self.tester.driver.find_element_by_name("source").send_keys("12.2.2.2")
        self.tester.driver.find_element_by_name("source_mask").send_keys("255.255.255.240")
        self.tester.driver.find_element_by_name("sport").send_keys("80")

        self.tester.driver.find_element_by_name("dest").send_keys("10.22.22.1")
        self.tester.driver.find_element_by_name("dest_mask").send_keys("255.255.255.0")
        self.tester.driver.find_element_by_name("dport").send_keys("3224")

        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.2/255.255.255.240 port 80,')]//ancestor::tr/td[contains(normalize-space(), '10.22.22.1/255.255.255.0 port 3224,')]//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.2/255.255.255.240 port 80,')]//ancestor::tr/td[contains(normalize-space(), '10.22.22.1/255.255.255.0 port 3224,')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.2/255.255.255.240 port 80,')]"))

    def test_ListOfPorts(self):
        self.gotoRules()
        self.tester.clickOptions("Create Rule")

        self.tester.driver.find_element_by_name("source").send_keys("12.2.2.2")
        self.tester.driver.find_element_by_name("source_mask").send_keys("255.255.255.240")
        self.tester.driver.find_element_by_name("sport").send_keys("10,20,30")

        self.tester.driver.find_element_by_name("dest").send_keys("10.22.22.1")
        self.tester.driver.find_element_by_name("dest_mask").send_keys("255.255.255.0")
        self.tester.driver.find_element_by_name("dport").send_keys("1,2,3,4")

        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.2/255.255.255.240 port 10,20,30,')]//ancestor::tr/td[contains(normalize-space(), '10.22.22.1/255.255.255.0 port 1,2,3,4,')]//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.2/255.255.255.240 port 10,20,30,')]//ancestor::tr/td[contains(normalize-space(), '10.22.22.1/255.255.255.0 port 1,2,3,4,')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.2/255.255.255.240 port 10,20,30,')]"))

    def test_PortRange(self):
        self.gotoRules()
        self.tester.clickOptions("Create Rule")

        self.tester.driver.find_element_by_name("source").send_keys("12.2.2.2")
        self.tester.driver.find_element_by_name("source_mask").send_keys("255.255.255.240")
        self.tester.driver.find_element_by_name("sport_start").send_keys("10")
        self.tester.driver.find_element_by_name("sport_end").send_keys("20")

        self.tester.driver.find_element_by_name("dest").send_keys("10.22.22.1")
        self.tester.driver.find_element_by_name("dest_mask").send_keys("255.255.255.0")
        self.tester.driver.find_element_by_name("dport_start").send_keys("1")
        self.tester.driver.find_element_by_name("dport_end").send_keys("5")

        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.2/255.255.255.240 between ports 10-20')]//ancestor::tr/td[contains(normalize-space(), '10.22.22.1/255.255.255.0 between ports 1-5')]//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.2/255.255.255.240 between ports 10-20')]//ancestor::tr/td[contains(normalize-space(), '10.22.22.1/255.255.255.0 between ports 1-5')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.2/255.255.255.240 between ports 10-20')]"))


    def test_BasicCriteria_Deny(self):
        self.gotoRules()
        self.tester.clickOptions("Create Rule")

        self.tester.driver.find_element_by_name("source").send_keys("12.2.2.10")
        self.tester.driver.find_element_by_name("source_mask").send_keys("255.255.255.240")
        self.tester.driver.find_element_by_xpath("//input[@value='DROP']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.10/255.255.255.240')]//ancestor::tr/td/font/b[contains(normalize-space(), 'deny')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.10/255.255.255.240')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()


    def test_Enable_Disable(self):
        self.gotoRules()
        self.tester.clickOptions("Create Rule")

        self.tester.driver.find_element_by_name("source").send_keys("12.2.2.8")
        self.tester.driver.find_element_by_name("source_mask").send_keys("255.255.255.240")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.8/255.255.255.240')]"))

        enabled = self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.8/255.255.255.240')]//ancestor::tr/td/form/input")
        self.assertFalse(enabled.is_selected())

        enabled.click()
        enabled = self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.8/255.255.255.240')]//ancestor::tr/td/form/input")
        self.assertTrue(enabled.is_selected())

        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.8/255.255.255.240')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()

    def test_Aliases(self):
        self.gotoRules()
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Pools')]").click()

        self.tester.driver.find_element_by_name("name").send_keys("test-alias1")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[normalize-space()='IP Range List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_name("name").send_keys("test-destalias")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[normalize-space()='IP Range List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.clickAcl()
        self.tester.clickOptions("Create Rule")
        self.tester.driver.find_element_by_xpath("//select[@name='source_alias']/option[normalize-space()='test-alias1']").click()
        self.tester.driver.find_element_by_xpath("//select[@name='dest_alias']/option[normalize-space()='test-destalias']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'alias test-alias1')]//ancestor::tr/td[contains(normalize-space(),'alias test-destalias')]//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'alias')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()

        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Pools')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(normalize-space(), 'test-alias1')]//ancestor::tr/td/a[contains(normalize-space(), 'Delete')]").click()

    def test_Devices(self):
        self.gotoRules()
        self.tester.clickOptions("Create Rule")

        self.tester.driver.find_element_by_xpath("//select[@name='source_dev']/option[@value='lo']").click()
        self.tester.driver.find_element_by_xpath("//select[@name='dest_dev']/option[@value='eth0']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'interface lo')]//ancestor::tr/td[contains(normalize-space(), 'interface eth0')]//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(),'interface lo')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()

    def test_Groups(self):
        self.gotoRules()
        self.tester.clickOptions("Create Rule")
        self.tester.driver.find_element_by_id("advanceInfoLink").click()
        self.tester.driver.find_element_by_xpath("//select[@name='groupid']/option[text()='defaultadmin']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'group defaultadmin')]//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(),'group defaultadmin')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()

    def test_Protocols(self):
        self.gotoRules()
        self.tester.clickOptions("Create Rule")
        self.tester.driver.find_element_by_id("advanceInfoLink").click()

        self.tester.driver.find_element_by_xpath("//select[@name='protocol']/option[normalize-space()='TCP']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'TCP')]//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(),'TCP')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()

    def test_MacAddress(self):
        self.gotoRules()
        self.tester.clickOptions("Create Rule")
        self.tester.driver.find_element_by_id("advanceInfoLink").click()

        self.tester.driver.find_element_by_name("mac").send_keys("aa:bb:cc:dd:ee:ff")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'mac address aa:bb:cc:dd:ee:ff')]//ancestor::tr/td/font/b[contains(normalize-space(), 'allow')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(),'mac address aa:bb:cc:dd:ee:ff')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()

    def test_logAndIgnoreTracking(self):
        self.gotoRules()
        self.tester.clickOptions("Create Rule")
        self.tester.driver.find_element_by_id("advanceInfoLink").click()

        self.tester.select("log")
        self.tester.select("ignoreconntrack")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td/*[contains(normalize-space(),'dont track')]"))
        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td/*[contains(normalize-space(),'log')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/*[contains(normalize-space(),'log')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()

    def test_startHourStopHour(self):
        self.gotoRules()
        self.tester.clickOptions("Create Rule")
        self.tester.driver.find_element_by_id("advanceInfoLink").click()

        self.tester.setValue("startHour", 1)
        self.tester.setValue("finishHour", 22)
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(),'allow between 1 and 22')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(),'allow between 1 and 22')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()

    def tearDown(self):
        self.tester.disconnect()
