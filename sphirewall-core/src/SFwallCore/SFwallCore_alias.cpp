/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream> 
#include <sstream>
#include <vector>
#include <map>

using namespace std;

#include "Core/System.h"
#include "Utils/IP4Addr.h"
#include "SFwallCore/Alias.h"
#include "SFwallCore/ConnTracker.h"
#include "Utils/Utils.h"
#include "Utils/NetworkUtils.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include <curl/curl.h>
#include "Api/JsonManagementService.h"
#include "SFwallCore/ApplicationLevelFilter.h"
#include "Utils/HttpRequestWrapper.h"

SFwallCore::Alias* SFwallCore::AliasDb::get(string id) {
        for (map<string, Alias*>::iterator iter = aliases.begin();
                        iter != aliases.end();
                        ++iter) {
		if(iter->first.compare(id) == 0){
			return iter->second;
		}
	}
	return NULL;
}

SFwallCore::Alias* SFwallCore::AliasDb::getByName(string id) {
        for (map<string, Alias*>::iterator iter = aliases.begin();
                        iter != aliases.end();
                        ++iter) {
		if(iter->second->name.compare(id) == 0){
			return iter->second;
		}
	}
	return NULL;
}

void SFwallCore::AliasDb::load() {
	ObjectContainer* root;
	if (configurationManager->has("aliasDb")) {
		root = configurationManager->getElement("aliasDb");

		ObjectContainer* arr = root->get("aliases")->container();
		for (int x = 0; x < arr->size(); x++) {
			Alias* alias = NULL;
			ObjectContainer* o = arr->get(x)->container();
			int type = o->get("type")->number();
			std::string name = o->get("name")->string();
			std::string id = o->get("id")->string();
			switch((AliasType)type){
				case IP_RANGE:{
						      alias = new IpRangeAlias();
						      break;
					      }

				case IP_SUBNET: {
							alias = new IpSubnetAlias();
							break;
						}

				case WEBSITE_LIST : {
							    alias = new WebsiteListAlias();
							    break;
						    }
			}

			if(alias){
				alias->name = name;
				alias->id = id;	
				
				if(o->has("source")){
					AliasListSourceType source = (AliasListSourceType) o->get("source")->number();
					switch(source){
						case DNS:{
								 alias->source = new DnsListSource();
								 alias->source->detail = o->get("detail")->string();
								 break;
							 }
						case HTTP_FILE:{
								       alias->source = new HttpFileSource();
								       alias->source->detail = o->get("detail")->string();
								       break;
							       }
						case MAX_MIND:{
								      alias->source = new MaxMindSource();
								      alias->source->detail = o->get("detail")->string();
								      break;
							      }
					};

				}else{
					ObjectContainer* values = o->get("values")->container();
					for(int z = 0; z < values->size(); z++){
						alias->addEntry(values->get(z)->string());
					}
				}
			}
			alias->load();
			aliases[alias->id] = alias;	
		}
	}
}

void SFwallCore::AliasDb::save() {
	ObjectContainer* root = new ObjectContainer(CREL);

	map<string, Alias*>::iterator iter = aliases.begin();
	ObjectContainer* arr = new ObjectContainer(CARRAY);
	while (iter != aliases.end()) {
		ObjectContainer* o = new ObjectContainer(CREL);
		Alias* temp = iter->second;
		o->put("id", new ObjectWrapper((string) temp->id));
		o->put("name", new ObjectWrapper((string) temp->name));
		o->put("type", new ObjectWrapper((double) temp->type()));

		if(temp->source != NULL){
			o->put("source", new ObjectWrapper((double) temp->source->type()));
			o->put("detail", new ObjectWrapper((string) temp->source->detail));
		}else{
			ObjectContainer* values = new ObjectContainer(CARRAY);

			list<string> i = temp->listEntries();
			list<string>::iterator viter;
			for(viter = i.begin(); viter != i.end(); viter++){
				values->put(new ObjectWrapper((string) (*viter)));	
			}
			o->put("values", new ObjectWrapper(values));	
		}

		arr->put(new ObjectWrapper(o)); iter++;
	}

	root->put("aliases", new ObjectWrapper(arr));

	if(configurationManager){
		configurationManager->setElement("aliasDb", root);
		configurationManager->save();
	}
}

std::string remoteBuffer;
size_t function( char *ptr, size_t size, size_t nmemb, void *userdata){
	remoteBuffer += string(ptr);
	return size * nmemb;
}

int SFwallCore::AliasDb::create(Alias* alias) {
	alias->id = StringUtils::genRandom(); 
	aliases[alias->id] = alias;

	int ret = alias->load();
	save();
	return ret;
}

int SFwallCore::AliasDb::del(Alias* target) {
	std::string key = target->id;

	for(list<AliasRemovedListener*>::iterator iter = removedListeners.begin();
		iter != removedListeners.end();
		iter++){
		
		(*iter)->aliasRemoved(target);
	}	

	map<string, Alias*>::iterator iter = aliases.find(key);
	if (iter != aliases.end()) {
		aliases.erase(iter);
	}

	save();
	return 0;
}

bool SFwallCore::IpRangeAlias::searchForNetworkMatch(unsigned int ip){
	if(lock->tryLock()){
		Range target;
		target.start = ip;
		bool res = items.find(target) != items.end();
	
		lock->unlock();
		return res;
	}
	return false;
}

void SFwallCore::IpRangeAlias::addEntry(std::string entry){
	vector<string> csv;
	split(entry, '-', csv);
        if(csv.size() != 2){
                return;
        }

	std::string startIp = StringUtils::trim(csv[0], "\"");
	std::string endIp = StringUtils::trim(csv[1], "\"");

	Range range;
	range.start = IP4Addr::stringToIP4Addr(startIp);
	range.end = IP4Addr::stringToIP4Addr(endIp);	
	items.insert(range);
}

std::list<std::string> SFwallCore::IpRangeAlias::listEntries(){
	std::list<std::string> ret;
	std::multiset<Range>::iterator iter;
	for (iter = items.begin(); iter != items.end(); iter++) {
		stringstream ss;
		ss << IP4Addr::ip4AddrToString((*iter).start);
		ss << "-";
		ss << IP4Addr::ip4AddrToString((*iter).end);
		ret.push_back(ss.str());	
	}

	return ret;
}

void SFwallCore::IpRangeAlias::removeEntry(std::string entry){
	vector<string> csv;
	split(entry, '-', csv);
        if(csv.size() != 2){
                return;
        }

	std::string startIp = StringUtils::trim(csv[0], "\"");
	std::string endIp = StringUtils::trim(csv[1], "\"");

	unsigned int first = IP4Addr::stringToIP4Addr(startIp);
	unsigned int second = IP4Addr::stringToIP4Addr(endIp);

	std::multiset<Range>::iterator iter;
	for (iter = items.begin(); iter != items.end(); iter++) {
		if(first == (*iter).start && second == (*iter).end){
			items.erase(iter);
			break;
		}	
	}
}

bool SFwallCore::IpSubnetAlias::searchForNetworkMatch(unsigned int ip){
        if(lock->tryLock()){
                Range target;
                target.start = ip;
                bool res = items.find(target) != items.end();

                lock->unlock();
                return res;
        }
        return false;
}

void SFwallCore::IpSubnetAlias::addEntry(std::string entry){
	vector<string> csv;
	split(entry, '/', csv);
	if(csv.size() != 2){
		return;
	}	
	
	std::string startIp = StringUtils::trim(csv[0], "\"");
	std::string endIp = StringUtils::trim(csv[1], "\"");

        Range range;
        range.start = IP4Addr::stringToIP4Addr(startIp);
        range.end = IP4Addr::stringToIP4Addr(endIp);
        items.insert(range);
}

std::list<std::string> SFwallCore::IpSubnetAlias::listEntries(){
        std::list<std::string> ret;
        std::multiset<Range>::iterator iter;
        for (iter = items.begin(); iter != items.end(); iter++) {
                stringstream ss;
                ss << IP4Addr::ip4AddrToString((*iter).start);
                ss << "/";
                ss << IP4Addr::ip4AddrToString((*iter).end);
                ret.push_back(ss.str());
        }

        return ret;
}

void SFwallCore::IpSubnetAlias::removeEntry(std::string entry){
	vector<string> csv;
	split(entry, '/', csv);
        if(csv.size() != 2){
                return;
        }

	std::string startIp = StringUtils::trim(csv[0], "\"");
	std::string endIp = StringUtils::trim(csv[1], "\"");

	unsigned int first = IP4Addr::stringToIP4Addr(startIp);
	unsigned int second = IP4Addr::stringToIP4Addr(endIp);

        std::multiset<Range>::iterator iter;
        for (iter = items.begin(); iter != items.end(); iter++) {
                if(first == (*iter).start && second == (*iter).end){
                        items.erase(iter);
                        break;
                }
        }
}

bool SFwallCore::WebsiteListAlias::searchForNetworkMatch(unsigned int ip){
	return false;
}

bool SFwallCore::WebsiteListAlias::checkUrl(std::string url){
	lock->lock();
	bool ret = items.find(url) != items.end();
	lock->unlock();
	return ret;
}

void SFwallCore::WebsiteListAlias::addEntry(std::string entry){
	lock->lock();
	items.insert(entry);
	lock->unlock();
}

std::list<std::string> SFwallCore::WebsiteListAlias::listEntries(){
	lock->lock();
	std::list<std::string> ret;
	unordered_set<std::string>::iterator iter;
	for (iter = items.begin(); iter != items.end(); iter++) {
		ret.push_back((*iter));
	}
	
	lock->unlock();
	return ret;
}

void SFwallCore::WebsiteListAlias::removeEntry(std::string entry){
	lock->lock();
	items.erase(entry);
	lock->unlock();
}

int SFwallCore::DnsListSource::load(Alias* alias){
	if(alias->type() != IP_SUBNET){
		if(System::getInstance()->getFirewall() && System::getInstance()->getFirewall()->getLogContext()){
			System::getInstance()->getFirewall()->getLogContext()->log(SLogger::INFO, " Tried to add a alias with source as dns with invalid format");
		}

		return -2;
	}

	System::getInstance()->getFirewall()->getLogContext()->log(SLogger::INFO, "Loading Hostname alias for " + detail);
	vector<unsigned int> hosts = hostToIPAddresses(detail);

	for (int x = 0; x < (int) hosts.size(); x++) {
		unsigned int ip = hosts[x];
		stringstream ss;
		ss << IP4Addr::ip4AddrToString(ip) << "/255.255.255.255";
		alias->addEntry(ss.str());	
	}
	return 0;
}

int SFwallCore::HttpFileSource::load(Alias* alias){
	if(System::getInstance()->getFirewall()&& System::getInstance()->getFirewall()->getLogContext()){
		System::getInstance()->getFirewall()->getLogContext()->log(SLogger::INFO, "Loading HttpList alias from " + detail);
	}

	HttpRequestWrapper* request = new HttpRequestWrapper(detail.c_str(), GET);
	try{	
		vector<string> items;
		split(request->execute(), '\n', items);
		for(int x= 0; x < items.size(); x++){
			alias->addEntry(items[x]);
		}
	}catch(HttpRequestWrapperException* ex){
		if(System::getInstance()->getFirewall()&& System::getInstance()->getFirewall()->getLogContext()){
			System::getInstance()->getFirewall()->getLogContext()->log(SLogger::ERROR, "Failed to load HttpList alias from " + detail);
		}
		delete request;
		return -1;
	}

	delete request;
	return 0;
}

int SFwallCore::MaxMindSource::load(Alias* alias){
	if(alias->type() != IP_RANGE){
		if(System::getInstance()->getFirewall() && System::getInstance()->getFirewall()->getLogContext()){
			System::getInstance()->getFirewall()->getLogContext()->log(SLogger::INFO, " Tried to add a alias with source as maxmind with invalid format");
		}

		return -2;
	}

	if(System::getInstance()->getFirewall() && System::getInstance()->getFirewall()->getLogContext()){
		System::getInstance()->getFirewall()->getLogContext()->log(SLogger::INFO, "Loading Maxmind GeoIP alias for " + detail);
	}

	CURL* c;
	c = curl_easy_init();
	curl_easy_setopt( c, CURLOPT_URL, "http://mirror.sphirewall.net/resources/GeoIPCountryWhois.csv" );
	curl_easy_setopt( c, CURLOPT_WRITEFUNCTION, function );
	remoteBuffer.clear();
	int err = curl_easy_perform( c );
	if(err != 0){
		if(System::getInstance()->getFirewall() && System::getInstance()->getFirewall()->getLogContext()){
			System::getInstance()->getFirewall()->getLogContext()->log(SLogger::ERROR, "Failed to load Maxmind GeoIP country for " + detail);
		}

		return -1;
	}

	curl_easy_cleanup( c );

	vector<string> items;
	split(remoteBuffer, '\n', items);
	for(int x= 0; x < items.size(); x++){
		vector<string> csv;
		split(items[x], ',', csv);
		std::string startIp = StringUtils::trim(csv[0], "\"");
		std::string endIp = StringUtils::trim(csv[1], "\"");

		std::string targetCountryCode = StringUtils::trim(csv[4], "\"");        
		std::string targetCountry = StringUtils::trim(csv[5], "\"");    

		if(targetCountry.compare(detail) == 0 || targetCountryCode.compare(detail) == 0){
			stringstream ss; ss << startIp << "-" << endIp;
			alias->addEntry(ss.str());
		}
	}
	return 0;
}

void SFwallCore::Alias::clear(){
	list<string> values = listEntries();
	for(list<string>::iterator iter = values.begin();
			iter != values.end();
			iter++){

		removeEntry((*iter));
	}
}

bool SFwallCore::IpRangeOperator::operator() (SFwallCore::Range const& n1, SFwallCore::Range const& n2) const
{
	if(n1.end == -1){
		bool res = (n1.start < n2.start);
		return res;
	}

	if(n2.end == -1){
		bool res = n1.end < n2.start;
		return res;
	}

	if(n1.start < n2.start){
		return true;
	}

	return false;
}

bool SFwallCore::IpSubnetOperator::operator() (SFwallCore::Range const& n1, SFwallCore::Range const& n2) const
{
	if(n1.end == -1){
		return (n1.start < n2.start);
	}

	if(n2.end == -1){
		return (n1.start | ( ~ n1.end)) < n2.start;
	}

	if(n1.start < n2.start){
		return true;
	}

	return false;
}


