/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_SYSTEM_H_INCLUDED
#define SPHIREWALL_SYSTEM_H_INCLUDED

#include "Cron.h"
#include "Config.h"
#include "Core/ConfigurationManager.h"
#include "Core/Logger.h"
#include "Utils/NetDevice.h"

typedef enum {
	RUNLEVEL_RESTART = -1, RUNLEVEL_STOP = 0, RUNLEVEL_NORMAL = 1, RUNLEVEL_DEV_MODE = 2, RUNLEVEL_SEGFAULT = 3, EXIT
} runlevel_t;

class AuthenticationManager;
class Dhcp3ConfigurationManager;
class ConnectionManager;
class OpenvpnManager;
class EmailSender;
class WirelessConfiguration;
class CloudConnection;
class HostDiscoveryService;
class GlobalDynDns;
class SessionDb;
class GroupDb;
class UserDb;
class IDS;
class SysMonitor;
class BandwidthDbPrimer;

class EventDb;
class QuotaWatch;
class EventDb;
class InterfaceManager;
class DNSConfig;
class QuotaWatch;

namespace SFwallCore {
	class Firewall;
};

class System {
friend class CloudConnection;
public:
	std::string version;
	std::string versionName;

	/*!Return an instance of System*/
	static System *getInstance();
	static void sysRelease();

	void init(const string& config_file, runlevel_t runLevel);

	SLogger::Logger logger;
	SLogger::LogContext* authLogger;

	/*!Replaces the system and popen commands*/
	int exec(string command, string &ret, bool RUN_AS_ROOT);
	static int execWithNoFds(string command);
	static int exec(string command, string &ret);

	pid_t getProcessId();
	bool ALIVE;
	string lockFile;

        ConfigurationManager configurationManager;
        Config config;

	SFwallCore::Firewall* getFirewall() {
		return sFirewall;	
	}

	BandwidthDbPrimer* getBandwidthDbPrimer(){
		return bandwidthDb;
	}
		
	IDS* getIds(){
		return ids;
	}

	GroupDb* getGroupDb(){
		return groupDb;
	}

	UserDb* getUserDb(){
		return userDb;
	}

	SessionDb* getSessionDb(){
		return sessionDb;
	}

	HostDiscoveryService* getArp(){
		return arp;
	}

	InterfaceManager* getInterfaces() {
		return &interfaces;
	}

	CronManager* getCronManager(){
		return cronManager;
	}

	EmailSender* getEmailSender(){
		return emailSender;
	}

	EventDb* getEventDb(){
		return events;
	}

	WirelessConfiguration* getWirelessConfiguration(){
		return wirelessConfiguration;
	}
	
	CloudConnection* getCloudConnection() {
		return cloud;
	}

	GlobalDynDns* getGlobalDynDns(){
		return globalDynDns;
	}

	QuotaWatch* getQuotaWatch(){
		return quotaWatch;
	}

	ConfigurationManager* getConfigurationManager(){
		return &configurationManager;
	}
private:
	System();
	System(const System&);
	System & operator=(const System&);
	~System();
	static System* m_instance;

	void createLock(string lockFile);
	void removeLock(string lockFile);
	pid_t retLockPid(string lockFile);

	//Processes
	SysMonitor* sysMonitor;
	SFwallCore::Firewall* sFirewall;
	Dhcp3ConfigurationManager* dhcpConfigurationManager;
	OpenvpnManager* openvpn;
	BandwidthDbPrimer* bandwidthDb;
	IDS* ids;

	GroupDb* groupDb;
	UserDb* userDb;
	EventDb* events;
	SessionDb* sessionDb;
	InterfaceManager interfaces;
	HostDiscoveryService* arp;
	DNSConfig dnsConfig;
	ConnectionManager* connectionManager;
	EmailSender* emailSender;

	CronManager* cronManager;
	AuthenticationManager* authenticationManager;
	LoggingConfiguration* loggingConfig;

	WirelessConfiguration* wirelessConfiguration;
	CloudConnection* cloud;
	GlobalDynDns* globalDynDns;
	QuotaWatch* quotaWatch;
};

#endif
