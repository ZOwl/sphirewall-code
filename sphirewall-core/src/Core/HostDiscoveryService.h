/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALLD_ARP_H
#define SPHIREWALLD_ARP_H

#include <map>
#include <vector>
#include <list>
#include <netinet/in.h>

#include "Core/Logger.h"
#include "Core/Cron.h"
#include "Core/Lockable.h"

class EventDb;
class Lock;
namespace SFwallCore {
	class Packet;
};

class Host {
public:
	string mac;
	in_addr_t ip;
	int lastSeen;
	int firstSeen;

	string getIp();	
	string hostname();

private:
	string hostnameCache;
};

class HostDiscoveryService : public Lockable {
	public:

		class CleanUpCron : public CronJob {
			public:

				CleanUpCron(HostDiscoveryService* root) : root(root), CronJob(60 * 30, "HOST_DISCOVERY_CLEANUP_CRON", true) {
				}
				void run();

			private:
				HostDiscoveryService* root;
		};

		HostDiscoveryService(SLogger::Logger* logger, CronManager* cron, Config* config);
		HostDiscoveryService();
		string get(string ip);
		void update(SFwallCore::Packet* packet);
		void update(unsigned int ip, std::string mac);
		void list(std::list<Host*>& entries);

		int size();
		void setEventDb(EventDb* eventDb){
			this->eventDb = eventDb;
		}

                void addIgnoreLocal(string mac, in_addr_t ip);

	private:
		map<string, std::list<Host*> > entryTable;

		SLogger::Logger* logger;
		EventDb* eventDb;

		void add(string mac, in_addr_t ip);
		Host* getByMac(string mac, in_addr_t ip);
		Host* get(in_addr_t ip);

		static int HOST_DISCOVERY_SERVICE_TIMEOUT;
};

#endif
