/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <time.h>
#include <string>
#include <map>
#include <sstream>
#include <vector>
#include <queue>
#include <boost/regex.hpp>

using namespace std;

#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "BandwidthDb/Bandwidth.h"
#include "Core/System.h"
#include "Utils/Utils.h"
#include "Utils/IP4Addr.h"
#include "BandwidthDb/AnalyticsClient.h"
#include "Utils/Lock.h"

void BandwidthDbPrimer::push(ConnectionStatistics* statistic) {
	transferLock->lock();
	bufferedConnections.push_back(statistic);
	int size = bufferedConnections.size();
	transferLock->unlock();	

	if (size > maxBufferSize) {
		boost::thread(boost::bind(&BandwidthDbPrimer::run, this));
	}
}

/*Cron job*/
void BandwidthDbPrimer::run() {
	static int push = 0;
	transferLock->lock();
	std::vector<ConnectionStatistics*> temp(bufferedConnections); 
	bufferedConnections.clear();
	transferLock->unlock();


	//Double check mechanism
	if(temp.size() < MIN_BANDWIDTH_TRANSFER_THRESHOLD){
		for(int x= 0; x < temp.size(); x++){
			this->push(temp[x]);
		}			

		return;
	}

	transferToAnaLock->lock();
	try {
		stringstream logss; logss <<  "Flushing bandwidth data " << temp.size() << " connections being pushed";
		logger->log(SLogger::ERROR, "BandwidthDbPrimer::run()", logss.str());

		AnalyticsClient* client = db();
		client->addTaInputSlot(temp);
		delete client;

		logger->log(SLogger::ERROR, "BandwidthDbPrimer::runn()", "Finished running run()");

		push++;
	} catch (exception& e) {
		stringstream ss;
		ss << "Could not push data to the analytics engine: " << e.what();
		logger->log(SLogger::ERROR, "BandwidthDbPrimer::add()", ss.str());
	}
	transferToAnaLock->unlock();
}

AnalyticsClient* BandwidthDbPrimer::db() {
	return new AnalyticsClient();
}

void BandwidthDbPrimer::setMaxBufferSize(int i) {
	maxBufferSize = i;
}

void BandwidthDbPrimer::hook(SFwallCore::Connection* connection){
	if(EXCLUDE_NON_WEB == 1){
		if(connection->getDestinationPort() != 80 && 
				connection->getDestinationPort() != 443 && 
				connection->getDestinationPort() != 8080){
			return;
		}	
	}

	ConnectionStatistics* stat = new ConnectionStatistics();
	stat->upload = connection->getUpload();	
	stat->download = connection->getDownload();	
	stat->m_sourceIp = connection->getSrcIp();
	stat->m_destIp = connection->getDstIp();
	stat->m_sport = connection->getSourcePort();
	stat->m_dport = connection->getDestinationPort();
	stat->httpHost = connection->getHttpHost();
	stat->protocol = connection->getProtocol();
	stat->startTime = connection->getTime();
	stat->hwAddress = connection->getHwAddress();	

	NetDevice* sourceDev;
	NetDevice* destDev;
	if((sourceDev = connection->getSourceNetDevice()) != NULL){
		stat->sourceDev = sourceDev->getInterface();
	}

	if((destDev = connection->getDestNetDevice()) != NULL){
		stat->destDev = destDev->getInterface();	
	}

	if(connection->getSession() != NULL){
		stat->user = connection->getSession()->getUserName();
	}

	push(stat);
}	
