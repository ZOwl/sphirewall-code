/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef sphirewall_bandwidth_TYPES_H
#define sphirewall_bandwidth_TYPES_H

#include <stdint.h>

class taInputSlot {
public:
	uint32_t sourceIp;
	std::string sourceHw;
	uint32_t destIp;
	std::string user;
	int32_t sourcePort;
	int32_t destPort;
	int32_t time;
	int32_t upload;
	int32_t download;
	std::string inputDev;
	std::string outputDev;
	int32_t protocol;
	std::string httpHost;
};

class taSummaryUnit {
public:

	taSummaryUnit() : tcpUp(0), tcpDown(0), udpUp(0), udpDown(0), startTime(0), finishTime(0), displayTime("") {
	}

	virtual ~taSummaryUnit() throw () {
	}

	long long tcpUp;
	long long tcpDown;
	long long udpUp;
	long long udpDown;
	int64_t startTime;
	int64_t finishTime;
	std::string device;
	std::string displayTime;
	int64_t port;
	std::string httpHost;
	std::string username;
	std::string hw;
	int64_t sourceIp;
};

class taWebsiteSummaryUnit : public taSummaryUnit {

public:
    taWebsiteSummaryUnit() : ip(0) {};

    virtual ~taWebsiteSummaryUnit() throw () {};

    int64_t ip;
};
#endif
