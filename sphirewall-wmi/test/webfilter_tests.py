#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester

class WebfilterTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login("fw", "admin", "admin")

    def gotoAliases(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Firewall')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Pools')]").click()

    def deleteAlias(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Pools')]").click()
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'porn')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()

    def set_choserval(self, id, value):
        select_js = "$(\"#"+id+"\").find(\"option:contains('"+value+"')\").each("\
        "function(){"\
            "if( $(this).text() == '"+value+"' ) {"\
                "$(this).attr(\"selected\",\"selected\");"\
            "}"\
        "});"

        self.tester.driver.execute_script(select_js)
        self.tester.driver.execute_script("$('#" + id + "').trigger('chosen:updated');")

    def test_createWebsiteList(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_name("name").send_keys("porn")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='Website List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Web Filter')]").click()
        self.tester.clickOptions("Add Rule")

        self.tester.driver.find_element_by_name("sourceIp").send_keys("10.1.1.0")
        self.tester.driver.find_element_by_name("sourceMask").send_keys("255.255.255.240")
        self.set_choserval("group", "defaultadmin")
        self.set_choserval("list", "porn")

        self.tester.driver.find_element_by_name("startTime").send_keys("1200")
        self.tester.driver.find_element_by_name("endTime").send_keys("1359")
        self.tester.driver.find_element_by_name("mon").click()
        self.tester.driver.find_element_by_name("tues").click()
        self.tester.driver.find_element_by_name("wed").click()
        self.tester.driver.find_element_by_name("thurs").click()
        self.tester.driver.find_element_by_name("fri").click()
        self.tester.driver.find_element_by_name("sat").click()
        self.tester.driver.find_element_by_name("sun").click()

        self.tester.driver.find_element_by_name("fireEvent").click()
        self.tester.driver.find_element_by_name("redirect").click()
        self.tester.driver.find_element_by_name("redirectUrl").send_keys("http://google.com")

        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        #Check Action Text
        self.assertTrue(
            self.tester.check_exists_by_xpath("//table//tr/td[contains(normalize-space(), 'Between 1200 and 1359 on, Mon, Tues, Wed, Thurs, Fri, Sat, Sun, Blacklist and fire event and redirect to http://google.com')]"))

        #Check the group field
        self.assertTrue(
            self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), 'defaultadmin,')]"))

        #Check the list field
        self.assertTrue(
            self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'porn')]"))

        #Check the source field
        self.assertTrue(
            self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.1.1.0/255.255.255.240')]"))

        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(text(), '10.1.1.0/255.255.255.240')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(
            self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.1.1.0/255.255.255.240')]"))

        self.deleteAlias()

    def test_enableDisableRule(self):
        self.gotoAliases()
        self.tester.driver.find_element_by_name("name").send_keys("porn")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='Website List']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Web Filter')]").click()
        self.tester.clickOptions("Add Rule")

        self.tester.driver.find_element_by_name("sourceIp").send_keys("10.1.1.0")
        self.tester.driver.find_element_by_name("sourceMask").send_keys("255.255.255.240")
        self.set_choserval("group", "defaultadmin")
        self.set_choserval("list", "porn")

        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()
        self.assertTrue(
            self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.1.1.0/255.255.255.240')]"))

        enabled = self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(text(), '10.1.1.0/255.255.255.240')]//ancestor::tr/td/form/input")
        self.assertFalse(enabled.is_selected())

        enabled.click()
        enabled = self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(text(), '10.1.1.0/255.255.255.240')]//ancestor::tr/td/form/input")
        self.assertTrue(enabled.is_selected())

        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(text(), '10.1.1.0/255.255.255.240')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(
            self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), '10.1.1.0/255.255.255.240')]"))

        self.deleteAlias()


    def tearDown(self):
        self.tester.disconnect()
