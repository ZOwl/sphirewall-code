/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "Core/System.h"
#include "Core/Openvpn.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Utils/FileUtils.h"
#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

using namespace std;

void Openvpn::start(){
	publish();
	stringstream ss;
	ss << "/bin/bash -c 'start-stop-daemon --start --quiet --oknodo --pidfile "<< getPidFile() <<" --exec /usr/sbin/openvpn";
	ss << " -- --writepid "<<getPidFile()<<" --daemon ovpn-openvpn --cd "<<getPath()<<" --config "<<getPath()<<"/openvpn.conf'";

	System::execWithNoFds(ss.str());
	sleep(3);
	active = true;
}

void Openvpn::stop(){
	string pid = FileUtils::read(getPidFile().c_str());
	if(pid.size() > 0){
		int ipid = atoi(pid.c_str());
		if(ipid > 1){		
			kill(ipid, SIGTERM);
			remove(getPidFile().c_str());
			remove("/var/run/openvpn.openvpn.status");
		}
		sleep(3);
	}
	active = false;
}

void Openvpn::initalise(){
	if(active){
		start();
	}
}

void Openvpn::destroy(){
	if(active){
		stop();
		active = true;
	}
}

void Openvpn::publish(){
	stringstream ss;
	if(isStatic()){
		ss << "ifconfig " << serverip << " " << clientip << endl;
		ss << "secret "<<getPath()<<"/static.key\n";
	}else{
		ss << "proto tcp\n";
		ss << "server " << serverip << " " << servermask << "\n";
		ss << "ifconfig-pool-persist ipp.txt\n";
		ss << "ca "<<getPath()<<"/easy-rsa/keys/ca.crt\n";
		ss << "cert "<<getPath()<<"/easy-rsa/keys/server.crt\n";
		ss << "key "<< getPath() <<"/easy-rsa/keys/server.key\n";
		ss << "dh "<< getPath()<< "/easy-rsa/keys/dh1024.pem\n";
		ss << "push \"route " << serverip;
		ss << " " << servermask << "\"\n";
	}

	//Default options:
	ss << "dev tun\n";
	ss << "keepalive 10 60\n";
	ss << "ping-timer-rem\n";
	ss << "persist-tun\n";
	ss << "persist-key\n";
	ss << "group daemon\n";
	ss << "daemon\n";
	ss << "cd "<< getPath() << "\n";
	ss << "log-append " << getLogFile() << "\n";

	//Customisable Options
	ss << "port " << remoteport << "\n";
	if(compression){
		ss << "comp-lzo\n";
	}

	string config = ss.str(); 
	string filename = getPath() + "/openvpn.conf";
	ofstream configFile;
	configFile.open(filename.c_str());
	configFile << config;
}

bool Openvpn::status(){
	return FileUtils::checkExists(getPidFile());
}

std::string Openvpn::getPidFile(){
	stringstream ss;
	ss << "/var/run/openvpn." << name << ".pid";
	return ss.str();
}

OpenvpnClientConf* Openvpn::createClient(std::string name){
	if(!isStatic()){
		OpenvpnClientConf* c = new OpenvpnClientConf();
		c->name = name;
		stringstream ss; 
		ss << "/bin/bash -c 'cd "<< getPath() << "/easy-rsa && source ./vars && export EASY_RSA=\'${EASY_RSA:-.}\' && $EASY_RSA/pkitool " << name << "'";
		system(ss.str().c_str());		
		clients.push_back(c);
		return c;
	}
	return NULL;
}

std::string Openvpn::generateUserFile(std::string name){
	stringstream ss;

	//Global variables:
	if(compression){
		ss << "comp-lzo\n";
	}

	ss << "dev tun\n";
	ss << "resolv-retry infinite\n";
	ss << "nobind\n";
	ss << "persist-key\n";
	ss << "persist-tun\n";
	ss << "verb 3\n\n";

	if(isStatic()){
		ss << "remote " << remoteserverip << " " << remoteport << "\n";	
		ss << "ifconfig " << clientip << " " << serverip << "\n";
		ss << "<secret>\n";
		string keyfile = getPath() + "/static.key";

		ss << FileUtils::read(keyfile.c_str());
		ss << "</secret>\n";

	}else{
		ss << "remote " << remoteserverip << " " << remoteport << "\n";
		ss << "client\n";
		ss << "proto tcp\n";
		ss << "port " << remoteport << "\n";

		ss << "<ca>\n";
		stringstream ca; ca << getPath() << "/easy-rsa/keys/ca.crt";
		ss << FileUtils::read(ca.str().c_str());	
		ss << "</ca>\n";
		ss << "<cert>\n";
		stringstream crtfile; crtfile << getPath() <<"/easy-rsa/keys/" << name << ".crt";
		string caEntireFile = FileUtils::read(crtfile.str().c_str());	
		int startPos = caEntireFile.find("-----BEGIN CERTIFICATE-----");
		int stopPos = caEntireFile.find("-----END CERTIFICATE-----");

		for(int x = startPos; x < stopPos; x++){
			ss << caEntireFile[x];
		}		
		ss << "-----END CERTIFICATE-----\n";	
		ss << "</cert>\n";

		ss << "<key>\n";
		stringstream keyname; keyname << getPath() << "/easy-rsa/keys/" << name << ".key";
		ss << FileUtils::read(keyname.str().c_str());	
		ss << "</key>\n";
	}
	return ss.str();
}

void Openvpn::deleteClient(OpenvpnClientConf* c){
	if(!isStatic()){
		clients.remove(c);	
		stringstream ss; ss << "/bin/bash -c 'cd " << getPath() << "/easy-rsa/keys/ && rm " << c->name << ".*'";
		system(ss.str().c_str());
	}
}

std::list<OpenvpnClientConf*>& Openvpn::getClients(){
	return clients;
}

std::string Openvpn::log(){
	string ret;
	string c = "tail -n 500 " + getLogFile();
	System::exec(c.c_str(), ret);
	return ret;
}

void Openvpn::installbase(){
	if(isStatic()){
		stringstream ss;
		ss << "/bin/bash -c 'mkdir " << getPath() << " && /usr/sbin/openvpn --genkey --secret "<< getPath() <<"/static.key'";

		string ret;
		System::exec(ss.str(), ret);

		OpenvpnClientConf* nc = new OpenvpnClientConf();
		nc->name = "Default Static Client"; 
		clients.push_back(nc);
	}else{
		stringstream ss;
		ss << "/bin/bash -c '/usr/bin/setup-openvpn.sh ";
		ss << certcountry << " ";
		ss << certprovince << " ";
		ss << certcity << " ";
		ss << certorg << " ";
		ss << certemail << " ";
		ss << getPath();
		ss << "'";
		string ret;
		System::exec(ss.str(), ret);

	}
}

std::string Openvpn::getPath(){
	return "/etc/sphirewall-openvpn-" + name;
}

std::string Openvpn::getLogFile(){
	stringstream ss;
	ss << "/var/log/sphirewall-openvpn-" << name << ".log";
	return ss.str();
}	

void Openvpn::uninstallbase(){
	stop();
	clients.clear();

	stringstream ss;
	ss << "/bin/bash -c 'rm -Rf "<<getPath()<<"'";
	string ret;
	System::exec(ss.str(), ret);
}

void OpenvpnManager::load(){
	if(config->has("openvpn")){
		ObjectContainer* root = config->getElement("openvpn");
		ObjectContainer* servers = root->get("servers")->container();
		for(int x=0; x < servers->size(); x++){
			ObjectContainer* o = servers->get(x)->container();			

			Openvpn* openvpn = new Openvpn();	
			openvpn->serverip = o->get("serverip")->string();
			openvpn->clientip = o->get("clientip")->string();
			openvpn->servermask = o->get("servermask")->string();
			openvpn->remoteserverip = o->get("remoteserverip")->string();
			openvpn->customopts = o->get("customopts")->string();
			openvpn->remoteport = o->get("remoteport")->number();
			openvpn->compression = o->get("compression")->boolean();

			if(o->has("active")){
				openvpn->active = o->get("active")->boolean();
			}			

			openvpn->staticAuthentication = o->get("staticAuthentication")->boolean();
			openvpn->name = o->get("name")->string();

			ObjectContainer* clientsArr = o->get("clients")->container();
			for(int x =0; x < clientsArr->size(); x++){
				ObjectContainer* o = clientsArr->get(x)->container();
				OpenvpnClientConf* nc = new OpenvpnClientConf();
				nc->name = o->get("name")->string();     
				openvpn->getClients().push_back(nc);
			}            

			instances.push_back(openvpn); 
		}	
	}
}

void OpenvpnManager::save(){
	ObjectContainer* root = new ObjectContainer(CREL); 
	ObjectContainer* sarr = new ObjectContainer(CARRAY);
	for(list<Openvpn*>::iterator iter = instances.begin(); iter != instances.end(); iter++){
		Openvpn* openvpn = (*iter);
		ObjectContainer* single = new ObjectContainer(CREL);
		single->put("name", new ObjectWrapper((string) openvpn->name));
		single->put("staticAuthentication", new ObjectWrapper((bool) openvpn->isStatic()));

		single->put("serverip", new ObjectWrapper((string) openvpn->serverip));
		single->put("clientip", new ObjectWrapper((string) openvpn->clientip));
		single->put("servermask", new ObjectWrapper((string) openvpn->servermask));
		single->put("remoteserverip", new ObjectWrapper((string) openvpn->remoteserverip));
		single->put("customopts", new ObjectWrapper((string) openvpn->customopts));
		single->put("remoteport", new ObjectWrapper((double) openvpn->remoteport));
		single->put("compression", new ObjectWrapper((bool) openvpn->compression));
		single->put("active", new ObjectWrapper((bool) openvpn->active));		

		ObjectContainer* clientArray = new ObjectContainer(CARRAY);
		for(list<OpenvpnClientConf*>::iterator citer = openvpn->getClients().begin();
				citer != openvpn->getClients().end();
				citer++){

			ObjectContainer* o = new ObjectContainer(CREL);   
			o->put("name", new ObjectWrapper((string) (*citer)->name));          
			clientArray->put(new ObjectWrapper(o));
		}  

		single->put("clients", new ObjectWrapper(clientArray));
		sarr->put(new ObjectWrapper(single));
	}

	root->put("servers",  new ObjectWrapper(sarr));	
	config->setElement("openvpn", root);
	config->save();
}

Openvpn* OpenvpnManager::getInstance(std::string name){
	for(list<Openvpn*>::iterator iter = instances.begin(); iter != instances.end(); iter++){
		if((*iter)->name.compare(name) == 0){
			return (*iter);
		}
	}
	return NULL;
}

void OpenvpnManager::addInstance(Openvpn* target){
	instances.push_back(target);
	target->installbase();
}

void OpenvpnManager::removeInstance(Openvpn* target){
	target->uninstallbase();

	for(list<Openvpn*>::iterator iter = instances.begin(); iter != instances.end(); iter++){
		if((*iter) == target){
			instances.erase(iter);
			break;
		}
	}
}

bool OpenvpnManager::daemonExists(){
	return FileUtils::checkExists("/usr/sbin/openvpn");
}

void OpenvpnManager::initalise(){
	load();
	for(list<Openvpn*>::iterator iter = instances.begin(); iter != instances.end(); iter++){
		(*iter)->initalise();	
	}

	save();
}

void OpenvpnManager::destroy(){
	for(list<Openvpn*>::iterator iter = instances.begin(); iter != instances.end(); iter++){
		(*iter)->destroy();
	}
	save();
}
