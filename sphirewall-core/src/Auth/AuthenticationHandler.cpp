/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
using namespace std;

#include "Auth/AuthenticationHandler.h"
#include "Auth/UserDb.h"
#include "Auth/User.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include <iostream>
#include <fcntl.h>
#include <sys/resource.h>
#include <security/pam_appl.h>
#include <pwd.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include "Utils/HttpRequestWrapper.h"
#include <boost/regex.hpp>
#include <set>

#define STATUS_OK 	  0
#define STATUS_UNKNOWN	  1 
#define STATUS_INVALID	  2
#define STATUS_BLOCKED    3 
#define STATUS_EXPIRED    4 
#define STATUS_PW_EXPIRED 5 
#define STATUS_NOLOGIN    6 
#define STATUS_MANYFAILS  7 

#define STATUS_INT_USER  50
#define STATUS_INT_ARGS  51
#define STATUS_INT_ERR   52

int hisuid;
int haveuid = 0;

#define BFSZ 1024
#define msgi(i) (*(msg[i]))

struct ad_user {
	char *login;
	char *passwd;
};

#define SCOPE LDAP_SCOPE_SUBTREE
#define LDAP_DEPRECATED 1

#include <ldap.h>

using namespace std;

void AuthenticationManager::mergeGroups(AuthenticationMethod* method, User* user){
	stringstream ss; ss << "merging group information for user '" << user->getUserName() << "'";
	logger->log(SLogger::INFO, ss.str());

	vector<Group*> newGroups;
	list<string> groups =  method->getGroupNames(user);
	for(list<string>::iterator iter = groups.begin();
			iter != groups.end();
			iter++){
		Group* group = localGroupDb->getGroup((*iter));
		if(!group){
			group = localGroupDb->createGroup((*iter));
		}
		newGroups.push_back(group);
	}
	
	localGroupDb->save();
	user->setGroups(newGroups);
}

User* AuthenticationManager::resolveUser(std::string username){
	lock->lock(); //This is not such a great idea, but needed at the moment:

	User* userCopyFromDatabase = localUserDb->getUser(username);
	if (userCopyFromDatabase) {
		//Check the user type:
		AuthenticationMethod* method = getMethodForUser(userCopyFromDatabase->getAuthenticationType());
		if (method) {
			if(method->getType() == LDAP_DB){
				mergeGroups(method, userCopyFromDatabase);
			}

			lock->unlock();
			return userCopyFromDatabase;
		} else {
			//This is an error, we should always have a method:
			lock->unlock();
			return userCopyFromDatabase;
		}

	} else {
		//Need to get some information on this user, we dont know anything about him yet
		for (std::list<AuthenticationMethod*>::iterator iter = this->methods.begin();
				iter != this->methods.end();
				iter++) {

			AuthenticationMethod* method = (*iter);
			if (method->isEnabled()) {
				User* newUserFound = method->findUser(username);
				if (newUserFound) {
					localUserDb->persistUserFromExternalSource(newUserFound);

					//Add groups:
					if (System::getInstance()->configurationManager.hasByPath("general:remote_authentication_group")) {
						int groupId = System::getInstance()->configurationManager.get("general:remote_authentication_group")->number();
						if (groupId != -1) {
							Group* group = localGroupDb->getGroup(groupId);
							if (group) {
								newUserFound->addGroup(group);
							}
						}
					}

					if(method->getType() == LDAP_DB){
						mergeGroups(method, newUserFound);
					}

					lock->unlock();
					return newUserFound; 
				}
			}
		}
	}

	lock->unlock();
	return NULL;
}

bool AuthenticationManager::authenticate(std::string username, std::string password) {
	User* userCopyFromDatabase = resolveUser(username); 
	if (userCopyFromDatabase) {
		//Check the user type:
		AuthenticationMethod* method = getMethodForUser(userCopyFromDatabase->getAuthenticationType());
		if (method) {
			bool result = method->authenticate(userCopyFromDatabase, username, password);
			return result;
		} else {
			//This is an error, we should always have a method:
			return false;
		}

	} 

	return false;
}

AuthenticationMethod* AuthenticationManager::getMethodForUser(AuthenticationType methodType) {
	list<AuthenticationMethod*>::iterator iter;
	for (iter = this->methods.begin();
			iter != this->methods.end();
			iter++) {

		AuthenticationMethod* method = (*iter);
		if (method->canConnect() && method->isEnabled()) {
			if (method->getType() == methodType) {
				return method;
			}
		}
	}

	return NULL;
}

bool LocalDbAuthenticationMethod::authenticate(User* userObject, std::string username, std::string password) {
	return userDb->authenticateUser(username, password);
}

User* LocalDbAuthenticationMethod::findUser(std::string username) {
	return userDb->getUser(username);
}

void AuthenticationManager::SyncCron::run(){
	for (std::list<AuthenticationMethod*>::iterator iter = authenticationManager->methods.begin();
			iter != authenticationManager->methods.end();
			iter++) {

		AuthenticationMethod* method = (*iter);
		if(method->isEnabled()){
			method->sync(authenticationManager->localUserDb, authenticationManager->localGroupDb);
		}
	}
}

std::string LdapAuthenticationMethod::processGroupAttribute(std::string input){
	string groupMembershipRaw = input;
	//cn=it_test_grp,ou=Departments,ou=Groups,ou=Global-Res,o=BHS   
	static boost::regex p("cn=(.*?),.+", boost::regex::icase);
	boost::smatch what;
	if (boost::regex_match(groupMembershipRaw, what, p, boost::match_extra)) {
		string groupName = what[1];
		return groupName;
	}
	return "";
}

static const char* EDIRECTORY_GROUPTAG = "groupMembership";
static const char* AD_GROUPTAG = "memberOf";

void LdapAuthenticationMethod::sync(UserDb* userDb, GroupDb* groupDb){
	logger->log(SLogger::INFO, "starting ldap group sync");

	set<string> ret;
	LDAP* connection = connect(getHostname(), getPort(), getLdapUsername(), getLdapPassword());
	if (connection) {
		logger->log(SLogger::INFO, "connected to ldap service");
		LDAPMessage *result;
		char* attributes[] = {"*", "+", NULL};
		int rc = ldap_search_ext_s(connection, getBaseDn().c_str(), SCOPE, "CN=*", attributes, 0, NULL, NULL, NULL, 0, &result);
		if (rc == LDAP_SUCCESS) {
			LDAPMessage* entry;
			for ( entry = ldap_first_entry( connection, result ); entry != NULL;
					entry = ldap_next_entry( connection, entry ) ){

				BerElement* ber;
				for(char* attribute = ldap_first_attribute(connection, entry, &ber);
						attribute != NULL;  attribute = ldap_next_attribute(connection, entry, ber)){

					if(string(attribute).compare(EDIRECTORY_GROUPTAG) == 0 || string(attribute).compare(AD_GROUPTAG) == 0){
						char **values = ldap_get_values(connection, entry, attribute);
						if( values != NULL)
						{
							for(int i = 0; values[i] != NULL; i++)
							{
								ret.insert(processGroupAttribute(values[i]));
							}
							ldap_value_free(values);
						}
					}
					ldap_memfree(attribute);
				}

				if(ber != NULL){
					ber_free(ber, 0);
				}
			}
		}else{
			logger->log(SLogger::ERROR, "could not query ldap service, reason was " + string(ldap_err2string(rc)));
		}

		ldap_msgfree(result);
		ldap_unbind(connection);
	}

	groupDb->lock();
	for(set<string>::iterator iter = ret.begin();
			iter != ret.end();
			iter++){
		std::string name = (*iter);
		if(!groupDb->getGroup(name)){
			groupDb->createGroup(name);
			stringstream ss; ss << "added group '" << name << "' to sphirewall groupdb";
			logger->log(SLogger::INFO, ss.str());
		}
	}
	groupDb->unlock();
	logger->log(SLogger::INFO, "finished sync ldap service");
}

bool LdapAuthenticationMethod::canConnect() {
	LDAP* connection = connect(getHostname(), getPort(), getLdapUsername(), getLdapPassword());
	if(connection){
		ldap_unbind(connection);
		return true;
	}
	return false;
}

bool LdapAuthenticationMethod::authenticate(User* userObject, std::string username, std::string password) {
	stringstream ss; ss << "attempting to authenticate user '"<< username<< "' with ldap service"; 
	logger->log(SLogger::INFO, ss.str());
	if(password.size() == 0){
		return false;
	}

	LDAP* connection = connect(getHostname(), getPort(), userObject->dn.size() > 0 ? userObject->dn : userObject->getUserName(), password);
	if(connection){
		ldap_unbind(connection);
		return true;
	}

	return false;
}

list<string> LdapAuthenticationMethod::getGroupNames(User* user){
	stringstream s1; s1 << "attempting to find group membership for user '" << user->getUserName() << "'";
	logger->log(SLogger::INFO, s1.str());

	list<string> ret;
	LDAP* connection = connect(getHostname(), getPort(), getLdapUsername(), getLdapPassword());
	if (connection) {
		LDAPMessage *result;
		string fullCN = "CN=" + user->getUserName();
		char* attributes[] = {"*", "+", NULL};
		int rc = ldap_search_ext_s(connection, getBaseDn().c_str(), SCOPE, fullCN.c_str(), attributes, 0, NULL, NULL, NULL, 0, &result);
		if (rc == LDAP_SUCCESS) {
			LDAPMessage* entry = ldap_first_entry(connection, result);
			if(entry != NULL)
			{
				BerElement* ber;
				for(char* attribute = ldap_first_attribute(connection, entry, &ber); 
						attribute != NULL;  attribute = ldap_next_attribute(connection, entry, ber)){

					if(string(attribute).compare(EDIRECTORY_GROUPTAG) == 0 || string(attribute).compare(AD_GROUPTAG) == 0){
						char **values = ldap_get_values(connection, entry, attribute);
						if( values != NULL)
						{
							for(int i = 0; values[i] != NULL; i++)
							{
								string groupMembershipRaw = values[i];
								stringstream s2; s2 << "found group attribute " << groupMembershipRaw << " for user '" << user->getUserName() << "'";
								logger->log(SLogger::INFO, s2.str());

								ret.push_back(processGroupAttribute(groupMembershipRaw));
							}
							ldap_value_free(values);
						}
					}else if(string(attribute).compare("entryDN") == 0){
						char **values = ldap_get_values(connection, entry, attribute);
						if( values != NULL)
						{
							for(int i = 0; values[i] != NULL; i++)
							{
								if(string(attribute).compare("entryDN") == 0){
									stringstream s2; s2 << "found entryDn attribute " << values[i] << " for user '" << user->getUserName() << "'";
									logger->log(SLogger::INFO, s2.str());

									user->dn = values[i];
								}
							}
							ldap_value_free(values);
						}
					}
					ldap_memfree(attribute);
				}

				if(ber != NULL){
					ber_free(ber, 0);
				}
			}
		}	
		ldap_msgfree(result);
		ldap_unbind(connection);
	}

	return ret;
}

User* LdapAuthenticationMethod::findUser(std::string username) {
	LDAP* connection = connect(getHostname(), getPort(), getLdapUsername(), getLdapPassword());
	if (connection) {
		LDAPMessage *result;
		string fullCN = "CN=" + username;

		int rc = ldap_search_ext_s(connection, getBaseDn().c_str(), SCOPE, fullCN.c_str(), NULL, 0, NULL, NULL, NULL, 0, &result);
		if (rc != LDAP_SUCCESS) {
			stringstream s2; s2 << "could not find user " << username << " reason is " << ldap_err2string(rc);
			logger->log(SLogger::ERROR, s2.str());

			ldap_msgfree(result);
			ldap_unbind(connection);
			return NULL;
		}

		LDAPMessage* entry = ldap_first_entry(connection, result);
		if(entry != NULL)
		{
			stringstream s2; s2 << "creating new user object in sphirewall db for " << username;
			logger->log(SLogger::INFO, s2.str());

			User* newUser = new User();
			newUser->setAuthenticationType(LDAP_DB);
			newUser->setUserName(username);

			ldap_msgfree(result);
			ldap_unbind(connection);
			return newUser;
		}

		ldap_msgfree(result);
		ldap_unbind(connection);
	}

	return NULL;
}

std::string LdapAuthenticationMethod::getHostname() const {
	if (configurationManager->hasByPath("ldapSettings:hostname")) {
		return configurationManager->get("ldapSettings:hostname")->string();
	}

	return "";
}

int LdapAuthenticationMethod::getPort() const {
	if (configurationManager->hasByPath("ldapSettings:port")) {
		return configurationManager->get("ldapSettings:port")->number();
	}

	return -1;
}

std::string LdapAuthenticationMethod::getLdapUsername() const {
	if (configurationManager->hasByPath("ldapSettings:ldapusername")) {
		return configurationManager->get("ldapSettings:ldapusername")->string();
	}

	return "";
}

std::string LdapAuthenticationMethod::getLdapPassword() const {
	if (configurationManager->hasByPath("ldapSettings:ldappassword")) {
		return configurationManager->get("ldapSettings:ldappassword")->string();
	}

	return "";
}

std::string LdapAuthenticationMethod::getBaseDn() const {
	if (configurationManager->hasByPath("ldapSettings:basedn")) {
		return configurationManager->get("ldapSettings:basedn")->string();
	}

	return "";
}

bool LdapAuthenticationMethod::isEnabled() {
	if (configurationManager->hasByPath("ldapSettings:enabled")) {
		return configurationManager->get("ldapSettings:enabled")->boolean();
	}

	return false;
}

LDAP* LdapAuthenticationMethod::connect(std::string hostname, int port, std::string username, std::string password) {
	LDAP* ld;
	if ((ld = ldap_init(hostname.c_str(), port)) == NULL) {
		logger->log(SLogger::ERROR, "ldap init failed");
		return NULL;
	}

	int version = LDAP_VERSION3;
	ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &version);

	int rc = ldap_simple_bind_s(ld, username.c_str(), password.c_str());
	if (rc != LDAP_SUCCESS) {
		stringstream ss; ss << "could not connect to ldap service, reason is '" << string(ldap_err2string(rc)) << "'";
		logger->log(SLogger::ERROR, ss.str());
		ldap_unbind(ld);		
		return NULL;
	}

	return ld;
}

bool PamAuthenticationMethod::isEnabled() {
	if (configurationManager->hasByPath("pamSettings:enabled")) {
		return configurationManager->get("pamSettings:enabled")->boolean();
	}

	return false;
}

bool PamAuthenticationMethod::canConnect() {
	return true;
}

int PAM_conv(int num_msg, const struct pam_message **msg, struct pam_response **resp, void *appdata_ptr) {
	struct ad_user *user = (struct ad_user *) appdata_ptr;
	struct pam_response *response;
	int i;

	if (msg == NULL || resp == NULL || user == NULL)
		return PAM_CONV_ERR;

	response = (struct pam_response *)
		malloc(num_msg * sizeof (struct pam_response));

	for (i = 0; i < num_msg; i++) {
		response[i].resp_retcode = 0;
		response[i].resp = NULL;

		switch (msgi(i).msg_style) {
			case PAM_PROMPT_ECHO_ON:
				/* Store the login as the response */
				/* This likely never gets called, since login was on pam_start() */
				response[i].resp = appdata_ptr ? (char *) strdup(user->login) : NULL;
				break;

			case PAM_PROMPT_ECHO_OFF:
				/* Store the password as the response */
				response[i].resp = appdata_ptr ? (char *) strdup(user->passwd) : NULL;
				break;

			case PAM_TEXT_INFO:
			case PAM_ERROR_MSG:
				/* Shouldn't happen since we have PAM_SILENT set. If it happens
				 * anyway, ignore it. */
				break;

			default:
				/* Something strange... */
				if (response != NULL) free(response);
				return PAM_CONV_ERR;
		}
	}
	/* On success, return the response structure */
	*resp = response;
	return PAM_SUCCESS;
}

bool PamAuthenticationMethod::authenticate(User* userObject,std::string username, std::string password) {

	char* usernameArray = (char*) malloc(username.size() * sizeof (char));
	char* passwordArray = (char*) malloc(password.size() * sizeof (char));
	memcpy((void*) usernameArray, (void*) username.c_str(), username.size());
	memcpy((void*) passwordArray, (void*) password.c_str(), password.size());

	char *strchr();
	struct rlimit rlim;

	rlim.rlim_cur = rlim.rlim_max = 0;
	(void) setrlimit(RLIMIT_CORE, &rlim);

	struct ad_user user_info = {usernameArray, passwordArray};
	struct pam_conv conv = {PAM_conv, (void *) &user_info};
	pam_handle_t *pamh = NULL;
	int retval;

	user_info.login = usernameArray;
	user_info.passwd = passwordArray;

	string pam_module;
	if (configurationManager->hasByPath("pamSettings:module")) {
		pam_module = configurationManager->get("pamSettings:module")->string();
	} else {
		pam_module = "common-password";
	}

	retval = pam_start((char*) pam_module.c_str(), usernameArray, &conv, &pamh);

	if (retval == PAM_SUCCESS)
		retval = pam_authenticate(pamh, PAM_SILENT);

	if (retval == PAM_SUCCESS)
		retval = pam_acct_mgmt(pamh, 0);

	if (pam_end(pamh, retval) != PAM_SUCCESS) {
		pamh = NULL;
		free(usernameArray);
		free(passwordArray);
		return false;
	}

	if (retval == 0) {
		free(usernameArray);
		free(passwordArray);
		return true;
	}
}

User* PamAuthenticationMethod::findUser(std::string username) {
	User* tempUser = new User();
	tempUser->setUserName(username);
	tempUser->setAuthenticationType(PAM_DB);
	return tempUser;
}

//Basic Auth Handler:
bool BasicHttpAuthenticationHandler::isEnabled(){
	if (configurationManager->hasByPath("basicHttpAuthHandler:enabled")) {
		return configurationManager->get("basicHttpAuthHandler:enabled")->boolean();
	}

	return false;
}

bool BasicHttpAuthenticationHandler::authenticate(User* userObj, std::string username, std::string password){
	string baseurl = configurationManager->get("basicHttpAuthHandler:baseurl")->string();
	stringstream ss; ss << baseurl << "?username=" << username << "&password=" << password;

	HttpRequestWrapper* req = new HttpRequestWrapper(ss.str(), GET);	
	try {
		string response = req->execute();
		if(response.compare("OK") == 0){
			return true;
		}else{
			return false;
		}
	}catch(HttpRequestWrapperException* e){
		return false;
	}
	delete req;
	return false;		
}

User* BasicHttpAuthenticationHandler::findUser(std::string username){
	User* tempUser = new User();
	tempUser->setUserName(username);
	tempUser->setAuthenticationType(BASIC_HTTP);
	return tempUser;
}

AuthenticationManager::AuthenticationManager(CronManager* cronManager) {
	lock = new Lock();
}

void AuthenticationManager::addMethod(AuthenticationMethod* method) {
	methods.push_back(method);
}

AuthenticationMethod* AuthenticationManager::getMethod(AuthenticationType type) {
	std::list<AuthenticationMethod*>::iterator iter;
	for (iter = methods.begin(); iter != methods.end(); iter++) {
		AuthenticationMethod* m = (*iter);
		if (m->getType() == type) {
			return m;
		}
	}

	return NULL;
}


