#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

from string import replace
from flask.globals import request
from flask.templating import render_template
from werkzeug.utils import redirect
from sphirewallwmi import app
import service

@app.route("/network/devices")
def network_devices():
    return render_template("network_devices.html", routes=service.get_sphirewallapi().network().routes(),
        devices=service.get_sphirewallapi().network().devices(configured=True),
        isSynced=service.get_sphirewallapi().network().isSynced())


@app.route("/network/devices/<device>/toggle")
def network_devices_toggle(device):
    service.get_sphirewallapi().network().devices_toggle(device)
    return redirect("/network/devices")


@app.route("/network/devices/<device>/add", methods=["GET", "POST"])
def network_devices_add(device):
    #Is this an existing device we are persisting?
    network__device = service.get_sphirewallapi().network().devices(device=device)
    if network__device:
        service.get_sphirewallapi().network().devices_set(device, network__device["ip"], network__device["mask"],
            False, "", False, [])
    else:
        service.get_sphirewallapi().network().devices_set(device, "", "", False, "", False, [])

    return redirect("/network/devices")


@app.route("/network/devices/<device>/remove", methods=["GET", "POST"])
def network_devices_remove(device):
    service.get_sphirewallapi().network().devices_remove(device)
    return redirect("/network/devices")


@app.route("/network/devices/<device>", methods=["GET", "POST"])
def network_devices_edit(device):
    if request.method == "POST":
        service.get_sphirewallapi().network().devices_set(text_value("device"), text_value("ip"), text_value("mask"),
            checkbox_value("dhcp"), text_value("name"), checkbox_value("bridge"), request.form.getlist('bridgeDevices'),
            text_value("aliases"), text_value("gateway"))
        return redirect("/network/devices")

    dhcpHw = arg("hw")
    if len(arg("hostname")) == 0:
        dhcpHostname = replace(dhcpHw, ":", "-")

    return render_template("network_devices_edit.html", dhcp=service.get_sphirewallapi().network().device_dhcp(device),
        device=service.get_sphirewallapi().network().devices(device, configured=True),
        staticleases=service.get_sphirewallapi().network().devices_dhcp_static(device),
        clients=service.get_sphirewallapi().general().hosts_list(), dhcpHostname=dhcpHostname, dhcpHost=arg("host"),
        dhcpHw=dhcpHw, logs=replace(service.get_sphirewallapi().general().logs(filter="dhcpd"), "\n", "<br>"),
        devices=service.get_sphirewallapi().network().devices(configured=True))


@app.route("/network/devices/addbridge")
def network_devices_createbridge():
    service.get_sphirewallapi().network().devices_createbridge()
    return redirect("/network/devices")


@app.route("/network/devices/<device>/aliases", methods=["POST"])
def network_devices_aliases(device):
    service.get_sphirewallapi().network().devices_aliases_add(device, request.form["alias"]);
    return redirect("/network/devices/" + device)


@app.route("/network/devices/<device>/aliases/delete/<alias>")
def network_devices_aliases_delete(device, alias):
    service.get_sphirewallapi().network().devices_aliases_del(device, alias)
    return redirect("/network/devices/" + device)


@app.route("/network/devices/<device>/dhcp", methods=["GET", "POST"])
def network_devices_dhcp(device):
    service.get_sphirewallapi().network().devices_dhcp(device, checkbox_value("enabled"), text_value("startIpAddress"),
        text_value("finishIpAddress"),
        text_value("domainNameService"), text_value("domainName"), text_value("defaultRouter"))
    return redirect("/network/devices/" + device)


@app.route("/network/devices/<device>/dhcp/leases", methods=["POST"])
def network_devices_dhcp_leases(device):
    service.get_sphirewallapi().network().devices_dhcp_static_add(device, text_value("hostname"), text_value("ip"),
        text_value("hw"))
    return redirect("/network/devices/" + device)


@app.route("/network/devices/<device>/dhcp/leases/delete/<hostname>/<ip>/<hw>", methods=["GET"])
def network_devices_dhcp_lease_delete(device, hostname, ip, hw):
    service.get_sphirewallapi().network().devices_dhcp_static_del(device, hostname, ip, hw)
    return redirect("/network/devices/" + device)


@app.route("/network/devices/routes", methods=["GET", "POST"])
def network_devices_routes():
    service.get_sphirewallapi().network().routes_add(text_value("dest"), text_value("gw"), text_value("device"))
    return redirect("/network/devices")


@app.route("/network/devices/routes/del/<id>", methods=["GET"])
def network_devices_routes_del(id):
    service.get_sphirewallapi().network().routes_del(id)
    return redirect("/network/devices")


@app.route("/network/connections", methods=["GET", "POST"])
def network_connections():
    if request.method == "POST":
        service.get_sphirewallapi().network().connections_add(text_value("name"), text_value("type"))

    return render_template("network_connections.html", connections=service.get_sphirewallapi().network().connections())


@app.route("/network/connections/delete/<name>", methods=["GET"])
def network_connections_del(name):
    service.get_sphirewallapi().network().connections_del(name)
    return redirect("/network/connections")


@app.route("/network/connections/<name>", methods=["GET", "POST"])
def network_connections_edit(name):
    connection = service.get_sphirewallapi().network().connections(name)
    if request.method == "POST":
        if connection["type"] == 0:
            service.get_sphirewallapi().network().connections_pppoe_save(name, text_value("username"),
                text_value("password"), text_value("authenticationType"), text_value("device"))
        if connection["type"] == 1:
            service.get_sphirewallapi().network().connections_openvpn_save(name, text_value("keyfile"))

    connection = service.get_sphirewallapi().network().connections(name)
    return render_template("network_connection_edit.html", connection=connection,
        log=replace(connection["log"], "\n", "<br>"))


@app.route("/network/connections/<name>/connect")
def network_connections_connect(name):
    service.get_sphirewallapi().network().connection_connect(name)
    return redirect("/network/connections/" + name)


@app.route("/network/connections/<name>/disconnect")
def network_connections_disconnect(name):
    service.get_sphirewallapi().network().connection_disconnect(name)
    return redirect("/network/connections/" + name)


@app.route("/network/dns", methods=["GET", "POST"])
def network_dns():
    if request.method == "POST":
        service.get_sphirewallapi().network().dns_config_set(text_value("domain"), text_value("search"),
            text_value("dns1"), text_value("dns2"))
    return render_template("network_dns.html", settings=service.get_sphirewallapi().network().dns_config())


@app.route("/network/vpn", methods=["GET", "POST"])
def network_vpn():
    if request.method == "POST":
        if text_value("type") == "static":
            service.get_sphirewallapi().network().vpns_openvpnstatic_create(text_value("name"))
        if text_value("type") == "tls":
            service.get_sphirewallapi().network().vpns_openvpntls_create(text_value("name"), text_value("certcountry"),
                text_value("certcity"), text_value("certprovince"), text_value("certorg"), text_value("certemail"))

    return render_template("network_vpn.html", vpns=service.get_sphirewallapi().network().vpns())


@app.route("/network/vpn/<name>/delete", methods=["GET", "POST"])
def network_vpn_delete(name):
    service.get_sphirewallapi().network().vpns_delete(name)
    return redirect("/network/vpn")


@app.route("/network/vpn/<name>", methods=["GET", "POST"])
def network_vpn_edit(name):
    vpn = service.get_sphirewallapi().network().vpns(name)
    if request.method == "POST":
        remoteServiceIp = request.form["remoteserverip"]
        remotePort = request.form["remoteport"]
        lzoCompression = checkbox_value("compression")
        customOptions = request.form["customopts"]

        if vpn["type"] == "tls":
            serverIp = request.form["serverip"]
            serverMask = request.form["servermask"]
            service.get_sphirewallapi().network().vpns_tls_save(name, remoteServiceIp, remotePort, lzoCompression,
                customOptions, serverIp, serverMask)

        if vpn["type"] == "static":
            serverIp = request.form["serverip"]
            clientIp = request.form["clientip"]
            service.get_sphirewallapi().network().vpns_static_save(name, remoteServiceIp, remotePort, lzoCompression,
                customOptions, serverIp, clientIp)

    vpn = service.get_sphirewallapi().network().vpns(name)
    return render_template("network_vpn_edit.html", log=replace(vpn["log"], "\n", "<br>"), vpn=vpn,
        clients=service.get_sphirewallapi().network().vpns_clients(name))


@app.route("/network/vpn/<name>/client/<clientName>")
def network_vpn_clientconfig(name, clientName):
    return render_template("blank.html",
        conf=replace(service.get_sphirewallapi().network().vpns_clients(name, clientName)["conf"], "\n", "<br>"))


@app.route("/network/vpn/<name>/client", methods=["POST"])
def network_vpn_client_create(name):
    service.get_sphirewallapi().network().vpns_clients_add(name, text_value("clientname"))
    return redirect("/network/vpn/" + name)


@app.route("/network/vpn/<name>/client/<client>/delete")
def network_vpn_client_delete(name, client):
    service.get_sphirewallapi().network().vpns_clients_delete(name, client)
    return redirect("/network/vpn/" + name)


@app.route("/network/vpn/<name>/start")
def network_vpn_start(name):
    service.get_sphirewallapi().network().vpns_start(name)
    return redirect("/network/vpn/" + name)


@app.route("/network/vpn/<name>/stop")
def network_vpn_stop(name):
    service.get_sphirewallapi().network().vpns_stop(name)
    return redirect("/network/vpn/" + name)


@app.route("/network/devices/publish")
def network_devices_publish():
    service.get_sphirewallapi().network().devices_publish()
    return redirect("/network/devices")


@app.route("/network/wireless", methods=["GET", "POST"])
def network_wireless():
    if request.method == "POST":
        service.get_sphirewallapi().general().configuration("wireless:enabled", checkbox_value("enabled"))
        service.get_sphirewallapi().general().configuration("wireless:interface", text_value("interface"))
        service.get_sphirewallapi().general().configuration("wireless:ssid", text_value("ssid"))
        service.get_sphirewallapi().general().configuration("wireless:channel", int(text_value("channel")))
        service.get_sphirewallapi().general().configuration("wireless:driver", text_value("driver"))

        if checkbox_value("enabled"):
            service.get_sphirewallapi().network().wireless_start()
        else:
            service.get_sphirewallapi().network().wireless_stop()

    enabled = service.get_sphirewallapi().general().configuration("wireless:enabled")
    interface = service.get_sphirewallapi().general().configuration("wireless:interface")
    channel = service.get_sphirewallapi().general().configuration("wireless:channel")
    ssid = service.get_sphirewallapi().general().configuration("wireless:ssid")
    driver = service.get_sphirewallapi().general().configuration("wireless:driver")
    online = service.get_sphirewallapi().network().wireless_online()
    devices = service.get_sphirewallapi().network().devices()

    return render_template("network_wireless.html", enabled=enabled, interface=interface, channel=channel, ssid=ssid,
        online=online, driver=driver, devices=devices)


@app.route("/network/dyndns", methods=["GET", "POST"])
def network_dyndns():
    if request.method == "POST":
        service.get_sphirewallapi().network().dyndns_configure(checkbox_value("enabled"), text_value("username"),
            text_value("password"), text_value("domain"), text_value("type"))

    return render_template("network_dyndns.html", config=service.get_sphirewallapi().network().dyndns_get())


def checkbox_value(key):
    if key in request.form:
        return True
    return False


def text_value(key):
    if key in request.form:
        return request.form[key]
    return ""


def arg(key):
    if key in request.args:
        return request.args[key]
    return ""