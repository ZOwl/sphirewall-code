/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <time.h>

using namespace std;
using std::string;

#include "Core/System.h"
#include "Utils/Utils.h"
#include "Auth/Session.h"
#include "Auth/User.h"

Session::Session(User* u, string mc, string ipaddress) {
	m_macAddr = mc.substr(0, 17);
	m_ip = ipaddress;

	m_session_start_time = time(NULL);
	touch();

	m_user = u;
}

Session::Session() {
}

string Session::getMac() const {
	return m_macAddr;
}

string Session::getUserName() const {
	return m_user->getUserName();
}

string Session::getIp() const {
	return m_ip;
}

int Session::getStartTime() const {
	return m_session_start_time;
}

int Session::getSessionLast() const {
	return m_last_packet_time;
}

void Session::touch() {
	m_last_packet_time = time(NULL);
}

User* Session::getUser() const {
	return m_user;
}

