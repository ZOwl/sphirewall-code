/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <iostream>

#include "Api/JsonManagementService.h"
#include "Core/Config.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/SysMonitor.h"
#include "Core/ConfigurationManager.h"

using namespace std;

class TestDelegate : public Delegate {
public:

	TestDelegate() {
		hit = false;
	}

	JSONObject process(std::string url, JSONObject object) {
		hit = true;

		JSONObject obj;
		obj.put(L"blah", new JSONValue(StringToWString("testing")));

		return obj;
	}

	string rexpression() {
		return "test/process";
	}

	bool hit;
};

class MockSysMonitor : public virtual SysMonitor {
public:

	MockSysMonitor() {

	}

};

TEST(JsonManagementService, parseToDelegate) {
	TestDelegate* del = new TestDelegate();

	JsonManagementService* svc = new JsonManagementService();
	svc->ignoreAuthentication(true);
	svc->registerDelegate(del);

	string output = svc->process("{\"request\":\"test\/process\",\"args\":null}");
	EXPECT_TRUE(del->hit == true);

	string expected = "{\"code\":0,\"response\":{\"blah\":\"testing\"}}";

	EXPECT_TRUE(expected.compare(output) == 0);
}

TEST(JsonManagementService, jsonErrorHandling) {
	TestDelegate* del = new TestDelegate();

	JsonManagementService* svc = new JsonManagementService();
	svc->ignoreAuthentication(true);
	svc->registerDelegate(new GeneralDelegate(new Config()));

	string output = svc->process("{\"request\":\"general\/config\/get\",\"args\":{\"vv\":null}}");
	string expected = "{\"code\":-1,\"message\":\"failed to pass request, missing field 'key'\"}";
	cout << "output:" << output << endl;
	EXPECT_TRUE(expected.compare(output) == 0);
}

TEST(JsonManagementService, delegateNotFound) {
	TestDelegate* del = new TestDelegate();

	JsonManagementService* svc = new JsonManagementService();
	svc->ignoreAuthentication(true);
	svc->registerDelegate(new GeneralDelegate(new Config()));

	string output = svc->process("{\"request\":\"dude\",\"args\":{\"vv\":null}}");

	string expected = "{\"code\":-1,\"message\":\"could not find delegate\"}";
	EXPECT_TRUE(expected.compare(output) == 0);
}

TEST(GeneralDelegate, runtime_list) {
	int a = 0, b = 1;
	Config* config = new Config();
	config->setConfigurationManager(new ConfigurationManager());
	config->getRuntime()->loadOrPut("key1", &a);
	config->getRuntime()->loadOrPut("key2", &b);

	GeneralDelegate* del = new GeneralDelegate(config);
	JSONObject pass;
	JSONObject obj = del->process("general/runtime/list", pass);

	JSONArray elems = obj[L"keys"]->AsArray();
	JSONObject o1 = elems[0]->AsObject();
	JSONObject o2 = elems[1]->AsObject();
	EXPECT_TRUE(o1[L"key"]->String().compare("key1") == 0);
	EXPECT_TRUE(o2[L"key"]->String().compare("key2") == 0);
}

TEST(GeneralDelegate, runtime_get) {
	int a = 0, b = 1;
	Config* config = new Config();
	config->setConfigurationManager(new ConfigurationManager());
	config->getRuntime()->loadOrPut("key1", &a);
	config->getRuntime()->loadOrPut("key2", &b);

	GeneralDelegate* del = new GeneralDelegate(config);
	JSONObject pass;
	pass.put(L"key", new JSONValue((std::string) "key1"));

	JSONObject obj = del->process("general/runtime/get", pass);

	EXPECT_TRUE(obj[L"value"]->AsNumber() == 0);
}

TEST(GeneralDelegate, runtime_set) {
	int a = 0, b = 1;
	Config* config = new Config();
	config->setConfigurationManager(new ConfigurationManager());
	config->getRuntime()->loadOrPut("key1", &a);
	config->getRuntime()->loadOrPut("key2", &b);

	GeneralDelegate* del = new GeneralDelegate(config);
	JSONObject pass;
	pass.put(L"key", new JSONValue((std::string) "key1"));
	pass.put(L"value", new JSONValue((double) 10));

	del->process("general/runtime/set", pass);

	EXPECT_TRUE(a == 10);
}

TEST(GeneralDelegate, runtime_set_invalid_entry) {
	Config* config = new Config();
	config->setConfigurationManager(new ConfigurationManager());
	GeneralDelegate* del = new GeneralDelegate(config);
	JSONObject pass;
	pass.put(L"key", new JSONValue((std::string) "key1"));
	pass.put(L"value", new JSONValue((double) 10));

	del->process("general/runtime/set", pass);
}

TEST(GeneralDelegate, runtime_get_invalid_entry) {
	Config* config = new Config();
	config->setConfigurationManager(new ConfigurationManager());
	GeneralDelegate* del = new GeneralDelegate(config);

	JSONObject pass;
	pass.put(L"key", new JSONValue((std::string) "key1"));

	bool exception = false;
	try {
		del->process("general/runtime/get", pass);
	} catch (DelegateGeneralException e) {
		exception = true;
	}

	EXPECT_TRUE(exception);
}

TEST(GeneralDelegate, set_logging_invalid_context){
	GeneralDelegate* del = new GeneralDelegate(NULL, new MockSysMonitor(), new LoggingConfiguration(new ConfigurationManager(), new SLogger::Logger()), NULL);
	JSONObject args;
	args.put(L"context", new JSONValue((string) "invalid-context"));
	args.put(L"level", new JSONValue((double) 1));

	bool exception = false;
	try {
		del->process("general/logging/set", args);
	} catch (DelegateGeneralException* e) {
		exception = true;
	}

	EXPECT_TRUE(exception);
}

TEST(GeneralDelegate, set_logging_invalid_level){
	SLogger::Logger* logger = new SLogger::Logger();
	GeneralDelegate* del = new GeneralDelegate(NULL, new MockSysMonitor(), new LoggingConfiguration(new ConfigurationManager(), logger), NULL);
	JSONObject args;
	args.put(L"context", new JSONValue((string) logger->getDefault()->name));
	args.put(L"level", new JSONValue((double) 100));

	bool exception = false;
	try {
		del->process("general/logging/set", args);
	} catch (DelegateGeneralException* e) {
		exception = true;
	}

	EXPECT_TRUE(exception);
}

TEST(GeneralDelegate, set_logging_valid){
	SLogger::Logger* logger = new SLogger::Logger();
	GeneralDelegate* del = new GeneralDelegate(NULL, new MockSysMonitor(), new LoggingConfiguration(new ConfigurationManager(), logger), NULL);
	JSONObject args;
	args.put(L"context", new JSONValue((string) logger->getDefault()->name));
	args.put(L"level", new JSONValue((double) 3));

	del->process("general/logging/set", args);

	EXPECT_TRUE(logger->getDefault()->level == (int) 3);
}


