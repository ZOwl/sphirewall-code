#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

class NetworkSettings:
    connection = None

    def __init__(self, connection):
        self.connection = connection

    def devices(self, device=None, configured=False):
        if device is not None:
            allDevices = self.devices(configured=configured)
            for d in allDevices:
                if d["interface"] == device:
                    return d

        return self.connection.request("network/devices/list", {"configuredDevices": configured})["devices"]

    def devices_remove(self, device):
        self.connection.request("network/devices/delete", {"device":device})

    def devices_set(self, device, ip, mask, dhcp, name, bridged, bridgeDevices, aliases="", gateway=""):
        args = {}
        args["interface"] = device
        args["ip"] = ip
        args["mask"] = mask
        args["dhcp"] = dhcp
        args["name"] = name

        args["bridge"] = bridged
        if bridgeDevices is not None:
            args["bridgeDevices"] = bridgeDevices
        args["aliases"] = aliases
        args["gateway"] = gateway

        self.connection.request("network/devices/set", args)

    def devices_aliases_add(self, device, ip):
        args = {}
        args["interface"] = device
        args["ip"] = ip
        self.connection.request("network/devices/alias/add", args)

    def devices_aliases_del(self, device, ip):
        args = {}
        args["interface"] = device
        args["ip"] = ip
        self.connection.request("network/devices/alias/delete", args)

    def devices_ddns(self, device, ddenabled, ddusername, ddpassword, dddomain):
        args = {}
        args["interface"] = device
        args["ddenabled"] = ddenabled
        args["ddusername"] = ddusername
        args["ddpassword"] = ddpassword
        args["dddomain"] = dddomain
        self.connection.request("network/devices/dynamicdns/set", args)

    def device_dhcp(self, device):
        args = {}
        args["interface"] = device
        return self.connection.request("network/dhcp/globals/get", args)

    def devices_dhcp(self, device, enabled, startIpAddress, finishIpAddress,
                     domainNameService, domainName, defaultRouter):
        args = {}
        args["enabled"] = enabled
        args["interface"] = device
        args["startIpAddress"] = startIpAddress
        args["finishIpAddress"] = finishIpAddress
        args["defaultRouter"] = defaultRouter
        args["domainNameService"] = domainNameService
        args["domainName"] = domainName

        self.connection.request("network/dhcp/globals/set", args)

    def devices_dhcp_static(self, device):
        args = {}
        args["interface"] = device
        return self.connection.request("network/dhcp/static", args)["items"]

    def devices_dhcp_static_add(self, device, hostname, ip, hw):
        args = {}
        args["interface"] = device
        args["hostname"] = hostname
        args["ip"] = ip
        args["hw"] = hw

        self.connection.request("network/dhcp/static/add", args)

    def devices_dhcp_static_del(self, device, hostname, ip, hw):
        args = {}
        args["interface"] = device
        args["hostname"] = hostname
        args["ip"] = ip
        args["hw"] = hw

        self.connection.request("network/dhcp/static/del", args)

    def devices_toggle(self, device):
        args = {}
        args["interface"] = device
        self.connection.request("network/devices/toggle", args)

    def routes(self):
        return self.connection.request("network/routes/list", None)["routes"]

    def routes_add(self, ip, gateway, device):
        args = {}
        args["dest"] = ip
        args["gw"] = gateway
        args["device"] = device
        self.connection.request("network/routes/add", args)

    def routes_del(self, id):
        args = {}
        args["id"] = id
        self.connection.request("network/routes/del", args)

    def connections(self, name=None):
        if name is not None:
            allConnections = self.connections()
            for conn in allConnections:
                if conn["name"] == name:
                    return conn

        args = {}
        return self.connection.request("network/connections/list", args)["items"]

    def connections_add(self, name, type):
        args = {}
        args["name"] = name
        args["type"] = int(type)
        self.connection.request("network/connections/add", args)

    def connections_del(self, name):
        args = {}
        args["name"] = name
        self.connection.request("network/connections/del", args)

    def connections_pppoe_save(self, name, username, password, authenticationType, interface):
        args = {}
        args["name"] = name
        args["username"] = username
        args["password"] = password
        args["authenticationType"] = int(authenticationType)
        args["device"] = interface
        self.connection.request("network/connections/save", args)

    def connections_openvpn_save(self, name, configuration):
        args = {}
        args["name"] = name
        args["keyfile"] = configuration
        self.connection.request("network/connections/save", args)

    def connection_disconnect(self, name):
        args = {}
        args["name"] = name
        self.connection.request("network/connections/disconnect", args)

    def connection_connect(self, name):
        args = {}
        args["name"] = name
        self.connection.request("network/connections/connect", args)

    def dns_config(self):
        return self.connection.request("network/dns/get", None)

    def dns_config_set(self, domain, search, ns1, ns2):
        args = {}
        args["domain"] = domain
        args["search"] = search
        args["ns1"] = ns1
        args["ns2"] = ns2
        self.connection.request("network/dns/set", args)

    def vpns(self, name=None):
        if name is not None:
            allVpns = self.vpns()
            for vpn in allVpns:
                if vpn["name"] == name:
                    return vpn

        return self.connection.request("network/openvpn/instances/list", None)["items"]

    def vpns_openvpnstatic_create(self, name):
        args = {}
        args["name"] = name
        args["type"] = "static"
        self.connection.request("network/openvpn/instances/create", args)

    def vpns_openvpntls_create(self, name, certcountry, certcity, certprovince, certorg, certemail):
        args = {}
        args["name"] = name
        args["type"] = "tls"
        args["certcountry"] = certcountry
        args["certcity"] = certcity
        args["certprovince"] = certprovince
        args["certorg"] = certorg
        args["certemail"] = certemail
        self.connection.request("network/openvpn/instances/create", args)


    def vpns_delete(self, name):
        args = {}
        args["name"] = name
        self.connection.request("network/openvpn/instances/remove", args)

    def vpns_set_option(self, name, key, value):
        args = {}
        args["name"] = name
        args[key] = value
        self.connection.request("network/openvpn/setoption", args)

    def vpns_tls_save(self, name, remoteServiceIp, remotePort, lzoCompression, customOptions, serverIp, serverMask):
        self.vpns_set_option(name, "customopts", customOptions)
        self.vpns_set_option(name, "compression", lzoCompression)
        self.vpns_set_option(name, "remoteserverip", remoteServiceIp)
        self.vpns_set_option(name, "remoteport", int(remotePort))

        self.vpns_set_option(name, "serverip", serverIp)
        self.vpns_set_option(name, "servermask", serverMask)

    def vpns_static_save(self, name, remoteServiceIp, remotePort, lzoCompression, customOptions, serverIp, clientIp):
        self.vpns_set_option(name, "customopts", customOptions)
        self.vpns_set_option(name, "compression", lzoCompression)
        self.vpns_set_option(name, "remoteserverip", remoteServiceIp)
        self.vpns_set_option(name, "remoteport", int(remotePort))

        self.vpns_set_option(name, "serverip", serverIp)
        self.vpns_set_option(name, "clientip", clientIp)

    def vpns_clients(self, name, clientName=None):
        if clientName is not None:
            allClients = self.vpns_clients(name)
            for c in allClients:
                if c["name"] == clientName:
                    print "got a response:" + c["name"]
                    return c
        args = {}
        args["name"] = name
        return self.connection.request("network/openvpn/client/list", args)["clients"]

    def vpns_clients_add(self, name, clientName):
        args = {}
        args["name"] = name
        args["clientname"] = clientName
        self.connection.request("network/openvpn/client/add", args)

    def vpns_clients_delete(self, name, clientName):
        args = {}
        args["name"] = name
        args["clientname"] = clientName
        self.connection.request("network/openvpn/client/delete", args)

    def devices_publish(self):
        self.connection.request("network/devices/publish", None)

    def vpns_start(self, name):
        args = {}
        args["name"] = name
        self.connection.request("network/openvpn/start", args)

    def vpns_stop(self, name):
        args = {}
        args["name"] = name
        self.connection.request("network/openvpn/stop", args)

    def wireless_start(self):
        self.connection.request("network/wireless/start", None)

    def wireless_stop(self):
        self.connection.request("network/wireless/stop", None)

    def wireless_online(self):
        return self.connection.request("network/wireless/online", None)["status"]

    def dyndns_configure(self, enabled, username, password, domain, type):
        args = {}
        args["enabled"] = enabled
        args["username"] = username
        args["password"] = password
        args["domain"] = domain
        args["type"] = int(type)
        self.connection.request("network/dyndns/set", args)

    def dyndns_get(self):
        return self.connection.request("network/dyndns", None)

    def isSynced(self):
        return self.connection.request("network/devices/synced", None)["synced"]

    def devices_createbridge(self):
        return self.connection.request("network/devices/createbridge", None)