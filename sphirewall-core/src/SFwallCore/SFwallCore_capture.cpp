/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <linux/types.h>
#include <sys/types.h>
#include <sstream>
#include <vector>

#include "sphirewall_queue.h"
#include "client.h"

using namespace std;

#include "Utils/Checksum.h"
#include "SFwallCore/Utils.h"
#include "Core/System.h"
#include "Utils/IP4Addr.h"
#include "SFwallCore/Capture.h"
#include "SFwallCore/Acl.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/ConnTracker.h"
#include "BandwidthDb/Bandwidth.h"
#include "Core/HostDiscoveryService.h"
#include "SFwallCore/ApplicationLevelFilter.h"
#include "SFwallCore/Rewrite.h"

struct sq_packet* SFwallCore::Packet::cloneSqp(struct sq_packet* sqp){
	struct sq_packet* copy = (struct sq_packet*) malloc(sizeof(struct sq_packet));
	unsigned char* dataCopy = (unsigned char*) malloc(RAWSIZE);	
	
	memcpy(copy, sqp, sizeof(struct sq_packet));
	memcpy(dataCopy, sqp->raw_packet, RAWSIZE);
	copy->raw_packet = dataCopy;
	return copy;
}

struct NatInstruction* SFwallCore::processPrerouting(struct sq_packet *sqp) {
	Firewall* sFirewall = System::getInstance()->getFirewall();
	if(sFirewall->PREROUTING_ENABLED == 1){
		Packet *packet = Packet::parsePacket(sqp); //very cheap operation, just sets up pointers
		if (packet && (packet->getProtocol() == TCP || packet->getProtocol() == UDP || packet->getProtocol() == ICMP)) {
			PacketDirection pd;
			sFirewall->acls->getlock();
			NatRule* natRule = sFirewall->acls->matchNatAcls(packet, DNAT);
			if (natRule) {
				struct NatInstruction* instruction = (struct NatInstruction*) malloc(sizeof(struct NatInstruction));				
				instruction->type = NAT_DNAT;
				instruction->targetPort = natRule->natPort;
				instruction->targetAddress = natRule->natTarget;
		
				sFirewall->acls->releaselock();
				delete packet;
				return instruction;
			}
			sFirewall->acls->releaselock();
		}
		delete packet;
	}
	return NULL;
}

int SFwallCore::processFilterHook(struct sq_packet *sqp, bool &modified) {
	Firewall* sFirewall = System::getInstance()->getFirewall();
	SLogger::LogContext* logger = sFirewall->getLogContext();

	Timer* timer = new Timer();
	timer->start();

	Packet* packet = NULL;
	Connection* conn = NULL;
	int verdict = SQ_DENY;
	int nice = 1;
	sFirewall->connectionTracker->lock.lock();
	packet = Packet::parsePacket(sqp);

	if (packet && !packet->isBroadcast() && !packet->isMulticast() 
		&& (packet->getProtocol() == TCP || packet->getProtocol() == UDP || packet->getProtocol() == ICMP) && !(sFirewall->acls->existsInDenyList(packet))) {

		conn = sFirewall->connectionTracker->offer(packet);
		if (conn) {
			//Add it for the hooks:
			packet->setConnection(conn);
			nice = conn->nice;
			if (sFirewall->QOS_ENABLED == 1 && conn->getQosBucket()) {
				verdict = SQ_QUEUE;
			} else {
				verdict = SQ_ACCEPT;
			}

			//Given what we know, must we rewrite this something? 
			if(sFirewall->REWRITE_ENABLED == 1 && !conn->getRewriteRule() && !conn->isRewriteVerdictApplied() && !conn->getSession()){
				if(HttpTcpPacket::matchPort(conn->getDestinationPort())){
					conn->setRewriteRule(sFirewall->rewriteEngine->process(conn, packet));
				}

				if(sFirewall->REWRITE_PROXY_REQUESTS_ENABLED == 1){
					if(HttpProxyPacket::matchPort(conn->getDestinationPort())){
						conn->setRewriteRule(sFirewall->rewriteEngine->process(conn, packet));
					}
				}

				//Its possible, that FORCE_LOGIN is set, there is no session, but the traffic is https, in which case we must rst the connection
				if(HttpsTcpPacket::matchPort(conn->getDestinationPort())){
					RewriteRule* rewrite = sFirewall->rewriteEngine->process(conn, packet);
					if(rewrite){
						verdict = SQ_ACCEPT;
						conn->terminate();
						delete rewrite;
					}
				}

			}
			
			//Must we process a rewrite
			if(sFirewall->REWRITE_ENABLED == 1 && conn->getRewriteRule()){
				if(conn->getRewriteRule()->match(packet)){
					conn->getRewriteRule()->rewrite(conn, packet);
					modified = true;
				}
			}

			if(conn->mustLog()){
				logger->log(SLogger::EVENT, packet->toString());
			}

			//Check if this connection is marked for termination:
			//TCP connections can be reset, others are just blocked until timeout:
			if (conn->isTerminating() && conn->checkPacket(packet, Connection::DIR_OPPOSITE)) {
				if (conn->getProtocol() == TCP) {
					Utils::setRstFlag(sqp->raw_packet, sqp->len);
					modified = true;
					sFirewall->noRstSampler->input(1);
					verdict = SQ_ACCEPT;
				} else {
					sFirewall->noRstSampler->input(1);
					verdict = SQ_DENY;
				}
			}else{
				if(!sFirewall->httpFilter->filter(packet)){
					//Are we rewriting this request ? 
					if(!conn->getRewriteRule()){
						verdict = SQ_ACCEPT;
						conn->terminate();
					}

					if(HttpsTcpPacket::matchPort(packet->getDstPort())){
						verdict = SQ_ACCEPT;
						conn->terminate();
					}
				}
			}

		} else {
			//Check the rules
			System::getInstance()->getArp()->update(packet);
			bool capturePortal = false;
			sFirewall->acls->getlock();
			FilterRule* matchingRulePtr = sFirewall->acls->matchFilterAcls(packet, capturePortal);
			if (matchingRulePtr) {
				if((verdict = matchingRulePtr->action) == SQ_ACCEPT){
					if(!matchingRulePtr->ignoreconntrack){
						conn = sFirewall->connectionTracker->create(packet);
					}
					if (conn) {
						packet->setConnection(conn);

						if(sFirewall->QOS_ENABLED == 1){
							conn->setQosBucket(sFirewall->trafficShaper->add(packet));
							if (conn->getQosBucket()) {
								verdict = SQ_QUEUE;
							}

							PriorityRule* pr = sFirewall->acls->matchPriorityQos(packet);
							if(pr != NULL){
								nice = conn->nice = pr->nice;
							}else{
								nice = conn->nice = 1;
							}
						}

						conn->capturePortal = capturePortal;	

						SessionDb* sessionDb = System::getInstance()->getSessionDb();
						if(sessionDb->tryLock()){
							Session* session = sessionDb->get(packet->getHw(), IP4Addr::ip4AddrToString(packet->getSrcIp()));
							if (session) {
								conn->setSession(session);
								session->touch();
							}
							sessionDb->releaseLock();						
						}

						/*if(sFirewall->REWRITE_ENABLED == 1){
							if(HttpTcpPacket::matchPort(conn->getDestinationPort())){	
								conn->setRewriteRule(sFirewall->rewriteEngine->process(conn, packet));
							}

							if(sFirewall->REWRITE_PROXY_REQUESTS_ENABLED == 1){
								if(HttpProxyPacket::matchPort(conn->getDestinationPort())){  
									conn->setRewriteRule(sFirewall->rewriteEngine->process(conn, packet));
								}
							}

							//Its possible, that FORCE_LOGIN is set, there is no session, but the traffic is https, in which case we must rst the connection
							if(!conn->getSession() && sFirewall->rewriteEngine->CAPTURE_PORTAL_FORCE == 1 && HttpsTcpPacket::matchPort(conn->getDestinationPort())){
								verdict = SQ_ACCEPT;
								conn->terminate();
							}

						}*/
						conn->setMustLog(matchingRulePtr->log);	
					}

					if(matchingRulePtr->log){
						logger->log(SLogger::EVENT, packet->toString());
					}
				}

				if(matchingRulePtr->log){
					logger->log(SLogger::EVENT, packet->toString());
				}
			}
			sFirewall->acls->releaselock();
		}

		if(sFirewall->POST_PROCESSOR_ENABLED == 1){
			sFirewall->post->add(packet);
		}
	} else if (packet && !(sFirewall->acls->existsInDenyList(packet))) {

		bool capture = false;
		sFirewall->acls->getlock();
		FilterRule* matchingRulePtr = sFirewall->acls->matchFilterAcls(packet, capture);
		if (matchingRulePtr){
			verdict = matchingRulePtr->action;
			if(matchingRulePtr->log){               
				logger->log(SLogger::EVENT, packet->toString());
			}       
		}
		sFirewall->acls->releaselock();
	}

	if (sFirewall->QOS_ENABLED == 1 && (verdict == SQ_QUEUE && conn)) {
		if(conn->getQosBucket()){
			if (conn->checkPacket(packet, SFwallCore::Connection::DIR_SAME)) {
				conn->getQosBucket()->addPacketToQueue(SFwallCore::Packet::cloneSqp(sqp), false);
			} else {
				conn->getQosBucket()->addPacketToQueue(SFwallCore::Packet::cloneSqp(sqp), true);
			}
		}
	}

	sFirewall->connectionTracker->lock.unlock();
	//Last but not least --> lets update the counters for metrics
	if (packet) {
		switch (packet->getProtocol()) {
			case TCP:
				sFirewall->totalTcpPackets++;
				break;
			case UDP:
				sFirewall->totalUdpPackets++;
				break;
			case ICMP:
				sFirewall->totalIcmpPackets++;
				break;
		}

		//Final dealings before sending the verdict
		if (verdict == SQ_DENY) {
			logger->log(SLogger::INFO, "processFilterHook()", "Dropping packet: " + packet->toString());
			sFirewall->totalDefaultAction++;
		}
	}

	sFirewall->transferSampler->input(packet->getLen());
	timer->stop();
	sFirewall->avgPacketProcess->input(timer->value());
	sFirewall->noPacketsSampler->input(1);

	//Cleanup:
	delete packet;
	delete timer;

	if(verdict == SQ_QUEUE){
		sFirewall->noAcceptsSampler->input(1);
		return 0;
	}else if(verdict == SQ_DENY){
		sFirewall->noDropsSampler->input(1);	
		return -1;
	}else{
		if(sFirewall->QOS_ENABLED == 1 && sFirewall->acls->listPriorityRules().size() > 0){
			sFirewall->noAcceptsSampler->input(1);
			return nice + 1;
		}else{
			sFirewall->noAcceptsSampler->input(1);
			return 1;
		}
	}
}

struct NatInstruction* SFwallCore::processPostrouting(struct sq_packet *sqp) {
	Firewall* sFirewall = System::getInstance()->getFirewall();
	if(sFirewall->POSTROUTING_ENABLED == 1){
		Packet *packet = Packet::parsePacket(sqp);
		PacketDirection pd;

		if (packet && (packet->getProtocol() == TCP || packet->getProtocol() == UDP || packet->getProtocol() == ICMP)) {
			sFirewall->acls->getlock();
			NatRule* natRule = sFirewall->acls->matchNatAcls(packet, SNAT);
			if (natRule) {
				struct NatInstruction* instruction = (struct NatInstruction*) malloc(sizeof(struct NatInstruction));
				instruction->type = NAT_SNAT;
				instruction->targetAddress = natRule->getUsableNatTargetIp(); 

				sFirewall->acls->releaselock();
				delete packet;
				return instruction;
			}
			sFirewall->acls->releaselock();
		}

		delete packet;
	}
	return NULL; // not modified
}

void* SFwallCore::startCapture() {
	SqClient* client = &System::getInstance()->getFirewall()->sqClient;

	System::getInstance()->logger.getDefault()->log(SLogger::EVENT, "SFwallCore::startCapture","Attempting to establish a connction with the kernel module", true);
	client->registerFilterCallback(&processFilterHook);
	client->registerPreroutingCallback(&processPrerouting);
	client->registerPostroutingCallback(&processPostrouting);

	if(client->connect() < 0){
		System::getInstance()->logger.getDefault()->log(SLogger::CRITICAL_ERROR, "SFwallCore::startCapture","Could not connect to the sphirewall_queue kernel module", true);
		System::getInstance()->init("", EXIT);
	}

	client->setYieldCountSetting(&SFwallCore::Firewall::SQUEUE_DEQUEUE_YIELD_TIMEOUT);
	client->setUsleepInterval(&SFwallCore::Firewall::SQUEUE_DEQUEUE_YIELD_SLEEP_INTERVAL);

	System::getInstance()->logger.getDefault()->log(SLogger::EVENT, "SFwallCore::startCapture","Core packet capture started", true);
	client->run();
	System::getInstance()->logger.getDefault()->log(SLogger::EVENT, "SFwallCore::startCapture","Core packet capture stopped", true);
}

