/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;
using std::string;

#include "Auth/UserDb.h"
#include "Auth/SessionDb.h"
#include "Core/System.h"
#include "Utils/Utils.h"
#include "Auth/Session.h"
#include "SFwallCore/ConnTracker.h"
#include "Auth/User.h"

void SessionDb::loadDb() {
	sessions.clear();
}

Session* SessionDb::get(string mac, string ip) {
	Session *ret = NULL;
	map<string, std::list<Session*> >::iterator iter = sessions.find(mac);
	while (iter != sessions.end()) {
		std::list<Session*>& inner = iter->second;
		for(std::list<Session*>::iterator iiter = inner.begin();
				iiter != inner.end();
				iiter++){

			Session* target = (*iiter);
			if ((int) mac.find(iter->first) != -1 && target->getIp().compare(ip) == 0) {
				return target;
			}
		}
		++iter;
	}
	return ret;
}

void SessionDb::deleteSession(Session* session) {
        if (session != NULL) {
                removeFromStore(session);
        }

	removeFromTracker(session);
	delete session;
}

void SessionDb::removeFromTracker(Session* session){
	plainConnTracker->lock.lock();

	SFwallCore::ConnectionCriteria criteria;
	criteria.setSession(session);

	std::list<SFwallCore::Connection*> connections = plainConnTracker->listConnections(criteria);
	for (std::list<SFwallCore::Connection*>::iterator iter = connections.begin(); iter != connections.end(); iter++) {
		SFwallCore::Connection* connection = (*iter);
		if (connection) {
			connection->terminate();
			connection->setSession(NULL);
		}
	}

	plainConnTracker->lock.unlock();
}

void SessionDb::removeFromStore(Session* session){
	for (map<string, std::list<Session*> >::iterator iter = sessions.begin(); iter != sessions.end(); iter++) {
		std::list<Session*>& inner = iter->second;
		for(std::list<Session*>::iterator iiter = inner.begin();
				iiter != inner.end();
				iiter++){

			Session* target = (*iiter);
			if (session == target) {
				inner.erase(iiter);
				break;
			}
		}
	}
}

std::list<Session*> SessionDb::findSessionsForUser(User* user) {
	std::list<Session*> ret;

	std::map<string, std::list<Session*> >::iterator iter = sessions.begin();
	while (iter != sessions.end()) {
		std::list<Session*>& inner = iter->second; 
		for(std::list<Session*>::iterator iiter = inner.begin();
				iiter != inner.end();
				iiter++){

			Session* target = (*iiter);
			if (target->getUser() == user) {
				ret.push_back(target);
			}
		}
		++iter;
	}
	return ret;
}

void SessionDb::handleDeleteUser(User* user){
	std::list<Session*> sessions = findSessionsForUser(user);
	for(std::list<Session*>::iterator iter = sessions.begin();
			iter != sessions.end();
			iter++){

		deleteSession((*iter));
	}	
}

string SessionDb::findUser(string ip) {
	map<string, std::list<Session*> >::iterator iter = sessions.begin();
	while (iter != sessions.end()) {
		std::list<Session*>& inner = iter->second; 
		for(std::list<Session*>::iterator iiter = inner.begin();
				iiter != inner.end();
				iiter++){

			Session* target = (*iiter);     
			if (target->getIp().compare(ip) == 0) {
				return target->getUserName();
			}
		}
		++iter;
	}
	return "unknown";
}

vector<Session*> SessionDb::list() {
	vector<Session*> ret;
	for (map<string, std::list<Session*> >::iterator iter = sessions.begin();
			iter != sessions.end();
			++iter) {

		std::list<Session*>& inner = iter->second; 
		for(std::list<Session*>::iterator iiter = inner.begin();
				iiter != inner.end();
				iiter++){

			Session* target = (*iiter);     
			ret.push_back(target);
		}
	}
	return ret;
}

void SessionDb::CleanUpCron::run() {
	sessionDb->holdLock();
	string user;

	int timeout = System::getInstance()->configurationManager.get("general:session_timeout")->number();
	if(timeout == 0){
		sessionDb->releaseLock();
		return;
	}

	std::vector<Session*> toDelete;
	std::vector<Session*> targets = sessionDb->list();
	for(int x = 0; x < targets.size(); x++) {
		Session* s = targets[x];
		if ((time(NULL) - s->getSessionLast()) > timeout) {
			sessionDb->removeFromStore(s);
			toDelete.push_back(s);
		}
	}
	sessionDb->releaseLock();

	for(int x = 0; x < toDelete.size(); x++) {
		Session* s = toDelete[x];
		sessionDb->removeFromTracker(s);
		delete s;
	}
}

Session* SessionDb::create(User* user, string hwAddress, string ip) {
	Session* session = new Session(user, hwAddress, ip);
	sessions[hwAddress].push_back(session);
	return session;
}

