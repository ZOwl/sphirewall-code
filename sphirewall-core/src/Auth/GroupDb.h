/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_GROUPDB_H_INCLUDED
#define SPHIREWALL_GROUPDB_H_INCLUDED

#include "Core/Logger.h"
#include "Utils/Lock.h"
#include "Core/Lockable.h"

#include <map>
#include <vector>
#include <list>

class ConfigurationManager;
class Group;

class GroupRemovedListener {
	public:
		virtual void groupRemoved(Group* group) = 0;
};

class GroupDb : Lockable {
	public:
		GroupDb(SLogger::LogContext* logger, ConfigurationManager* config) 
			: Lockable(), logger(logger), config(config) {
			}

		void loadDb();
		void save();

		Group* createGroup(std::string name);
		bool delGroup(Group*);

		Group* getGroup(int id);
		Group* getGroup(std::string gName);
		std::vector<Group*> list();
		std::vector<Group*> listGroupsByRule(std::string);

		void lock();
		void unlock();
		void registerRemovedListener(GroupRemovedListener* listener);
	private:
		int findUniqueId();
		std::map<int, Group*> groups;
		ConfigurationManager* config;
		SLogger::LogContext* logger;

		std::list<GroupRemovedListener*> removeListeners; 
};

#endif


