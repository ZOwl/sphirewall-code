/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef POST_H
#define POST_H

#include "Packet.h"
#include "Core/Logger.h"

#include <list>
#include <queue>

namespace SFwallCore {

	class Firewall;
	class Handler {
	public:
		virtual void handle(Packet* packet) = 0;
	};

	class PostProcessor {
	public:

		PostProcessor(SLogger::LogContext* logger, Firewall* firewall) 
			: logger(logger), firewall(firewall) {
			
			lock = new Lock();
		}

		void add(Packet* packet);
		void roll();
		void registerHandler(Handler* handler);
		void start();

	private:
		std::list<Handler*> handlers;
		std::queue<Packet*> q;

		SLogger::LogContext* logger;
		Firewall* firewall;
		Lock* lock;
	};

};
#endif
