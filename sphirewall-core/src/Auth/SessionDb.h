/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_SESSIONSDB_H_INCLUDED
#define SPHIREWALL_SESSIONSDB_H_INCLUDED

#include "Auth/Session.h"
#include "Core/Logger.h"
#include "Core/Cron.h"
#include "Core/Lockable.h"

namespace SFwallCore {
	class PlainConnTracker;
};

class User;
class DeleteUserListener;
class SessionDb : public virtual DeleteUserListener, public Lockable {
	public:

		class CleanUpCron : public CronJob {
			public:
				CleanUpCron(SessionDb* sessionDb) 
					: CronJob(60 * 5, "SESSIONDB_CLEANUP_CRON", true), sessionDb(sessionDb) {
					}
				void run();
			private:
				SessionDb* sessionDb;
		};

		/*!	Constructor methods	*/
		SessionDb(SLogger::LogContext* logger, CronManager* cronManager, SFwallCore::PlainConnTracker* plainConnTracker) : Lockable(), logger(logger), plainConnTracker(plainConnTracker) {
			CleanUpCron* cron = new CleanUpCron(this);

			if (cronManager != NULL) {
				cronManager->registerJob(cron);
			}
		};

		void loadDb();

		Session* create(User* user, string hwAddress, string ip);
		Session* get(string mac, string ip);
		void deleteSession(Session* session);

		std::list<Session*> findSessionsForUser(User* user);

		string findUser(string);

		vector<Session*> list();
		std::multimap<string, int> failCount;
		void handleDeleteUser(User*);

		void removeFromTracker(Session* session);
		void removeFromStore(Session* session);
	private:
		std::map<std::string, std::list<Session*> > sessions;
		SLogger::LogContext* logger;
		SFwallCore::PlainConnTracker* plainConnTracker;
};

#endif
