/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QQ_QUEUE
#define QQ_QUEUE

#ifdef USE_PTHREADS
	#define DO_YIELD() pthread_yield()
#else
	#define DO_YIELD() yield()
#endif

struct meta {
        int head;
        int tail;
        int capacity;
        int elem_size;
	int offset;
	int closing;
	int pid;
};

#define STATE_OFFLINE 0
#define STATE_ONLINE 1

struct qmgr {
	struct meta* block;
	int state;
};

extern struct qmgr* qmgr_recv;
extern struct qmgr* qmgr_send;

extern struct meta* queue_init(int cap, int elem_size, void* memory_block);

extern struct message* queue_enqueue(struct qmgr*);
extern void queue_push(struct qmgr*);

extern void queue_pop(struct qmgr*);
extern struct message* queue_peek(struct qmgr*);
#endif

