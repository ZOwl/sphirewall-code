/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gcrypt.h>
#include <iostream>

using namespace std;

#include "Core/System.h"
#include "Utils/Hash.h"

/* Creates a hash from string input
 */
string Hash::create_hash(string input, int unhashed_len) {
	if(System::getInstance()->configurationManager.hasByPath("general:hashpasswords") && System::getInstance()->configurationManager.get("general:hashpasswords")->boolean()){
		gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);
		int hash_len = gcry_md_get_algo_dlen(GCRY_MD_SHA1);
		char ret[hash_len];
		int msg_len = unhashed_len; // set length

		unsigned char hash[hash_len];

		char *out = (char *) malloc(sizeof (char) * ((hash_len * 2) + 1));
		char *p = out;

		gcry_md_hash_buffer(GCRY_MD_SHA1, hash, input.c_str(), msg_len);

		int i;
		for (i = 0; i < hash_len; i++, p += 2) {
			snprintf(p, 3, "%02x", hash[i]);
		}
		// cleanup
		strcpy(ret, out);
		free(out);
		return ret;
	}else{
		return input;
	}
}

bool Hash::check_hash(string check, string compare) {
	string log_buffer;
	if (!check.compare(compare)) {
		return true;
	}
	return false;
}
