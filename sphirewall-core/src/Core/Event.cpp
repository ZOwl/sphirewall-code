/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <time.h>
#include <string>
#include <time.h>
#include <map>
#include <sstream>
#include <vector>
#include <queue>
#include <list>

using namespace std;

#include "Core/Event.h"
#include "Utils/Utils.h"
#include "Core/System.h"
#include "Auth/UserDb.h"
#include "Auth/GroupDb.h"
#include "Core/Logger.h"
#include "Core/EventHandler.h"
#include "SFwallCore/Rewrite.h"

const char* IDS_EVENT = "event.ids";
const char* IDS_PORTSCAN = "event.ids.portscan";
const char* IDS_SSHD_BRUTEFORCE = "event.ids.sshbruteforce";
const char* SYSTEM_MONITOR = "event.sysmonitor.alert";
const char* IDS_FLOOD_ATTACK = "event.ids.flood";
const char* IDS_SNORT_ALERT = "event.ids.snort";
const char* IDS_SNORT_ALERT_1 = "event.ids.snort.1";
const char* IDS_SNORT_ALERT_2 = "event.ids.snort.2";
const char* IDS_SNORT_ALERT_3 = "event.ids.snort.3";
const char* IDS_SNORT_ALERT_4 = "event.ids.snort.4";
const char* IDS_SNORT_ALERT_5 = "event.ids.snort.5";
const char* USERDB_LOGIN_FAILED = "event.userdb.auth.failed";
const char* USERDB_LOGIN_SUCCESS = "event.userdb.auth.success";
const char* USERDB_QUOTA_EXCEEDED = "event.userdb.quota.exceeded";
const char* USERDB_QUOTA_OK = "event.userdb.quota.ok";
const char* USERDB_SESSION_CREATE= "event.userdb.session.create";
const char* USERDB_SESSION_TIMEOUT = "event.userdb.session.timeout";
const char* USERDB_LOGOUT = "event.userdb.logout";
const char* AUDIT_EVENT = "event.audit";
const char* EVENT_PURGE = "event.purge";
const char* AUDIT_CONFIGURATION_CHANGED = "event.audit.configuration.general.set";
const char* AUDIT_RPC_CALL="event.audit.rpc";
const char* AUDIT_CONFIGURATION_IDS_EXCEPTION_ADDED = "event.audit.configuration.ids.exceptions.add";
const char* AUDIT_CONFIGURATION_IDS_EXCEPTION_REMOVED= "event.audit.configuration.ids.exceptions.removed";
const char* AUDIT_CONFIGURATION_NETWORK_DEVICES_TOGGLE= "event.audit.configuration.network.devices.toggle";
const char* AUDIT_CONFIGURATION_NETWORK_DEVICES_SETIP= "event.audit.configuration.network.devices.setip";
const char* AUDIT_CONFIGURATION_NETWORK_DNS_SET= "event.audit.configuration.network.dns.set";
const char* AUDIT_CONFIGURATION_NETWORK_ROUTES_ADD= "event.audit.configuration.network.routes.add";
const char* AUDIT_CONFIGURATION_NETWORK_ROUTES_DEL= "event.audit.configuration.network.routes.del";
const char* AUDIT_CONFIGURATION_NETWORK_DHCP_SET= "event.audit.configuration.network.dhcp.set";
const char* AUDIT_CONFIGURATION_NETWORK_DHCP_PUBLISH= "event.audit.configuration.network.dhcp.publish";
const char* AUDIT_CONFIGURATION_NETWORK_DHCP_START= "event.audit.configuration.network.dhcp.start";
const char* AUDIT_CONFIGURATION_NETWORK_DHCP_STOP= "event.audit.configuration.network.dhcp.stop";
const char* AUDIT_CONFIGURATION_NETWORK_CONNECTION_START= "event.audit.configuration.network.connection.start";
const char* AUDIT_CONFIGURATION_NETWORK_CONNECTION_STOP= "event.audit.configuration.network.connection.stop";
const char* AUDIT_CONFIGURATION_GROUPDB_ADD= "event.audit.configuration.groupdb.add";
const char* AUDIT_CONFIGURATION_GROUPDB_DEL= "event.audit.configuration.groupdb.del";
const char* AUDIT_CONFIGURATION_GROUPDB_MODIFIED= "event.audit.configuration.groupdb.modified";
const char* AUDIT_CONFIGURATION_USERDB_ADD= "event.audit.configuration.userdb.add";
const char* AUDIT_CONFIGURATION_USERDB_DEL= "event.audit.configuration.userdb.del";
const char* AUDIT_CONFIGURATION_USERDB_MODIFIED= "event.audit.configuration.userdb.modified";
const char* AUDIT_CONFIGURATION_USERDB_SETPASSWORD= "event.audit.configuration.userdb.setpassword";
const char* AUDIT_CONFIGURATION_USERDB_GROUPS_ADD= "event.audit.configuration.userdb.groups.add";
const char* AUDIT_CONFIGURATION_USERDB_GROUPS_DEL= "event.audit.configuration.userdb.groups.del";
const char* AUDIT_CONFIGURATION_USERDB_DISABLE= "event.audit.configuration.userdb.disable";
const char* AUDIT_CONFIGURATION_USERDB_ENABLE= "event.audit.configuration.userdb.enable";
const char* AUDIT_FIREWALL_CONNECTIONS_TERMINATE = "event.audit.firewall.connections.terminate";
const char* NETWORK_DEVICES_ADDED = "event.network.devices.added";
const char* NETWORK_DEVICES_CHANGED = "event.network.devices.change";
const char* NETWORK_DEVICES_REMOVED = "event.network.devices.removed";
const char* NETWORK_ARP_DISCOVERED = "event.network.arp.discovered";
const char* NETWORK_ARP_TIMEDOUT = "event.network.arp.timedout";
const char* RUNTIME_METRIC = "event.metric";
const char* START = "event.start";

const char* FIREWALL_WEBFILTER_HIT = "event.firewall.webfilter.hit";
const char* GLOBAL_QUOTA_EXCEEDED = "event.global.quota.exceeded";
const char* GLOBAL_QUOTA_OK = "event.global.quota.ok";

const char* WATCHDOG_INTERNET_CONNECTION_UP = "event.watchdog.connectivity.google.up";
const char* WATCHDOG_INTERNET_CONNECTION_DOWN = "event.watchdog.connectivity.google.down";

int EventDb::EVENT_DB_PURGE_THRESHOLD = 60 * 10;
void EventDb::add(Event* e) {
	if(async){
		boost::thread(&EventDb::runhandlers, this, e);
	}else{
		this->runhandlers(e);
	}
	
	lock->lock();
	events.insert(events.begin(), e);
	lock->unlock();
}

void EventDb::runhandlers(Event* e){
	multimap<std::string, EventHandler*>::iterator iter;
        for (iter=handlers.begin(); iter!=handlers.end(); ++iter){
                EventHandler* handler = iter->second;
                std::string hkey = iter->first;
                if(e->key.find(hkey) != -1){
                        handler->handle(e);
                }
        }
}

void EventDb::purgeEvents() {
	lock->lock();
	for(vector<Event*>::iterator iter = events.begin(); iter != events.end(); iter++){
                delete (*iter);
	}

	events.clear();
	lock->unlock();
	add(new Event(EVENT_PURGE, EventParams()));
}

void EventDb::purgeOldEvents() {
	lock->lock();
	int minTime = time(NULL) - EVENT_DB_PURGE_THRESHOLD; 
	vector<Event*>::iterator iter = events.begin();
	while(iter != events.end()){
		Event* event = (*iter);
		if(event->t < minTime){
			iter = events.erase(iter);		
			delete event;
		}else{
			iter++;
		}
	}	
	lock->unlock();
}

int EventDb::size() {
	return events.size();
}

EventDb::EventDb(SLogger::Logger* logger, Config* config)
	: logger(logger), config(config), CronJob(60 * 5, "EVENT_DB_PURGE", true){
		this->async = true;
		this->lock = new Lock();
	
		if(config){
			config->getRuntime()->loadOrPut("EVENT_DB_PURGE_THRESHOLD", &EVENT_DB_PURGE_THRESHOLD);
		}
	}

EventDb::EventDb(SLogger::Logger* l) : CronJob(60 * 5, "EVENT_DB_PURGE", true) {
	EventDb(l, NULL);
}

int EventDb::sendEmail(Event event) {
	return 0;
}

void EventDb::load() {
	if (config->getConfigurationManager()->has("eventDb")) {
		ObjectContainer* root = config->getConfigurationManager()->getElement("eventDb");
		ObjectContainer* arr1 = root->get("handlers")->container();
		for (int x = 0; x < arr1->size(); x++) {
			ObjectContainer* o = arr1->get(x)->container();

			string handlerKey = o->get("handler")->string();
			if(handlerKey.compare("handler.log") == 0){
				addHandler(o->get("key")->string(), new LogEventHandler());
			}else if(handlerKey.compare("handler.stdout") == 0){
				addHandler(o->get("key")->string(), new StdOutEventHandler());
			}else if(handlerKey.compare("handler.users.disable") == 0){
				addHandler(o->get("key")->string(), new DisableUserEventHandler());
			}else if(handlerKey.compare("handler.users.delete") == 0){
				addHandler(o->get("key")->string(), new DeleteUserEventHandler());
			}else if(handlerKey.compare("handler.firewall.block") == 0){
				addHandler(o->get("key")->string(), new FirewallBlockSourceAddressHandler());
			}else if(handlerKey.compare("handler.email") == 0){
				EmailEventHandler* handler = new EmailEventHandler();	
				if(o->has("email")){
					handler->setEmail(o->get("email")->string());
				}			

				addHandler(o->get("key")->string(), handler);
			}else if(handlerKey.compare("handler.firewall.rewrite") == 0){
				RedirectToUrl* handler = new RedirectToUrl();
				if(o->has("url")){
					handler->setUrl(o->get("url")->string());
				}

				addHandler(o->get("key")->string(), handler);

			}else {
				logger->log(SLogger::ERROR, "Could not find a handler that was stored in configuration");
			}
		}
	}
}

void EventDb::save() {
	ObjectContainer* root = new ObjectContainer(CREL);
	ObjectContainer* a = new ObjectContainer(CARRAY);

	for(multimap<string, EventHandler*>::iterator iter = getHandlers().begin(); iter != getHandlers().end(); iter++){
		string ikey = iter->first;
		EventHandler* ihandler = iter->second;
		string ihandlerkey = ihandler->key();

		ObjectContainer* o = new ObjectContainer(CREL);
		o->put("key", new ObjectWrapper((string) ikey));
		o->put("handler", new ObjectWrapper((string) ihandlerkey));
		if(ihandlerkey.compare("handler.email") == 0){
			o->put("email", new ObjectWrapper((string) ((EmailEventHandler*) ihandler)->getEmail()));
		}

		if(ihandlerkey.compare("handler.firewall.rewrite") == 0){
			o->put("url", new ObjectWrapper((string) ((RedirectToUrl*) ihandler)->getUrl()));
		}

		a->put(new ObjectWrapper(o));
	}

	root->put("handlers", new ObjectWrapper(a));
	config->getConfigurationManager()->setElement("eventDb", root);
	config->getConfigurationManager()->save();
}

void EventDb::list(vector<Event*>& _return) {
	_return = events;
}

void EventDb::addHandler(std::string key, EventHandler* handler) {
	handlers.insert(pair<string, EventHandler*>(key, handler));
}

void EventDb::removeHandler(std::string key, EventHandler* handler) {
	multimap<std::string, EventHandler*>::iterator iter;
	for (iter=handlers.find(key); iter!=handlers.end(); ++iter){
		if((*iter).second == handler){
			handlers.erase(iter);
			break;
		}
	}
}

void EventDb::clearHandlers(){
	handlers.clear();
}
