/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

#include "Database/TrafficAnalyticsDb.h"
#include "Utils/IP4Addr.h"
#include "Utils/TimeUtils.h"
#include "Utils/TimeWrapper.h"

#define HASH_MAX 10240000

int TrafficAnalyticsDb::prepareStatements() {
	logger->log(SLogger::DEBUG, "Loading prepared statements");

	int dbStatus;
	const char *dbEndCompile;
	string message;

	for (map<sqlite3_stmt**, string>::iterator iter = PREP_STATEMENTS.begin();
			iter != PREP_STATEMENTS.end();
			++iter) {

		logger->log(SLogger::DEBUG, "Loading statement " + iter->second);
		dbStatus = sqlite3_prepare_v2(this->dbConn,
				iter->second.c_str(),
				-1, iter->first, &dbEndCompile);

		if (dbStatus != SQLITE_OK) {
			throw new DatabaseException(sqlite3_errmsg(this->connection()),
					iter->second.c_str());
		}
	}

	logger->log(SLogger::DEBUG, "Finished loading prepared statements");
	return dbStatus;
}

void TrafficAnalyticsDb::initDb() {
	logger->log(SLogger::INFO, "Starting TrafficAnalytics Database");

	int dbStatus;
	string message;
	string workStr;
	char*  dbErrorMsg;

	createTables();
	createPrepStatements();
	try {
		dbStatus = sqlite3_open_v2(dbName.c_str(), &this->dbConn,
				SQLITE_OPEN_READWRITE, NULL);


		if (dbStatus == SQLITE_OK) {
			// Existing DB so clean it up.  Prep. statement not needed.
			dbStatus = sqlite3_exec(this->dbConn, "vacuum;analyze;",NULL, NULL,&dbErrorMsg);
			dbStatus = sqlite3_exec(this->dbConn, "PRAGMA synchronous = OFF",NULL, NULL,&dbErrorMsg);
			dbStatus = sqlite3_exec(this->dbConn, "PRAGMA journal_mode = MEMORY",NULL, NULL,&dbErrorMsg);

			if (dbErrorMsg != NULL) { // Error but not critical so carry on.
				message = (const char*) dbErrorMsg;
				logger->log(SLogger::ERROR,
						"TrafficAnalyticsDb::initDb", message);
				sqlite3_free((void*) dbErrorMsg);
			}
		}
		else
		{ // Database does not exist.  Create a new one
			dbStatus = sqlite3_open_v2(dbName.c_str(),
					&this->dbConn, SQLITE_OPEN_READWRITE |
					SQLITE_OPEN_CREATE, NULL);

			if (dbStatus != SQLITE_OK) {
				throw new DatabaseException("Could not create database", "");
			}

			sqlite3_exec(this->dbConn, "PRAGMA synchronous = OFF",NULL, NULL,&dbErrorMsg);
			sqlite3_exec(this->dbConn, "PRAGMA journal_mode = MEMORY",NULL, NULL,&dbErrorMsg);

			for (unsigned int x = 0; x < TABLES.size(); x++) {
				execSQL(TABLES[x]);
			}

		}
		dbStatus = prepareStatements();
	}	catch (exception& except) {
		message = "Exception opening/creating Bandwidth DB : ";
		message.append(except.what());
		logger->log(SLogger::ERROR,
				"BandwidthDb::BandwidthDb", message);
		this->dbConn = NULL;
	}
}

void TrafficAnalyticsDb::createTables() {
	TABLES.push_back("CREATE TABLE taMaster (date DATE,hw TEXT, address INTEGER, user TEXT, tcp_up, tcp_down, udp_up, udp_down, port INTEGER, website_url TEXT);");
	TABLES.push_back("CREATE TABLE taMaster_day (date DATE, hw TEXT, address INTEGER, user TEXT, tcp_up, tcp_down, udp_up, udp_down, port INTEGER, website_url TEXT);");

	TABLES.push_back("CREATE TABLE taDeviceByHour (device TEXT, date DATE, tcp_up INTEGER, tcp_down INTEGER, udp_up INTEGER, udp_down INTEGER);");
	TABLES.push_back("CREATE TABLE taDateSummary (date DATE, tcp_up, tcp_down, udp_up, udp_down);");

	TABLES.push_back("CREATE TABLE taDeviceByHour_day (device TEXT, date DATE, tcp_up INTEGER, tcp_down INTEGER, udp_up INTEGER, udp_down INTEGER);");
	TABLES.push_back("CREATE TABLE taDateSummary_day (date DATE, tcp_up, tcp_down, udp_up, udp_down);");

	TABLES.push_back("CREATE INDEX taDeviceByHourIndex ON taDeviceByHour (device, date);");
	TABLES.push_back("CREATE INDEX taDateSummaryIndex ON taDateSummary (date);");

	TABLES.push_back("CREATE INDEX taDeviceByHour_dayIndex ON taDeviceByHour_day (device, date);");
	TABLES.push_back("CREATE INDEX taDateSummary_dayIndex ON taDateSummary_day (date);");

	//taMaster indexes 
	TABLES.push_back("create index taMaster_InsertSelectIndex on taMaster(date, address, user, port, website_url, hw);");
	TABLES.push_back("create index taMaster_IndexDate on taMaster(date);");
	TABLES.push_back("create index taMaster_IndexDateAddress on taMaster(date, address, hw);");
	TABLES.push_back("create index taMaster_IndexDateUser on taMaster(date, user);");
        TABLES.push_back("create index taMaster_IndexDateAddressPort on taMaster(date, address, port);");
        TABLES.push_back("create index taMaster_IndexDateUserPort on taMaster(date, user, port);");
	TABLES.push_back("create index taMaster_WebsiteUser on taMaster(date, user, website_url);");
	TABLES.push_back("create index taMaster_WebsiteAddress on taMaster(date, address, website_url);");

	//taMaster_day indexes:
	TABLES.push_back("create index taMaster_day_IndexDate on taMaster_day(date);");
        TABLES.push_back("create index taMaster_day_IndexDateAddress on taMaster_day(date, address, hw);");
        TABLES.push_back("create index taMaster_day_IndexDateDate on taMaster_day(date, user);");
        TABLES.push_back("create index taMaster_day_IndexDateAddressPort on taMaster_day(date, address, port);");
        TABLES.push_back("create index taMaster_day_IndexDateDatePort on taMaster_day(date, user, port);");
        TABLES.push_back("create index taMaster_day_WebsiteAddress on taMaster_day(date, address, website_url);");
        TABLES.push_back("create index taMaster_day_WebsiteUser on taMaster_day(date, user, website_url);");
        TABLES.push_back("create index taMaster_day_InsertSelectIndex on taMaster_day(date, address, user, port, website_url, hw);");
}

void TrafficAnalyticsDb::createPrepStatements() {
}

void TrafficAnalyticsDb::deleteUsingDate(string tableName, string date) {
	Query* query = getQuery();
	stringstream ss; ss << "delete from " << tableName << " where date < @targetDate;";
	query->addQuery(ss.str());
	query->addString(1, date);
	query->execute();
	delete query;	
}

std::string TrafficAnalyticsDb::reduceurl(std::string input){
	string r = "([a-z,A-Z]+)\\.(com\\.au|co\\.nz|com\\.uk|net\\.nz|co\\.uk|net\\.au|net\\.cn|ac\\.nz|org\\.nz|org|net|com|ca|jp|co|info|de|edu|tv|us)";	
	static boost::regex getRequest(r);
	std::string packetUrl = "";

	// This searches a large region. Is it nessesary? NOPE, lets optimise it at some point
	boost::smatch what;
	if (boost::regex_search(input, what, getRequest)) {
		stringstream ss;
		ss << what[1] << "." << what[2];
		return ss.str(); 
	}

	return input;
}

void TrafficAnalyticsDb::add(taInputSlot slot) {
	if(slot.upload + slot.download < getByteThreshold()){
		return;
	}

	slot.httpHost = reduceurl(slot.httpHost);

	long psuedehash = slot.sourceIp + slot.destPort + 
		StringUtils::strUniqHash(slot.user, slot.user.size()) + 
		StringUtils::strUniqHash(slot.httpHost, slot.httpHost.size()) + 
		StringUtils::strUniqHash(slot.sourceHw, slot.sourceHw.size());	

	list<taInputSlot>& matches = flushCache[psuedehash % HASH_MAX];
	for(list<taInputSlot>::iterator iter = matches.begin();
			iter != matches.end();
			iter++){

		//Exact comparison:
		taInputSlot& target = (*iter);
		if(target.sourceIp == slot.sourceIp &&
				target.destPort == slot.destPort &&
				target.user.compare(slot.user) == 0 &&
				target.httpHost.compare(slot.httpHost) == 0 &&
				target.sourceHw.compare(slot.sourceHw) == 0){

			target.upload += slot.upload;
			target.download += slot.download;
			reductionCounter++;
			return;
		}
	}

	matches.push_back(slot);
}

void TrafficAnalyticsDb::flush(){
	reductionCounter = 0;
	for(map<long, list<taInputSlot> >::iterator iter = flushCache.begin();
			iter != flushCache.end();
			iter++){

		list<taInputSlot>& targets = iter->second;
		for(list<taInputSlot>::iterator titer = targets.begin();
				titer != targets.end();
				titer++){

			taInputSlot slot = (*titer);
			int tcpUp = 0, tcpDown = 0, udpUp = 0, udpDown = 0;
			if (slot.protocol == 0) {
				tcpUp += slot.upload;
				tcpDown += slot.download;
			} else if (slot.protocol == 1) {
				udpUp += slot.upload;
				udpDown += slot.download;
			}

			updateTaMasterTable(tcpUp, tcpDown, udpUp, udpDown, slot.sourceIp, slot.time, slot.user, slot.destPort, slot.httpHost, slot.sourceHw);
			updateTaMasterDayTable(tcpUp, tcpDown, udpUp, udpDown, slot.sourceIp, slot.time, slot.user, slot.destPort, slot.httpHost, slot.sourceHw);

			updateDeviceByHour(tcpUp, tcpDown, udpUp, udpDown, slot.outputDev, slot.time);
			updateDeviceByHour(tcpDown, tcpUp,udpDown, udpUp, slot.inputDev, slot.time);

			updateDateSummary(tcpUp, tcpDown, udpUp, udpDown, slot.time);
		}
	}

	flushCache.clear();
}

void TrafficAnalyticsDb::addPostHook() {
	logger->log(SLogger::INFO, "Updating TrafficAnalytics post aggregated tables");

	updateDeviceByDay();
	updateDateSummary();

	//cleanup everything:
	string targetDate;
	TimeUtils::convertToString(time(NULL) - getRetensionPeriod(), "%Y-%m-%d", targetDate);

	deleteUsingDate("taDeviceByHour", targetDate);
	deleteUsingDate("taMaster", targetDate);
	deleteUsingDate("taDateSummary", targetDate);

	logger->log(SLogger::INFO, "Finished updating TrafficAnalytics post aggregated tables");

	if(insertSinceLastVacume > 50){
	        logger->log(SLogger::INFO, "Running vacume and analyse");
		vacume();		
	        logger->log(SLogger::INFO, "Finished running vacume and analyse");
		insertSinceLastVacume = 0;
	}
}

void TrafficAnalyticsDb::getTopLocalAddresses(vector<taSummaryUnit>& _return,Time startDate, Time stopDate, int limit) {
	string sql;
	if (stopDate.timestamp() - startDate.timestamp() > daySwitchPeriod)     {
		sql = "SELECT address, hw, sum(tcp_up + tcp_down + udp_up + udp_down) AS x, sum(tcp_up), sum(tcp_down), sum(udp_up), sum(udp_down) FROM taMaster_day WHERE date BETWEEN @startDate AND @stopDate GROUP BY address, hw ORDER BY x DESC LIMIT @limit;";
	} else {
		sql = "SELECT address, hw, sum(tcp_up + tcp_down + udp_up + udp_down) AS x, sum(tcp_up), sum(tcp_down), sum(udp_up), sum(udp_down) FROM taMaster WHERE date BETWEEN @startDate AND @stopDate GROUP BY address, hw ORDER BY x DESC LIMIT @limit;";
	}

	try{
		Query* query = getQuery(); 
		query->addQuery(sql);
		query->addString(1, startDate.midnight());
		query->addString(2, stopDate.last());
		query->addNumber(3, limit);			

		while (query->next()) {
			taSummaryUnit unit;
			unit.sourceIp = query->longResult(0);
			unit.hw = query->stringResult(1);
			unit.tcpUp       += query->longResult(3);
			unit.tcpDown     += query->longResult(4);
			unit.udpUp       += query->longResult(5);
			unit.udpDown     += query->longResult(6);
			_return.push_back(unit);
		}

		delete query;
	}catch(DatabaseException* e){
		logger->log(SLogger::ERROR, "TrafficAnalyticsDb::getTopUsers", e->message());
		return;
	}
}

void TrafficAnalyticsDb::getTopUsers(vector<taSummaryUnit>& _return, Time startDate, Time stopDate, int limit) {
	string sql;
	if (stopDate.timestamp() - startDate.timestamp() > daySwitchPeriod)     {
		sql = "SELECT user, sum(tcp_up + tcp_down + udp_up + udp_down) AS x, sum(tcp_up), sum(tcp_down), sum(udp_up), sum(udp_down) FROM taMaster_day WHERE date BETWEEN @startDate AND @stopDate GROUP BY user ORDER BY x DESC LIMIT @limit;";
	}
	else {
		sql = "SELECT user, sum(tcp_up + tcp_down + udp_up + udp_down) AS x, sum(tcp_up), sum(tcp_down), sum(udp_up), sum(udp_down) FROM taMaster WHERE date BETWEEN @startDate AND @stopDate GROUP BY user ORDER BY x DESC LIMIT @limit;";
	}

	try {
		Query* query = getQuery(); 
		query->addQuery(sql);
		query->addString(1, startDate.midnight());
		query->addString(2, stopDate.last());
		query->addNumber(3, limit);	

		while (query->next()) {
			taSummaryUnit unit;
			unit.username= query->stringResult(0);
			unit.tcpUp       += query->longResult(2);
			unit.tcpDown     += query->longResult(3);
			unit.udpUp       += query->longResult(4);
			unit.udpDown     += query->longResult(5);
			_return.push_back(unit);
		}
		delete query;
	}catch(DatabaseException* e){
		logger->log(SLogger::ERROR, "TrafficAnalyticsDb::getTopUsers", e->message());
		return;
	}
}

void TrafficAnalyticsDb::dump(std::vector<taSummaryUnit>& _return , Filter filter){
	stringstream ss;
	ss << "SELECT sum(tcp_up), sum(tcp_down), sum(udp_up), sum(udp_down), date, port, website_url, hw, user, address ";
	ss << "FROM taMaster_day WHERE date BETWEEN @startDate AND @endDate group by date, port, website_url, user, hw, address";

	try {
		Query* query = getQuery();
		query->addQuery(ss.str());
		query->addString(1, filter.startTime.day());
		query->addString(2, filter.endTime.last());

		while (query->next()) {
			taSummaryUnit unit;
			unit.tcpUp       += query->longResult(0);
			unit.tcpDown     += query->longResult(1);
			unit.udpUp       += query->longResult(2);
			unit.udpDown     += query->longResult(3);
			unit.displayTime += query->stringResult(4);
			unit.port 	 =  query->longResult(5);
			unit.httpHost	 +=  query->stringResult(6);
			unit.hw		 +=  query->stringResult(7);
			unit.username  +=  query->stringResult(8);
			unit.sourceIp	 =  query->longResult(9);			

			_return.push_back(unit);
		}
		delete query;
	}catch(DatabaseException* e){
		logger->log(SLogger::ERROR, "TrafficAnalyticsDb::getTopUsers", e->message());
	}
}

void TrafficAnalyticsDb::getTransfer(std::vector<taSummaryUnit>& _return, Filter filter){
	stringstream sql;
	if (filter.endTime.timestamp() - filter.startTime.timestamp() > daySwitchPeriod) {
		sql << "SELECT sum(tcp_up), sum(tcp_down), sum(udp_up), sum(udp_down), date ";
		sql << " FROM taMaster_day WHERE date BETWEEN @startDate AND @endDate ";
		if(filter.sourceIp_set){
			sql << " AND address = @address ";
		}else if(filter.hw_set){
			sql << " AND hw = @hw";
		}else if(filter.user_set){
			sql << " AND user = @user";
		}

		sql << " GROUP by date; ";
	}
	else {
		sql << "SELECT sum(tcp_up), sum(tcp_down), sum(udp_up), sum(udp_down), date ";
		sql << "FROM taMaster WHERE date BETWEEN @startDate AND @endDate "; 

		if(filter.sourceIp_set){
			sql << " AND address = @address ";
		}else if(filter.hw_set){
			sql << " AND hw = @hw";
		}else if(filter.user_set){
			sql << " AND user = @user";
		}

		sql << " GROUP by date; ";
	}

	try {
		Query* query = getQuery();
		query->addQuery(sql.str());
		query->addString(1, filter.startTime.midnight());
		query->addString(2, filter.endTime.last());

		if(filter.sourceIp_set){
			query->addNumber(3, filter.getSourceIp());
		}else if(filter.hw_set){
			query->addString(3, filter.getHw());	
		}else if(filter.user_set){
			query->addString(3, filter.getUser());
		}

		while (query->next()) {
			taSummaryUnit unit;
			unit.tcpUp       += query->longResult(0);
			unit.tcpDown     += query->longResult(1);
			unit.udpUp       += query->longResult(2);
			unit.udpDown     += query->longResult(3);
			unit.displayTime += query->stringResult(4);

			_return.push_back(unit);
		}
		delete query;
	}catch(DatabaseException* e){
		logger->log(SLogger::ERROR, "TrafficAnalyticsDb::getTopUsers", e->message());
	}
}

void TrafficAnalyticsDb::getDeviceSummaryByHour(std::vector<taSummaryUnit>& _return, std::string device, Time startDate, Time endDate) {
	string sql;
	if (endDate.timestamp() - startDate.timestamp() > daySwitchPeriod) {
		sql = "SELECT tcp_up, tcp_down, udp_up, udp_down, date FROM taDeviceByHour_day WHERE device like @device AND date BETWEEN @startDate AND @endDate;";
	}
	else {
		sql = "SELECT tcp_up, tcp_down, udp_up, udp_down, date FROM taDeviceByHour WHERE device like @device AND date BETWEEN @startDate AND @endDate;";
	}

	try {
		Query* query = getQuery();
		query->addQuery(sql);
		query->addString(1, device);
		query->addString(2, startDate.midnight());
		query->addString(3, endDate.last());

		while (query->next()) {
			taSummaryUnit unit;
			unit.tcpUp       += query->longResult(0);
			unit.tcpDown     += query->longResult(1);
			unit.udpUp       += query->longResult(2);
			unit.udpDown     += query->longResult(3);
			unit.displayTime += query->stringResult(4);

			_return.push_back(unit);
		}
		delete query;
	}catch(DatabaseException* e){
		logger->log(SLogger::ERROR, "TrafficAnalyticsDb::getTopUsers", e->message());
	}
}

void TrafficAnalyticsDb::getWebsiteTransfer(std::vector<taWebsiteSummaryUnit>& _return, Filter filter){
	const string sqlSelectClause =
		"SELECT sum(tcp_up), sum(tcp_down), sum(udp_up), sum(udp_down), website_url";
	const string sqlWhereClause =
		" WHERE website_url != \"\" AND date BETWEEN @startDate AND @endDate";
	const string sqlGroupOrderByClause =
		" GROUP by website_url order by (sum(tcp_up) + sum(tcp_down) + sum(udp_up) + sum(udp_down)) desc";

	stringstream sql;
	int          argIndex;


	sql << sqlSelectClause;

	if (filter.endTime.timestamp() - filter.startTime.timestamp() > daySwitchPeriod) {
		sql << " FROM taMaster_day";
	}
	else {
		sql << " FROM taMaster";
	}

	sql << sqlWhereClause;

	if(filter.sourceIp_set){
		sql << " AND address = @address ";
	}else if(filter.hw_set){
		sql << " AND hw = @hw";
	}else if(filter.user_set){
		sql << " AND user = @user";
	}

	sql << sqlGroupOrderByClause;

	if (filter.getLimit() >= 0) {
		sql << " limit @limit";
	}

	sql << ";";

	try {
		Query* query = getQuery();
		query->addQuery(sql.str());
		query->addString(1, filter.startTime.midnight());
		query->addString(2, filter.endTime.last());
		argIndex = 2;

		if(filter.sourceIp_set){
			argIndex++;
			query->addNumber(argIndex, filter.getSourceIp());
		}else if(filter.hw_set){
			argIndex++;
			query->addString(argIndex, filter.getHw());
		}else if(filter.user_set){
			argIndex++;
			query->addString(argIndex, filter.getUser());
		}

		if (filter.getLimit() >= 0) {
			argIndex++;
			query->addNumber(argIndex, filter.getLimit());
		}

		while (query->next()) {
			taWebsiteSummaryUnit unit;
			unit.tcpUp       += query->longResult(0);
			unit.tcpDown     += query->longResult(1);
			unit.udpUp       += query->longResult(2);
			unit.udpDown     += query->longResult(3);
			unit.httpHost     = query->stringResult(4);
			_return.push_back(unit);
		}
		delete query;
	}catch(DatabaseException* e){
		logger->log(SLogger::ERROR, "TrafficAnalyticsDb::getTopUsers", e->message());
	}
}

void TrafficAnalyticsDb::getPortTransfer(std::vector<taSummaryUnit>& _return, Filter filter){
	stringstream sql;
	if (filter.endTime.timestamp() - filter.startTime.timestamp() > daySwitchPeriod) {
		sql << "SELECT sum(tcp_up), sum(tcp_down), sum(udp_up), sum(udp_down), date, port FROM taMaster_day WHERE date BETWEEN @startDate AND @endDate ";
		if(filter.sourceIp_set){
			sql << " AND address = @address ";
		}else if(filter.hw_set){
			sql << " AND hw = @hw";
		}else if(filter.user_set){
			sql << " AND user = @user";
		}
		sql << " GROUP by date, port;";
	}
	else {
		sql << "SELECT sum(tcp_up), sum(tcp_down), sum(udp_up), sum(udp_down), date, port FROM taMaster WHERE date BETWEEN @startDate AND @endDate ";
		if(filter.sourceIp_set){
			sql << " AND address = @address ";
		}else if(filter.hw_set){
			sql << " AND hw = @hw";
		}else if(filter.user_set){
			sql << " AND user = @user";
		}
		sql << " GROUP by date, port;";
	}

	try {
		Query* query = getQuery();
		query->addQuery(sql.str());
		query->addString(1, filter.startTime.midnight());
		query->addString(2, filter.endTime.last());

		if(filter.sourceIp_set){
			query->addNumber(3, filter.getSourceIp());
		}else if(filter.hw_set){
			query->addString(3, filter.getHw());
		}else if(filter.user_set){
			query->addString(3, filter.getUser());
		}

		while (query->next()) {
			taSummaryUnit unit;
			unit.tcpUp       += query->longResult(0);
			unit.tcpDown     += query->longResult(1);
			unit.udpUp       += query->longResult(2);
			unit.udpDown     += query->longResult(3);
			unit.displayTime += query->stringResult(4);
			unit.port 	 = query->longResult(5);
			_return.push_back(unit);
		}
		delete query;
	}catch(DatabaseException* e){
		logger->log(SLogger::ERROR, "TrafficAnalyticsDb::getTopUsers", e->message());
	}
}

void TrafficAnalyticsDb::getDateSummary(std::vector<taSummaryUnit>& _return, Time startDate, Time endDate) {
	string sql;
	if (endDate.timestamp() - startDate.timestamp() > daySwitchPeriod) {
		sql = "SELECT tcp_up, tcp_down, udp_up, udp_down, date FROM taDateSummary_day WHERE date BETWEEN @startDate AND @endDate;";
	}
	else {
		sql = "SELECT tcp_up, tcp_down, udp_up, udp_down, date FROM taDateSummary WHERE date BETWEEN @startDate AND @endDate;";
	}

	try {
		Query* query = getQuery();
		query->addQuery(sql);
		query->addString(1, startDate.midnight());
		query->addString(2, endDate.last());

		while (query->next()) {
			taSummaryUnit unit;
			unit.tcpUp       += query->longResult(0);
			unit.tcpDown     += query->longResult(1);
			unit.udpUp       += query->longResult(2);
			unit.udpDown     += query->longResult(3);
			unit.displayTime += query->stringResult(4);
			unit.port = query->longResult(5);
			_return.push_back(unit);
		}
		delete query;

	}catch(DatabaseException* e){
		logger->log(SLogger::ERROR, "TrafficAnalyticsDb::getTopUsers", e->message());
	}
}

void TrafficAnalyticsDb::updateDeviceByHour(int tcpUp, int tcpDown, int udpUp, int udpDown, string device, int time) {
	string targetDate = Time(time).hour();
	Query* squery = getQuery();
	squery->addQuery("select tcp_up, tcp_down, udp_up, udp_down FROM taDeviceByHour WHERE date = @date AND device = @device;");
	squery->addString(2, device);
	squery->addString(1, targetDate);

	if (squery->next()) { // Found user record.
		Query* uquery = getQuery();
		uquery->addQuery("UPDATE taDeviceByHour SET tcp_up = @tcpUp, tcp_down = @tcpDown, udp_up = @udpUp, udp_down = @udpDown where device = @device and date = @date;");

		long long oldTcpUp = squery->longResult(0);
		long long oldTcpDown = squery->longResult(1);
		long long oldUdpUp = squery->longResult(2);
		long long oldUdpDown = squery->longResult(3);

		uquery->addNumber(1, oldTcpUp + tcpUp);
		uquery->addNumber(2, oldTcpDown + tcpDown);
		uquery->addNumber(3, oldUdpUp + udpUp);
		uquery->addNumber(4, oldUdpDown + udpDown);
		uquery->addString(5, device);
		uquery->addString(6, targetDate);
		uquery->execute();
		delete uquery;	
	} else {
		Query* iquery = getQuery();
		iquery->addQuery("insert into taDeviceByHour values(@device, datetime(@date), @tcp_up, @tcp_down, @udp_up,@udp_down);");
		iquery->addString(1, device);
		iquery->addString(2, targetDate);
		iquery->addNumber(3, tcpUp);
		iquery->addNumber(4, tcpDown);
		iquery->addNumber(5, udpUp);
		iquery->addNumber(6, udpDown);
		iquery->execute();
		delete iquery;
	}

	delete squery;
}

void TrafficAnalyticsDb::updateTaMasterTable(int tcpUp, int tcpDown, int udpUp, int udpDown, unsigned int targetAddress, int time, std::string user, int port, std::string websiteUrl, std::string hw) {
	if(!IP4Addr::isLocal(targetAddress) || port > IP4Addr::CLIENT_PORT_MIN || daySwitchPeriod == 0){
		return;
	}

	string targetDate = Time(time).hour();

	Query* squery = getQuery();
	squery->addQuery("select tcp_up, tcp_down, udp_up, udp_down, hw, address, user, date from taMaster where address = @address and user = @user and date = @date and port = @port and website_url = @websiteUrl and hw = @hw;");
	squery->addNumber(1, targetAddress);
	squery->addNumber(4, port);
	squery->addString(3, targetDate);
	squery->addString(2, user);
	squery->addString(5, websiteUrl);
	squery->addString(6, hw);

	if (squery->next()) { // Found user record.
		Query* uquery = getQuery();
		uquery->addQuery("UPDATE taMaster SET tcp_up = tcp_up + @tcpUp, tcp_down = tcp_down + @tcpDown, udp_up = udp_up + @udpUp, udp_down = udp_down + @udpDown WHERE address = @address AND date = @date AND user = @user and port = @port and website_url = @websiteUrl and hw = @hw;");
		uquery->addNumber(1, tcpUp);
		uquery->addNumber(2, tcpDown);
		uquery->addNumber(3, udpUp);
		uquery->addNumber(4, udpDown);
		uquery->addNumber(5, targetAddress);
		uquery->addString(6, targetDate);
		uquery->addString(7, user);
		uquery->addNumber(8, port);		
		uquery->addString(9, websiteUrl);		
		uquery->addString(10, hw);
		uquery->execute();
		delete uquery;	
	} else {
		Query* iquery = getQuery();
		iquery->addQuery("INSERT into taMaster VALUES(datetime(@date), @hw, @address, @user, @tcpUp, @tcpDown, @udpUp, @udpDown, @port, @websiteUrl);");
		iquery->addString(1, targetDate);
		iquery->addString(2, hw);
		iquery->addNumber(3, targetAddress);
		iquery->addString(4, user);
		iquery->addNumber(5, tcpUp);
		iquery->addNumber(6, tcpDown);
		iquery->addNumber(7, udpUp);
		iquery->addNumber(8, udpDown); 
		iquery->addNumber(9, port); 
		iquery->addString(10, websiteUrl); 
		iquery->execute();
		delete iquery;
	}

	delete squery;

}

void TrafficAnalyticsDb::updateTaMasterDayTable(int tcpUp, int tcpDown, int udpUp, int udpDown, unsigned int targetAddress, int time, std::string user, int port, std::string websiteUrl, std::string hw) {
        if(!IP4Addr::isLocal(targetAddress) || port > IP4Addr::CLIENT_PORT_MIN){
                return;
        }

	//PERFORM UPDATE ON taMaster_day
	string targetDateDay = Time(time).day();

	Query* squeryDay = getQuery();
	squeryDay->addQuery("select tcp_up, tcp_down, udp_up, udp_down, hw, address, user, date from taMaster_day where address = @address and user = @user and date = @date and port = @port and website_url = @websiteUrl and hw = @hw;");
	squeryDay->addNumber(1, targetAddress);
	squeryDay->addNumber(4, port);
	squeryDay->addString(3, targetDateDay);
	squeryDay->addString(2, user);
	squeryDay->addString(5, websiteUrl);
	squeryDay->addString(6, hw);

	if (squeryDay->next()) { // Found user record.
		Query* uquery = getQuery();
		uquery->addQuery("UPDATE taMaster_day SET tcp_up = tcp_up + @tcpUp, tcp_down = tcp_down + @tcpDown, udp_up = udp_up + @udpUp, udp_down = udp_down + @udpDown WHERE address = @address AND date = @date AND user = @user and port = @port and website_url = @websiteUrl and hw = @hw;");
		uquery->addNumber(1, tcpUp);
		uquery->addNumber(2, tcpDown);
		uquery->addNumber(3, udpUp);
		uquery->addNumber(4, udpDown);
		uquery->addNumber(5, targetAddress);
		uquery->addString(6, targetDateDay);
		uquery->addString(7, user);
		uquery->addNumber(8, port);		
		uquery->addString(9, websiteUrl);		
		uquery->addString(10, hw);
		uquery->execute();
		delete uquery;	
	} else {
		Query* iquery = getQuery();
		iquery->addQuery("INSERT into taMaster_day VALUES(datetime(@date), @hw, @address, @user, @tcpUp, @tcpDown, @udpUp, @udpDown, @port, @websiteUrl);");
		iquery->addString(1, targetDateDay);
		iquery->addString(2, hw);
		iquery->addNumber(3, targetAddress);
		iquery->addString(4, user);
		iquery->addNumber(5, tcpUp);
		iquery->addNumber(6, tcpDown);
		iquery->addNumber(7, udpUp);
		iquery->addNumber(8, udpDown); 
		iquery->addNumber(9, port); 
		iquery->addString(10, websiteUrl); 
		iquery->execute();
		delete iquery;
	}

	delete squeryDay;
}

void TrafficAnalyticsDb::updateDateSummary(int tcpUp, int tcpDown, int udpUp, int udpDown, int time){
	string targetDate = Time(time).hour();

	Query* squery = getQuery();
	squery->addQuery("select tcp_up, tcp_down, udp_up, udp_down FROM taDateSummary where date = @date;");
	squery->addString(1, targetDate);

	if (squery->next()) { // Found user record.
		Query* uquery = getQuery();
		uquery->addQuery("update taDateSummary set tcp_up = @tcpUp, tcp_down = @tcpDown, udp_up = @udpUP, udp_down = @udpDown where date = @date;");
		uquery->addNumber(1, squery->longResult(0) + tcpUp);
		uquery->addNumber(2, squery->longResult(1) + tcpDown);
		uquery->addNumber(3, squery->longResult(2) + udpUp);
		uquery->addNumber(4, squery->longResult(3) + udpDown);
		uquery->addString(5, targetDate);	
		uquery->execute();
		delete uquery;	
	} else {
		Query* iquery = getQuery();
		iquery->addQuery("insert into taDateSummary values(datetime(@date), @tcp_up, @tcp_down, @udp_up, @udp_down);");	
		iquery->addString(1, targetDate);
		iquery->addNumber(2, tcpUp);
		iquery->addNumber(3, tcpDown);
		iquery->addNumber(4, udpUp);
		iquery->addNumber(5, udpDown);
		iquery->execute();
		delete iquery;
	}
	delete squery;
}


void TrafficAnalyticsDb::updateDeviceByDay() {
	string targetDate = Time(time(NULL)).day();
	Query* dquery = getQuery();
	dquery->addQuery("delete from taDeviceByHour_day where date = @targetDate;");
	dquery->addString(1, targetDate);
	dquery->execute();
	delete dquery;

	Query* query = getQuery();
	query->addQuery("insert into taDeviceByHour_day (device, date, tcp_up, tcp_down, udp_up, udp_down) select device, strftime('%Y-%m-%d', date), sum(tcp_up), sum(tcp_down), sum(udp_up), sum(udp_down) from taDeviceByHour where strftime('%Y-%m-%d', date) = @targetDate group by device;");
	query->addString(1, targetDate);
	query->execute();
	delete query;
}

void TrafficAnalyticsDb::updateDateSummary() {
	string targetDate = Time(time(NULL)).day();
	Query* dquery = getQuery();
	dquery->addQuery("delete from taDateSummary_day where date = @targetDate;");
	dquery->addString(1, targetDate);
	dquery->execute();
	delete dquery;

	Query* query = getQuery();
	query->addQuery("insert into taDateSummary_day (date, tcp_up, tcp_down, udp_up, udp_down) select strftime('%Y-%m-%d', date), sum(tcp_up), sum(tcp_down), sum(udp_up), sum(udp_down) from taDateSummary where strftime('%Y-%m-%d', date) = @targetDate;");
	query->addString(1, targetDate);
	query->execute();
	delete query;

}

void TrafficAnalyticsDb::getWebsiteByAddress(std::vector<taWebsiteSummaryUnit>& _return, std::string httpHost,Time startDate, Time endDate) {
	string sql;
	if (endDate.timestamp() - startDate.timestamp() > daySwitchPeriod) {
		sql = "SELECT address, sum(tcp_up), sum(tcp_down), sum(udp_up), sum(udp_down), hw, user FROM taMaster_day WHERE length(website_url) > 0 and website_url = @httpHost AND date BETWEEN @startDate AND @endDate GROUP BY address, hw,user order by sum(tcp_up) + sum(tcp_down) + sum(udp_up) + sum(udp_down) desc;";
	}
	else {
		sql = "SELECT address, sum(tcp_up), sum(tcp_down), sum(udp_up), sum(udp_down), hw,user FROM taMaster WHERE length(website_url) > 0 and website_url= @httpHost AND date BETWEEN @startDate AND @endDate GROUP BY address, hw, user order by sum(tcp_up) + sum(tcp_down) + sum(udp_up) + sum(udp_down) desc;"; 
	}

	try {
		Query* query = getQuery();
		query->addQuery(sql);
		query->addString(1, httpHost);
		query->addString(2, startDate.midnight());
		query->addString(3, endDate.last());

		while (query->next()) {
			taWebsiteSummaryUnit unit;
			unit.ip      = query->longResult(0);
			unit.tcpUp   = query->longResult(1);
			unit.tcpDown = query->longResult(2);
			unit.udpUp   = query->longResult(3);
			unit.udpDown = query->longResult(4);
			unit.hw      = query->stringResult(5);
			unit.username = query->stringResult(6);

			_return.push_back(unit);
		}
		delete query;
	}catch(DatabaseException* e){
		logger->log(SLogger::ERROR, "TrafficAnalyticsDb::getTopUsers", e->message());
	}
}

std::string TrafficAnalyticsDb::getTopWebsite(Filter filter){
	stringstream sql;
        if (filter.endTime.timestamp() - filter.startTime.timestamp() > daySwitchPeriod) {
		sql << "SELECT website_url, sum(tcp_up) + sum(tcp_down) + sum(udp_up) + sum(udp_down) as x FROM taMaster_day WHERE date BETWEEN @startDate AND @endDate and website_url != '' group by website_url order by x desc limit 1";
        }
        else {
		sql << "SELECT website_url, sum(tcp_up) + sum(tcp_down) + sum(udp_up) + sum(udp_down) as x FROM taMaster WHERE date BETWEEN @startDate AND @endDate and website_url != '' group by website_url order by x desc limit 1";
        }

	Query* query = getQuery(); 
	query->addQuery(sql.str());
	query->addString(1, filter.startTime.midnight());
	query->addString(2, filter.endTime.last());

	while (query->next()) {
		std::string website = query->stringResult(0);
		delete query;
		return website;
	}
	return "-";
}

std::string TrafficAnalyticsDb::getTopUser(Filter filter){
        stringstream sql;
        if (filter.endTime.timestamp() - filter.startTime.timestamp() > daySwitchPeriod) {
                sql << "SELECT user, sum(tcp_up) + sum(tcp_down) + sum(udp_up) + sum(udp_down) as x FROM taMaster_day WHERE date BETWEEN @startDate AND @endDate and user != '' group by user order by x desc limit 1";
        }
        else {
                sql << "SELECT user, sum(tcp_up) + sum(tcp_down) + sum(udp_up) + sum(udp_down) as x FROM taMaster WHERE date BETWEEN @startDate AND @endDate and user != '' group by user order by x desc limit 1";
        }

        Query* query = getQuery();
        query->addQuery(sql.str());
        query->addString(1, filter.startTime.midnight());
        query->addString(2, filter.endTime.last());

        while (query->next()) {
                std::string user = query->stringResult(0);
                delete query;
                return user;
        }
	return "-";
}

std::string TrafficAnalyticsDb::getTopIp(Filter filter){
        stringstream sql;
        if (filter.endTime.timestamp() - filter.startTime.timestamp() > daySwitchPeriod) {
                sql << "SELECT address, sum(tcp_up) + sum(tcp_down) + sum(udp_up) + sum(udp_down) as x FROM taMaster_day WHERE date BETWEEN @startDate AND @endDate group by address order by x desc limit 1";
        }
        else {
                sql << "SELECT user, sum(tcp_up) + sum(tcp_down) + sum(udp_up) + sum(udp_down) as x FROM taMaster WHERE date BETWEEN @startDate AND @endDate group by address order by x desc limit 1";
        }

        Query* query = getQuery(); 
        query->addQuery(sql.str());
        query->addString(1, filter.startTime.midnight());
        query->addString(2, filter.endTime.last());

        while (query->next()) {
                std::string user = IP4Addr::ip4AddrToString(query->longResult(0));
                delete query;
                return user;
        }
	return "-";
}

std::string TrafficAnalyticsDb::getTopDevice(Filter filter){
        stringstream sql;
	if (filter.endTime.timestamp() - filter.startTime.timestamp() > daySwitchPeriod) {
                sql << "SELECT hw , sum(tcp_up) + sum(tcp_down) + sum(udp_up) + sum(udp_down) as x FROM taMaster_day WHERE date BETWEEN @startDate AND @endDate and hw != '' group by hw order by x desc limit 1";
	}
	else {                
		sql << "SELECT hw , sum(tcp_up) + sum(tcp_down) + sum(udp_up) + sum(udp_down) as x FROM taMaster WHERE date BETWEEN @startDate AND @endDate and hw != '' group by hw order by x desc limit 1";
	}

	Query* query = getQuery(); 
	query->addQuery(sql.str());
	query->addString(1, filter.startTime.midnight());
	query->addString(2, filter.endTime.last());

	while (query->next()) {
		std::string user = query->stringResult(0);
		delete query;
		return user;
	}

	return "-";
}

