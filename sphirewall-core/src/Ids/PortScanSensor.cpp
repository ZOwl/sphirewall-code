/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;

#include "Ids/Ids.h"
#include "Core/Event.h"
#include "Ids/PortScanSensor.h"
#include "SFwallCore/Packet.h"
#include "Utils/IP4Addr.h"

int PortScanSensor::hash(unsigned int source, unsigned int dest) {
	return (source + dest) % HASH_MAX;
}

PortScanSensor::Hit* PortScanSensor::findMatch(unsigned int source, unsigned int dest) {
	int h = hash(source, dest);

	list<Hit*>& items = hits[h];
	for (list<Hit*>::iterator iter = items.begin(); iter != items.end(); iter++) {
		Hit* hit = *iter;
		if ((hit->source == source) && (hit->dest == dest) && !hit->hasExpired()) {
			return hit;
		} else if ((hit->source == dest) && (hit->dest == source) && !hit->hasExpired()) {
			return hit;
		}

		if (hit->hasExpired()) {
			iter = items.erase(iter);
			delete hit;
		}
	}

	return NULL;
}

void PortScanSensor::handle(SFwallCore::Packet* p) {
	SFwallCore::TcpUdpPacket* packet = dynamic_cast<SFwallCore::TcpUdpPacket*> (p);

	int h = hash(packet->getSrcIp(), packet->getDstIp());
	Hit* hit = findMatch(packet->getSrcIp(), packet->getDstIp());

	if (hit) {
		int targetPort = 0;
		if (packet->getSrcPort() < IP4Addr::CLIENT_PORT_MIN) {
			targetPort = packet->getSrcPort();
		} else if (packet->getDstPort() < IP4Addr::CLIENT_PORT_MIN) {
			targetPort = packet->getDstPort();
		}

		hit->insert(targetPort);
		if (hit->hits() > HIT_THRESHOLD) {
			if (!hit->reported) {
				alert(packet->getSrcIp(), packet->getDstIp());
				hit->reported = true;
			}
		}
	} else {
		hit = new Hit(packet->getSrcIp(), packet->getDstIp(), packet->getDstPort());
		hits[h].push_back(hit);
	}
	return;
}

std::string PortScanSensor::type() {
	return IDS_PORTSCAN;
}

