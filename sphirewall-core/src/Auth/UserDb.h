/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef USERDB_H
#define USERDB_H

#include "Core/Logger.h"
#include "Core/Lockable.h"
#include "Auth/GroupDb.h"

class SessionDb;
class ConfigurationManager;
class GroupRemovedListener;
class User;
class Event;
class EventDb;

class DeleteUserListener {
        public:
                virtual ~DeleteUserListener(){}
                virtual void handleDeleteUser(User* user) = 0;
};

class UserDb : public GroupRemovedListener, public Lockable{
public:

	UserDb(EventDb* eventDb, GroupDb* groupDb, SLogger::LogContext* l, ConfigurationManager* config, SessionDb* sessionDb) :
		Lockable(), eventDb(eventDb), groupDb(groupDb), logger(l), config(config), sessionDb(sessionDb) {
	}
	~UserDb();

	void loadDb();
	void save(bool lockIt = true);
	User* createUser(string username);
	bool delUser(User* user);
	
	void disableUser(User* user);
	void enableUser(User* user);

	bool authenticateUser(string username, string password);

	User* getUser(string username);
	std::vector<string> list();
	long getNextTokenId();
	void persistUserFromExternalSource(User* user);
	void setSessionDb(SessionDb* sessionDbPtr); 
	void registerDeleteListener(DeleteUserListener* target){
		deleteListeners.push_back(target);
	}

	//Listeners:
	void groupRemoved(Group* group);
private:
	EventDb* eventDb;
	GroupDb* groupDb;
	SLogger::LogContext* logger;
	std::string filename;
	std::map<string, User*> users;
	int pamAuth(char* login, char* passwd);
	SessionDb* sessionDb;
	ConfigurationManager* config;
	std::list<DeleteUserListener*> deleteListeners;
};

#endif
