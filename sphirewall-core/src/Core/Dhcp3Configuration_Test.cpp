/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <iostream>
using namespace std;

#include "Core/Dhcp3Configuration.h"
#include "Utils/NetDevice.h"
/*
class FakeDevice : public virtual NetDevice {
	public:
        string getNetMask(){
		return "255.255.255.0";
	}

        string getNetworkAddress(){
		return "192.168.2.0";
	}

	string getInterface(){
		return "eth0";
	}

};

TEST(DhcpConfigurationManager, generateConfig){
	InterfaceManager* mgr = new InterfaceManager();
	NetDevice* dev = new FakeDevice();
	mgr->pushDevice(0, dev);

	Dhcp3ConfigurationManager* manager = new Dhcp3ConfigurationManager(NULL, mgr);
	
	manager->startIpAddress = "10.1.1.10";
	manager->finishIpAddress = "10.1.1.12";
	manager->defaultRouter = "192.168.1.1";
	manager->domainNameService = "8.8.8.8";
	manager->domainName = "sphinix.com";

	manager->addStaticLease("dude", "aa:bb:cc:dd:ee", "10.1.1.1");
	string output = manager->generateConfigurationString();

	string rawConfig;
        rawConfig = "log-facility local0;\n";
        rawConfig +=  "subnet 192.168.2.0 netmask 255.255.255.0\n";
	rawConfig += "{\n";
	rawConfig += "range 10.1.1.10 10.1.1.12;\n";
	rawConfig += "option routers 192.168.1.1;\n";
	rawConfig += "option domain-name-servers 8.8.8.8;\n";
	rawConfig += "option domain-name sphinix.com;\n";
	
	rawConfig += "host dude{\n";
	rawConfig += "hardware ethernet 10.1.1.1;\n";
	rawConfig += "fixed-address aa:bb:cc:dd:ee;\n";
	rawConfig += "}\n";
	rawConfig += "} \n";
		
	EXPECT_TRUE(rawConfig.compare(output) == 0);
}
*/
