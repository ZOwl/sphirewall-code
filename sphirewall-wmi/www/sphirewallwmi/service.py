#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

from flask.globals import request
from api.sphirewall_api import SphirewallClient

def session():
    return request.cookies.get("TOKEN")


def sesson_hostname():
    return request.cookies.get("HOSTNAME")


def session_port():
    return request.cookies.get("PORT")


def get_sphirewallapi():
    return SphirewallClient(hostname=sesson_hostname(), port=int(session_port()), token=session())


def authenticate(username, password, hostname, port):
    client = SphirewallClient(hostname, int(port), username, password)
    return client.get_token()