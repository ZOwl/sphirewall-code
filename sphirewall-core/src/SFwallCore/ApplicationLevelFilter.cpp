/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <list>

#include "SFwallCore/Packet.h"
#include "SFwallCore/Conn.h"
#include "Core/Config.h"
#include "SFwallCore/ApplicationLevelFilter.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include "SFwallCore/Alias.h"
#include "Utils/TimeWrapper.h"
#include "Core/Event.h"

void SFwallCore::HttpUrlApplicationFilter::load(){
	lock();
	if(config){
		ObjectContainer* root;
		if (config->has("webfilter")) {
			root = config->getElement("webfilter");
			ObjectContainer* rules = root->get("rules")->container();
			for(int x = 0; x < rules->size(); x++){
				ObjectContainer* source = rules->get(x)->container();
				FilterCriteria* target = new FilterCriteria();
				target->id = source->get("id")->string();	
				if(source->has("sourceIp") && source->has("sourceMask")){
					target->sourceIp = IP4Addr::stringToIP4Addr(source->get("sourceIp")->string());
					target->sourceMask = IP4Addr::stringToIP4Addr(source->get("sourceMask")->string());
				}	

				if(source->has("groups")){
					ObjectContainer* groups = source->get("groups")->container();
					for(int y = 0; y < groups->size(); y++){
						Group* g = groupDb->getGroup(groups->get(y)->number());
						if(g){
							target->groups.push_back(g);
						}
					}
				}

				if(source->has("lists")){
					ObjectContainer* lists = source->get("lists")->container();
					for(int z = 0; z < lists->size(); z++){		
						target->lists.push_back(((WebsiteListAlias*) aliases->get(lists->get(z)->string())));
					}
				}

				if(source->has("fireEvent")){
					target->fireEvent = source->get("fireEvent")->boolean();
				}

				if(source->has("action")){
					target->action = (ApplicationFilterAction) source->get("action")->number();
				}	

				if(source->has("redirect")){
					target->redirect = source->get("redirect")->boolean();
					target->redirectUrl = source->get("redirectUrl")->string();
				}

				if(source->has("endTime") && source->has("startTime")){
					target->startTime = source->get("startTime")->number();
					target->endTime = source->get("endTime")->number();

					target->mon = source->get("mon")->boolean();
					target->tues= source->get("tues")->boolean();
					target->wed = source->get("wed")->boolean();
					target->thurs = source->get("thurs")->boolean();
					target->fri= source->get("fri")->boolean();
					target->sat= source->get("sat")->boolean();
					target->sun= source->get("sun")->boolean();
				}	

				if(source->has("exclusive")){
					target->exclusive = source->get("exclusive")->boolean();
				}

				if(source->has("sourceAliases")){
					ObjectContainer* sourceAliases = source->get("sourceAliases")->container();
					for(int y = 0; y < sourceAliases->size(); y++){
						target->sourceAlias.push_back(aliases->get(sourceAliases->get(y)->string()));
					}
				}

				if(source->has("enabled")){
					target->enabled  = source->get("enabled")->boolean();
				}

				filters.push_back(target);		
			}
		}
	}
	unlock();
}

void SFwallCore::HttpUrlApplicationFilter::save(){
	if(config){
		ObjectContainer* root = new ObjectContainer(CREL);
		ObjectContainer* rulesArray = new ObjectContainer(CARRAY);

		for(list<FilterCriteria*>::iterator iter = filters.begin(); iter != filters.end(); iter++){
			FilterCriteria* source = (*iter);
			ObjectContainer* target = new ObjectContainer(CREL); 
			target->put("id", new ObjectWrapper((string) source->id));

			if(source->sourceIp != -1){
				target->put("sourceIp", new ObjectWrapper((string) IP4Addr::ip4AddrToString(source->sourceIp)));
				target->put("sourceMask", new ObjectWrapper((string) IP4Addr::ip4AddrToString(source->sourceMask)));
			}			

			ObjectContainer* groups = new ObjectContainer(CARRAY);
			for(list<Group*>::iterator giter = source->groups.begin(); giter != source->groups.end(); giter++){
				Group* targetGroup = (*giter);
				if(targetGroup){
					groups->put(new ObjectWrapper((double) targetGroup->getId()));
				}
			}
			target->put("groups", new ObjectWrapper(groups));

			ObjectContainer* lists = new ObjectContainer(CARRAY);
			for(list<WebsiteListAlias*>::iterator liter = source->lists.begin(); liter != source->lists.end(); liter++){
				WebsiteListAlias* alias = (*liter);
				lists->put(new ObjectWrapper((string) alias->id));
			}
			target->put("lists", new ObjectWrapper(lists));

			ObjectContainer* sourceAliases = new ObjectContainer(CARRAY);
			for(list<Alias*>::iterator siter = source->sourceAlias.begin(); siter != source->sourceAlias.end(); siter++){
				sourceAliases->put(new ObjectWrapper((string) (*siter)->id));
			}
			target->put("sourceAliases", new ObjectWrapper(sourceAliases));

			target->put("fireEvent", new ObjectWrapper((bool) source->fireEvent));
			target->put("action", new ObjectWrapper((double) source->action));
			target->put("redirect", new ObjectWrapper((bool) source->redirect));
			target->put("redirectUrl",new ObjectWrapper((string) source->redirectUrl));

			if(source->startTime != -1 && source->endTime != -1){
				target->put("startTime", new ObjectWrapper((double) source->startTime));
				target->put("endTime", new ObjectWrapper((double) source->endTime));

				target->put("mon", new ObjectWrapper((bool) source->mon));
				target->put("tues", new ObjectWrapper((bool) source->tues));
				target->put("wed", new ObjectWrapper((bool) source->wed));
				target->put("thurs", new ObjectWrapper((bool) source->thurs));
				target->put("fri", new ObjectWrapper((bool) source->fri));
				target->put("sat", new ObjectWrapper((bool) source->sat));
				target->put("sun", new ObjectWrapper((bool) source->sun));
			}

			target->put("exclusive", new ObjectWrapper((bool) source->exclusive));
			target->put("enabled", new ObjectWrapper((bool) source->enabled));
			rulesArray->put(new ObjectWrapper(target));		
		}

		root->put("rules", new ObjectWrapper(rulesArray));
		config->setElement("webfilter", root);
		config->save();
	}
}

void SFwallCore::HttpUrlApplicationFilter::aliasRemoved(Alias* alias){
	lock();
	std::list<FilterCriteria*> rules =getRules();
	for(std::list<FilterCriteria*>::iterator iter = rules.begin();
			iter != rules.end();
			iter++){
		FilterCriteria* filter = (*iter);
		for(list<Alias*>::iterator sourceAliasIter = filter->sourceAlias.begin(); sourceAliasIter != filter->sourceAlias.end();
			sourceAliasIter++){
				
			Alias* target = (*sourceAliasIter);	
			if(target == alias){
				removeRule(filter);
				break;
			}
		}	

		for(list<WebsiteListAlias*>::iterator listIter = filter->lists.begin(); listIter!= filter->lists.end();
				listIter++){

			WebsiteListAlias* target = (*listIter);	
			if(target == alias){
				removeRule(filter);
				break;
			}
		}	
	}
	unlock();
}

bool SFwallCore::HttpUrlApplicationFilter::hasHit(std::string host, SFwallCore::FilterCriteria* crit){	
	bool response = true;
	for(list<WebsiteListAlias*>::iterator liter = crit->lists.begin(); liter != crit->lists.end(); liter++){
		WebsiteListAlias* websiteList = (*liter);
		if(websiteList->checkUrl(host)){
			response = false;
			break;
		}else{
			//now check the rest of the url for a match
			for(int x = 0; x < host.size(); x++){
				if(host[x] == '.'){
					string target = host.substr(x + 1, host.size() - x + 1);
					if(websiteList->checkUrl(target)){
						response = false;
						break;
					}
				}
			}
			if(!response){
				break;
			}
		}
	}

	return !response;
}

bool SFwallCore::HttpUrlApplicationFilter::filter(SFwallCore::Packet* packet){
	if(!(packet->getProtocol() == TCP)){
		return true;	
	}

	if(!(HttpsTcpPacket::matchPort(packet->getDstPort()) || 
				HttpTcpPacket::matchPort(packet->getDstPort()) || 
				HttpProxyPacket::matchPort(packet->getDstPort()))){

		return true;
	}

	lock();
	list<FilterCriteria*>::iterator iter;
	for(iter = filters.begin(); iter != filters.end(); iter++){
		FilterCriteria* crit = (*iter);

		if(crit->match(packet)){
			//bool response = true;
			Connection* connection = packet->getConnection();
			if(connection->hasHttpHost()){
				std::string host = connection->getHttpHost();
				//first check the full url for a match
				bool websiteHit = hasHit(host, crit);
				unlock();

				if(websiteHit == true && crit->fireEvent && eventDb){
					EventParams params;
					params["host"] = host;
					params["sourceIp"] = IP4Addr::ip4AddrToString(packet->getSrcIp());
					if(packet->getUser()){
						params["user"] = packet->getUser()->getUserName();
					}

					eventDb->add(new Event(FIREWALL_WEBFILTER_HIT, params));
				}

				//If this is a denylist and the host exists in it || action is to block everything
				if((crit->action == DENY && websiteHit) || crit->action == DENY_ALL){
					if(crit->redirect){
						packet->getConnection()->setRewriteRule(new HttpRedirectRewrite(crit->redirectUrl));
					}

					return false;
				}		

				if(crit->action == ALLOW && websiteHit){
					return true;
				}	
			}
		}
	}

	unlock();
	return true;
}

bool SFwallCore::FilterCriteria::match(SFwallCore::Packet* packet){
	if(!enabled){
		return false;
	}

	if((sourceIp > 0 && sourceMask > 0)){
		if(!(packet->getSrcIp() >= sourceIp && packet->getSrcIp() <= sourceMask)){
			return false;
		}
	}

	if(groups.size() > 0){
		if(!packet->getUser()){
			return false;
		}

		bool hasGroupMatch = false;
		for(std::list<Group*>::iterator iter = groups.begin(); iter != groups.end(); iter++){
			if(packet->getUser()->checkForGroup((*iter))){
				hasGroupMatch = true;
				break;
			}
		}

		if(!hasGroupMatch){
			return false;
		}
	}

	if(sourceAlias.size() > 0){
		bool hasSourceMatch = false;
		for(std::list<Alias*>::iterator iter = sourceAlias.begin(); iter != sourceAlias.end(); iter++){
			if((*iter)->searchForNetworkMatch(packet->getSrcIp())){
				hasSourceMatch = true;	
			}
		}

		if(!hasSourceMatch){
			return false;
		}
	}

	if(startTime != -1 && endTime != -1){
		Time* now = new Time(time(NULL)); 
		int wday = now->wday();
		int hour = now->extractHour();
		int minutes = now->extractMins();

		delete now;
		int current = (hour * 100) + minutes;

		//check times
		//16 >= 8 && 16 <= 15
		if(!(current >= startTime && current <= endTime)){
			return false;
		} 

		//check weekday
		switch(wday){
			case 1:
				return mon;
			case 2:
				return tues;
			case 3:
				return wed;
			case 4:
				return thurs;
			case 5:
				return fri;
			case 6:
				return sat;
			case 7:
				return sun;
		}

	}
	return true;
}

void SFwallCore::HttpUrlApplicationFilter::addRule(SFwallCore::FilterCriteria* rule){
	filters.push_back(rule);	
}

void SFwallCore::HttpUrlApplicationFilter::removeRule(SFwallCore::FilterCriteria* rule){
	filters.remove(rule);	
	delete rule;
}

std::list<SFwallCore::FilterCriteria*>& SFwallCore::HttpUrlApplicationFilter::getRules(){
	return filters;
}

void SFwallCore::HttpUrlApplicationFilter::lock(){
	lockPtr->lock();
}

void SFwallCore::HttpUrlApplicationFilter::unlock(){
	lockPtr->unlock();
}

int SFwallCore::HttpUrlApplicationFilter::findpos(FilterCriteria* target){
	int n = 0;
	for(list<FilterCriteria*>::iterator iter = filters.begin();
			iter != filters.end();
			iter++){
		if((*iter) == target){
			return n;
		}
		n++;
	}
	return -1;
}	

void SFwallCore::HttpUrlApplicationFilter::moveup(FilterCriteria* target){
	int currentpos = findpos(target);
	if(currentpos <= 0){
		return;
	}	

	filters.remove(target);
	insert(currentpos -1, target);
}

void SFwallCore::HttpUrlApplicationFilter::insert(int pos, FilterCriteria* target){
	int n =0;
	for(list<FilterCriteria*>::iterator iter = filters.begin();
			iter != filters.end();
			iter++){

		if(n == pos){
			filters.insert(iter, target);
			return;
		}
		n++;
	}

	filters.push_back(target);
}

void SFwallCore::HttpUrlApplicationFilter::movedown(FilterCriteria* target){
	int currentpos = findpos(target);
	if(currentpos == filters.size() -1){                
		return;
	}

	filters.remove(target);
	insert(currentpos + 1, target);
}

SFwallCore::FilterCriteria* SFwallCore::HttpUrlApplicationFilter::get(std::string id){
	for(list<FilterCriteria*>::iterator iter = filters.begin();
			iter != filters.end();
			iter++){

		if((*iter)->id.compare(id) == 0){
			return (*iter);
		}
	}

	return NULL;
}

void SFwallCore::HttpUrlApplicationFilter::groupRemoved(Group* group){
	for(list<FilterCriteria*>::iterator iter = filters.begin();
			iter != filters.end();
			iter++){

		(*iter)->groups.remove(group);	
	}
}
