/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_CRON_H_INCLUDED
#define SPHIREWALL_CRON_H_INCLUDED

#include <list>
#include "Core/Logger.h"
#include "Utils/Lock.h"

class CronManager;
class Config;
class CronJob {
	friend class CronManager;

public:
	CronJob(int cron) : cron(cron) {
		lastRun = 0;
		name = "UNKNOWN_CRON";
		log = false;
	}

	CronJob(int cron, std::string name, bool log) : cron(cron), name(name), log(log) {
		lastRun = 0;
	}

	bool canRun() const;
	virtual void run() = 0;
	void setLastRun(int t);
private:
        bool log;
        std::string name;
	int lastRun;
	int cron;
};

class CronManager {
public:

	CronManager(SLogger::LogContext* logger, Config* config) 
		: logger(logger), config(config) {
		lock = new Lock();
	}

	void registerJob(CronJob* job);
	void roll();
	void run();
	void start();
private:
	SLogger::LogContext* logger;
	Config* config;
	std::list<CronJob*> jobs;
	Lock* lock;
};

#endif
