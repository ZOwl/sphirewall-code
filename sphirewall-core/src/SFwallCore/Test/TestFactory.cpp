/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <gtest/gtest.h>
#include <iostream>
#include <algorithm>
#include "SFwallCore/ConnTracker.h"
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <linux/icmp.h>
#include <netinet/ip.h>

#include "SFwallCore/Test/TestFactory.h"

using namespace SFwallCore;

TcpPacket* PacketBuilder::createTcp(std::string sourceIp, std::string destIp, int sourcePort, int destPort){
	char* packet = (char*) malloc(sizeof(struct iphdr) + sizeof(struct tcphdr));
	struct iphdr* iphdr = (struct iphdr *)(packet);
	struct tcphdr* tcphdr = (struct tcphdr *)(packet + sizeof(struct iphdr));

	iphdr->version = 4;
	iphdr->ihl = 5;
	iphdr->tos = 0;
	iphdr->tot_len = sizeof(struct iphdr) + sizeof(struct tcphdr);
	iphdr->id = 0;
	iphdr->frag_off = 0;
	iphdr->ttl = 255;
	iphdr->protocol = IPPROTO_TCP;
	iphdr->check = 0;
	iphdr->saddr     = htonl(IP4Addr::stringToIP4Addr(sourceIp));
	iphdr->daddr     = htonl(IP4Addr::stringToIP4Addr(destIp));

	tcphdr->source = htons(sourcePort);
	tcphdr->dest   = htons(destPort);

	tcphdr->syn = 0;
	tcphdr->rst = 0;
	tcphdr->ack = 0;
	tcphdr->fin = 0;

	struct sq_packet* sqp = (struct sq_packet*) malloc(sizeof(struct sq_packet));
	sqp->raw_packet = (unsigned char*) packet;
	Packet* ret = Packet::parsePacket(sqp);
	return (TcpPacket*) ret;
}

void PacketBuilder::setSourceDev(Packet* packet, int device){
	struct sq_packet* sqp = packet->getInternalSqp();
	sqp->indev = device;
}

void PacketBuilder::setDestDev(Packet* packet, int device){
	struct sq_packet* sqp = packet->getInternalSqp();
	sqp->outdev = device;
}

UdpPacket* PacketBuilder::createUdp(std::string sourceIp, std::string destIp, int sourcePort, int destPort){
	char* packet = (char*) malloc(sizeof(struct iphdr) + sizeof(struct udphdr));
	struct iphdr* iphdr = (struct iphdr *)(packet);
	struct udphdr* udphdr = (struct udphdr*)(packet + sizeof(struct iphdr));

	iphdr->version = 4;
	iphdr->ihl = 5;
	iphdr->tos = 0;
	iphdr->tot_len = sizeof(struct iphdr) + sizeof(struct tcphdr);
	iphdr->id = 0;
	iphdr->frag_off = 0;
	iphdr->ttl = 255;
	iphdr->protocol = IPPROTO_UDP;
	iphdr->check = 0;
	iphdr->saddr     = htonl(IP4Addr::stringToIP4Addr(sourceIp));
	iphdr->daddr     = htonl(IP4Addr::stringToIP4Addr(destIp));

	udphdr->source = htons(sourcePort);
	udphdr->dest   = htons(destPort);

	struct sq_packet* sqp = (struct sq_packet*) malloc(sizeof(struct sq_packet));
	sqp->raw_packet = (unsigned char*) packet;
	Packet* ret = Packet::parsePacket(sqp);
	return (UdpPacket*) ret;
}

IcmpPacket* PacketBuilder::createIcmp(std::string sourceIp, std::string destIp, int icmpid){
	char* packet = (char*) malloc(sizeof(struct iphdr) + sizeof(struct icmphdr));
	struct iphdr* iphdr = (struct iphdr *)(packet);
	struct icmphdr* udphdr = (struct icmphdr*)(packet + sizeof(struct iphdr));

	iphdr->version = 4;
	iphdr->ihl = 5;
	iphdr->tos = 0;
	iphdr->tot_len = sizeof(struct iphdr) + sizeof(struct tcphdr);
	iphdr->id = 0;
	iphdr->frag_off = 0;
	iphdr->ttl = 255;
	iphdr->protocol = IPPROTO_ICMP;
	iphdr->check = 0;
	iphdr->saddr     = htonl(IP4Addr::stringToIP4Addr(sourceIp));
	iphdr->daddr     = htonl(IP4Addr::stringToIP4Addr(destIp));

	struct icmphdr* icmp = (struct icmphdr *) (packet + (iphdr->ihl << 2));
	icmp->un.echo.id = htons(icmpid);

	struct sq_packet* sqp = (struct sq_packet*) malloc(sizeof(struct sq_packet));
	sqp->raw_packet = (unsigned char*) packet;
	Packet* ret = Packet::parsePacket(sqp);
	return (IcmpPacket*) ret;
}


void PacketBuilder::setAck(TcpPacket* tcpPacket){
	unsigned char* packet = tcpPacket->getInternalSqp()->raw_packet;
	struct iphdr* packet_header= (struct iphdr*) packet;
	struct tcphdr* packet_tcp_header= (struct tcphdr *) (packet+ (packet_header->ihl << 2));
	packet_tcp_header->ack == 1;
}

void PacketBuilder::setSyn(TcpPacket* tcpPacket){
	unsigned char* packet = tcpPacket->getInternalSqp()->raw_packet;
	struct iphdr* packet_header= (struct iphdr*) packet;
	struct tcphdr* packet_tcp_header= (struct tcphdr *) (packet+ (packet_header->ihl << 2));
	packet_tcp_header->syn == 1;
}

