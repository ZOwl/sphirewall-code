/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <resolv.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/wait.h>
#include <signal.h>
#include <map>
#include <poll.h>
#include <linux/types.h>
#include <linux/rtnetlink.h>
#include <curl/curl.h>

extern "C" {
#include <linux/netfilter.h>           /* for NF_ACCEPT */
#include <libnfnetlink/libnfnetlink.h>
}

using namespace std;
using std::string;

#include "Core/System.h"
#include "Utils/Utils.h"
#include "Utils/NetDevice.h"
#include "Utils/IP4Addr.h"
#include "Utils/NetworkChangeNotifier.h"
#include "Core/Event.h"

InterfaceManager::InterfaceManager() {
	lock = new Lock();
	for(int x=0; x < MAX_DEVICES; x++){
		devices[x] = NULL;
	}
}		

void InterfaceManager::load() {
	new boost::thread(boost::bind(&InterfaceManager::listenForEvents, this));
	sleep(5);

	if(config->has("network")){
		lock->lock();

		ObjectContainer* configRoot = config->getElement("network");
		ObjectContainer* configDevices = configRoot->get("devices")->container();
		for(int x=0; x < configDevices->size(); x++){	
			ObjectContainer* configDevice = configDevices->get(x)->container(); 
			NetDeviceConfiguration* target = new NetDeviceConfiguration();

			target->interface = configDevice->get("interface")->string();
			target->ip = configDevice->get("ip")->string();
			target->mask = configDevice->get("mask")->string();
			target->dhcp = configDevice->get("dhcp")->boolean();
			target->name = configDevice->get("name")->string();
			target->gateway = configDevice->get("gateway")->string();
			target->bridge = configDevice->get("bridge")->boolean();
			if(target->bridge){
				ObjectContainer* bridgeDevices = configDevice->get("bridgeDevices")->container();
				for(int y = 0; y < bridgeDevices->size(); y++){
					target->bridgeDevices.push_back(bridgeDevices->get(y)->string());    
				}	
			}

			if(configDevice->has("aliases")){
				ObjectContainer* configAliases = configDevice->get("aliases")->container(); 
				for(int y = 0; y < configAliases->size(); y++){
					target->aliases.push_back(configAliases->get(y)->string());
				}
			}

			configuredDevices.push_back(target);
		}

		if(configRoot->has("routes")){
			ObjectContainer* rarr = configRoot->get("routes")->container();
			for(int x = 0; x < rarr->size(); x++){
				ObjectContainer* routeTarget = rarr->get(x)->container();
				Route* target = new Route();	
				target->gw = routeTarget->get("gw")->string();
				target->dev = routeTarget->get("dev")->string();
				target->dest = routeTarget->get("dest")->string();
				target->id= routeTarget->get("id")->string();
				routes[target->id] = target;
			}
		}
	}
	lock->unlock();
	publish();
}

void InterfaceManager::save(){
	lock->lock();
	ObjectContainer* root = new ObjectContainer(CREL);
	ObjectContainer* confDevices = new ObjectContainer(CARRAY);

	for(list<NetDeviceConfiguration*>::iterator iter = configuredDevices.begin(); 
			iter != configuredDevices.end();
			iter++){

		NetDeviceConfiguration* target = (*iter); 
		if(target){
			ObjectContainer* jsonTarget = new ObjectContainer(CREL);

			jsonTarget->put("ip", new ObjectWrapper((string) target->ip));
			jsonTarget->put("mask", new ObjectWrapper((string) target->mask));
			jsonTarget->put("interface", new ObjectWrapper((string) target->interface));
			jsonTarget->put("gateway", new ObjectWrapper((string) target->gateway));
			jsonTarget->put("dhcp", new ObjectWrapper((bool) target->dhcp));
			jsonTarget->put("name", new ObjectWrapper((string) target->name));
			jsonTarget->put("bridge", new ObjectWrapper((bool) target->bridge));

			if(target->bridge){
				ObjectContainer* bridgeNodes = new ObjectContainer(CARRAY);
				list<string>& devs = target->bridgeDevices;
				for(list<string>::iterator iter = devs.begin(); iter != devs.end(); iter++){
					bridgeNodes->put(new ObjectWrapper((*iter)));
				}

				jsonTarget->put("bridgeDevices", new ObjectWrapper(bridgeNodes));	
			}

			ObjectContainer* aliases = new ObjectContainer(CARRAY);
			list<string>::iterator aIter;
			for(aIter = target->aliases.begin(); aIter != target->aliases.end(); aIter++){
				aliases->put(new ObjectWrapper((string) (*aIter)));	
			}

			jsonTarget->put("aliases", new ObjectWrapper(aliases));						
			confDevices->put(new ObjectWrapper(jsonTarget));
		}
	}

	ObjectContainer* rarr = new ObjectContainer(CARRAY);
	for(map<string, Route*>::iterator iter = routes.begin(); iter != routes.end(); iter++){
		ObjectContainer* target = new ObjectContainer(CREL);
		target->put("gw", new ObjectWrapper((string)iter->second->gw));
		target->put("id", new ObjectWrapper((string)iter->second->id));
		target->put("dev", new ObjectWrapper((string)iter->second->dev));
		target->put("dest", new ObjectWrapper((string) iter->second->dest));
		rarr->put(new ObjectWrapper(target));
	}

	root->put("routes", new ObjectWrapper(rarr));
	root->put("devices", new ObjectWrapper(confDevices));
	config->setElement("network", root);
	config->save();
	lock->unlock();
}

void InterfaceManager::listenForEvents() { 
	stringstream ss; ss << "Starting network interface change notifier ";
	System::getInstance()->logger.getDefault()->log(SLogger::INFO, "InterfaceManager", ss.str(), true);

	notifier.connect();
	notifier.pollfirst();

	while (notifier.waitForEvent() >= 0) {
		std::list<NetworkChange> changes;
		notifier.handleEvent(changes);
		stringstream ss; ss << "Network interface change detected";
		System::getInstance()->logger.getDefault()->log(SLogger::DEBUG, "InterfaceManager", ss.str());

		lock->lock();
		for (list<NetworkChange>::iterator iter = changes.begin();
				iter != changes.end();
				iter++) {
			NetworkChange cn = (*iter);
			if(cn.eventType == DEVICE_ADD_EXISTS){
				NetDevice* target = get(cn.ifid);
				if(target == NULL){
					EventParams params;
					params["ifid"] = cn.ifid; 
					params["ifname"] = cn.ifname;
					eventDb->add(new Event(NETWORK_DEVICES_ADDED, params));

					if(cn.ifid < MAX_DEVICES){
						//Lets search the pendingBridges structure first:
						if(!devices[cn.ifid]){
							devices[cn.ifid] = new NetDevice(cn.ifname);
						}

						devices[cn.ifid]->setId(cn.ifid);
						devices[cn.ifid]->flush();
					}else{
						stringstream ss; ss << "Error, device id is beyond allowed range " << cn.ifname;
						System::getInstance()->logger.getDefault()->log(SLogger::INFO, "InterfaceManager", ss.str());
					}
					notify();
				}		
			}else if(cn.eventType == DEVICE_CHANGE){

				NetDevice* target = get(cn.ifid);
				if(target != NULL){
					EventParams params; 
					params["ifid"] = cn.ifid;
					params["ifname"] = cn.ifname;
					eventDb->add(new Event(NETWORK_DEVICES_CHANGED, params));

					target->flush();
					notify();
				}
			}else if(cn.eventType == DEVICE_REMOVAL){
				NetDevice* target = get(cn.ifid);
				if(target != NULL){
					EventParams params; 
					params["ifid"] = cn.ifid;
					params["ifname"] = cn.ifname;
					eventDb->add(new Event(NETWORK_DEVICES_REMOVED, params));

					devices[cn.ifid] = NULL;
					notify();
					delete target;
				}
			}
		} 
		lock->unlock();
	}

	stringstream s2; s2 << "Shutting down network interface change notifier ";
	System::getInstance()->logger.getDefault()->log(SLogger::INFO, "InterfaceManager", s2.str(), true);
	notifier.disconnect();	
}

void InterfaceManager::notify(){
	list<Listener*>::iterator iter;
	for(iter = registeredExternalListeners.begin();
			iter != registeredExternalListeners.end();
			iter++){

		(*iter)->event();
	}
}

NetDeviceConfiguration* InterfaceManager::addDevice(std::string name){
	NetDeviceConfiguration* device = new NetDeviceConfiguration();
	device->interface = name; 
	configuredDevices.push_back(device);
	return device;
}

void InterfaceManager::deleteDevice(std::string name){
	NetDeviceConfiguration* target = getConfiguredDevice(name);
	configuredDevices.remove(target);	
	delete target;
}

NetDeviceConfiguration* InterfaceManager::getConfiguredDevice(std::string ifname){
	for(list<NetDeviceConfiguration*>::iterator iter = configuredDevices.begin();
			iter != configuredDevices.end();
			iter++){

		if((*iter)->interface.compare(ifname) == 0){
			return (*iter);
		}
	}	
	return NULL;
}


NetDevice* InterfaceManager::get(int id) {
	if(id < MAX_DEVICES){
		return devices[id];
	}
	return NULL;
}

NetDevice* InterfaceManager::get(string name) {
	for (int x = 0; x < MAX_DEVICES; x++) {
		NetDevice* device = devices[x];
		if(device){
			if (device->getInterface().find(name) != -1) {
				return device;
			}
		}
	}

	return NULL;
}

NetDevice::NetDevice() {
	this->dhcp = false;
	this->ip_unpersisted = -1;
	this->netmask_unpersisted = -1;
	this->type = UNKNOWN;

	this->ip = UNSET_ADDRESS;
	this->netmask = UNSET_ADDRESS;
	this->broadcast = UNSET_ADDRESS;
	this->netaddress = UNSET_ADDRESS;
	this->ptpAddress = UNSET_ADDRESS;
	this->linkLayerAddress = UNSET_ADDRESS;
}

std::string mStringAddr(const struct addr* a) {
	char buf[50];
	sprintf(buf, "%s", addr_ntoa(a));
	return std::string(buf);
}

NetDevice::NetDevice(string device) {
	m_interface = device;
	this->dhcp = false;
	this->iPtr = intf_open();
	this->ip_unpersisted = -1;
	this->netmask_unpersisted = -1;
	this->type = UNKNOWN;

	this->ip = UNSET_ADDRESS;
	this->netmask = UNSET_ADDRESS;
	this->broadcast = UNSET_ADDRESS;
	this->netaddress = UNSET_ADDRESS;
	this->ptpAddress = UNSET_ADDRESS;
	this->linkLayerAddress = UNSET_ADDRESS;
}

NetDevice::~NetDevice() {
	intf_close(iPtr);
}

void NetDevice::addBridgeDevice(std::string device){
	bridgeDevices.push_back(device);
}

void NetDevice::removeBridgeDevice(std::string device){
	bridgeDevices.remove(device);
}

struct intf_entry* NetDevice::getRawDevice(std::string name) {
	struct intf_entry* interface = (struct intf_entry*) malloc(sizeof (struct intf_entry) + sizeof (struct addr) * 5);
	interface->intf_len = sizeof (struct intf_entry) + sizeof (struct addr) * 5;
	sprintf(interface->intf_name, "%s", name.c_str());
	if (intf_get(iPtr, interface) == 0) {
		return interface;
	}

	return NULL;
}

void NetDevice::setIp(string i, string netmask) {
	stringstream ss;
	ss << "Set IP address on " << m_interface << " to " << i << "/" << netmask;
	System::getInstance()->logger.getDefault()->log(SLogger::INFO, "InterfaceManager", ss.str());

	this->ip = IP4Addr::stringToIP4Addr(i);
	this->netmask = IP4Addr::stringToIP4Addr(netmask);
}

void NetDevice::flush(){
	struct intf_entry* interface = getRawDevice(this->m_interface);
	if (interface && interface->intf_addr.addr_type != ADDR_TYPE_NONE) {
		string m_ip = mStringAddr(&interface->intf_addr);
		if (m_ip.find("/") != -1) {
			m_ip = m_ip.erase(m_ip.find("/"));
			this->ip = IP4Addr::stringToIP4Addr(m_ip);
		}else{
			this->ip = IP4Addr::stringToIP4Addr(m_ip);
		}

		ip_unpersisted = -1;
		netmask_unpersisted = -1;

		struct addr s_netmask;
		struct sockaddr snetmask;

		addr_btos(interface->intf_addr.addr_bits, &snetmask);
		addr_ston(&snetmask, &s_netmask);
		this->netmask= IP4Addr::stringToIP4Addr(mStringAddr(&s_netmask));

		struct addr s_broadcast;
		addr_bcast(&interface->intf_addr, &s_broadcast);
		this->broadcast = IP4Addr::stringToIP4Addr(mStringAddr(&s_broadcast));

		struct addr net;
		addr_net(&interface->intf_addr, &net);
		this->netaddress = IP4Addr::stringToIP4Addr(mStringAddr(&net));

		if(isUp()){
			this->aliases.clear();	
			for (int x = 0; x < interface->intf_alias_num; x++) {
				NetDeviceAlias alias;
				alias.fromDb = true;

				struct addr target = interface->intf_alias_addrs[x];
				if (target.addr_type == ADDR_TYPE_IP) {
					string m_ip = mStringAddr(&target);

					struct addr netmask;
					struct sockaddr snetmask;

					addr_btos(target.addr_bits, &snetmask);
					addr_ston(&snetmask, &netmask);
					string m_netmask = mStringAddr(&netmask);

					alias.ip = m_ip; alias.ip += "/" + m_netmask;
					alias.address = target;
					aliases.push_back(alias);
				}
			}
		}

		if(interface->intf_type == INTF_TYPE_LOOPBACK){
			type = LOOPBACK;
		}else if(interface->intf_type == INTF_TYPE_TUN){
			type = TUNNEL;
		}else if(interface->intf_type == INTF_TYPE_ETH){
			type = ETHERNET; //This is not really correct
		}else if(interface->intf_type == INTF_TYPE_OTHER){
			type = UNKNOWN;
		}

		if(type == TUNNEL){
			struct addr ptp;
			addr_net(&interface->intf_dst_addr, &ptp);
			this->ptpAddress = IP4Addr::stringToIP4Addr(mStringAddr(&ptp));

			struct addr ll;
			addr_net(&interface->intf_dst_addr, &ll);
			this->linkLayerAddress = IP4Addr::stringToIP4Addr(mStringAddr(&ll));
		}

		this->mtu = interface->intf_mtu;

		free(interface);
	}
}

string NetDevice::getMacAddress() {
	return "";
}

std::list<NetDeviceAlias> NetDevice::getAliases() {
	return aliases;
}

void NetDevice::addAlias(NetDeviceAlias alias) {
	stringstream ss;
	ss << "Added an alias to " << m_interface << " on ip " << alias.ip;
	System::getInstance()->logger.getDefault()->log(SLogger::INFO, "InterfaceManager", ss.str());

	for(list<NetDeviceAlias>::iterator iter = aliases.begin(); iter != aliases.end(); iter++){
		if((*iter).ip.compare(alias.ip) == 0){
			return;
		}
	}

	aliases.push_back(alias);
}

void NetDevice::deleteAlias(NetDeviceAlias alias) {
	stringstream ss;
	ss << "Removed an alias from " << m_interface << " on ip " << alias.ip;
	System::getInstance()->logger.getDefault()->log(SLogger::INFO, "InterfaceManager", ss.str());

	list<NetDeviceAlias>::iterator iter;
	for (iter = aliases.begin(); iter != aliases.end(); iter++) {
		NetDeviceAlias target = (*iter);
		if (target.ip.compare(alias.ip) == 0) {
			aliases.erase(iter);
			break;
		}
	}
}

NetDeviceAlias NetDevice::getAliasByAddress(std::string ip, std::string mask) {
	list<NetDeviceAlias>::iterator iter;
	for (iter = aliases.begin(); iter != aliases.end(); iter++) {
		NetDeviceAlias target = (*iter);
		if (target.ip.compare(ip) == 0) {
			return target;
		}
	}
	return NetDeviceAlias();
}

bool NetDevice::isUp() {
	struct intf_entry* interface = getRawDevice(this->m_interface);
	if(interface){
		bool up = (interface->intf_flags & INTF_FLAG_UP);
		free(interface);
		return up;
	}

	return false;
}

void NetDevice::toggleUp() {
	struct intf_entry* interface = getRawDevice(this->m_interface);
	if(interface){
		if(isUp()){
			interface->intf_flags &= ~INTF_FLAG_UP;
		}else{
			interface->intf_flags |= INTF_FLAG_UP;
		}

		intf_set(iPtr, interface);
		free(interface);
	}
}

void NetDevice::toggleDhcp(bool enabled){
	dhcp = enabled;
}

std::string NetDevice::getDeviceDescription(){
	stringstream ss;
	if(name.size() == 0){
		ss << getInterface() << " " << getIp();
	}else{
		ss << name;
	}

	return ss.str();
}

bool InterfaceManager::isSynced(){
	lock->lock();
	bool result = FileUtils::read("/etc/network/interfaces").compare(generateConfiguration()) == 0;	
	lock->unlock();
	return result;
}

std::string InterfaceManager::generateConfiguration(){
	stringstream file;
	file << "#/etc/network/interfaces file generated by sphirewalld\n";

	list<NetDeviceConfiguration*>::iterator iter;
	list<NetDeviceConfiguration*> dlist = configuredDevices;

	for(iter = dlist.begin(); iter != dlist.end(); iter++){
		NetDeviceConfiguration* target = (*iter);
		if(target->interface.compare("lo") == 0){
			file << "auto " << target->interface << "\n";
			file << "iface lo inet loopback\n";
		}else { 
			file << "auto " << target->interface << "\n";
			file << "allow-hotplug " << target->interface << endl;
			if(target->dhcp){
				file << "iface " << target->interface <<" inet dhcp" << endl;
                                if(target->bridge){
                                        file << "bridge_ports ";
                                        for(list<std::string>::iterator biter = target->bridgeDevices.begin();
                                                        biter != target->bridgeDevices.end();
                                                        biter++){
                                                file << (*biter) << " ";
                                        }
                                        file << endl;
                                }
			}else{
				if(target->ip.size() > 0){
					file << "iface " << target->interface <<" inet static" << endl;
					file << "address " << target->ip << endl;
					file << "netmask " << target->mask << endl;
					
					if(target->gateway.size() > 0){
						file << "gateway " << target->gateway << endl;
					}
				}else{
					file << "iface " << target->interface <<" inet manual" << endl;
				}

				if(target->bridge){
					file << "bridge_ports ";
					for(list<std::string>::iterator biter = target->bridgeDevices.begin();
							biter != target->bridgeDevices.end();
							biter++){
						file << (*biter) << " ";
					}
					file << endl;
				}

				int x = 0;
				for(list<string>::iterator aiter = aiter = target->aliases.begin(); aiter != target->aliases.end(); aiter++){
					file << " up ip addr add " << (*aiter) <<" dev "<< target->interface <<" label " << target->interface << ":" << x << "\n";
					file << " down ip addr del " << (*aiter) <<" dev "<< target->interface <<" label " << target->interface << ":" << x << "\n";
					x++;
				}
			}
		}

		file << "\n";
	}

	for(map<std::string, Route*>::iterator riter = routes.begin(); riter != routes.end(); riter++){
		Route* target = riter->second;	
		file << "up route add -net " << target->dest << " gw " << target->gw << " dev " << target->dev << "\n"; 
		file << "down route add -net " << target->dest << " gw " << target->gw << " dev " << target->dev << "\n"; 
	}

	return file.str();	
}

void InterfaceManager::publish(){
	//Need to check, zero configured devices is probably a mistake:
	if(configuredDevices.size() == 0){
		return;
	}	

	lock->lock();	
	std::string conf = generateConfiguration();
	lock->unlock();

	System::execWithNoFds("ifdown -a");
	sleep(5);

	ofstream configFile;
	configFile.open("/etc/network/interfaces");
	configFile << conf; 
	configFile.close();

	System::execWithNoFds("ifup -a");	
	sleep(5);
}

std::list<NetDevice*> InterfaceManager::listDevices(bool configured){
	list<NetDevice*> ret;
	if(!configured){
		for(int x= 0;x < MAX_DEVICES;x++){
			if(devices[x]){
				ret.push_back(devices[x]);
			}
		}
	}
	return ret;
}

std::list<Route*> InterfaceManager::listRoutes(){
	list<Route*> ret;
	for(map<string, Route*>::iterator iter = routes.begin(); iter != routes.end(); iter++){
		ret.push_back(iter->second);
	}
	return ret;
}

void InterfaceManager::addRoute(Route* target){
	routes[target->id] = target;
	save();
}

void InterfaceManager::deleteRoute(Route* target){
	routes.erase(target->id);
	delete target;
	save();
}

std::string buffer;
size_t curlreaddata( char *ptr, size_t size, size_t nmemb, void *userdata){
	buffer += string(ptr);
	return size * nmemb;
}

Route* InterfaceManager::getRoute(std::string id){
	return routes[id];
}

DNSConfig::DNSConfig() {
	System::exec("cat /etc/resolv.conf | grep domain | cut -d' ' -f2", domain);
	System::exec("cat /etc/resolv.conf | grep search | cut -d' ' -f2", search);

	string dnsServers;
	System::exec("cat /etc/resolv.conf | grep nameserver | cut -d' ' -f2", dnsServers);
	nameserver1 = dnsServers.substr(0, dnsServers.find("\n"));
	nameserver2 = dnsServers.substr(dnsServers.find("\n") + 1, dnsServers.size());
}

string DNSConfig::save() {
	string err;

	if (((int) search.size() == 0) |
			((int) domain.size() == 0) |
			((int) nameserver1.size() == 0) |
			((int) nameserver2.size() == 0)) {
		err += "Not all fields complete";
		return err;
	}

	ofstream fp;
	fp.open("/etc/resolv.conf");
	if (fp.is_open()) {
		fp << "search " << search << endl;
		fp << "domain " << domain << endl;
		fp << "nameserver " << nameserver1 << endl;
		fp << "nameserver " << nameserver2 << endl;
		fp.close();
	} else {
		System::getInstance()->logger.getDefault()->log(SLogger::ERROR, "Could not open /etc/resolve.conf file");
	}

	return err;
}

string NetDevice::getIp() const {
	if(getIp_Int() == UNSET_ADDRESS){
		return "";
	}

	return IP4Addr::ip4AddrToString(getIp_Int());
}

string NetDevice::getNetMask() const {
	if(getNetMask_Int() == UNSET_ADDRESS){
		return "";
	}

	return IP4Addr::ip4AddrToString(getNetMask_Int());
}

string NetDevice::getBroadCast() const {
	if(getBroadCast_Int() == UNSET_ADDRESS){
		return "";
	}
	return IP4Addr::ip4AddrToString(getBroadCast_Int());
}

string NetDevice::getNetworkAddress() const {
	if(getNetworkAddress_Int() == UNSET_ADDRESS){
		return "";
	}

	return IP4Addr::ip4AddrToString(getNetworkAddress_Int());
}

string NetDevice::getPtpAddress() const {
	if(getPtpAddress_Int() == UNSET_ADDRESS){
		return "";
	}

	return IP4Addr::ip4AddrToString(getPtpAddress_Int());
}

string NetDevice::getLinkLayerAddress() const {
	if(getLinkLayerAddress_Int() == UNSET_ADDRESS){
		return "";
	}
	return IP4Addr::ip4AddrToString(getLinkLayerAddress_Int());
}

void InterfaceManager::disconnect(){
	notifier.disconnect();
}

