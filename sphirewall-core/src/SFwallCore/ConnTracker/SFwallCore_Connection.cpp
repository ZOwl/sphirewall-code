/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <linux/icmp.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <sstream>
#include "SFwallCore/Firewall.h"
#include "SFwallCore/ConnTracker.h"
#include "SFwallCore/Utils.h"
#include "Utils/Utils.h"
#include "Core/System.h"
#include "Utils/TimeWrapper.h"
#include "SFwallCore/Exceptions.h"

using namespace std;

/* Plain, snat, dnat connections for each protocol */

SFwallCore::Connection::~Connection() {
	delete rewriteRule;
}

NetDevice* SFwallCore::Connection::getSourceNetDevice(){
	InterfaceManager* manager = System::getInstance()->getInterfaces();
	NetDevice* device = manager->get(getSourceDev());
	return device;
}

NetDevice* SFwallCore::Connection::getDestNetDevice(){
        InterfaceManager* manager = System::getInstance()->getInterfaces();
        NetDevice* device = manager->get(getDestDev());
        return device;
}

int SFwallCore::Connection::getDestDev() const{
	return this->destDev;
}
int SFwallCore::Connection::getSourceDev() const{
	return this->sourceDev;
}


void SFwallCore::Connection::updateTransferRate(const SFwallCore::Packet* packet) {
	if (checkPacket(packet, DIR_SAME)) {
		upload += packet->getLen();
	} else {
		download += packet->getLen();
	}
}

in_addr_t SFwallCore::Connection::getSrcIp() const {
	return m_sourceIp;
}

in_addr_t SFwallCore::Connection::getDstIp() const {
	return m_destIp;
}

int SFwallCore::Connection::hash() const {
	return m_hash;
}

int SFwallCore::Connection::getTime() const {
	return m_time_stamp;
}

Session* SFwallCore::Connection::getSession() const {
	return sessionPtr;
}

TokenBucket* SFwallCore::Connection::getQosBucket() const {
	return qos_bucket;
}

void SFwallCore::Connection::increment(const SFwallCore::Packet* packet) {
	m_last_packet = time(NULL);
	noPackets++;
	updateTransferRate(packet);

	//If this is an http connection, collect http data:
	if(!httpHostSet && packet->getProtocol() == TCP){
		if(HttpTcpPacket::matchPort(packet->getDstPort())){
			HttpTcpPacket* p = new HttpTcpPacket((TcpUdpPacket*) packet);		
			std::string hostname = p->getHost();
			if(hostname.size() > 3){
				httpHostSet = true;
				httpHost = hostname;
			}
			delete p;
		}

		if(HttpsTcpPacket::matchPort(packet->getDstPort())){
			HttpsTcpPacket* p = new HttpsTcpPacket((TcpUdpPacket*)packet);
			if(System::getInstance()->getFirewall()->REQUIRE_TLS == 1 && !p->isTls()){
				terminate();
			}

			std::string hostname = p->getHost();
			if(hostname.size() > 3){
				httpHostSet = true;
				httpHost = hostname;
			}
			delete p;
		}

		if(HttpProxyPacket::matchPort(packet->getDstPort())){
			HttpProxyPacket* p = new HttpProxyPacket((TcpUdpPacket*)packet);
			std::string hostname = p->getHost();
			if(hostname.size() > 3){
				httpHostSet = true;
				httpHost = hostname;
			}
			delete p;
		}
	}
}

void SFwallCore::Connection::setQosBucket(TokenBucket* bucket) {
	qos_bucket = bucket;
}

void SFwallCore::Connection::setSession(Session* session) {
	sessionPtr = session;
}

int SFwallCore::Connection::idle() const {
	return (time(NULL) - m_last_packet);
}

SFwallCore::TcpState* SFwallCore::TcpConnection::getState() {
	return &tcpTrackable->state; 
}

bool SFwallCore::Connection::hasExpired() {
	if(getProtocol() == TCP){
		int i = idle();
		return ((TcpConnection*)this)->tcpTrackable->hasExpired(i);
	}else if(getProtocol() == UDP){
		int diff = time(NULL) - m_last_packet;
		if (diff > Firewall::UDP_CONNECTION_TIMEOUT_THESHOLD) {
			return true;
		}

		if (isTerminating() && ((time(NULL) - pleaseTerminate) > 120)) {
			return true;
		}

		return false;
	}else if(getProtocol() == ICMP){
		int diff = time(NULL) - m_last_packet;
		if (diff > Firewall::ICMP_CONNECTION_TIMEOUT_THESHOLD) {
			return true;
		}

		if (isTerminating() && ((time(NULL) - pleaseTerminate) > 120)) {
			return true;
		}

		return false;
	}

	return false;
}

int SFwallCore::Connection::getNoPackets() const {
	return noPackets;
}

long SFwallCore::Connection::getDownload() const {
	return download;
}

long SFwallCore::Connection::getUpload() const {
	return upload;
}

string SFwallCore::Connection::getHwAddress() const {
	return hwAddress;
}

SFwallCore::Connection* SFwallCore::Connection::parseConnection(Packet* packet) {
	Connection* conn = NULL;
	switch (packet->getProtocol()) {
		case TCP:
			conn = new TcpConnection((TcpPacket*)packet);
			break;

		case UDP:
			conn = new UdpConnection((UdpPacket*)packet);
			break;

		case ICMP:
			conn = new IcmpConnection((IcmpPacket*) packet);
			break;
	};

	if (conn) {
		conn->pleaseTerminate = -1;
	}

	return conn;
}

SFwallCore::Connection::Connection(Packet* packet) {
	rewriteVerdictApplied = false;
	protocol = NDEF;
	nice = 1;
	rewriteRule = NULL;
	qos_bucket = NULL;
	pleaseTerminate = -1;
	m_time_stamp = time(NULL);
	m_last_packet = time(NULL);
	upload = 0;
	download = 0;
	capturePortal = false;
	sessionPtr = NULL;
	log = false;
	httpHostSet = false;

	/*Check to see if we must reverse this connection*/
	if(packet->getDstPort() <= IP4Addr::CLIENT_PORT_MIN){	
		m_sourceIp = packet->getSrcIp();
		m_destIp = packet->getDstIp();
		m_dport = packet->getDstPort();
		m_sport = packet->getSrcPort();
		sourceDev = packet->getSourceDev();
		destDev = packet->getDestDev();
	}else {
		m_sourceIp = packet->getDstIp();
		m_destIp = packet->getSrcIp();
		m_dport = packet->getSrcPort();
		m_sport = packet->getDstPort();
		sourceDev = packet->getDestDev();
		destDev = packet->getSourceDev();
	}

	hwAddress = packet->getHw();
	noPackets = 1;
}

SFwallCore::TcpConnection::TcpConnection(TcpPacket* tcpPacket) : Connection(tcpPacket) {
	m_hash = TcpConnection::hash(tcpPacket);
	tcpTrackable = new TcpTrackable(tcpPacket);
	protocol = TCP;
}

SFwallCore::UdpConnection::UdpConnection(UdpPacket* udpPacket) : Connection(udpPacket) {
	m_hash = UdpConnection::hash(udpPacket);
	protocol = UDP;
}

SFwallCore::IcmpConnection::IcmpConnection(IcmpPacket* icmpPacket) : Connection(icmpPacket) {
	m_icmpId = icmpPacket->getIcmpId();
	m_hash = IcmpConnection::hash(icmpPacket);
	protocol = ICMP;
}

int SFwallCore::Connection::hash(Packet* tcpPacket) {
	proto protocol = tcpPacket->getProtocol();
	if(protocol == TCP || protocol == UDP){
		return (tcpPacket->getSrcPort() +
				tcpPacket->getDstPort() +
				tcpPacket->getDstIp() +
				tcpPacket->getSrcIp()) % Firewall::PLAIN_CONN_HASH_MAX;

	}else if(protocol == ICMP){
		return tcpPacket->getIcmpId() % Firewall::PLAIN_CONN_HASH_MAX;
	}

	return -1;
}

bool SFwallCore::Connection::checkPacket(const Packet* p, PacketDirection filter) const {
	if(getProtocol() == TCP && p->getProtocol() == TCP){
		const TcpPacket* packet = (const TcpPacket*) p;
		if (!packet)
			return false;

		if (filter & DIR_SAME) {
			if (getSrcIp() == packet->getSrcIp() && m_sport == packet->getSrcPort()
					&& getDstIp() == packet->getDstIp() && m_dport == packet->getDstPort()) {
				return true;
			}
		}

		if (filter & DIR_OPPOSITE) {
			if (getSrcIp() == packet->getDstIp() && m_sport == packet->getDstPort()
					&& getDstIp() == packet->getSrcIp() && m_dport == packet->getSrcPort()) {
				return true;
			}
		}

		return false;
	}else if(getProtocol() == UDP && p->getProtocol() == UDP){
		const UdpPacket* packet = (const UdpPacket*) p;
		if (!packet)
			return false;

		if (filter & DIR_SAME) {
			if (getSrcIp() == packet->getSrcIp() && m_sport == packet->getSrcPort()
					&& getDstIp() == packet->getDstIp() && m_dport == packet->getDstPort()) {
				return true;
			}
		}

		if (filter & DIR_OPPOSITE) {
			if (getSrcIp() == packet->getDstIp() && m_sport == packet->getDstPort()
					&& getDstIp() == packet->getSrcIp() && m_dport == packet->getSrcPort()) {
				return true;
			}
		}

		return false;

	}else if(getProtocol() == ICMP && p->getProtocol() == ICMP){
		const IcmpPacket* packet = (const IcmpPacket*) p;
		if (!packet)
			return false;

		if (filter & DIR_SAME) {
			if (getSrcIp() == packet->getSrcIp()
					&& getDstIp() == packet->getDstIp()
					&& m_icmpId == packet->getIcmpId()) {
				return true;
			}
		}

		if (filter & DIR_OPPOSITE) {
			if (getSrcIp() == packet->getDstIp()
					&& getDstIp() == packet->getSrcIp()
					&& m_icmpId == packet->getIcmpId()) {
				return true;
			}
		}

		return false;
	}
}

std::string SFwallCore::TcpConnection::toString() {
	std::stringstream ret;
	ret << "Tcp ";
	ret << IP4Addr::ip4AddrToString(getSrcIp()) << ":" << getSourcePort();
	ret << " -> ";
	ret << IP4Addr::ip4AddrToString(getDstIp()) << ":" << getDestinationPort();
	ret << " Hash:" << m_hash;

	return ret.str();
}

std::string SFwallCore::UdpConnection::toString() {
	std::stringstream ret;
	ret << "Tcp ";
	ret << IP4Addr::ip4AddrToString(getSrcIp()) << ":" << getSourcePort();
	ret << " -> ";
	ret << IP4Addr::ip4AddrToString(getDstIp()) << ":" << getDestinationPort();
	ret << " Hash:" << m_hash;

	return ret.str();
}

std::string SFwallCore::IcmpConnection::toString() {
	std::stringstream ret;
	ret << "Icmp ";
	ret << IP4Addr::ip4AddrToString(getSrcIp());
	ret << " -> ";
	ret << IP4Addr::ip4AddrToString(getDstIp());
	ret << " Hash:" << m_hash;
	return ret.str();
}

void SFwallCore::Connection::terminate() {

	TcpConnection* connection = NULL;
	if (getProtocol() == TCP) {
		connection = (TcpConnection*) this;
		connection->getState()->state = RESET;
	}
	pleaseTerminate = time(NULL);
}

bool SFwallCore::Connection::isTerminating() {
	if (pleaseTerminate != -1 && pleaseTerminate != 0) {
		return true;
	}
	return false;
}
