/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QUOTA_H
#define QUOTA_H

class QuotaInfo{
	public:
		QuotaInfo(){
			quotaExceeded = false;
			dailyQuota = false;
			dailyQuotaLimit = 0;
			weeklyQuota = false;
			weeklyQuotaLimit = 0;
			monthQuota = false;
			monthQuotaLimit = 0;
			monthQuotaBillingDay = 1;
			monthQuotaIsSmart = false;
			totalQuota = false;
			totalQuotaLimit = 0;
			timeQuota = false;
			timeQuotaLimit = 0;
		}

		bool dailyQuota;
		long dailyQuotaLimit;

		bool weeklyQuota;
		long weeklyQuotaLimit;

		bool monthQuota;
		long monthQuotaLimit;
		long monthQuotaBillingDay;
		bool monthQuotaIsSmart;

		bool totalQuota;
		long totalQuotaLimit;

		bool timeQuota;
		long timeQuotaLimit;

		bool quotaExceeded;
};

#endif
