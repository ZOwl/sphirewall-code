/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "SFwallCore/ConnTracker.h"
#include "SFwallCore/TrafficShaper/TrafficShaper.h"
#include "Core/System.h"
#include "Core/Logger.h"

using namespace std;

void TrafficShaper::addBucket(TokenBucket* b) {
	bucket.push_back(b);
}

static char FIELD_DELIM = ':';

TrafficShaper::TrafficShaper(ConfigurationManager* config, GroupDb* groupDb, SessionDb* sessionDb, SFwallCore::PlainConnTracker* connections) : config(config),
groupDb(groupDb),
sessionDb(sessionDb),
connections(connections) {
	logger = new SLogger::LogContext(SLogger::INFO, "TRAFFICSHAPER", "sphirewalld-shaper");
	System::getInstance()->logger.registerContext(logger);
}

void TrafficShaper::start() {
	store = new TSRuleStore(logger, config, this, groupDb, sessionDb);

	new boost::thread(boost::bind(&TrafficShaper::loop, this));
}

TrafficShaper::~TrafficShaper() {
	delete store;
}

void TrafficShaper::loop() {
	while (System::getInstance()->ALIVE) {
		if (bucket.size() > 0) {
			for (int x = 0; x < bucket.size(); x++) {
				bucket[x]->rollBucket();

				if (bucket[x]->hasExpired()) {
					logger->log(SLogger::DEBUG,
							"TrafficShaper::loop", "Bucket has expired ---> deleting");
					del(bucket[x]);
				}
			}

			usleep(100);
		} else { // No shaping specified.  Wait and see if situation changes.
			sleep(5);
		}
	}
}

TokenBucket* TrafficShaper::add(SFwallCore::Packet* packet) {
	for (int x = 0; x < bucket.size(); x++) {
		if (bucket[x]->filter->check(packet)) {
			return bucket[x];
		}
	}

	return store->match(packet);
}

void TrafficShaper::del(TokenBucket* tkb) {
	if (tkb != NULL) {
		lock.lock();

		for (vector<TokenBucket*>::iterator iter = bucket.begin();
				iter != bucket.end();
				++iter) {
			if (*iter == tkb) {
				bucket.erase(iter);
				break;
			}
		}

		connections->deleteTokenBucket(tkb);

		delete tkb;
		lock.unlock();
	}
}

