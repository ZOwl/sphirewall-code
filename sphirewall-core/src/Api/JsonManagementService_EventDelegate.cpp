/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <list>

using namespace std;

#include "Api/JsonManagementService.h"
#include "Core/Event.h"
#include "Core/EventHandler.h"

JSONObject EventDelegate::process(string uri, JSONObject object) {
	if (uri.compare("events/list") == 0) {
		return list(object);
	} else if (uri.compare("events/config/add") == 0) {
		return config_add(object);
	} else if (uri.compare("events/config/del") == 0) {
		return config_del(object);
	} else if (uri.compare("events/config/list") == 0) {
		return config_list(object);
	} else if (uri.compare("events/size") == 0){
		return size();
	} else if (uri.compare("events/purge") == 0) {
		return purge(object);
	} else {
		throw new DelegateNotFoundException(uri);
	}
}

JSONObject EventDelegate::size(){
	JSONObject ret;
	ret.put(L"size", new JSONValue((double) eventDb->size()));
	return ret;
}

JSONObject EventDelegate::list(JSONObject object) {
	std::vector<Event*> events;
	eventDb->list(events);

	int minTimestamp = -1;
	if(object.has(L"time")){
		minTimestamp = object[L"time"]->AsNumber();
	}

	JSONObject ret;
	JSONArray array;
	vector<Event*>::iterator iter;
	for (iter = events.begin(); iter != events.end(); iter++) {
		Event* target = (*iter);
		if(minTimestamp != -1 && target->t < minTimestamp){
			continue;
		}

		JSONObject e;
		e.put(L"time", new JSONValue((double) target->t));
		e.put(L"key", new JSONValue((string) target->key));
		
		JSONArray data;
		map<string, Param> params = target->params.getParams();
		for(map<string, Param>::iterator piter = params.begin(); piter != params.end(); piter++){
			Param p = piter->second;
			JSONObject po;
			
			if(p.isString()){
				po.put(StringToWString(piter->first), new JSONValue((string) p.string()));
			}

			if(p.isNumber()){
				po.put(StringToWString(piter->first), new JSONValue((double) p.number()));
			}
			
			data.push_back(new JSONValue(po));
		}	

		e.put(L"params", new JSONValue(data));
		array.push_back(new JSONValue(e));
	}

	ret.put(L"events", new JSONValue(array));
	return ret;
}

JSONObject EventDelegate::purge(JSONObject object) {
	eventDb->purgeEvents();

	return JSONObject();
}

JSONObject EventDelegate::config_add(JSONObject object) {
	string key = object[L"key"]->String();
	string handler = object[L"handler"]->String();

	EventHandler* target = NULL;
	for(multimap<string, EventHandler*>::iterator iter = eventDb->getHandlers().begin(); iter != eventDb->getHandlers().end(); iter++){
		string ikey = iter->first;
		EventHandler* ihandler = iter->second;
		string ihandlerkey = ihandler->key();

		if(ikey.compare(key) == 0 && ihandlerkey.compare(handler) == 0){
			target = ihandler;	
		}
	}		

	if(target == NULL){
		if(handler.compare("handler.log") == 0){	
			target = new LogEventHandler();
		}

		if(handler.compare("handler.stdout") == 0){
			target = new StdOutEventHandler();
		}			

		if(handler.compare("handler.users.disable") == 0){
			target = new DisableUserEventHandler();
		}

		if(handler.compare("handler.users.delete") == 0){
			target = new DeleteUserEventHandler();
		}

		if(handler.compare("handler.firewall.block") == 0){
			target = new FirewallBlockSourceAddressHandler();
		}

		if(handler.compare("handler.email") == 0){
			target = new EmailEventHandler();
		}

		if(handler.compare("handler.groups.add") == 0){
			target = new AddGroupEventHandler();
		}

		if(handler.compare("handler.groups.remove") == 0){
			target = new RemoveGroupEventHandler();
		}

		if(handler.compare("handler.firewall.rewrite") == 0){
			target = new RedirectToUrl();
		}

		if(target != NULL){
			eventDb->addHandler(key, target);
		}else{
			throw DelegateGeneralException("could not match handler");
		}
	}	

	if(target != NULL){
                if(handler.compare("handler.email") == 0 && object.has(L"email")){
                        ((EmailEventHandler*)target)->setEmail(object[L"email"]->String());
                }

		if(handler.compare("handler.groups.add") == 0 && object.has(L"groupid")){
                        ((AddGroupEventHandler*)target)->setGroupId(object[L"groupid"]->AsNumber());
                }
                
                if(handler.compare("handler.groups.remove") == 0 && object.has(L"groupid")){
                        ((RemoveGroupEventHandler*)target)->setGroupId(object[L"groupid"]->AsNumber());
                }

		if(handler.compare("handler.firewall.rewrite") == 0 && object.has(L"url")){
                        ((RedirectToUrl*)target)->setUrl(object[L"url"]->String());
                }
	}

	eventDb->save();
	return JSONObject();
}

JSONObject EventDelegate::config_del(JSONObject object) {
	string handler = object[L"handler"]->String();
	string key = object[L"key"]->String();

	EventHandler* target = NULL;
	for(multimap<string, EventHandler*>::iterator iter = eventDb->getHandlers().begin(); iter != eventDb->getHandlers().end(); iter++){
		string ikey = iter->first;
		EventHandler* ihandler = iter->second;
		if(ikey.compare(key) == 0 && ihandler->key().compare(handler) == 0){
			target = iter->second;
			eventDb->removeHandler(key, target);
			break;
		}
	}               

	return JSONObject();
}

JSONObject EventDelegate::config_list(JSONObject object) {
	JSONObject ret;
	JSONArray array;

	for(multimap<string, EventHandler*>::iterator iter = eventDb->getHandlers().begin(); iter != eventDb->getHandlers().end(); iter++){
		string ikey = iter->first;
		EventHandler* ihandler = iter->second;

		JSONObject o;
		o.put(L"handler", new JSONValue((string) ihandler->key()));
		o.put(L"key", new JSONValue((string) ikey));
		o.put(L"name", new JSONValue((string) ihandler->name()));

		if(ihandler->key().compare("handler.email") == 0){
			o.put(L"email", new JSONValue((string) ((EmailEventHandler*)ihandler)->getEmail()));
		}		

		if(ihandler->key().compare("handler.groups.add") == 0){
			o.put(L"groupid", new JSONValue((double) ((AddGroupEventHandler*)ihandler)->getGroupId()));
		}

                if(ihandler->key().compare("handler.groups.remove") == 0){
                        o.put(L"groupid", new JSONValue((double) ((RemoveGroupEventHandler*)ihandler)->getGroupId()));
                }

                if(ihandler->key().compare("handler.firewall.rewrite") == 0){
                        o.put(L"url", new JSONValue((std::string) ((RedirectToUrl*)ihandler)->getUrl()));
                }

		array.push_back(new JSONValue(o));
	}

	ret.put(L"handlers", new JSONValue(array));
	return ret;
}

