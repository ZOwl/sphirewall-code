/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <linux/module.h>
#include <net/sock.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <asm/types.h>
#include <linux/skbuff.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/icmp.h>
#include <linux/in.h>
#include <linux/netfilter_ipv4.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/netfilter.h>
#include <linux/kfifo.h>
#include <net/netfilter/nf_queue.h>
#include <linux/msg.h>
#include <linux/ipc.h>
#include <linux/types.h>
#include <linux/in_route.h>
#include <net/ip.h>
#include <linux/kfifo.h>
#include "queue.h"
#include <linux/version.h>
#include "sphirewall_queue.h"

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,8,0)
#define NFW_register_queue_handler(a,b) nf_register_queue_handler((a),(b))
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,8,0)
#define NFW_register_queue_handler(a,b) nf_register_queue_handler((b))
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,7,0)
#define VM_RESERVED (VM_IO | VM_LOCKED | (VM_DONTEXPAND | VM_DONTDUMP))
#endif

static int NAT_SNATID_START = 10000;
static int NAT_SNATID_END = 64000;
static int UDP_MAX_CONNECTION_LIFE = 120;
static int ICMP_MAX_CONNECTION_LIFE = 120;
static int NAT_GC_TIMER = 360;
static int TCP_CONNECTION_TIMEOUT_THESHOLD = 7200;
static int TCP_CLOSING_TIMEOUT = 120;

module_param(NAT_SNATID_START, int, 0644);
module_param(NAT_SNATID_END, int, 0644);
module_param(UDP_MAX_CONNECTION_LIFE, int, 0644);
module_param(ICMP_MAX_CONNECTION_LIFE, int, 0644);
module_param(NAT_GC_TIMER, int, 0644);
module_param(TCP_CONNECTION_TIMEOUT_THESHOLD, int, 0644);
module_param(TCP_CLOSING_TIMEOUT, int, 0644);

#define NAT_CONNTRACKER_MAXCONNS 30000 
#define DIFF 0
#define SAME 1
#define TCPUP 1
#define CLOSING 2

struct qmgr* qmgr_recv = NULL;
struct qmgr* qmgr_send = NULL;

static int nat_conntracker_lastgc = 0;
struct list_head nat_conntracker_bucket[NAT_CONNTRACKER_MAXCONNS];
struct list_head nat_conntracker_bucket_dnat[NAT_CONNTRACKER_MAXCONNS];
void nat_conntracker_init(void);
void nat_conntracker_destroy(void);
void nat_conntracker_garbagecollector(unsigned long data);

void nat_conntracker_releaseport(int port);
void nat_conntracker_setupports(void);
int nat_conntracker_findunusedport(void);

static spinlock_t nat_conntracker_lockobj;
void nat_conntracker_lock(void);
void nat_conntracker_unlock(void);

void msg_setup_and_copy_data(struct nf_queue_entry*, struct message*);
void msg_wrap_devices(struct message* msg, struct nf_queue_entry* entry);
void msg_translate_hooks(struct nf_queue_entry* entry, struct message* msg);

struct natct {
	//Translate it to this
	struct list_head list;
	int type;
	long target_address;
	int target_port;
	int protocol;
	int state;

	long source_address;
	long destination_address;
	int source_port;
	int destination_port;
	int icmp_id;

	int timestamp;
	int fin;
	int hash;
};

struct natct* nat_conntracker_check(struct nf_queue_entry* entry, int type, int direction);
void nat_do_translation(struct natct* ct, struct nf_queue_entry* entry, int direction);
struct natct* nat_conntracker_create(struct nf_queue_entry* entry, struct message* m, int type);
void nat_conntracker_updateconnectionstate(struct natct* ct, struct nf_queue_entry* entry);
struct unusedport{
        struct list_head list;
        int port;
};

struct list_head unusedportlist;

/*Queue constants*/
static struct nf_hook_ops in_hook = {
	.hook     = hook_func,
	.hooknum  = NF_INET_LOCAL_IN,
	.pf       = 2,
	.priority = NF_IP_PRI_FIRST,
};

static struct nf_hook_ops out_hook = {
	.hook     = hook_func,
	.hooknum  = NF_INET_LOCAL_OUT,
	.pf       = 2,
	.priority = NF_IP_PRI_FIRST,
};

static struct nf_hook_ops prerouting_hook = {
	.hook     = hook_func,
	.hooknum  = NF_INET_PRE_ROUTING,
	.pf       = 2,
	.priority = NF_IP_PRI_FIRST,
};

static struct nf_hook_ops postrouting_hook = {
	.hook     = hook_func,
	.hooknum  = NF_INET_POST_ROUTING,
	.pf       = 2,
	.priority = NF_IP_PRI_FIRST,
};

static struct nf_hook_ops forward_hook = {
	.hook     = hook_func,
	.hooknum  = NF_INET_FORWARD,
	.pf       = 2,
	.priority = NF_IP_PRI_FIRST,
};

long checksum(unsigned short *addr, unsigned int count);
static void nf_nat_ipv4_csum_recalc(struct sk_buff *skb, u8 proto, void *data, __sum16 *check, int datalen, int oldlen);

static const struct nf_queue_handler nfqh = {
//#ifndef NEW_KERNEL
//	.name   = "sphirewall_queue",
//#endif
	.outfn  = &squeue_outgoing_push,
};

void register_hooks(void){
	nat_conntracker_init();
	nf_register_hook(&in_hook);
	nf_register_hook(&out_hook);
	nf_register_hook(&forward_hook);
	nf_register_hook(&prerouting_hook);
	nf_register_hook(&postrouting_hook);
	NFW_register_queue_handler(2, &nfqh);
}

void unregister_hooks(void){
	nf_unregister_hook(&in_hook);
	nf_unregister_hook(&out_hook);
	nf_unregister_hook(&forward_hook);
	nf_unregister_hook(&prerouting_hook);
	nf_unregister_hook(&postrouting_hook);
#ifndef NEW_KERNEL
	nf_unregister_queue_handlers(&nfqh);
#endif
	wake_up(&send_wait_lock);
	nat_conntracker_destroy();
}

int currenttimestamp(void){
	return (jiffies / HZ);
}

struct tcphdr* tcphdr_ptr(struct sk_buff* skb){
	return (struct tcphdr *) ((unsigned char*) ip_hdr(skb) + (ip_hdr(skb)->ihl << 2));		
}

struct udphdr* udphdr_ptr(struct sk_buff* skb){
	return (struct udphdr*) ((unsigned char*) ip_hdr(skb) + (ip_hdr(skb)->ihl << 2));	
}

struct icmphdr* icmphdr_ptr(struct sk_buff* skb){
	return (struct icmphdr*) ((unsigned char*) ip_hdr(skb) + (ip_hdr(skb)->ihl << 2));	
}

/*
 * Function stolen from nfnetlink_queue.c:
 * 
 * This function looks after mangling packets.
 */
static int nfqnl_mangle(void *data, int data_len, struct nf_queue_entry *e)
{
	struct sk_buff *nskb;
	int diff;

	diff = data_len - e->skb->len;
	if (diff < 0) {
		if (pskb_trim(e->skb, data_len))
			return -ENOMEM;
	} else if (diff > 0) {
		if (data_len > 0xFFFF)
			return -EINVAL;
		if (diff > skb_tailroom(e->skb)) {
			nskb = skb_copy_expand(e->skb, skb_headroom(e->skb),
					diff, GFP_ATOMIC);
			if (!nskb) {
				PMESSAGE("OOM in mangle, dropping packet\n");
				return -ENOMEM;
			}
			kfree_skb(e->skb);
			e->skb = nskb;
		}
		skb_put(e->skb, diff);
	}
	if (!skb_make_writable(e->skb, data_len))
		return -ENOMEM;
	skb_copy_to_linear_data(e->skb, data, data_len);
	e->skb->ip_summed = CHECKSUM_NONE;
	return 0;
}

void squeue_incoming_worker(void* data)
{
	struct nf_queue_entry* entry = NULL;
	struct message* msg = NULL;
	struct sq_packet* packet = NULL;
	struct iphdr* iph = NULL;
	struct natct* ct = NULL;

	while(!kthread_should_stop()){
		while(qmgr_recv->state == STATE_ONLINE){
			msg = queue_peek(qmgr_recv);
			if(msg == NULL){
				PMESSAGE("msg pointer was null\n");
				wake_up(&send_wait_lock);
				continue;
			}

			entry = entry_map_get(msg->id);
			if(entry){
				if((currenttimestamp() - nat_conntracker_lastgc) > NAT_GC_TIMER){
					nat_conntracker_lock();
					nat_conntracker_garbagecollector(0);	
					nat_conntracker_lastgc = currenttimestamp();
					nat_conntracker_unlock();
				}

				if(entry->hook == NF_INET_POST_ROUTING){
					if(msg->nat.type == NAT_SNAT){
						nat_conntracker_lock();
						ct = nat_conntracker_create(entry, msg, NAT_SNAT);
						if(ct){
							nat_do_translation(ct, entry, SAME);	
							nat_conntracker_updateconnectionstate(ct, entry);
						}
						nat_conntracker_unlock();
					}

					nf_reinject(entry, NF_ACCEPT);
					queue_pop(qmgr_recv);
					continue;
				}else if(entry->hook == NF_INET_PRE_ROUTING){	
					if(msg->nat.type == NAT_DNAT){
						nat_conntracker_lock();
						ct = nat_conntracker_create(entry, msg, NAT_DNAT);
						if(ct){
							nat_do_translation(ct, entry, SAME);	
							nat_conntracker_updateconnectionstate(ct, entry);
						}
						nat_conntracker_unlock();
					}

					nf_reinject(entry, NF_ACCEPT);
					queue_pop(qmgr_recv);
					continue;
				}

				if(msg->modified == 1){
					packet = &msg->packet;
					iph = (struct iphdr*) packet->raw_packet;
					//Re calculate the checksums if required:
					nfqnl_mangle((char*) packet->raw_packet, ntohs(iph->tot_len), entry);	

					iph->check = 0;
					iph->check = checksum((unsigned short *) iph, sizeof (struct iphdr));
					
					if(iph->protocol == IPPROTO_TCP){		
						tcphdr_ptr(entry->skb)->check = 0;
						int len = ntohs(iph->tot_len) - (iph->ihl << 2);
						nf_nat_ipv4_csum_recalc(entry->skb, IPPROTO_TCP, tcphdr_ptr(entry->skb), &tcphdr_ptr(entry->skb)->check, len, len);
					}

					nf_reinject(entry, msg->verdict);
					queue_pop(qmgr_recv);

				}else{
					nf_reinject(entry, msg->verdict);
					queue_pop(qmgr_recv);
				}
			}
		}
		msleep(100);
	}
}

unsigned int hook_func(	unsigned int hooknum, 
		struct sk_buff *pskb, 
		const struct net_device *in, 
		const struct net_device *out, 
		int (*okfn)(struct sk_buff *)){

	if(qmgr_send->state == STATE_ONLINE && qmgr_recv->state == STATE_ONLINE){
		if((in && in->ifindex == 1) || (out && out->ifindex == 1)){
			return NF_ACCEPT;
		}

		return NF_QUEUE;
	}

	return DEFAULT_ACTION;
} 

int squeue_outgoing_push(struct nf_queue_entry *entry, unsigned int queuenum){
	send_queue_push_entry(entry);
	wake_up(&send_wait_lock);

	return 0;
}

int squeue_outgoing_worker(void* data){
	struct natct* ct = NULL;
	struct natct* sct = NULL;
	struct natct* dct = NULL;
	struct nf_queue_entry* entry = NULL;
	struct message* msg = NULL;

	for(;;){
		wait_event_interruptible(send_wait_lock, !send_queue_empty() || kthread_should_stop() || qmgr_send->state == STATE_OFFLINE);
		if(kthread_should_stop()){
			break;		
		}

		if(qmgr_send->state == STATE_OFFLINE){
			msleep(10);
			continue;
		}

		while(!send_queue_empty()){
			entry = send_queue_pop_entry();	
			if(entry){	
				if(entry->hook == NF_INET_PRE_ROUTING){

					nat_conntracker_lock();
					ct = nat_conntracker_check(entry, NAT_DNAT, SAME);
					if(ct){
						nat_do_translation(ct, entry, SAME);
						nat_conntracker_updateconnectionstate(ct, entry);
						nat_conntracker_unlock();
						nf_reinject(entry, NF_ACCEPT);
						continue;
					}


					//Check SNAT connection tracker					
					sct = nat_conntracker_check(entry, NAT_SNAT, DIFF);
					if(sct){
						nat_do_translation(sct, entry, DIFF);
						nat_conntracker_updateconnectionstate(sct, entry);
						nat_conntracker_unlock();
						nf_reinject(entry, NF_ACCEPT);
						continue;
					}
					nat_conntracker_unlock();

					//If we are not a new connection, then give it back to the kernel
					if(ip_hdr(entry->skb)->protocol == IPPROTO_TCP){
						if(tcphdr_ptr(entry->skb)->syn != 1){
							nf_reinject(entry, NF_ACCEPT);
							continue;
						}
					}
				}

				if(entry->hook == NF_INET_POST_ROUTING){
					//Check SNAT connection tracker
					nat_conntracker_lock();
					ct = nat_conntracker_check(entry, NAT_SNAT, SAME);
					if(ct){
						nat_do_translation(ct, entry, SAME);
						nat_conntracker_updateconnectionstate(ct, entry);
						nat_conntracker_unlock();
						nf_reinject(entry, NF_ACCEPT);
						continue;
					}

					//Check DNAT connection tracker
					dct = nat_conntracker_check(entry, NAT_DNAT, DIFF);
					if(dct){
						nat_do_translation(dct, entry, DIFF);
						nat_conntracker_updateconnectionstate(dct, entry);
						nat_conntracker_unlock();
						nf_reinject(entry, NF_ACCEPT);
						continue;
					}

					nat_conntracker_unlock();
					if(ip_hdr(entry->skb)->protocol == IPPROTO_TCP){
						if(tcphdr_ptr(entry->skb)->syn != 1){
							nf_reinject(entry, NF_ACCEPT);
							continue;
						}
					}

				}

				if(qmgr_send->state == STATE_OFFLINE){
					nf_reinject(entry, DEFAULT_ACTION);
					break;
				}

				msg = queue_enqueue(qmgr_send);	
				if(msg == NULL){
					printk("halting enqueue\n");
					break;
				}

				msg_setup_and_copy_data(entry, msg);
				msg_wrap_devices(msg, entry);
				msg_translate_hooks(entry, msg);

				msg->id = entry_map_add(entry);
				if(msg->id == -1){
					msg->id = entry_map_add(entry);
					if(msg->id == -1){
						printk(KERN_CRIT "failed to add packet to kernel --> userspace queue, it appears full\n");
						nf_reinject(entry, NF_DROP);
						return -1;
					}
				}	

				queue_push(qmgr_send);
			}
		}
	}
	return 0;
}

void msg_setup_and_copy_data(struct nf_queue_entry* entry, struct message* msg){
	static int x = 0;

	msg->packet.id = entry->id = ++x;
	msg->packet.len = ntohs(ip_hdr(entry->skb)->tot_len);
	msg->entryPtr = entry;
	msg->verdict = 0;

	memcpy( msg->packet.raw_packet, (unsigned char *)ip_hdr(entry->skb),ntohs(ip_hdr(entry->skb)->tot_len));
}

void msg_wrap_devices(struct message* msg, struct nf_queue_entry* entry){
	struct net_device* indev = NULL;
	struct net_device* outdev = NULL;
	struct sk_buff* skb = NULL;

	skb = entry->skb;
	indev = entry->indev;
	outdev = entry->outdev;

	if(indev){
		msg->packet.indev = indev->ifindex;
	}else{
		msg->packet.indev = 0;
	}

	if(outdev){
		msg->packet.outdev = outdev->ifindex;
	}else{
		msg->packet.outdev = 0;
	}

	if (indev && skb->dev && skb->network_header != skb->mac_header) {

		msg->packet.hwlen = dev_parse_header(skb, (unsigned char*)&msg->packet.hw_addr);
	}else{
		msg->packet.hwlen = 0;
	}
}

void msg_translate_hooks(struct nf_queue_entry* entry, struct message* msg){
	if(!entry || !msg){
		PMESSAGE("!entry or !msg in msg_translate_hooks\n");
		return;
	}

	switch(entry->hook){
		case NF_INET_PRE_ROUTING:
			msg->hook = 1;
			break;

		case NF_INET_LOCAL_IN:
			msg->hook = 2;
			break;

		case NF_INET_FORWARD:
			msg->hook = 2;
			break;

		case NF_INET_LOCAL_OUT:
			msg->hook = 2;
			break;

		case NF_INET_POST_ROUTING:
			msg->hook = 3;
			break;
	}
}

void nat_conntracker_init(void){
	int i = 0;
	for(i = 0; i < NAT_CONNTRACKER_MAXCONNS; i++){
		INIT_LIST_HEAD(&nat_conntracker_bucket[i]);
	}

	for(i = 0; i < NAT_CONNTRACKER_MAXCONNS; i++){
		INIT_LIST_HEAD(&nat_conntracker_bucket_dnat[i]);
	}

	nat_conntracker_setupports();
}

void nat_conntracker_destroy(void){
}

void nat_conntracker_lock(void){
	spin_lock_bh(&nat_conntracker_lockobj);
}

void nat_conntracker_unlock(void){
	spin_unlock_bh(&nat_conntracker_lockobj);
}

bool nat_icmp_matches(struct nf_queue_entry* entry, unsigned int source_adddress, unsigned int destination_address, unsigned int id){
	if(ip_hdr(entry->skb)->protocol == IPPROTO_ICMP){
		if(ip_hdr(entry->skb)->saddr == source_adddress 
				&& ip_hdr(entry->skb)->daddr == destination_address){

			if(icmphdr_ptr(entry->skb)->un.echo.id == id){
				return true;
			}
		}
	}

	return false;
}


bool nat_tcp_matches(struct nf_queue_entry* entry, unsigned int source_adddress, unsigned int destination_address, int source_port, int destination_port){
	struct iphdr* ip = ip_hdr(entry->skb);
	if(ip->protocol == IPPROTO_TCP){
		struct tcphdr* tcp = tcphdr_ptr(entry->skb);
		if(ip->saddr == source_adddress && ip->daddr == destination_address){
			if(tcp->source == source_port && tcp->dest== destination_port){
				return true;
			}
		}
	}else if(ip->protocol == IPPROTO_UDP){
		struct udphdr* tcp = udphdr_ptr(entry->skb); 
		if(ip->saddr == source_adddress && ip->daddr == destination_address){
			if(tcp->source == source_port && tcp->dest== destination_port){
				return true;
			}
		}
	}

	return false;
}

bool nat_conntracker_match(struct natct* target, struct nf_queue_entry* entry,int type, int direction){
	if(target->type == NAT_DNAT){
		if(ip_hdr(entry->skb)->protocol == IPPROTO_TCP || ip_hdr(entry->skb)->protocol == IPPROTO_UDP){
			if(direction == SAME){
				return nat_tcp_matches(entry, target->source_address, target->destination_address, target->source_port, target->destination_port);
			}else if(direction == DIFF){
				return nat_tcp_matches(entry, target->target_address, target->source_address, target->target_port, target->source_port);
			}
			return false;

		}else if (ip_hdr(entry->skb)->protocol == IPPROTO_ICMP){
			if(direction == SAME){
				return nat_icmp_matches(entry, target->source_address, target->destination_address, target->icmp_id);
			}else if(direction == DIFF){
				return nat_icmp_matches(entry, target->destination_address, target->target_address, target->icmp_id);
			}
			return false;
		}

	}else if(target->type == NAT_SNAT){
		if(ip_hdr(entry->skb)->protocol == IPPROTO_TCP || ip_hdr(entry->skb)->protocol == IPPROTO_UDP){
			if(direction == SAME){
				return nat_tcp_matches(entry, target->source_address, target->destination_address, target->source_port, target->destination_port);
			}else if(direction == DIFF){
				return nat_tcp_matches(entry, target->destination_address, target->target_address, target->destination_port, target->target_port);
			}
			return false;

		}else if (ip_hdr(entry->skb)->protocol == IPPROTO_ICMP){
			if(direction == SAME){  
				return nat_icmp_matches(entry, target->source_address, target->destination_address, target->icmp_id);
			}else if(direction == DIFF){
				return nat_icmp_matches(entry, target->destination_address, target->target_address, target->icmp_id);
			}
			return false;
		}
	}

	return false;
}

int nat_conntracker_tcp_hash(struct nf_queue_entry* entry, int type, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	struct tcphdr* tcp = tcphdr_ptr(entry->skb); 

	if(type == NAT_SNAT){
		if(direction == SAME){
			return (ip->daddr + tcp->dest) % NAT_CONNTRACKER_MAXCONNS;
		}else if(direction == DIFF){
			return (ip->saddr + tcp->source) % NAT_CONNTRACKER_MAXCONNS;
		}
	}else if (type == NAT_DNAT){
		if(direction == SAME){
			return (ip->saddr + tcp->source) % NAT_CONNTRACKER_MAXCONNS;
		}else if(direction == DIFF){
			return (ip->daddr + tcp->dest) % NAT_CONNTRACKER_MAXCONNS; 
		}
	}

	return -1;	
}

int nat_conntracker_udp_hash(struct nf_queue_entry* entry, int type, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	struct udphdr* tcp = udphdr_ptr(entry->skb);

	if(type == NAT_SNAT){
		if(direction == SAME){
			return (ip->daddr + tcp->dest) % NAT_CONNTRACKER_MAXCONNS;
		}else if(direction == DIFF){
			return (ip->saddr + tcp->source) % NAT_CONNTRACKER_MAXCONNS;
		}
	}else if (type == NAT_DNAT){
		if(direction == SAME){
			return (ip->saddr + tcp->source) % NAT_CONNTRACKER_MAXCONNS;
		}else if(direction == DIFF){
			return (ip->daddr + tcp->dest) % NAT_CONNTRACKER_MAXCONNS;
		}
	}

	return -1;	
}

int nat_conntracker_icmp_hash(struct nf_queue_entry* entry, int type, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	if(type == NAT_SNAT){
		if(direction == SAME){
			return ip->daddr % NAT_CONNTRACKER_MAXCONNS;
		}else if(direction == DIFF){
			return ip->saddr % NAT_CONNTRACKER_MAXCONNS;
		}
	}else if (type == NAT_DNAT){
		if(direction == SAME){
			return ip->saddr % NAT_CONNTRACKER_MAXCONNS;
		}else if(direction == DIFF){
			return ip->daddr % NAT_CONNTRACKER_MAXCONNS;
		}
	}

	return -1;	
}

int nat_conntracker_hash(struct nf_queue_entry* entry, int type, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	if(ip->protocol == IPPROTO_TCP){
		return nat_conntracker_tcp_hash(entry, type, direction);
	}else if(ip->protocol == IPPROTO_UDP){
		return nat_conntracker_udp_hash(entry, type, direction);
	}else if(ip->protocol == IPPROTO_ICMP){
		return nat_conntracker_icmp_hash(entry, type, direction);
	}	

	return -1;
}

struct natct* nat_conntracker_check(struct nf_queue_entry* entry, int type, int direction){
	int timestamp = currenttimestamp();
	struct iphdr* ip = ip_hdr(entry->skb);
	if(!(ip->protocol == IPPROTO_TCP || ip->protocol == IPPROTO_UDP || ip->protocol == IPPROTO_ICMP)){
		return NULL;
	}

	if(type == NAT_SNAT){
		int hash = nat_conntracker_hash(entry, type, direction);
		struct natct *cursor = NULL;
		list_for_each_entry(cursor, &nat_conntracker_bucket[hash], list) {
			struct natct* ct = cursor; 
			if(nat_conntracker_match(ct, entry, type, direction)){
				ct->timestamp = timestamp;
				return ct;
			}
		}
	}else if(type == NAT_DNAT){
		int hash = nat_conntracker_hash(entry, type, direction);
		struct natct *cursor = NULL;
		list_for_each_entry(cursor, &nat_conntracker_bucket_dnat[hash], list) {
			struct natct* ct = cursor;
			if(nat_conntracker_match(ct, entry, type, direction)){
				return ct;
			}
		}
	}
	return NULL;
}

long checksum(unsigned short *addr, unsigned int count) {
	register long sum = 0;
	while (count > 1) {
		sum += *addr++;
		count -= 2;
	}

	if (count > 0)
		sum += *(unsigned char *) addr;

	while (sum >> 16) {
		sum = (sum & 0xffff) + (sum >> 16);
	}
	return ~sum;
}

static void nf_nat_ipv4_csum_recalc(struct sk_buff *skb, u8 proto, void *data, __sum16 *check, int datalen, int oldlen){
	const struct iphdr *iph = ip_hdr(skb);
	if (skb->ip_summed != CHECKSUM_PARTIAL) {
		if ((!skb->dev || skb->dev->features & NETIF_F_V4_CSUM)) {
			skb->ip_summed = CHECKSUM_PARTIAL;
			skb->csum_start = skb_headroom(skb) + skb_network_offset(skb) + ip_hdrlen(skb);
			skb->csum_offset = (void *)check - data;
			*check = ~csum_tcpudp_magic(iph->saddr, iph->daddr, datalen, proto, 0);
		} else {
			*check = 0;
			*check = csum_tcpudp_magic(iph->saddr, iph->daddr, datalen, proto, csum_partial(data, datalen,0));
			if (proto == IPPROTO_UDP && !*check){
				*check = CSUM_MANGLED_0;
			}
		}
	} else{
		inet_proto_csum_replace2(check, skb, htons(oldlen), htons(datalen), 1);
	}
}

void nat_conntracker_updateconnectionstate(struct natct* ct, struct nf_queue_entry* entry){
	ct->timestamp = currenttimestamp();	

	if(ct->protocol == IPPROTO_TCP){
		struct iphdr* ip = ip_hdr(entry->skb);
		struct tcphdr* tcp = (struct tcphdr *) ((unsigned char*) ip + (ip->ihl << 2));
		if(ct->state == CLOSING || tcp->fin == 1 || tcp->rst == 1){
			ct->state = CLOSING;
		}else{
			ct->state = TCPUP;
		}
	}	
}

int nat_conntracker_hasexpired(struct natct* ct){
	int idleTime = currenttimestamp() - ct->timestamp; 

	if(ct->protocol == IPPROTO_TCP){
		if (ct->state == TCPUP && idleTime >= TCP_CONNECTION_TIMEOUT_THESHOLD)
			return 1;

		if (ct->state == CLOSING && idleTime >= TCP_CLOSING_TIMEOUT)
			return 1;

	}else if(ct->protocol == IPPROTO_UDP){
		if((currenttimestamp() - ct->timestamp) > UDP_MAX_CONNECTION_LIFE){
			return 1;
		}
	}else if(ct->protocol == IPPROTO_ICMP){
		if((currenttimestamp() - ct->timestamp) > ICMP_MAX_CONNECTION_LIFE){
			return 1;
		}
	}

	return 0;
}

void nat_conntracker_garbagecollector(unsigned long data){
	int freed = 0;
	int i = 0;
	for(i = 0; i < NAT_CONNTRACKER_MAXCONNS; i++){
		struct list_head *pos, *q;
		struct natct* cursor = NULL;
		list_for_each_safe(pos, q, &nat_conntracker_bucket[i]) {
			cursor = list_entry(pos, struct natct, list);
			if(nat_conntracker_hasexpired(cursor) == 1){
				if(cursor->protocol == IPPROTO_TCP || cursor->protocol == IPPROTO_UDP){
					nat_conntracker_releaseport(ntohs(cursor->target_port));
				}

				list_del(pos);
				kfree(cursor);
				freed++;
			}
		}
	}

	for(i = 0; i < NAT_CONNTRACKER_MAXCONNS; i++){
		struct list_head *pos, *q;
		struct natct* cursor = NULL;
		list_for_each_safe(pos, q, &nat_conntracker_bucket_dnat[i]) {
			cursor = list_entry(pos, struct natct, list);
			if(nat_conntracker_hasexpired(cursor) == 1){
				list_del(pos);
				kfree(cursor);
				freed++;
			}
		}
	}

	printk("nat_conntracker_garbagecollector free'd %d connections\n", freed);
}

void nat_tcp_updatechecksum(struct sk_buff* skb, struct iphdr* ip, struct tcphdr* tcp){
	int len = 0;

	ip->check = 0;
	ip->check = checksum((unsigned short *) ip, sizeof (struct iphdr));

	tcp->check = 0;
	len = ntohs(ip->tot_len) - (ip->ihl << 2);
	nf_nat_ipv4_csum_recalc(skb, IPPROTO_TCP, tcp, &tcp->check, len, len);
}

void nat_udp_updatechecksum(struct iphdr* ip, struct udphdr* udp){
	ip->check = 0;
	ip->check = checksum((unsigned short *) ip, sizeof (struct iphdr));
	udp->check = 0;
}

void nat_icmp_updatechecksum(struct iphdr* ip, struct icmphdr* icmp){
	int size = 0;
	ip->check = 0;
	ip->check = checksum((unsigned short *) ip, sizeof (struct iphdr));
	icmp->checksum = 0;
	size = ntohs(ip->tot_len) - sizeof (struct iphdr);
	icmp->checksum = checksum((unsigned short*) icmp, size);
}

void nat_do_translation_snat(struct natct* ct, struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb); 
	if(ip->protocol == IPPROTO_TCP || ip->protocol == IPPROTO_UDP){
		if(direction == SAME){
			if(ip->protocol == IPPROTO_TCP){
				struct tcphdr* tcp = (struct tcphdr *) ((unsigned char*) ip + (ip->ihl << 2));
				ip->saddr = ct->target_address;
				tcp->source = ct->target_port;

				nat_tcp_updatechecksum(entry->skb, ip, tcp);
				return;
			}else if(ip->protocol == IPPROTO_UDP){
				struct udphdr* udp = (struct udphdr*) ((unsigned char*) ip + (ip->ihl << 2));
				ip->saddr = ct->target_address;
				udp->source = ct->target_port;

				nat_udp_updatechecksum(ip, udp);
				return;
			}
		}

		if(direction == DIFF){
			if(ip->protocol == IPPROTO_TCP){
				struct tcphdr* tcp = (struct tcphdr *) ((unsigned char*) ip + (ip->ihl << 2));
				ip->daddr = ct->source_address;
				tcp->dest = ct->source_port;

				nat_tcp_updatechecksum(entry->skb, ip, tcp);
				return;
			}else if(ip->protocol == IPPROTO_UDP){
				struct udphdr* udp = (struct udphdr*) ((unsigned char*) ip + (ip->ihl << 2));
				ip->daddr = ct->source_address;
				udp->dest = ct->source_port;

				nat_udp_updatechecksum(ip, udp);
				return;
			}
		}


	}else if(ip->protocol == IPPROTO_ICMP){
		if(direction == SAME){
			struct icmphdr* icmp = (struct icmphdr*) ((unsigned char*) ip + (ip->ihl << 2));
			ip->saddr = ct->target_address;

			nat_icmp_updatechecksum(ip, icmp);
			return;
		}
		if(direction == DIFF){
			struct icmphdr* icmp = (struct icmphdr*) ((unsigned char*) ip + (ip->ihl << 2));
			ip->daddr = ct->source_address;
			nat_icmp_updatechecksum(ip, icmp);
			return;
		}
	}
}


void nat_do_translation_dnat(struct natct* ct, struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb); 
	if(ip->protocol == IPPROTO_TCP || ip->protocol == IPPROTO_UDP){
		if(direction == SAME){
			if(ip->protocol == IPPROTO_TCP){
				struct tcphdr* tcp = (struct tcphdr *) ((unsigned char*) ip + (ip->ihl << 2));
				ip->daddr = ct->target_address;
				tcp->dest= ct->target_port;

				nat_tcp_updatechecksum(entry->skb, ip, tcp);
				return;
			}else if(ip->protocol == IPPROTO_UDP){
				struct udphdr* udp = (struct udphdr*) ((unsigned char*) ip + (ip->ihl << 2));
				ip->daddr = ct->target_address;
				udp->dest = ct->target_port;

				nat_udp_updatechecksum(ip, udp);
				return;
			}
		}

		if(direction == DIFF){
			if(ip->protocol == IPPROTO_TCP){
				struct tcphdr* tcp = (struct tcphdr *) ((unsigned char*) ip + (ip->ihl << 2));
				ip->saddr = ct->destination_address;
				tcp->source= ct->destination_port;

				nat_tcp_updatechecksum(entry->skb, ip, tcp);
				return;
			}else if(ip->protocol == IPPROTO_UDP){
				struct udphdr* udp = (struct udphdr*) ((unsigned char*) ip + (ip->ihl << 2));
				ip->saddr = ct->destination_address;
				udp->source = ct->destination_port;

				nat_udp_updatechecksum(ip, udp);
				return;
			}
			return;
		}

	}else if(ip->protocol == IPPROTO_ICMP){
		if(direction == SAME){
			struct icmphdr* icmp = (struct icmphdr*) ((unsigned char*) ip + (ip->ihl << 2));
			ip->saddr = ct->target_address;

			nat_icmp_updatechecksum(ip, icmp);
			return;
		}
		if(direction == DIFF){
			struct icmphdr* icmp = (struct icmphdr*) ((unsigned char*) ip + (ip->ihl << 2));
			ip->daddr = ct->source_address;
			nat_icmp_updatechecksum(ip, icmp);
			return;
		}
	}
}


void nat_do_translation(struct natct* ct, struct nf_queue_entry* entry, int direction){
	if (!skb_make_writable(entry->skb, entry->skb->len)){
		printk(KERN_CRIT "nat_do_translation failed, out of memory\n");
		return;
	}

	entry->skb->ip_summed = CHECKSUM_NONE;
	if(ct->type == NAT_DNAT){
		nat_do_translation_dnat(ct, entry, direction);
	}else if(ct->type == NAT_SNAT){
		nat_do_translation_snat(ct, entry, direction);
	}
}

void nat_conntracker_setupports(void){
	int x = 0;
	INIT_LIST_HEAD(&unusedportlist);

	for(x = NAT_SNATID_START; x < NAT_SNATID_END; x++){
		nat_conntracker_releaseport(x);	
	} 
}

int nat_conntracker_findunusedport(void){
	struct unusedport* u = NULL;
	int return_port = -1;

	if(list_empty(&unusedportlist)){
		printk(KERN_CRIT "nat_conntracker_findunusedport failed, no available ports");
		nat_conntracker_garbagecollector(0);
		return -1;
	}

	list_for_each_entry(u, &unusedportlist, list) {
		break;
	}

	if(u){
		list_del(&u->list);
		return_port = u->port;
		kfree(u);
	}

	return return_port;
}

void nat_conntracker_releaseport(int port){
	struct unusedport* u = (struct unusedport*) kmalloc(sizeof(struct unusedport), GFP_ATOMIC);
	u->port = port;

	list_add_tail(&u->list, &unusedportlist);
}

struct natct* nat_conntracker_create(struct nf_queue_entry* entry, struct message* m, int type){
	struct natct* ct = (struct natct*) kmalloc(sizeof(struct natct),  GFP_ATOMIC);
	ct->type = type;
	ct->target_address = htonl(m->nat.target_address);
	ct->target_port = htons(m->nat.target_port);
	ct->timestamp = currenttimestamp();
	ct->fin= 0;

	ct->source_address = ip_hdr(entry->skb)->saddr;
	ct->destination_address = ip_hdr(entry->skb)->daddr;

	switch(ip_hdr(entry->skb)->protocol){
		case IPPROTO_TCP:{
					 ct->source_port = tcphdr_ptr(entry->skb)->source;
					 ct->destination_port = tcphdr_ptr(entry->skb)->dest;
					 ct->protocol = IPPROTO_TCP;
					 ct->state = TCPUP;
					 ct->target_port = htons(nat_conntracker_findunusedport());
					 break;
				 };

		case IPPROTO_UDP:{
					 ct->source_port = udphdr_ptr(entry->skb)->source;
					 ct->destination_port = udphdr_ptr(entry->skb)->dest;
					 ct->protocol = IPPROTO_UDP;
					 ct->target_port = htons(nat_conntracker_findunusedport());
					 break;
				 };

		case IPPROTO_ICMP:{
					  ct->icmp_id = icmphdr_ptr(entry->skb)->un.echo.id;
					  ct->protocol = IPPROTO_ICMP;
					  break;
				  };
		default:
				  return NULL;
	}

	if(type == NAT_SNAT){
		ct->hash = nat_conntracker_hash(entry, type, SAME);
		list_add_tail(&ct->list, &nat_conntracker_bucket[ct->hash]);
		return ct;
	}else if (type == NAT_DNAT){
		ct->hash = nat_conntracker_hash(entry, type, SAME);
		list_add_tail(&ct->list, &nat_conntracker_bucket_dnat[ct->hash]);
		return ct;
	}
	printk("nat_conntracker_create failed to add bucket\n");
	return NULL;
}

