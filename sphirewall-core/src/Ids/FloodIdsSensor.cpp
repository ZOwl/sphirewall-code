/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;

#include "Ids/Ids.h"
#include "Ids/FloodIdsSensor.h"
#include "Core/Event.h"
#include "Core/SysMonitor.h"
#include "Core/System.h"

void FloodSensor::handle(SFwallCore::Packet* ignore){
	//SysMonitor* monitor = System::getInstance()->sysMonitor;
	/*if(monitor->last != NULL){
		int averageTcpConns = monitor->average("firewall.connectiontracker.tcp.size.all");
		int lastTcpConns = monitor->last->items["firewall.connectiontracker.tcp.size.all"];

		if(averageTcpConns != 0 && lastTcpConns != 0){	
			if((lastTcpConns / averageTcpConns) > THRESHOLD){
				alert(0, 0);			
			}		
		}
	}*/
}

std::string FloodSensor::type() {
	return IDS_FLOOD_ATTACK;
}

