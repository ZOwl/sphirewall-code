/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gcrypt.h>
#include <iostream>
#include <fstream>
#include <string>

#ifndef REQ_VER
#define REQ_VER "1.0.0" // minimum libgcrypt version required
#endif
#ifndef SPHIREWALL_HASH_FILE_INCLUDED
#define SPHIREWALL_HASH_FILE_INCLUDED

class Hash {
public:
	std::string create_hash(std::string input, int unhashed_len);
	bool check_hash(std::string check, std::string compare);
};
#endif
