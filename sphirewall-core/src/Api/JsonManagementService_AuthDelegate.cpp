/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
using namespace std;

#include "Core/HostDiscoveryService.h"
#include "Api/JsonManagementService.h"
#include "Json/JSON.h"
#include "Utils/StringUtils.h"
#include "Core/System.h"
#include "Core/Event.h"
#include "Auth/AuthenticationHandler.h"
#include "Utils/Hash.h"

JSONObject AuthDelegate::process(string uri, JSONObject args) {
	if (uri.compare("auth/groups/list") == 0) {
		return groups_list(args);
	} else if (uri.compare("auth/groups/create") == 0) {
		return groups_create(args);
	} else if (uri.compare("auth/groups/create/bulk") == 0) {
		return groups_create_bulk(args);
	} else if (uri.compare("auth/groups/get") == 0) {
		return groups_get(args);
	} else if (uri.compare("auth/groups/del") == 0) {
		return groups_del(args);
	} else if(uri.compare("auth/groups/mergeoveruser") == 0){
		return groups_mergeoveruser(args);	
	} else if (uri.compare("auth/groups/save") == 0) {
		return groups_save(args);
	} else if (uri.compare("auth/users/list") == 0) {
		return users_list(args);
	} else if (uri.compare("auth/users/add") == 0) {
		return users_add(args);
	} else if (uri.compare("auth/users/del") == 0) {
		return users_del(args);
	} else if (uri.compare("auth/users/get") == 0) {
		return users_get(args);
	} else if (uri.compare("auth/users/save") == 0) {
		return users_save(args);
	} else if (uri.compare("auth/users/setpassword") == 0) {
		return user_setpassword(args);
	} else if (uri.compare("auth/users/groups/add") == 0) {
		return users_groups_add(args);
	} else if (uri.compare("auth/users/groups/del") == 0) {
		return users_groups_del(args);
	} else if (uri.compare("auth/users/disable") == 0) {
		return users_disable(args);
	} else if (uri.compare("auth/users/enable") == 0) {
		return users_enable(args);
	} else if (uri.compare("auth/sessions/list") == 0) {
		return sessions_list(args);
	} else if (uri.compare("auth/login") == 0) {
		return login(args);
	} else if (uri.compare("auth/logout") == 0) {
		return logout(args);
	}else if (uri.compare("auth/users/defaultpasswordset") == 0){
		return defaultpasswordset();
	}else if(uri.compare("auth/createsession") == 0){
		return createsession(args);
	}else if(uri.compare("auth/ldap") == 0){
		return ldap();
	}else if(uri.compare("auth/ldap/sync") == 0){
		return ldap_sync();
	} else {
		throw new DelegateNotFoundException(uri);
	}
}

JSONObject AuthDelegate::ldap_sync(){
	AuthenticationMethod* method = authManager->getMethod(LDAP_DB);
	if(method->canConnect()){
		method->sync(userDb, groupDb);
	}	
	return JSONObject();
}

JSONObject AuthDelegate::ldap(){
	AuthenticationMethod* method = authManager->getMethod(LDAP_DB);
	JSONObject ret;
	ret.put(L"online", new JSONValue((bool) method->canConnect()));
	return ret;
}

JSONObject AuthDelegate::groups_create_bulk(JSONObject args){
	JSONArray values = args[L"values"]->AsArray();
	for(int x= 0; x < values.size(); x++){
		JSONObject insert;
		insert.put(L"name", new JSONValue(values[x]->String()));
		groups_create(insert);
	}

	return JSONObject();
}

JSONObject AuthDelegate::groups_mergeoveruser(JSONObject args){
	//First remove the user from all groups:
	User* target = userDb->getUser(args[L"username"]->String());
	if(!target){
		target = userDb->createUser(args[L"username"]->String());
	}

	vector<Group*> groups = target->getGroups();
	for(int x= 0; x < groups.size(); x++){
		Group* toDelete = groups[x];
		target->removeGroup(toDelete);
	}

	JSONArray groupNames = args[L"groups"]->AsArray();
	for(int x =0; x < groupNames.size(); x++){
		Group* targetGroup = groupDb->getGroup(groupNames[x]->String());
		if(!targetGroup){
			targetGroup = groupDb->createGroup(groupNames[x]->String());
		}			
		target->addGroup(targetGroup);
	}		

	groupDb->save();
	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::defaultpasswordset(){
	JSONObject ret;

	User* targetUser = userDb->getUser("admin");
	if(targetUser != NULL){
		Hash hash;
		if(targetUser->getPassword().compare(hash.create_hash("admin", 5)) != -1){
			ret.put(L"value", new JSONValue((bool) true));
		}else{
			ret.put(L"value", new JSONValue((bool) false));
		}
	}else{
		ret.put(L"value", new JSONValue((bool) false));
	}

	return ret;	
}

JSONObject AuthDelegate::groups_list(JSONObject args) {
	JSONObject ret;
	JSONArray groups;
	vector<Group*> intGroups = groupDb->list();
	for (int x = 0; x < intGroups.size(); x++) {
		Group* target = intGroups[x];
		JSONObject group;
		group.put(L"id", new JSONValue((double) target->getId()));
		group.put(L"name", new JSONValue((string) target->getName()));
		group.put(L"desc", new JSONValue((string) target->getDesc()));
		group.put(L"manager", new JSONValue((string) target->getManager()));
		group.put(L"allowMui", new JSONValue((bool) target->isAllowMui()));

		//Quota information:
		QuotaInfo* quotas = target->getQuota();
		group.put(L"dailyQuota", new JSONValue((bool) quotas->dailyQuota));
		group.put(L"dailyQuotaLimit", new JSONValue((double) quotas->dailyQuotaLimit));

		group.put(L"weeklyQuota", new JSONValue((bool) quotas->weeklyQuota));
		group.put(L"weeklyQuotaLimit", new JSONValue((double) quotas->weeklyQuotaLimit));

		group.put(L"monthQuota", new JSONValue((bool) quotas->monthQuota));
		group.put(L"monthQuotaIsSmart", new JSONValue((bool) quotas->monthQuotaIsSmart));
		group.put(L"monthQuotaLimit", new JSONValue((double) quotas->monthQuotaLimit));
		group.put(L"monthQuotaBillingDay", new JSONValue((double) quotas->monthQuotaBillingDay));

		group.put(L"totalQuota", new JSONValue((bool) quotas->totalQuota));
		group.put(L"totalQuotaLimit", new JSONValue((double) quotas->totalQuotaLimit));

		group.put(L"timeQuota", new JSONValue((bool) quotas->timeQuota));
		group.put(L"timeQuotaLimit", new JSONValue((double) quotas->timeQuotaLimit));

		groups.push_back(new JSONValue(group));
	}

	ret.put(L"groups", new JSONValue(groups));
	return ret;
}

JSONObject AuthDelegate::groups_create(JSONObject args) {
	if(!groupDb->getGroup(args[L"name"]->String())){
		groupDb->createGroup(args[L"name"]->String());
		if(eventDb){
			EventParams params;
			params["name"] = args[L"name"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_GROUPDB_ADD, params));
		}

		groupDb->save();
	}
	return JSONObject();
}

JSONObject AuthDelegate::groups_get(JSONObject args) {
	Group* target = groupDb->getGroup(args[L"id"]->AsNumber());
	if (target) {
		JSONObject group;
		group.put(L"id", new JSONValue((double) target->getId()));
		group.put(L"name", new JSONValue((string) target->getName()));
		group.put(L"desc", new JSONValue((string) target->getDesc()));
		group.put(L"manager", new JSONValue((string) target->getManager()));
		group.put(L"allowMui", new JSONValue((bool) target->isAllowMui()));

		QuotaInfo* quotas = target->getQuota();
		group.put(L"dailyQuota", new JSONValue((bool) quotas->dailyQuota));
		group.put(L"dailyQuotaLimit", new JSONValue((double) quotas->dailyQuotaLimit));

		group.put(L"weeklyQuota", new JSONValue((bool) quotas->weeklyQuota));
		group.put(L"weeklyQuotaLimit", new JSONValue((double) quotas->weeklyQuotaLimit));

		group.put(L"monthQuota", new JSONValue((bool) quotas->monthQuota));
		group.put(L"monthQuotaIsSmart", new JSONValue((bool) quotas->monthQuotaIsSmart));
		group.put(L"monthQuotaLimit", new JSONValue((double) quotas->monthQuotaLimit));
		group.put(L"monthQuotaBillingDay", new JSONValue((double) quotas->monthQuotaBillingDay));

		group.put(L"totalQuota", new JSONValue((bool) quotas->totalQuota));
		group.put(L"totalQuotaLimit", new JSONValue((double) quotas->totalQuotaLimit));

		group.put(L"timeQuota", new JSONValue((bool) quotas->timeQuota));
		group.put(L"timeQuotaLimit", new JSONValue((double) quotas->timeQuotaLimit));

		return group;
	}

	return JSONObject();
}

JSONObject AuthDelegate::groups_del(JSONObject args) {
	Group* target = groupDb->getGroup(args[L"id"]->AsNumber());
	if(target){
		if(eventDb){
			EventParams params;
			params["group"] = target->getName(); 
			eventDb->add(new Event(AUDIT_CONFIGURATION_GROUPDB_DEL, params));
		}
		groupDb->delGroup(target);
	}
	return JSONObject();
}

JSONObject AuthDelegate::groups_save(JSONObject args) {
	Group* target = groupDb->getGroup(args[L"id"]->AsNumber());
	if (target) {
		if(args.has(L"desc")){
			target->setDesc(args[L"desc"]->String());
		}

		if(args.has(L"manager")){
			target->setManager(args[L"manager"]->String());
		}

		if(args.has(L"mui")){
			target->setAllowMui(args[L"mui"]->AsBool());
		}

		QuotaInfo* quota = target->getQuota();
		if(args.has(L"dailyQuota")){
			quota->dailyQuota = args[L"dailyQuota"]->AsBool();
			quota->dailyQuotaLimit = args[L"dailyQuotaLimit"]->AsNumber();
		}

		if(args.has(L"weeklyQuota")){
			quota->weeklyQuota= args[L"weeklyQuota"]->AsBool();
			quota->weeklyQuotaLimit = args[L"weeklyQuotaLimit"]->AsNumber();
		}

		if(args.has(L"monthQuota")){
			quota->monthQuota = args[L"monthQuota"]->AsBool();
			quota->monthQuotaIsSmart = args[L"monthQuotaIsSmart"]->AsBool();
			quota->monthQuotaLimit = args[L"monthQuotaLimit"]->AsNumber();
			quota->monthQuotaBillingDay = args[L"monthQuotaBillingDay"]->AsNumber();
		}

		if(args.has(L"totalQuota")){
			quota->totalQuota = args[L"totalQuota"]->AsBool();
			quota->totalQuotaLimit = args[L"totalQuotaLimit"]->AsNumber();
		}

		if(args.has(L"timeQuota")){
			quota->timeQuota= args[L"timeQuota"]->AsBool();
			quota->timeQuotaLimit = args[L"timeQuotaLimit"]->AsNumber();
		}
		if(eventDb){
			EventParams params; 
			params["id"] = args[L"id"]->AsNumber();
			eventDb->add(new Event(AUDIT_CONFIGURATION_GROUPDB_MODIFIED, params));
		}
	}

	groupDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_list(JSONObject args) {
	JSONObject ret;
	JSONArray users;

	vector<string> userList = userDb->list();

	for (int x = 0; x < userList.size(); x++) {
		User* target = userDb->getUser(userList[x]);
		if (target) {
			JSONObject user;
			user.put(L"username", new JSONValue((string) target->getUserName()));
			user.put(L"fname", new JSONValue((string) target->getFname()));
			user.put(L"lname", new JSONValue((string) target->getLname()));
			user.put(L"email", new JSONValue((string) target->getEmail()));
			user.put(L"lastLogin", new JSONValue((double) target->getLastLogin()));
			user.put(L"enabled", new JSONValue((bool) target->getIsEnabled()));

			//Quota information:
			QuotaInfo* quotas = target->getQuota();
			user.put(L"dailyQuota", new JSONValue((bool) quotas->dailyQuota));
			user.put(L"dailyQuotaLimit", new JSONValue((double) quotas->dailyQuotaLimit));

			user.put(L"weeklyQuota", new JSONValue((bool) quotas->weeklyQuota));
			user.put(L"weeklyQuotaLimit", new JSONValue((double) quotas->weeklyQuotaLimit));

			user.put(L"monthQuota", new JSONValue((bool) quotas->monthQuota));
			user.put(L"monthQuotaIsSmart", new JSONValue((bool) quotas->monthQuotaIsSmart));
			user.put(L"monthQuotaLimit", new JSONValue((double) quotas->monthQuotaLimit));
			user.put(L"monthQuotaBillingDay", new JSONValue((double) quotas->monthQuotaBillingDay));

			user.put(L"totalQuota", new JSONValue((bool) quotas->totalQuota));
			user.put(L"totalQuotaLimit", new JSONValue((double) quotas->totalQuotaLimit));

			user.put(L"timeQuota", new JSONValue((bool) quotas->timeQuota));
			user.put(L"timeQuotaLimit", new JSONValue((double) quotas->timeQuotaLimit));

			JSONArray groupsArray;
			vector<Group*> groups = target->getGroups();
			for (int y = 0; y < groups.size(); y++) {
				JSONObject jsonGroup;
				jsonGroup.put(L"id", new JSONValue((double) groups[y]->getId()));
				jsonGroup.put(L"name", new JSONValue((string) groups[y]->getName()));
				jsonGroup.put(L"desc", new JSONValue((string) groups[y]->getDesc()));

				groupsArray.push_back(new JSONValue(jsonGroup));
			}

			user.put(L"groups", new JSONValue(groupsArray));
			users.push_back(new JSONValue(user));
		}
	}

	ret.put(L"users", new JSONValue(users));
	return ret;
}

JSONObject AuthDelegate::users_add(JSONObject args) {

	if (StringUtils::trim(args[L"username"]->String()) != "") {
		userDb->createUser(args[L"username"]->String());

		if(eventDb){
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_ADD, params));
		}
	}

	return JSONObject();
}

JSONObject AuthDelegate::users_del(JSONObject args) {
	User* user = userDb->getUser(args[L"username"]->String());

	if (user) {
		userDb->delUser(user);

		if(eventDb){
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_DEL, params));
		}
	}

	userDb->save();

	return JSONObject();
}

JSONObject AuthDelegate::users_get(JSONObject args) {
	User* target = userDb->getUser(args[L"username"]->String());
	if (target) {
		JSONObject user;
		user.put(L"username", new JSONValue((string) target->getUserName()));
		user.put(L"fname", new JSONValue((string) target->getFname()));
		user.put(L"lname", new JSONValue((string) target->getLname()));
		user.put(L"email", new JSONValue((string) target->getEmail()));
		user.put(L"lastLogin", new JSONValue((double) target->getLastLogin()));
		user.put(L"enabled", new JSONValue((bool) target->getIsEnabled()));
		user.put(L"authenticationType", new JSONValue((double) target->getAuthenticationType()));

		QuotaInfo* quotas = target->getQuota();
		user.put(L"dailyQuota", new JSONValue((bool) quotas->dailyQuota));
		user.put(L"dailyQuotaLimit", new JSONValue((double) quotas->dailyQuotaLimit));

		user.put(L"weeklyQuota", new JSONValue((bool) quotas->weeklyQuota));
		user.put(L"weeklyQuotaLimit", new JSONValue((double) quotas->weeklyQuotaLimit));

		user.put(L"monthQuota", new JSONValue((bool) quotas->monthQuota));
		user.put(L"monthQuotaIsSmart", new JSONValue((bool) quotas->monthQuotaIsSmart));
		user.put(L"monthQuotaLimit", new JSONValue((double) quotas->monthQuotaLimit));
		user.put(L"monthQuotaBillingDay", new JSONValue((double) quotas->monthQuotaBillingDay));

		user.put(L"totalQuota", new JSONValue((bool) quotas->totalQuota));
		user.put(L"totalQuotaLimit", new JSONValue((double) quotas->totalQuotaLimit));

		user.put(L"timeQuota", new JSONValue((bool) quotas->timeQuota));
		user.put(L"timeQuotaLimit", new JSONValue((double) quotas->timeQuotaLimit));


		JSONArray groupsArray;
		vector<Group*> groups = target->getGroups();
		for (int y = 0; y < groups.size(); y++) {
			JSONObject jsonGroup;
			jsonGroup.put(L"id", new JSONValue((double) groups[y]->getId()));
			jsonGroup.put(L"name", new JSONValue((string) groups[y]->getName()));
			jsonGroup.put(L"desc", new JSONValue((string) groups[y]->getDesc()));

			groupsArray.push_back(new JSONValue(jsonGroup));
		}

		user.put(L"groups", new JSONValue(groupsArray));

		return user;
	}else{
		throw new DelegateGeneralException("Could not find user");
	}
}

JSONObject AuthDelegate::users_save(JSONObject args) {
	User* target = userDb->getUser(args[L"username"]->String());
	if (target) {
		if (args.has(L"fname")) {
			target->setFname(args[L"fname"]->String());
		}

		if (args.has(L"lname")) {
			target->setLname(args[L"lname"]->String());
		}

		if (args.has(L"email")) {
			target->setEmail(args[L"email"]->String());
		}

		QuotaInfo* quota = target->getQuota();

		if(args.has(L"dailyQuota")){
			quota->dailyQuota = args[L"dailyQuota"]->AsBool();
			quota->dailyQuotaLimit = args[L"dailyQuotaLimit"]->AsNumber();
		}

		if(args.has(L"weeklyQuota")){
			quota->weeklyQuota= args[L"weeklyQuota"]->AsBool();
			quota->weeklyQuotaLimit = args[L"weeklyQuotaLimit"]->AsNumber();
		}

		if(args.has(L"monthQuota")){
			quota->monthQuota = args[L"monthQuota"]->AsBool();

			if (args.has(L"monthQuotaIsSmart")) {
				quota->monthQuotaIsSmart = args[L"monthQuotaIsSmart"]->AsBool();
			}

			quota->monthQuotaLimit = args[L"monthQuotaLimit"]->AsNumber();

			if (args.has(L"monthQuotaBillingDay")) {
				quota->monthQuotaBillingDay = args[L"monthQuotaBillingDay"]->AsNumber();
			}
		} // End if(args.has(L"monthQuota")).

		if(args.has(L"totalQuota")){
			quota->totalQuota = args[L"totalQuota"]->AsBool();
			quota->totalQuotaLimit = args[L"totalQuotaLimit"]->AsNumber();
		}

		if(args.has(L"timeQuota")){
			quota->timeQuota= args[L"timeQuota"]->AsBool();
			quota->timeQuotaLimit = args[L"timeQuotaLimit"]->AsNumber();
		}
		if(eventDb){
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_MODIFIED, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::user_setpassword(JSONObject args) {
	User* target = userDb->getUser(args[L"username"]->String());
	if (target) {
		target->setPassword(args[L"password"]->String());

		if(eventDb){
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_SETPASSWORD, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_groups_add(JSONObject args) {
	User* target = userDb->getUser(args[L"username"]->String());
	Group* group = groupDb->getGroup(args[L"group"]->AsNumber());
	if (target && group) {
		target->addGroup(group);

		if(eventDb){
			EventParams params;
			params["username"] = args[L"username"]->String();
			params["group"] = args[L"group"]->AsNumber();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_GROUPS_ADD, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_groups_del(JSONObject args) {
	User* target = userDb->getUser(args[L"username"]->String());
	Group* group = groupDb->getGroup(args[L"group"]->AsNumber());
	if (target && group) {
		target->removeGroup(group);
		if(eventDb){
			EventParams params;     
			params["username"] = args[L"username"]->String();
			params["group"] = args[L"group"]->AsNumber();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_GROUPS_DEL, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_disable(JSONObject args) {
	User* user = userDb->getUser(args[L"username"]->String());
	if (user) {
		userDb->disableUser(user);
		if(eventDb){
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_DISABLE, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_enable(JSONObject args) {
	User* user = userDb->getUser(args[L"username"]->String());
	if (user) {
		userDb->enableUser(user);
		if(eventDb){
			EventParams params;     
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_ENABLE, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::sessions_list(JSONObject args) {
	JSONObject ret;
	JSONArray sessions;

	SessionDb* db = sessionDb;
	db->holdLock();	
	vector<Session*> sessions_orig = db->list();
	for (int x = 0; x < sessions_orig.size(); x++) {
		Session* session = sessions_orig[x];
		JSONObject s;
		s.put(L"user", new JSONValue((string) session->getUserName()));
		s.put(L"hw", new JSONValue((string) session->getMac()));
		s.put(L"loginTime", new JSONValue((double) session->getStartTime()));
		s.put(L"host", new JSONValue((string) session->getIp()));

		sessions.push_back(new JSONValue(s));
	}
	db->releaseLock();
	ret.put(L"sessions", new JSONValue(sessions));
	return ret;
}

JSONObject AuthDelegate::createsession(JSONObject input) {
	string ipAddress = input[L"ipaddress"]->String();
	string username = input[L"username"]->String();

        string macAddress = "";
        if(input.has(L"mac")){
                macAddress = input[L"mac"]->String();
        }else{
                macAddress = arp->get(ipAddress);
        }

	if(macAddress.size() == 0){
		throw new DelegateGeneralException("Could not find mac address for given ip address");
	}

	User* user = authManager->resolveUser(username);
	if(!user){
		throw new DelegateGeneralException("Could not find user for the given username");
	}

	if(eventDb){
		EventParams params;     
		params["username"] = username; 
		params["ipaddress"] = ipAddress;
		params["mac"] = macAddress;
		eventDb->add(new Event(USERDB_SESSION_CREATE, params));
	}

	sessionDb->holdLock();
	//Check for an existing session
	Session* session = sessionDb->get(macAddress, ipAddress);
	if(session){
		if(session->getUser() == user){
			sessionDb->releaseLock();
			return JSONObject();
			//Do nothing,
		}else{
			sessionDb->deleteSession(session);	
		}
	}
	
        arp->update(IP4Addr::stringToIP4Addr(ipAddress), macAddress);
	sessionDb->create(user, macAddress, ipAddress); 
	sessionDb->releaseLock();
	return JSONObject();
}

JSONObject AuthDelegate::login(JSONObject input) {
	string ipAddress = input[L"ipaddress"]->String();
	string username = input[L"username"]->String();
	string password;

	if(input.has(L"password")){
		password = input[L"password"]->String();
	}

	JSONObject ret;
	string macAddress = arp->get(ipAddress);
	bool authenticated = authManager->authenticate(username, password);
	if (authenticated && macAddress.size() > 0) {
		sessionDb->holdLock();
		if (sessionDb->get(macAddress, ipAddress) == NULL) {
			sessionDb->create(userDb->getUser(username), macAddress, ipAddress); //If the session doesnt exist we save the new one

			ret.put(L"response", new JSONValue((double) 0));

			if(eventDb != NULL){
				EventParams params; params["user"] = username; params["ip"] = ipAddress; params["hw"] = macAddress;
				eventDb->add(new Event(USERDB_LOGIN_SUCCESS,params));
			}
		} else {
			ret.put(L"response", new JSONValue((double) -5));
			ret.put(L"message", new JSONValue((string) "Session already exists for mac address"));
		}
		sessionDb->releaseLock();
	} else {
		if(eventDb != NULL){
			EventParams params; params["user"] = username; params["ip"] = ipAddress; params["hw"] = macAddress;
			eventDb->add(new Event(USERDB_LOGIN_FAILED,params));
		}
		ret.put(L"response", new JSONValue((double) - 1));
	}
	return ret;
}

JSONObject AuthDelegate::logout(JSONObject input) {
	JSONObject ret;
	string ipAddress = input[L"ipaddress"]->String();

	string mac = arp->get(ipAddress);
	Session* session = sessionDb->get(mac, ipAddress);
	if (session != NULL) {
		ret.put(L"response", new JSONValue((double) 0));

		if(eventDb){
			EventParams params; params["user"] = session->getUserName(); params["ipaddress"] = ipAddress;
			eventDb->add(new Event(USERDB_LOGOUT,params));
		}

		sessionDb->holdLock();
		sessionDb->deleteSession(session);
		sessionDb->releaseLock();
	} else {
		ret.put(L"response", new JSONValue((double) - 1));
		ret.put(L"message", new JSONValue((string) "Could not find session"));
	}
	return ret;
}

