/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MONITORING_H
#define MONITORING_H

#include "Core/Logger.h"
#include "Utils/Utils.h"
#include "Common/sphirewall_bandwidth_types.h"
#include "Database/BaseDb.h"

class Time;
class MonitoringDb : public BaseDb {
public:

	MonitoringDb(std::string dbFileName, SLogger::LogContext* logger) :
	dbName(dbFileName), logger(logger) {
	}

	int prepareStatements();
	void createTables();
	bool initDb();
	void createPrepStatements();

	void cleanupMetrics();
	void addMetric(std::string key, int value);
	int getOrCreateKey(std::string key);
	void getMetrics(std::vector<std::pair<std::string, int> >& items, std::string key, Time start, Time finish);
	void getAllMetricsTotals(std::vector<std::pair<std::string, int> >& items,
        Time start, Time finish);
	void updateMetricsHour(std::string);
	std::list<std::string> availableMetrics();	

	sqlite3* connection() {
		return dbConn;
	}
private:
	sqlite3 *dbConn;
	std::string dbName;
	SLogger::LogContext* logger;
};

#endif
