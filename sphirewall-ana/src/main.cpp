/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <signal.h>
#include <execinfo.h>
using namespace std;

#include "Api/RequestService.h"
#include "Api/RequestHandler.h"
#include "Database/BandwidthDb.h"
#include "Core/Logger.h"
#include "Utils/IP4Addr.h"

void crash_handler(int){
	SLogger::LogContext* context = new SLogger::LogContext(SLogger::DEBUG, "analytics", "DEFAULT");
	
	void *array[10];
	size_t size;
	char **strings;
	size_t i;

	size = backtrace (array, 10);
	strings = backtrace_symbols (array, size);

	context->log(SLogger::CRITICAL_ERROR, "(void) sighandler()", "SIGSEGV or SIGTERM caught", true);
	for (i = 0; i < size; i++){
		context->log(SLogger::CRITICAL_ERROR, "(void) sighandler()", strings[i]);
	}
	
	context->flush();
	free (strings);
	exit(-1);
}

int main(int argc, char* argv[]) {
	signal(SIGABRT, crash_handler);
	signal(SIGSEGV, crash_handler);

	int port = 8002;
	std::string file = "/tmp/bwDb";

	cout << "sphirewall_ana 0.9.9.7\n";
	if (argc == 3) {
		port = atoi(argv[1]);
		file = argv[2];
	} else {
		cout << "+ starting with default properties\n";
	}

	cout << "+ listening for incomming requests on port " << port << endl;
	cout << "+ reading/writing to database file " << file << endl;

	SLogger::Logger* logger = new SLogger::Logger();
	SLogger::LogContext* context = new SLogger::LogContext(SLogger::INFO, "analytics", "DEFAULT");

	logger->registerContext(context);
	logger->start();
	context->log(SLogger::INFO, "Starting sphirewall_analytics engine");

	BandwidthDb* bwDb = new BandwidthDb(file, context);

	RequestHandler* h = new RequestHandler();
	h->setBandwidthDb(bwDb);
	h->setLogger(context);
	RequestService* service = new RequestService(port, h);

	service->run();
	return 0;
}
