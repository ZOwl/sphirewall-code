#Copyright Michael Lawson, John Deal
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import subprocess
import os
import sys

cliProcess   = None
testFile     = None
testsPassed  = 0
testsFailed  = 0
startOut     = 0
endOut       = 0
verbose      = False
devices      = None
aclRules     = None

####
# Class definition for device.
####
class Device:
    def __init__(self, devString):
        """Constructs a single device information object."""
        self.devString = devString
        devAttributes  = self.devString.split('|')

        # Note: splitting on prettytable borders.
        # Null strings at beginning and end must be accounted for.

        if len(devAttributes) != 8:
            print('Unable to decode device: ' + devString)
            print('Expected 6 attributes, received ' + \
                str(len(devAttributes) - 2) + '.')

            if verbose:
                for attribute in devAttributes:
                    print str(attribute)

        self.name    = devAttributes[1].strip()
        self.ipAddr  = devAttributes[2].strip()
        self.mask    = devAttributes[3].strip()
        self.network = devAttributes[4].strip()
        self.state   = devAttributes[5].strip()
        self.aliases = devAttributes[6].strip()


    def getDevString(self):
        """Returns complete device string."""
        return self.devString

    def getName(self):
        """Returns device name."""
        return self.name

    def getIPAddr(self):
        """Returns device IP address."""
        return self.ipAddr

    def setIPAddr(self, ip):
        """Sets ip address."""
        self.ipAddr = ip
        return

    def getMask(self):
        """Returns device network mask."""
        return self.mask

    def setMask(self, mask):
        """Set mask."""
        self.mask = mask
        return

    def getNetwork(self):
        """Get network."""
        return self.network

    def setNetwork(self, network):
        """Set network."""
        self.network = network
        return

    def getState(self):
        """Returns device state."""
        return self.state

    def setState(self, state):
        """Set device state."""
        self.state = state
        return

    def getAliases(self):
        """Returns aliases."""
        return self.aliases

    def setAliases(self, aliases):
        """Set aliases."""
        self.aliases= aliases
        return

####
# Class definition for loading ACLs.
####
class AclRule:
    def __init__(self, aclText):
        """Initialize AclRule object."""

        workText       = aclText
        self.aclString = aclText
        workIndex      = aclText.find(":")

        if workIndex < 1:
            print "AclRule.__init__(): Error.  Could not find position for rule:" \
                + aclText
            return

        try:
            self.position  = int(aclText[0:workIndex])
        except:
            print "AclRule.__init))(): Error.  Position not an integer for rule: " \
                + aclText
            return

        # Remove position part and leading '(' of rule ID.
        aclText   = aclText[workIndex + 1:]
        workIndex = aclText.find(")")

        if workIndex < 1:
            print "AclRule.__init__(): Error.  Could not find id for rule:" \
                + self.aclString
            return

        self.id   = aclText[1:workIndex]


    def toString(self):
        """Return formatted list as a string."""
        global verbose

        workString =  "Pos: " + str(self.getPosition()) + " id: " + \
                self.getId() + "\nstring: " + self.getAclString()

        return workString
        
    def getPosition(self):
        return self.position

    def setPosition(self, pos):
        self.position = int(pos)

    def getId(self):
        return self.id

    def setId(self, id):
        self.id = id

    def getAclString(self):
        # Must return string represting the current position.
        workIndex = self.aclString.find(":")
        workStr   = str(self.getPosition()) + self.aclString[workIndex:]
        return workStr

    def setAclString(self, acl):
        self.aclString = acl 

def moveAclValidate(pos):
    if aclRules == None:
        print "aclRules not initialized.  Move failed.l"
        return False
    else:
        if pos < 0 or pos >= len(aclRules):
            print "Invalid rule position " + str(pos) + ". Move failed."
            return False
        else:
            return True

def moveAclUp(pos):
    """Move specified ACL up one position in ACL list."""

    if moveAclValidate(pos) == False:
        return False

    if pos == 0:
        print "Position at begining of rule list.  Move up failed."
        return False

    # Swap positions attributes.
    aclRules[pos].setPosition(pos - 1)
    aclRules[pos - 1].setPosition(pos)

    # Swap positions.
    tempAcl = aclRules[pos - 1]
    aclRules[pos - 1] = aclRules[pos]
    aclRules[pos]     = tempAcl
    return True


def moveAclDown(pos):
    """Move specified ACL down one position in ACL list."""

    if moveAclValidate(pos) == False:
        return False

    if pos == (len(aclRules) - 1):
        print "Position at end of rule list.  Move down failed."
        return False

    # Swap positions attributes.
    aclRules[pos].setPosition(pos + 1)
    aclRules[pos + 1].setPosition(pos)

    # Swap positions.
    tempAcl = aclRules[pos + 1]
    aclRules[pos + 1] = aclRules[pos]
    aclRules[pos]     = tempAcl
    return True

###
# Error routine for malformed out: rec.  Issues warning messange and returns
# "standard" expected record.
####
def malformedOutRec(outRec, cause):
    """Displays malformed expected record error information."""
    print("WARNING: Malformed out record: '" + outRec + "'")
    print("Reason: " + cause + ".")
    return outRec[4:] # Return default record if malformed.

####
# Extracts expected part of record string.
####
def extractExpected(outRec):
    """Extracts expected record information from string."""
    global startOut, endOut # Stupid Python can't do int references.
    result = None

    startOut = 0
    endOut   = len(outRec) - 4

    if outRec[4:13] == "[ignore]:":
        endOut = 0
        return "" # Ignore output so return empty string.

    if outRec[0:8] == "testend:":
        endOut = 8
        return outRec[startOut:endOut] # return terminator.

    if outRec[3] == ':' and outRec[4:11] != "[start=":
        return outRec[4:] # Normal out record beginning with out:.
    else:
        # Only return subset of out record.
        if outRec[4:11] != '[start=':
            return malformedOutRec(outRec, "start= specification")

        endPos = outRec[11:].find("end=")

        if endPos == -1:
            return malformedOutRec(outRec, "end= specification")

        endPos = 11 + endPos - 1

        try:
            start = int(outRec[11:endPos])
        except ValueError:
            return malformedOutRec(outRec, "start= number specification")

        if start == None:
            return malformedOutRec(outRec, "start= number specification")

        endSpec = outRec[endPos + 4:].find("]:")

        if endSpec == -1:
            return malformedOutRec(outRec, "end of range specification")

        endSpec = endSpec + endPos + 4

        try:
            end = int(outRec[(endPos + 5):endSpec])
        except ValueError:
            return malformedOutRec(outRec, "end= number specification")

        if end == None:
            return malformedOutRec(outRec, "end= number specification")

        startOut = start
        endOut   = end
        return outRec[(endSpec + 2):] # Return specified subset of out record


def expandTestRec(testRec):
    """Expand any special test record commands."""
    global aclRules

    workIndex = testRec.find("$ACLID(")

    if workIndex >= 0:
        aclId     = None
        workStr   = ""
        workStr    = testRec[0:workIndex]
        workIndex += 7
        workIndex2 = testRec[workIndex:].find(")")

        if workIndex2 < 1:
            print "Malformed $ACLID command."
            return testRec
        else:
            try:
                workIndex2 += workIndex
                pos = int(testRec[workIndex:workIndex2])
                
                if pos < 0 or pos >= len(aclRules):
                    print "Invalid $ACLID command. No rule at position " \
                        + str(pos)
                    return testRec

                workStr += aclRules[pos].getId() + testRec[workIndex2 + 1:]
            except:
                print "Invalid $ACLID command."
                return testRec

        return workStr
    # End processing of $ACLID command.

    return testRec
# End def expandTestRec(testRec):
                

####
# Reads next Stdout record from client.
####
def readClientStdout():
    """Reads next Stdout record."""
    global cliProcess, verbose

    cliProcess.stdout.flush()
    received = cliProcess.stdout.readline()

    if verbose:
        print("Read:" + received)

    return received

####
# Write to client Stdin.
####
def writeClientStdin(record):
    """Writes record to client Stdin."""
    global cliProcess, verbose

    if verbose:
        print "Record to write: " + record

    cliProcess.stdin.write(record)
    cliProcess.stdin.flush()

    if verbose:
        print("Wrote:" + record)

####
# Process interactive prompt.  Returns True if processed successfully
# and False if not.  Note does not check output after responding to the
# prompt.
####
def processPrompt(inRec):
    """Processes a prompt test record."""
    # Note: Different split character since ':' is used as part of promnpt.
    recParts = inRec.split('|')

    if len(recParts) != 2:
        print("Malformed prompt record: " + "prompt:" + inRec)
        return False

    receivedPrompt = readClientStdout()

    ####
    #  Compare prompt against expected.  Note last two characters of read
    #  prompt control characters and ignored.
    ####
    if receivedPrompt[0:len(receivedPrompt) - 2] != recParts[0]:
        print("Prompt Error: Expected(" + str(len(recParts[0])) + "): " + \
            recParts[0] + " Received(" + str(len(receivedPrompt)) + "): " + \
            receivedPrompt)
        return False

    # Prompt issued now reply with answer.
    writeClientStdin(recParts[1])

    return True

def compare(expected, received):
    """Compare function used by compareOutput()."""

    if expected == received[0:len(expected)]:
        return False # No error.
    else:
        print("TEST FAILED: expected:" + expected)
        print("             got     :" + received)
        return True # There is an error.

####
# Compare output to expected results.
####
def compareOutput(terminator):
    """Compare expected and received test output."""
    global cliProcess, testFile, startOut, endOut, verbose, aclRules

    result   = True
    received = ""
    expected = ""
    temp     = 4
    error    = False

    while True:
        received = testFile.readline()

        if received[0:7] == "prompt:":               
            error = processPrompt(received[7:])
            continue # Finished with prompt processing
        
        #expected = extractExpected(testFile.readline())
        expected = extractExpected(received)

        if verbose:
            print("expected:" + expected)

        if expected[startOut:endOut] == terminator:
            if verbose:
                print("Terminating Test")

            break # Reached end of test.

        if expected[0:11] == "$ALLDEVICES":
            for device in devices: # Compare output with stored device strings.
                expected = device.getDevString()
                received = readClientStdout()

                if verbose:
                    print("expected device: " + expected)
                    print("received device: " + received)

                
                compare(expected, received)

            if error == False: # If no error need to get next expected record.
                expected = extractExpected(testFile.readline())
        # End if expected[0:11] == "$ALLDEVICES"

        if expected[0:8] == "$ALLACLS":
            # It is expected the fixed part of show firewall acls command are
            # specified before and after the $ALLACLS command.

            aclRules.sort(key=lambda x: x.position)

            for acl in aclRules:
                received = readClientStdout()
                expected = acl.getAclString()

                if verbose:
                    print("expected: " + expected)
                    print("received: " + received)

                compare(expected, received)

            expected = extractExpected(testFile.readline())
        # End if expected[0:8] == "$ALLACLS".

        received = readClientStdout()

        if expected != "" and received[startOut:endOut] \
            != expected[startOut:endOut]:
            print("TEST FAILED: expected(" + \
                str(len(expected[startOut:endOut])) + ") '" + \
                expected[startOut:endOut] + "' got(" + \
                str(len(received[startOut:endOut])) + ") '" + \
                received[startOut:endOut] + "'")
            result = False

    return result
        
####
# Starts SCLI subprocess.
####
def startClient(inRec):
    """Starts the client under test."""
    global cliProcess, testFile

    resultString = "Login Test: "
    print(inRec[6:])
    cliProcess = subprocess.Popen(inRec[6:],\
        bufsize = 0, stdin = subprocess.PIPE, stdout = subprocess.PIPE, \
        stderr = subprocess.STDOUT, shell = True)
    result = compareOutput("testend:")

    if result == True:
        print(resultString + "PASSED")
    else: 
        print(resultString + "FAILED")

    return result

####
# Run a test.
####
def runTest(inRec):
    """Run a single command test."""
    global cliProcess, testFile, testsPassed, testsFailed

    resultString = "Test: "

    testRec = None

    while True:
        testRec = testFile.readline()

        if testRec[0] == '#' or testRec[0] == '\n': # Skip comments/blanks.
            continue
        else:
            break # Should be "in:" record.

    if verbose:
        print("testRec:" + testRec)

    if testRec[0:3] != "in:":
        while testRec[0:] != "endtest:":
            testRec = testFile.readline()

        testsFailed += 1
        return malformedOutRec(outRec, "in:  TEST SKIPPED")

    testRec = expandTestRec(testRec)
    writeClientStdin(testRec[3:])
    result  = compareOutput("testend:") # Evaluate output.

    if result == True:
        print(resultString + "PASSED")
        testsPassed += 1
    else: 
        print(resultString + "FAILED")
        testsFailed += 1

    return result

def printUsage():
    """Display program syntax."""
    print("Usage: python cliTest.py [<test file> | -h] | [<test file> [-v]]")

def printHelp():
    """Display program help information."""
    printUsage()
    print("<test file>: File containing test commands.")
    print("-v: Verbose output.  Echos expected and read results.")
    print("-h: Prints help information.")
    print("See: https://sphirewall-all.atlassian.net/wiki/display/SD/Contributors+Guide for more testing documentation.")

####
# Loads device information from show network devices command output.
####
def loadDevices():
    """Loads device information."""
    global devices, verbose

    if verbose:
        print "Loading Devices...."

    devices = []
    writeClientStdin('show network devices\n')

    while True:
        received = readClientStdout()

        if verbose:
            print "received: " + received

        if received[0:11] == 'sphirewall>':
            break # Reached end of show network device output.

        if received[0:1] == '+':
            continue # prettytable border.

        if received[0:7] == '| Name ':
            continue # Device column labels.

        if len(received) < 10: # Hack to ignore formatted blank line at end.
            continue

        # received = received.replace('|', ' ') # Remove perttytable dividers.
        devices.append(Device(received)) # Add device to list.

        if verbose:
            device = devices[len(devices) - 1]
            print("Loading device: " + received + ' Attributes Name:' + \
                device.getName() + ' IPAddr: ' + device.getIPAddr() + \
                ' Mask: ' + device.getMask() + ' State: ' + \
                device.getState())

########
# Uupdate a stored device.
########
def updateDevice(deviceSpec):
    """ Update device ip or mask. """
    global devices, verbose

    devIndex  = None
    devParsed = deviceSpec.split(':')

    if len(devParsed) != 3:
        print("Usage: updatedevice:[name | index]:[ip | mask]:<value>")
        print("Invalid updatedevice specification: " + deviceSpec)
        print("Device update failed.")
        return

    if devParsed[0].isdigit(): # If 1st parameter number use as devices index.
        try:
            devIndex = int(devParsed[0])
        except:
            print("Invalid device index in updatedevice specification: " + \
                deviceSpec)
            print("Device update failed.")
            return

        if devIndex < 0:
            print("Invalid device index in updatedevice specification: " + \
                deviceSpec)
            print("Device update failed.")
            return

        if devIndex > len(devices):
            print("Device index beyond end of device list: " + \
                deviceSpec)
            print("Device update failed.")
            return
    else:
        devIndex = 0

        while devIndex < len(devices):
            if devices[devIndex].name == devParsed[0]:
                break

            devIndex += 1

        if devIndex >= len(devices):
            print("Device name in updatedevice specification not found: " + \
                deviceSpec)
            print("Device update failed.")
            return

    # At this point we have device index to update.
    devParsed[2] = devParsed[2][0:len(devParsed[2]) - 1] # remove line feed

    if devParsed[1] == "ip":
        devices[devIndex].setIPAddr(devParsed[2])
    else:
        if devParsed[1] == "mask":
            devices[devIndex].setMask(devParsed[2])
        else:
            print("Invalid device update type: " + devParsed[1])
            print("Device update failed")

########
# Display stored devices.  This is generally for debugging tests.
########
def displayStoredDevices():
    """ Display stored devices """
    print("Display of stored devices")
    print("-------------------------")

    for device in devices:
        print(device.getName() + " ipaddr:" + device.getIPAddr() + \
            " network:" + device.getMask() + " state:" + \
            device.getState())

    print("-------------------------")

####
# Loads ACL rule information from show firewall acls command output.
####
def loadAclRules():
    """Loads device information."""
    global aclRules, verbose


    if verbose:
        print "loadAclRules(): Start."

    aclRules = []
    writeClientStdin('show firewall acls\n')

    while True:
        received = readClientStdout()

        if received[0:11] == 'sphirewall>':
            break # Reached end of show firewall acls output.

        if len(received) < 10: # Hack to ignore formatted blank line at end.
            continue

        aclRules.append(AclRule(received)) # Add ACL rule to list.

        if verbose:
            rule = aclRules[len(aclRules) - 1]
            print "Loading ACL rule: " + received + ' Attributes position:' + \
                str(rule.getPosition()) + " id: " + str(rule.getId())
            print " aclString: " + rule.getAclString()

    if verbose:
        print "Dump of loaded ACL rules."
        print "-----------------"

        for acl in aclRules:
            print acl.toString()

        print "-----------------"

####
# Main function.
####
print "Sphirewall CLI Test"

if len(sys.argv) < 2 or len(sys.argv) > 3:
    printUsage()
    exit(-1)

if len(sys.argv) == 2:
    if sys.argv[1] == "-h":
        printHelp()
        exit(0)
else:
    if sys.argv[2] == "-v":
        verbose = True
    else:
        printUsage()
        exit(-1)

try:
    testFile = open(sys.argv[1])
except IOError:
    print("Open of test file: " + sys.argv[1] + " failed.")
    exit(-1)

while True:
    inRec = testFile.readline()
 
    if inRec == '':
        break
    else:
        # Parse the input record.
        if inRec[0] == '#' or inRec[0] == '\n':
            pass # Comment or blank line.
        else:
            if inRec[0:5] == "test:":
                print("TEST: " + inRec[5:])
                runTest(inRec)
            else:
                if inRec[0:6] == "start:":
                    print("TEST: ******** LOGIN TEST ********")
                    print("login: " + inRec[6:])

                    if startClient(inRec) == True:
                        testsPassed += 1
                    else:
                        testsFailed += 1
                else:
                    if inRec[0:11] == "getdevices:":
                        loadDevices()
                    else:
                        if inRec[0:13] == "updatedevice:":
                            updateDevice(inRec[13:])
                        else:
                            if inRec[0:15] == "displaydevices:":
                                displayStoredDevices()
                            else:
                                if inRec[0:9] == "loadAcls:":
                                    loadAclRules()
                                else:
                                    if inRec[0:10] == "moveAclUp:":
                                        moveAclUp(inRec[10:])
                                    else:
                                        if inRec[0:12] == "moveAclDown:":
                                            moveAclDown(inRec[12:])
                                        else:
                                            if inRec[0:4] != "end:":
                                                print("Invalid test command: " \
                                                     + inRec)

print("Tests Passed: " + str(testsPassed) + " Tests Failed: " + \
    str(testsFailed) + " Total Tests: " + str((testsPassed + testsFailed)))
exit(0)
