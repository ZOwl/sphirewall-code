#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester

class EventTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester();
        self.tester.connect();
        self.tester.login("fw", "admin", "admin")

    def gotoEventHandlers(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Configure')]").click();
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Event Handling')]").click();

    def test_GeneralConfig(self):
        self.gotoEventHandlers()
        self.tester.select("log")
        self.tester.select("console")
        self.tester.select("quotas")
        self.tester.select("ids")
        self.tester.select("email")
        self.tester.driver.find_element_by_name("emailAddr").clear()
        self.tester.driver.find_element_by_name("emailAddr").send_keys("michael@sphinix.com")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.tester.check_selected("log")
        self.tester.check_selected("console")
        self.tester.check_selected("quotas")
        self.tester.check_selected("ids")
        self.tester.check_selected("email")
        self.assertEquals("michael@sphinix.com", self.tester.driver.find_element_by_name("emailAddr").get_attribute("value"))

    def test_ManualConfiguration(self):
        self.gotoEventHandlers()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Advanced Manual')]").click()
        self.tester.driver.find_element_by_name("eventType").send_keys("event.something")
        self.tester.driver.find_element_by_xpath("//select[@name='handler']/option[text()='Log Event']").click()
        self.tester.driver.find_element_by_id("submit").click()

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), 'event.something')]//ancestor::tr/td[contains(text(), 'Log Event Handler')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), 'event.something')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click();
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td[contains(text(), 'event.something')]//ancestor::tr/td[contains(text(), 'Log Event Handler')]"))

    def test_ManualConfiguration_EmailEventHandler(self):
        self.gotoEventHandlers()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Advanced Manual')]").click()
        self.tester.driver.find_element_by_name("eventType").send_keys("event.something")
        self.tester.driver.find_element_by_xpath("//select[@name='handler']/option[text()='Send email alert']").click()
        self.tester.driver.find_element_by_id("submit").click()

        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), 'event.something')]//ancestor::tr/td/a[contains(text(), 'Manage')]").click();
        self.tester.setValue("email", "email@sphinix.com")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()
        self.assertEqual("email@sphinix.com", self.tester.driver.find_element_by_name("email").get_attribute("value"))
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.gotoEventHandlers()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Advanced Manual')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), 'event.something')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click();

    def test_ManualConfiguration_AddGroupHandler(self):
        self.gotoEventHandlers()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Advanced Manual')]").click()
        self.tester.driver.find_element_by_name("eventType").send_keys("event.something")
        self.tester.driver.find_element_by_xpath("//select[@name='handler']/option[text()='Add user to group']").click()
        self.tester.driver.find_element_by_id("submit").click()

        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), 'event.something')]//ancestor::tr/td/a[contains(text(), 'Manage')]").click();
        self.tester.driver.find_element_by_xpath("//select[@name='groupid']/option[text()='defaultadmin']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertEqual("1", self.tester.driver.find_element_by_name("groupid").get_attribute("value"))
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.gotoEventHandlers()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Advanced Manual')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), 'event.something')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click();

    def test_ManualConfiguration_RemoveGroupHandler(self):
        self.gotoEventHandlers()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Advanced Manual')]").click()
        self.tester.driver.find_element_by_name("eventType").send_keys("event.something")
        self.tester.driver.find_element_by_xpath("//select[@name='handler']/option[text()='Remove user from group']").click()
        self.tester.driver.find_element_by_id("submit").click()

        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), 'event.something')]//ancestor::tr/td/a[contains(text(), 'Manage')]").click();
        self.tester.driver.find_element_by_xpath("//select[@name='groupid']/option[text()='defaultadmin']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertEqual("1", self.tester.driver.find_element_by_name("groupid").get_attribute("value"))
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()
        self.gotoEventHandlers()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Advanced Manual')]").click()

        self.tester.driver.find_element_by_xpath("//table//tr/td[contains(text(), 'event.something')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click();

    def tearDown(self):
        self.tester.disconnect()
