#Copyright Michael Lawson, John Deal
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import re
import getpass
import sys
from Prompt import prompt

inTestMode = False; # Set by Cli.py.

def set_inTestMode(testMode):
    """Set test mode for Set functions."""
    global inTestMode

    inTestMode = testMode
 
# Common function to add quota settings.
#
# idFieldName - name of ID field (ex. "username)
# idFieldType - I = integer, S = string
# input       - string to parse   
def add_quota_common(idFieldName, idFieldType, input):
    """Common parsing and parameter loading for quota add functions."""
    usage = "Usage: {group number} period {TOTAL | DAY | WEEK | MONTH smart {Y | N} billday {billing day number}}  limit {quota limit in MBs} timelimit {hours}"

    try:
        p       = re.compile('(.+) period (.+)')
        m       = p.match(input.strip())        
        
        unit      = m.group(1).strip()        
        period    = m.group(2).strip()
        workIndex = period.find(" ")
        period    = period[0:workIndex]

        updateArg = {}

        if idFieldType == "I":
            updateArg[idFieldName] = int(unit)
        else:
            updateArg[idFieldName] = unit

        if period == "TOTAL":
            updateArg["totalQuota"] = True
            period                  = "totalQuotaLimit"
        else:
            if period == "DAY":
                updateArg["dailyQuota"] = True
                period                  = "dailyQuotaLimit"
            else:
                if period == "WEEK":
                    updateArg["weeklyQuota"] = True
                    period                   = "weeklyQuotaLimit"
                else:
                    if period == "MONTH":
                        updateArg["monthQuota"]  = True
                        period                   = "monthQuotaLimit"

                        workIndex     = input.rfind("smart")
                        workStr       = input[workIndex:]

                        # Note: limit extract used as limit for billday.
                        compiledRex   = \
                            re.compile('smart (.+) billday (.+) limit (.+)')
                        parsedMatches = compiledRex.match(workStr.strip())
                        smart         = parsedMatches.group(1).strip()        

                        if smart == "Y":
                            smart = True
                        else:
                            smart = False

                        updateArg["monthQuotaIsSmart"] = smart
                        billDay                        = \
                            parsedMatches.group(2).strip() 
                        updateArg["monthQuotaBillingDay"] = int(billDay)
                    else:
                        print usage;
                        return

        
        workIndex    = input.find("limit") 
        compiledRex  = re.compile('limit (.+) timelimit (.+)')
        parsedLimits = compiledRex.match(input[workIndex:])        
        limit        = parsedLimits.group(1).strip()
        timeLimit    = parsedLimits.group(2).strip()

        try:
            limit     = int(limit);
            timeLimit = int(timeLimit)
        except:
            print "Ensure 'limit' and 'timelimit' has a numerical values\n" + \
                usage;
            return;
        
        updateArg[period] = int(limit);

        if timeLimit == 0:
            updateArg["timeQuota"] = False
        else:
            updateArg["timeQuota"] = True
            
        updateArg["timeQuotaLimit"] = int(timeLimit)
        return updateArg

    except:
        print usage;
        return;

def delete_quota_common(period):
    """Common quota delete parsing."""

    updateArg = {}

    if period == "DAY":
        updateArg["dailyQuota"]      = False
        updateArg["dailyQuotaLimit"] = 0
    else:
        if period == "WEEK":
            updateArg["weeklyQuota"]      = False
            updateArg["weeklyQuotaLimit"] = 0
        else:
            if period == "MONTH":
                updateArg["monthQuota"]           = False
                updateArg["monthQuotaLimit"]      = 0
                updateArg["monthQuotaIsSmart"]    = False 
                updateArg["monthQuotaBillingDay"] = 1
            else:
                if period == "TOTAL":
                    updateArg["totalQuota"]      = False
                    updateArg["totalQuotaLimit"] = 0
                else:
                    if period == "TIMELIMIT":
                        updateArg["timeQuota"]      = False
                        updateArg["timeQuotaLimit"] = 0

    return updateArg

def set_config_runtime(connectionDetail, input):
    usage = "Syntax error.\nUsage: set config runtime {parameter} = {value}\nUse 'show config system' to obtain a list of valid parameters";
    try:
        if len(input.split("=")) == 2:
            key = input.split("=")[0].strip();
            value = input.split("=")[1].strip();
            print "setting...." + key + " to " + value;            
        else:
            print usage;
            return;
    except:
        print usage;
        return;
    connectionDetail.request("general/runtime/set", {"key": key, "value": int(value)});
    
def set_config_system(connectionDetail, input):
    usage = "Syntax error.\nUsage: set config system {parameter} = {value}\nUse 'show config system' to obtain a list of valid parameters";
    try:
        if len(input.split("=")) == 2:
            key = input.split("=")[0].strip();
            value = input.split("=")[1].strip();

            if key == "session_timeout":
                try:
                    intValue = int(value)
                    value    = intValue
                    dispValue = str(value)
                except:
                    print "Value must be an integer"
                    print usage
                    return
            else:
                dispValue = value
                
            key = "general:" + key
            print "setting...." + key + " to " + dispValue;    
        else:
            print usage;
            return;
    except:
        print usage;
        return;

    connectionDetail.request("general/config/set", { \
        "authenticatedUsername": connectionDetail.username, "key": key, \
        "value": value});

def set_config_loglevel(connectionDetail, input):
    usage = "Syntax error.\nUsage: {parameter} = {level}\nUse 'show config loglevel' to obtain a list of valid parameters";
    try:
        if len(input.split("=")) == 2:
            key = input.split("=")[0].strip();
            value = input.split("=")[1].strip();
            print "setting loglevel for " + key + " to " + value;
        else:
            print usage;
            return;
    except:
        print usage;
        return;
    
    connectionDetail.request("general/logging/set", {"context": key, "level": int(value)});

def set_firewall_acls_add(connectionDetail, input):
    usage = "Syntax error.\nUsage: {accept | reject} --> source \"address {source address}[/{source subnet mask}] [port {port number}] [alias {name}]\" dest \"address {destination address}[/{destination subnet mask}] [port {port number}] [alias {name}]\" options \"protocol {\"\" | tcp | udp | icmp | igmp} groupid {\"\" | group id number} hwaddress {\"\" | MAC address} comment {text}\"";
    try:
        p = re.compile('(.+) --> source (.+) dest (.+) options (.+)');
        m = p.match(input.strip());
    
        source = m.group(2).strip();
        action = m.group(1).strip();
        dest = m.group(3).strip();
    
        #Lets work out the action first then source and destination specs.
        newRule = {};
        determineAclAction(newRule, action);
        determineTargetPartDetails(newRule,"source", source);
        determineTargetPartDetails(newRule,"dest", dest);
    
        try:
            options = m.group(4).strip();
            workOption = matchWithGroups(options, 'protocol ([A-Za-z]+)');

            if not workOption == None:
                newRule["protocol"] = workOption.group(1).upper();
            
            workOption = matchWithGroups(options, 'groupid ([0-9]+)');

            if not workOption == None:
                try:
                    newRule["groupid"] = int(workOption.group(1));
                except ValueError:
                    print "groupid must be a positive integer";
                    return;
            
            workOption = matchWithGroups(options, 'hwAddress ([A-Za-z0-:]+)');

            if not workOption == None:
                newRule["hwAddress"] = workOption.group(1);
            
            workOption = matchWithGroups(options, 'comment (.+)*');

            if not workOption == None:
                newRule["comment"] = workOption.group(1);
        except IndexError:
            pass;
    except IndexError:
        pass; # Do not worryabout optional specifications.
        connectionDetail.request("firewall/acls/add", newRule);
        return;
    except:
        print usage;
        return;

    connectionDetail.request("firewall/acls/add", newRule);

def determineTargetPartDetails(newRule, prefix ,source):
    addressPattern  = matchWithGroups(source, 'address ([0-9,\.]+)/([0-9,\.]+)')
    if addressPattern != None:
        newRule[prefix + "Address"] = addressPattern.group(1);
        newRule[prefix + "Subnet"] = addressPattern.group(2);

    portPattern = matchWithGroups(source, 'port ([0-9]+)')
    if portPattern != None:
        newRule[prefix + "Port"] = int(portPattern.group(1));

    aliasPattern = matchWithGroups(source, 'alias \'(.+)\'')
    if aliasPattern != None:
        newRule[prefix + "Alias"] = aliasPattern.group(1).strip("\'");


def determineAclAction(newRule, action):
    if action == "drop":
        newRule["action"] = 0;
    if action == "accept":
        newRule["action"] = 1;

    masqPatternMatcher  = matchWithGroups(action, 'masquerade (.+)')
    if masqPatternMatcher != None:
        newRule["action"] = 3;
        newRule["masqueradeTarget"] = masqPatternMatcher.group(1);

    forwardPatternMatcher  = matchWithGroups(action, 'forward (.+):(.+)');
    if forwardPatternMatcher != None:
        newRule["action"] = 4;
        newRule["forwardTarget"] = forwardPatternMatcher.group(1);
        newRule["forwardPort"] = int(forwardPatternMatcher.group(2));

    if newRule.has_key("action") == False:
        print "Error, you did not supply an action"


def matchWithGroups(input, expression):
    forwardPattern = re.compile(expression);
    forwardPatternMatcher  = forwardPattern.search(input);

    return forwardPatternMatcher;

def set_firewall_acls_move(connectionDetail, input):
    usage = "Usage: set firewall acls move {rule ID} {up | down}\nUse: 'show firewall acls' command to see rule IDs.\nExample: set firewall acls move 7ffeadef-a111-4f54-a924-d3ce528ea361 up";

    try:
        p = re.compile('(.+) (.+)')
        m = p.match(input.strip())
	
        if m is None:
            print "You must enter a rule number\n" + usage;
            return;
	
        direction = m.group(2).strip().lower();

        if direction not in ("up", "down"):
            print "Direction must be 'up' or 'down'"; + usage
            return
	
        cmdArg       = {}
        cmdArg["id"] = m.group(1).strip()
	
        if direction == "up":
            connectionDetail.request("firewall/acls/filter/up", cmdArg);
        else:
            connectionDetail.request("firewall/acls/filter/down", cmdArg)
    except:
        print "Moving of rule failed."
        print usage

def set_firewall_acls_delete(connectionDetail, input):
    usage = "Usage: set firewall acls delete {rule id}\nUse: 'show firewall acls' command to see rule ids\nExample: set firewall acls delete 7ffeadef-a111-4f54-a924-d3ce528ea361"
    p       = re.compile('(.+)')
    m       = p.match(input.strip())
    
    if m is None:
        print "You must enter a rule ID\n" + usage
        return
        
    cmdArg       = {}
    cmdArg["id"] = m.group(1).strip()
    connectionDetail.request("firewall/acls/filter/delete", cmdArg)

def set_user_add(connectionDetail, input):
    usage = "Syntax error.\nUsage: set user add {username}";
    try:
        p = re.compile('(.+)');
        m = p.match(input.strip());
    
        userName = m.group(1).strip();
    
    except:
        print usage;
        return;

    userNameArg             = {};
    userNameArg["username"] = userName;
    connectionDetail.request("auth/users/add", userNameArg);

def set_user_delete(connectionDetail, input):
    usage = "Syntax error.\nUsage: set user delete {username}";
    try:
        p = re.compile('(.+)');
        m = p.match(input.strip());
    
        userName = m.group(1).strip();
    
    except:
        print usage;
        return;

    userNameArg             = {};
    userNameArg["username"] = userName;
    connectionDetail.request("auth/users/del", userNameArg);

def set_user_update(connectionDetail, input):
    usage = "Syntax error.\nUsage: {username} lname {last name} fname {first name} email {email address}";
    try:
        p = re.compile('(.+) lname (.+) fname (.+) email (.+)');
        m = p.match(input.strip());
    
        userName = m.group(1).strip();
        lName    = m.group(2).strip();
        fName    = m.group(3).strip();
        email    = m.group(4).strip();
    
    except:
        print usage;
        return;

    updateUserArg             = {};
    updateUserArg["username"] = userName;

    if lName != "\"\"":
        updateUserArg["lname"] = lName;

    if fName != "\"\"":
        updateUserArg["fname"] = fName;

    if email != "\"\"":
        updateUserArg["email"] = email;

    connectionDetail.request("auth/users/save", updateUserArg);

def set_user_password(connectionDetail, input):
    global inTestMode

    ####
    #  This function is only used for passowrd prompts during testing.
    #  Reference SP-410.
    ####
    def testPassPrompt(prompt):
        print prompt
        sys.stdout.flush()
        return raw_input()

    usage = "Usage: set user password {username}";

    try:
        p = re.compile('(.+)'); 
        m = p.match(input.strip());
    
        userName      = m.group(1).strip();
        passPrompt    = "Password: "
        reenterPrompt = "Re-enter: "

        try:
            if inTestMode == False:
                password1 = getpass.getpass(passPrompt);
                password2 = getpass.getpass(reenterPrompt);
            else:
                password1 = testPassPrompt(passPrompt)
                password2 = testPassPrompt(reenterPrompt)

            if password1 != password2:
                print "Error: Passwords must match.";
                return;
        except:
            print usage;
            return;
    except:
        print usage;
        return;

    passwordArg             = {};
    passwordArg["username"] = userName;
    passwordArg["password"] = password1;
    connectionDetail.request("auth/users/setpassword", passwordArg);

def set_group_add(connectionDetail, input):
    usage = "Syntax error.\nUsage: set group add [groupname}";
    try:
        p = re.compile('(.+)');
        m = p.match(input.strip());
    
        groupName = m.group(1).strip();
    
    except:
        print usage;
        return;

    groupNameArg         = {};
    groupNameArg["name"] = groupName;
    connectionDetail.request("auth/groups/create", groupNameArg);

def set_group_delete(connectionDetail, input):
    usage = "Syntax error.\nUsage: set group delete {groupId}";
    try:
        p = re.compile('(.+)');
        m = p.match(input.strip());
    
        groupId = m.group(1).strip();
    
    except:
        print usage;
        return;

    try:
        groupIdArg       = {};
        groupIdArg["id"] = float(groupId);
    except:
        print groupId + "not valid integer index";
        return;
    
    connectionDetail.request("auth/groups/del", groupIdArg);

def set_group_update(connectionDetail, input):
    usage = "Syntax error.\nUsage: set group update {groupId} desc {groupDesc} manager {name} mui {0 | 1} (0 = false, 1 = true)";

    try:
        p = re.compile('(.+) desc (.+) manager (.+) mui (.+)');
        m = p.match(input.strip());
    
        groupId      = m.group(1).strip();
        groupDesc    = m.group(2).strip();
        groupManager = m.group(3).strip();
        groupMui     = m.group(4).strip();

        if groupMui != "0" and groupMui != "1":
            print "Syntax error. mui of " + groupMui + " invalid.";
            print usage;
            return;
    except:
        print usage;
        return;

    try:
        groupIdArg            = {};
        groupIdArg["id"]      = float(groupId);
        groupIdArg["desc"]    = groupDesc;
        groupIdArg["manager"] = groupManager;
        groupIdArg["mui"]     = bool(groupMui);
    except:
        print "Syntax Error\nBad parameter(s)";
        print usage;
        return;

    connectionDetail.request("auth/groups/save", groupIdArg);

    
def set_network_device_update(connectionDetail, input):
    usage = "Syntax error.\nUsage: {interface} name {name} ip {ip address} mask {subnet mask} dhcp {0 | 1}";
    try:
        p       = re.compile('(.+) name (.+) ip (.+) mask (.+) dhcp (.+)' )
        m       = p.match(input.strip())

        interface   = m.group(1).strip();
        name        = m.group(2).strip();
        ip          = m.group(3).strip();
        mask        = m.group(4).strip();
        dhcp        = m.group(5).strip();

    except:
        print usage;
        return;

    updateDeviceArg              = {};
    updateDeviceArg["interface"] = interface;

    if name == "\"\"":
        name = "" # Update with blank name.

    updateDeviceArg["name"]      = name

    if ip != "\"\"":
        updateDeviceArg["ip"] = ip;

    if mask != "\"\"":
        updateDeviceArg["mask"] = mask;

    updateDeviceArg["dhcp"] = dhcp

    connectionDetail.request("network/devices/set", updateDeviceArg);
       
def set_network_device_toggle(connectionDetail, input):
    usage = "Syntax error.\nUsage: {interface} {up | down} ";
    try:
        p       = re.compile('(.+) (.+)');
        m       = p.match(input.strip());
    
        interface   = m.group(1).strip();
        toggle      = m.group(2).strip().lower();

    except:
        print usage;
        return;
    
    if toggle not in ("up", "down"):
        print usage;
        return;
    
    #check current state
    device_request = connectionDetail.request("network/devices/get", {"interface":interface});
    state = device_request['state'];
    if state == True:
        state = "up";
    else:
        state = "down";
        
    if state == toggle:
        print "Interface " + interface + " is already " + toggle;
        return;
    else:
        updateDeviceArg                = {};
        updateDeviceArg["interface"]   = interface;
        if toggle == "down":
            userinput = prompt("If you are connected to Sphirewall via " + interface + "; this connection will terminate. Continue?");
            if userinput == True:
                try:
                    try:
                        connectionDetail.request("network/devices/toggle", updateDeviceArg);
                    except:
                        raise;
                        return;
                except:
                    print "Connection terminated"
                    exit();
            else:
                print "Device state wasn't changed"
        
        else:
            connectionDetail.request("network/devices/toggle", updateDeviceArg);
            
def set_network_device_alias_add(connectionDetail, input):
    usage = "Syntax error.\nUsage: {interface} {alias ip} ";
    try:
        p       = re.compile('(.+) (.+)');
        m       = p.match(input.strip());
    
        interface   = m.group(1).strip();
        alias       = m.group(2).strip().lower();
    
    except:
        print usage;
        return;        
    
    updateDeviceArg                = {};
    updateDeviceArg["interface"]   = interface;
    updateDeviceArg["ip"]          = alias;
        
    connectionDetail.request("network/devices/alias/add", updateDeviceArg);
    
def set_network_device_alias_delete(connectionDetail, input):
    usage = "Syntax error.\nUsage: {interface} {alias ip} ";
    try:
        p       = re.compile('(.+) (.+)');
        m       = p.match(input.strip());
    
        interface   = m.group(1).strip();
        alias       = m.group(2).strip().lower();
    
    except:
        print usage;
        return;        
    
    updateDeviceArg                = {};
    updateDeviceArg["interface"]   = interface;
    updateDeviceArg["ip"]          = alias;
        
    connectionDetail.request("network/devices/alias/delete", updateDeviceArg);
    
def set_firewall_qos_add(connectionDetail, input):
    usage = "Usage: srcblock {0 | 1} srcip {SourceIPAddress | - } srcmask {SourceSubnetMask | - } srcport {SourcePort | - } group {Group ID} destblock {0 | 1} destip {DestinationIPAddress | - }  destmask {DestinationSubnetMask | - } destport {DestinationPort | - } upload {Upload Limit (kb/s) | - } download (Download Limit (kb/s) | - )";
    try:
        p = re.compile('srcblock (.+) srcip (.+) srcmask (.+) srcport (.+) group (.+) destblock (.+) destip (.+) destmask (.+) destport (.+) upload (.+) download (.+)');
        m = p.match(input.strip());
        
        srcBlock                         = m.group(1).strip().lower()
        srcIP        = blank_value(value = m.group(2).strip())
        srcMask      = blank_value(value = m.group(3).strip())
        destBlock                        = m.group(6).strip().lower()
        destIP       = blank_value(value = m.group(7).strip())
        destMask     = blank_value(value = m.group(8).strip())
        try:
            group    = blank_value(value = m.group(5).strip())
            group    = int(group);
        except:
            group    = -1;        
        try:
            srcPort  = blank_value(value = m.group(4).strip())
            srcPort  = int(srcPort);
        except:
            srcPort  = 0;
        try:
            destPort = blank_value(value = m.group(9).strip())
            destPort = int(destPort);
        except:
            destPort = 0;
        try:
            upload   = blank_value(value = m.group(10).strip())
            upload   = int(upload);
        except:
            upload   = 0;  
        try:
            download = blank_value(value = m.group(11).strip())
            download = int(download);
        except:
            download = 0;
        
        if srcBlock not in ("0", "1") or destBlock not in ("0", "1"):
            print "Please check srcblock / destblock are '0' or '1'";
            print usage;
            return;
        
        if srcBlock     == "0": srcBlock   = False;
        else: srcBlock  = True;
        if destBlock    == "0": destBlock = False; 
        else: destBlock = True;
        
    except:
        print "Syntax error.";
        print usage;
        return;
        
    updateDeviceArg                     = {};
    updateDeviceArg["sourceBlock"]      = srcBlock;
    updateDeviceArg["source"]           = srcIP;
    updateDeviceArg["sourceMask"]       = srcMask;
    updateDeviceArg["sourcePort"]       = int(srcPort);
    updateDeviceArg["groupid"]          = group;
    updateDeviceArg["destinationBlock"] = destBlock;
    updateDeviceArg["destination"]      = destIP;
    updateDeviceArg["destinationMask"]  = destMask;
    updateDeviceArg["destinationPort"]  = int(destPort);
    updateDeviceArg["upload"]           = int(upload * 1024);
    updateDeviceArg["download"]         = int(download * 1024);

    connectionDetail.request("firewall/acls/add/trafficshaper", updateDeviceArg);

def blank_value(value):
    if value  == '-':
        value = "";
        return value;
    else:
        return value;
    
def set_firewall_qos_delete(connectionDetail, input):
    usage = "Syntax error.\nUsage: {rule id}";
    try:
        p       = re.compile('(.+)');
        m       = p.match(input.strip());
    
        rule = m.group(1).strip();
        
        updateDeviceArg         = {};
        updateDeviceArg["id"]   = int(rule);

    except:
        print usage;
        return; 
    
    connectionDetail.request("firewall/acls/del/trafficshaper", updateDeviceArg);
    
def set_user_quota_add(connectionDetail, input):
    connectionDetail.request("auth/users/save", \
        add_quota_common("username", "S", input))

def set_user_quota_delete(connectionDetail, input):
    usage = "Usage: {username} period {TOTAL | DAY | WEEK | MONTH | TIMELIMIT}";
    try:
        p       = re.compile('(.+) period (.+)')
        m       = p.match(input.strip())

        userName  = m.group(1).strip()        
        period    = m.group(2).strip() 
        updateArg = delete_quota_common(period)

        if len(updateArg) == 0:
            print "Please ensure period is correct\n" + usage;
            return;

        updateArg["username"] = userName
    except:
        print usage;
        return;
    
    connectionDetail.request("auth/users/save", updateArg);

def set_group_quota_add(connectionDetail, input):
    connectionDetail.request("auth/groups/save", \
        add_quota_common("id", "I", input))

def set_group_quota_delete(connectionDetail, input):
    usage = "Usage: {group id} period {TOTAL | DAY | WEEK | MONTH | TIMELIMIT}";
    try:
        p       = re.compile('(.+) period (.+)');
        m       = p.match(input.strip());

        group    = m.group(1).strip();        
        period   = m.group(2).strip(); 
        
        updateGroupArg = delete_quota_common(period)

        if len(updateGroupArg) < 1:
            print "Please ensure period is correct\n" + usage;
            return;
        
        updateGroupArg["id"]      = int(group);
    except:
        print usage;
        return;
    
    connectionDetail.request("auth/groups/save", updateGroupArg);


def set_user_disconnect(connectionDetail, input):
    usage = "Usage: user disconnect {ip address of user}";
    try:
        p       = re.compile('(.+)');
        m       = p.match(input.strip());        
        
        ip    = m.group(1).strip();        
        is_valid = re.match("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$", ip);
        if not is_valid:
            print "Enter an IP address\n" + usage;
            return;
        
        updateDeviceArg                     = {};
        updateDeviceArg["ipaddress"]        = ip;
        
    except:
        print usage;
        return;
    
    try:
        connectionDetail.request("auth/logout", updateDeviceArg);
    except:
        return;
