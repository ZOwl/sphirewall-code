/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <iostream>
#include "test.h"

using namespace std;
using namespace SFwallCore;
#include "SFwallCore/Alias.h"
#include "SFwallCore/Firewall.h"
#include "Api/JsonManagementService.h"
#include "Core/Logger.h"
#include "Core/ConfigurationManager.h"
#include "SFwallCore/Acl.h"
#include "SFwallCore/ApplicationLevelFilter.h"

TEST(FirewallDelegate, alias_list) {
	SFwallCore::Firewall* firewall = new SFwallCore::Firewall();
	firewall->aliases = new SFwallCore::AliasDb(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager());
	firewall->aliases->setResolve(false);

	Alias* alias = new IpRangeAlias();
	alias->name = "google.com";
	
	Alias* alias2 = new IpRangeAlias();
	alias2->name = "yahoo.com";
	firewall->aliases->create(alias);
	firewall->aliases->create(alias2);

	FirewallDelegate* del = new FirewallDelegate(firewall, NULL, NULL);
	JSONObject ret = del->process("firewall/aliases/list", JSONObject());

	JSONArray aliases = ret[L"aliases"]->AsArray();
	EXPECT_TRUE(aliases.size() == 2);
}

TEST(FirewallDelegate, alias_add) {
	SFwallCore::Firewall* firewall = new SFwallCore::Firewall();
	firewall->aliases = new SFwallCore::AliasDb(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager());
	FirewallDelegate* del = new FirewallDelegate(firewall, NULL, NULL);
	firewall->aliases->setResolve(false);

	JSONObject args;
	args.put(L"name", new JSONValue((string) "Blah Name"));
	args.put(L"type", new JSONValue((double) 0));
	JSONObject ret = del->process("firewall/aliases/add", args);

	EXPECT_TRUE(firewall->aliases->aliases.size() == 1);
}

TEST(FirewallDelegate, alias_createwithurl) {
	SFwallCore::Firewall* firewall = new SFwallCore::Firewall();
	firewall->setLogContext(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"));
	firewall->aliases = new SFwallCore::AliasDb(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager());
	FirewallDelegate* del = new FirewallDelegate(firewall, NULL, NULL);
	firewall->aliases->setResolve(false);

	JSONObject args;
	args.put(L"name", new JSONValue((string) "google.com"));
	args.put(L"detail", new JSONValue((string) "http://mirror.sphirewall.net/resources/list.txt"));
	args.put(L"type", new JSONValue((double) 2));
		
	JSONObject ret = del->process("firewall/aliases/add", args);

	EXPECT_TRUE(firewall->aliases->aliases.size() == 1);
}

TEST(FirewallDelegate, alias_del) {
	SFwallCore::Firewall* firewall = new SFwallCore::Firewall();
	firewall->aliases = new SFwallCore::AliasDb(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager());
	firewall->aliases->setAclStore(new SFwallCore::ACLStore(NULL, NULL, NULL, NULL, firewall->aliases));
	firewall->aliases->setResolve(false);

	Alias* alias = new IpRangeAlias();
        alias->name = "google.com";

        Alias* alias2 = new IpRangeAlias();
        alias2->name = "yahoo.com";
        firewall->aliases->create(alias);
        firewall->aliases->create(alias2);

	FirewallDelegate* del = new FirewallDelegate(firewall, NULL, NULL);

	EXPECT_TRUE(firewall->aliases->aliases.size() == 2);

	JSONObject args;
	args.put(L"id", new JSONValue((string) alias->id));
	JSONObject ret = del->process("firewall/aliases/del", args);

	EXPECT_TRUE(firewall->aliases->aliases.size() == 1);
}

TEST(FirewallDelegate, alias_add_address) {
	SFwallCore::Firewall* firewall = new SFwallCore::Firewall();
	firewall->aliases = new SFwallCore::AliasDb(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager());
	firewall->aliases->setAclStore(new SFwallCore::ACLStore(NULL, NULL, NULL, NULL, firewall->aliases));
	firewall->aliases->setResolve(false);

        Alias* alias = new IpRangeAlias();
        alias->name = "google.com";

	firewall->aliases->create(alias);
	FirewallDelegate* del = new FirewallDelegate(firewall, NULL, NULL);

	JSONObject args;
	args.put(L"id", new JSONValue((string) alias->id));
	args.put(L"value", new JSONValue((string) "10.1.1.1-10.1.1.2"));
	JSONObject ret = del->process("firewall/aliases/alias/add", args);

	EXPECT_TRUE(alias->listEntries().size() == 1);
}


TEST(FirewallDelegate, alias_del_address) {
	SFwallCore::Firewall* firewall = new SFwallCore::Firewall();
	firewall->aliases = new SFwallCore::AliasDb(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager());
	firewall->aliases->setAclStore(new SFwallCore::ACLStore(NULL, NULL, NULL, NULL, firewall->aliases));
	firewall->aliases->setResolve(false);

        Alias* alias = new IpRangeAlias();
        alias->name = "google.com";

	firewall->aliases->create(alias);
	FirewallDelegate* del = new FirewallDelegate(firewall, NULL, NULL);
	alias->addEntry("10.1.1.1-10.1.1.10");

	JSONObject args;
	args.put(L"id", new JSONValue((string) alias->id));
	args.put(L"value", new JSONValue((string) "10.1.1.1-10.1.1.10"));
	JSONObject ret = del->process("firewall/aliases/alias/del", args);

	EXPECT_TRUE(alias->listEntries().size() == 0);
}

TEST(FirewallDelegate, alias_list_address) {
	SFwallCore::Firewall* firewall = new SFwallCore::Firewall();
	firewall->aliases = new SFwallCore::AliasDb(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager());
	firewall->aliases->setAclStore(new SFwallCore::ACLStore(NULL, NULL, NULL, NULL, firewall->aliases));
	firewall->aliases->setResolve(false);

        Alias* alias = new IpRangeAlias();
        alias->name = "google.com";
        firewall->aliases->create(alias);

	FirewallDelegate* del = new FirewallDelegate(firewall, NULL, NULL);

	alias->addEntry("10.1.1.1-10.1.1.10");
	alias->addEntry("10.1.2.1-10.1.2.10");

	JSONObject args;
	args.put(L"id", new JSONValue((string) alias->id));
	JSONObject ret = del->process("firewall/aliases/alias/list", args);

	EXPECT_TRUE(ret[L"items"]->AsArray().size() == 2);
}

TEST(FirewallDelegate, acls_list_BASIC) {
	SFwallCore::Firewall* firewall = new SFwallCore::Firewall();
	SFwallCore::ACLStore* store = new SFwallCore::ACLStore();
	firewall->acls = store;

	SFwallCore::FilterRule* rule1 = new SFwallCore::FilterRule();
	SFwallCore::NatRule* rule2 = new SFwallCore::NatRule();
	store->loadAclEntry(rule1);
	store->loadAclEntry(rule2);

	FirewallDelegate* del = new FirewallDelegate(firewall, NULL, NULL);

	JSONObject ret = del->process("firewall/acls/list", JSONObject());
	JSONArray filterRules = ret[L"normal"]->AsArray();
	JSONArray natRules = ret[L"nat"]->AsArray();

	EXPECT_TRUE(filterRules.size() == 1);
	EXPECT_TRUE(natRules.size() == 1);
}

TEST(FirewallDelegate, acls_add_filter) {
	SFwallCore::Firewall* firewall = new SFwallCore::Firewall();
	SFwallCore::ACLStore* store = new SFwallCore::ACLStore(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), NULL, NULL, NULL, NULL);
	store->setConfigurationManager(new ConfigurationManager());
	firewall->acls = store;

	FirewallDelegate* del = new FirewallDelegate(firewall, NULL, NULL);

	JSONObject args;
	args.put(L"action", new JSONValue((double) 0));
	args.put(L"sourceAddress", new JSONValue((string) "10.1.1.1"));
	args.put(L"sourceSubnet", new JSONValue((string) "255.255.255.255"));

	args.put(L"destAddress", new JSONValue((string) "10.1.1.2"));
	args.put(L"destSubnet", new JSONValue((string) "255.255.255.254"));

	JSONObject ret = del->process("firewall/acls/add", args);

	EXPECT_TRUE(store->listFilterRules().size() == 1);
	EXPECT_TRUE(store->listFilterRules()[0]->action == 0);

	EXPECT_TRUE(store->listFilterRules()[0]->dest == IP4Addr::stringToIP4Addr("10.1.1.2"));
	EXPECT_TRUE(store->listFilterRules()[0]->destMask == IP4Addr::stringToIP4Addr("255.255.255.254"));

	EXPECT_TRUE(store->listFilterRules()[0]->source == IP4Addr::stringToIP4Addr("10.1.1.1"));
	EXPECT_TRUE(store->listFilterRules()[0]->sourceMask == IP4Addr::stringToIP4Addr("255.255.255.255"));
}

TEST(FirewallDelegate, acls_add_nat_dnat) {
	SFwallCore::Firewall* firewall = new SFwallCore::Firewall();
	SFwallCore::ACLStore* store = new SFwallCore::ACLStore(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), NULL, NULL, NULL, NULL);
	store->setConfigurationManager(new ConfigurationManager());
	firewall->acls = store;

	FirewallDelegate* del = new FirewallDelegate(firewall, NULL, NULL);

	JSONObject args;
	args.put(L"action", new JSONValue((double) 3));
	args.put(L"forwardTarget", new JSONValue((string) "10.2.2.2"));
	args.put(L"forwardPort", new JSONValue((double) 80));

	JSONObject ret = del->process("firewall/acls/add", args);

	EXPECT_TRUE(store->listNatRules().size() == 1);
	EXPECT_TRUE(store->listNatRules()[0]->action == 3);

	SFwallCore::NatRule* r = dynamic_cast<SFwallCore::NatRule*> (store->listNatRules()[0]);
	EXPECT_TRUE(r->natTarget == IP4Addr::stringToIP4Addr("10.2.2.2"));
	EXPECT_TRUE(r->natPort == 80);
	EXPECT_TRUE(r->natType == SFwallCore::DNAT);
}

TEST(FirewallDelegate, acls_add_nat_snat) {
	SFwallCore::Firewall* firewall = new SFwallCore::Firewall();
	SFwallCore::ACLStore* store = new SFwallCore::ACLStore(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), NULL, NULL, NULL, NULL);
	store->setConfigurationManager(new ConfigurationManager());
	firewall->acls = store;

	FirewallDelegate* del = new FirewallDelegate(firewall, NULL, NULL);

	JSONObject args;
	args.put(L"action", new JSONValue((double) 2));
	args.put(L"masqueradeTarget", new JSONValue((string) "10.2.2.2"));

	JSONObject ret = del->process("firewall/acls/add", args);

	EXPECT_TRUE(store->listNatRules().size() == 1);
	EXPECT_TRUE(store->listNatRules()[0]->action == 2);

	SFwallCore::NatRule* r = dynamic_cast<SFwallCore::NatRule*> (store->listNatRules()[0]);
	EXPECT_TRUE(r->natTarget == IP4Addr::stringToIP4Addr("10.2.2.2"));
	EXPECT_TRUE(r->natType == SFwallCore::SNAT);
}

TEST(FirewallDelegate, acls_filter_delete) {
	SFwallCore::Firewall* firewall = new SFwallCore::Firewall();
	SFwallCore::ACLStore* store = new SFwallCore::ACLStore(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), NULL, NULL, NULL, NULL);
	store->setConfigurationManager(new ConfigurationManager());
	firewall->acls = store;

	FirewallDelegate* del = new FirewallDelegate(firewall, NULL, NULL);

	SFwallCore::FilterRule* rule1 = new SFwallCore::FilterRule();
	rule1->id = "1";
	SFwallCore::FilterRule* rule2 = new SFwallCore::FilterRule();
	rule2->id = "2";

	store->loadAclEntry(rule1);
	store->loadAclEntry(rule2);

	JSONObject args;
	args.put(L"id", new JSONValue((string)rule1->id ));
	JSONObject ret = del->process("firewall/acls/filter/delete", args);

	cout << "entries:" << store->listFilterRules().size() << endl;
	EXPECT_TRUE(store->listFilterRules().size() == 1);
}

TEST(FirewallDelegate, acls_nat_delete) {
	SFwallCore::Firewall* firewall = new SFwallCore::Firewall();
	SFwallCore::ACLStore* store = new SFwallCore::ACLStore(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), NULL, NULL, NULL, NULL);
	store->setConfigurationManager(new ConfigurationManager());
	firewall->acls = store;

	FirewallDelegate* del = new FirewallDelegate(firewall, NULL, NULL);

	SFwallCore::NatRule* rule1 = new SFwallCore::NatRule();
	SFwallCore::NatRule* rule2 = new SFwallCore::NatRule();
	rule1->id = "1";
	rule2->id = "2";

	store->loadAclEntry(rule1);
	store->loadAclEntry(rule2);

	JSONObject args;
	args.put(L"id", new JSONValue((string) rule1->id));
	JSONObject ret = del->process("firewall/acls/nat/delete", args);

	EXPECT_TRUE(store->listNatRules().size() == 1);
}

TEST(FirewallDelegate, acls_delete_trafficshaper){
	MockFactory* mocks = new MockFactory();
        FirewallDelegate* del = new FirewallDelegate(mocks->givenFw(), NULL, NULL);
        TsRule* rule = new TsRule(0);

	mocks->givenFw()->trafficShaper->store->add(rule);

	JSONObject args;
	args.put(L"id", new JSONValue((double) 0));

	del->process("firewall/acls/del/trafficshaper", args);
	EXPECT_TRUE(mocks->givenFw()->trafficShaper->store->list().size() == 0);
}

TEST(FirewallDelegate, acls_delete_trafficshaper_invalid){
	MockFactory* mocks = new MockFactory();
        FirewallDelegate* del = new FirewallDelegate(mocks->givenFw(), NULL, NULL);
        TsRule* rule = new TsRule(0);

	mocks->givenFw()->trafficShaper->store->add(rule);

	JSONObject args;
	args.put(L"id", new JSONValue((double) 100));
	
	expectException(del, "firewall/acls/del/trafficshaper", args);
}

TEST(FirewallDelegate, webfilter_rules_list){
	SFwallCore::HttpUrlApplicationFilter* filter = new SFwallCore::HttpUrlApplicationFilter(NULL, NULL, NULL);
	
	SFwallCore::WebsiteListAlias* list = new SFwallCore::WebsiteListAlias();
	list->id = "12345";
	SFwallCore::FilterCriteria* rule1 = new SFwallCore::FilterCriteria();
	rule1->sourceIp = IP4Addr::stringToIP4Addr("10.1.1.1");
	rule1->sourceMask = IP4Addr::stringToIP4Addr("255.255.255.0");
	
	Group* group = new Group();
	group->setId(11);
	rule1->groups.push_back(group);
	rule1->lists.push_back(list);
	rule1->redirect = false;
	rule1->redirectUrl = "";
	filter->addRule(rule1);
	
	FirewallDelegate* del = new FirewallDelegate(NULL, NULL, NULL);
	del->setHttpApplicationFilter(filter);	
	
	JSONObject res = del->process("firewall/webfilter/rules/list", JSONObject());
	JSONArray rules = res[L"rules"]->AsArray();
	EXPECT_TRUE(rules.size() == 1);

	JSONObject r1 = rules[0]->AsObject();
	EXPECT_TRUE(r1[L"sourceIp"]->String().compare("10.1.1.1") == 0);
	EXPECT_TRUE(r1[L"sourceMask"]->String().compare("255.255.255.0") == 0);
}
TEST(FirewallDelegate, webfilter_rules_add){
	SFwallCore::AliasDb* aliasDb = new SFwallCore::AliasDb(NULL, NULL);

	SFwallCore::WebsiteListAlias* alias = new SFwallCore::WebsiteListAlias();	
	aliasDb->create(alias);
	SFwallCore::HttpUrlApplicationFilter* filter = new SFwallCore::HttpUrlApplicationFilter(NULL, aliasDb, NULL);

        FirewallDelegate* del = new FirewallDelegate(NULL, NULL, new GroupDb(NULL, NULL));
        del->setHttpApplicationFilter(filter);
	
	JSONObject args;
	args.put(L"list", new JSONValue((string) alias->id));
	args.put(L"sourceIp", new JSONValue((string) "10.1.1.1"));
	args.put(L"sourceMask", new JSONValue((string) "255.255.255.0"));
	args.put(L"group", new JSONValue((double) 0));
	args.put(L"action", new JSONValue((double) 0));
	args.put(L"fireEvent", new JSONValue((bool) true));	
	args.put(L"redirect", new JSONValue((bool) false));
	args.put(L"redirectUrl", new JSONValue((string) ""));	
	
	args.put(L"mon", new JSONValue((bool) true));	
	args.put(L"tues", new JSONValue((bool) true));	
	args.put(L"wed", new JSONValue((bool) true));	
	args.put(L"thurs", new JSONValue((bool) true));	
	args.put(L"fri", new JSONValue((bool) true));	
	args.put(L"sat", new JSONValue((bool) true));	
	args.put(L"sun", new JSONValue((bool) true));	
	args.put(L"startTime", new JSONValue((double) 1));	
	args.put(L"endTime", new JSONValue((double) 20));	

        JSONObject res = del->process("firewall/webfilter/rules/add", args);
	
	EXPECT_TRUE(filter->getRules().size() == 1);
}
