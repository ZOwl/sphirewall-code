/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <iostream>

using namespace std;

#include "Auth/UserDb.h"
#include "Auth/GroupDb.h"
#include "Auth/SessionDb.h"
#include "Core/Logger.h"
#include "SFwallCore/ConnTracker.h"

TEST(sessionDb, findSessionsForUser) {
	Config* config = new Config();
	SLogger::Logger* logger = new SLogger::Logger();
	CronManager* cronManager = new CronManager(logger->getDefault(), config);
	SFwallCore::PlainConnTracker* plainConnTracker = new SFwallCore::PlainConnTracker(logger->getDefault(), NULL);
	SessionDb* sessionDb = new SessionDb(logger->getDefault(), cronManager, plainConnTracker);

	string mac = "OO:AA:BB:CC:DD";
	string ip = "10.0.0.1";

	User* user = new User();
	Session* session = sessionDb->create(user, mac, ip);
	EXPECT_TRUE(session != NULL);

	EXPECT_TRUE(session == sessionDb->get(mac, ip)) << "Get session from mac address";

	std::list<Session*> listOfSessions = sessionDb->findSessionsForUser(user);
	EXPECT_TRUE(listOfSessions.size() == 1);

	std::list<Session*>::iterator iter;
	for (iter = listOfSessions.begin(); iter != listOfSessions.end(); iter++) {
		EXPECT_TRUE(session == (*iter));
		break;
	}
}
