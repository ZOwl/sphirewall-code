/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "SFwallCore/Packet.h"
#include "SFwallCore/TrafficShaper/TokenFilter.h"
#include "Auth/AuthenticationHandler.h"

using namespace std;

TokenFilter::TokenFilter() {
	dport = 0;
	sport = 0;
	dest = -1;
	destMask = -1;
	source = -1;
	sourceMask = -1;
	userPtr = NULL;
	dest_dev = -1;
	source_dev = -1;

	block_dest = -1;
	block_destMask = -1;
	block_source = -1;
	block_sourceMask = -1;
	block_groupid = -1;
}

bool TokenFilter::check(SFwallCore::Packet* packet) {
	if (packet->getProtocol() == SFwallCore::TCP || packet->getProtocol() == SFwallCore::UDP) {
		const SFwallCore::TcpUdpPacket* tcpudp = dynamic_cast<const SFwallCore::TcpUdpPacket*> (packet);
		if (dport != 0) {
			if (tcpudp->getDstPort() != dport) {
				return false;
			}
		}

		if (sport != 0) {
			if (tcpudp->getSrcPort() != sport) {
				return false;
			}
		}
	}
	if (dest != -1 && destMask != -1) {
		if (IP4Addr::matchNetwork(packet->getDstIp(), dest, destMask) == -1) {
			return false;
		}
	}

	if (source != -1 && sourceMask != -1) {
		if (IP4Addr::matchNetwork(packet->getSrcIp(), source, sourceMask) == -1) {
			return false;
		}
	}

	if (userPtr != NULL) {
		if (packet->getUser() != userPtr) {
			return false;
		}
	}

	if (source_dev != -1) {
		if (source_dev != packet->getSourceDev()) {
			return false;
		}
	}

	if (dest_dev != -1) {
		if (dest_dev != packet->getDestDev()) {
			return false;
		}
	}

	return true;
}

