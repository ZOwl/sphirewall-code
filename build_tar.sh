#!/bin/bash
TEMP=tar_build
VERSION=0.9.9.7

build_pkgs() {
	sh autogen.sh
	sh configure
	make
	ldconfig
	make dist
}

echo "Sphirewall Build Script"
rm -Rf $TEMP
mkdir $TEMP

echo "Setting up env"
cd sphirewall-libs
build_pkgs
cp sphirewall-libs-$VERSION.tar.gz ../$TEMP/

cd ../sphirewall-core
build_pkgs
cp sphirewall-core-$VERSION.tar.gz ../$TEMP/

cd ../sphirewall-ana
build_pkgs
cp sphirewall-ana-$VERSION.tar.gz ../$TEMP/ 

cd ../
tar cfz sphirewall-utils-$VERSION.tar.gz sphirewall-utils/
tar cfz sphirewall-scli-$VERSION.tar.gz sphirewall-scli/
tar cfz sphirewall-wmi-$VERSION.tar.gz sphirewall-wmi/
tar cfz sphirewall-kernel-$VERSION.tar.gz sphirewall-kernel/

mv sphirewall-scli-$VERSION.tar.gz $TEMP/
mv sphirewall-wmi-$VERSION.tar.gz $TEMP/
mv sphirewall-kernel-$VERSION.tar.gz $TEMP/
mv sphirewall-utils-$VERSION.tar.gz $TEMP/

tar cf sphirewall-all-$VERSION.tar $TEMP/
