/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <map>
#include <list>
#include <vector>
#include <queue>

#ifndef SPHIREWALL_BANDWIDTH_H_INCLUDED
#define SPHIREWALL_BANDWIDTH_H_INCLUDED

#include "Core/Logger.h"
#include "Utils/Utils.h"
#include "Common/sphirewall_bandwidth_types.h"

class TrafficAnalyticsDb;
class MonitoringDb;

class BandwidthDb {
private:

	bool findHost(std::string hostName);
	TrafficAnalyticsDb* taDb;
	MonitoringDb* monDb;
	SLogger::LogContext* logger;

public:

	TrafficAnalyticsDb* getTaDb() const {
		return taDb;
	}

	MonitoringDb* getMonitoringDb() const {
		return monDb;
	}

	BandwidthDb(std::string dbFileName, SLogger::LogContext* logger);
	~BandwidthDb();

	void lock();
	void unlock();

};

#endif

