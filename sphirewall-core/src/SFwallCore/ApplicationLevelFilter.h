/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef APP_LEVEL_FILTER
#define APP_LEVEL_FILTER

#include "Utils/Lock.h"
#include "SFwallCore/Alias.h"
#include "Auth/GroupDb.h"

class ConfigurationManager;
class Group;
class EventDb;
namespace SFwallCore {
	class WebsiteListAlias;
	class AliasDb;

	class Packet;
	enum ApplicationFilterAction {
		ALLOW = 1, /* Whitelist*/
		DENY = 0 /* Blacklist */,
		DENY_ALL =2
	};

	class FilterCriteria {
		public:
			FilterCriteria(){
				sourceIp = 0;
				sourceMask = 0;
		
				log = false;
				fireEvent = false;
				action = DENY;
				redirect = false;
				startTime = -1;
				endTime = -1;

				mon = false;
				tues = false;
				wed = false;
				thurs = false;
				fri = false;
				sat = false;
				sun = false;
				exclusive = false;
				enabled = false;
			}			

			std::string id;
			unsigned int sourceIp;
			unsigned int sourceMask;
			std::list<Group*> groups;
			//Group* group;
			std::list<WebsiteListAlias*> lists;
			std::list<Alias*> sourceAlias;

			bool match(SFwallCore::Packet* packet);

			ApplicationFilterAction action;	
			bool log;
			bool fireEvent;	
			bool redirect;
			std::string redirectUrl;
			int startTime;
			int endTime;

			bool mon;
			bool tues;
			bool wed;
			bool thurs;
			bool fri;
			bool sat;
			bool sun;
			bool exclusive;
			bool enabled;
	};

	class HttpUrlApplicationFilter : public AliasRemovedListener, public GroupRemovedListener{
		public:

			HttpUrlApplicationFilter(ConfigurationManager* config, AliasDb* aliases, GroupDb* groupDb) : config(config), aliases(aliases), groupDb(groupDb){
				lockPtr = new Lock();
				eventDb = NULL;
			}
			bool filter(SFwallCore::Packet* packet);

			void lock();
			void unlock();	

			void load();
			void save();

			void addRule(FilterCriteria*);
			void removeRule(FilterCriteria*);
			std::list<FilterCriteria*>& getRules();
			FilterCriteria* get(std::string id);
			AliasDb* getAliasDb(){
				return aliases;
			}

			void setEventDb(EventDb* eventDb){
				this->eventDb = eventDb;
			}

			void moveup(FilterCriteria* target);	
			void movedown(FilterCriteria* target);	

			
		private:
			int findpos(FilterCriteria* target);
			void insert(int pos, FilterCriteria* target);
			std::list<FilterCriteria*> filters;		

			Lock* lockPtr;
			ConfigurationManager* config;
			AliasDb* aliases;
			GroupDb* groupDb;
			EventDb* eventDb;

                        void aliasRemoved(Alias* alias);
			void groupRemoved(Group* group);

			bool hasHit(std::string host, SFwallCore::FilterCriteria* crit);
	};
};
#endif
