/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <iostream>

using namespace std;

#include "Auth/UserDb.h"
#include "Auth/SessionDb.h"
#include "Core/Logger.h"
#include "SFwallCore/ConnTracker.h"
#include "Core/ConfigurationManager.h"

TEST(userDb, createUser) {
	Config* config = new Config();
	SLogger::Logger* logger = new SLogger::Logger();
	CronManager* cronManager = new CronManager(logger->getDefault(), config);
	SFwallCore::PlainConnTracker* plainConnTracker = new SFwallCore::PlainConnTracker(logger->getDefault(), NULL);
	SessionDb* sessionDb = new SessionDb(logger->getDefault(), cronManager, plainConnTracker);

	UserDb* userDb = new UserDb(NULL, NULL, logger->getDefault(), new ConfigurationManager(), NULL);

	User* user = userDb->createUser("michael");
	EXPECT_TRUE(user != NULL);

	EXPECT_TRUE(user->getUserName().compare("michael") == 0);
	EXPECT_TRUE(user->getAuthenticationType() == 0);
}

TEST(userDb, deleteUser) {
	Config* config = new Config();
	SLogger::Logger* logger = new SLogger::Logger();
	CronManager* cronManager = new CronManager(logger->getDefault(), config);
	SFwallCore::PlainConnTracker* plainConnTracker = new SFwallCore::PlainConnTracker(logger->getDefault(), NULL);
	SessionDb* sessionDb = new SessionDb(logger->getDefault(), cronManager, plainConnTracker);

	UserDb* userDb = new UserDb(NULL, NULL, logger->getDefault(), new ConfigurationManager(), NULL);
	userDb->setSessionDb(sessionDb);

	string mac = "OO:AA:BB:CC:DD";
	string ip = "10.0.0.1";

	User* user = userDb->createUser("michael");
	user->enable();

	Session* session = sessionDb->create(user, mac, ip);
	EXPECT_TRUE(session != NULL);

	EXPECT_TRUE(sessionDb->findSessionsForUser(user).size() == 1);

	//Now disable this user: We expect that all sessions will be removed
	userDb->delUser(user);
	EXPECT_TRUE(sessionDb->findSessionsForUser(user).size() == 0);
	EXPECT_TRUE(userDb->list().size() == 0);
}

TEST(userDb, disableUser) {
	Config* config = new Config();
	SLogger::Logger* logger = new SLogger::Logger();
	CronManager* cronManager = new CronManager(logger->getDefault(), config);
	SFwallCore::PlainConnTracker* plainConnTracker = new SFwallCore::PlainConnTracker(logger->getDefault(), NULL);
	SessionDb* sessionDb = new SessionDb(logger->getDefault(), cronManager, plainConnTracker);

	UserDb* userDb = new UserDb(NULL, NULL, logger->getDefault(), new ConfigurationManager(), NULL);
	userDb->setSessionDb(sessionDb);

	string mac = "OO:AA:BB:CC:DD";
	string ip = "10.0.0.1";

	User* user = userDb->createUser("michael");
	user->enable();

	Session* session = sessionDb->create(user, mac, ip);
	EXPECT_TRUE(session != NULL);

	EXPECT_TRUE(sessionDb->findSessionsForUser(user).size() == 1);

	//Now disable this user: We expect that all sessions will be removed
	userDb->disableUser(user);
	EXPECT_TRUE(sessionDb->findSessionsForUser(user).size() == 0);


}
