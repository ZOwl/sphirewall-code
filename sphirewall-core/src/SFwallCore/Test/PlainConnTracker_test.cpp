/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <iostream>
#include <algorithm>
#include "SFwallCore/ConnTracker.h"
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <linux/icmp.h>
#include <netinet/ip.h>

#include "SFwallCore/Test/TestFactory.h"
using namespace SFwallCore;

TEST(PlainConnTracker, createTcpConnection) {
	SLogger::Logger* logger = new SLogger::Logger();

	TcpPacket* tcp = PacketBuilder::createTcp("10.1.1.1", "192.168.0.1", 3453, 80);
	PacketBuilder::setAck(tcp);

	PlainConnTracker* tracker = new PlainConnTracker(logger->getDefault(), NULL);
	Connection* tcpConnection = tracker->create(tcp);
	EXPECT_TRUE(tcpConnection);
	EXPECT_TRUE((tcpConnection->getProtocol() == TCP));

	TcpConnection* connection2 = dynamic_cast<TcpConnection*> (tcpConnection);
	EXPECT_TRUE(connection2->getState()->state == TCPUP) << "Check the state";

	Connection* temp = tracker->offer(tcp);
	EXPECT_TRUE(temp == tcpConnection) << "Check we match the right connection";

	TcpPacket* tcp2 = PacketBuilder::createTcp("192.168.0.1", "10.1.1.1", 80, 3453);
	EXPECT_TRUE(tracker->offer(tcp) == tcpConnection) << "Check the return packet matches the right connection";

	delete tcp;
	delete tracker;
	delete logger;
}

TEST(PlainConnTracker, createUdpConnection) {
	SLogger::Logger* logger = new SLogger::Logger();
	UdpPacket* udp= PacketBuilder::createUdp("192.1.1.1", "192.168.0.1", 3453, 80);

	PlainConnTracker* tracker = new PlainConnTracker(logger->getDefault(), NULL);
	Connection* udpConnection = tracker->create(udp);
	EXPECT_TRUE(udpConnection);
	cout << "connection is a? : " << udpConnection->getProtocol() << endl;

	EXPECT_TRUE((udpConnection->getProtocol() == UDP));

	Connection* temp = tracker->offer(udp);
	EXPECT_TRUE(temp == udpConnection);

	delete udp;
	delete tracker;
	delete logger;
}


TEST(PlainConnTracker, createIcmpConnection) {
	SLogger::Logger* logger = new SLogger::Logger();

	IcmpPacket* icmp = PacketBuilder::createIcmp("192.1.1.1", "192.168.0.1", 1);
	PlainConnTracker* tracker = new PlainConnTracker(logger->getDefault(), NULL);

	Connection* icmpConnection = tracker->create(icmp);
	EXPECT_TRUE(icmpConnection);
	EXPECT_TRUE((icmpConnection->getProtocol() == ICMP));

	Connection* temp = tracker->offer(icmp);
	EXPECT_TRUE(temp == icmpConnection);

	delete icmp;
	delete tracker;
	delete logger;
}


TEST(ConnectionCriteria, matchTcp_AllCriteria) {
	SLogger::Logger* logger = new SLogger::Logger();
	unsigned int source = IP4Addr::stringToIP4Addr("10.1.1.1");
	unsigned int dest = IP4Addr::stringToIP4Addr("192.168.0.1");

	TcpPacket* tcp = PacketBuilder::createTcp("10.1.1.1", "192.168.0.1", 40000, 80);

	PlainConnTracker* tracker = new PlainConnTracker(logger->getDefault(), NULL);
	Connection* connection = tracker->create(tcp);

	ConnectionCriteria* criteria = new ConnectionCriteria();
	criteria->setSource(source);
	criteria->setSourcePort(40000);
	criteria->setDest(dest);
	criteria->setDestPort(80);
	criteria->setProtocol(TCP);

	EXPECT_TRUE(criteria->match(connection));
}


TEST(ConnectionCriteria, matchUdp_AllCriteria) {
	SLogger::Logger* logger = new SLogger::Logger();
	unsigned int source = IP4Addr::stringToIP4Addr("10.1.1.1");
	unsigned int dest = IP4Addr::stringToIP4Addr("192.168.0.1");

	UdpPacket* udp = PacketBuilder::createUdp("10.1.1.1", "192.168.0.1", 40000, 80); 
	PlainConnTracker* tracker = new PlainConnTracker(logger->getDefault(), NULL);
	Connection* connection = tracker->create(udp);

	ConnectionCriteria* criteria = new ConnectionCriteria();
	criteria->setSource(source);
	criteria->setSourcePort(40000);
	criteria->setDest(dest);
	criteria->setDestPort(80);
	criteria->setProtocol(UDP);

	EXPECT_TRUE(criteria->match(connection));
}


TEST(ConnectionCriteria, matchUdp_WrongType) {
	SLogger::Logger* logger = new SLogger::Logger();

	UdpPacket* udp = PacketBuilder::createUdp("10.1.1.1", "192.168.0.1", 3453, 80);
	PlainConnTracker* tracker = new PlainConnTracker(logger->getDefault(), NULL);
	Connection* connection = tracker->create(udp);

	ConnectionCriteria* criteria = new ConnectionCriteria();
	criteria->setProtocol(TCP);

	EXPECT_TRUE(!criteria->match(connection));
}


TEST(ConnectionCriteria, matchUdp_WrongSource) {
	SLogger::Logger* logger = new SLogger::Logger();
	unsigned int source = IP4Addr::stringToIP4Addr("10.1.1.1");

	UdpPacket* udp = PacketBuilder::createUdp("10.1.1.2", "192.168.0.1", 3453, 80);
	PlainConnTracker* tracker = new PlainConnTracker(logger->getDefault(), NULL);
	Connection* connection = tracker->create(udp);

	ConnectionCriteria* criteria = new ConnectionCriteria();
	criteria->setSource(source);
	criteria->setProtocol(UDP);

	EXPECT_TRUE(!criteria->match(connection));
}


TEST(ConnectionCriteria, matchUdp_WrongDest) {
	SLogger::Logger* logger = new SLogger::Logger();
	unsigned int dest = IP4Addr::stringToIP4Addr("10.1.1.1");

	UdpPacket* udp = PacketBuilder::createUdp("10.1.1.2", "192.168.0.1", 3453, 80);
	PlainConnTracker* tracker = new PlainConnTracker(logger->getDefault(), NULL);
	Connection* connection = tracker->create(udp);

	ConnectionCriteria* criteria = new ConnectionCriteria();
	criteria->setSource(dest);
	criteria->setProtocol(UDP);

	EXPECT_TRUE(!criteria->match(connection));
}
