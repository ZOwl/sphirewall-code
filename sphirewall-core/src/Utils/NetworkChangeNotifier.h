/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <assert.h>
#include <sys/socket.h>
#include <asm/types.h>
#include <inttypes.h>

#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <list>
#ifndef IFLA_RTA
#include <linux/if_addr.h>
#define IFLA_RTA(r)  ((struct rtattr*)(((char*)(r)) + NLMSG_ALIGN(sizeof(struct ifinfomsg))))
#endif

#ifndef IFA_RTA
#include <linux/if_addr.h>
#define IFA_RTA(r)  ((struct rtattr*)(((char*)(r)) + NLMSG_ALIGN(sizeof(struct ifaddrmsg))))
#endif

#ifndef NETWORK_CHANGE_H
#define NETWORK_CHANGE_H

using namespace std;

enum NetworkChangeEventType{
	DEVICE_ADD_EXISTS = 0,
	DEVICE_CHANGE = 1,
	DEVICE_REMOVAL = 2
};

class NetworkChange {
	public:
		std::string ifname;
		int ifid;
		NetworkChangeEventType eventType;
};

class NetworkChangeNotifier {
	public:
		int connect();
		int pollfirst();
		int waitForEvent();
		int handleEvent(list<NetworkChange>& _changes);	
		int disconnect(); 

	private:
		int fd;	
};

#endif
