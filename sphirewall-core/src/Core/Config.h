/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_CONFIG_H_INCLUDED
#define SPHIREWALL_CONFIG_H_INCLUDED

#include <list>
#include <string>
#include <map>
#include <vector>
#include "Core/Logger.h"

using namespace std;

class ConfigurationManager;

class LoggingConfiguration {
public:

	LoggingConfiguration(ConfigurationManager* configManager, SLogger::Logger* rootLogger) :
	configManager(configManager), rootLogger(rootLogger) {
		load();
	}

	std::list<std::string> getContexts();
	int getLevel(std::string context);
	int setLevel(std::string context, int priority);

	void save();
	void load();

private:
	ConfigurationManager* configManager;
	SLogger::Logger* rootLogger;
};

class RuntimeConfiguration {
public:
	RuntimeConfiguration(){
		configurationManager = NULL;	
	}

	int get(std::string key);
	int set(std::string key, int newvalue);
	void loadOrPut(std::string key, int* value);
	void listAll(std::list<string>& l);
	bool contains(std::string key);

	void save();
	void setConfigurationManager(ConfigurationManager* configManager){
		this->configurationManager = configManager;
	}
private:
	void put(std::string key, int* ptr);
	map<string, int*> configMap;
	ConfigurationManager* configurationManager;
};

class Config {
public:

	Config() {
		runtime = new RuntimeConfiguration();
	}

	void save();
	int loadConfig();

	static const string timeSlotName;

	RuntimeConfiguration* getRuntime() {
		return runtime;
	}

	void setConfigurationManager(ConfigurationManager* configurationManager) {
		this->configurationManager = configurationManager;
		this->runtime->setConfigurationManager(configurationManager);
	}

	ConfigurationManager* getConfigurationManager() {
		return configurationManager;
	}
private:
	ConfigurationManager* configurationManager;
	RuntimeConfiguration* runtime;
};


#endif
