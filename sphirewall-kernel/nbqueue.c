/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <net/sock.h>
#include <linux/delay.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <asm/types.h>
#include <linux/skbuff.h>
#include <linux/module.h>
#include <linux/kernel.h>

#include "queue.h"
#include "sphirewall_queue.h"
#include "nbqueue.h"

struct meta* queue_init(int cap, int elem_size, void* memory_block){
	struct meta* queue = (struct meta*) memory_block;
        queue->tail= 0;
        queue->capacity = cap;
        queue->head= 0;
        queue->elem_size = 4096;
	queue->offset = 4096;	
	queue->closing = 0;
	queue->pid = 0;
	return queue;
}

void notify_userspace(int pid){
	struct siginfo info;
	memset(&info, 0, sizeof(struct siginfo));
	info.si_signo = SIGUSR1;
	info.si_code = SI_QUEUE;
	info.si_int = 1234;

	send_sig_info(SIGUSR1, &info, pid_task(find_vpid(pid), PIDTYPE_PID));
}

void queue_push(struct qmgr* mgr){
	if(mgr->state == STATE_ONLINE){
		static int notify = 0;
		if(mgr->block->tail == mgr->block->head){
			notify =1;
		}
		mgr->block->tail = (mgr->block->tail + 1) % (mgr->block->capacity + 1);

		if(notify == 1){
			if(mgr->block->pid != 0){
				notify_userspace(mgr->block->pid);
				notify = 0;
			}
		}
	}
}

struct message* queue_enqueue(struct qmgr* mgr){
	struct message* addr = NULL;	
	while (mgr->state == STATE_ONLINE && (mgr->block->tail + 1) % (mgr->block->capacity + 1) == mgr->block->head){
		DO_YIELD();
	}

	if(mgr->state == STATE_OFFLINE){
		return NULL;
	}

	addr = ((char*)mgr->block) + (mgr->block->offset + (mgr->block->elem_size * mgr->block->tail));
	addr->packet.raw_packet = ((char*)addr)+ sizeof(struct message);
	return addr;
}

void queue_pop(struct qmgr* mgr){
	if(mgr->state == STATE_ONLINE){
		mgr->block->head = (mgr->block->head + 1) % (mgr->block->capacity + 1);
	}
}

struct message* queue_peek(struct qmgr* mgr){
	static int deSleepTimer = 0;
	while (mgr->state == STATE_ONLINE &&  mgr->block->head == mgr->block->tail){
		if(deSleepTimer++ == 10000){
			wait_event_interruptible(recv_wait_lock, mgr->state == STATE_OFFLINE || mgr->block->head != mgr->block->tail);
			deSleepTimer= 0;
		}else{
			DO_YIELD();
		}
	}

	if(mgr->state == STATE_OFFLINE ){
		return NULL;
	}

	void* addr = ((char*)mgr->block) + (mgr->block->elem_size * mgr->block->head) + mgr->block->offset;
	struct message* m = (struct message*) addr;
	m->packet.raw_packet = (unsigned char*) ((char*) addr) + sizeof(struct message);
	return m;
}

