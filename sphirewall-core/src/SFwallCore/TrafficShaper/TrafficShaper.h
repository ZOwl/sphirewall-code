/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TRAFFICSHAPER_H
#define TRAFFICSHAPER_H

#include <map>
#include "TokenBucket.h"
#include "Utils/Lock.h"
#include "SFwallCore/Packet.h"
#include "TSRule.h"
#include "TSRuleStore.h"
#include "Core/Logger.h"
#include "Core/Config.h"
#include "Auth/SessionDb.h"
#include "Auth/UserDb.h"

class TSRuleStore;

namespace SFwallCore {
	class PlainConnTracker;
};

class TrafficShaper {
public:
	Lock lock;

	TokenBucket* add(SFwallCore::Packet* packet);
	void del(TokenBucket*);

	void push(TokenBucket* bucket, struct sq_packet* packet);

	/*Top up all the buckets*/
	void topup();

	/*Run through everything*/
	void loop();

	void start();
	void stop();

	TSRuleStore* store;

	void addBucket(TokenBucket*);

	TrafficShaper(ConfigurationManager* config, GroupDb* groupDb, SessionDb* sessionDb, SFwallCore::PlainConnTracker* connections);
	TrafficShaper(){}
	~TrafficShaper();
private:
	std::vector<TokenBucket*> bucket;
	std::string ruleFile;

	ConfigurationManager* config;
	SLogger::LogContext* logger;
	GroupDb* groupDb;
	SessionDb* sessionDb;
	SFwallCore::PlainConnTracker* connections;
};

#endif
