/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <fstream>
#include <map>

using namespace std;
using std::string;

#include "Core/System.h"
#include "Utils/Utils.h"
#include "Auth/User.h"
#include "Auth/UserDb.h"
#include "Utils/Hash.h"
#include "Auth/SessionDb.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"

UserDb::~UserDb() {
	for (map<std::string, User*>::iterator iter = users.begin();
			iter != users.end();
			++iter) {
		delete iter->second;
	}
}

void UserDb::loadDb() {
	logger->log(SLogger::EVENT, "Loading User Database");

	if (!config->has("userDb")) {
		logger->log(SLogger::ERROR, "Loading user database failed, no users found. Creating default admin user account");
		/*Lets load into the db a default admin user*/

		User* defaultAdminUser = new User("admin");
		defaultAdminUser->setCreationTime(time(NULL));
		defaultAdminUser->setPassword("admin");
		Group* group = groupDb->getGroup(1);

		logger->log(SLogger::EVENT,"Adding default admin user to group --> " + group->getName());
		logger->log(SLogger::EVENT, "Does this group have MUI access ? --> " + group->isAllowMui());

		defaultAdminUser->addGroup(group);
		defaultAdminUser->enable();
		defaultAdminUser->setAuthenticationType(LOCAL_DB);
		users.insert(users.begin(), pair<string, User*>(defaultAdminUser->getUserName(), defaultAdminUser));
		save();
	} else {

		ObjectContainer* root = config->getElement("userDb");
		ObjectContainer* users = root->get("users")->container();
		for (int x = 0; x < users->size(); x++) {
			ObjectContainer* o = users->get(x)->container();
			User* temp = NULL;

			temp = new User();

			temp->m_user = o->get("username")->string();
			temp->m_fname = o->get("firstname")->string();
			temp->m_lname = o->get("lastname")->string();
			temp->m_password = o->get("password")->string();
			temp->isEnabled = o->get("enabled")->boolean();
			temp->m_lastLogin = o->get("lastLogin")->number();
			temp->m_lastFail = o->get("lastFail")->number();
			temp->m_email = o->get("email")->string();
			temp->setAuthenticationType((AuthenticationType) o->get("authenticationType")->number());
			temp->setCreationTime(o->get("createdTime")->number());			

			QuotaInfo* quota = temp->getQuota();
			quota->dailyQuota = o->get("dailyQuota")->boolean();
			quota->dailyQuotaLimit = o->get("dailyQuotaLimit")->number();

			quota->weeklyQuota= o->get("weeklyQuota")->boolean();
			quota->weeklyQuotaLimit = o->get("weeklyQuotaLimit")->number();

			quota->monthQuota = o->get("monthQuota")->boolean();
			quota->monthQuotaIsSmart = o->get("monthQuotaIsSmart")->boolean();
			quota->monthQuotaLimit = o->get("monthQuotaLimit")->number();
			quota->monthQuotaBillingDay = o->get("monthQuotaBillingDay")->number();

			quota->totalQuota = o->get("totalQuota")->boolean();
			quota->totalQuotaLimit = o->get("totalQuotaLimit")->number();

			quota->timeQuota= o->get("timeQuota")->boolean();
			quota->timeQuotaLimit = o->get("timeQuotaLimit")->number();

			ObjectContainer* groups = o->get("groups")->container();
			for (int gid = 0; gid < groups->size(); gid++) {
				ObjectContainer* jsonGroup = groups->get(gid)->container();
				Group *g = groupDb->getGroup(jsonGroup->get("id")->number());

				if (g != NULL) {
					temp->m_groups.push_back(g);
				}
			}
			this->users[temp->m_user] = temp;
		}
	}
}

/**
  Saves changes to the user database.

  @params lockIt - determines if this method should lock the user DB.
  Defaults to true (see UserDb.h).  Set to false if calling routine
  has user DB already locked.
 **/
void UserDb::save(bool lockIt) {
	if (lockIt) {
		holdLock();
	}

	if(config){
		map<string, User*>::iterator iter = users.begin();

		ObjectContainer* root = new ObjectContainer(CREL);
		ObjectContainer* array = new ObjectContainer(CARRAY);
		while (iter != users.end()) {
			User* source = iter->second;

			ObjectContainer* user = new ObjectContainer(CREL);
			user->put("username", new ObjectWrapper((string) source->m_user));
			user->put("firstname", new ObjectWrapper((string) source->m_fname));
			user->put("lastname", new ObjectWrapper((string) source->m_lname));
			user->put("enabled", new ObjectWrapper((bool) source->isEnabled));
			user->put("lastLogin", new ObjectWrapper((double) source->m_lastLogin));
			user->put("lastFail", new ObjectWrapper((double) source->m_lastFail));
			user->put("email", new ObjectWrapper((string) source->m_email));
			user->put("password", new ObjectWrapper((string) source->m_password));
			user->put("createdTime", new ObjectWrapper((double) source->getCreationTime()));

			QuotaInfo* quotas = source->getQuota();
			user->put("dailyQuota", new ObjectWrapper((bool) quotas->dailyQuota));
			user->put("dailyQuotaLimit", new ObjectWrapper((double) quotas->dailyQuotaLimit));

			user->put("weeklyQuota", new ObjectWrapper((bool) quotas->weeklyQuota));
			user->put("weeklyQuotaLimit", new ObjectWrapper((double) quotas->weeklyQuotaLimit));

			user->put("monthQuota", new ObjectWrapper((bool) quotas->monthQuota));
			user->put("monthQuotaIsSmart", new ObjectWrapper((bool) quotas->monthQuotaIsSmart));
			user->put("monthQuotaLimit", new ObjectWrapper((double) quotas->monthQuotaLimit));
			user->put("monthQuotaBillingDay", new ObjectWrapper((double) quotas->monthQuotaBillingDay));

			user->put("totalQuota", new ObjectWrapper((bool) quotas->totalQuota));
			user->put("totalQuotaLimit", new ObjectWrapper((double) quotas->totalQuotaLimit));

			user->put("timeQuota", new ObjectWrapper((bool) quotas->timeQuota));
			user->put("timeQuotaLimit", new ObjectWrapper((double) quotas->timeQuotaLimit));


			ObjectContainer* groups = new ObjectContainer(CARRAY);
			for (int x = 0; x < (int) source->m_groups.size(); x++) {
				ObjectContainer* group = new ObjectContainer(CREL);
				group->put("id", new ObjectWrapper((double) source->m_groups[x]->getId()));
				groups->put(new ObjectWrapper(group));
			}

			user->put("groups", new ObjectWrapper(groups));
			user->put("authenticationType", new ObjectWrapper((double) source->getAuthenticationType()));
			array->put(new ObjectWrapper(user));
			iter++;
		}

		root->put("users", new ObjectWrapper(array));
		config->setElement("userDb", root);
		config->save();
	}

	if (lockIt) {
		releaseLock();	
	}
}

User* UserDb::createUser(string username) {
	User* user = new User(username);
	users[user->m_user] = user;
	user->setAuthenticationType(LOCAL_DB);
	user->setCreationTime(time(NULL));

	save();
	return user;
}

bool UserDb::delUser(User* user) {
	holdLock();

	for(std::list<DeleteUserListener*>::iterator liter = deleteListeners.begin();
			liter != deleteListeners.end();
			liter++){

		(*liter)->handleDeleteUser(user);
	}

	map<string, User*>::iterator iter = users.find(user->m_user);

	if (iter != users.end()) {
		users.erase(iter);
		save(false); // We already have user DB locked.
		delete user;
	}

	releaseLock();
	return true;
}

User *UserDb::getUser(string username) {

	map<string, User*>::iterator iter = users.find(username);
	if (iter != users.end()) {
		return iter->second;
	}

	return NULL;
}

bool UserDb::authenticateUser(string username, string password) {
	bool success = false;

	if (username.size() < 1) {
		success = false;
	}

	User *u = getUser(username);

	if (u != NULL) {
		if (u->isEnabled == true) {
			Hash h;
			string pword_hash = h.create_hash(password, password.size());
			if (h.check_hash(pword_hash, u->m_password) || password.size() == 0) {
				u->m_lastLogin = time(NULL);
				save();

				success = true;
			} else {
				u->m_lastFail = time(NULL);
				save();
				success = false;
			}
		}
	} else {
		success = false;
	}

	return success;
}

vector<string> UserDb::list() {
	vector<string> ret;

	map<string, User*>::iterator iter = users.begin();
	while (iter != users.end()) {
		ret.push_back(iter->first);
		++iter;
	}
	return ret;
}

void UserDb::disableUser(User* user) {
	user->disable();

	//Find all current sessions and remove them:
	sessionDb->holdLock();
	std::list<Session*> sessions = sessionDb->findSessionsForUser(user);
	for (std::list<Session*>::iterator iter = sessions.begin(); iter != sessions.end(); iter++) {
		Session* session = (*iter);
		sessionDb->deleteSession(session);
	}
	sessionDb->releaseLock();
}

void UserDb::enableUser(User* user) {
	user->enable();
}

void UserDb::persistUserFromExternalSource(User* user) {
	users[user->getUserName()] = user;
	save();
}

void UserDb::setSessionDb(SessionDb* sessionDbPtr) {
	sessionDb = sessionDbPtr;
	registerDeleteListener(sessionDbPtr);
}

void UserDb::groupRemoved(Group* group){
	holdLock();
	for(map<string, User*>::iterator iter = users.begin();
		iter != users.end();
		iter++){
		
		User* user = iter->second;
		if(user->checkForGroup(group)){
			user->removeGroup(group);
		}	
	}
	releaseLock();
}
