/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <gtest/gtest.h>

using namespace std;

#include "Core/SysMonitor.h"
#include "Auth/UserDb.h"
#include "Auth/User.h"
#include "Auth/GroupDb.h"
#include "Utils/TimeWrapper.h"

class CustomQuotaWatch : public QuotaWatch {
	public:
		CustomQuotaWatch(long limit, UserDb* userDb, GroupDb* groupDb) 
			: limit(limit), QuotaWatch(userDb, groupDb, NULL) {}

	protected:
		long getTransfer(User* user, long start, long end){
			return limit;
		}	
		long limit;
};

TEST(normalQuota, not_exceeded){
	GroupDb* groupDb = new GroupDb(NULL, NULL);
	UserDb* userDb = new UserDb(NULL, groupDb, NULL, NULL, NULL);

	CustomQuotaWatch* watch = new CustomQuotaWatch(10, userDb, groupDb);	
	User* user = userDb->createUser("michael");
	user->getQuota()->dailyQuota = true;
	user->getQuota()->dailyQuotaLimit = 100 ;

	EXPECT_FALSE(watch->evaluateQuotaForUser(user));
}

TEST(normalQuota, exceeded){
	GroupDb* groupDb = new GroupDb(NULL, NULL);
	UserDb* userDb = new UserDb(NULL, groupDb, NULL, NULL, NULL);

	CustomQuotaWatch* watch = new CustomQuotaWatch(10, userDb, groupDb);	

	User* user = userDb->createUser("michael");
        user->getQuota()->dailyQuota = true;
        user->getQuota()->dailyQuotaLimit = 5;

	EXPECT_TRUE(watch->evaluateQuotaForUser(user));
}

class SmartQuotasCustomQuotaWatch1 : public QuotaWatch {
        public:
                SmartQuotasCustomQuotaWatch1(long alreadySpent, long limit, UserDb* userDb, GroupDb* groupDb)
                        : alreadySpent(alreadySpent), limit(limit), QuotaWatch(userDb, groupDb, NULL) {}

        protected:
                long getTransfer(User* user, long start, long end){
			if(start == 1326110400 && end == 1326106800){
				//This was for the month to date:
				return alreadySpent;	
			}
                        return limit;
                }
                long limit;
		long alreadySpent;

};

TEST(smartGroupQuota, not_exceeded){
	GroupDb* groupDb = new GroupDb(NULL, NULL);
	UserDb* userDb = new UserDb(NULL, groupDb, NULL, NULL, NULL);

	SmartQuotasCustomQuotaWatch1* watch = new SmartQuotasCustomQuotaWatch1(10, 2, userDb, groupDb);	

	User* user = userDb->createUser("michael");
        Group* group = groupDb->createGroup("group1");
        user->addGroup(group);

        group->getQuota()->monthQuota= true;
        group->getQuota()->monthQuotaLimit= 70;
        group->getQuota()->monthQuotaIsSmart= true;
        group->getQuota()->monthQuotaBillingDay= 1;

	Time t("2012-05-10");
	EXPECT_FALSE(watch->evaluateQuotaForUser(t.timestamp(), user));
}

TEST(smartGroupQuota, exceeded){
	GroupDb* groupDb = new GroupDb(NULL, NULL);
	UserDb* userDb = new UserDb(NULL, groupDb, NULL, NULL, NULL);

	SmartQuotasCustomQuotaWatch1* watch = new SmartQuotasCustomQuotaWatch1(10, 4, userDb, groupDb);	

	User* user = userDb->createUser("michael");
	Group* group = groupDb->createGroup("group1");
	user->addGroup(group);	

        group->getQuota()->monthQuota= true;
        group->getQuota()->monthQuotaLimit=70;
        group->getQuota()->monthQuotaIsSmart= true;
        group->getQuota()->monthQuotaBillingDay= 1;

	Time t("2012-05-10");
	EXPECT_TRUE(watch->evaluateQuotaForUser(t.timestamp(), user));
}
