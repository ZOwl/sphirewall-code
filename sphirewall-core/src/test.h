/* This file is part of Sphirewall, it comes with a copy of the license in LICENSE.txt, which is also available at http://sphirewall.net/LICENSE.txt */

#ifndef test_h
#define test_h

#include <iostream>

#include <gtest/gtest.h>
#include "Json/JSON.h"
#include "Api/JsonManagementService.h"
#include "Auth/User.h"
#include "Auth/Group.h"
#include "Auth/GroupDb.h"
#include "Core/ConfigurationManager.h"
#include "Core/HostDiscoveryService.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/TrafficShaper/TrafficShaper.h"

using namespace std;

class MockFactory {
        private:
                GroupDb* groupDb ;
                UserDb* userDb ;
		SFwallCore::ACLStore* store;
		SFwallCore::Firewall* firewall;
	
        public:
                MockFactory(){
                        groupDb= NULL;
                        userDb = NULL;
			store = NULL;
			firewall = NULL;
                }

                SLogger::LogContext* givenLogger(){
                        SLogger::LogContext* logContext = new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing");
                        SLogger::Logger* logger = new SLogger::Logger();
                        return logContext;
                }

                SessionDb* givenSessionDb(){
                        return new SessionDb(givenLogger(), NULL, NULL);
                }

                GroupDb* givenGroupDb(){
                        if(groupDb == NULL){
                                groupDb = new GroupDb(givenLogger(), new ConfigurationManager());
                        }
                        return groupDb;
                }

                UserDb* givenUserDb(){
                        if(userDb == NULL){
                                userDb = new UserDb(NULL, givenGroupDb(), givenLogger(), new ConfigurationManager(), givenSessionDb());
                        }

                        return userDb;
                }


		SFwallCore::Firewall* givenFw(){
			if(firewall == NULL){
				firewall = new SFwallCore::Firewall();
				firewall->trafficShaper = new TrafficShaper(); 
				firewall->trafficShaper->store = new TSRuleStore(givenLogger(), NULL, NULL, NULL, NULL);
			}
			return firewall;
		}

		SFwallCore::ACLStore* givenRuleStore(){
			if(store == NULL){
				store = new SFwallCore::ACLStore(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), NULL, NULL, NULL, NULL);
				store->setConfigurationManager(new ConfigurationManager());
				givenFw()->acls = store;
			}
			return store;
		}

		void reset(){
			delete groupDb;
			delete userDb;

			groupDb = NULL;
			userDb = NULL;
		}

};

void expectException(Delegate* delegate, std::string path, JSONObject args);
#endif
