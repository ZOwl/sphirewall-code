/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <stdarg.h>
#include <limits.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <sstream>
#include <vector>

using namespace std;

#include "SFwallCore/Utils.h"
#include "Utils/Checksum.h"
#include "Utils/IP4Addr.h"
#include "SFwallCore/Capture.h"
#include "SFwallCore/Acl.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/ConnTracker.h"
#include "BandwidthDb/Bandwidth.h"
#include "Core/HostDiscoveryService.h"

void SFwallCore::Utils::setRstFlag(unsigned char * packet, int len) {
	struct iphdr* packet_header = NULL;
	struct tcphdr* packet_tcp_header = NULL;

	packet_header = (struct iphdr*) packet;
	if (packet_header->protocol == IPPROTO_TCP) {
		packet_tcp_header = (struct tcphdr *) (packet + (packet_header->ihl << 2));

		/*Now decide what to replay to this socket*/
		packet_header->check = 0;
		packet_header->check = Checksum::checksum((unsigned short *) packet, sizeof (struct iphdr));

		packet_tcp_header->rst = 1;
		packet_tcp_header->check = 0;
		packet_tcp_header->check = Checksum::get_tcp_checksum(packet_header, packet_tcp_header);
	}
}

void SFwallCore::Utils::redirectToHTTPAuthPortal(unsigned char * packet, int len, std::string url) {
	struct iphdr* packet_header = NULL;
	struct tcphdr* packet_tcp_header = NULL;

	packet_header = (struct iphdr*) packet;
	if (packet_header->protocol == IPPROTO_TCP) {
		packet_tcp_header = (struct tcphdr *) (packet + (packet_header->ihl << 2));
		packet_tcp_header->rst = 0;
		/*Now decide what to replay to this socket*/
		int tcpdatalen = len - (packet_tcp_header->doff * 4) - (packet_header->ihl * 4);

		/*Only modify packets with data - allow ack/syn to pass through*/
		if (tcpdatalen > 0) {
			/*Dodgy little return packet*/
			string s;
			s += "HTTP/1.1 307 " + url;
			s += "\nLocation: " + url;
			s += "\nContent-Length:0\n\n\n";

			int hLen = len - tcpdatalen;
			bzero((unsigned char*) packet + hLen, len - hLen);
			memcpy((unsigned char*) packet + hLen, s.c_str(), s.size());

			packet_header->check = 0;
			packet_header->check = packet_header->check = Checksum::checksum((unsigned short *) packet, sizeof (struct iphdr));

			packet_tcp_header->check = 0;
			packet_tcp_header->check = htons(Checksum::get_tcp_checksum(packet_header, packet_tcp_header));
		}
	}
}

