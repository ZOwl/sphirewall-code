/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ACL_H
#define ACL_H

#include <vector>
#include "Packet.h"
#include "SFwallCore/Alias.h"
#include "Core/Logger.h"
#include "Auth/UserDb.h"
#include "Auth/GroupDb.h"
#include "Auth/SessionDb.h"
#include "Utils/NetDevice.h"

class ConfigurationManager;
class ObjectContainer;
namespace SFwallCore {

	enum NAT_TYPE {
		NO_NAT = -1, DNAT = 0, SNAT = 1
	};

	class Alias;
	class AliasDb;

	class Nat {
	public:

		Nat() {
			type = NO_NAT;
		};

		int target;
		NAT_TYPE type;
	};

	class Rule {
	public:
		Rule();
		std::string id;
		int sport_start, sport_end;
		int dport_start, dport_end;

		std::set<int> dportList, sportList;
		void setDportList(std::string);
		void setSportList(std::string);
		std::string getDportList();
		std::string getSportList();

		int action;

		int source, sourceMask;
		int dest, destMask;
	
		Alias* sourceAlias;
		Alias* destAlias;

		string sourceDevName; string destDevName;
		NetDevice* sourceDevPtr; NetDevice* destDevPtr;
		string hwAddr;

		int groupid;

		proto type;
		string comment;
	
		bool enabled;
		bool valid;
                int count;

		virtual std::string stringAction() = 0;
		virtual std::string toString() = 0;

		virtual void setNatPort(int port) {
		}

		virtual void setNatTarget(in_addr_t host) {
		}

		virtual void setNatType(NAT_TYPE type) {
		}
	
                virtual void setNatTargetDevice(NetDevice* device){
		}

		NetDevice* getSourceDevice(){
			return this->sourceDevPtr;		
		}

		NetDevice* getDestDevice(){
			return this->destDevPtr;
		}
	
		bool supermatch(Packet* packet, bool& capturePortal, SessionDb*, GroupDb*);
		bool isActive(){
			return valid && enabled;
		}

		int startHour;
		int finishHour;
		bool ignoreconntrack;
	};

	class FilterRule : public Rule {
	public:
		FilterRule() : Rule(){
			log = false;
		}
		std::string stringAction();
		std::string toString();
		bool log;
	};

	class NatRule : public Rule {
	public:
		NatRule(): Rule(){
			natPort = -1;
			natTarget = -1;
			natTargetDevicePtr = NULL;

			//default natType is DNAT:
			natType = DNAT;
		}

		std::string stringAction();
		std::string toString();

		in_port_t natPort;
		in_addr_t natTarget;
		NetDevice* natTargetDevicePtr;		
		std::string natTargetDevice;
		NAT_TYPE natType;

		void setNatPort(int port) {
			this->natPort = port;
		}

		void setNatTarget(in_addr_t host) {
			this->natTarget = host;
		}

		void setNatType(NAT_TYPE type) {
			this->natType = type;
		}
		
		void setNatTargetDevice(NetDevice* device){
			this->natTargetDevicePtr = device;
		}
		
		in_addr_t getUsableNatTargetIp() {
			if(natTarget != -1){
				return natTarget;
			}else if(natTargetDevicePtr != NULL){
				return natTargetDevicePtr->getIp_Int();
			}

			return -1;
		}
	};

	class PriorityRule : public Rule {
		public:
			PriorityRule(): Rule(){
				nice = 1;
			}

			std::string stringAction(){
				return "qos";
			}
			std::string toString(){
				return "qos-rule";
			}

			int nice;
	};

	class ACLStore : public AliasRemovedListener{
		public:
			class InterfaceChangeListener : public virtual Listener {
				public:
					InterfaceChangeListener(ACLStore* store) :
						store(store){}

					void event();
				private:
					ACLStore* store;
			};

			ACLStore(SLogger::LogContext* logger, UserDb*, GroupDb*, SessionDb*, AliasDb* aliases);
			ACLStore() {
				this->logger = NULL;
			}
			~ACLStore();
			int loadAclEntry(Rule*);
			int unloadAclEntry(Rule*);

			Rule* getRuleById(std::string id);	
			void movedownByPos(int rulePos);
			void movedown(Rule*);
			void moveupByPos(int rulePos);
			void moveup(Rule*);

			FilterRule* matchFilterAcls(Packet*, bool&);
			NatRule* matchNatAcls(Packet*, NAT_TYPE natType);
			PriorityRule* matchPriorityQos(Packet* packet);

			void loadFilterFile();
			void loadNatFile();
			void initLazyObjects(SFwallCore::Rule*);			

			int createAclEntry(Rule* e);
			int deleteAclEntry(Rule* e);
			int deleteAclEntryByPos(int rulePos);

			void save();
			void setConfigurationManager(ConfigurationManager* configurationManager) {
				this->configurationManager = configurationManager;
			}

			void setInterfaceManager(InterfaceManager* interfaceManager){
				this->interfaceManager = interfaceManager;
			}

			void aliasRemoved(Alias* alias);

			vector<FilterRule*>& listFilterRules(){
				return entries;
			}	

			vector<NatRule*>& listNatRules(){
				return natEntries;
			}

			vector<PriorityRule*>& listPriorityRules(){
				return priorityQosRules;
			}

			set<unsigned int>& getDenyList(){
				return denyList;
			}

			void addToDenyList(unsigned int i);
			void removeFromDenyList(unsigned int i);
			bool existsInDenyList(Packet* packet);
			void loadDenyList();
                        void loadPriorityFile();
			void saveDenyList();

			void getlock(){
				lock->lock();	
			}

			void releaselock(){
				lock->unlock();
			}
		private:
			void saveFilterFile();
			void saveNatFile();
			void savePriorityFile();
	
			int findPos(Rule* rule);
			static const string FIELD_DELIM;
			static const string tagFieldDelim;
			static const int fieldsNumber = 19;

			SLogger::LogContext* logger;
			UserDb* userDb;
			GroupDb* groupDb;
			SessionDb* sessionDb;

			string natFileName;
			string filterFileName;
			AliasDb* aliases;

			ConfigurationManager* configurationManager;
			InterfaceManager* interfaceManager;

			vector<FilterRule*> entries;
			vector<NatRule*> natEntries;
			vector<PriorityRule*> priorityQosRules;
			set<unsigned int> denyList;

			void serializeRule(ObjectContainer* rule, SFwallCore::Rule* target);
			void deserializeRule(ObjectContainer* source, SFwallCore::Rule* entry);

			Lock* lock;
	};

}

#endif
