/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_TOKENBUCKET_H
#define SPHIREWALL_TOKENBUCKET_H

#include <queue>
#include <ctime>
#include "SFwallCore/Packet.h"
#include "TokenFilter.h"
#include "Utils/Lock.h"

class TokenBucket {
public:
	TokenFilter* filter;
	TokenBucket(long upload, long download, TokenFilter* filter);

	void addPacketToQueue(struct sq_packet* pk, bool isDownload);
	void rollBucket();
	bool hasExpired();

	int getTokens() {
		return uploadTokens + downloadTokens;
	}

private:
	void flip();
	bool canFlip();
	timeval t;

	int uploadTokens;
	int downloadTokens;

	std::queue<struct sq_packet*> uploadPacketQueue;
	std::queue<struct sq_packet*> downloadPacketQueue;

	long uploadRate;
	long downloadRate;

	Lock* lock;
};

#endif
