/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CLOUD_H
#define CLOUD_H
#include "Core/Logger.h"

class ConfigurationManager;
class CloudConnection {
	public:
		CloudConnection(ConfigurationManager* cm, SLogger::LogContext* logger) 
			: config(cm), logger(logger){
			this->connectedFlag = false;
		}

		std::string getHostname();
		std::string getKey();
		std::string getDeviceId();

		bool status();
		void connect();
		void disconect();			

		int getPort();
		bool enabled();

		bool connected();
	private:
		ConfigurationManager* config;
		SLogger::LogContext* logger;
		bool connectedFlag;
};

#endif
