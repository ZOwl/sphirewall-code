/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_NATCONN_H
#define SPHIREWALL_NATCONN_H

#include "State.h"
#include "Packet.h"
#include "Acl.h"
#include "Utils/Lock.h"
#include "Auth/User.h"
#include "TrafficShaper/TokenBucket.h"
#include "Exceptions.h"

namespace SFwallCore {

	class NatConn {
	public:
		NatConn(const Packet& packet, NatRule* rule);

		virtual ~NatConn() {
		};

		virtual void translateRawPacket(unsigned char* rawPacket, PacketDirection pd) const = 0;
		virtual int hash() const = 0;

		virtual proto getType() const;
		virtual NAT_TYPE getNatType() const;

		virtual bool hasExpired() = 0;

		int idleTime() const;
		void updateLastTime();

		virtual bool checkPacket(const Packet* packet, PacketDirection filter) const = 0;

	protected:
		int m_lastTime;

		in_addr_t m_sourceIp, m_destIp;
		NatRule* m_rule;
	};

	class DNatConn : public NatConn {
	protected:

		virtual bool compareSource(DNatConn* conn);

	public:

		DNatConn(const Packet& packet, NatRule* rule) : NatConn(packet, rule) {
		}

		virtual ~DNatConn() {
		}

		NAT_TYPE getNatType() const;
	};

	class SNatConn : public NatConn {
	protected:

		friend class SNatConnTracker;

		typedef unsigned short NatId;
		static const int NAT_ID_FROM = 1024;
		static const int NAT_ID_TO = 65535;

		NatId m_snatId;

		virtual NatId getOrigSNatId() = 0;

		virtual bool compareDestination(SNatConn* conn);

		vector<in_port_t> occupiedLocalPorts;
	public:

		SNatConn(const Packet& packet, NatRule* rule) : NatConn(packet, rule) {
		}

		virtual ~SNatConn() {
		}

		NAT_TYPE getNatType() const;

	};

	class TcpUdpDNatConn : public DNatConn {
	public:

		virtual ~TcpUdpDNatConn() {
		}

		TcpUdpDNatConn(const Packet& packet, NatRule* rule) : DNatConn(packet, rule) {
		}

	protected:
		in_addr_t m_sport, m_dport;
	};

	class TcpDNatConn : public TcpTrackable, public TcpUdpDNatConn {
	public:
		TcpDNatConn(const TcpPacket& tcpPacket, NatRule* rule);

		~TcpDNatConn() {
		}

		bool checkPacket(const Packet* packet, PacketDirection filter) const;
		int hash() const;
		void translateRawPacket(unsigned char* rawPacket, PacketDirection pd) const;
		static int hash(const TcpPacket* tcpPacket, PacketDirection pd);

		TcpState* getState();
		proto getType() const;

		unsigned int getSrcIp() const;
		unsigned int getDstIp() const;

		bool hasExpired();
	private:
		unsigned int m_source;
		unsigned int m_dest;
	};

	class UdpDNatConn : public TcpUdpDNatConn {
	public:

		UdpDNatConn(const UdpPacket& udpPacket, NatRule* rule) : TcpUdpDNatConn(udpPacket, rule) {
			m_sport = udpPacket.getSrcPort();
			m_dport = udpPacket.getDstPort();
		}

		~UdpDNatConn() {
		}

		bool checkPacket(const Packet* packet, PacketDirection filter) const;
		int hash() const;
		void translateRawPacket(unsigned char* rawPacket, PacketDirection pd) const;
		static int hash(const UdpPacket* udpPacket, PacketDirection pd);

		bool hasExpired();
		proto getType() const;
	};

	class IcmpDNatConn : public DNatConn {
	public:

		IcmpDNatConn(const IcmpPacket& icmpPacket, NatRule* rule) : DNatConn(icmpPacket, rule) {
			m_icmpId = icmpPacket.getIcmpId();
		}

		~IcmpDNatConn() {
		}

		void translateRawPacket(unsigned char* rawPacket, PacketDirection pd) const;
		bool checkPacket(const Packet* packet, PacketDirection filter) const;
		int hash() const;
		static int hash(const IcmpPacket* icmpPacket, PacketDirection pd);
		proto getType() const;

		bool hasExpired();
	protected:
		unsigned short m_icmpId;
	};

	class TcpUdpSNatConn : public SNatConn {
	public:

		TcpUdpSNatConn(const Packet& packet, NatRule* rule) : SNatConn(packet, rule) {
		}

		virtual ~TcpUdpSNatConn() {
		}
		NatId getOrigSNatId();
	protected:
		in_addr_t m_sport, m_dport;
	};

	class TcpSNatConn : public TcpTrackable, public TcpUdpSNatConn {
	public:
		TcpSNatConn(const TcpPacket& tcpPacket, NatRule* rule);

		~TcpSNatConn() {
		}

		bool checkPacket(const Packet* packet, PacketDirection filter) const;
		void translateRawPacket(unsigned char* rawPacket, PacketDirection pd) const;
		int hash() const;
		static int hash(const TcpPacket* tcpPacket, PacketDirection pd);
		proto getType() const;
		TcpState* getState();

		unsigned int getDstIp() const;
		unsigned int getSrcIp() const;

		bool hasExpired();
	private:
		unsigned int m_source;
		unsigned int m_dest;
	};

	class UdpSNatConn : public TcpUdpSNatConn {
	public:

		UdpSNatConn(const UdpPacket& udpPacket, NatRule* rule) : TcpUdpSNatConn(udpPacket, rule) {
			m_sport = udpPacket.getSrcPort();
			m_dport = udpPacket.getDstPort();
		}

		~UdpSNatConn() {
		}

		bool checkPacket(const Packet* packet, PacketDirection filter) const;
		int hash() const;
		void translateRawPacket(unsigned char* rawPacket, PacketDirection pd) const;
		static int hash(const UdpPacket* udpPacket, PacketDirection pd);
		proto getType() const;

		bool hasExpired();
	};

	class IcmpSNatConn : public SNatConn {
	public:

		IcmpSNatConn(const IcmpPacket& icmpPacket, NatRule* rule) : SNatConn(icmpPacket, rule) {
			m_icmpId = icmpPacket.getIcmpId();
		}

		~IcmpSNatConn() {
		}

		void translateRawPacket(unsigned char* rawPacket, PacketDirection pd) const;
		NatId getOrigSNatId();
		bool checkPacket(const Packet* packet, PacketDirection filter) const;
		int hash() const;
		static int hash(const IcmpPacket* icmpPacket, PacketDirection pd);
		proto getType() const;

		bool hasExpired();
	protected:
		unsigned short m_icmpId;
	};
};

#endif
