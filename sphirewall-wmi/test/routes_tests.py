#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester

class RoutesTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login("fw", "admin", "admin")

    def gotoRoutes(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Networking')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Devices')]").click()

    def test_SavingInterface(self):
        self.gotoRoutes()

        self.tester.driver.find_element_by_name("dest").send_keys("1.2.3.4/24")
        self.tester.driver.find_element_by_name("gw").send_keys("127.0.0.1")
        self.tester.driver.find_element_by_xpath("//select[@name='device']/option[text()='eth0']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(text(), '1.2.3.4')]//ancestor::tr/td[contains(text(), '127.0.0.1')]//ancestor::tr/td[contains(text(), 'eth0')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(text(), '1.2.3.4')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(text(), '1.2.3.4')]//ancestor::tr/td[contains(text(), '127.0.0.1')]"))

    def tearDown(self):
        self.tester.disconnect()
