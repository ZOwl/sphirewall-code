#Copyright Michael Lawson, John Deal
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

from _socket import SOCK_STREAM, AF_INET, socket
import json
import ssl, pprint
from socket import socket # JRD

class ConnectionDetail:
    token    = None
    hostname = None;
    port     = None;
    username = None
    ssl      = False

    def __init__(self, token, hostname, port, username, ssl):
        self.token    = token
        self.hostname = hostname
        self.port     = port
        self.username = username
        self.ssl      = ssl

    def send(self, data):
        s = socket(AF_INET, SOCK_STREAM)
        s.connect((self.hostname, int(self.port)))

        if self.ssl == True:
            sslSocket = ssl.wrap_socket(s)
            # print repr(sslSocket.getpeername())
            # print sslSocket.cipher()
            # print pprint.pformat(sslSocket.getpeercert())

            sslSocket.sendall(json.dumps(data) + "\n")
        else:
            s.sendall(json.dumps(data) + "\n")

        responseBuffer = "";

        while 1:
            if self.ssl == True:
		        # Hangs on 2nd read so give it 10 MB.  JRD
                data = sslSocket.read(1024 * 10000)
            	responseBuffer = data; # HACK!
            else:
                data = s.recv(1024)

            if not data or self.ssl == True:
                break;

            responseBuffer += data;
        return responseBuffer;


    def request(self, type, args):
        req = {'request':type, 'token':self.token,'args':args};
        response = self.send(req)

        json_loads = json.loads(response)
        if json_loads['code'] == 0:
            return json_loads['response'];
        else:
            print json_loads['message'];
            return -1;
