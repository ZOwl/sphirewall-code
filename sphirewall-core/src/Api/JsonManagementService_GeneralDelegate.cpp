/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <boost/regex.hpp>
#include <sstream>

#include "Core/System.h"
#include "Api/JsonManagementService.h"
#include "Json/JSONValue.h"
#include "Json/JSON.h"
#include "Core/Config.h"
#include "Core/SysMonitor.h"
#include "Utils/EmailSender.h"
#include "Core/Cloud.h"
#include "Core/Event.h"
#include "Auth/AuthenticationHandler.h"
#include "Core/HostDiscoveryService.h"

using namespace std;

JSONObject GeneralDelegate::process(string uri, JSONObject object) {
	if (uri.compare("general/config/get") == 0) {
		return config_get(object);
	} else if (uri.compare("general/config/set") == 0) {
		return config_set(object);
	} else if (uri.compare("general/config/set/bulk") == 0) {
		return config_set_bulk(object);
	} else if (uri.compare("general/runtime/list") == 0) {
		return runtime_list(object);
	} else if (uri.compare("general/runtime/get") == 0) {
		return runtime_get(object);
	} else if (uri.compare("general/runtime/set") == 0) {
		return runtime_set(object);
	} else if (uri.compare("general/version") == 0) {
		return version(object);
	} else if (uri.compare("general/logging/list") == 0) {
		return logging_list(object);
	} else if (uri.compare("general/logging/set") == 0) {
		return logging_set(object);
	} else if (uri.compare("general/ldap/testconnection") == 0) {
		return ldap_testconnection(object);
	} else if (uri.compare("general/smtp/testconnection") == 0) {
		return smtp_testconnection(object);
	} else if (uri.compare("general/smtp/publish") == 0) {
		return smtp_publish(object);
	} else if(uri.compare("general/config") == 0){
		return config_dump();
	} else if(uri.compare("general/cloud/connected") == 0){
		return cloud_connected();
	} else if(uri.compare("general/rpc") == 0){
		return rpc(object);
	} else if(uri.compare("general/quotas") == 0){
		return quotas();
	} else if(uri.compare("general/quotas/set") == 0){
		return quotas_set(object);
	} else if(uri.compare("general/logs") == 0){
		return logs(object);
	} else if(uri.compare("general/info") == 0){
		return info(object);
	} else {
		throw new DelegateNotFoundException(uri);
	}
}

JSONObject GeneralDelegate::info(JSONObject obj){
	JSONObject ret;
	ret.put(L"version", new JSONValue((string) System::getInstance()->version));
	ret.put(L"versionName", new JSONValue((string) System::getInstance()->versionName));

	ret.put(L"devices", new JSONValue((double) System::getInstance()->getArp()->size())); 
	ret.put(L"sessions", new JSONValue((double) System::getInstance()->getSessionDb()->list().size())); 
	return ret;	
}

JSONObject GeneralDelegate::logs(JSONObject obj){
	stringstream cmd; cmd << "cat /var/log/syslog";
        if(obj.has(L"filter")){
		string filter = obj[L"filter"]->String();
		if(filter.size() > 0){
			cmd << " | grep " << filter;
		}
        }       

	string response;
	System::exec(cmd.str(), response);
		
	JSONObject ret;
	ret.put(L"log", new JSONValue((string) response));
	return ret;
}

JSONObject GeneralDelegate::quotas(){
	JSONObject ret;
	QuotaWatch* watch = System::getInstance()->getQuotaWatch(); 
	
	ret.put(L"dailyQuota", new JSONValue((bool) watch->getQuota()->dailyQuota));
	ret.put(L"dailyQuotaLimit", new JSONValue((double) watch->getQuota()->dailyQuotaLimit));

	ret.put(L"weeklyQuota", new JSONValue((bool) watch->getQuota()->weeklyQuota));
	ret.put(L"weeklyQuotaLimit", new JSONValue((double) watch->getQuota()->weeklyQuotaLimit));

	ret.put(L"monthQuota", new JSONValue((bool) watch->getQuota()->monthQuota));
	ret.put(L"monthQuotaIsSmart", new JSONValue((bool) watch->getQuota()->monthQuotaIsSmart));
	ret.put(L"monthQuotaLimit", new JSONValue((double) watch->getQuota()->monthQuotaLimit));
	ret.put(L"monthQuotaBillingDay", new JSONValue((double) watch->getQuota()->monthQuotaBillingDay));

	return ret;	
}

JSONObject GeneralDelegate::quotas_set(JSONObject args){
        QuotaWatch* watch = System::getInstance()->getQuotaWatch();
	QuotaInfo* quota = watch->getQuota();
	quota->dailyQuota = args[L"dailyQuota"]->AsBool();
	quota->dailyQuotaLimit = args[L"dailyQuotaLimit"]->AsNumber();
	quota->weeklyQuota= args[L"weeklyQuota"]->AsBool();
	quota->weeklyQuotaLimit = args[L"weeklyQuotaLimit"]->AsNumber();
	quota->monthQuota = args[L"monthQuota"]->AsBool();
	quota->monthQuotaIsSmart = args[L"monthQuotaIsSmart"]->AsBool();
	quota->monthQuotaLimit = args[L"monthQuotaLimit"]->AsNumber();
	quota->monthQuotaBillingDay = args[L"monthQuotaBillingDay"]->AsNumber();
	
	watch->save();
	return JSONObject();	
}

JSONObject GeneralDelegate::rpc(JSONObject obj){
	if(config->getConfigurationManager()->hasByPath("general:rpc") 
			&& config->getConfigurationManager()->get("general:rpc")->boolean()){

		string command = obj[L"command"]->String();
		string ret;
		System::exec(command, ret);

		JSONObject jret;
		jret.put(L"response", new JSONValue((string)ret));

		EventParams params;
		params["command"] = command;
		eventDb->add(new Event(AUDIT_RPC_CALL, params));

		return jret;
	}	

	return JSONObject();
}

JSONObject GeneralDelegate::cloud_connected(){
	JSONObject ret;
	ret.put(L"status", new JSONValue((bool) System::getInstance()->getCloudConnection()->connected()));	
	return ret;
}

JSONObject GeneralDelegate::config_dump(){
	JSONObject ret;
	ret.put(L"dump", new JSONValue((string) config->getConfigurationManager()->dump()));
	return ret;	
}

JSONObject GeneralDelegate::config_get(JSONObject obj) {
	JSONObject ret;
	if (config->getConfigurationManager()->hasByPath(obj[L"key"]->String())) {
		ObjectWrapper* o = config->getConfigurationManager()->get(obj[L"key"]->String());
		if (o->isBoolean()) {
			ret.put(L"value", new JSONValue((bool) o->boolean()));
		} else if (o->isNumber()) {
			ret.put(L"value", new JSONValue((double) o->number()));
		} else if (o->isString()) {
			ret.put(L"value", new JSONValue((string) o->string()));
		} else {
			if (obj.has(L"hideExceptions") && obj[L"hideExceptions"]->AsBool() == true) {
				ret.put(L"value", new JSONValue((string) ""));
			} else {
				throw new DelegateGeneralException("Variable type is not defined, this is very strange");
			}
		}
	} else {
		if (obj.has(L"hideExceptions") && obj[L"hideExceptions"]->AsBool() == true) {
			ret.put(L"value", new JSONValue((string) ""));
		} else {
			throw new DelegateGeneralException("Variable not found in configuration");
		}
	}

	return ret;
}

JSONObject GeneralDelegate::config_set_bulk(JSONObject obj) {
	JSONArray values = obj[L"values"]->AsArray();
	for(int x= 0; x < values.size(); x++){
		JSONObject i = values[x]->AsObject();
		config_set(i);
	}	

        return JSONObject();
}

JSONObject GeneralDelegate::config_set(JSONObject obj) {
	string key = WStringToString(obj[L"key"]->AsString());

	if (obj[L"value"]->IsString()) {
		if(eventDb){
			if(config->getConfigurationManager()->hasByPath(key)){
				string oldvalue = config->getConfigurationManager()->get(key)->string();
				if(oldvalue.compare(obj[L"value"]->String()) != 0){
					EventParams params;
					params["key"] = obj[L"key"]->String();
					params["value"] = obj[L"value"]->String();
					eventDb->add(new Event(AUDIT_CONFIGURATION_CHANGED, params));
				}
			}
		}

		config->getConfigurationManager()->put(key, obj[L"value"]->String());
	} else if (obj[L"value"]->IsBool()) {
		if(eventDb){    
			if(config->getConfigurationManager()->hasByPath(key)){
				if(config->getConfigurationManager()->get(key)->boolean() != obj[L"value"]->AsBool()){
					EventParams params;
					params["key"] = obj[L"key"]->String();
					params["value"] = obj[L"value"]->AsBool();
					eventDb->add(new Event(AUDIT_CONFIGURATION_CHANGED, params));
				}
			}
		}
		config->getConfigurationManager()->put(key, obj[L"value"]->AsBool());
	} else if (obj[L"value"]->IsNumber()) {
		if(eventDb){
			if(config->getConfigurationManager()->hasByPath(key)){
				if(config->getConfigurationManager()->get(key)->number() != obj[L"value"]->AsNumber()){

					EventParams params;
					params["key"] = obj[L"key"]->String();
					params["value"] = obj[L"value"]->AsNumber();
					eventDb->add(new Event(AUDIT_CONFIGURATION_CHANGED, params));
				}
			}
		}
		config->getConfigurationManager()->put(key, obj[L"value"]->AsNumber());
	}

	return JSONObject();
}

JSONObject GeneralDelegate::runtime_list(JSONObject object) {
	list<string> keys;
	config->getRuntime()->listAll(keys);

	JSONArray array;
	list<string>::iterator iter;
	for (iter = keys.begin(); iter != keys.end(); iter++) {
		JSONObject v;
		v.put(L"key", new JSONValue((string) *iter));
		v.put(L"value", new JSONValue((double) config->getRuntime()->get(*iter)));
		array.push_back(new JSONValue((v)));
	}

	JSONObject ret;
	ret.put(L"keys", new JSONValue(array));

	return ret;
}

JSONObject GeneralDelegate::runtime_get(JSONObject object) {
	string key = object[L"key"]->String();
	if (config->getRuntime()->contains(key)) {
		int value = config->getRuntime()->get(key);

		JSONObject ret;
		ret.put(L"value", new JSONValue((double) value));
		return ret;
	} else {
		throw DelegateGeneralException("invalid runtime variable supplied");
	}
}

JSONObject GeneralDelegate::runtime_set(JSONObject object) {
	string key = object[L"key"]->String();
	int value = object[L"value"]->AsNumber();

	if(eventDb){
		if(config->getRuntime()->get(key) != value){

			EventParams params;
			params["key"] = object[L"key"]->String();
			params["value"] = object[L"value"]->AsNumber();
			eventDb->add(new Event(AUDIT_CONFIGURATION_CHANGED, params));
		}
	}
	config->getRuntime()->set(key, value);
	return JSONObject();
}

JSONObject GeneralDelegate::version(JSONObject obj) {
	JSONObject ret;
	ret.put(L"version", new JSONValue((string) System::getInstance()->version));
	ret.put(L"name", new JSONValue((string) System::getInstance()->versionName));

	return ret;
}

JSONObject GeneralDelegate::logging_list(JSONObject obj) {
	JSONObject ret;
	JSONArray arr;

	std::list<string> contexts = loggingConfiguration->getContexts();
	for (list<string>::iterator iter = contexts.begin();
			iter != contexts.end();
			iter++) {

		JSONObject o;
		o.put(L"context", new JSONValue((string) * iter));
		o.put(L"level", new JSONValue((double) loggingConfiguration->getLevel(*iter)));
		arr.push_back(new JSONValue(o));
	}

	ret.put(L"levels", new JSONValue(arr));
	return ret;
}

JSONObject GeneralDelegate::logging_set(JSONObject obj) {
	int ret = loggingConfiguration->setLevel(obj[L"context"]->String(), obj[L"level"]->AsNumber());
	if(ret == -1){
		throw new DelegateGeneralException("Could not find logging context");
	}

	return JSONObject();
}

JSONObject GeneralDelegate::ldap_testconnection(JSONObject o) {
	JSONObject ret;
	AuthenticationMethod* method = authenticationManager->getMethod(LDAP_DB);
	if (method) {
		bool canConnect = method->canConnect();
		ret.put(L"status", new JSONValue((bool) canConnect));
	} else {
		ret.put(L"status", new JSONValue((bool) false));
	}

	return ret;
}

JSONObject GeneralDelegate::smtp_testconnection(JSONObject object){
	JSONObject ret;
	int r = System::getInstance()->getEmailSender()->send(System::getInstance()->getEmailSender()->getDefaultEmail(), "Test Email", "This is a test email from Sphirewall");

	ret.put(L"status", new JSONValue((double) r == 0));
	return ret; 
}

JSONObject GeneralDelegate::smtp_publish(JSONObject object){
	System::getInstance()->getEmailSender()->publish();
	return JSONObject();
}

