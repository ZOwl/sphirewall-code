/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <sstream>
#include <proc/readproc.h>

using namespace std;
using std::string;

#include "Core/SysMonitor.h"
#include "Core/System.h"
#include "Utils/Utils.h"
#include "BandwidthDb/AnalyticsClient.h"
#include "SFwallCore/ConnTracker.h"
#include "Auth/Quota.h"
#include "Utils/TimeWrapper.h"
#include "Core/HostDiscoveryService.h"
#include "Core/Event.h"

class GeneralMetricSampler: public MetricSampler {
	public:
		void sample(map<string, long>& items){
			struct proc_t usage;
			look_up_our_self(&usage);
			items["system.vsize"] = usage.vsize;
			items["system.utime"] = usage.utime;
			items["system.stime"] = usage.stime;
			items["system.priority"] = usage.priority;
			items["system.nice"] = usage.nice;
			items["system.size"] = usage.size;

			System::getInstance()->getSessionDb()->holdLock();
			items["sessiondb.size"] = System::getInstance()->getSessionDb()->list().size();
			items["firewall.arp.size"] = System::getInstance()->getArp()->size();
			System::getInstance()->getSessionDb()->releaseLock();
		}
};

void SysMonitor::pollall() {
	std::list<Watch*>::iterator iter;
	for (iter = registeredWatches.begin();
			iter != registeredWatches.end();
			iter++) {

		(*iter)->poll();
	}

	try {
		AnalyticsClient* client = new AnalyticsClient();

		map<string, long> input;
		for(list<MetricSampler*>::iterator iter = registeredSamplers.begin();
				iter != registeredSamplers.end(); iter++){
			MetricSampler* sampler = (*iter);
			sampler->sample(input);
		}

		client->addMetricSnapshot(input);
		delete client;
	} catch (exception& e) {}
}

void SysMonitor::registerMetric(MetricSampler* sampler){
	registeredSamplers.push_back(sampler);
}

void QuotaWatch::poll() {
	//Check the user quota's first:
	userDb->holdLock();

	bool globalExceeded = evaluateGlobalQuotas();
	alertOnQuota(globalExceeded);

	vector<string> users = userDb->list();
	for (int x = 0; x < users.size(); x++) {
		User* user = userDb->getUser(users[x]);
		if (user) {
			//Check that this guy is still valid:
			bool exceeded = evaluateQuotaForUser(user);	
			alertOnQuota(user, exceeded);
		}
	}
	userDb->releaseLock();
}

bool QuotaWatch::evaluateQuotaForUser(User* user){
	return evaluateQuotaForUser(time(NULL), user);
}

bool QuotaWatch::evaluateGlobalQuotas(){
	if (getQuota()->dailyQuota) {
		long totalTransfer = getTransfer(time(NULL), time(NULL));
		if(totalTransfer > getQuota()->dailyQuotaLimit){
			return true;
		}
	}

	if (getQuota()->weeklyQuota) {
		long totalTransfer = getTransfer(Time::startOfWeek()->timestamp(), time(NULL));

		if(totalTransfer > getQuota()->weeklyQuotaLimit){
			return true;
		}
	}

	int baseTime = time(NULL);
	if (getQuota()->monthQuota) {
		//are we a group/smart quota?
		if(getQuota()->monthQuotaIsSmart){
			Time* lastBillingDate = Time::getLastDayOfMonth(baseTime, getQuota()->monthQuotaBillingDay);
			Time* nextBillingDate = Time::getNextDayOfMonth(baseTime, getQuota()->monthQuotaBillingDay);

			long totalTransfer = 0;
			Time* yesterday = Time::yesterday(baseTime);
			totalTransfer += getTransfer(Time::addDays(lastBillingDate->timestamp(), 1)->timestamp(), yesterday->timestamp());

			long quotaLeft = getQuota()->monthQuotaLimit - totalTransfer;
			int daysLeft = Time::dayDiff(nextBillingDate, yesterday);
			long dailyQuota = quotaLeft / daysLeft;

			delete yesterday;
			delete lastBillingDate;
			delete nextBillingDate;
			if(dailyQuota < getTransfer(time(NULL), time(NULL))){
				return true;
			}
		}else{
			long totalTransfer = getTransfer(Time::startOfMonth()->timestamp(), time(NULL));
			if(totalTransfer > getQuota()->monthQuotaLimit){
				return true;
			}
		}
	}
	return false;
}

bool QuotaWatch::evaluateQuotaForUser(int baseTime, User* user){
	//First go through the users groups:
	vector<Group*> groups = user->getGroups();
	for(int x = 0; x < groups.size(); x++){
		Group* group = groups[x];

		if (group->getQuota()->totalQuota) {
			long totalTransfer = getTransfer(user, 0, time(NULL) + 24 * 60 * 60);
			if(totalTransfer > group->getQuota()->totalQuotaLimit){
				return true;
			}	
		} 

		if (group->getQuota()->dailyQuota) {
			long totalTransfer = getTransfer(user, time(NULL), time(NULL));
			if(totalTransfer > group->getQuota()->dailyQuotaLimit){
				return true;
			}
		} 

		if (group->getQuota()->weeklyQuota) {
			long totalTransfer = getTransfer(user, Time::startOfWeek()->timestamp(), time(NULL));
			if(totalTransfer > group->getQuota()->weeklyQuotaLimit){
				return true;
			}	
		} 

		if (group->getQuota()->monthQuota) {
			//are we a group/smart quota?
			if(group->getQuota()->monthQuotaIsSmart){
				Time* lastBillingDate = Time::getLastDayOfMonth(baseTime, group->getQuota()->monthQuotaBillingDay);
				Time* nextBillingDate = Time::getNextDayOfMonth(baseTime, group->getQuota()->monthQuotaBillingDay);

				long totalTransfer = 0;
				int userCount =0;

				Time* yesterday = Time::yesterday(baseTime);
				vector<string> listOfUsernames = userDb->list();
				for(int x= 0;x < listOfUsernames.size(); x++){
					User* user = userDb->getUser(listOfUsernames[x]);
					if(user->checkForGroup(group)){
						userCount++;
						totalTransfer += getTransfer(user, Time::addDays(lastBillingDate->timestamp(), 1)->timestamp(), yesterday->timestamp());				
					}	
				}

				long quotaLeft = group->getQuota()->monthQuotaLimit - totalTransfer;
				int daysLeft = Time::dayDiff(nextBillingDate, yesterday); 
				long dailyQuota = quotaLeft / daysLeft;		

				delete lastBillingDate;
				delete nextBillingDate;
				delete yesterday;	
				if(dailyQuota < getTransfer(user, time(NULL), time(NULL))){
					return true;
				}
			}else{
				long totalTransfer = getTransfer(user, Time::startOfMonth()->timestamp(), time(NULL));
				if(totalTransfer > group->getQuota()->monthQuotaLimit){
					return true;
				}
			}
		}
	}

	if (user->getQuota()->totalQuota) {
		long totalTransfer = getTransfer(user, 0, time(NULL) + 24 * 60 * 60);
		if(totalTransfer > user->getQuota()->totalQuotaLimit){
			return true;
		}
	}

	if (user->getQuota()->dailyQuota) {
		long totalTransfer = getTransfer(user, time(NULL), time(NULL));
		if(totalTransfer > user->getQuota()->dailyQuotaLimit){
			return true;
		}
	}

	if (user->getQuota()->weeklyQuota) {
		long totalTransfer = getTransfer(user, Time::startOfWeek()->timestamp(), time(NULL));
		if(totalTransfer > user->getQuota()->weeklyQuotaLimit){
			return true;
		}
	}

	if (user->getQuota()->monthQuota) {
		//are we a group/smart quota?
		if(user->getQuota()->monthQuotaIsSmart){
			Time* lastBillingDate = Time::getLastDayOfMonth(baseTime, user->getQuota()->monthQuotaBillingDay);
			Time* nextBillingDate = Time::getNextDayOfMonth(baseTime, user->getQuota()->monthQuotaBillingDay);

			long totalTransfer = 0;
			int userCount =0;

			Time* yesterday = Time::yesterday(baseTime);
			totalTransfer += getTransfer(user, Time::addDays(lastBillingDate->timestamp(), 1)->timestamp(), yesterday->timestamp());                               

			long quotaLeft = user->getQuota()->monthQuotaLimit - totalTransfer;
			int daysLeft = Time::dayDiff(nextBillingDate, yesterday);
			long dailyQuota = quotaLeft / daysLeft;

			delete yesterday;
                        delete lastBillingDate;
                        delete nextBillingDate;

			if(dailyQuota < getTransfer(user, time(NULL), time(NULL))){
				return true;
			}
		}else{
			long totalTransfer = getTransfer(user, Time::startOfMonth()->timestamp(), time(NULL));
			if(totalTransfer > user->getQuota()->monthQuotaLimit){
				return true;
			}
		}
	}
	return false;
}

void QuotaWatch::alertOnQuota(bool exceeded) {
	if (exceeded) {
		if (getQuota()->quotaExceeded) {
			//do nothing
		} else {
			getQuota()->quotaExceeded = true;	

			EventParams params;
			eventDb->add(new Event(GLOBAL_QUOTA_EXCEEDED, params));
		}
	} else {
		if (getQuota()->quotaExceeded) {
			getQuota()->quotaExceeded = false;

			EventParams params;
			eventDb->add(new Event(GLOBAL_QUOTA_OK, params));
		}
	}
}

void QuotaWatch::alertOnQuota(User* user, bool exceeded) {
	if (exceeded) {
		if (user->isQuotaExceeded()) {
			//do nothing
		} else {
			user->setQuotaExceeded(true);	

			EventParams params;
			params["username"] = user->getUserName();
			eventDb->add(new Event(USERDB_QUOTA_EXCEEDED, params));
		}
	} else {
		if (user->isQuotaExceeded()) {
			user->setQuotaExceeded(false);			

			EventParams params;
			params["username"] = user->getUserName();
			eventDb->add(new Event(USERDB_QUOTA_OK, params));

		}
	}
}

long QuotaWatch::getTransfer(long start, long end) {
	long res = -1;

	try {
		AnalyticsClient* client = new AnalyticsClient();
		res = client->getTotalTransfer(Time(start), Time(end));
		delete client;
	} catch (exception& e) {}

	return res;
}

long QuotaWatch::getTransfer(User* user, long start, long end) {
	long res = -1;

	try {
		AnalyticsClient* client = new AnalyticsClient();
		res = client->getBandwidthByUser(user->getUserName(), Time(start), Time(end));
		delete client;
	} catch (exception& e) {}

	return res;
}

void QuotaWatch::load(){
	if(config->has("quotas")){
		ObjectContainer* root = config->getElement("quotas");
		quota->dailyQuota = root->get("dailyQuota")->boolean();
		quota->dailyQuotaLimit = root->get("dailyQuotaLimit")->number();

		quota->weeklyQuota= root->get("weeklyQuota")->boolean();
		quota->weeklyQuotaLimit = root->get("weeklyQuotaLimit")->number();

		quota->monthQuota = root->get("monthQuota")->boolean();
		quota->monthQuotaIsSmart = root->get("monthQuotaIsSmart")->boolean();
		quota->monthQuotaLimit = root->get("monthQuotaLimit")->number();
		quota->monthQuotaBillingDay = root->get("monthQuotaBillingDay")->number();
	}
}

void QuotaWatch::save(){
	ObjectContainer* root = new ObjectContainer(CREL);
	root->put("dailyQuota", new ObjectWrapper((bool) quota->dailyQuota));
	root->put("dailyQuotaLimit", new ObjectWrapper((double) quota->dailyQuotaLimit));

	root->put("weeklyQuota", new ObjectWrapper((bool) quota->weeklyQuota));
	root->put("weeklyQuotaLimit", new ObjectWrapper((double) quota->weeklyQuotaLimit));

	root->put("monthQuota", new ObjectWrapper((bool) quota->monthQuota));
	root->put("monthQuotaIsSmart", new ObjectWrapper((bool) quota->monthQuotaIsSmart));
	root->put("monthQuotaLimit", new ObjectWrapper((double) quota->monthQuotaLimit));
	root->put("monthQuotaBillingDay", new ObjectWrapper((double) quota->monthQuotaBillingDay));

	config->setElement("quotas", root);
	config->save();
}

SysMonitor::SysMonitor(EventDb* eventDb, SLogger::Logger* logger, CronManager* cronManager, SFwallCore::Firewall* firewall, BandwidthDbPrimer* bandwidthDb, QuotaWatch* quotaWatch) {
	this->eventDb = eventDb;
	this->logger = logger;
	this->firewall = firewall;

	InternetConnectionWatch* icw = new InternetConnectionWatch(&System::getInstance()->configurationManager);
	icw->setEventDb(eventDb);

	registeredWatches.push_back(icw);
	quotaWatch->setEventDb(eventDb);
	registeredWatches.push_back(quotaWatch);

	cronManager->registerJob(new SysMonitorCron(this));

	registerMetric(new GeneralMetricSampler());
}

void InternetConnectionWatch::poll(){
	if(config->hasByPath("watchdog:monitorGoogle") && config->get("watchdog:monitorGoogle")->boolean()){
		int res = NetworkUtils::ping("google.com");
		if(res == -1 && state){
			eventDb->add(new Event(WATCHDOG_INTERNET_CONNECTION_DOWN, EventParams()));	
			state = false;
		}else if(res != -1 && !state){
			eventDb->add(new Event(WATCHDOG_INTERNET_CONNECTION_UP, EventParams()));
			state = true;
		}
	}
}

QuotaWatch::QuotaWatch(UserDb* userDb, GroupDb* groupDb, ConfigurationManager* config) : Watch() {
	this->userDb = userDb;
	this->groupDb = groupDb;
	this->quota = new QuotaInfo();
	this->config = config;
}


