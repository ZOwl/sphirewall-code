#Copyright Michael Lawson, John Deal
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import re
import time
from prettytable import PrettyTable
from datetime    import date
from datetime    import timedelta

def show_session_token(connectionDetail, input):
    print "Authenticated with token:" + connectionDetail.token;


def show_session_connection(connectionDetail, input):
    print "Connected to management service on %s" % ( connectionDetail.hostname )

def convertMetricChar(metricChar):

    metricStr = 'gbytes'

    if metricChar == 'b':
        metricStr = 'bytes'
    else:
        if metricChar == 'k':
            metricStr = 'kbytes'
        else:
            if metricChar == 'm':
                metricStr = 'mbytes'

    return metricStr

# Used to load PrettyTable for user and group quotas.
# Returns loaded PrettyTable.
def loadQuotas(quotas):
        pTable = PrettyTable(["Quota", "Limit (bytes)"])
        pTable.align["Quota"] = "l"
        pTable.align["Limit (bytes)"] = "r"
        pTable.padding_width = 1

        pTable.add_row(["Daily", str(quotas['dailyQuota'])])
        pTable.add_row(["Daily Limit", str(quotas['dailyQuotaLimit'])])
        pTable.add_row(["Weekly", str(quotas['weeklyQuota'])])
        pTable.add_row(["Weekly Limit", str(quotas['weeklyQuotaLimit'])])
        pTable.add_row(["Monthly", str(quotas['monthQuota'])])
        pTable.add_row(["Monthly Smart", str(quotas['monthQuotaIsSmart'])])
        pTable.add_row(["Monthly Limit", str(quotas['monthQuotaLimit'])])
        pTable.add_row(["Monthly Billing Day",
            str(quotas['monthQuotaBillingDay'])])
        pTable.add_row(["Total", str(quotas['totalQuota'])])
        pTable.add_row(["Total Limit", str(quotas['totalQuotaLimit'])])
        pTable.add_row(["Time", str(quotas['timeQuota'])])
        pTable.add_row(["Time Limit", str(quotas['timeQuotaLimit'])])
        return pTable

def show_metrics(connectionDetail, input):
    # Get all matrics statistics.
    metrics = None

    try:
        # Process total data from beginning to tomorrow for each availabe
        #statistic.
        endDate = date.today() + timedelta(days=1)
        metrics = connectionDetail.request( \
            "analytics/stats/metrics/getAllTotals", \
            {"startDate": "2013-08-01", "endDate": str(endDate)});
    except:
        print "Unable to obtain all metric statistics.  Make sure sphirewall_ana is running."

    pTable = PrettyTable(["Metric", "Value"])
    pTable.align["Metric"] = "l"
    pTable.align["Value"] = "r"
    pTable.padding_width   = 1

    for metric in metrics['items']:
        try:
            pTable.add_row([metric['name'], str(metric['value'])])
        except:
            print "Value not available for metric " + metric['name'] + "."

    print pTable

def show_runtime(connectionDetail, input):
    detail_request = connectionDetail.request("general/runtime/list", None)
    values = detail_request['keys']

    pTable = PrettyTable(["Runtime Parameter", "Value"])
    pTable.align["Runtime Parameter"] = "l"
    pTable.align["Value"]             = "r"
    pTable.padding_width              = 1

    for n in values:
        pTable.add_row([n['key'], str(connectionDetail.request("general/runtime/get", {"key":n['key']})['value'])])

    print pTable

def show_connections(connectionDetail, input):
    detail_request = connectionDetail.request("firewall/tracker/list", None)
    for n in detail_request['connections']:
        out = " "
        out += n['sourceIp']

        if n['protocol'] == 0 or n['protocol'] == 1:
            out += ":" + str(n['sourcePort'])

        out += " --> " + n['destIp']
        if n['protocol'] == 0 or n['protocol'] == 1:
            out += ":" + str(n['destPort'])

        if n['protocol'] == 0:
            out += " " + n['state']

        print out

def show_devices(connectionDetail, input):
    detail_request = connectionDetail.request("network/devices/list", None);

    pTable = PrettyTable(["Name", "IP Address", "Netmask", \
        "Network", "State", "Aliases"])
    pTable.align["Name"]       = "l"
    pTable.align["IP Address"] = "l"
    pTable.align["Netmask"]    = "l"
    pTable.align["Network"]    = "l"
    pTable.align["State"]      = "l"
    pTable.align["Aliases"]    = "l"
    pTable.padding_width       = 1

    for n in detail_request['devices']:
        state = n["state"];
        if state == True:
            state = "up";
        else:
            state = "down";
         
        pTable.add_row([n["interface"], n["ip"], n["mask"], n["network"], \
            state, n['aliases']])

    print pTable

def show_acls(connectionDetail, input):
    detail_request = connectionDetail.request("firewall/acls/list", None);
    index = 0;
    
    for n in detail_request['normal']:
        show_acls_print(index, n);
        index += 1;
        
    detail_request = connectionDetail.request("firewall/acls/list", None);
    index = 0;

    for n in detail_request['nat']:
        show_acls_print(index, n);
        index += 1;
    
def show_acls_print(index, n):
    s = str(index) + ":(" + str(n["id"]) + "): source";
    if n.has_key("sourceAddress"):
        s += " address " + n["sourceAddress"] + "/" + n["sourceSubnet"];
    if n.has_key("sourceAlias"):
        s += " alias " + n["sourceAlias"];
    if n.has_key("sourceAddress") == False and n.has_key("sourceAlias") == False:
        s += " any";
    if n.has_key("sourcePort"):
        s += " port " + str(n["sourcePort"]);
    if n.has_key("sourceInterface"):
        s += " device " + n["sourceInterface"];

    s += " dest"
    if n.has_key("destAddress"):
        s += " address " + n["destAddress"] + "/" + n["destSubnet"]
    if n.has_key("destAlias"):
        s += " alias " + n["destAlias"]
    if n.has_key("destAddress") == False or n.has_key("destAlias") == False:
        s += " any"
    if n.has_key("destPort"):
        s += " port " + str(n["destPort"])
    if n.has_key("destInterface"):
        s += " device " + n["destInterface"]

    s += " --> "
    if n["action"] == 0:
        s += "drop";
    if n["action"] == 1:
        s += "accept";
    if n["action"] == 2:
        s += " masquerade to " + n["masqueradeTarget"];
    if n["action"] == 3:
        s += " forward to " + n["forwardTarget"] + ":" + str(n["forwardPort"]);

    # Display enabled status
    if n['enabled'] == True:
        s += " enabled"
    else:
        s += " disabled"
        
    print s
    
def show_config_system(connectionDetail, input):
    configAttributes = ["admin_group", "bandwidth_hostname", "bandwidth_port", \
        "bandwidth_retension", "bandwidth_threshold", "config_dir", "gid", \
        "redirect_to_auth", "session_timeout", "snort_enabled", \
        "snort_file", "ssl_password", "ssl_port"]

    pTable = PrettyTable(["System Parameter", "Value"])
    pTable.align["System Parameter"] = "l"
    pTable.align["Value"]            = "l"
    pTable.padding_width             = 1

    try:
        for key in configAttributes:
            if key == "ssl_password" or key == "ssl_port":
                value = "****"
            else:
                try:
                    value = connectionDetail.request("general/config/get", \
                        {"key":"general:" + key})['value'];
                except:
                    value = "Unable to retrieve"
 
            pTable.add_row([key, value])

        print pTable
    except:
        print "Unable to retrieve system configuration"

def show_config_loglevel(connectionDetail, input):
    items = connectionDetail.request("general/logging/list", None);

    pTable = PrettyTable(["Log Type", "Level"])
    pTable.align["Log Type"] = "l"
    pTable.padding_width     = 1

    for n in items["levels"]:
        pTable.add_row([n["context"], str(n["level"])])

    print pTable

def show_users(connectionDetail, input):
    try:
        users = connectionDetail.request("auth/users/list", None);
 
        pTable = PrettyTable(["User", "L. Name", "F. Name", \
            "Email", "Groups"])
        pTable.align["User"]    = "l"          
        pTable.align["L. Name"] = "l"          
        pTable.align["F. Name"] = "l"          
        pTable.align["Email"]   = "l"          
        pTable.align["Groups"]  = "l"          
        pTable.padding_width    = 1

        for user in users['users']:
            # out = "";
            # out = user['username'] + " =  Lastname:" + user['lname'] + "  Firstname:" + user['fname'] + "  Email:" + user['email'] + " Groups: ";
    
            groups = user['groups'];
    
            # for group in groups:
            #     out += "[" + group['name'] + " \"" + group['desc'] + "\"] ";
        
            userGroups = ""

            for group in groups:
                userGroups += "[" + group['name'] + " \"" + group['desc'] + \
                    "\"] ";

            pTable.add_row([user['username'], user['lname'], user['fname'], \
                user['email'], userGroups])
            # print out;

        print pTable
    except:
        print "There was an error displaying the list of users.";
        
def show_groups(connectionDetail, input):
    try:
        groups = connectionDetail.request("auth/groups/list", None);

        pTable = PrettyTable(["ID", "Name", "Description", "Manager"])
        pTable.align["ID"]          = "l"
        pTable.align["Name"]        = "l"
        pTable.align["Description"] = "l"
        pTable.align["Manager"]     = "l"
        pTable.padding_width        = 1

        for group in groups['groups']:
            pTable.add_row([group['id'], group['name'], group['desc'], \
                group['manager']])

        print pTable
    except:
        print "There was an error displaying the list of groups.";


def show_qos(connectionDetail, input):
    try:
        qos = connectionDetail.request("firewall/acls/list/trafficshaper", None);
        index = 0;
    
        for n in qos['items']:
            print "Rule ID:" + str(n['id']);
            show_qos_print(index, n);
            index += 1;
    except:
        print "Error obtaining trafficshaper rules";

            
def show_qos_print(index, n):
    s = " - Source=";
    if n.has_key("source"):
        if n.has_key("sourceMask"):
            s += "  Address:" + n["source"] + "/" + n["sourceMask"] + ",";
        else:
            s += "  Address:" + n["source"] + ",";
    if n.has_key("sourceBlock"):
        s += "  Block:" + str(n["sourceBlock"]) + ",";               
    if n.has_key("sourcePort"):
        s += "  Port:" + str(n["sourcePort"]) + ",";
    if n.has_key("groupname"):
        s += "  Group:" + n["groupname"];
        
    s += "\n - Destination="
    if n.has_key("destination"):
        if n.has_key("destinationMask"):
            s += "  Address:" + n["destination"] + "/" + n["destinationMask"] + ",";
        else:
            s += "  Address:" + n["destination"] + ",";
    if n.has_key("destinationBlock"):
        s += "  Block:" + str(n["destinationBlock"]) + ",";
    if n.has_key("destinationPort"):
        s += "  Port:" + str(n["destinationPort"]);
        
    s += "\n - Rules="
    if n.has_key("download"):
        s += "  Download:" +str(n["download"]) + ",";   
    if n.has_key("upload"):
        s += "  Upload:" + str(n["upload"]);   
    
    s += "\n - Statistics="
    if n.has_key("hits"):
        s += "  Hits:" + str(n["hits"]) + ",";   
    if n.has_key("buckets"):
        s += "  Active Buckets:" + str(n["buckets"]);  
              
    print s           
            
def show_user_quotas(connectionDetail, input):
    usage = "Syntax error.\nUsage: {username}"
    try:
        try:
            p = re.compile('(.+)');
            m = p.match(input.strip());
        
            userName = m.group(1).strip();
    
        except:
            print usage;
            return;
        
        cmdArg        = {};
        cmdArg["username"] = userName;
        
        # quotas = connectionDetail.request("auth/users/quotas/list", cmdArg);
        quotas = connectionDetail.request("auth/users/get", cmdArg);
        
        try:
            pTable = loadQuotas(quotas)
            print pTable
        except:
            print "User " + userName + " doesn't exist or incorrect user entered\n" + usage;
            return;
    except:
        print "There was an error displaying the quotas for" + userName;
        
def show_group_quotas(connectionDetail, input):
    usage = "Syntax error.\nUsage: {group ID}"
    try:
        try:
            p = re.compile('(.+)');
            m = p.match(input.strip());
        
            group = m.group(1).strip();
    
        except:
            print usage;
            return;
        
        cmdArg          = {};

        try:
            cmdArg["id"] = int(group);
        except:
            print "Group must be a numeric value.  Use show group list."
            return
        
        quotas = connectionDetail.request("auth/groups/get", cmdArg);

        try:    
            pTable = loadQuotas(quotas)
            print pTable
        except:
            print "Group " + group + " doesn't exist or incorrect group entered\n" + usage;
            return;
    except:
        print "There was an error displaying the quotas for" + group;

def show_status_clients(connectionDetail, input):
    try:
        hosts = connectionDetail.request("network/arp/list", None);

        for host in hosts['hosts']:
            out = "host: " + host['host'] + "   macaddress: " + host['hw'];
            print out;
    except:
        print "There was an error displaying the list of connected clients.";

def show_status_users(connectionDetail, input):
    try:
        users = connectionDetail.request("auth/sessions/list", None);

        for user in users['sessions']:
            out = "user: " + user['user'] + "   host: " + user['host'] + "   macaddress: " + user['hw'];
            print out;
    except:
        print "There was an error displaying the list of connected users.";

def show_status_devices(connectionDetail, input):
    usage = "show status devices start {yyyy-mm-dd | begin} stop {yyyy-mm-dd | now} metric {b | k | m | g}"
    try:
        params = re.compile('start (.+) stop (.+) metric (.+)')

        paramGroup = params.match(input.strip())
        startDate  = paramGroup.group(1).strip()
        stopDate   = paramGroup.group(2).strip()
        metric     = convertMetricChar(paramGroup.group(3).strip())

        if startDate == 'begin':
            startDate = '2013-01-01'

        if stopDate == 'now':
            stopDate = str(date.today() + timedelta(days=1))

        devices = connectionDetail.request("network/devices/list", \
            None)["devices"]

        requestParams = {}
        requestParams["startTime"] = startDate
        requestParams["endTime"]   = stopDate
        requestParams["metric"]    = metric

        pTable = PrettyTable(["Device", "TCP Up", "TCP Down", "UDP Up", \
            "UDP Down"])
        pTable.align["Device"]   = "l"
        pTable.align["TCP Up"]   = "r"
        pTable.align["TCP Down"] = "r"
        pTable.align["UDP Up"]   = "r"
        pTable.align["UDP Down"] = "r"
        pTable.padding_width     = 1

        print "Units: " + metric

        for device in devices:
            requestParams["device"] = device["interface"]
            stats = connectionDetail.request( \
                "analytics/stats/bandwidth/query/device/sum", requestParams)
            pTable.add_row([device['interface'], str(stats["tcpUp"]), \
                str(stats["tcpDown"]), str(stats["udpUp"]), \
                str(stats["udpDown"])])

        print pTable
    except:
        print "Usage: " + usage
        print "There was an error displaying the list of devices.";

def show_status_websites(connectionDetail, input):
    usage = "show status websites start {yyyy-mm-dd | begin} stop {yyyy-mm-dd | now} limit {integer number | all} metric {b | k | m | g}"

    try:
        params = re.compile('start (.+) stop (.+) limit (.+) metric (.+)')

        paramGroup = params.match(input.strip())
        startDate  = paramGroup.group(1).strip()
        stopDate   = paramGroup.group(2).strip()
        limit      = paramGroup.group(3).strip()
        metric     = convertMetricChar(paramGroup.group(4).strip())

        if startDate == 'begin':
            startDate = '2013-01-01'

        if stopDate == 'now':
            stopDate = str(date.today() + timedelta(days=1))

        if limit == 'all':
            limit = -1

        requestParams = {}
        requestParams["startTime"] = startDate
        requestParams["endTime"]   = stopDate
        requestParams["metric"]    = metric
        requestParams["limit"]     = int(limit)

        websites = connectionDetail.request( \
            "analytics/stats/bandwidth/query/web", requestParams)
        print "Units: " + metric

        pTable = PrettyTable(["Host", "TCP Up", "TCP Down", "UDP Up", \
            "UDP Down"])
        pTable.align["Host"]     = "l"
        pTable.align["TCP Up"]   = "r"
        pTable.align["TCP Down"] = "r"
        pTable.align["UDP Up"]   = "r"
        pTable.align["UDP Down"] = "r"
        pTable.padding_width     = 1

        for site in websites["items"]:
            pTable.add_row([site['httpHost'], str(site["tcpUp"]), \
                str(site["tcpDown"]),  str(site["udpUp"]), \
                str(site["udpDown"])])

        print pTable
    except:
        print "Usage: " + usage
        print "There was an error displaying the list of websites.";
