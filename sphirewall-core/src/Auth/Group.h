/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GROUPS_H
#define GROUPS_H

#include <vector>

class QuotaInfo;
class Group {
friend class GroupDb;
public:
	Group();
	int getId() const; 
	std::string getName() const;
	void setId(int id) ;
	QuotaInfo* getQuota()const;
	std::string getDesc() const;
	std::string getManager() const;
	bool isAllowMui() const;
	void setDesc(std::string desc);
	void setManager(std::string manager);
	void setAllowMui(bool a);

private:
	int m_id;
	std::string m_name;
	std::string m_file;
	std::string m_desc;
	int m_createTime;
	std::string m_manager;
	bool allow_mui;
	QuotaInfo* quota;
};

#endif
