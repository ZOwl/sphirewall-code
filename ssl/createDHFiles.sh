#/bin/bash
echo "+++++++++++++++++++++++++++++"
echo "Creating 1024 byte Diffie-Hellman file."
echo "+++++++++++++++++++++++++++++"
openssl dhparam -check -text 5 1024 -out dh1024.pem
echo "+++++++++++++++++++++++++++++"
echo "Copying Diffie-Hellman file to Sphirewall shared area."
echo "+++++++++++++++++++++++++++++"
# cp dh1024.pem /usr/share/sphirewall/ca/dh1024.pem
