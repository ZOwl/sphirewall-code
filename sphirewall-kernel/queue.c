/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Incomming packet queue. Packets provided by the netfilter interrupts are pushed onto this queue
 * and processed on a bottom end kthread. 
*/

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/kfifo.h>
#include <net/netfilter/nf_queue.h>

static struct list_head queue_list;
static spinlock_t lock;

void send_queue_init(void){
	INIT_LIST_HEAD(&queue_list);
}

int send_queue_empty(void){
	return list_empty(&queue_list);
}

void send_queue_push_entry(struct nf_queue_entry *entry)
{
        spin_lock_bh(&lock);

        list_add_tail(&entry->list, &queue_list);
        spin_unlock_bh(&lock);
}

struct nf_queue_entry *send_queue_pop_entry(void)
{
        struct nf_queue_entry *entry = NULL;
        spin_lock_bh(&lock);

        list_for_each_entry(entry, &queue_list, list) {
                break;
        }

        if (entry) {
                list_del(&entry->list);
        }

        spin_unlock_bh(&lock);
        return entry;
}

