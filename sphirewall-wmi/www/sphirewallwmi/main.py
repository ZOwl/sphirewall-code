#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import datetime
from flask.globals import request
from flask.helpers import make_response
from flask.templating import render_template
from werkzeug.utils import redirect
from sphirewallwmi import app
from sphirewallwmi import service
from sphirewallwmi.api.sphirewall_api import SphirewallClient
from sphirewallwmi.api.sphirewall_connection import SessionTimeoutException, ApiError
from sphirewallwmi.api.utils import DateHelper

@app.route('/')
def main():
    if service.session() is not None:
        service.get_sphirewallapi().version()
        return render_template('home.html', totalUsers=len(service.get_sphirewallapi().general().active_users_list()),
            totalHosts=service.get_sphirewallapi().general().hosts_size(),
            totalConnection=service.get_sphirewallapi().firewall().connections_size(),
            version=service.get_sphirewallapi().version(),
            events=service.get_sphirewallapi().general().events_size(),
            hostname=service.get_sphirewallapi().get_hostname(), port=service.get_sphirewallapi().get_port(),
            token=service.get_sphirewallapi().get_token(),
            connections=service.get_sphirewallapi().statistics_list("firewall.conntracker.tcp.size", DateHelper.yesterday().isoformat(), DateHelper.today().isoformat()),
            reportingConnected=service.get_sphirewallapi().reporting().connected(),
            defaultPasswordSet=service.get_sphirewallapi().general().defaultpasswordset(),
            defaultRuleset=service.get_sphirewallapi().firewall().defaultruleset())
    else:
        return redirect("/login")


@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "GET":
        return render_template("login.html")
    else:
        try:
            token = service.authenticate(request.form.get("username"), request.form.get("password"),
                request.form.get("host"), request.form.get("port"))
        except:
            return render_template("login.html", error="Could not connect to sphirewall")

        if token is not None:
            resp = make_response(redirect("/"))
            resp.set_cookie('TOKEN', token)
            resp.set_cookie('HOSTNAME', request.form.get("host"))
            resp.set_cookie('PORT', request.form.get("port"))
            return resp
        else:
            return render_template("login.html", error="Invalid username or password")



@app.route("/logout", methods=["GET", "POST"])
def logout():
    resp = make_response(redirect("/"))
    resp.set_cookie('TOKEN', None, 0)
    return resp


@app.route("/authenticate", methods=["POST", "GET"])
def authenticate():
    if request.method == "GET":
        return render_template("authenticate.html")
    else:
        try:
            hostname = request.args["host"] if len(request.args["host"]) > 0 else "127.0.0.1"
            client = SphirewallClient(hostname=hostname, port=int(request.args["port"]), token=None)
            args = {}
            args["username"] = request.form["username"]
            args["password"] = request.form["password"]
            args["ipaddress"] = request.args["ip"]

            response = client.connection.request("auth/login", args)
            if response['response'] == 0:
                if "url" in request.args and len(request.args["url"]) > 0:
                    return redirect("http://" + request.args["url"])
                else:
                    return redirect("http://google.com")
            else:
                return render_template("authenticate.html", error="Invalid login:" + response["message"])
        except:
            return render_template("authenticate.html", error="Could not login, invalid login or bad connection")


@app.errorhandler(SessionTimeoutException)
def session_timedout_handler(error=""):
    return render_template("login.html", error="Session timed out, please login again")


@app.errorhandler(ApiError)
def api_error_handler(error):
    return render_template("error.html", error=str(error))

@app.errorhandler(500)
def internal_error(error):
    return render_template("login.html", error="An unexpected error occurred, please login again")
