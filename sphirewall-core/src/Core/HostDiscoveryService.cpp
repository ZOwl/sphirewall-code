/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;

#include "Core/HostDiscoveryService.h"
#include "Utils/IP4Addr.h"
#include "Utils/Utils.h"
#include "Utils/NetDevice.h"
#include "Core/System.h"
#include "Core/Event.h"
#include "Utils/Lock.h"
#include "Core/Lockable.h"
#include "SFwallCore/Packet.h"

int HostDiscoveryService::HOST_DISCOVERY_SERVICE_TIMEOUT = 60 * 10;

void HostDiscoveryService::update(unsigned int ip, std::string mac){
	if(tryLock()){
		Host* arpEntry = getByMac(mac, ip);
		if (arpEntry != NULL) {
			arpEntry->lastSeen = time(NULL);
		} else {
			add(mac, ip);
		}
		releaseLock();
	}
}

void HostDiscoveryService::update(SFwallCore::Packet* pk) {
	if(IP4Addr::isLocal(pk->getSrcIp()) && pk->getHw().size() != 0){
		if(tryLock()){
			Host* arpEntry = getByMac(pk->getHw(), pk->getSrcIp());
			if (arpEntry != NULL) {
				arpEntry->lastSeen = time(NULL);
			} else {
				add(pk->getHw(), pk->getSrcIp());

				EventParams params;
				params["ip"] = IP4Addr::ip4AddrToString(pk->getSrcIp());
				params["hw"] = pk->getHw();	
				if(eventDb){
					eventDb->add(new Event(NETWORK_ARP_DISCOVERED, params));
				}		
			}
			releaseLock();	
		}
	}	
}

string HostDiscoveryService::get(string ip) {
	holdLock();	
	in_addr_t intIp = IP4Addr::stringToIP4Addr(ip);
	Host* entry = get(intIp);
	if (entry != NULL) {
		releaseLock();	
		return entry->mac;
	}

	releaseLock();	
	return "";
}

Host* HostDiscoveryService::get(in_addr_t ip) {
	map<string, std::list<Host*> >::iterator iter;
	for (iter = entryTable.begin();
			iter != entryTable.end();
			iter++) {

		std::list<Host*> entries = iter->second;
		for (std::list<Host*>::iterator eiter = entries.begin();
				eiter != entries.end(); eiter++) {	
			Host* entry = (*eiter);
			if (entry->ip == ip) {
				return entry;
			}
		}
	}
	return NULL;
}

Host* HostDiscoveryService::getByMac(string mac, in_addr_t ip) {
	map<string, std::list<Host*> >::iterator iter = entryTable.find(mac);
	if (iter != entryTable.end()) {
		std::list<Host*> entries = iter->second;
		for (std::list<Host*>::iterator eiter = entries.begin();
				eiter != entries.end(); eiter++) {
			Host* entry = (*eiter);
			if (entry->ip == ip) {
				return entry;
			}
		}
	}

	return NULL;
}

void HostDiscoveryService::list(std::list<Host*>& entries) {
	for (map<string, std::list<Host*> >::iterator iter = entryTable.begin();
			iter != entryTable.end();
			iter++) {

		for (std::list<Host*>::iterator eiter = iter->second.begin();
				eiter != iter->second.end(); eiter++) {
			Host* target = (*eiter);
			entries.push_back(target);
		}
	}
}

void HostDiscoveryService::CleanUpCron::run() {
	int t = time(NULL) - HOST_DISCOVERY_SERVICE_TIMEOUT;
	root->holdLock();	
	vector<string> rowsToDelete;
	for (map<string, std::list<Host*> >::iterator iter = root->entryTable.begin();
			iter != root->entryTable.end();
			iter++) {

		for (std::list<Host*>::iterator eiter = iter->second.begin();
				eiter != iter->second.end();
				eiter++) {

			Host* target = (*eiter);
			if (target->lastSeen < t) {
				EventParams params;
				params["ip"] = target->getIp();
				params["hw"] = target->mac; 
				if(root->eventDb){
					root->eventDb->add(new Event(NETWORK_ARP_TIMEDOUT, params));
				}               

				eiter = iter->second.erase(eiter);
				delete target;
			}
		}

		if(iter->second.size() == 0){
			/*
			   map<string, std::list<Host*> >::iterator toDelete = iter;
			   iter++;
			   root->entryTable.erase(toDelete);
			 */
			rowsToDelete.push_back(iter->first);
		}
	}

	for(int x = 0; x < rowsToDelete.size(); x++){
		root->entryTable.erase(rowsToDelete[x]);
	}
	root->releaseLock();
}

void HostDiscoveryService::add(string mac, in_addr_t ip) {
	if (mac.size() > 0) {
		Host* entry = new Host();
		entry->mac = mac;
		entry->ip = ip;
		entry->firstSeen = time(NULL);
		entry->lastSeen = time(NULL);

		entryTable[mac].push_back(entry);
	}
}

void HostDiscoveryService::addIgnoreLocal(string mac, in_addr_t ip) {
	Host* entry = new Host();
	entry->mac = mac;
	entry->ip = ip;
	entry->firstSeen = time(NULL);
	entry->lastSeen = time(NULL);

	entryTable[mac].push_back(entry);
}

std::string Host::hostname(){
	if(hostnameCache.size() == 0){
		hostnameCache = IP4Addr::hostname(ip);
	}

	return hostnameCache;
}

int HostDiscoveryService::size() {
	holdLock();	
	std::list<Host*> buffer;
	list(buffer);
	int res = buffer.size();
	releaseLock();	
	return res;
}

string Host::getIp() {
	return IP4Addr::ip4AddrToString(ip);
}

HostDiscoveryService::HostDiscoveryService(SLogger::Logger* logger, CronManager* cron, Config* config) : logger(logger) {
	cron->registerJob(new CleanUpCron(this));
	config->getRuntime()->loadOrPut("HOST_DISCOVERY_SERVICE_TIMEOUT", &HOST_DISCOVERY_SERVICE_TIMEOUT);
}

HostDiscoveryService::HostDiscoveryService(){}
