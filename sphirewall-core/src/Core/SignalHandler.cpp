/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <signal.h>

using namespace std;

#include "Core/SignalHandler.h"
#include "Core/System.h"
#include "Utils/Utils.h"
#include <execinfo.h>
#include <signal.h>
#include <exception>
#include <iostream>
#include <cxxabi.h>

void sighandler(int sig) {
	switch (sig) {
		case SIGINT:
			System::getInstance()->logger.log(SLogger::EVENT, "(void) sighandler()", "SIGINT caught");
			System::getInstance()->init("", RUNLEVEL_STOP);
			break;

		case SIGSEGV:

		case SIGABRT:{
				     void *array[10];
				     size_t size;
				     char **strings;
				     size_t i;

				     size = backtrace (array, 10);
				     strings = backtrace_symbols (array, size);

				     System::getInstance()->logger.log(SLogger::CRITICAL_ERROR, "(void) sighandler()", "SIGSEGV or SIGTERM caught");
				     for (i = 0; i < size; i++){
					     System::getInstance()->logger.log(SLogger::CRITICAL_ERROR, "(void) sighandler()", strings[i]);
				     }
				     free (strings);
				     System::getInstance()->init("", RUNLEVEL_SEGFAULT);
				     break;
			     }
		case SIGPIPE:
			     System::getInstance()->logger.log(SLogger::EVENT, "(void) sighandler()", "SIGPIPE caught");
			     break;

		case SIGTERM:
			     System::getInstance()->logger.log(SLogger::EVENT, "(void) sighandler()", "SIGTERM caught");
			     System::getInstance()->init("", RUNLEVEL_STOP);
			     break;

		case SIGHUP:
			     System::getInstance()->logger.log(SLogger::EVENT, "(void) sighandler()", "SIGHUP caught");
			     System::getInstance()->init("", RUNLEVEL_RESTART);
			     break;

		default:
			     break;
	}
}
