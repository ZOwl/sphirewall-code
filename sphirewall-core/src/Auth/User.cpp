/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <map>

using namespace std;
using std::string;

#include "Core/Logger.h"
#include "Core/System.h"
#include "Utils/Utils.h"
#include "Auth/User.h"
#include "Utils/Hash.h"

bool User::enableDisable(bool enable) {
	isEnabled = enable;
	return true;
}

void User::setPassword(string password) {
	Hash hash;
	string new_pass = hash.create_hash(password.c_str(), password.size());
	m_password = new_pass;
}

bool User::checkForGroup(Group* group) {
	groupLock->lock();
	for (int x = 0; x < (int) m_groups.size(); x++) {
		if (group->getId() == m_groups[x]->getId()){
			groupLock->unlock();
			return true;
		}
	}
	
	groupLock->unlock();
	return false;
}

void User::addGroup(Group* group) {
	if (!checkForGroup(group)) {
		System::getInstance()->logger.log(SLogger::DEBUG, "adding group to user");
		
		groupLock->lock();
		m_groups.push_back(group);
		groupLock->unlock();
	}
}

void User::removeGroup(Group* group) {
	groupLock->lock();
	for (int x = 0; x < (int) m_groups.size(); x++) {
		if (m_groups[x]->getId() == group->getId()) {
			m_groups.erase(m_groups.begin() + x);
			break;
		}
	}
	groupLock->unlock();
}

bool User::enable() {
	isEnabled = true;
	return true;
}

bool User::disable() {
	isEnabled = false;
	return true;
}

bool User::isAllowedMuiAccess() {
	groupLock->lock();
	for (int x = 0; x < m_groups.size(); x++) {
		Group* group = m_groups[x];
		if (group->isAllowMui()) {
			groupLock->unlock();
			return true;
		}
	}

	groupLock->unlock();
	return false;
}

string User::getUserName() {
	return m_user;
}

string User::getFname() {
	return m_fname;
}

string User::getLname() {
	return m_lname;
}

string User::getPassword() {
	return m_password;
}

bool User::getIsEnabled() {
	return isEnabled;
}

int User::getLastLogin() {
	return m_lastLogin;
}

int User::getLastFail() {
	return m_lastFail;
}

std::string User::getEmail() const {
	return m_email;
}

void User::setFname(string str) {
	m_fname = str;
}

void User::setLname(string str) {
	m_lname = str;
}

void User::setGroups(vector<Group*> groups) {
	groupLock->lock();
	m_groups = groups;
	groupLock->unlock();
}

void User::setEmail(std::string email) {
	this->m_email = email;
}

void User::setUserName(std::string n) {
	this->m_user = n;
}

AuthenticationType User::getAuthenticationType() {
	return this->authenticationType;
}

void User::setAuthenticationType(AuthenticationType authenticationType) {
	this->authenticationType = authenticationType;
}

std::vector<Group*> User::getGroups() const {
	return m_groups;
}
