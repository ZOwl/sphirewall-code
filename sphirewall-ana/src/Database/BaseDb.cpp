/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

#include "Database/BaseDb.h"
#include "Utils/Lock.h"

void Query::addQuery(std::string query){
	//Attempt to get it from cache:
	dbPrepStatement = baseDb->cache[query];	
	if(dbPrepStatement == NULL){
		const char      *dbEndCompile;
		int dbStatus = sqlite3_prepare_v2(conn, query.c_str(), -1,
				&dbPrepStatement, &dbEndCompile);
		if (dbStatus != SQLITE_OK)
		{
			throw new DatabaseException(sqlite3_errmsg(conn), query);
		}else {	
			baseDb->cache[query] = dbPrepStatement;			
		}		
	}
	sqlite3_reset(dbPrepStatement); 
}

void Query::addNumber(int pos, unsigned int value){
	int res = sqlite3_bind_int64(dbPrepStatement,pos, value);
	if(res != SQLITE_OK){
		throw new DatabaseException(sqlite3_errmsg(conn), "");
	}
}

void Query::addNumber(int pos, long long value){
	int res = sqlite3_bind_int64(dbPrepStatement,pos, value);
	if(res != SQLITE_OK){
		throw new DatabaseException(sqlite3_errmsg(conn), "");
	}
}
void Query::addNumber(int pos, int value){
	int res = sqlite3_bind_int64(dbPrepStatement,pos, value);
	if(res != SQLITE_OK){
		throw new DatabaseException(sqlite3_errmsg(conn), "");
	}
}
void Query::addString(int pos, std::string value){
	int res = sqlite3_bind_text(dbPrepStatement,pos,
			(const char*) value.c_str(), -1, SQLITE_TRANSIENT);

	if(res != SQLITE_OK){
		throw new DatabaseException(sqlite3_errmsg(conn), query); 
	}
}

int Query::execute(){
	int res = sqlite3_step(statement());
	if(res == SQLITE_ERROR){
		throw new DatabaseException(sqlite3_errmsg(conn), query);
	}

	return res;
}

bool Query::next(){
	int res = sqlite3_step(statement());
	if(res == SQLITE_ROW){
		return true;
	}else if(res == SQLITE_DONE){
		return false;
	}else if(res ==SQLITE_ERROR){
		throw new DatabaseException(sqlite3_errmsg(conn), query);
	}

	return false;
}

long long Query::longResult(int pos){
	return sqlite3_column_int64(statement(), pos);
}

std::string Query::stringResult(int pos){
	return (char*) sqlite3_column_text(statement(), pos);
}

bool BaseDb::execSQL(string sqlStatement)
{
	int             dbStatus;
	sqlite3_stmt    *dbPrepStatement;
	const char      *dbEndCompile;

	dbStatus = sqlite3_prepare_v2(this->connection(), sqlStatement.c_str(), -1,
			&dbPrepStatement, &dbEndCompile);

	if (dbStatus != SQLITE_OK)
	{
		throw new DatabaseException(sqlite3_errmsg(this->connection()), sqlStatement);
	}

	if (sqlite3_step(dbPrepStatement) != SQLITE_DONE)
	{
		throw new DatabaseException(sqlite3_errmsg(this->connection()), sqlStatement);
	}

	if (sqlite3_finalize(dbPrepStatement) != SQLITE_OK)
	{
		throw new DatabaseException(sqlite3_errmsg(this->connection()), sqlStatement);
	}

	return true;
} 

void BaseDb::lock(){
	l->lock();
}

bool BaseDb::tryLock(){
	return l->tryLock();	
}

void BaseDb::unlock(){
	l->unlock();
}

BaseDb::BaseDb(){
	l= new Lock();	
}

void BaseDb::beginTransaction(){
	sqlite3_stmt    *dbPrepStatement;
	const char      *dbEndCompile;

	int dbStatus = sqlite3_prepare_v2(this->connection(), "begin transaction;", -1,
			&dbPrepStatement, &dbEndCompile);

	if (dbStatus != SQLITE_OK)
	{
		throw new DatabaseException(sqlite3_errmsg(this->connection()), "begin transaction;");
	}

	dbStatus = sqlite3_step(dbPrepStatement);
	sqlite3_finalize(dbPrepStatement);
}

void BaseDb::endTransaction(){
	sqlite3_stmt    *dbPrepStatement;
	const char      *dbEndCompile;

	int dbStatus = sqlite3_prepare_v2(this->connection(), "end transaction;", -1,
			&dbPrepStatement, &dbEndCompile);

	if (dbStatus != SQLITE_OK)
	{
		throw new DatabaseException(sqlite3_errmsg(this->connection()), "end transaction");
	}

	dbStatus = sqlite3_step(dbPrepStatement);
	sqlite3_finalize(dbPrepStatement);
}

void BaseDb::deleteByDate(std::string table, std::string date){
        stringstream deleteQuery; deleteQuery << "delete from "<<table<<" where date = '" << date<< "';";

	Query* query = getQuery();
	query->addQuery(deleteQuery.str());
	query->execute();
	delete query;
}

void BaseDb::deleteBeforeDate(std::string table, std::string date){
        stringstream deleteQuery; deleteQuery << "delete from "<<table<<" where date < '" << date<< "';";

        Query* query = getQuery();
        query->addQuery(deleteQuery.str());
        query->execute();
        delete query;
}

void BaseDb::jsonSql(std::string query)
{
	/*sqlite3_stmt* s = getQuery(query);
	char* outbuf = (char*) calloc(sizeof(char), 5000);
	int blen = 5000;

	int nuse=0;
	int res,i=0,ncolumn=sqlite3_column_count(s);

	nuse+=snprintf(outbuf+nuse,blen-nuse,"[\n");
	while(SQLITE_ROW==(res=sqlite3_step(s))) {
		nuse+=snprintf(outbuf+nuse,blen-nuse, "\t{\n");
		for(i=0;i<ncolumn;++i) {
			nuse+=snprintf(outbuf+nuse,blen-nuse,
					"\t\t\"%s\" : ", sqlite3_column_name(s,i));
			switch (sqlite3_column_type(s,i)) {
				case SQLITE_INTEGER:
					nuse+=snprintf(outbuf+nuse,blen-nuse,
							"%d,\n", sqlite3_column_int(s,i));
					break;
				case SQLITE_FLOAT:
					nuse+=snprintf(outbuf+nuse,blen-nuse,
							"%e,\n", sqlite3_column_double(s,i));
					break;
				case SQLITE_TEXT:
					nuse+=snprintf(outbuf+nuse,blen-nuse,
							"\"%s\",\n", sqlite3_column_text(s,i));
					break;
			}
		}
		nuse-=2; // trailing ',\n'
		nuse+=snprintf(outbuf+nuse,blen-nuse, "\n\t},\n");
	}
	if(i&&res==SQLITE_DONE) nuse-=2; //trailing ',\n'
	nuse+=snprintf(outbuf+nuse,blen-nuse,"\n]\n");

	cout << outbuf << endl;
	*/
}

void BaseDb::deleteUsingDate(string tableName, string date) {
        Query* query = getQuery();
        stringstream ss; ss << "delete from " << tableName << " where date < @targetDate;";
        query->addQuery(ss.str());
        query->addString(1, date);
        query->execute();
        delete query;
}

void BaseDb::vacume(){
        Query* query = getQuery();
        query->addQuery("vacuum;analyze;");
        query->execute();
        delete query;
}

