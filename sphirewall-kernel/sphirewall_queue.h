/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_QUEUE_H
#define SPHIREWALL_QUEUE_H

#define RAWSIZE 1501

#define PREROUTING 1
#define FILTER 2
#define POSTROUTING 3

#define NAT_IGNORE 0
#define NAT_DNAT 1
#define NAT_SNAT 2

struct sq_packet {
        int overMtu;
        int len;
        int id;

        int indev;
        int outdev;

        int hwlen;
        u_int8_t hw_addr[8];

        unsigned char* raw_packet;
};

struct nat_instruction {
        int type;
        unsigned int target_address;
        int target_port;
};

struct message {
        int verdict;
        unsigned int id;
        int hook;
        int modified;

        struct sq_packet packet;
        struct nat_instruction nat;

        void* entryPtr;
};

#endif
