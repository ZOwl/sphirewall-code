/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Dhcp3Configuration.h"
#include <iostream>
#include <sstream>

using namespace std;
using std::string;

#include "Core/System.h"
#include "Utils/Utils.h"
#include "Utils/NetDevice.h"
#include "BandwidthDb/Bandwidth.h"
#include "Auth/UserDb.h"
#include "Core/SignalHandler.h"
#include "SFwallCore/Firewall.h" 
#include "Auth/AuthenticationHandler.h"
#include "Api/JsonManagementService.h"
#include "Api/ManagementSocketServer.h"
#include "Core//Dhcp3Configuration.h"
#include "ConnectionManager.h"
#include "Core/Openvpn.h"
#include "Utils/EmailSender.h"
#include "Core/Cloud.h"
#include "Core/Wireless.h"
#include "Core/HostDiscoveryService.h"
#include "Utils/DynDns.h"
//#include "autoconf.h"
#include "Core/Event.h"
#include "Ids/Ids.h"

System* System::m_instance = 0; // Pointer to THE System instance

System *System::getInstance() {
	if (0 == m_instance)
		m_instance = new System();
	return m_instance;
}

void System::sysRelease() {
	if (m_instance)
		delete m_instance;
	m_instance = 0;
}

System::System() {
	version = "0.9.9.7 ";
	std::stringstream vn; 
	//vn << SVN_VERSION << "@" << BUILD_TIME;
	versionName = vn.str(); 
}

System::~System() {
	delete sFirewall;
	delete arp;
	delete sessionDb;
	delete groupDb;
	delete userDb;
	delete sysMonitor;
	delete bandwidthDb;
	delete ids;
	delete events;
	delete cronManager;
}

pid_t System::getProcessId() {
	lockFile = "/var/lock/sphirewall.lock";
	return retLockPid(lockFile);
}

void System::init(const string& configFile, runlevel_t runLevel) {
	System::getInstance()->ALIVE = true;
	if (runLevel == RUNLEVEL_NORMAL || runLevel == RUNLEVEL_DEV_MODE) {
		cout << "\nsphirewalld: version " << version << " -- " << versionName << " pid:" << getpid() << endl;
		config.setConfigurationManager(&configurationManager);
		lockFile = "/var/lock/sphirewall.lock";
		if (retLockPid(lockFile) != -1) {
			logger.getDefault()->log(SLogger::ERROR, "System::init","A lock file exists, cannot start the core daemon", true);
			exit(-1);
		}else{
			logger.getDefault()->log(SLogger::EVENT, "System::init", "Creating lock file", true);
			createLock(lockFile);
		}

		try{
			/*Setup the logger*/
			logger.getDefault()->log(SLogger::EVENT, "System::init", "Starting Sphirewall", true);
			logger.getDefault()->log(SLogger::EVENT, "System::init()", "Loading the configuration files", true);
			configurationManager.load(configFile);
			config.loadConfig();

			logger.getDefault()->level = SLogger::INFO;
			logger.start();
			
			emailSender = new EmailSender(&configurationManager, logger.getDefault());
                        events = new EventDb(&logger, &config);

                        logger.getDefault()->log(SLogger::EVENT, "System::init()", "Loading interfaces", true);
			interfaces.setConfig(&configurationManager);
			interfaces.setEventDb(events);
			interfaces.load();

			cronManager = new CronManager(logger.getDefault(), &config);
			cronManager->registerJob(events);		

			logger.getDefault()->log(SLogger::INFO, "System::init()","Setting up the Firewall", true);
			arp = new HostDiscoveryService(&logger, cronManager, &config);
			arp->setEventDb(events);			

			ids = new IDS(events, &logger);
			cronManager->registerJob(ids);

			authLogger = new SLogger::LogContext(SLogger::INFO, "AUTH", "sphirewalld-auth");
			logger.registerContext(authLogger);

			groupDb = new GroupDb(authLogger, &configurationManager);
			groupDb->loadDb();

			userDb = new UserDb(events, groupDb, authLogger, &configurationManager, sessionDb);
			userDb->loadDb();
			groupDb->registerRemovedListener(userDb);		
	
			bandwidthDb = new BandwidthDbPrimer(userDb, &config, &logger);
			cronManager->registerJob(bandwidthDb);
			logger.getDefault()->log(SLogger::EVENT, "System::init()", "Bandwidth Database Started", true);
			bandwidthDb->setMaxBufferSize(50000);

			sFirewall = new SFwallCore::Firewall(&config, &logger, &configurationManager);
			sFirewall->setInterfaceManager(&interfaces);

			logger.getDefault()->log(SLogger::INFO, "System::init()", "About to start the SFwallCore Engine", true);
			quotaWatch = new QuotaWatch(userDb, groupDb, &configurationManager);
			quotaWatch->load();

			sysMonitor = new SysMonitor(events, &logger, cronManager, sFirewall, bandwidthDb, quotaWatch);
			events->load();
			ids->start();

			sessionDb = new SessionDb(authLogger, cronManager, sFirewall->getPlainConnTracker());
			sessionDb->loadDb();
			userDb->setSessionDb(sessionDb);

			loggingConfig = new LoggingConfiguration(&configurationManager, &logger);

			this->authenticationManager = new AuthenticationManager(cronManager);
			this->authenticationManager->setLocalUserDb(userDb);
			this->authenticationManager->setLocalGroupDb(groupDb);
			this->authenticationManager->setLogger(&logger);
			cronManager->registerJob(new AuthenticationManager::SyncCron(this->authenticationManager));

			LocalDbAuthenticationMethod* localMethod = new LocalDbAuthenticationMethod();
			localMethod->setUserDb(userDb);
			this->authenticationManager->addMethod(localMethod);

			LdapAuthenticationMethod* ldap = new LdapAuthenticationMethod();
			ldap->setConfigurationManager(&configurationManager);
			this->authenticationManager->addMethod(ldap);
			ldap->setLogger(&logger);

			PamAuthenticationMethod* pam = new PamAuthenticationMethod();
			pam->setConfigurationManager(&configurationManager);
			this->authenticationManager->addMethod(pam);

			BasicHttpAuthenticationHandler* bha = new BasicHttpAuthenticationHandler();
			bha->setConfigurationManager(&configurationManager);
			this->authenticationManager->addMethod(bha);

			dhcpConfigurationManager = new Dhcp3ConfigurationManager(&configurationManager, &interfaces);
			dhcpConfigurationManager->initalise();
			dhcpConfigurationManager->publishConfigurationToServer();			

			connectionManager = new ConnectionManager();
			connectionManager->setConfigurationManager(&configurationManager);
			connectionManager->load();

			openvpn = new OpenvpnManager();
			openvpn->setConfigurationManager(&configurationManager);
			openvpn->initalise();			

			wirelessConfiguration = new WirelessConfiguration(&configurationManager, logger.getDefault());
			if(wirelessConfiguration->enabled()){
				wirelessConfiguration->start();
			}

			globalDynDns = new GlobalDynDns(&configurationManager);
			globalDynDns->load();
			cronManager->registerJob(globalDynDns);
	
			JsonManagementService* json = new JsonManagementService();
			json->setConfig(&config);
			json->setIds(System::getInstance()->ids);
			json->setSysMonitor(System::getInstance()->sysMonitor);
			json->setEventDb(System::getInstance()->events);
			json->setInterfaceManager(&System::getInstance()->interfaces);
			json->setDnsConfig(&System::getInstance()->dnsConfig);
			json->setFirewall(System::getInstance()->sFirewall);
			json->setGroupDb(System::getInstance()->groupDb);
			json->setUserDb(System::getInstance()->userDb);
			json->setSessionDb(System::getInstance()->sessionDb);
			json->setHostDiscoveryService(System::getInstance()->arp);
			json->setLoggingConfiguration(System::getInstance()->loggingConfig);
			json->setOpenvpn(openvpn);
			json->setDhcp(dhcpConfigurationManager);
			json->setAuthenticationManager(authenticationManager);
			json->setConnectionManager(connectionManager);
			json->initDelegate();

			ManagementSocketServer* webSvc = new ManagementSocketServer(8001, json, false); // Web/non-SSL CLI interface server.
			webSvc->setLogger(&logger);
			webSvc->setConfigurationManager(&configurationManager);

			ManagementSocketServer* sslSvc = new ManagementSocketServer(8003, json, true); // SSL CLI interface server.
			sslSvc->setLogger(&logger);
			sslSvc->setConfigurationManager(&configurationManager);

                        cloud = new CloudConnection(&configurationManager, logger.getDefault());
                        new boost::thread(boost::bind(&CloudConnection::connect, cloud));

			cronManager->start();
			sFirewall->start(true);
		
			loggingConfig->load();
			new boost::thread(boost::bind(&ManagementSocketServer::run, webSvc));
			new boost::thread(boost::bind(&ManagementSocketServer::run, sslSvc));

                        events->add(new Event(START, EventParams()));

			sysMonitor->registerMetric(sFirewall->connectionTracker);
			sysMonitor->registerMetric(sFirewall);
			
		}catch(JSONFieldNotFoundException*){
			logger.getDefault()->log(SLogger::CRITICAL_ERROR, "System::init()", "Could not load the configuration, missing required field", true);
			init("", EXIT);
		}catch(ConfigurationException* e){
			stringstream ss; ss << "Could not load the configuration: " << e->what();
			logger.getDefault()->log(SLogger::CRITICAL_ERROR, "System::init()", ss.str(), true);
			init("", EXIT);
		}

		while (ALIVE) {
			sleep(10);
		}
	}

	if (runLevel == RUNLEVEL_STOP || runLevel == RUNLEVEL_SEGFAULT || runLevel == EXIT) {
		logger.getDefault()->log(SLogger::EVENT, "System::init()", "sphirewalld is exiting", true);
		logger.flush();
		//sFirewall->stop();
		removeLock(lockFile);
		exit(-1);
	}
}

/*As the system is running in effective id 0 we need to drop the privs on some exec commands*/
int System::exec(string command, string &ret, bool RUN_AS_ROOT) {
	string sbuf;

	if (RUN_AS_ROOT) {
		char cbuf[1024];

		command += " 2>&1";

		FILE* fp;
		fp = popen(command.c_str(), "r");

		while (fgets(cbuf, 1024, fp) != NULL)
			sbuf += cbuf;

		pclose(fp);
	}

	if (!RUN_AS_ROOT) {
		char cbuf[1024];
		command += " 2>&1";

		FILE* fp;
		fp = popen(command.c_str(), "r");

		while (fgets(cbuf, 1024, fp) != NULL)
			sbuf += cbuf;

		pclose(fp);
	}

	ret = sbuf;
	return 0;
}

int System::exec(string command, string &ret) {
	string sbuf;

	char cbuf[1024];

	command += " 2>&1";

	FILE* fp;
	fp = popen(command.c_str(), "r");

	while (fgets(cbuf, 1024, fp) != NULL)
		sbuf += cbuf;

	pclose(fp);

	ret = sbuf;
	return 0;
}

int System::execWithNoFds(std::string s){	
	if(!fork()){
		int fd_limit = sysconf(_SC_OPEN_MAX);
		for(int x=3; x < fd_limit; x++){
			close(x);
		}

		system(s.c_str());
		exit(-1);
	}
}


void System::createLock(string lockFile) {
	ofstream nfp(lockFile.c_str());
	nfp << intToString(getpid());
	nfp.close();
}

void System::removeLock(string lockFile) {
	string cmd = "rm " + lockFile;

	string ret;
	exec(cmd.c_str(), ret, true);
}

pid_t System::retLockPid(string lockFile) {
	ifstream fp(lockFile.c_str());
	if (fp.is_open()) {
		string str_pid;
		getline(fp, str_pid);
		return atoi(str_pid.c_str());
	}
	return -1;
}
