/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <boost/asio.hpp>
#include <vector>
#include <sstream>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace boost::algorithm;

#include "Core/Cloud.h"
#include "Api/JsonManagementService.h"
#include "Core/System.h"
#include "Core/ConfigurationManager.h"

using boost::asio::ip::tcp;
using namespace std;

std::string CloudConnection::getHostname(){
	if(config->hasByPath("cloud:hostname")){	
		return config->get("cloud:hostname")->string();	
	}

	return "";	
}

std::string CloudConnection::getKey(){
        if(config->hasByPath("cloud:key")){
                return config->get("cloud:key")->string();
        }

        return "";      
}

std::string CloudConnection::getDeviceId(){
	if(config->hasByPath("cloud:deviceid")){
                return config->get("cloud:deviceid")->string();
        }

	string ret = FileUtils::read("/sys/class/net/eth0/address");
	trim(ret);
	return ret;
}

int CloudConnection::getPort(){
	if(config->hasByPath("cloud:port")){
		return config->get("cloud:port")->number();
	}

	return -1;      
}

bool CloudConnection::enabled(){
	if(config->hasByPath("cloud:enabled")){
		return config->get("cloud:enabled")->boolean();
	}

	return false;
}

bool CloudConnection::connected() {
	return this->connectedFlag;
}

void CloudConnection::connect(){
	while(System::getInstance()->ALIVE){
		if(enabled()){
			try{
				connectedFlag = false;
				boost::asio::io_service io_service;
				stringstream ss; ss << 8085;

				tcp::resolver resolver(io_service);
				tcp::resolver::query query(tcp::v4(), getHostname(), ss.str());
				tcp::resolver::iterator iterator = resolver.resolve(query);

				stringstream ss4; ss4 << "Connecting to a sphirewall cloud provider, " << getHostname() << ":" << getPort();
				logger->log(SLogger::INFO, ss4.str());
				tcp::socket s(io_service);
				s.connect(*iterator);

				connectedFlag = true;
				stringstream auth;
				auth << "{\"key\":\""<< getKey() << "\", \"deviceid\":\""<< getDeviceId() <<"\"}\n";
				boost::asio::write(s, boost::asio::buffer(auth.str(), auth.str().size()));

				stringstream ss2; ss2 << "Connected to cloud provider, ready to accept rpc calls";
				logger->log(SLogger::INFO, ss2.str());

				JsonManagementService* service = new JsonManagementService();
				service->setConfig(&System::getInstance()->config);
				service->setIds(System::getInstance()->ids);
				service->setSysMonitor(System::getInstance()->sysMonitor);
				service->setEventDb(System::getInstance()->events);
				service->setInterfaceManager(&System::getInstance()->interfaces);
				service->setDnsConfig(&System::getInstance()->dnsConfig);
				service->setFirewall(System::getInstance()->sFirewall);
				service->setGroupDb(System::getInstance()->groupDb);
				service->setUserDb(System::getInstance()->userDb);
				service->setSessionDb(System::getInstance()->sessionDb);
				service->setHostDiscoveryService(System::getInstance()->arp);
				service->setLoggingConfiguration(System::getInstance()->loggingConfig);
				service->setOpenvpn(System::getInstance()->openvpn);
				service->setDhcp(System::getInstance()->dhcpConfigurationManager);
				service->setAuthenticationManager(System::getInstance()->authenticationManager);
				service->setConnectionManager(System::getInstance()->connectionManager);
				service->ignoreAuthentication(true);
				service->initDelegate();

				while(1){
					boost::asio::streambuf response;
					boost::asio::read_until(s, response, "\n");
					string buffer = string((istreambuf_iterator<char>(&response)), istreambuf_iterator<char>());
					string output = service->process(buffer);		
					output += "\n";
					boost::asio::write(s, boost::asio::buffer(output, output.size()));

					stringstream ss3; ss3 << "Received and processed a request from the cloud service provider";
					logger->log(SLogger::INFO, ss3.str());
				}
				
				delete service;
				s.close();
				io_service.stop();
			}catch(exception& e){
				connectedFlag = false;				

				stringstream ss3; ss3 << "Connection to a sphirewall cloud provider was dropped, or could not be established";
				logger->log(SLogger::ERROR, ss3.str());
			}
		}
		sleep(10);
	}
}

