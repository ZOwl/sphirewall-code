/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_USERS_H_INCLUDED
#define SPHIREWALL_USERS_H_INCLUDED

#include <string>
#include <vector>
#include <map>

using namespace std;

#include "Auth/Group.h"
#include "Auth/Quota.h"
#include "Utils/Lock.h"
#include "Auth/AuthenticationType.h"

extern bool validateUserInfo(string username, string password, string fname,
		string lname, vector<Group> groups);

class QuotaInfo;
class User {
public:
	//Public member variables
	friend class UserDb;
	
	User() {
		this->quota = new QuotaInfo();
		this->quotaExceeded= false;
		this->groupLock = new Lock();
	}

	User(string username)
	: m_user(username) {
		this->quota = new QuotaInfo();
                this->quotaExceeded= false;
		this->groupLock = new Lock();
	}

	~User() {
	}

	void addGroup(Group* group);
	void removeGroup(Group* group);
	bool checkForGroup(Group* group);
	bool enable();
	bool disable();

	string getUserName();
	string getFname();
	string getLname();

	string getPassword();

	bool getIsEnabled();

	int getLastLogin();
	int getLastFail();

	std::string getEmail() const;

	void setFname(string str);
	void setLname(string str);

	void setGroups(vector<Group*> groups);
	bool enableDisable(bool enable);
	void setPassword(string password);

	void setEmail(std::string email);
	bool isAllowedMuiAccess();
	QuotaInfo* getQuota() const {
		return this->quota;
	}

	void setUserName(std::string n);
	AuthenticationType getAuthenticationType();
	void setAuthenticationType(AuthenticationType authenticationType);
	std::vector<Group*> getGroups() const;
	
	int getCreationTime() const {
		return m_creationTime;
	}
	
	void setCreationTime(int t){
		this->m_creationTime = t;
	}
	
	bool isQuotaExceeded() const {
		return quotaExceeded;
	}
	
	void setQuotaExceeded(bool value) {
		this->quotaExceeded = value;
	}

	std::string dn;
protected:
	bool isEnabled;

private:
	string m_fname;
	string m_lname;

	string m_password;

	int m_lastLogin;
	int m_lastFail;
	int m_creationTime;

	vector<Group*> m_groups;
	string m_user;

	bool isTemp;
	map<string, int> hosts;

	string m_email;
	AuthenticationType authenticationType;
	QuotaInfo* quota;
	bool quotaExceeded;
	Lock* groupLock;
};

#endif
