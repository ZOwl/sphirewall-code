# This script creates a self-signed certificate good for 1095 days.

openssl req -new -x509 -key serverkey.pem -out servercert.pem -days 1095
