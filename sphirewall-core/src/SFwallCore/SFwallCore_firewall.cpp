/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream> 
#include <string.h>
#include <time.h>
#include <sstream>
#include <vector>

typedef unsigned char u8;
typedef signed char s8;
typedef unsigned short u16;
typedef signed short s16;
typedef unsigned long u32;
typedef signed long s32;
using namespace std;

#include "Core/System.h"
#include "SFwallCore/Capture.h"
#include "SFwallCore/Acl.h"
#include "SFwallCore/Alias.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/ConnTracker.h"
#include "Core/Config.h"
#include "Core/Logger.h"
#include "SFwallCore/ApplicationLevelFilter.h"
#include "Ids/Ids.h"
#include "BandwidthDb/Bandwidth.h"

using namespace SLogger;

int SFwallCore::Firewall::TCP_CONNECTION_TIMEOUT_THESHOLD = TimeUtils::DAY_SECONDS * 5;
int SFwallCore::Firewall::UDP_CONNECTION_TIMEOUT_THESHOLD = 10;
int SFwallCore::Firewall::ICMP_CONNECTION_TIMEOUT_THESHOLD = 10;
int SFwallCore::Firewall::SYN_SYNACK_WAIT_TIMEOUT = 60 * 2;
int SFwallCore::Firewall::SYN_ACKACK_TIMEOUT = 60;
int SFwallCore::Firewall::FIN_FINACK_WAIT_TIMEOUT = 60 * 2;
int SFwallCore::Firewall::FIN_WAIT_TIMEOUT = 60 * 2;
int SFwallCore::Firewall::TIME_WAIT_TIMEOUT = 60 * 2;
int SFwallCore::Firewall::CLOSE_WAIT_TIMEOUT = 60 * 60 * 12;
int SFwallCore::Firewall::LAST_ACK_TIMEOUT = 30;
int SFwallCore::Firewall::CACHE_TTL_MAX = 20;
int SFwallCore::Firewall::SQUEUE_DEQUEUE_YIELD_TIMEOUT = 10000;
int SFwallCore::Firewall::SQUEUE_DEQUEUE_YIELD_SLEEP_INTERVAL = 1000;

int SFwallCore::Firewall::POST_PROCESSOR_ENABLED = 1;
int SFwallCore::Firewall::BLOCK_LIST_ENABLED = 1;
int SFwallCore::Firewall::PREROUTING_ENABLED = 1;
int SFwallCore::Firewall::POSTROUTING_ENABLED = 1;
int SFwallCore::Firewall::QOS_ENABLED = 0;
int SFwallCore::Firewall::CONNECTIONTRACKER_CACHE_ENABLED = 1;
int SFwallCore::Firewall::BANDWIDTHDB_ENABLED= 1;
int SFwallCore::Firewall::REWRITE_ENABLED = 1;
int SFwallCore::Firewall::REWRITE_PROXY_REQUESTS_ENABLED= 1;
int SFwallCore::Firewall::REQUIRE_TLS= 0;

SFwallCore::Firewall::Firewall(Config* config, SLogger::Logger* logger, ConfigurationManager* configurationManager)
: config(config), logger(logger), configurationManager(configurationManager){

	/*Install the runtime configuration*/
	config->getRuntime()->loadOrPut("TCP_CONNECTION_TIMEOUT_THESHOLD", &TCP_CONNECTION_TIMEOUT_THESHOLD);
	config->getRuntime()->loadOrPut("UDP_CONNECTION_TIMEOUT_THESHOLD", &UDP_CONNECTION_TIMEOUT_THESHOLD);
	config->getRuntime()->loadOrPut("ICMP_CONNECTION_TIMEOUT_THESHOLD", &ICMP_CONNECTION_TIMEOUT_THESHOLD);
	config->getRuntime()->loadOrPut("SYN_SYNACK_WAIT_TIMEOUT", &SYN_SYNACK_WAIT_TIMEOUT);
	config->getRuntime()->loadOrPut("SYN_ACKACK_TIMEOUT", &SYN_ACKACK_TIMEOUT);
	config->getRuntime()->loadOrPut("FIN_FINACK_WAIT_TIMEOUT", &FIN_FINACK_WAIT_TIMEOUT);
	config->getRuntime()->loadOrPut("FIN_WAIT_TIMEOUT", &FIN_WAIT_TIMEOUT);
	config->getRuntime()->loadOrPut("TIME_WAIT_TIMEOUT", &TIME_WAIT_TIMEOUT);
	config->getRuntime()->loadOrPut("CLOSE_WAIT_TIMEOUT", &CLOSE_WAIT_TIMEOUT);
	config->getRuntime()->loadOrPut("LAST_ACK_TIMEOUT", &LAST_ACK_TIMEOUT);
	config->getRuntime()->loadOrPut("TCPCONN_CACHE_TTL_MAX", &CACHE_TTL_MAX);
	config->getRuntime()->loadOrPut("SQUEUE_DEQUEUE_YIELD_TIMEOUT", &SQUEUE_DEQUEUE_YIELD_TIMEOUT);
	config->getRuntime()->loadOrPut("SQUEUE_DEQUEUE_YIELD_SLEEP_INTERVAL", &SQUEUE_DEQUEUE_YIELD_SLEEP_INTERVAL);
	config->getRuntime()->loadOrPut("POST_PROCESSOR_ENABLED", &POST_PROCESSOR_ENABLED);
	config->getRuntime()->loadOrPut("BLOCK_LIST_ENABLED", &BLOCK_LIST_ENABLED);
	config->getRuntime()->loadOrPut("PREROUTING_ENABLED", &PREROUTING_ENABLED);
	config->getRuntime()->loadOrPut("POSTROUTING_ENABLED", &POSTROUTING_ENABLED);
	config->getRuntime()->loadOrPut("QOS_ENABLED", &QOS_ENABLED);
	config->getRuntime()->loadOrPut("CONNECTIONTRACKER_CACHE_ENABLED", &CONNECTIONTRACKER_CACHE_ENABLED);
	config->getRuntime()->loadOrPut("BANDWIDTHDB_ENABLED", &BANDWIDTHDB_ENABLED);
	config->getRuntime()->loadOrPut("REWRITE_ENABLED", &REWRITE_ENABLED);
	config->getRuntime()->loadOrPut("REWRITE_PROXY_REQUESTS_ENABLED", &REWRITE_PROXY_REQUESTS_ENABLED);
	config->getRuntime()->loadOrPut("REQUIRE_TLS", &REQUIRE_TLS);

	totalTcpPackets = 0;
	totalUdpPackets = 0;
	totalIcmpPackets = 0;
	totalDefaultAction = 0;

	debug = false;
	defaultAction = -1;

	totalTcpUp = 0;
	totalTcpDown = 0;
	totalUdpUp = 0;
	totalUdpDown = 0;

	avgPacketProcess = new AverageSampler(60);
	transferSampler = new FlexibleSecondIntervalSampler();
	noPacketsSampler = new FlexibleSecondIntervalSampler();
        noAcceptsSampler = new FlexibleSecondIntervalSampler();
        noDropsSampler = new FlexibleSecondIntervalSampler();
        noRstSampler = new FlexibleSecondIntervalSampler();

	logContext = new LogContext(EVENT, "FIREWALL", "sphirewalld-firewall");
	logger->registerContext(logContext);

	post = new PostProcessor(logContext, this);

	connectionTracker = new PlainConnTracker(logContext, this);
}

SFwallCore::Firewall::~Firewall() {
	delete acls;
	delete trafficShaper;
	delete aliases;
	delete garbageCron;
	delete connectionTracker;
}

void SFwallCore::Firewall::start(bool d) {
        logContext->log(SLogger::EVENT, "SFwallCore::Firewall::start()", "Firewall Starting", true);

	logContext->log(SLogger::EVENT, "SFwallCore::Firewall::start()", "Loading traffic shaper", true);
	garbageCron = new GarbageCollectorCron(connectionTracker, logContext);
	trafficShaper = new TrafficShaper(configurationManager, System::getInstance()->getGroupDb(), System::getInstance()->getSessionDb(), connectionTracker);

	logContext->log(SLogger::EVENT, "SFwallCore::Firewall::start()", "Loading aliases and pools", true);
	aliases = new AliasDb(logger->getDefault(), configurationManager);
	aliases->load();

        rewriteEngine = new RewriteEngine(configurationManager, logContext, aliases);
        rewriteEngine->load();  

	acls = new ACLStore(logContext, System::getInstance()->getUserDb(), System::getInstance()->getGroupDb(), System::getInstance()->getSessionDb(), aliases);
	acls->setConfigurationManager(configurationManager);
	acls->setInterfaceManager(interfaceManager);
	interfaceManager->registerChangeListener(new ACLStore::InterfaceChangeListener(acls));

	logContext->log(SLogger::EVENT, "SFwallCore::Firewall::start()", "Loading application filter", true);
	httpFilter = new HttpUrlApplicationFilter(configurationManager, aliases, System::getInstance()->getGroupDb());
	System::getInstance()->getGroupDb()->registerRemovedListener(httpFilter);

	httpFilter->setEventDb(System::getInstance()->getEventDb());
	httpFilter->load();

	debug = d;
	trafficShaper->start();

        logContext->log(SLogger::EVENT, "SFwallCore::Firewall::start()", "Loading rules and acls", true);
	acls->loadNatFile();
	acls->loadFilterFile();
	acls->loadDenyList();
	acls->loadPriorityFile();
	aliases->setAclStore(acls);
	aliases->registerRemovedListeners(httpFilter);	
	aliases->registerRemovedListeners(acls);

	boost::thread th(startCapture);

	logContext->log(SLogger::EVENT, "SFwallCore::Firewall::start()", "Starting post processor", true);
	post->registerHandler(System::getInstance()->getIds());
	post->start();

        connectionTracker->registerDeleteHook(System::getInstance()->getBandwidthDbPrimer());
	System::getInstance()->getCronManager()->registerJob(garbageCron);
}

void SFwallCore::Firewall::stop() {
	sqClient.disconnect();
	logger->unregisterContext(logContext);
}

void SFwallCore::GarbageCollectorCron::run() {
	waiting = true;

	logger->log(SLogger::INFO, "GarbageCollectorCron::run()", "Starting cleanup on the Plain Connection Tracker");
	int ct_count = connectionTracker->cleanup();
	logger->log(SLogger::INFO, "GarbageCollectorCron::run()",
			"Finished cleanup on the Plain Connection Tracker: Removed connection count:" + intToString(ct_count));

	waiting = false;
}

void SFwallCore::Firewall::sample(map<string, long>& input){
	input["firewall.transfer.persec"] = transferSampler->value();
	input["firewall.packets.persec"] = noPacketsSampler->value();
	input["firewall.accepts.persec"] = noAcceptsSampler->value();
	input["firewall.drops.persec"] = noDropsSampler->value();
	input["firewall.rsts.persec"] = noRstSampler->value();
}
