/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <time.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <sstream>
#include <vector>
#include <queue>

using namespace std;

#include "Utils/TimeUtils.h"
#include "Database/BandwidthDb.h"
#include "Utils/Utils.h"
#include "Utils/IP4Addr.h"
#include "Database/TrafficAnalyticsDb.h"
#include "Database/MonitoringDb.h"
#include "Utils/TimeWrapper.h"

bool MonitoringDb::initDb() {
	int dbStatus;
	string message;
	string workStr;

	try {
		dbStatus = sqlite3_open_v2(dbName.c_str(), &this->dbConn,
				SQLITE_OPEN_READWRITE, NULL);


		if (dbStatus != SQLITE_OK) { // Database does not exist.  Create a new one
			dbStatus = sqlite3_open_v2(dbName.c_str(),
					&this->dbConn, SQLITE_OPEN_READWRITE |
					SQLITE_OPEN_CREATE, NULL);

			execSQL("create table metrics (key INTEGER, date DATE, value INTEGER, primary key (key, date));");
			execSQL("create table metrics_hour (key INTEGER, date DATE, value INTEGER, primary key (key, date));");	
			execSQL("create table keys (key INTEGER PRIMARY KEY AUTOINCREMENT, skey TEXT);");	

			execSQL("create index metrics_date on metrics(date);");
			execSQL("create index metrics_hour_date on metrics_hour(date);");
			execSQL("create index metrics_hour_date_key on metrics_hour(date, key);");
			execSQL("create index keys_skey on keys(skey);");

			if (dbStatus != SQLITE_OK) { // Error in opening DB.
				throw new DatabaseException(sqlite3_errmsg(this->connection()), "Could not open database");
			}


		}
	}catch (exception& except) {
		message = "Exception opening/creating Bandwidth DB : ";
		message.append(except.what());
		logger->log(SLogger::ERROR,
				"BandwidthDb::BandwidthDb", message);
		this->dbConn = NULL;
		return false;
	}

	return true;
}

list<string> MonitoringDb::availableMetrics(){
	list<string> ret;
	try{
		Query* query = getQuery();
		query->addQuery("select skey from keys;");
		while (query->next()) {
			ret.push_back(query->stringResult(0));
		}

		delete query;
	}catch(DatabaseException* e){
		logger->log(SLogger::ERROR, "MonitoringDb::getMetrics", e->message(), true);
	}
	return ret;
}

void MonitoringDb::getMetrics(std::vector<std::pair<std::string, int> >& items, std::string key, Time start, Time finish) {
	string sql;
	if (finish.timestamp() - start.timestamp() < (60 * 60 * 48)) {
		sql = "select value, date from metrics where key = @key and date between @start and @finish;";
	}
	else {
		sql = "select value, date from metrics_hour where key = @key and date between @start and @finish;";
	}

	try {
		Query* query = getQuery();
		query->addQuery(sql);
		query->addNumber(1, getOrCreateKey(key));
		query->addString(2, start.midnight());
		query->addString(3, finish.last());

		while (query->next()) {
			pair<std::string, int> p;
			p.first  = query->stringResult(1);
			p.second = query->longResult(0); 

			items.push_back(p);
		}
		delete query;
	}catch(DatabaseException* e){
		logger->log(SLogger::ERROR, "MonitoringDb::getMetrics", e->message(), true);
	}
}

void MonitoringDb::getAllMetricsTotals(std::vector<std::pair<std::string, int> >& items, Time start, Time finish) {
	string sql;
	if (finish.timestamp() - start.timestamp() < (60 * 60 * 48)) {
		sql = "select skey, sum(value) from keys inner join metrics on (keys.key = metrics.key) where date between @start and @finish group by skey order by skey;";
	}
	else {
		sql = "select skey, sum(value) from keys inner join metrics_hour on (keys.key = metrics_hour.key) where date between @start and @finish group by skey order by skey;";
	}

	try {
		Query* query = getQuery();
		query->addQuery(sql);
		query->addString(1, start.midnight());
		query->addString(2, finish.last());

		while (query->next()) {
			pair<std::string, int> p;
			p.first  = query->stringResult(0);
			p.second = query->longResult(1); 

			items.push_back(p);
		}
		delete query;
	}catch(DatabaseException* e){
		logger->log(SLogger::ERROR, "MonitoringDb::getMetrics", e->message(), true);
	}
}

void MonitoringDb::cleanupMetrics() {
        string targetDate;
        TimeUtils::convertToString(time(NULL) - (60 * 60 * 24), "%Y-%m-%d", targetDate);

        deleteUsingDate("metrics", targetDate);
}

int MonitoringDb::getOrCreateKey(std::string key){
	Query* query = getQuery();
	query->addQuery("select key from keys where skey = @skey;");
	query->addString(1, key);
	while (query->next()) {
		int res = query->longResult(0);
		delete query;
		return res;
	}

	delete query;
	//No key, we must create one:
	Query* iquery = getQuery();
	iquery->addQuery("insert into keys (skey) values(@skey);");
	iquery->addString(1, key);
	iquery->execute();
	delete iquery;		
	return getOrCreateKey(key);
}

void MonitoringDb::addMetric(std::string key, int value) {
	string targetDate;
	TimeUtils::convertToString(time(NULL), "%Y-%m-%d %H:%M:%S", targetDate);

	try {
		Query* query = getQuery();
		query->addQuery("insert into metrics values(@key, datetime(@date), @value);");           
		query->addNumber(1, getOrCreateKey(key));                   
		query->addString(2, targetDate);
		query->addNumber(3, value);
		query->execute();
		delete query;

	}catch(DatabaseException* e){
		logger->log(SLogger::ERROR, "add metric failed", e->message());
	}
}

void MonitoringDb::updateMetricsHour(std::string key) {
	string targetDate;
	TimeUtils::convertToString(time(NULL), "%Y-%m-%d %H", targetDate);
	stringstream ss; ss << "delete from metrics_hour where date = '" << targetDate << "';";

	Query* deleteQuery = getQuery();
	deleteQuery->addQuery(ss.str());
	deleteQuery->execute();
	delete deleteQuery;

	Query* updateQuery = getQuery();
	updateQuery->addQuery("insert or replace into metrics_hour (date, key, value) select strftime('%Y-%m-%d %H', date), key, avg(value) from metrics group by key, strftime('%Y-%m-%d %H',date) ;");
	updateQuery->execute();
	delete updateQuery;
}
