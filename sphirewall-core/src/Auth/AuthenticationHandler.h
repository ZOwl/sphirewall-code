/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AUTH
#define AUTH

#define LDAP_DEPRECATED 1

#include <ldap.h>
#include <list>
#include "Core/Logger.h"
#include "Core/Cron.h"
#include "Auth/AuthenticationType.h"

class UserDb;
class User;
class ConfigurationManager;
class GroupDb;
class Lock;

class AuthenticationMethod {
public:
	virtual bool authenticate(User*, std::string username, std::string password) = 0;
	virtual bool isEnabled() = 0;
	virtual bool canConnect() = 0;
	virtual AuthenticationType getType() = 0;
	virtual User* findUser(std::string username) = 0;

	virtual std::list<std::string> getGroupNames(User* user){
		return std::list<std::string>();	
	}

	virtual void sync(UserDb*, GroupDb*){}
};

class BasicHttpAuthenticationHandler : public virtual AuthenticationMethod {
public:

	BasicHttpAuthenticationHandler() {
	}

	AuthenticationType getType() {
		return BASIC_HTTP;
	}

	bool isEnabled();
	bool canConnect() {
		return true;
	}

	bool authenticate(User*, std::string username, std::string password);
	User* findUser(std::string username);

	void setConfigurationManager(ConfigurationManager* configurationManager) {
		this->configurationManager = configurationManager;
	}

protected:
	ConfigurationManager* configurationManager;

};


class PamAuthenticationMethod : public virtual AuthenticationMethod {
public:

	PamAuthenticationMethod() {
	}

	AuthenticationType getType() {
		return PAM_DB;
	}

	bool isEnabled();
	bool canConnect();
	bool authenticate(User*, std::string username, std::string password);
	User* findUser(std::string username);

	void setConfigurationManager(ConfigurationManager* configurationManager) {
		this->configurationManager = configurationManager;
	}

protected:
	ConfigurationManager* configurationManager;

};

class LdapAuthenticationMethod : public virtual AuthenticationMethod {
public:

	LdapAuthenticationMethod() {
	}

	AuthenticationType getType() {
		return LDAP_DB;
	}

	bool isEnabled();
	bool canConnect();
	bool authenticate(User*, std::string username, std::string password);
	User* findUser(std::string username);
	std::list<std::string> getGroupNames(User* user);
	void setConfigurationManager(ConfigurationManager* configurationManager) {
		this->configurationManager = configurationManager;
	}

	void setLogger(SLogger::Logger* logger){
		this->logger = logger;
	}

	void sync(UserDb*, GroupDb*);
protected:
	ConfigurationManager* configurationManager;
	LDAP* connect(std::string hostname, int port, std::string username, std::string password);

	std::string getHostname() const;
	int getPort() const;
	std::string getLdapUsername() const;
	std::string getLdapPassword() const;
	std::string getBaseDn() const;
	void refreshUserGroups(User* user);
	SLogger::Logger* logger;

private:
	std::string processGroupAttribute(std::string input);
};

class LocalDbAuthenticationMethod : public virtual AuthenticationMethod {
public:

	bool isEnabled() {
		return true;
	}

	AuthenticationType getType() {
		return LOCAL_DB;
	}

	bool canConnect() {
		return true;
	}

	bool authenticate(User*, std::string username, std::string password);
	User* findUser(std::string username);

	void setUserDb(UserDb* userDb) {
		this->userDb = userDb;
	}

private:
	UserDb* userDb;
};

class AuthenticationManager {
	public:

		class SyncCron: public CronJob {
			public:
				SyncCron(AuthenticationManager* authenticationManager)
					: CronJob(60 * 60, "AUTHENTICATION_MANAGER_SYNC_CRON", true), authenticationManager(authenticationManager) {
					}
				void run();
			private:
				AuthenticationManager* authenticationManager;
		};


		AuthenticationManager(CronManager* cronManager);
		bool authenticate(std::string username, std::string password);
		AuthenticationMethod* getMethodForUser(AuthenticationType methodType);
		User* resolveUser(std::string username);

		void setLocalUserDb(UserDb* uDb) {
			this->localUserDb = uDb;
		}

		void setLocalGroupDb(GroupDb* gdb){
			this->localGroupDb = gdb;
		}	

		void setLogger(SLogger::Logger* logger){
			this->logger = logger;
		}	

		void addMethod(AuthenticationMethod* method);
		AuthenticationMethod* getMethod(AuthenticationType type);
	private:
		std::list<AuthenticationMethod*> methods;
		void mergeGroups(AuthenticationMethod* method, User* user);

		UserDb* localUserDb;
		GroupDb* localGroupDb;
		SLogger::Logger* logger;
		Lock* lock;
};

#endif
