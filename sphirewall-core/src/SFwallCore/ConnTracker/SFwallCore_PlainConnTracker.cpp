/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <sstream>
#include "SFwallCore/ConnTracker.h"
#include "Core/System.h"

void SFwallCore::ProtocolHandler::registerDeleteHook(SFwallCore::DeleteConnectionHook* handler){
	deleteHooks.push_back(handler);
}

SFwallCore::TrackerMetrics* SFwallCore::TcpProtocolHandler::metrics(){
	TrackerMetrics* metrics = new TrackerMetrics();	
	for (int x = 0; x < Firewall::PLAIN_CONN_HASH_MAX; x++) {
		std::list<Connection*>& l = conn_map[x];
		for (std::list<Connection*>::const_iterator liter = l.begin(); liter != l.end(); ++liter) {
			Connection* conn = *liter;
			if (conn) {
				metrics->firewall_conntracker_tcp_size++;
				if(!conn->hasExpired()){
					metrics->firewall_conntracker_tcp_valid++;
				}

				TcpConnection* tcpConn = (TcpConnection*) conn;
				switch(tcpConn->getState()->state){
					case SYNSYNACK:
						metrics->firewall_conntracker_tcp_synsynack++;
						break;
					case SYNACKACK:
						metrics->firewall_conntracker_tcp_synackack++;
						break;
					case TCPUP:
						metrics->firewall_conntracker_tcp_tcpup++;
						break;
					case FIN_WAIT:
						metrics->firewall_conntracker_tcp_finwait++;
						break;
					case CLOSE_WAIT:
						metrics->firewall_conntracker_tcp_closewait++;
						break;
					case LAST_ACK:
						metrics->firewall_conntracker_tcp_lastack++;
						break;
					case TIME_WAIT:
						metrics->firewall_conntracker_tcp_timewait++;
						break; 
					case FINFINACK:
						metrics->firewall_conntracker_tcp_finfinack++;
						break;
					case CLOSED:
						metrics->firewall_conntracker_tcp_closed++;
						break;
					case RESET:
						metrics->firewall_conntracker_tcp_reset++;
						break;
				}

			}
		}
	}
	return metrics;
}

void SFwallCore::TcpProtocolHandler::sample(map<string, long>& items){
	TrackerMetrics* m = metrics();
	items["firewall.conntracker.tcp.hit.success.sec"] = successPerSecond->value();
	items["firewall.conntracker.tcp.hit.fail.sec"] = failsPerSecond->value() ;
	items["firewall.conntracker.tcp.size"] = m->firewall_conntracker_tcp_size; 
	items["firewall.conntracker.tcp.valid"] = m->firewall_conntracker_tcp_valid;

	items["firewall.conntracker.tcp.synsynack"] = m->firewall_conntracker_tcp_synsynack;	
	items["firewall.conntracker.tcp.synackack"] = m->firewall_conntracker_tcp_synackack;	
	items["firewall.conntracker.tcp.tcpup"] = m->firewall_conntracker_tcp_tcpup;	
	items["firewall.conntracker.tcp.finwait"] = m->firewall_conntracker_tcp_finwait;
	items["firewall.conntracker.tcp.closewait"] = m->firewall_conntracker_tcp_closewait;	
	items["firewall.conntracker.tcp.lastack"] = m->firewall_conntracker_tcp_lastack;	
	items["firewall.conntracker.tcp.timewait"] = m->firewall_conntracker_tcp_timewait;	
	items["firewall.conntracker.tcp.finfinack"] = m->firewall_conntracker_tcp_finfinack;	
	items["firewall.conntracker.tcp.closed"] = m->firewall_conntracker_tcp_closed;	
	items["firewall.conntracker.tcp.reset"] = m->firewall_conntracker_tcp_reset;	

	items["firewall.conntracker.tcp.search.cost"] = avgIterations->value();
	items["firewall.conntracker.tcp.gb.averagetime"] = gbTime->value();
	items["firewall.conntracker.tcp.offers.sec"] = offerRequests->value();
	items["firewall.conntracker.tcp.new.sec"] = newConns->value();

	delete m;
}

void SFwallCore::UdpProtocolHandler::sample(map<string, long>& items){
	int total_size = 0;
	int total_valid = 0;

	for (int x = 0; x < Firewall::PLAIN_CONN_HASH_MAX; x++) {
		std::list<Connection*>& l = conn_map[x];
		for (std::list<Connection*>::const_iterator liter = l.begin(); liter != l.end(); ++liter) {
			Connection* conn = *liter;
			if (conn) {
				total_size++;
				if(!conn->hasExpired()){
					total_valid++;
				}
			}
		}
	}

	items["firewall.conntracker.udp.hit.success.sec"] = successPerSecond->value();
	items["firewall.conntracker.udp.hit.fail.sec"] = failsPerSecond->value() ;
	items["firewall.conntracker.udp.size"] = total_size;
	items["firewall.conntracker.udp.valid"] = total_valid;

	items["firewall.conntracker.udp.search.cost"] = avgIterations->value();
	items["firewall.conntracker.udp.gb.averagetime"] = gbTime->value();
	items["firewall.conntracker.udp.offers.sec"] = offerRequests->value();
	items["firewall.conntracker.udp.new.sec"] = newConns->value();
}

void SFwallCore::IcmpProtocolHandler::sample(map<string, long>& items){
        int total_size = 0;
        int total_valid = 0;

        for (int x = 0; x < Firewall::PLAIN_CONN_HASH_MAX; x++) {
                std::list<Connection*>& l = conn_map[x];
                for (std::list<Connection*>::const_iterator liter = l.begin(); liter != l.end(); ++liter) {
                        Connection* conn = *liter;
                        if (conn) {
                                total_size++;
                                if(!conn->hasExpired()){
                                        total_valid++;
                                }
                        }       
                }
        }

	items["firewall.conntracker.icmp.hit.success.sec"] = successPerSecond->value();
	items["firewall.conntracker.icmp.hit.fail.sec"] = failsPerSecond->value() ;
	items["firewall.conntracker.icmp.size"] = total_size;
	items["firewall.conntracker.icmp.valid"] = total_valid;

	items["firewall.conntracker.icmp.search.cost"] = avgIterations->value();
	items["firewall.conntracker.icmp.gb.averagetime"] = gbTime->value();
	items["firewall.conntracker.icmp.offers.sec"] = offerRequests->value();
	items["firewall.conntracker.icmp.new.sec"] = newConns->value();
}

void SFwallCore::PlainConnTracker::terminate(Connection* connection) {
	stringstream msg;
	msg << "ConnectionTracker terminating connection: " << connection->toString();
	logger->log(SLogger::INFO, "PlainConnTracker::terminate()", msg.str());

	connection->terminate();
}

SFwallCore::PlainConnTracker::~PlainConnTracker() {
}

SFwallCore::Connection* SFwallCore::ProtocolHandler::create(SFwallCore::Packet* packet){
	Connection* conn = Connection::parseConnection(packet);
	if (conn) {
		add(conn);
		newConns->input(1);

		if (logger->level == SLogger::INFO) {
			logger->log(SLogger::INFO, "processFilterHook()","Creating new Filter connection for: " + packet->toString());
		}
	}

	return conn;
}

SFwallCore::Connection* SFwallCore::PlainConnTracker::create(SFwallCore::Packet* packet) {
	return handlers[packet->getProtocol()]->create(packet);
}

SFwallCore::Connection* SFwallCore::ProtocolHandler::offer(SFwallCore::Packet* packet) {
	Connection* connection = findConn(packet, UdpConnection::hash(packet));
	if (connection) {
		connection->increment(packet);
	}

	offerRequests->input(1);
	return connection;
}

SFwallCore::Connection* SFwallCore::TcpProtocolHandler::offer(SFwallCore::Packet* packet){
	Connection* connection = NULL;
	connection = findConn(packet, TcpConnection::hash(packet));
	if(connection){
		((TcpConnection*)connection)->getTcpTrackable()->calcState((const TcpPacket*) packet);

	}

	if (connection) {
		connection->increment(packet);
	}

	offerRequests->input(1);
	return connection;
}

SFwallCore::Connection* SFwallCore::PlainConnTracker::offer(SFwallCore::Packet* packet) {
	return handlers[packet->getProtocol()]->offer(packet);
}

vector<SFwallCore::Connection*> SFwallCore::ProtocolHandler::listConnections() {
	vector<Connection*> ret;

	for (int x = 0; x < Firewall::PLAIN_CONN_HASH_MAX; x++) {
		std::list<Connection*>& l = conn_map[x];
		for (std::list<Connection*>::const_iterator liter = l.begin(); liter != l.end(); ++liter) {
			ret.push_back(*liter);
		}
	}

	return ret;
}

vector<SFwallCore::Connection*> SFwallCore::PlainConnTracker::listConnections() {
	vector<Connection*> ret;
	for(int x = 0; x < MAX_HANDLERS; x++){
		ProtocolHandler* target = handlers[x];
		if(target){
			vector<Connection*> toInsert = target->listConnections();
			ret.insert(ret.end(), toInsert.begin(), toInsert.end());
		}
	}	

	return ret;
}

void SFwallCore::PlainConnTracker::deleteTokenBucket(TokenBucket* bucket) {
	vector<Connection*> connVector = listConnections();
	for (int x = 0; x < connVector.size(); x++) {
		Connection* conn = connVector[x];
		if (conn->getQosBucket() == bucket) {
			conn->setQosBucket(NULL);
		}

	}
}

SFwallCore::Connection* SFwallCore::ProtocolHandler::findConn(Packet* packet, int hash){
	list<Connection*>& vec = conn_map[hash]; 
	list<Connection*>::iterator iter;

	int c = 0;
	for (iter = vec.begin(); iter != vec.end(); ++iter) {
		c++;
		Connection* conn = *iter;
		if (conn->hasExpired()) {
			if (logger->level == SLogger::DEBUG){
				logger->log(SLogger::DEBUG, "PlainConnTracker::findConn()",
						"Erasing expired connection: " + conn->toString());
			}

			deleteHook(conn);
			delete conn;
			iter = vec.erase(iter);

		} else if (conn->checkPacket(packet, Connection::DIR_ANY)) {
			if (logger->level == SLogger::DEBUG) {
				logger->log(SLogger::DEBUG, "PlainConnTracker::findConn()",
						"Found matching filter connection for packet " + packet->toString());
			}
			successPerSecond->input(1);
			avgIterations->input(c);
			return *iter;
		}
	}

	failsPerSecond->input(1);
	return NULL;
}

SFwallCore::Connection* SFwallCore::TcpProtocolHandler::findConn(Packet* packet, int hash){
	list<Connection*>& vec = conn_map[hash]; 
	list<Connection*>::iterator iter;

	int c = 0;
	for (iter = vec.begin(); iter != vec.end(); ++iter) {
		c++;
		Connection* conn = *iter;
		if (conn->hasExpired()) {
			if (logger->level == SLogger::DEBUG){
				logger->log(SLogger::DEBUG, "PlainConnTracker::findConn()",
						"Erasing expired connection: " + conn->toString());
			}

			deleteHook(conn);
			delete conn;
			iter = vec.erase(iter);

		} else if (conn->checkPacket(packet, Connection::DIR_ANY)) {
			if (logger->level == SLogger::DEBUG) {
				logger->log(SLogger::DEBUG, "PlainConnTracker::findConn()",
						"Found matching filter connection for packet " + packet->toString());
			}
			successPerSecond->input(1);
			avgIterations->input(c);
			return conn;
		}
	}

	failsPerSecond->input(1);
	return NULL;
}


SFwallCore::Connection* SFwallCore::PlainConnTracker::findConn(Packet* packet, int hash) {
	return handlers[packet->getProtocol()]->findConn(packet, hash);
}

void SFwallCore::ProtocolHandler::add(Connection* conn) {
	conn_map[conn->hash()].push_back(conn);
}

void SFwallCore::PlainConnTracker::add(Connection* conn) {
	handlers[conn->getProtocol()]->add(conn);
}

void SFwallCore::ProtocolHandler::deleteHook(Connection* connection){
	//Ignore other traffic
	if(connection->getProtocol() == TCP || connection->getProtocol() == UDP){
		if(firewall->BANDWIDTHDB_ENABLED == 1){
			for(int x= 0; x < deleteHooks.size(); x++){
				DeleteConnectionHook* hook = deleteHooks[x];
				hook->hook(connection);
			}
		}
	}
}

void SFwallCore::PlainConnTracker::erase(Connection* conn) {
	handlers[conn->getProtocol()]->erase(conn);
}

void SFwallCore::ProtocolHandler::erase(Connection* conn) {
	list<Connection*>::iterator item;
	list<Connection*>& vec = conn_map[conn->hash()]; 
	item = find(vec.begin(), vec.end(), conn);
	if (item != vec.end()) {
		vec.erase(item);

		if (logger->level == SLogger::INFO) {
			logger->log(SLogger::INFO, "PlainConnTracker::erase()",
					"Erasing connection " + conn->toString());
		}
	}
}

int SFwallCore::ProtocolHandler::cleanup(){
	int count = 0;
	for (int x = 0; x < Firewall::PLAIN_CONN_HASH_MAX; x++) {
		list<Connection*>& vec = conn_map[x];
		for (list<Connection*>::iterator liter = vec.begin(); liter != vec.end(); liter++) {
			Connection* conn = *liter;
			if (conn->hasExpired()) {
				deleteHook(conn);
				liter = vec.erase(liter);
				if (logger->level == SLogger::INFO) {
					logger->log(SLogger::INFO, "PlainConnTracker::erase()", "Erasing connection " + conn->toString());
				}
				count++;
				delete conn;
			}
		}
	}
	return count;
}

int SFwallCore::TcpProtocolHandler::cleanup(){
	int count = 0;
	for (int x = 0; x < Firewall::PLAIN_CONN_HASH_MAX; x++) {
		list<Connection*>& vec = conn_map[x];
		for (list<Connection*>::iterator liter = vec.begin(); liter != vec.end(); liter++) {
			Connection* conn = *liter;
			if (conn->hasExpired()) {
				deleteHook(conn);
				liter = vec.erase(liter);
				if (logger->level == SLogger::INFO) {
					logger->log(SLogger::INFO, "PlainConnTracker::erase()", "Erasing connection " + conn->toString());
				}
				count++;
				delete conn;
			}
		}
	}

	return count;
}

int SFwallCore::PlainConnTracker::cleanup() {
	int count = 0;
	lock.lock();
	Timer* timer = new Timer();
	timer->start();

	for(int x = 0; x < MAX_HANDLERS;x++){
		ProtocolHandler* handler = handlers[x];
		if(handler){
			count += handler->cleanup();
		}
	}

	timer->stop();
	delete timer;
	lock.unlock();
	return count;
}

std::list<SFwallCore::Connection*> SFwallCore::PlainConnTracker::listConnections(ConnectionCriteria criteria) {
	list<Connection*> ret;

	vector<Connection*> allConnections = listConnections();
	for(vector<Connection*>::iterator iter = allConnections.begin();
			iter != allConnections.end();
			iter++){

		Connection* connection = (*iter);	
		if(criteria.match(connection)){
			ret.push_back(connection);
		}			
	}		

	return ret;
}

bool SFwallCore::ConnectionCriteria::match(SFwallCore::Connection* connection) {
	if (session != NULL) {
		if (connection->getSession() != session) {
			return false;
		}
	}

	if (connection->getProtocol() == TCP || connection->getProtocol() == UDP) {
		if (destPort != -1) {
			if (connection->getDestinationPort() != destPort) {
				return false;
			}
		}

		if (sourcePort != -1) {
			if (connection->getSourcePort() != sourcePort) {
				return false;
			}
		}
	}

	if (type != NDEF && type != connection->getProtocol()) {
		return false;
	}

	if (dest != -1) {
		if (connection->getDstIp() != dest) {
			return false;
		}
	}

	if (source != -1) {
		if (connection->getSrcIp() != source) {
			return false;
		}
	}

	return true;
}

