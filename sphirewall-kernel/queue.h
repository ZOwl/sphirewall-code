/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QUEUE_H
#define QUEUE_H

#include "nbqueue.h"

#define DEFAULT_ACTION NF_ACCEPT
#define NETLINK_TEST 19

#define VFW_GROUP 1

#ifdef DEBUG
#define PDEBUG(args...) printk ("sphirewall_queue: " args)
#else
#define PDEBUG(args...)
#endif

#define PMESSAGE(args...) printk("sphirewall_queue: " args)

/* Hash map used to store nf_queue_entries based on id, that are waiting on a response from the client */
extern int entry_map_add(struct nf_queue_entry *entry);
extern struct nf_queue_entry *entry_map_get(int x);

/* Processing queue*/
extern void send_queue_init(void);
extern int send_queue_empty(void);
extern void send_queue_push_entry(struct nf_queue_entry *entry);
extern struct nf_queue_entry *send_queue_pop_entry(void);

/* Processing workers */
extern void squeue_incoming_worker(void*);
extern int squeue_outgoing_worker(void* data);
extern int squeue_outgoing_push(struct nf_queue_entry *entry, unsigned int queuenum);

extern unsigned int hook_func( unsigned int hooknum,
                        struct sk_buff *pskb,
                        const struct net_device *in,
                        const struct net_device *out,
                        int (*okfn)(struct sk_buff *));

/* Setup and tear down the queues and hooks*/
extern void register_hooks(void);
extern void unregister_hooks(void);

//extern struct meta* send_nbqueue;
//extern struct meta* recv_nbqueue;

extern int open_shm(void);
extern void close_shm(void);

extern wait_queue_head_t recv_wait_lock;
static DECLARE_WAIT_QUEUE_HEAD(send_wait_lock);

#endif
