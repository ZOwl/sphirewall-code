#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester

class SmtpTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester();
        self.tester.connect();
        self.tester.login("fw", "admin", "admin")

    def setField(self, n, value):
        name = self.tester.driver.find_element_by_name(n)
        name.clear()
        name.send_keys(value)

    def test_SmtpSettings(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Configure')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Smtp Server')]").click()

        self.setField("hostname", "localhost")
        self.setField("port", "25")
        self.setField("username", "michael@sphirewall.net")
        self.setField("password", "12345")
        self.setField("default", "root@sphirewall.net")
        if self.tester.driver.find_element_by_name("tls").get_attribute("checked"):
            self.tester.driver.find_element_by_name("tls").click()

        self.tester.driver.find_element_by_name("tls").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertEquals(self.tester.driver.find_element_by_name("hostname").get_attribute("value"), "localhost")
        self.assertEquals(self.tester.driver.find_element_by_name("port").get_attribute("value"), "25")
        self.assertEquals(self.tester.driver.find_element_by_name("username").get_attribute("value"), "michael@sphirewall.net")
        self.assertEquals(self.tester.driver.find_element_by_name("password").get_attribute("value"), "12345")
        self.assertTrue(self.tester.driver.find_element_by_name("tls").get_attribute("checked"))

    def tearDown(self):
        self.tester.disconnect()
