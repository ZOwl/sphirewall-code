/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REQUEST_HANDLER_H
#define REQUEST_HANDLER_H

class JSONObject;
class BandwidthDb;
#include "Core/Logger.h"
class LockHeldException: public std::exception {
public:

	LockHeldException() {
	}

	~LockHeldException() throw () {
	}

	virtual const char* what() const throw () {
		return w.c_str();
	}

	std::string message() {
		return w;
	}

private:
	std::string w;
};


class DelegateGeneralException : public std::exception {

public:

	DelegateGeneralException(std::string message) {
		w = message;
	}

	~DelegateGeneralException() throw () {
	}

	virtual const char* what() const throw () {
		return w.c_str();
	}

	std::string message() {
		return w;
	}

private:
	std::string w;
};

class DelegateNotFoundException : public std::exception {
public:

	DelegateNotFoundException(std::string message) {
		w = message;
	}

	~DelegateNotFoundException() throw () {
	}

	virtual const char* what() const throw () {
		return w.c_str();
	}

	std::string message() {
		return w;
	}

private:
	std::string w;
};

class RequestHandler {
public:
	RequestHandler(){
		setLogger(NULL);
	}

	std::string handle(std::string input);
	JSONObject process(std::string request, JSONObject args);

	void setBandwidthDb(BandwidthDb* db) {
		this->bandwidthDb = db;
	}

	void setLogger(SLogger::LogContext* logger){
		this->logger = logger;
	}
private:
	BandwidthDb* bandwidthDb;
	double convertMetric(std::string metric, long long input);
	void lock(std::string);
	void unlock(std::string);
	SLogger::LogContext* logger;
};

#endif
