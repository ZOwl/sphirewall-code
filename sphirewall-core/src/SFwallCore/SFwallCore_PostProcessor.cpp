/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "PostProcessor.h"
#include "Core/System.h"
#include "SFwallCore/Firewall.h"

void SFwallCore::PostProcessor::registerHandler(Handler* handler) {
	logger->log(SLogger::INFO, "PostProcessor::registerHandler()", "Registering post processing handler ");
	handlers.push_back(handler);
}

void SFwallCore::PostProcessor::add(Packet* packet) {
	Packet* copy = NULL;
	if (packet->getProtocol() == TCP) {
		copy = new TcpPacket((const TcpPacket&) *packet);
	} else if (packet->getProtocol() == UDP) {
		copy = new UdpPacket((const UdpPacket&) *packet);
	}

	if (copy){
	        copy->setInternalSqp(Packet::cloneSqp(packet->getInternalSqp()));

		lock->lock();
		q.push(copy);
		lock->unlock();
	}
}

void SFwallCore::PostProcessor::roll() {
	while(true){
		if(firewall && firewall->POST_PROCESSOR_ENABLED == 0){
			sleep(10);
			continue;
		}	

		while (true) {
			lock->lock();
			if(q.empty()){
				lock->unlock();
				break;
			}

			Packet* packet = q.front();
			q.pop();
			lock->unlock();

			for (list<Handler*>::iterator iter = handlers.begin(); iter != handlers.end(); ++iter) {
				(*iter)->handle(packet);
			}

			free(packet->getInternalSqp()->raw_packet);
			free(packet->getInternalSqp());
			delete packet;
		}
		usleep(100);
	}
}

void SFwallCore::PostProcessor::start() {
	logger->log(SLogger::EVENT, "PostProcessor::start()", "Starting postprocessor");

	new boost::thread(boost::bind(&SFwallCore::PostProcessor::roll, this));
}
