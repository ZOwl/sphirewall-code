/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;

#include "Json/JSON.h"
#include "Api/JsonManagementService.h"
#include "Utils/NetDevice.h"
#include "Core/HostDiscoveryService.h"
#include "Core/Dhcp3Configuration.h"
#include "Core/ConnectionManager.h"
#include "Core/ConnectionManager.h"
#include "Core/Openvpn.h"
#include "Core/System.h"
#include "Core/Wireless.h"
#include "Utils/DynDns.h"
#include "Core/Event.h"

JSONObject NetworkSettingsDelegate::process(std::string uri, JSONObject obj) {
	if (uri.compare("network/devices/list") == 0) {
		return devices_list(obj);
	} else if (uri.compare("network/devices/set") == 0) {
		return devices_set(obj);
	} else if (uri.compare("network/devices/delete") == 0) {
		return devices_delete(obj);
	} else if (uri.compare("network/dns/get") == 0) {
		return dns_get(obj);
	} else if (uri.compare("network/dns/set") == 0) {
		return dns_set(obj);
	} else if (uri.compare("network/routes/list") == 0) {
		return routes_list(obj);
	} else if (uri.compare("network/routes/add") == 0) {
		return routes_add(obj);
	} else if (uri.compare("network/routes/del") == 0) {
		return routes_del(obj);
	} else if (uri.compare("network/arp/list") == 0) {
		return arp_list(obj);
	} else if (uri.compare("network/arp/size") == 0) {
		return arp_size();
	} else if (uri.compare("network/dhcp/globals/get") == 0) {
		return dhcp_globals(obj);
	} else if (uri.compare("network/dhcp/globals/set") == 0) {
		return dhcp_globals_set(obj);
	} else if (uri.compare("network/dhcp/running") == 0) {
		return dhcp_running(obj);
	} else if (uri.compare("network/dhcp/static/add") == 0) {
		return dhcp_static_add(obj);
	} else if (uri.compare("network/dhcp/static/del") == 0) {
		return dhcp_static_del(obj);
	} else if (uri.compare("network/dhcp/static") == 0) {
		return dhcp_static(obj);
	} else if (uri.compare("network/devices/alias/list") == 0) {
		return devices_alias_list(obj);
	} else if (uri.compare("network/devices/alias/delete") == 0) {
		return devices_alias_delete(obj);
	} else if (uri.compare("network/devices/alias/add") == 0) {
		return devices_alias_add(obj);
	} else if (uri.compare("network/connections/list") == 0) {
		return connections_list(obj);
	} else if (uri.compare("network/connections/add") == 0) {
		return connectons_add(obj);
	} else if (uri.compare("network/connections/del") == 0) {
		return connections_del(obj);
	} else if (uri.compare("network/connections/save") == 0) {
		return connections_save(obj);
	} else if (uri.compare("network/connections/connect") == 0) {
		return connections_connect(obj);
	} else if (uri.compare("network/connections/disconnect") == 0) {
		return connections_disconnect(obj);
	} else if (uri.compare("network/devices/up") == 0) {
		return devices_up(obj);
	} else if (uri.compare("network/devices/toggle") == 0) {
		return devices_toggle(obj);
	} else if(uri.compare("network/devices/publish") == 0){
		return devices_publish(obj);
	} else if(uri.compare("network/devices/toggledhcp") == 0){
		return devices_toggledhcp(obj);
	} else if(uri.compare("network/openvpn/start") == 0){
		return openvpn_start(obj);		
	} else if(uri.compare("network/openvpn/stop") == 0){
		return openvpn_stop(obj);		
	} else if(uri.compare("network/openvpn/publish") == 0){
		return openvpn_publish(obj);		
	} else if(uri.compare("network/openvpn/status") == 0){
		return openvpn_status(obj);		
	} else if(uri.compare("network/openvpn/log") == 0){
		return openvpn_log(obj);		
	} else if(uri.compare("network/openvpn/client/add") == 0){
		return openvpn_clients_add(obj);
	} else if(uri.compare("network/openvpn/client/delete") == 0){
		return openvpn_clients_delete(obj);
	} else if(uri.compare("network/openvpn/client/list") == 0){
		return openvpn_clients_list(obj);
	} else if(uri.compare("network/openvpn/instances/list") == 0){
		return openvpn_instances_list(obj);
	} else if(uri.compare("network/openvpn/instances/create") == 0){
		return openvpn_instance_create(obj);
	} else if(uri.compare("network/openvpn/instances/remove") == 0){
		return openvpn_instance_remove(obj);
	} else if(uri.compare("network/openvpn/setoption") == 0){
		return openvpn_instance_setoption(obj);
	} else if(uri.compare("network/openvpn/daemonexists") == 0){
		return openvpn_daemonexists(obj);
	} else if(uri.compare("network/dhcp/log") == 0){
		return dhcp_log(obj);
	}else if(uri.compare("network/devices/synced") == 0){
		return devices_synced(obj);
	}else if(uri.compare("network/wireless/start") == 0){
		return wireless_start(obj);
	}else if(uri.compare("network/wireless/stop") == 0){
		return wireless_stop(obj);
	}else if(uri.compare("network/wireless/online") == 0){
		return wireless_online(obj);
	}else if(uri.compare("network/dyndns") == 0){
		return dyndns();
	}else if (uri.compare("network/dyndns/set") == 0){
		return dyndns_set(obj);
	} else {
		throw new DelegateNotFoundException(uri);
	}
}

JSONObject NetworkSettingsDelegate::wireless_online(JSONObject obj){
	JSONObject ret;
	ret.put(L"status", new JSONValue((bool)System::getInstance()->getWirelessConfiguration()->online()));
	return ret;
}

JSONObject NetworkSettingsDelegate::wireless_start(JSONObject obj){
	System::getInstance()->getWirelessConfiguration()->start();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::wireless_stop(JSONObject obj){
	System::getInstance()->getWirelessConfiguration()->stop();
	return JSONObject();
}

void NetworkSettingsDelegate::devices_serialize_configured(JSONObject& interface, NetDeviceConfiguration* configuredDevice){
	interface.put(L"interface", new JSONValue((string) configuredDevice->interface));
	interface.put(L"dhcp", new JSONValue((bool) configuredDevice->dhcp));
	interface.put(L"name", new JSONValue((string) configuredDevice->name));
	interface.put(L"gateway", new JSONValue((string) configuredDevice->gateway));
	interface.put(L"configured", new JSONValue((bool) true));
	interface.put(L"bridge", new JSONValue((bool) configuredDevice->bridge));

	//We must override the settings if another ip is set:
	if(!configuredDevice->dhcp && configuredDevice->ip.size() > 0){
		interface.put(L"ip", new JSONValue((string) configuredDevice->ip));
		interface.put(L"mask", new JSONValue((string) configuredDevice->mask));
	}

	if(configuredDevice->bridge){
		JSONArray barr;
		for(list<string>::iterator biter = configuredDevice->bridgeDevices.begin();
				biter != configuredDevice->bridgeDevices.end();
				biter++){

			barr.push_back(new JSONValue((string) (*biter)));
		}

		interface.put(L"bridgeDevices", new JSONValue(barr));
	}

	string aliases;
	for(list<string>::iterator iter = configuredDevice->aliases.begin();
		iter != configuredDevice->aliases.end();
		iter++){
		aliases += (*iter);
		aliases += " ";
	}

	interface.put(L"aliases", new JSONValue((string) aliases));
}

JSONObject NetworkSettingsDelegate::devices_list(JSONObject obj) {
	JSONObject ret;
	JSONArray devices;
	list<NetDevice*> availableDevices = interfaceManager->listDevices();
	list<NetDevice*>::iterator iter;	
	for (iter = availableDevices.begin();
			iter != availableDevices.end();
			iter++) {

		JSONObject interface;
		NetDevice* d = (*iter); 
		if (d != NULL) {
			interface.put(L"interface", new JSONValue((string) d->getInterface()));
			interface.put(L"ip", new JSONValue((string) d->getIp()));
			interface.put(L"mask", new JSONValue((string) d->getNetMask()));
			interface.put(L"broadcast", new JSONValue((string) d->getBroadCast()));
			interface.put(L"mac", new JSONValue((string) d->getMacAddress()));
			interface.put(L"network", new JSONValue((string) d->getNetworkAddress()));
			interface.put(L"state", new JSONValue((bool) d->isUp()));
			interface.put(L"type", new JSONValue((double) d->getType()));
			interface.put(L"ptp", new JSONValue((string) d->getPtpAddress()));
			interface.put(L"link", new JSONValue((string) d->getLinkLayerAddress()));
			interface.put(L"mtu", new JSONValue((double) d->getMtu()));
			interface.put(L"id", new JSONValue((double) d->getId()));

			//If we are a system interface, not yet managed by sphirewall we might have aliases:
			list<NetDeviceAlias> aliases = d->getAliases();
			list<NetDeviceAlias>::iterator iter;
			
			std::string aliasString;
			for (iter = aliases.begin(); iter != aliases.end(); iter++) {
				NetDeviceAlias a = (*iter);
				aliasString += a.ip;
				aliasString += " ";
			}

			interface.put(L"aliases", new JSONValue((string) aliasString));

			//Configuration Items:
			NetDeviceConfiguration* configuredDevice = interfaceManager->getConfiguredDevice(d->getInterface());
			if(configuredDevice){
				devices_serialize_configured(interface, configuredDevice);
			}
		}else{
			continue;
		}

		devices.push_back(new JSONValue(interface));
	}

	list<NetDeviceConfiguration*> configuredDevices = interfaceManager->getConfiguredDevices();
	for(list<NetDeviceConfiguration*>::iterator iter = configuredDevices.begin();
			iter != configuredDevices.end();
			iter++){
		NetDeviceConfiguration* device = (*iter);
		if(interfaceManager->get(device->interface) == NULL){
			JSONObject interface;
			devices_serialize_configured(interface, device);
			devices.push_back(new JSONValue(interface));	
		}
	}

	ret.put(L"devices", new JSONValue(devices));
	return ret;
}

JSONObject NetworkSettingsDelegate::devices_toggledhcp(JSONObject obj){
	JSONObject interface;
	NetDevice* d = interfaceManager->get((string) obj[L"interface"]->String());
	if (d != NULL) {
		d->toggleDhcp(obj[L"dhcp"]->AsBool());
		interfaceManager->save();
	}	
	return JSONObject(); 
}

JSONObject NetworkSettingsDelegate::devices_synced(JSONObject obj){
	JSONObject ret;
	ret.put(L"synced", new JSONValue((bool)interfaceManager->isSynced()));
	return ret;
}

JSONObject NetworkSettingsDelegate::devices_publish(JSONObject obj){
	interfaceManager->publish();
	return JSONObject();
}


JSONObject NetworkSettingsDelegate::devices_up(JSONObject obj) { JSONObject ret;
	NetDevice* d = interfaceManager->get(obj[L"interface"]->String());
	if (d != NULL) {
		ret.put(L"state", new JSONValue((bool) d->isUp()));
	}else{
		throw new DelegateGeneralException("could not find interface");
	}

	return ret;
}

JSONObject NetworkSettingsDelegate::devices_toggle(JSONObject obj) {
	NetDevice* d = interfaceManager->get(obj[L"interface"]->String());
	if (d != NULL) {
		d->toggleUp();
		interfaceManager->save();

		if(eventDb){
			EventParams params;
			params["interface"] = obj[L"interface"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_DEVICES_TOGGLE, params));
		}

	}else{
		throw new DelegateGeneralException("could not find interface");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_set(JSONObject obj) {
	if (obj.has(L"interface") && obj.has(L"ip") && obj.has(L"mask")) {
		//blank interface?
		if(obj[L"interface"]->String().size() == 0){
			return JSONObject();
		}

		JSONObject interface;
		NetDeviceConfiguration* d = interfaceManager->getConfiguredDevice(obj[L"interface"]->String());
		if(!d){
			d = interfaceManager->addDevice(obj[L"interface"]->String());
		}

		d->ip = obj[L"ip"]->String();
		d->mask = obj[L"mask"]->String();

		if(obj.has(L"bridge")){
			d->bridge = obj[L"bridge"]->AsBool();
		}

		if(obj.has(L"dhcp")){
			d->dhcp = obj[L"dhcp"]->AsBool();
		}

		if(obj.has(L"name")){
			d->name = obj[L"name"]->String();
		}

		if(obj.has(L"gateway")){
			d->gateway = obj[L"gateway"]->String();
		}

		if(obj.has(L"aliases")){
			std::string raw = obj[L"aliases"]->String();
			vector<string> aliases;
			split(raw, ' ', aliases);

			d->aliases.clear();
			for(int x =0; x < aliases.size(); x++){
				d->aliases.push_back(aliases[x]);
			}
		}

		if(eventDb){    
			EventParams params;
			params["interface"] = obj[L"interface"]->String();
			params["ip"] = obj[L"ip"]->String();
			params["mask"] = obj[L"mask"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_DEVICES_SETIP, params));
		}

		if(d->bridge && obj.has(L"bridgeDevices")){
			d->bridgeDevices.clear();

			JSONArray barr = obj[L"bridgeDevices"]->AsArray();
			for(int x= 0; x < barr.size(); x++){
				d->bridgeDevices.push_back(barr[x]->String());
			} 
		}
		interfaceManager->save();
		return JSONObject();
	}

	throw new DelegateGeneralException("could not find interface");
}

JSONObject NetworkSettingsDelegate::dns_get(JSONObject) {
	JSONObject ret;
	ret.put(L"ns1", new JSONValue((string) dnsConfig->getNS1()));
	ret.put(L"ns2", new JSONValue((string) dnsConfig->getNS2()));
	ret.put(L"domain", new JSONValue((string) dnsConfig->getDomain()));
	ret.put(L"search", new JSONValue((string) dnsConfig->getSearch()));

	return ret;
}

JSONObject NetworkSettingsDelegate::dns_set(JSONObject obj) {
	dnsConfig->setNS1(obj[L"ns1"]->String());
	dnsConfig->setNS2(obj[L"ns2"]->String());
	dnsConfig->setSearch(obj[L"search"]->String());
	dnsConfig->setDomain(obj[L"domain"]->String());

	dnsConfig->save();

	if(eventDb){
		EventParams params;
		eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_DNS_SET, params));
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::routes_list(JSONObject) {
	JSONObject ret;
	JSONArray routes;

	list<Route*> routesSource = interfaceManager->listRoutes();
	list<Route*>::iterator iter;
	for (iter = routesSource.begin(); iter != routesSource.end(); iter++) {
		Route* route = (*iter);
		JSONObject r;

		r.put(L"dest", new JSONValue((string) route->dest));
		r.put(L"gw", new JSONValue((string) route->gw));
		r.put(L"id", new JSONValue((string) route->id));
		r.put(L"device", new JSONValue((string) route->dev));

		routes.push_back(new JSONValue(r));
	}

	ret.put(L"routes", new JSONValue(routes));
	return ret;
}

JSONObject NetworkSettingsDelegate::routes_add(JSONObject args) {
	Route* route = new Route();
	route->dest = args[L"dest"]->String();
	route->gw = args[L"gw"]->String();
	route->id = StringUtils::genRandom(); 
	route->dev = args[L"device"]->String();

	interfaceManager->addRoute(route);
	if(eventDb){
		EventParams params;
		params["dest"] = args[L"dest"]->String();
		params["gw"] = args[L"gw"]->String();

		eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_ROUTES_ADD, params));
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::routes_del(JSONObject args) {
	Route* route = interfaceManager->getRoute(args[L"id"]->String());
	if(route){
		interfaceManager->deleteRoute(route);

		if(eventDb){
			EventParams params;
			params["id"] = args[L"id"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_ROUTES_DEL, params));
		}
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::arp_size(){
	JSONObject ret;
	int size = arp->size();	
	ret.put(L"size", new JSONValue((double) size));
	return ret;
}

JSONObject NetworkSettingsDelegate::arp_list(JSONObject args) {
	JSONObject ret;
	JSONArray hosts;

	arp->holdLock();
	std::list<Host*> a;
	arp->list(a);
	for (list<Host*>::const_iterator iter = a.begin();
			iter != a.end();
			++iter) {
		JSONObject obj;

		Host* entry = *iter;
		obj.put(L"host", new JSONValue((string) IP4Addr::ip4AddrToString(entry->ip)));
		obj.put(L"hw", new JSONValue((string) entry->mac));
		obj.put(L"loginTime", new JSONValue((double) entry->firstSeen));

		if(args.has(L"resolve") && args[L"resolve"]->AsBool() == true){
			obj.put(L"hostname", new JSONValue((string) entry->hostname()));
		}

		hosts.push_back(new JSONValue(obj));
	}
	arp->releaseLock();

	ret.put(L"hosts", new JSONValue(hosts));
	return ret;
}

JSONObject NetworkSettingsDelegate::dhcp_globals(JSONObject o) {
	string interface = o[L"interface"]->String();
	Dhcp3ServiceInstance* instance = dhcpConfiguration->getOrCreate(interface);

	JSONObject ret;
	ret.put(L"startIpAddress", new JSONValue((string) instance->startIpAddress));
	ret.put(L"finishIpAddress", new JSONValue((string) instance->finishIpAddress));
	ret.put(L"defaultRouter", new JSONValue((string) instance->defaultRouter));
	ret.put(L"domainNameService", new JSONValue((string) instance->domainNameService));
	ret.put(L"domainName", new JSONValue((string) instance->domainName));
	ret.put(L"enabled", new JSONValue((bool) instance->enabled));

	return ret;
}

JSONObject NetworkSettingsDelegate::dhcp_globals_set(JSONObject o) {
	string interface = o[L"interface"]->String();
	Dhcp3ServiceInstance* instance = dhcpConfiguration->getOrCreate(interface);

	instance->startIpAddress = o[L"startIpAddress"]->String();
	instance->finishIpAddress = o[L"finishIpAddress"]->String();
	instance->defaultRouter = o[L"defaultRouter"]->String();
	instance->domainNameService = o[L"domainNameService"]->String();
	instance->domainName = o[L"domainName"]->String();
	instance->enabled = o[L"enabled"]->AsBool();

	dhcpConfiguration->publishConfigurationToServer();
	if(eventDb){            
		EventParams params;
		eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_DHCP_SET, params));
	}               

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::dhcp_publish(JSONObject o) {
	dhcpConfiguration->publishConfigurationToServer();

	if(eventDb){            
		EventParams params; 
		eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_DHCP_PUBLISH, params));
	}               

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::dhcp_start(JSONObject o) {
	dhcpConfiguration->start();

	if(eventDb){
		EventParams params;
		eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_DHCP_START, params));
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::dhcp_stop(JSONObject o) {
	dhcpConfiguration->stop();

	if(eventDb){
		EventParams params;
		eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_DHCP_STOP, params));
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::dhcp_running(JSONObject) {
	JSONObject ret;
	ret.put(L"running", new JSONValue((bool) dhcpConfiguration->running()));
	return ret;
}

JSONObject NetworkSettingsDelegate::dhcp_static_add(JSONObject o) {
	string interface = o[L"interface"]->String();
	Dhcp3ServiceInstance* instance = dhcpConfiguration->getOrCreate(interface);

	instance->addStaticLease(o[L"hostname"]->String(), o[L"ip"]->String(), o[L"hw"]->String());
	dhcpConfiguration->save();
	dhcpConfiguration->publishConfigurationToServer();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::dhcp_static_del(JSONObject o) {
	string interface = o[L"interface"]->String();
	Dhcp3ServiceInstance* instance = dhcpConfiguration->getOrCreate(interface);

	instance->deleteStaticLease(o[L"hostname"]->String(), o[L"ip"]->String(), o[L"hw"]->String());
	dhcpConfiguration->publishConfigurationToServer();

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::dhcp_static(JSONObject o) {
	string interface = o[L"interface"]->String();
	Dhcp3ServiceInstance* instance = dhcpConfiguration->getOrCreate(interface);

	JSONObject ret;
	JSONArray arr;
	list<StaticLease*>::iterator iter;
	for (iter = instance->staticLeases.begin();
			iter != instance->staticLeases.end();
			iter++) {
		StaticLease* lease = (*iter);

		JSONObject i;
		i.put(L"hostname", new JSONValue((string) lease->hostname));
		i.put(L"ip", new JSONValue((string) lease->ip));
		i.put(L"hw", new JSONValue((string) lease->hw));

		arr.push_back(new JSONValue(i));
	}

	ret.put(L"items", new JSONValue(arr));
	return ret;
}

JSONObject NetworkSettingsDelegate::devices_alias_list(JSONObject obj) {
	JSONObject ret;
	JSONArray arr;

	NetDevice* device = interfaceManager->get(obj[L"interface"]->String());
	if (device != NULL) {

		list<NetDeviceAlias> aliases = device->getAliases();
		list<NetDeviceAlias>::iterator iter;
		for (iter = aliases.begin(); iter != aliases.end(); iter++) {
			NetDeviceAlias a = (*iter);

			JSONObject o;
			o.put(L"ip", new JSONValue((string) a.ip));
			arr.push_back(new JSONValue(o));
		}
	}else{
		throw new DelegateGeneralException("could not find interface");
	}

	ret.put(L"items", new JSONValue(arr));
	return ret;
}

JSONObject NetworkSettingsDelegate::devices_alias_delete(JSONObject obj) {
	NetDeviceAlias alias;
	alias.ip = obj[L"ip"]->String();

	NetDevice* device = interfaceManager->get(obj[L"interface"]->String());
	if(device){
		device->deleteAlias(alias);
	}else{
		throw new DelegateGeneralException("could not find interface");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_alias_add(JSONObject obj) {
	NetDeviceAlias alias;
	alias.ip = obj[L"ip"]->String();

	NetDevice* device = interfaceManager->get(obj[L"interface"]->String());
	if(device){
		device->addAlias(alias);
	}else{
		throw new DelegateGeneralException("could not find interface");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_list(JSONObject obj) {
	JSONObject ret;
	JSONArray arr;

	list<Connection*> connections = connectionManager->getConnections();
	list<Connection*>::iterator iter;
	for (iter = connections.begin();
			iter != connections.end();
			iter++) {

		Connection* target = (*iter);
		JSONObject o;
		o.put(L"name", new JSONValue((string) target->getName()));			
		o.put(L"device", new JSONValue((string) target->getDevice()));
		o.put(L"username", new JSONValue((string) target->getAuthenticationCredentials().username));
		o.put(L"password", new JSONValue((string) target->getAuthenticationCredentials().password));

		o.put(L"authenticationType", new JSONValue((double) target->getAuthenticationCredentials().authenticationType));
		o.put(L"type", new JSONValue((double) target->type()));
		o.put(L"state", new JSONValue((bool) target->isConnected()));
		o.put(L"keyfile", new JSONValue((string) target->getAuthenticationCredentials().keyfile));
		o.put(L"log", new JSONValue((string) target->log()));
		o.put(L"connected", new JSONValue((bool) target->isConnected()));
		arr.push_back(new JSONValue(o));
	}

	ret.put(L"items", new JSONValue(arr));
	return ret;
}

JSONObject NetworkSettingsDelegate::connectons_add(JSONObject obj) {
	connectionManager->createConnection(obj[L"name"]->String(), (ConnectionType) obj[L"type"]->AsNumber());
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_del(JSONObject obj) {
	Connection* target = connectionManager->getConnection(obj[L"name"]->String());
	if (target) {
		connectionManager->deleteConnection(target);
	}else{
		throw new DelegateGeneralException("could not find connection");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_save(JSONObject obj) {
	Connection* connection = connectionManager->getConnection(obj[L"name"]->String());

	if (connection) {
		//Set the details:
		AuthenticationCredentials& auth = connection->getAuthenticationCredentials();
		if(connection->type() == PPPoE){
			connection->setDevice(obj[L"device"]->String());
			auth.username = obj[L"username"]->String();
			auth.password = obj[L"password"]->String();
			auth.authenticationType = (ConnectionAuthenticationType) obj[L"authenticationType"]->AsNumber();
		}else{
			auth.keyfile = obj[L"keyfile"]->String();
		}

		connectionManager->save();

		connectionManager->publishAuthenticationCredentials();
		connection->publishSettings();
	}else{
		throw new DelegateGeneralException("could not find connection");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_connect(JSONObject o) {
	Connection* connection = connectionManager->getConnection(o[L"name"]->String());
	if (connection) {
		connection->connect();
		if(eventDb){
			EventParams params;
			params["name"] = o[L"name"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_CONNECTION_START, params));
		}
	}else{
		throw new DelegateGeneralException("could not find connection");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_disconnect(JSONObject o) {
	Connection* connection = connectionManager->getConnection(o[L"name"]->String());
	if (connection) {
		connection->disconnect();
		if(eventDb){
			EventParams params; 
			params["name"] = o[L"name"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_CONNECTION_STOP, params));
		}       
	}else {
		throw new DelegateGeneralException("could not find connection");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::openvpn_start(JSONObject obj){
	Openvpn* instance = openvpn->getInstance(obj[L"name"]->String());
	if(instance){
		instance->start();
	}

	openvpn->save();
	return JSONObject();	
}

JSONObject NetworkSettingsDelegate::openvpn_stop(JSONObject obj){
	Openvpn* instance = openvpn->getInstance(obj[L"name"]->String());
	if(instance){
		instance->stop();
	}
	openvpn->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::openvpn_publish(JSONObject obj){
	Openvpn* instance = openvpn->getInstance(obj[L"name"]->String());
	if(instance){
		instance->publish();
	}
	openvpn->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::openvpn_status(JSONObject obj){
	Openvpn* instance = openvpn->getInstance(obj[L"name"]->String());
	if(instance){
		JSONObject ret;
		ret.put(L"status", new JSONValue((bool) instance->status()));
		return ret;
	}
	openvpn->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::openvpn_log(JSONObject obj){
	JSONObject ret;
	Openvpn* instance = openvpn->getInstance(obj[L"name"]->String());
	if(instance){
		ret.put(L"log", new JSONValue((string) instance->log()));
	}
	openvpn->save();
	return ret;
}

JSONObject NetworkSettingsDelegate::openvpn_clients_list(JSONObject obj){
	Openvpn* instance = openvpn->getInstance(obj[L"name"]->String());
	JSONObject ret;
	if(instance){
		list<OpenvpnClientConf*> confs = instance->getClients();
		JSONArray arr;
		for(list<OpenvpnClientConf*>::iterator iter = confs.begin();
				iter != confs.end();
				iter++){

			JSONObject obj;
			obj.put(L"name", new JSONValue((string) (*iter)->name));
			obj.put(L"conf", new JSONValue((string) instance->generateUserFile((*iter)->name)));

			arr.push_back(new JSONValue(obj));		
		}

		ret.put(L"clients", new JSONValue(arr));
	}
	openvpn->save();
	return ret;
}

JSONObject NetworkSettingsDelegate::openvpn_clients_add(JSONObject obj){
	Openvpn* instance = openvpn->getInstance(obj[L"name"]->String());
	if(instance){
		instance->createClient(obj[L"clientname"]->String());
	}
	openvpn->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::openvpn_clients_delete(JSONObject obj){
	Openvpn* instance = openvpn->getInstance(obj[L"name"]->String());
	if(instance){
		if(!instance->isStatic()){
			list<OpenvpnClientConf*> confs = instance->getClients();
			for(list<OpenvpnClientConf*>::iterator iter = confs.begin();
					iter != confs.end();
					iter++){
				if((*iter)->name.compare(obj[L"clientname"]->String()) == 0){
					instance->deleteClient((*iter));
				}
			}
		}
	}
	openvpn->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::openvpn_instances_list(JSONObject obj){
	JSONObject ret;
	JSONArray arr;
	list<Openvpn*> instances = openvpn->listInstances();
	list<Openvpn*>::iterator iter;
	for(iter = instances.begin();
			iter != instances.end();
			iter++){

		Openvpn* target = (*iter);
		JSONObject i;
		i.put(L"name", new JSONValue((string) target->name));

		//Configuration options:
		i.put(L"name", new JSONValue((string) target->name));
		i.put(L"serverip", new JSONValue((string) target->serverip));
		i.put(L"clientip", new JSONValue((string) target->clientip));
		i.put(L"customopts", new JSONValue((string) target->customopts));
		i.put(L"servermask", new JSONValue((string) target->servermask));
		i.put(L"remoteserverip", new JSONValue((string) target->remoteserverip));
		i.put(L"remoteport", new JSONValue((double) target->remoteport));
		i.put(L"compression", new JSONValue((bool) target->compression));
		i.put(L"type", new JSONValue((string) (target->staticAuthentication ? "static" : "tls")));	
		i.put(L"status", new JSONValue((bool) target->status()));
		i.put(L"log", new JSONValue((string) target->log()));
		arr.push_back(new JSONValue(i));
	}

	ret.put(L"items", new JSONValue(arr));	
	openvpn->save();
	return ret;
}

JSONObject NetworkSettingsDelegate::openvpn_instance_create(JSONObject obj){
	Openvpn* target = new Openvpn();
	target->name = obj[L"name"]->String();
	if(obj[L"type"]->String().compare("static") == 0){
		target->staticAuthentication = true;
	}else{
		target->staticAuthentication = false;
		target->certcountry = obj[L"certcountry"]->String();
		target->certprovince = obj[L"certprovince"]->String();
		target->certcity = obj[L"certcity"]->String();
		target->certorg = obj[L"certorg"]->String();
		target->certemail = obj[L"certemail"]->String();
	}

	openvpn->addInstance(target);
	openvpn->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::openvpn_instance_remove(JSONObject obj){
	Openvpn* instance = openvpn->getInstance(obj[L"name"]->String());
	if(instance){
		openvpn->removeInstance(instance);	
	}
	openvpn->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::dhcp_log(JSONObject obj){
	JSONObject ret;

	string log;
	System::exec("tail -n 500 /var/log/syslog | grep dhcpd", log);
	ret.put(L"log", new JSONValue((string) log));	
	return ret;
}

JSONObject NetworkSettingsDelegate::openvpn_instance_setoption(JSONObject obj){
	Openvpn* instance = openvpn->getInstance(obj[L"name"]->String());
	if(instance){
		if(obj.has(L"serverip")){
			instance->serverip = obj[L"serverip"]->String();	
		}	

		if(obj.has(L"clientip")){
			instance->clientip= obj[L"clientip"]->String();	
		}	

		if(obj.has(L"servermask")){
			instance->servermask = obj[L"servermask"]->String();	
		}	

		if(obj.has(L"remoteserverip")){
			instance->remoteserverip = obj[L"remoteserverip"]->String();	
		}	

		if(obj.has(L"remoteport")){
			instance->remoteport = obj[L"remoteport"]->AsNumber();	
		}	

		if(obj.has(L"compression")){
			instance->compression = obj[L"compression"]->AsBool();	
		}	

		if(obj.has(L"customopts")){
			instance->customopts = obj[L"customopts"]->String();	
		}	
	}
	openvpn->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::openvpn_daemonexists(JSONObject obj){
	JSONObject ret;
	ret.put(L"exists", new JSONValue((bool) openvpn->daemonExists()));
	return ret;
}

JSONObject NetworkSettingsDelegate::dyndns(){
	GlobalDynDns* dns = System::getInstance()->getGlobalDynDns();
	JSONObject ret;
	ret.put(L"username", new JSONValue((string) dns->getUsername()));
	ret.put(L"password", new JSONValue((string) dns->getPassword()));
	ret.put(L"domain", new JSONValue((string) dns->getDomain()));
	ret.put(L"enabled", new JSONValue((bool) dns->isEnabled()));
	ret.put(L"type", new JSONValue((double) (DynamicDnsClientProvider) dns->getType()));
	ret.put(L"active", new JSONValue((bool) dns->active()));
	ret.put(L"lastip", new JSONValue((string) dns->getLastIp()));
	return ret;
}

JSONObject NetworkSettingsDelegate::dyndns_set(JSONObject obj){
	GlobalDynDns* dns = System::getInstance()->getGlobalDynDns();
	dns->setUsername(obj[L"username"]->String());
	dns->setPassword(obj[L"password"]->String());
	dns->setDomain(obj[L"domain"]->String());
	dns->setEnabled(obj[L"enabled"]->AsBool());
	dns->setType((DynamicDnsClientProvider) obj[L"type"]->AsNumber());
	dns->run(true);

	dns->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_delete(JSONObject obj){
	interfaceManager->deleteDevice(obj[L"device"]->String());
	interfaceManager->save();
	return JSONObject();
}
