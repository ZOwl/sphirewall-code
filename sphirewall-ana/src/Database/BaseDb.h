/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BASEDB_H
#define BASEDB_H
#include <map>
#include <sqlite3.h>
class DatabaseException : public std::exception {
public:

	DatabaseException(std::string message, std::string query) {
		this->query = query;
		this->msg = message;
	}

	~DatabaseException() throw () {
	}

	virtual const char* what() const throw () {
		return msg.c_str();
	}

	std::string message() {
		return msg;
	}
private:
	std::string msg;
	std::string query;
};

class Lock;
class BaseDb;
class Query {
	public:
		Query(sqlite3* conn, BaseDb* baseDb) : conn(conn), baseDb(baseDb){}
		std::string query;
		void addNumber(int pos, long long value);
		void addNumber(int pos, unsigned int value);
		void addNumber(int pos, int value);
		void addString(int pos, std::string value);
		int execute();
		sqlite3_stmt* statement(){
			return this->dbPrepStatement;
		}

		void addQuery(std::string);

		bool next();
		long long longResult(int pos);
		std::string stringResult(int pos);

	private:
		sqlite3_stmt    *dbPrepStatement;
		sqlite3* conn;
		BaseDb* baseDb;
};

class BaseDb {
	public:
		BaseDb();
		virtual sqlite3* connection() = 0;
		bool execSQL(string sqlStatement);
		void deleteByDate(std::string table, std::string date);
		void deleteBeforeDate(std::string table, std::string date);
		void jsonSql(std::string query);

		void lock();
		void unlock();
		bool tryLock();

		void beginTransaction();
		void endTransaction();

		Query* getQuery(){
			return new Query(connection(), this);
		}
	
                std::map<std::string, sqlite3_stmt*> cache;
		void deleteUsingDate(string tableName, string date);
		void vacume();
	private:
		Lock* l;
};

#endif
