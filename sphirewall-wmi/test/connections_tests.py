#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester

class ConnectionsTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login("fw", "admin", "admin")

    def gotoConnections(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Network')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Connections')]").click()

    def gotoOpenvpn(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Network')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Vpn Servers')]").click()

    def test_CreatePPPoE(self):
        self.gotoConnections()

        self.tester.setValue("name", "pppoe")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='PPPoE']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td/a[contains(normalize-space(), 'pppoe')]//ancestor::tr/td[contains(normalize-space(), 'PPPoE')]//ancestor::tr/td[contains(normalize-space(), 'OFFLINE')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(normalize-space(), 'pppoe')]//ancestor::tr/td/a[contains(normalize-space(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td/a[contains(normalize-space(), 'pppoe')]"))

    def test_CreateOpenVpn(self):
        self.gotoConnections()

        self.tester.setValue("name", "openvpn")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='Openvpn']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td/a[contains(text(), 'openvpn')]//ancestor::tr/td[contains(text(), 'Openvpn')]//ancestor::tr/td[contains(text(), 'OFFLINE')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'openvpn')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td/a[contains(text(), 'openvpn')]"))

    def test_ManageOpenVpn(self):
        self.gotoOpenvpn()
        self.tester.driver.find_element_by_name("name").send_keys("static-custom-vpn1")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='Openvpn Static Key']").click()
        self.tester.driver.find_element_by_id("submit").click()

        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'static-custom-vpn1')]").click()
        self.tester.driver.find_element_by_name("remoteserverip").send_keys("localhost")
        self.tester.driver.find_element_by_name("remoteport").clear()
        self.tester.driver.find_element_by_name("remoteport").send_keys("1122")
        self.tester.driver.find_element_by_name("serverip").send_keys("10.22.1.1")
        self.tester.driver.find_element_by_name("clientip").send_keys("10.22.1.2")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()
        self.tester.clickOptions("Start")

        self.tester.driver.find_element_by_link_text("Clients").click()

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Vpn Servers')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'static-custom-vpn1')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'static-custom-vpn1')]"))


    def tearDown(self):
        self.tester.disconnect()
