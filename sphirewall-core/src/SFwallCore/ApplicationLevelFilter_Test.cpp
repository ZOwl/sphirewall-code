/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <iostream>
#include <list>

#include "Auth/GroupDb.h"
#include "SFwallCore/ApplicationLevelFilter.h"
#include "SFwallCore/Test/TestFactory.h"
#include "Utils/IP4Addr.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Alias.h"
#include "SFwallCore/Conn.h"

using namespace SFwallCore;

TEST(HttpFilter, addFilter){
	HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);
        filter->addRule(new FilterCriteria());

        EXPECT_TRUE(filter->getRules().size() == 1);
}

TEST(HttpFilter, removeFilter){
	HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);
	FilterCriteria* rule1 = new FilterCriteria();
	rule1->enabled = true;

        filter->addRule(rule1);
        filter->addRule(new FilterCriteria());
	
	EXPECT_TRUE(filter->getRules().size() == 2);

        filter->removeRule(rule1);
        EXPECT_TRUE(filter->getRules().size() == 1);
		
}

TEST(HttpFilter, filter_noMatchingRule){
	SFwallCore::TcpPacket* packet = (SFwallCore::TcpPacket*) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332,80);	
        packet->setData("");
	HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);

	EXPECT_TRUE(filter->filter(packet));
}

TEST(HttpFilter, filter_ProxiedTraffic){
	std::string data = "sdfsdfs\n\rGET http://double.testing.com/index.html?sdsdfsdsfd HTTP/1.0\r\ndsfsdfsfdsfs";

	SFwallCore::TcpPacket* packet = (SFwallCore::TcpPacket*) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332,8080);	
	packet->setData(data);

	SFwallCore::TcpConnection* connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);
	connection->increment(packet);	
		
	HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);
	
	SFwallCore::WebsiteListAlias* list = new SFwallCore::WebsiteListAlias();	
	list->addEntry("testing.com");

	SFwallCore::FilterCriteria* matchingRule = new SFwallCore::FilterCriteria();
	matchingRule->enabled = true;
	matchingRule->sourceIp = IP4Addr::stringToIP4Addr("10.1.1.0");
	matchingRule->sourceMask = IP4Addr::stringToIP4Addr("255.255.255.0");
	matchingRule->lists.push_back(list);	

	filter->addRule(matchingRule);

	EXPECT_FALSE(filter->filter(packet));
}

TEST(HttpFilter, filter_disabledRuleMissed){
	std::string data = "sdfsdfs\n\rHost: double.testing.com\r\ndsfsdfsfdsfs";

	SFwallCore::TcpPacket* packet = (SFwallCore::TcpPacket*) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80);	
	packet->setData(data);

	SFwallCore::TcpConnection* connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);
	connection->increment(packet);	
		
	HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);
	
	SFwallCore::WebsiteListAlias* list = new SFwallCore::WebsiteListAlias();	
	list->addEntry("testing.com");

	SFwallCore::FilterCriteria* matchingRule = new SFwallCore::FilterCriteria();
	matchingRule->enabled = false;
	matchingRule->sourceIp = IP4Addr::stringToIP4Addr("10.1.1.0");
	matchingRule->sourceMask = IP4Addr::stringToIP4Addr("255.255.255.0");
	matchingRule->lists.push_back(list);	

	filter->addRule(matchingRule);

	EXPECT_TRUE(filter->filter(packet));
}
TEST(HttpFilter, filter_basicSourceMatch){
	std::string data = "sdfsdfs\n\rHost: double.testing.com\r\ndsfsdfsfdsfs";

	SFwallCore::TcpPacket* packet = (SFwallCore::TcpPacket*) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80);	
	packet->setData(data);

	SFwallCore::TcpConnection* connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);
	connection->increment(packet);	
		
	HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);
	
	SFwallCore::WebsiteListAlias* list = new SFwallCore::WebsiteListAlias();	
	list->addEntry("testing.com");

	SFwallCore::FilterCriteria* matchingRule = new SFwallCore::FilterCriteria();
	matchingRule->enabled = true;
	matchingRule->sourceIp = IP4Addr::stringToIP4Addr("10.1.1.0");
	matchingRule->sourceMask = IP4Addr::stringToIP4Addr("255.255.255.0");
	matchingRule->lists.push_back(list);	

	filter->addRule(matchingRule);

	EXPECT_FALSE(filter->filter(packet));
}

TEST(HttpFilter, filter_basicAliasSourceMatch){
	std::string data = "sdfsdfs\n\rHost: double.testing.com\r\ndsfsdfsfdsfs";

	SFwallCore::TcpPacket* packet = (SFwallCore::TcpPacket*) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80);	
	packet->setData(data);

	SFwallCore::TcpConnection* connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);
	connection->increment(packet);	
		
	HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);
	
	SFwallCore::WebsiteListAlias* list = new SFwallCore::WebsiteListAlias();	
	list->addEntry("testing.com");

	SFwallCore::IpRangeAlias* alias = new SFwallCore::IpRangeAlias();	
	alias->addEntry("10.1.1.1-10.1.1.1");

	SFwallCore::FilterCriteria* matchingRule = new SFwallCore::FilterCriteria();
	matchingRule->enabled = true;
	matchingRule->lists.push_back(list);	
	matchingRule->sourceAlias.push_back(alias);
	filter->addRule(matchingRule);

	EXPECT_FALSE(filter->filter(packet));
}

TEST(HttpFilter, filter_basicAliasSourceMisMatch){
	std::string data = "sdfsdfs\n\rHost: double.testing.com\r\ndsfsdfsfdsfs";

	SFwallCore::TcpPacket* packet = (SFwallCore::TcpPacket*) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80);	
	packet->setData(data);

	SFwallCore::TcpConnection* connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);
	connection->increment(packet);	
		
	HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);
	
	SFwallCore::WebsiteListAlias* list = new SFwallCore::WebsiteListAlias();	
	list->addEntry("testing.com");

	SFwallCore::IpRangeAlias* alias= new SFwallCore::IpRangeAlias();	
	alias->addEntry("10.1.1.7-10.1.1.10");

	SFwallCore::FilterCriteria* matchingRule = new SFwallCore::FilterCriteria();
	matchingRule->enabled = true;
        matchingRule->lists.push_back(list);  
	matchingRule->sourceAlias.push_back(alias);	

	filter->addRule(matchingRule);

	EXPECT_TRUE(filter->filter(packet));
}


TEST(HttpFilter, filter_basicSourceMatch_multipleFilters){
	std::string data = "sdfsdfs\nHost: testing.com\r\ndsfsdfsfdsfs";

	SFwallCore::TcpPacket* packet = (SFwallCore::TcpPacket*) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332,80);	
	packet->setData(data);
        SFwallCore::TcpConnection* connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
        connection->increment(packet);

	HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);

        SFwallCore::WebsiteListAlias* list = new SFwallCore::WebsiteListAlias();	
	list->addEntry("testing.com");

	SFwallCore::FilterCriteria* matchingRule = new SFwallCore::FilterCriteria();
	matchingRule->enabled = true;
	matchingRule->sourceIp = IP4Addr::stringToIP4Addr("10.1.1.1");
	matchingRule->sourceMask = IP4Addr::stringToIP4Addr("10.1.1.1");
        matchingRule->lists.push_back(list);  

	SFwallCore::FilterCriteria* matchingRule2 = new SFwallCore::FilterCriteria();
	matchingRule2->enabled = true;
	matchingRule2->sourceIp = IP4Addr::stringToIP4Addr("10.33.11.3");
	matchingRule2->sourceMask = IP4Addr::stringToIP4Addr("255.255.255.0");
        matchingRule2->lists.push_back(list);  

	filter->addRule(matchingRule);
	filter->addRule(matchingRule2);

	EXPECT_FALSE(filter->filter(packet));
}

TEST(HttpFilter, filter_noMatchingUrl){
	std::string data = "sdfsdfs\nHost: testing.com\r\ndsfsdfsfdsfs";

	SFwallCore::TcpPacket* packet = (SFwallCore::TcpPacket*) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332,80);	
	packet->setData(data);
        SFwallCore::TcpConnection* connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
        connection->increment(packet);

	HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);

        SFwallCore::WebsiteListAlias* list = new SFwallCore::WebsiteListAlias();
	list->addEntry("google.com");

	SFwallCore::FilterCriteria* matchingRule = new SFwallCore::FilterCriteria();
	matchingRule->enabled = true;
	matchingRule->sourceIp = IP4Addr::stringToIP4Addr("10.1.1.1");
	matchingRule->sourceMask = IP4Addr::stringToIP4Addr("255.255.255.0");
        matchingRule->lists.push_back(list);  

	filter->addRule(matchingRule);

	EXPECT_TRUE(filter->filter(packet));
}

TEST(HttpFilter, filter_noMatchingFilter){
	std::string data = "sdfsdfs\nHost: testing.com\r\ndsfsdfsfdsfs";

	SFwallCore::TcpPacket* packet = (SFwallCore::TcpPacket*) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332,80);	
	packet->setData(data);
        SFwallCore::TcpConnection* connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
        connection->increment(packet);

	HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);

        SFwallCore::WebsiteListAlias* list = new SFwallCore::WebsiteListAlias();
	list->addEntry("google.com");

	SFwallCore::FilterCriteria* matchingRule = new SFwallCore::FilterCriteria();
	matchingRule->enabled = true;
	matchingRule->sourceIp = IP4Addr::stringToIP4Addr("10.1.1.10");
	matchingRule->sourceMask = IP4Addr::stringToIP4Addr("255.255.255.0");
        matchingRule->lists.push_back(list);  

	filter->addRule(matchingRule);

	EXPECT_TRUE(filter->filter(packet));
}

TEST(HttpFilter, filter_groupMatch){
	User* user = new User();
	Group* group = new Group();
	user->addGroup(group);

	std::string data = "sdfsdfs\nHost: testing.com\r\ndsfsdfsfdsfs";

        SFwallCore::TcpPacket* packet = (SFwallCore::TcpPacket*) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332,80);
        packet->setData(data);
	packet->setUser(user);

        SFwallCore::TcpConnection* connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
        connection->increment(packet);

        HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);

        SFwallCore::WebsiteListAlias* list = new SFwallCore::WebsiteListAlias();
        list->addEntry("testing.com");

        SFwallCore::FilterCriteria* matchingRule = new SFwallCore::FilterCriteria();
	matchingRule->enabled = true;
	matchingRule->groups.push_back(group);
        matchingRule->lists.push_back(list);  

        filter->addRule(matchingRule);

        EXPECT_FALSE(filter->filter(packet));
}

TEST(HttpFilter, filter_groupNoMatch){
	User* user = new User();
	Group* group = new Group();
	group->setId(0);
	Group* group2 = new Group();
	group2->setId(1);
	user->addGroup(group);

	std::string data = "sdfsdfs\nHost: testing.com\r\ndsfsdfsfdsfs";
        SFwallCore::TcpPacket* packet = (SFwallCore::TcpPacket*) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332,80);
        packet->setData(data);
	packet->setUser(user);

        SFwallCore::TcpConnection* connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
        connection->increment(packet);

        HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);
	
	SFwallCore::WebsiteListAlias* list = new SFwallCore::WebsiteListAlias();
	list->addEntry("testing.com");

        SFwallCore::FilterCriteria* matchingRule = new SFwallCore::FilterCriteria();
	matchingRule->enabled = true;
	matchingRule->groups.push_back(group2);
        matchingRule->lists.push_back(list);

        filter->addRule(matchingRule);

        EXPECT_TRUE(filter->filter(packet));
}

TEST(HttpFilter, whiteListMode_allow_exclusive){
        std::string data = "sdfsdfs\nHost: testing.com\r\ndsfsdfsfdsfs";
        SFwallCore::TcpPacket* packet = (SFwallCore::TcpPacket*) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332,80);
        packet->setData(data);
        SFwallCore::TcpConnection* connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
        connection->increment(packet);

        HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);

        SFwallCore::WebsiteListAlias* list = new SFwallCore::WebsiteListAlias();
        list->addEntry("testing.com");

        SFwallCore::FilterCriteria* matchingRule = new SFwallCore::FilterCriteria();
        matchingRule->lists.push_back(list);
	matchingRule->enabled = true;
        matchingRule->action = ALLOW;
	matchingRule->exclusive = true;
        filter->addRule(matchingRule);

        EXPECT_TRUE(filter->filter(packet));
}

TEST(HttpFilter, whiteListOverRidesBlackList){
        std::string data = "sdfsdfs\nHost: testing.com\r\ndsfsdfsfdsfs";
        SFwallCore::TcpPacket* packet = (SFwallCore::TcpPacket*) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332,80);
        packet->setData(data);
        SFwallCore::TcpConnection* connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
        connection->increment(packet);

        HttpUrlApplicationFilter* filter = new HttpUrlApplicationFilter(NULL, NULL, NULL);

        SFwallCore::WebsiteListAlias* list = new SFwallCore::WebsiteListAlias();
        list->addEntry("testing.com");

        SFwallCore::FilterCriteria* matchingRule = new SFwallCore::FilterCriteria();
	matchingRule->enabled = true;
        matchingRule->lists.push_back(list);
        matchingRule->action = ALLOW;

        filter->addRule(matchingRule);

	SFwallCore::FilterCriteria* blackListRule = new SFwallCore::FilterCriteria();
	blackListRule->enabled = true;
        blackListRule->lists.push_back(list);
        blackListRule->action = DENY;

        filter->addRule(blackListRule);
        EXPECT_TRUE(filter->filter(packet));
}
