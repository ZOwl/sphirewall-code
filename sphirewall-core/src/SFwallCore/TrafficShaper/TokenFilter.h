/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TOKENFILTER_H_INCLUDED
#define TOKENFILTER_H_INCLUDED

#include "../Packet.h"
#include "Auth/User.h"

class TokenFilter {
public:
	TokenFilter();

	TokenFilter(const TokenFilter& orig) {
		dport = orig.dport;
		sport = orig.sport;
		source = orig.source;
		sourceMask = orig.sourceMask;
		dest = orig.dest;
		destMask = orig.destMask;
		type = orig.type;
		userPtr = orig.userPtr;
		dest_dev = orig.dest_dev;
		source_dev = orig.source_dev;
	}

	bool check(SFwallCore::Packet* packet);
	int dest_dev, source_dev;
	int dport, sport;
	int source, sourceMask;
	int dest, destMask;
	int type;

	User* userPtr;

	int block_dest;
	int block_destMask;
	int block_source;
	int block_sourceMask;
	int block_groupid;

};

#endif
