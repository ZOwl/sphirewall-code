#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

class FirewallSettings:
    connection = None

    def __init__(self, connection):
        self.connection = connection

    def connections_list(self):
        return self.connection.request("firewall/tracker/list", None)["connections"]

    def connections_size(self):
        return self.connection.request("firewall/tracker/size", None)["size"]

    def aliases(self, id=None):
        if id is not None:
            allAliases = self.aliases()
            for alias in allAliases:
                if alias["id"] == id:
                    return alias

        return self.connection.request("firewall/aliases/list", None)["aliases"]

    def aliases_create(self, name, type, source, detail):
        args = {}
        args["name"] = name
        args["type"] = int(type)
        args["source"] = int(source)
        args["detail"] = detail

        self.connection.request("firewall/aliases/add", args)

    def aliases_delete(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/aliases/del", args)

    def aliases_sync(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/aliases/load", args)

    def aliases_list(self, id):
        args = {}
        args["id"] = id
        return self.connection.request("firewall/aliases/alias/list", args)["items"]

    def aliases_list_add(self, id, value):
        args = {}
        args["id"] = id
        args["value"] = value
        self.connection.request("firewall/aliases/alias/add", args)

    def aliases_list_del(self, id, value):
        args = {}
        args["id"] = id
        args["value"] = value
        self.connection.request("firewall/aliases/alias/del", args)

    def webfilter(self, id=None):
        if id is not None:
            all_rules = self.connection.request("firewall/webfilter/rules/list", None)["rules"]
            for rule in all_rules:
                if rule["id"] == id:
                    return rule
            return None
        return self.connection.request("firewall/webfilter/rules/list", None)["rules"]

    def webfilter_get(self, id):
        if id is not None:
            all_rules = self.connection.request("firewall/webfilter/rules/list", None)["rules"]
            for rule in all_rules:
                if rule["id"] == id:
                    return rule
            return None

    def webfilter_add(self, id, sourceIp, sourceMask, group, list, action, fireEvent, redirect, redirectUrl, startTime,
                      endTime, mon,
                      tues, wed, thurs, fri, sat, sun, exclusive, sourceAlias):
        args = {}
        if id is not None:
            args["id"] = id
        args["sourceIp"] = sourceIp
        args["sourceMask"] = sourceMask

        args["group"] = []
        for gid in group:
            args["group"].append(int(gid))

        args["list"] = []
        for listid in list:
            args["list"].append(listid)

        args["sourceAlias"] = []
        for alias in sourceAlias:
            args["sourceAlias"].append(alias)

        args["action"] = int(action)
        args["fireEvent"] = fireEvent
        args["redirect"] = redirect
        args["redirectUrl"] = redirectUrl

        args["mon"] = mon
        args["tues"] = tues
        args["wed"] = wed
        args["thurs"] = thurs
        args["fri"] = fri
        args["sat"] = sat
        args["sun"] = sun

        args["exclusive"] = exclusive

        if len(startTime) > 0:
            args["startTime"] = int(startTime)
        else:
            args["startTime"] = -1

        if len(endTime) > 0:
            args["endTime"] = int(endTime)
        else:
            args["endTime"] = -1

        self.connection.request("firewall/webfilter/rules/add", args)

    def webfilter_delete(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/webfilter/rules/remove", args)


    def webfilter_moveup(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/webfilter/rules/moveup", args)

    def webfilter_movedown(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/webfilter/rules/movedown", args)

    def webfilter_enable(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/webfilter/rules/enable", args)

    def webfilter_disable(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/webfilter/rules/disable", args)

    def denylist(self):
        return self.connection.request("firewall/deny/list", None)["list"]

    def denylist_delete(self, host):
        args = {}
        args["ip"] = host
        return self.connection.request("firewall/deny/del", args)

    def qos(self):
        return self.connection.request("firewall/acls/list/trafficshaper", None)["items"]

    def qos_add(self, sourceBlock, sourceIp, sourceMask, sourcePort, groupid, destinationBlock, destinationIp,
                destinationMask, destinationPort, upload, download):
        args = {}
        args["sourceBlock"] = sourceBlock
        args["destinationBlock"] = destinationBlock

        if len(sourceIp) > 0:
            args["source"] = sourceIp
            args["sourceMask"] = sourceMask
        if len(sourcePort) > 0:
            args["sourcePort"] = int(sourcePort)
        if len(groupid) > 0:
            args["groupid"] = int(groupid)
        if len(destinationIp) > 0:
            args["destination"] = destinationIp
            args["destinationMask"] = destinationMask
        if len(destinationPort) > 0:
            args["destinationPort"] = int(destinationPort)

        args["upload"] = int(upload)
        args["download"] = int(download)

        self.connection.request("firewall/acls/add/trafficshaper", args)

    def qos_del(self, id):
        args = {}
        args["id"] = int(id)
        self.connection.request("firewall/acls/del/trafficshaper", args)

    def normal_add(self, id,source, source_mask,
                   source_alias, sport_start, sport_end, dport_start, dport_end, sport, dport, source_dev, dest,
                   dest_mask, dest_alias, startHour, finishHour, dest_dev, ignoreconntrack, groupid, hwAddress,
                   comment, protocol, action, snat_target, snat_device_target, dnat_target, dnat_port, log, nice):
        args = {}
        if id is not None:
            args["id"] = id
        if len(source) > 0:
            args["sourceAddress"] = source
            args["sourceSubnet"] = source_mask
        if len(source_alias) > 0:
            args["sourceAlias"] = source_alias
        if len(sport_start) > 0:
            args["sport_start"] = int(sport_start)
            args["sport_end"] = int(sport_end)
        if len(dport_start) > 0:
            args["dport_start"] = int(dport_start)
            args["dport_end"] = int(dport_end)
        if len(sport) > 0:
            args["sourcePort"] = sport
        if len(source_dev) > 0 and source_dev is not "-1":
            args["sourceDev"] = source_dev

        if len(dest) > 0:
            args["destAddress"] = dest
            args["destSubnet"] = dest_mask
        if len(dest_alias) > 0:
            args["destAlias"] = dest_alias
        if len(dport) > 0:
            args["destPort"] = dport
        if len(startHour) > 0:
            args["startHour"] = int(startHour)
            args["finishHour"] = int(finishHour)
        if len(dest_dev) > 0 and dest_dev is not "-1":
            args["destDev"] = dest_dev

        args["ignoreconntrack"] = ignoreconntrack
        args["groupid"] = int(groupid)
        args["hwAddress"] = hwAddress
        args["comment"] = comment
        args["protocol"] = protocol
        args["log"] = log

        if action == "ACCEPT":
            args["action"] = 1
        if action == "DROP":
            args["action"] = 0
        if action == "MASQUERADE":
            args["action"] = 2
            if len(snat_target) > 0:
                args["masqueradeTarget"] = snat_target
            else:
                args["masqueradeTargetDevice"] = snat_device_target
        if action == "PRIORITISE":
            args["action"] = 4
            args["nice"] = int(nice)

        if action == "FORWARD":
            args["action"] = 3
            args["forwardTarget"] = dnat_target
            args["forwardPort"] = int(dnat_port)

        self.connection.request("firewall/acls/add", args)


    def acls(self):
        return self.connection.request("firewall/acls/list", None)["normal"]

    def priority(self):
        return self.connection.request("firewall/acls/list", None)["priority"]

    def get(self, id):
        all_rules = self.acls()
        for rule in all_rules:
            if rule["id"] == id:
                return rule

        all_rules = self.nat()
        for rule in all_rules:
            if rule["id"] == id:
                return rule

        all_rules = self.priority()
        for rule in all_rules:
            if rule["id"] == id:
                return rule

        return None

    def nat(self):
        return self.connection.request("firewall/acls/list", None)["nat"]

    def normal_delete(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/acls/filter/delete", args)

    def normal_up(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/acls/filter/up", args)

    def normal_down(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/acls/filter/down", args)

    def normal_enable(self, id):
        args = {}
        args["id"] = id
        args["ruleType"] = 0
        self.connection.request("firewall/acls/enable", args)

    def normal_disable(self, id):
        args = {}
        args["id"] = id
        args["ruleType"] = 0
        self.connection.request("firewall/acls/disable", args)

    def connection_terminate(self, sourceIp, sourcePort, destIp, destPort):
        args = {}
        args["source"] = sourceIp
        args["sourcePort"] = int(sourcePort)
        args["dest"] = destIp
        args["destPort"] = int(destPort)

        self.connection.request("firewall/tracker/terminate", args)

    def defaultruleset(self):
        current_acls = self.acls()
        print current_acls
        if len(current_acls) == 2:
            if current_acls[0]["id"] == "default1" and current_acls[1]["id"] == "default2":
                return True
        return False

    def force_auth_settings(self, source=None, websites=None):
        if source is not None and websites is not None:
            self.connection.request("firewall/rewrite/forceauthranges/set", {"ranges" : source, "websites" : websites})

        return self.connection.request("firewall/rewrite/forceauthranges", {})
