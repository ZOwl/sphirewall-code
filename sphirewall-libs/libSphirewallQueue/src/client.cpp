/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <queue>
#include <asm/types.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include  <sys/types.h> // key_t //
#include  <sys/ipc.h>   // IPC_CREAT, ftok //
#include  <sys/msg.h>   // msgget, msgbuf //
#include <netinet/udp.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <linux/icmp.h>
#include <linux/if_ether.h>
#include <map>
#include <linux/netfilter.h>
#include <fcntl.h>
#include <sys/mman.h>

#include "../includes/sphirewall_queue.h"
#include "../includes/client.h"
#include "nbqueue.h"

#define SHM_SIZE 4096 * 64 
/*Connection socket*/
int sock_fd;

/*Static variable that keeps the client alive - disconnect and connect handle this*/
bool SqClient::ALIVE = false;

pthread_mutex_t molock;
pthread_mutex_t sendlock;

/*Static function pointer - callback for processing the packets*/
int (*SqClient::filter_hook)(struct sq_packet* packet, bool &modified) = NULL;
struct NatInstruction* (*SqClient::prerouting_hook)(struct sq_packet* packet) = NULL;
struct NatInstruction* (*SqClient::postrouting_hook)(struct sq_packet* packet) = NULL;

/*stl fifo queue for holding message pointers, this prevents the module from halting, the client buffers packets*/
std::queue<struct message*> message_queue;

/*Map/Queue for packets that are queued for processing*/
std::map<int, struct message*> message_store;

void dump_packet(unsigned char* packet);

/*Processing sleep inteval - spinlock styles. Decrease to reduce latency, increase to reduce cpu time. Hard to find the best value*/
int SqClient::sleepInterval = 500;

/*Queues*/
struct meta* send_nbqueue;
struct meta* recv_nbqueue;
int send_nbqueue_configfd;
int recv_nbqueue_configfd;

static int x = 0;
int SqClient::life = -1;
int* SqClient::yieldCountSetting = NULL;
int* SqClient::usleepInterval = NULL;

/*Netlink Helper Functions*/
int sendMsg(struct message* m) {
	pthread_mutex_lock(&sendlock);
	queue_enqueue(m, send_nbqueue);	
	pthread_mutex_unlock(&sendlock);
	return 1;
}

void queue_wakeup(int sig){
	pthread_mutex_lock(&queue_recv_lock);
	pthread_cond_broadcast(&queue_recv_lock_wait_cond);
	pthread_mutex_unlock(&queue_recv_lock);
}

int SqClient::connect() {
	int yieldCountSettingInt = 10000;
	int usleepInt = 1000;

	setYieldCountSetting(&yieldCountSettingInt);
	setUsleepInterval(&usleepInt);

	pthread_mutexattr_t attr;
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK);

	pthread_mutex_init(&queue_recv_lock, &attr);
	pthread_mutex_init(&sendlock, NULL);
	signal(SIGUSR1, queue_wakeup);

	send_nbqueue_configfd = open("/sys/kernel/debug/sphirewall_recv", O_RDWR);
	if(send_nbqueue_configfd < 0) {
		return -1;
	}

	void* send_address = mmap(NULL, SHM_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, send_nbqueue_configfd, 0);
	if (send_address== MAP_FAILED) {
		return -1;
	}

	recv_nbqueue_configfd = open("/sys/kernel/debug/sphirewall_send", O_RDWR);
	if(recv_nbqueue_configfd < 0) {
		return -1;
	}

	void* recv_address= mmap(NULL, SHM_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, recv_nbqueue_configfd, 0);
	if (recv_address== MAP_FAILED) {
		return -1;
	}

	send_nbqueue = queue_connect(send_address);
	recv_nbqueue = queue_connect(recv_address);

	return 0;
}

void* process_packet_thread(void*);
bool process_packet_thread_open = false;

//FLOWS:
#define NO_FLOWS 5
#define CYCLE_COST 100
pthread_mutex_t enqueue_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t enqueue_lock_cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t flowqueuelock = PTHREAD_MUTEX_INITIALIZER;

void waitonenqueue(){
	pthread_mutex_lock(&enqueue_lock);

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	ts.tv_sec += 1;
	pthread_cond_timedwait(&enqueue_lock_cond, &enqueue_lock, &ts);
	pthread_mutex_unlock(&enqueue_lock);
}

void wakedequeue(){
	pthread_mutex_lock(&enqueue_lock);
        pthread_cond_broadcast(&enqueue_lock_cond);
        pthread_mutex_unlock(&enqueue_lock);
}

std::queue<struct message*> flows[NO_FLOWS];
void* dequeue_flows(void*){
	while(process_packet_thread_open){	
		//wake up lock
		int yieldCounter = 0;
		for(int x = NO_FLOWS; x-- > 0 ;){
			while(true){
				if(yieldCounter > CYCLE_COST){
					break;
				}	
	
				pthread_mutex_lock(&flowqueuelock);
				if(flows[x].empty()){
					pthread_mutex_unlock(&flowqueuelock);
					break;
				}

				struct message* target = flows[x].front();	
				if(target != NULL){
					sendMsg(target);

					free(target->packet.raw_packet);
					free(target); 

					flows[x].pop();
					yieldCounter++;

				}
				pthread_mutex_unlock(&flowqueuelock);
			}

			if(yieldCounter > CYCLE_COST){
				break;
			}
		}

		//a little bit inefficient - lets play later
		if(yieldCounter == 0){
			waitonenqueue();
		}		
	}
}

void enqueue_flows(struct message* msgPtr, int flow){
	struct message* nmessage = (struct message*) malloc(sizeof(struct message));
	unsigned char* rawpacket = (unsigned char*) malloc(RAWSIZE);
	memcpy(nmessage, msgPtr, sizeof(struct message));
	memcpy(rawpacket, msgPtr->packet.raw_packet, RAWSIZE);
	nmessage->packet.raw_packet = rawpacket;

	pthread_mutex_lock(&flowqueuelock);
	flows[flow].push(nmessage);
	pthread_mutex_unlock(&flowqueuelock);

	wakedequeue();
}
//END:

void SqClient::run() {
	int counter = 0;
	process_packet_thread_open = true;

	pthread_t tid2;
	pthread_create(&tid2, NULL, dequeue_flows, NULL);

	pthread_t tid;
	pthread_create(&tid, NULL, process_packet_thread, NULL);

	while(process_packet_thread_open){
		sleep(1000);
	}
}

void* process_packet_thread(void*) {
	queue_wakeup(-1);
	process_packet_thread_open = true;
	while (SqClient::life == -1 || x++ < SqClient::life) {
		struct message* msgPtr = queue_peek(recv_nbqueue);
		if(msgPtr == NULL || msgPtr->packet.raw_packet == NULL){
			return NULL;
		}

		bool modified = false;
		switch (msgPtr->hook) {
			case PREROUTING:
				{
					struct NatInstruction* ins  = (*SqClient::prerouting_hook)(&msgPtr->packet);
					if(ins){
						msgPtr->nat.type = ins->type;
						msgPtr->nat.target_address = ins->targetAddress;
						msgPtr->nat.target_port = ins->targetPort;
						msgPtr->verdict = 1;
						sendMsg(msgPtr);
						free(ins);
					}else {
						msgPtr->nat.type = 0;
						msgPtr->verdict = 1;
						sendMsg(msgPtr);
						free(ins);
					}

					break;
				}

			case FILTER:
				{
					int verdict = (*SqClient::filter_hook)(&msgPtr->packet, modified);
					if(verdict == -1){
						msgPtr->verdict = NF_DROP;
						sendMsg(msgPtr);
					}else if(verdict == 0){
						pthread_mutex_lock(&molock);
						//perform a deep copy on the message obj:
						struct message* nmessage = (struct message*) malloc(sizeof(struct message));
						unsigned char* rawpacket = (unsigned char*) malloc(RAWSIZE);
						memcpy(nmessage, msgPtr, sizeof(struct message));
						memcpy(rawpacket, msgPtr->packet.raw_packet, RAWSIZE);	
						nmessage->packet.raw_packet = rawpacket;	

						message_store[msgPtr->packet.id] = nmessage;
						pthread_mutex_unlock(&molock);
					}else if(verdict == 1){
						msgPtr->verdict = NF_ACCEPT;
						if (modified) {
							msgPtr->modified = 1;
						} else {
							msgPtr->modified = 0;
						}
						sendMsg(msgPtr);
					}else{
						msgPtr->verdict = NF_ACCEPT;
						if (modified) {
							msgPtr->modified = 1;
						} else {
							msgPtr->modified = 0;
						}
						enqueue_flows(msgPtr, verdict -2);
					}		
					break;
				}

			case POSTROUTING:
				{
					struct NatInstruction* ins  = (*SqClient::postrouting_hook)(&msgPtr->packet);
					if(ins){
						msgPtr->nat.type = ins->type;
						msgPtr->nat.target_address = ins->targetAddress;
						msgPtr->nat.target_port = ins->targetPort;
						msgPtr->verdict = 1;
						sendMsg(msgPtr);
						free(ins);
					}else{
                                                msgPtr->nat.type = 0;
                                                msgPtr->verdict = 1;
                                                sendMsg(msgPtr);
                                                free(ins);
					}

					break;
				}
		}
		queue_pop(recv_nbqueue);
	}

	process_packet_thread_open = false;
}

int SqClient::reinject(int id) {
	pthread_mutex_lock(&molock);
	//Note, this is not effective, stl uses a binary search tree
	if (message_store.find(id) != message_store.end()) {
		struct message* msgPtr = message_store[id];
		if (msgPtr != NULL) {
			msgPtr->verdict = 1;

			sendMsg(msgPtr);
			message_store.erase(id);

			free(msgPtr->packet.raw_packet);
			free(msgPtr);
		}
	} else {
		pthread_mutex_unlock(&molock);
		return -1;
	}

	pthread_mutex_unlock(&molock);
	return 0;
}

void SqClient::disconnect() {
	process_packet_thread_open = false;
	send_nbqueue->closing = 1;
	recv_nbqueue->closing = 1;

	while(send_nbqueue->closing != 2 && recv_nbqueue->closing != 2){
		sleep(2);
	}

	munmap(send_nbqueue, SHM_SIZE);	
	munmap(recv_nbqueue, SHM_SIZE);	
	close(send_nbqueue_configfd);
	close(recv_nbqueue_configfd);
}

/*
 *	Example and Test Code
 *	
 *	Compile this library as a executable, do fun stuff here, dont test on sphirewall - that would be silly now wouldnt it!!!!
 */
void dump_packet(unsigned char* packet) {
	printf("printing packet at loc: %p\n", packet);

	struct iphdr* ip = (struct iphdr*) packet;

	printf("checksum: %d\n", ntohs(ip->check));

	printf("\n==================================================\n");
	char src_ip[20], dest_ip[20];


	/* Get source and destination addresses */
	strcpy(src_ip, inet_ntoa(*(struct in_addr *) &ip->saddr));
	strcpy(dest_ip, inet_ntoa(*(struct in_addr *) &ip->daddr));
	printf("Type: ");

	if (ip->protocol == IPPROTO_TCP) {
		printf("TCP\n");
		struct tcphdr* packet_tcp_header;
		packet_tcp_header = (struct tcphdr *) (packet + (ip->ihl << 2));
		printf("dport: %d sport: %d\n", ntohs(packet_tcp_header->source), ntohs(packet_tcp_header->dest));

	} else if (ip->protocol == IPPROTO_UDP) {
		printf("UDP\n");
	}

	printf("%s -> %s\n", src_ip, dest_ip);
	printf("Length of IP data: %d\n", ntohs(ip->tot_len));
}

int process(struct sq_packet* sqp, bool & modified) {
	unsigned char* p = (unsigned char*) sqp->raw_packet;
	//dump_packet(p);

	struct iphdr* ip = (struct iphdr*) p;
	if (ip->protocol == IPPROTO_TCP) {
		struct tcphdr* packet_tcp_header;
		packet_tcp_header = (struct tcphdr *) (p+ (ip->ihl << 2));
		if(ntohs(packet_tcp_header->source) == 5005 || ntohs(packet_tcp_header->dest) == 5005){
			return 4;
		}
		if(ntohs(packet_tcp_header->source) == 5004 || ntohs(packet_tcp_header->dest) == 5004){
			return 3;
		}
	}	
	return 1;
}

NatInstruction* processPreRouting(struct sq_packet* sqp) {
	unsigned char* p = (unsigned char*) sqp->raw_packet;
	//	dump_packet(p);

	return NULL;
}

NatInstruction* processPostRouting(struct sq_packet* sqp) {
	return NULL;
}

int main() {
	SqClient client;
	client.connect();

	client.registerFilterCallback(&process);
	client.registerPreroutingCallback(&processPreRouting);
	client.registerPostroutingCallback(&processPostRouting);

	//	client.setPacketLifeLimit(20);	
	client.run();

	client.disconnect();

	return 0;
}

