#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester

class AuthenticationTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login("fw", "admin", "admin")

    def gotoOpenvpn(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Networking')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Vpn Servers')]").click()

    def test_Static_CreateAndRemove(self):
        self.gotoOpenvpn()
        self.tester.driver.find_element_by_name("name").send_keys("static-vpn1")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='Openvpn Static Key']").click()
        self.tester.driver.find_element_by_id("submit").click()

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'static-vpn1')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'static-vpn1')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'static-vpn1')]"))

    def test_Tls_CreateAndRemove(self):
        self.gotoOpenvpn()
        self.tester.driver.find_element_by_name("name").send_keys("tls-vpn1")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='Openvpn TLS Keys']").click()
        self.tester.driver.find_element_by_id("submit").click()

        self.assertTrue(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'tls-vpn1')]"))
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'tls-vpn1')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'tls-vpn1')]"))

    def test_Static_Configure(self):
        self.gotoOpenvpn()
        self.tester.driver.find_element_by_name("name").send_keys("static-custom-vpn1")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='Openvpn Static Key']").click()
        self.tester.driver.find_element_by_id("submit").click()

        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'static-custom-vpn1')]").click()
        self.tester.driver.find_element_by_name("remoteserverip").send_keys("fw")
        self.tester.driver.find_element_by_name("remoteport").clear()
        self.tester.driver.find_element_by_name("remoteport").send_keys("1122")
        self.tester.driver.find_element_by_name("compression").click()
        self.tester.driver.find_element_by_name("customopts").send_keys("verb 3")
        self.tester.driver.find_element_by_name("serverip").send_keys("10.1.1.1")
        self.tester.driver.find_element_by_name("clientip").send_keys("10.1.1.2")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertEquals(self.tester.driver.find_element_by_name("remoteserverip").get_attribute("value"), "fw")
        self.assertEquals(self.tester.driver.find_element_by_name("remoteport").get_attribute("value"), "1122")
        self.assertEquals(self.tester.driver.find_element_by_name("compression").get_attribute("value"), "on")
        self.assertEquals(self.tester.driver.find_element_by_name("customopts").get_attribute("value").strip(), "verb 3")
        self.assertEquals(self.tester.driver.find_element_by_name("serverip").get_attribute("value"), "10.1.1.1")
        self.assertEquals(self.tester.driver.find_element_by_name("clientip").get_attribute("value"), "10.1.1.2")

        self.tester.clickOptions("Start")
        self.tester.clickOptions("Stop")

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Vpn Servers')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'static-custom-vpn1')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'static-custom-vpn1')]"))

    def test_Tls_Configure(self):
        self.gotoOpenvpn()
        self.tester.driver.find_element_by_name("name").send_keys("tls-custom-vpn1")
        self.tester.driver.find_element_by_xpath("//select[@name='type']/option[text()='Openvpn TLS Keys']").click()
        self.tester.driver.find_element_by_id("submit").click()

        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'tls-custom-vpn1')]").click()
        self.tester.driver.find_element_by_name("remoteserverip").send_keys("fw")
        self.tester.driver.find_element_by_name("remoteport").clear()
        self.tester.driver.find_element_by_name("remoteport").send_keys("1122")
        self.tester.driver.find_element_by_name("compression").click()
        self.tester.driver.find_element_by_name("customopts").send_keys("verb 3")
        self.tester.driver.find_element_by_name("serverip").send_keys("10.1.1.0")
        self.tester.driver.find_element_by_name("servermask").send_keys("255.255.255.0")
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertEquals(self.tester.driver.find_element_by_name("remoteserverip").get_attribute("value"), "fw")
        self.assertEquals(self.tester.driver.find_element_by_name("remoteport").get_attribute("value"), "1122")
        self.assertEquals(self.tester.driver.find_element_by_name("compression").get_attribute("value"), "on")
        self.assertEquals(self.tester.driver.find_element_by_name("customopts").get_attribute("value").strip(), "verb 3")
        self.assertEquals(self.tester.driver.find_element_by_name("serverip").get_attribute("value"), "10.1.1.0")
        self.assertEquals(self.tester.driver.find_element_by_name("servermask").get_attribute("value"), "255.255.255.0")

        self.tester.clickOptions("Start")
        self.tester.clickOptions("Stop")

        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Vpn Servers')]").click()
        self.tester.driver.find_element_by_xpath("//table//tr/td/a[contains(text(), 'tls-custom-vpn1')]//ancestor::tr/td/a[contains(text(), 'Delete')]").click()
        self.assertFalse(self.tester.check_exists_by_xpath("//table//tr/td/a[contains(text(), 'tls-custom-vpn1')]"))

    def tearDown(self):
        self.tester.disconnect()
