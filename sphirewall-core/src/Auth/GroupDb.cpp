/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <time.h>
#include <string>
#include <map>
#include <cstring>
#include <vector>

using namespace std;
using std::string;

#include "Core/System.h"
#include "Auth/User.h"
#include "Auth/UserDb.h"
#include "Auth/Group.h"

#include "Utils/Utils.h"
#include "Auth/GroupDb.h"

void GroupDb::loadDb() {
	/*Ensure group map is empty first*/
	groups.clear();

	if (config->has("groupDb")) {
		ObjectContainer* root = config->getElement("groupDb");
		ObjectContainer* groups = root->get("groups")->container();
		for (int x = 0; x < groups->size(); x++) {
			ObjectContainer* jsonGroup = groups->get(x)->container();

			Group* group = new Group();
			group->m_name = jsonGroup->get("name")->string();
			group->m_id = jsonGroup->get("id")->number();
			group->m_createTime = jsonGroup->get("createTime")->number();
			group->m_manager = jsonGroup->get("manager")->string();
			group->allow_mui = jsonGroup->get("allowMui")->boolean();
			group->m_desc = jsonGroup->get("desc")->string();

			QuotaInfo* quota = group->getQuota();
			quota->dailyQuota = jsonGroup->get("dailyQuota")->boolean();
			quota->dailyQuotaLimit = jsonGroup->get("dailyQuotaLimit")->number();

			quota->weeklyQuota= jsonGroup->get("weeklyQuota")->boolean();
			quota->weeklyQuotaLimit = jsonGroup->get("weeklyQuotaLimit")->number();

			quota->monthQuota = jsonGroup->get("monthQuota")->boolean();
			quota->monthQuotaIsSmart = jsonGroup->get("monthQuotaIsSmart")->number();
			quota->monthQuotaLimit = jsonGroup->get("monthQuotaLimit")->number();
			quota->monthQuotaBillingDay = jsonGroup->get("monthQuotaBillingDay")->number();

			quota->totalQuota = jsonGroup->get("totalQuota")->boolean();
			quota->totalQuotaLimit = jsonGroup->get("totalQuotaLimit")->number();

			quota->timeQuota= jsonGroup->get("timeQuota")->boolean();
			quota->timeQuotaLimit = jsonGroup->get("timeQuotaLimit")->number();

			this->groups[group->m_id] = group;
		}
	} else {
		logger->log(SLogger::ERROR, "Group Database Failed to Load, no groups found. Creating default admin group");

		Group* systemAdminGroup = new Group();
		systemAdminGroup->allow_mui = true;
		systemAdminGroup->m_name = "defaultadmin";
		systemAdminGroup->m_desc = "Default Admin Group";
		systemAdminGroup->m_id = 1;
		groups[1] = systemAdminGroup;
	}

	Group* bGroup = new Group();
	bGroup->m_id = -1;
	bGroup->m_name = "any";
	groups[-1] = bGroup;

	save();
}

Group* GroupDb::getGroup(int group) {
	map<int, Group*>::iterator iter = groups.find(group);
	if (iter != groups.end()) {
		return iter->second;
	}
	return NULL;
}

Group* GroupDb::getGroup(string group) {
	map<int, Group*>::iterator iter = groups.begin();
	while (iter != groups.end()) {
		Group* g = iter->second;
		if (g->m_name.compare(group) == 0)
			return g;
		++iter;
	}
	return NULL;
}

void GroupDb::save() {
	lock();
	if(config){
		logger->log(SLogger::EVENT, "Saving Group Configuration File");

		ObjectContainer* root = new ObjectContainer(CREL);
		ObjectContainer* jsonGroups = new ObjectContainer(CARRAY);
		for (map<int, Group*>::iterator iter = groups.begin(); iter != groups.end(); iter++) {
			Group* group = iter->second;
			ObjectContainer* g = new ObjectContainer(CREL);
			g->put("id", new ObjectWrapper((double) group->m_id));
			g->put("name", new ObjectWrapper((string) group->m_name));
			g->put("createTime", new ObjectWrapper((double) group->m_createTime));
			g->put("manager", new ObjectWrapper((string) group->m_manager));
			g->put("allowMui", new ObjectWrapper((bool) group->allow_mui));
			g->put("desc", new ObjectWrapper((string) group->m_desc));

			QuotaInfo* quotas = group->getQuota();
			g->put("dailyQuota", new ObjectWrapper((bool) quotas->dailyQuota));
			g->put("dailyQuotaLimit", new ObjectWrapper((double) quotas->dailyQuotaLimit));

			g->put("weeklyQuota", new ObjectWrapper((bool) quotas->weeklyQuota));
			g->put("weeklyQuotaLimit", new ObjectWrapper((double) quotas->weeklyQuotaLimit));

			g->put("monthQuota", new ObjectWrapper((bool) quotas->monthQuota));
			g->put("monthQuotaIsSmart", new ObjectWrapper((bool) quotas->monthQuotaIsSmart));
			g->put("monthQuotaLimit", new ObjectWrapper((double) quotas->monthQuotaLimit));
			g->put("monthQuotaBillingDay", new ObjectWrapper((double) quotas->monthQuotaBillingDay));

			g->put("totalQuota", new ObjectWrapper((bool) quotas->totalQuota));
			g->put("totalQuotaLimit", new ObjectWrapper((double) quotas->totalQuotaLimit));

			g->put("timeQuota", new ObjectWrapper((bool) quotas->timeQuota));
			g->put("timeQuotaLimit", new ObjectWrapper((double) quotas->timeQuotaLimit));

			jsonGroups->put(new ObjectWrapper(g));
		}

		root->put("groups", new ObjectWrapper(jsonGroups));
		config->setElement("groupDb", root);
		config->save();
	}
	unlock();
}

Group* GroupDb::createGroup(std::string name){
	Group* group = new Group();
	group->m_name = name;
	group->m_id = findUniqueId();
	group->m_createTime = time(NULL);
	groups[group->m_id] = group;
	
	return group; 
}

int GroupDb::findUniqueId(){
	for(int x= 0; x < 102500; x++){
		if(!getGroup(x)){
			return x;
		}
	}	

	return 0;
}

bool GroupDb::delGroup(Group* group) {
	for(std::list<GroupRemovedListener*>::iterator iter = removeListeners.begin(); 
		iter != removeListeners.end(); 
		iter++){
		
		(*iter)->groupRemoved(group);
	}	
	

	map<int, Group*>::iterator iter = groups.find(group->getId());
	if (iter != groups.end()) {
		groups.erase(iter);
	}

	save();

	delete group;
	return true;
}

vector<Group*> GroupDb::list() {
	vector<Group*> ret;
	for (map<int, Group*>::iterator iter = groups.begin(); iter != groups.end(); iter++){
		ret.push_back(iter->second);
	}

	return ret;
}

void GroupDb::lock(){
	holdLock();
}
void GroupDb::unlock(){
	releaseLock();
}

void GroupDb::registerRemovedListener(GroupRemovedListener* listener){
	removeListeners.push_back(listener);
}

