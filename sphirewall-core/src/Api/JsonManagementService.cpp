/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <boost/regex.hpp>
#include <sstream>

#include "Api/JsonManagementService.h"
#include "Json/JSONValue.h"
#include "Json/JSON.h"
#include "Core/Config.h"
#include "Core/SysMonitor.h"
#include "Utils/StringUtils.h"
#include "BandwidthDb/AnalyticsClient.h"
#include "Core/System.h"
#include "Core/Event.h"
#include "Auth/User.h"
#include "Auth/AuthenticationHandler.h"

using namespace std;

JsonManagementService::JsonManagementService() {
	ignoreAuth = false;
	lock = new Lock();
	config = NULL;
}

void JsonManagementService::initDelegate() {
	GeneralDelegate* generalDelegate = new GeneralDelegate(config, sysMonitor, loggingConfiguration, authenticationManager);	
	generalDelegate->setEventDb(eventDb);
	registeredDelegates.push_back(generalDelegate);
	registeredDelegates.push_back(new EventDelegate(eventDb));

	IdsDelegate* idsDel = new IdsDelegate(ids);
	idsDel->setEventDb(eventDb);	
	registeredDelegates.push_back(idsDel);

	NetworkSettingsDelegate* net = new NetworkSettingsDelegate(interfaceManager, dnsConfig, arpTable, dhcpConfigurationManager);
	net->setConnectionManager(connectionManager);
	net->setEventDb(eventDb);
	net->setOpenvpnManager(openvpn);
	registeredDelegates.push_back(net);

	FirewallDelegate* fwall = new FirewallDelegate(firewall, interfaceManager, groupDb);
	fwall->setEventDb(eventDb);	
	registeredDelegates.push_back(fwall);
	
	AuthDelegate* authDelegate = new AuthDelegate(groupDb, userDb, firewall, sessionDb, arpTable, authenticationManager);
	authDelegate->setEventDb(eventDb);
	registeredDelegates.push_back(authDelegate);
	registeredDelegates.push_back(new AnalyticsDelegate(new AnalyticsClient()));

	//Register safe urls
	requestsWithoutAuth.insert("auth/login");
	requestsWithoutAuth.insert("auth/logout");
	requestsWithoutAuth.insert("general/version");
	requestsWithoutAuth.insert("auth/users/list");
	requestsWithoutAuth.insert("general/cloud/connected");
	requestsWithoutAuth.insert("firewall/rewrite/remove");
}

bool Delegate::matches(string uri) {
	boost::regex r(rexpression());
	boost::smatch what;
	if (boost::regex_match(uri, what, r, boost::match_extra)) {
		return true;
	}

	return false;
}

std::string JsonManagementService::handleAuthentication(JSONObject& root){
	if (authenticationManager->authenticate(root[L"username"]->String(), root[L"password"]->String())) {
		User* user = userDb->getUser(root[L"username"]->String());
		if (user && user->isAllowedMuiAccess()) {
			EventParams params; params["user"] = root[L"username"]->String();
			eventDb->add(new Event(USERDB_LOGIN_SUCCESS, params));

			string token = createSession(user);
			stringstream ss;
			ss << "{\"code\":0,\"message\":\"\",\"token\":\"" + token + "\"}";
			return ss.str();
		} else {
			EventParams params; params["user"] = root[L"username"]->String();
			eventDb->add(new Event(USERDB_LOGIN_FAILED, params));
			
			throw new AuthenticationException("user has not been granted access to the management api");
		}

	} else {
		EventParams params; params["user"] = root[L"username"]->String();
		eventDb->add(new Event(USERDB_LOGIN_FAILED, params));
		throw new AuthenticationException("invalid username or password");
	}
}

bool JsonManagementService::noAuthRequired(std::string requestUri){
	return requestsWithoutAuth.find(requestUri) != requestsWithoutAuth.end();
}

std::string JsonManagementService::handleRequest(std::string requestUri, JSONObject& root){
	std::string token;
	if (root.has(L"token")) {
		token = root[L"token"]->String();
	}

	lock->lock();
	ApiSession* session = getSession(token);
	lock->unlock();
	if (session || noAuthRequired(requestUri) || noSessionAuth(requestUri, token) || ignoreAuth) {
		list<Delegate*>::iterator iter;
		for (iter = registeredDelegates.begin();
				iter != registeredDelegates.end();
				iter++) {

			Delegate* delegate = (*iter);
			if (delegate->matches(requestUri)) {
				JSONObject responseRoot;
				responseRoot.put(L"code", new JSONValue((double) 0));

				JSONObject args = root[L"args"]->AsObject();
				JSONObject data = delegate->process(requestUri, args);
				responseRoot.put(L"response", new JSONValue(data));
				JSONValue *responseValue = new JSONValue(responseRoot);

				std::string sMsg = WStringToString(responseValue->Stringify());
				delete responseValue;
				return sMsg;
			}
		}

		throw new DelegateNotFoundException("deletegate not found");
	} else {
		throw new AuthenticationException("authentication token expired or invalid");	
	}

}

string JsonManagementService::process(std::string input) {
	JSONValue* value = JSON::Parse(input.c_str());
	if (!value) {
		return "{'response':'error','code':'-1','message':'could not parse json request'}";
	}

	JSONObject root = value->AsObject();
	try {
		AuthenticationManager* authManager = authenticationManager;
		std::string requestUri = WStringToString(root[L"request"]->AsString());
		if (requestUri.compare("auth") == 0) {
			lock->lock();
			std::string authResponse = handleAuthentication(root);
			lock->unlock();
			delete value;
			return authResponse;

		} else {
			std::string response = handleRequest(requestUri, root);
			delete value;
			return response;
		}

	} catch (JSONFieldNotFoundException* e) {
		delete value;
		stringstream ss;
		ss << "{\"code\":-1,\"message\":\"failed to pass request, missing field '" << e->what() << "'\"}";
		lock->unlock();
		return ss.str();
	} catch(AuthenticationException* e){
		delete value;
                stringstream ss;
		ss << "{\"code\":-2,\"message\":\"" << e->what() << "\"}";
		lock->unlock();
		return ss.str();
	} catch (DelegateNotFoundException* e) {
		delete value;
		lock->unlock();
		return "{\"code\":-1,\"message\":\"could not find delegate\"}";
	} catch (ServiceNotAvailableException* e) {
		delete value;
		lock->unlock();
		return "{\"code\":-3,\"message\":\"service not available\"}";
	} catch (DelegateGeneralException* e) {
		delete value;
		stringstream ss;
		ss << "{\"code\":-1,\"message\":\"" << e->what() << "\"}";
		lock->unlock();
		return ss.str();
	}
}

void JsonManagementService::handleDeleteUser(User* user){
	list<ApiSession*>::iterator iter;
	for(iter = sessions.begin(); iter != sessions.end(); iter++){
		ApiSession* target = (*iter);
		if(target->user == user){
			delete target;	
			iter = sessions.erase(iter);
		}
	}	

}

ApiSession* JsonManagementService::getSession(std::string token){
	for(list<ApiSession*>::iterator iter = sessions.begin();
			iter != sessions.end();
			iter++){

		//First check the session is valid:
		ApiSession* target = (*iter);
		if((time(NULL) - target->lastSeenTime) > System::getInstance()->configurationManager.get("general:session_timeout")->number()){
			EventParams params; params["user"] = target->user->getUserName();
			eventDb->add(new Event(USERDB_SESSION_TIMEOUT, params));	
			delete target;
			iter = sessions.erase(iter);
			continue;	
		}else{
			if(target->uuid.compare(token) == 0){
				target->lastSeenTime = time(NULL);
				return target;
			}
		}
	}

	return NULL;
}

std::string JsonManagementService::createSession(User* user){
	string uuid = StringUtils::genRandom();
	ApiSession* session = new ApiSession(user, uuid);
	sessions.push_back(session);
	return uuid;
}

bool JsonManagementService::noSessionAuth(string requesturi, string token){
	if(!config || !config->getConfigurationManager()){
		return false;
	}

	if(config->getConfigurationManager()->has("noSessionAuth")){
		ObjectContainer* tokens = config->getConfigurationManager()->getElement("noSessionAuth")->get("tokens")->container();
		for(int x= 0; x < tokens->size(); x++){
			ObjectContainer* target = tokens->get(x)->container();
			if(target->get("token")->string().compare(token) == 0){
				ObjectContainer* allowedUris = target->get("allowedUris")->container();
				for(int y = 0; y < allowedUris->size(); y++){
					if(allowedUris->get(y)->string().compare(requesturi) == 0){
						return true;
					}
				}	
			}
		}
	}
	
	return false;
}
