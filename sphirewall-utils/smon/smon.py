# This file is part of Sphirewall, it comes with a copy of the license in LICENSE.txt, which is also available at http://sphirewall.net/LICENSE.txt 

# smon.py
# A daemon for monitoring and restoring sphirewall on a machine

from socket import socket, SOCK_STREAM, AF_INET
import sys
from time import sleep
import subprocess
import syslog
import json

def usage():
    print "Usage:"
    print "./smon.py <hostname> <port>"
    print "./smon.py <hostname> <port> -r"
    print "\nPassing -r option will attempt to restart sphirewall when is fails";


def poll(hostname, port, reconnect = 0):
    try:
        s = socket(AF_INET, SOCK_STREAM);
        s.connect((hostname, int(port)));

        req = {'request':'version','args':[]};
        s.sendall(json.dumps(req) + "\n")

        syslog.syslog(syslog.LOG_DAEMON, 'could connected to sphirewall daemon')

    except:
        syslog.syslog(syslog.LOG_ERR, 'could not connect to sphirewall daemon')

        if(reconnect == 1):
            syslog.syslog(syslog.LOG_ERR, 'attempting to restart the sphirewall daemon')
            subprocess.call("rm /var/lock/sphirewall.lock", shell=True)
            subprocess.call("sphirewalld start", shell=True)

if __name__ == "__main__":
    print "\nsmon.py - A simple monitoring daemon for Sphirewall"
    print "+ starting smon daemon, logs sent to syslog";

    if len(sys.argv) == 3:
        while [1]:
            poll(sys.argv[1], sys.argv[2]);
            sleep(2)
    elif len(sys.argv) == 4:
        while [1]:
            poll(sys.argv[1], sys.argv[2], sys.argv[3] == "-r")
            sleep(2)
    else:
        usage()
