#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester

class RoutesTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login("fw", "admin", "admin")

    def gotoNetworking(self):
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Networking')]").click()

    def test_Options(self):
        self.gotoNetworking()
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'eth1')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Dhcp')]").click()

        self.tester.select("enabled")
        self.tester.driver.find_element_by_name("submit").click()
        self.tester.setValue("startIpAddress","10.1.1.10")
        self.tester.setValue("finishIpAddress","10.1.1.15")
        self.tester.setValue("domainNameService", "8.8.8.8")
        self.tester.setValue("domainName","sphirewall.net")
        self.tester.setValue("defaultRouter","10.1.1.1")
        self.tester.driver.find_element_by_name("submit").click()

        self.assertEquals("10.1.1.10", self.tester.driver.find_element_by_name("startIpAddress").get_attribute("value"))
        self.assertEquals("10.1.1.15",self.tester.driver.find_element_by_name("finishIpAddress").get_attribute("value"))
        self.assertEquals("8.8.8.8",self.tester.driver.find_element_by_name("domainNameService").get_attribute("value"))
        self.assertEquals("sphirewall.net",self.tester.driver.find_element_by_name("domainName").get_attribute("value"))
        self.assertEquals("10.1.1.1",self.tester.driver.find_element_by_name("defaultRouter").get_attribute("value"))

    def test_addStaticLease(self):
        self.gotoNetworking()
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td/a[contains(text(), 'eth1')]").click()
        self.tester.driver.find_element_by_xpath("//a[contains(text(), 'Dhcp')]").click()

        self.tester.driver.find_element_by_name("hostname").send_keys("pc1.local")
        self.tester.driver.find_element_by_xpath("//input[@type='text'][@name='ip']").send_keys("10.1.1.1")
        self.tester.driver.find_element_by_id("hw").send_keys("aa:bb:cc:dd:ee:ff")
        self.tester.driver.find_element_by_id("add-staticlease").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(text(),'pc1.local')]"
            "//ancestor::tr/td[contains(text(), '10.1.1.1')]"
            "//ancestor::tr/td[contains(text(), 'aa:bb:cc:dd:ee:ff')]"))

        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(text(),'pc1.local')]"
            "//ancestor::tr/td/a[contains(text(), 'Delete')]").click()

        self.assertFalse(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(text(),'pc1.local')]"
            "//ancestor::tr/td[contains(text(), '10.1.1.1')]"
            "//ancestor::tr/td[contains(text(), 'aa:bb:cc:dd:ee:ff')]"))

    def tearDown(self):
        self.tester.disconnect()
