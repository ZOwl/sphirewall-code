/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include <gtest/gtest.h>
#include "Json/JSON.h"
#include "Api/JsonManagementService.h"
#include "Auth/User.h"
#include "Auth/Group.h"
#include "Auth/GroupDb.h"
#include "Core/ConfigurationManager.h"
#include "Core/HostDiscoveryService.h"
#include "Auth/AuthenticationHandler.h"
#include "test.h"

using namespace std;

TEST(AuthDelegate, base) {
	EXPECT_TRUE(true);
}

TEST(AuthDelegate, auth_groups_list) {
	GroupDb* db = new GroupDb(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager());
	AuthDelegate* del = new AuthDelegate(db, NULL, NULL, NULL, NULL, NULL);

	Group* g1 = db->createGroup("group1"); 
	g1->setDesc("desc");
	g1->setManager("manager");
	g1->setAllowMui(true);

	Group* g2 = db->createGroup("anothergroup"); 
	g2->setDesc("desc");
	g2->setManager("manager");
	g2->setAllowMui(true);

	JSONObject ret = del->process("auth/groups/list", JSONObject());
	JSONArray arr = ret[L"groups"]->AsArray();

	EXPECT_TRUE(arr.size() == 2);
	JSONObject group1Json = arr[0]->AsObject();

	EXPECT_TRUE(group1Json[L"name"]->String().compare("anothergroup") == 0 || group1Json[L"name"]->String().compare("group1") == 0);
	EXPECT_TRUE(group1Json[L"desc"]->String().compare("desc") == 0);
	EXPECT_TRUE(group1Json[L"manager"]->String().compare("manager") == 0);
	EXPECT_TRUE(group1Json[L"allowMui"]->AsBool() == true);
}

TEST(AuthDelegate, auth_groups_create) {
	GroupDb* db = new GroupDb(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager());
	AuthDelegate* del = new AuthDelegate(db, NULL, NULL, NULL, NULL, NULL);

	JSONObject g;
	g.put(L"name", new JSONValue((string) "group1"));
	JSONObject ret = del->process("auth/groups/create", g);

	EXPECT_TRUE(db->list().size() == 1);
	Group* target = db->list()[0];
	EXPECT_TRUE(target != NULL);
	EXPECT_TRUE(target->getName().compare("group1") == 0);
}

TEST(AuthDelegate, auth_groups_get) {
	GroupDb* db = new GroupDb(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager());
	AuthDelegate* del = new AuthDelegate(db, NULL, NULL, NULL, NULL, NULL);

	Group* g1 = db->createGroup("group1"); 
	g1->setDesc("desc");
	g1->setManager("manager");
	g1->setAllowMui(true);

	int id = db->getGroup("group1")->getId();
	JSONObject g;
	g.put(L"id", new JSONValue((double) id));
	JSONObject ret = del->process("auth/groups/get", g);

	EXPECT_TRUE(ret[L"name"]->String().compare("group1") == 0);
	EXPECT_TRUE(ret[L"desc"]->String().compare("desc") == 0);
	EXPECT_TRUE(ret[L"manager"]->String().compare("manager") == 0);
	EXPECT_TRUE(ret[L"allowMui"]->AsBool() == true);
}

TEST(AuthDelegate, auth_groups_del) {
	GroupDb* db = new GroupDb(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager());
	AuthDelegate* del = new AuthDelegate(db, NULL, NULL, NULL, NULL, NULL);

	Group* g1 = db->createGroup("group1"); 
	EXPECT_TRUE(db->list().size() == 1);

	int id = db->getGroup("group1")->getId();
	JSONObject g;
	g.put(L"id", new JSONValue((double) id));
	JSONObject ret = del->process("auth/groups/del", g);

	EXPECT_TRUE(db->list().size() == 0);
}

TEST(AuthDelegate, auth_users_list) {
	GroupDb* groupDb = new GroupDb(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager());
	UserDb* userDb = new UserDb(NULL, groupDb, new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager(), NULL);
	AuthDelegate* del = new AuthDelegate(groupDb, userDb, NULL, NULL, NULL, NULL);

	userDb->createUser("user1");
	userDb->createUser("user2");

	JSONObject ret = del->process("auth/users/list", JSONObject());
	JSONArray users = ret[L"users"]->AsArray();
	EXPECT_TRUE(users.size() == 2);
}

TEST(AuthDelegate, auth_user_add) {
	GroupDb* groupDb = new GroupDb(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager());
	UserDb* userDb = new UserDb(NULL, groupDb, new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager(), NULL);
	AuthDelegate* del = new AuthDelegate(groupDb, userDb, NULL, NULL, NULL, NULL);

	EXPECT_TRUE(userDb->list().size() == 0);

	JSONObject u;
	u.put(L"username", new JSONValue((string) "user1"));
	JSONObject ret = del->process("auth/users/add", u);

	EXPECT_TRUE(userDb->list().size() == 1);
}

TEST(AuthDelegate, auth_user_del) {
	SessionDb* sessionDb = new SessionDb(NULL, NULL, NULL);
	GroupDb* groupDb = new GroupDb(new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager());
	UserDb* userDb = new UserDb(NULL, groupDb, new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing"), new ConfigurationManager(), NULL);
	AuthDelegate* del = new AuthDelegate(groupDb, userDb, NULL, NULL, NULL, NULL);

	userDb->createUser("user1");
	EXPECT_TRUE(userDb->list().size() == 1);

	JSONObject u;
	u.put(L"username", new JSONValue((string) "user1"));
	JSONObject ret = del->process("auth/users/del", u);

	EXPECT_TRUE(userDb->list().size() == 0);
}

TEST(AuthDelegate, auth_login) {
	SLogger::LogContext* logContext = new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing");
	SLogger::Logger* logger = new SLogger::Logger();

	SessionDb* sessionDb = new SessionDb(logContext, NULL, NULL);
	GroupDb* groupDb = new GroupDb(logContext, new ConfigurationManager());
	UserDb* userDb = new UserDb(NULL, groupDb, logContext, new ConfigurationManager(), sessionDb);
	HostDiscoveryService* arp = new HostDiscoveryService();
	arp->addIgnoreLocal("mac_address", IP4Addr::stringToIP4Addr("10.1.1.1"));

	AuthenticationManager* authManager = new AuthenticationManager(NULL);
	LocalDbAuthenticationMethod* method = new LocalDbAuthenticationMethod();
	method->setUserDb(userDb);
	authManager->addMethod(method);
	authManager->setLocalUserDb(userDb);
	
	AuthDelegate* del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp, authManager);

	User* user = userDb->createUser("michael");
	user->setPassword("1234");

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == 0);
	EXPECT_TRUE(sessionDb->get("mac_address", "10.1.1.1") != NULL);
}

TEST(AuthDelegate, auth_login_withoutpassword) {
	SLogger::LogContext* logContext = new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing");
	SLogger::Logger* logger = new SLogger::Logger();

	SessionDb* sessionDb = new SessionDb(logContext, NULL, NULL);
	GroupDb* groupDb = new GroupDb(logContext, new ConfigurationManager());
	UserDb* userDb = new UserDb(NULL, groupDb, logContext, new ConfigurationManager(), sessionDb);
        HostDiscoveryService* arp = new HostDiscoveryService();
	arp->addIgnoreLocal("mac_address", IP4Addr::stringToIP4Addr("10.1.1.1"));

	AuthenticationManager* authManager = new AuthenticationManager(NULL);
	LocalDbAuthenticationMethod* method = new LocalDbAuthenticationMethod();
	method->setUserDb(userDb);
	authManager->addMethod(method);
	authManager->setLocalUserDb(userDb);
	
	AuthDelegate* del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp, authManager);
	User* user = userDb->createUser("michael");
	
	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == 0);
	EXPECT_TRUE(sessionDb->get("mac_address", "10.1.1.1") != NULL);
}

TEST(AuthDelegate, auth_login_invalid_user) {
	SLogger::LogContext* logContext = new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing");
	SLogger::Logger* logger = new SLogger::Logger();

	SessionDb* sessionDb = new SessionDb(logContext, NULL, NULL);
	GroupDb* groupDb = new GroupDb(logContext, new ConfigurationManager());
	UserDb* userDb = new UserDb(NULL, groupDb, logContext, new ConfigurationManager(), sessionDb);
        HostDiscoveryService* arp = new HostDiscoveryService();

	arp->addIgnoreLocal("mac_address", IP4Addr::stringToIP4Addr("10.1.1.1"));
	
	AuthenticationManager* authManager = new AuthenticationManager(NULL);
	LocalDbAuthenticationMethod* method = new LocalDbAuthenticationMethod();
	method->setUserDb(userDb);
	authManager->addMethod(method);
	authManager->setLocalUserDb(userDb);
	
	AuthDelegate* del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp, authManager);

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == -1);
	EXPECT_TRUE(sessionDb->get("mac_address", "10.1.1.1") == NULL);
}

TEST(AuthDelegate, auth_login_invalid_password) {
	SLogger::LogContext* logContext = new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing");
	SLogger::Logger* logger = new SLogger::Logger();

	SessionDb* sessionDb = new SessionDb(logContext, NULL, NULL);
	GroupDb* groupDb = new GroupDb(logContext, new ConfigurationManager());
	UserDb* userDb = new UserDb(NULL, groupDb, logContext, new ConfigurationManager(), sessionDb);
        HostDiscoveryService* arp = new HostDiscoveryService();

	arp->addIgnoreLocal("mac_address", IP4Addr::stringToIP4Addr("10.1.1.1"));
	
	AuthenticationManager* authManager = new AuthenticationManager(NULL);
	LocalDbAuthenticationMethod* method = new LocalDbAuthenticationMethod();
	method->setUserDb(userDb);
	authManager->addMethod(method);
	authManager->setLocalUserDb(userDb);
	
	AuthDelegate* del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp, authManager);

	User* user = userDb->createUser("michael");
	user->setPassword("proper_password");

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == -1);
	EXPECT_TRUE(sessionDb->get("mac_address", "10.1.1.1") == NULL);
}

TEST(AuthDelegate, auth_login_invalid_macaddress) {
	SLogger::LogContext* logContext = new SLogger::LogContext(SLogger::CRITICAL_ERROR, "test", "/tmp/testing");
	SLogger::Logger* logger = new SLogger::Logger();

	SessionDb* sessionDb = new SessionDb(logContext, NULL, NULL);
	GroupDb* groupDb = new GroupDb(logContext, new ConfigurationManager());
	UserDb* userDb = new UserDb(NULL, groupDb, logContext, new ConfigurationManager(), sessionDb);
        HostDiscoveryService* arp = new HostDiscoveryService();
	
	AuthenticationManager* authManager = new AuthenticationManager(NULL);
	LocalDbAuthenticationMethod* method = new LocalDbAuthenticationMethod();
	method->setUserDb(userDb);
	authManager->addMethod(method);
	authManager->setLocalUserDb(userDb);
	AuthDelegate* del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp, authManager);

	User* user = userDb->createUser("michael");
	user->setPassword("1234");

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "1.1.1.2"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == -1);
	EXPECT_TRUE(sessionDb->list().size() == 0);
}


