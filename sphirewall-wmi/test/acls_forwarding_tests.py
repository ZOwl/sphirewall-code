#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from selenium_base import SeleniumTester

class FirewallAclsTestCase(unittest.TestCase):
    def setUp(self):
        self.tester = SeleniumTester()
        self.tester.connect()
        self.tester.login("fw", "admin", "admin")

    def gotoRules(self):
        self.clickFirewall()
        self.clickAcl()

    def clickAcl(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Forwarding')]").click()

    def clickFirewall(self):
        self.tester.driver.find_element_by_xpath("//a[contains(normalize-space(), 'Firewall')]").click()

    def test_Masquerade_Device(self):
        self.gotoRules()
        self.tester.clickOptions("Create Rule")

        self.tester.driver.find_element_by_name("source").send_keys("12.2.2.10")
        self.tester.driver.find_element_by_name("source_mask").send_keys("255.255.255.240")
        self.tester.driver.find_element_by_xpath("//select[@name='snat_device_target']/option[@value='lo']").click()

        self.tester.driver.find_element_by_xpath("//input[@value='MASQUERADE']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.10/255.255.255.240')]//ancestor::tr/td[contains(normalize-space(), 'Masquerade to lo')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.10/255.255.255.240')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()

    def test_Forwarding(self):
        self.gotoRules()
        self.tester.clickOptions("Create Rule")

        self.tester.driver.find_element_by_name("source").send_keys("12.2.2.10")
        self.tester.driver.find_element_by_name("source_mask").send_keys("255.255.255.240")
        self.tester.driver.find_element_by_name("dnat_target").send_keys("10.1.1.1")
        self.tester.driver.find_element_by_name("dnat_port").send_keys("80")

        self.tester.driver.find_element_by_xpath("//input[@value='FORWARD']").click()
        self.tester.driver.find_element_by_xpath("//input[@type='submit']").click()

        self.assertTrue(self.tester.check_exists_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.10/255.255.255.240')]//ancestor::tr/td[contains(normalize-space(), 'Forward to 10.1.1.1:80')]"))
        self.tester.driver.find_element_by_xpath(
            "//table//tr/td[contains(normalize-space(), 'host 12.2.2.10/255.255.255.240')]//ancestor::tr/td/a[contains(normalize-space(), 'Del')]").click()
