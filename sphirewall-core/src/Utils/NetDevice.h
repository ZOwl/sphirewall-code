/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_NETDEVICE_H_INCLUDED
#define SPHIREWALL_NETDEVICE_H_INCLUDED

#include "autoconf.h"
#include <map>

#include "NetworkUtils.h"

#ifdef HAVE_DUMBNET_H
	#include <dumbnet.h>
	#include <dumbnet/addr.h>
	#include <dumbnet/route.h>
#endif
#ifdef HAVE_DNET_H
	#include <dnet.h>
	#include <dnet/addr.h>
	#include <dnet/route.h>
#endif

#define MAX_DEVICES 255
#define UNSET_ADDRESS -1 

#include "Utils/NetworkChangeNotifier.h"

class EventDb;
class Lock;
class ConfigurationManager;
class Listener {
	public:
		virtual void event() = 0;
};

class NetDeviceAlias {
	public:
		NetDeviceAlias(){
			fromDb = false;
		}

		std::string ip;
		struct addr address;

		bool isIp(){
			return address.addr_type == 3 && fromDb;
		}

		bool fromDb;
};

enum NetDeviceType {
	UNKNOWN = -1,
	LOOPBACK = 0,
	ETHERNET = 1,
	TUNNEL = 2,
	ETHERNET_BRIDGE = 3
};

class NetDeviceConfiguration {
	public:
		std::string interface;
		std::string ip;
		std::string mask;
		std::string name;
		std::string gateway;

		bool dhcp;	
		bool bridge;	
		std::list<std::string> bridgeDevices;
		std::list<std::string> aliases;
};

class NetDevice {
	public:
		NetDevice();
		NetDevice(string device);
		~NetDevice();

		void setIp(string ip, string netmask);

		int getId() {
			return id;
		}

		string getInterface() {
			return m_interface;
		}

		virtual string getIp() const;
		virtual string getNetMask() const;
		virtual string getBroadCast() const;
		virtual string getMacAddress();
		virtual string getNetworkAddress() const;
		virtual string getPtpAddress() const;
		virtual string getLinkLayerAddress() const;
		unsigned int getIp_Int() const {
			return this->ip;
		}

		unsigned int getNetMask_Int() const {
			return this->netmask;
		}

		unsigned int getBroadCast_Int() const {
			return this->broadcast;
		}

		unsigned int getNetworkAddress_Int() const {
			return this->netaddress; 
		}

		unsigned int getPtpAddress_Int() const {
			return this->ptpAddress;
		}

		unsigned int getLinkLayerAddress_Int() const {
			return this->linkLayerAddress;
		}

		unsigned int getMtu(){
			return this->mtu;
		}

		virtual bool isUp();
		virtual void toggleUp();

		std::list<NetDeviceAlias> getAliases();
		void addAlias(NetDeviceAlias);
		void deleteAlias(NetDeviceAlias);
		NetDeviceAlias getAliasByAddress(std::string ip, std::string mask);

		void setId(int id) {
			this->id = id;
		}
		void flush();
		virtual NetDeviceType getType(){
			return type;	
		}

		void toggleDhcp(bool dhcp);
		bool isDhcp(){
			return this->dhcp;
		}

		unsigned int getIp_UnPersisted(){
			return ip_unpersisted;
		}

		unsigned int getNetmask_UnPersisted(){
			return netmask_unpersisted;
		}

		std::string getDeviceDescription();
		std::string getName(){
			return name;
		}

		void setName(std::string name){
			this->name = name;
		}

                void addBridgeDevice(std::string device);
                void removeBridgeDevice(std::string device);
		std::list<string>& getBridgeDevices() {
			return bridgeDevices;
		}

		void setType(NetDeviceType type){
			this->type = type;
		}
	private:
		intf_t* iPtr;
		struct intf_entry* getRawDevice(std::string name);

		int id;
		string m_interface;
		list<NetDeviceAlias> aliases;

		unsigned int ip_unpersisted;
		unsigned int netmask_unpersisted;

		unsigned int ip;
		unsigned int netmask;
		unsigned int broadcast;
		unsigned int netaddress;
		unsigned int ptpAddress;
		unsigned int linkLayerAddress;	
		unsigned int mtu;

		bool dhcp;	
		NetDeviceType type;
		std::string name;

		std::list<std::string> bridgeDevices;
};

class Route {
	public:
		std::string id;
		std::string dev;
		std::string dest;
		std::string gw;
};

class InterfaceManager {
	public:
		InterfaceManager();
		void load();
		void save();

		//Automatically discovered devices:
		NetDevice* get(int);
		NetDevice* get(string);
		int lookup_intf(const struct intf_entry *entry, void *arg);
		void notify();
		std::list<NetDevice*> listDevices(bool configured = false);
		void publish();
		bool isSynced();

		//Configured Stuff:
		std::list<Route*> listRoutes();
		void addRoute(Route*);
		void deleteRoute(Route*);
		Route* getRoute(std::string);

		NetDeviceConfiguration* addDevice(std::string string);
		void deleteDevice(std::string string);
		NetDeviceConfiguration* getConfiguredDevice(std::string ifname);
		std::list<NetDeviceConfiguration*> getConfiguredDevices() {
			return configuredDevices;
		}	
		//Pointless setters:
		void setConfig(ConfigurationManager* config){
			this->config = config;
		}

		void setEventDb(EventDb* eventDb){
			this->eventDb = eventDb;
		}

                void registerChangeListener(Listener* l){
                        registeredExternalListeners.push_back(l);
                }

                void pushDevice(int id, NetDevice* device){
                        devices[id] = device;
                }

                void disconnect();
	private:
		std::string generateConfiguration();
		Lock* lock;
		void listenForEvents();

		EventDb* eventDb;	
		ConfigurationManager* config;
		NetworkChangeNotifier notifier;
		list<Listener*> registeredExternalListeners;

		//Containers:
		NetDevice* devices[MAX_DEVICES];
		std::list<NetDeviceConfiguration*> configuredDevices;

		std::map<std::string, Route*> routes;
		list<NetDevice*> pendingBridges;
};

/*!	DNSConfig:
 * 
 * 	Class for managing dns client configuration.
 * 
 */
class DNSConfig {
	public:
		DNSConfig();

		string save();

		string getNS1() {
			return nameserver1;
		}

		string getNS2() {
			return nameserver2;
		}

		string getDomain() {
			return domain;
		}

		string getSearch() {
			return search;
		}

		void setNS1(string ns) {
			nameserver1 = ns;
		}

		void setNS2(string ns) {
			nameserver2 = ns;
		}

		void setDomain(string d) {
			domain = d;
		}

		void setSearch(string s) {
			search = s;
		}

	private:
		string nameserver1;
		string nameserver2;
		string domain;
		string search;

};

#endif

