#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import json
from string import replace
from flask.globals import request
from flask.templating import render_template
from werkzeug.utils import redirect
from sphirewallwmi import app
import service
from sphirewallwmi.api.utils import DateHelper

@app.route("/status/events")
def status_events():
    events = service.get_sphirewallapi().general().events_list()
    return render_template("status_events.html", events=events)


@app.route("/status/clients")
def status_clients():
    general__hosts_list = service.get_sphirewallapi().general().hosts_list()
    users_list = service.get_sphirewallapi().general().active_users_list()
    return render_template("status_clients.html", hosts=general__hosts_list, activeusers=users_list)


@app.route("/status/clients/disconnect/<address>")
def status_clients_disconnect(address):
    service.get_sphirewallapi().general().disconnect_session(address)
    return redirect("/status/clients")


@app.route("/status/connections")
def status_connections():
    connection_list = service.get_sphirewallapi().firewall().connections_list()
    return render_template("status_connections.html", connections=connection_list)


@app.route("/status/connections/terminate/<sourceIp>/<sourcePort>/<destIp>/<destPort>")
def status_connections_terminate(sourceIp, sourcePort, destIp, destPort):
    service.get_sphirewallapi().firewall().connection_terminate(sourceIp, sourcePort, destIp, destPort)
    return redirect("/status/connections")


@app.route("/status/reporting/address", methods=["POST", "GET"])
def status_reporting_address():
    reporting__address = service.get_sphirewallapi().reporting().address(processParams())
    return render_template("status_reporting_address.html",
        items=reporting__address,
        itemsJson=json.dumps(reporting__address), params=processParams(),
        hasResults=True if len(reporting__address) > 0 else False)


@app.route("/status/reporting")
def status_reporting():
    params = processParams()
    return render_template("status_reporting_summary.html", params=params,
        transfer=service.get_sphirewallapi().statistics_list("firewall.transfer.persec", params["startDate"],
            params["endDate"]), top=service.get_sphirewallapi().reporting().top(params),
        transferPoints=service.get_sphirewallapi().reporting().transfer(params=processParams()))

@app.route("/status/reporting/address/<address>")
def status_reporting_address_spec(address):
    uploadTotal = 0.00
    downloadTotal = 0.00
    transferPoints = service.get_sphirewallapi().reporting().transfer(address=address, params=processParams())
    for i in transferPoints:
        uploadTotal += (i["udpUp"] + i["tcpUp"])
        downloadTotal += (i["udpDown"] + i["tcpDown"])

    return render_template("status_reporting_address_spec.html", address=address,
        transferPoints=transferPoints,
        portPoints=service.get_sphirewallapi().reporting().port(address, params=processParams()),
        upload=safedivide(uploadTotal, (uploadTotal + downloadTotal)),
        download=safedivide(downloadTotal, (uploadTotal + downloadTotal)), params=processParams())


def safedivide(numerator, divisor):
    if divisor == 0:
        return 0
    return numerator / divisor


@app.route("/status/reporting/devices")
def status_reporting_device():
    devices = service.get_sphirewallapi().network().devices()
    deviceTotal = []
    for device in devices:
        line = service.get_sphirewallapi().reporting().devices(device["interface"], processParams())
        if len(line) is not 0:
            line["device"] = device["interface"]
            deviceTotal.append(line)

    return render_template("status_reporting_devices.html", deviceTotal=deviceTotal,
        jsonDeviceTotal=json.dumps(deviceTotal, processParams()), params=processParams())


@app.route("/status/reporting/devices/<device>")
def status_reporting_device_spec(device):
    deviceInfo = service.get_sphirewallapi().reporting().devices_spec(device, processParams())
    return render_template("status_reporting_devices_spec.html", device=device, deviceStats=deviceInfo,
        deviceStatsJson=json.dumps(deviceInfo), params=processParams())


@app.route("/status/reporting/websites")
def status_reporting_website():
    filterIp = None
    filterHw = None
    filterUser = None

    if "filter_sourceIp" in request.args:
        filterIp = request.args["filter_sourceIp"]
    if "filter_hw" in request.args:
        filterHw = request.args["filter_hw"]
    if "filter_user" in request.args:
        filterUser = request.args["filter_user"]

    websites = service.get_sphirewallapi().reporting().websites(processParams(), filterIp, filterHw, filterUser)
    return render_template("status_reporting_websites.html", filterIp=filterIp, filterUser=filterUser, filterHw=filterHw
        , websites=websites, websitesJson=json.dumps(websites),
        params=processParams())


@app.route("/status/reporting/users")
def status_reporting_users():
    return render_template("status_reporting_users.html",
        items=service.get_sphirewallapi().reporting().users(processParams()),
        itemsJson=json.dumps(service.get_sphirewallapi().reporting().users(processParams())), params=processParams())


@app.route("/status/reporting/users/<user>")
def status_reporting_users_spec(user):
    uploadTotal = 0.00
    downloadTotal = 0.00
    transferPoints = service.get_sphirewallapi().reporting().transfer(user=user, params=processParams())
    for i in transferPoints:
        uploadTotal += (i["udpUp"] + i["tcpUp"])
        downloadTotal += (i["udpDown"] + i["tcpDown"])

    return render_template("status_reporting_user_spec.html", user=user,
        transferPoints=transferPoints,
        portPoints=service.get_sphirewallapi().reporting().port(user=user, params=processParams()),
        upload=safedivide(uploadTotal, (uploadTotal + downloadTotal)),
        download=safedivide(downloadTotal, (uploadTotal + downloadTotal)), params=processParams())


@app.route("/status/reporting/hw/<hw>")
def status_reporting_hw_spec(hw):
    uploadTotal = 0.00
    downloadTotal = 0.00
    transferPoints = service.get_sphirewallapi().reporting().transfer(hw=hw, params=processParams())
    for i in transferPoints:
        uploadTotal += (i["udpUp"] + i["tcpUp"])
        downloadTotal += (i["udpDown"] + i["tcpDown"])

    return render_template("status_reporting_hw_spec.html", hw=hw,
        transferPoints=transferPoints,
        portPoints=service.get_sphirewallapi().reporting().port(hw=hw, params=processParams()),
        upload=safedivide(uploadTotal, (uploadTotal + downloadTotal)),
        download=safedivide(downloadTotal, (uploadTotal + downloadTotal)), params=processParams())


@app.route("/status/reporting/websites/<website>")
def status_reporting_websites_spec(website):
    return render_template("status_reporting_websites_spec.html", website=website,
        websiteStats=service.get_sphirewallapi().reporting().websites_spec(website, params=processParams()),
        params=processParams())


@app.route("/status/metrics")
def status_metrics():
    params = processParams()
    return render_template("status_metrics.html", params=params,
        transfer=service.get_sphirewallapi().statistics_list("firewall.transfer.persec", params["startDate"],
            params["endDate"]),
        connections=service.get_sphirewallapi().statistics_list("firewall.conntracker.tcp.size", params["startDate"],
            params["endDate"]),
        memory=service.get_sphirewallapi().statistics_list("system.vsize", params["startDate"], params["endDate"]),
        devices=service.get_sphirewallapi().statistics_list("firewall.arp.size", params["startDate"], params["endDate"])
        ,
        reportingConnected=service.get_sphirewallapi().reporting().connected(),
        idle=service.get_sphirewallapi().statistics_list("firewall.conntracker.icmp.size", params["startDate"],
            params["endDate"]))


@app.route("/status/metrics/all")
def status_metrics_all():
    params = processParams()
    available = service.get_sphirewallapi().statistics_list_available()
    arr = []
    for metric in available:
        item = {}
        item["key"] = replace(metric, ".", "-");
        item["data"] = service.get_sphirewallapi().statistics_list(metric, params["startDate"], params["endDate"])
        arr.append(item)

    print arr
    return render_template("status_metrics_all.html",
        reportingConnected=service.get_sphirewallapi().reporting().connected(), params=params,
        stats=arr)


@app.route("/status/logs", methods=["POST", "GET"])
def status_logs():
    filter = text_value("filter")
    if len(filter) > 0:
        general__logs = service.get_sphirewallapi().general().logs(filter=filter)
    else:
        general__logs = service.get_sphirewallapi().general().logs()

    return render_template("status_logs.html", logs=replace(general__logs, "\n", "<br>"), filter=filter)


def processParams():
    params = {}
    if "startDate" in request.args and len(request.args["startDate"]) > 0:
        params["startDate"] = request.args["startDate"]
    elif cookie_value("startDate") is not None:
        params["startDate"] = cookie_value("startDate")
    else:
        params["startDate"] = DateHelper.yesterday().isoformat()

    if "endDate" in request.args and len(request.args["endDate"]) > 0:
        params["endDate"] = request.args["endDate"]
    elif cookie_value("endDate") is not None:
        params["endDate"] = cookie_value("endDate")
    else:
        params["endDate"] = DateHelper.today().isoformat()

    if request.cookies.get("reportingmetric") is not None:
        metric = request.cookies.get("reportingmetric")
        params["metric"] = metric
    else:
        params["metric"] = "bytes"

    params["connected"] = service.get_sphirewallapi().reporting().connected()
    return params


def cookie_value(key):
    return request.cookies.get(key)


def text_value(key):
    if key in request.form:
        return request.form[key]
    if key in request.args:
        return request.args[key]
    return ""
