#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

class Reporting:
    connection = None

    def __init__(self, connection):
        self.connection = connection


    def address(self, params):
        params["limit"] = 10000
        try:
            return self.connection.request("analytics/stats/bandwidth/query/top/addresses", params)["addresses"]
        except:
            return []

    def transfer(self, address=None, user=None, hw=None, params=None):
        params["startTime"] = params["startDate"]
        params["endTime"] = params["endDate"]

        if address is not None:
            params["filter_sourceIp"] = address
        if user is not None:
            params["filter_user"] = user
        if hw is not None:
            params["filter_hw"] = hw

        try:
            return self.connection.request("analytics/stats/bandwidth/query/transfer", params)["items"]
        except:
            return []

    def port(self, address=None, user=None, params=None, hw=None):
        params["startTime"] = params["startDate"]
        params["endTime"] = params["endDate"]

        if address is not None:
            params["filter_sourceIp"] = address
        if user is not None:
            params["filter_user"] = user
        if hw is not None:
            params["filter_hw"] = hw

        try:
            return self.connection.request("analytics/stats/bandwidth/query/port/sum", params)["items"]
        except:
            return []

    def devices(self, device, params):
        params["startTime"] = params["startDate"]
        params["endTime"] = params["endDate"]
        params["device"] = device

        try:
            return self.connection.request("analytics/stats/bandwidth/query/device/sum", params)
        except:
            return []

    def devices_spec(self, device, params):
        params["startTime"] = params["startDate"]
        params["endTime"] = params["endDate"]
        params["device"] = device
        try:
            return self.connection.request("analytics/stats/bandwidth/query/device", params)["items"]
        except:
            return []

    def websites(self, params, filterIp=None, filterHw=None, filterUser=None):
        params["startTime"] = params["startDate"]
        params["endTime"] = params["endDate"]
        params["limit"] = 10000

        if filterIp is not None:
            params["filter_sourceIp"] = filterIp
        if filterHw is not None:
            params["filter_hw"] = filterHw
        if filterUser is not None:
            params["filter_user"] = filterUser

        try:
            return self.connection.request("analytics/stats/bandwidth/query/web", params)["items"]
        except:
            return []

    def users(self, params):
        params["limit"] = 10000

        try:
            return self.connection.request("analytics/stats/bandwidth/query/top/users", params)["users"]
        except:
            return []

    def connected(self):
        try:
            self.connection.request("analytics/version", None)
            return True
        except:
            return False

    def top(self, params):
        params["startTime"] = params["startDate"]
        params["endTime"] = params["endDate"]

        try:
            return self.connection.request("analytics/stats/bandwidth/top", params)
        except:
            return []

    def websites_spec(self, website, params):
        params["startTime"] = params["startDate"]
        params["endTime"] = params["endDate"]
        params["httpHost"] = website

        try:
            return self.connection.request("analytics/stats/bandwidth/query/web/address", params)["items"]
        except:
            return []
