/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sphirewall/Utils/FileUtils.h>
#include <fstream>
#include <ostream>
#include <sstream>
#include <stdlib.h>

#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Core/Dhcp3Configuration.h"
#include "Core/System.h"
#include "Api/JsonManagementService.h"
#include "Utils/NetDevice.h"

using namespace std;

Dhcp3ServiceInstance* Dhcp3ConfigurationManager::getOrCreate(std::string interface){
	for(list<Dhcp3ServiceInstance*>::iterator iter = instances.begin();
			iter != instances.end();
			iter++){
		Dhcp3ServiceInstance* instance = (*iter);
		if(instance->interface.compare(interface) == 0){
			return instance;
		}
	}	

	Dhcp3ServiceInstance* instance = new Dhcp3ServiceInstance();
	instance->interface = interface;
	instances.push_back(instance);
	return instance;
}

void Dhcp3ConfigurationManager::initalise(){
	load();
	if(active){
		start();
	}	
}

void Dhcp3ConfigurationManager::destroy(){
	if(running()){
		stop();
		active = true;
		save();
	}
}


void Dhcp3ConfigurationManager::load() {
	if(config->has("dhcp")){
		ObjectContainer* root = config->getElement("dhcp");
		ObjectContainer* instanceArray = root->get("instances")->container();
		for(int x = 0; x < instanceArray->size(); x++){
			Dhcp3ServiceInstance* instance = new Dhcp3ServiceInstance();		

			ObjectContainer* o = instanceArray->get(x)->container();	
			instance->startIpAddress = o->get("startIpAddress")->string();
			instance->finishIpAddress = o->get("finishIpAddress")->string();
			instance->defaultRouter = o->get("defaultRouter")->string();
			instance->domainNameService = o->get("domainNameService")->string();
			instance->domainName = o->get("domainName")->string();
			instance->interface = o->get("interface")->string();		
			instance->enabled= o->get("enabled")->boolean();

			ObjectContainer* a = o->get("static")->container();
			for(int x=0;x < a->size(); x++){
				ObjectContainer* target = a->get(x)->container();
				instance->addStaticLease(target->get("hostname")->string(),target->get("ip")->string(), target->get("hw")->string());
			}
			
			instances.push_back(instance);
		}
	}
}

void Dhcp3ConfigurationManager::save() {
	ObjectContainer* root = new ObjectContainer(CREL);
	ObjectContainer* instanceArray = new ObjectContainer(CARRAY);
	
	for(list<Dhcp3ServiceInstance*>::iterator iter = instances.begin();
			iter != instances.end();
			iter++){

		Dhcp3ServiceInstance* instance = (*iter);
		ObjectContainer* target = new ObjectContainer(CREL);
		target->put("startIpAddress", new ObjectWrapper((string) instance->startIpAddress));
		target->put("finishIpAddress", new ObjectWrapper((string) instance->finishIpAddress));
		target->put("defaultRouter", new ObjectWrapper((string) instance->defaultRouter));
		target->put("domainNameService", new ObjectWrapper((string) instance->domainNameService));
		target->put("domainName", new ObjectWrapper((string) instance->domainName));
		target->put("interface", new ObjectWrapper((string)instance->interface));
		target->put("enabled", new ObjectWrapper((bool)instance->enabled));

		ObjectContainer* arr = new ObjectContainer(CARRAY);
		list<StaticLease*>::iterator siter;
		for(siter = instance->staticLeases.begin();
				siter != instance->staticLeases.end();
				siter++){
			StaticLease* lease = (*siter);
			
			ObjectContainer* i = new ObjectContainer(CREL);
			i->put("hostname", new ObjectWrapper((string) lease->hostname));
			i->put("ip", new ObjectWrapper((string) lease->ip));
			i->put("hw", new ObjectWrapper((string) lease->hw));

			arr->put(new ObjectWrapper(i));
		}

		target->put("static", new ObjectWrapper(arr));
		instanceArray->put(new ObjectWrapper(target));
	}

	root->put("instances", new ObjectWrapper(instanceArray));
	config->setElement("dhcp", root);
	config->save();

}

string Dhcp3ConfigurationManager::generateConfigurationString() {
	stringstream ss;

	for(list<Dhcp3ServiceInstance*>::iterator iter = instances.begin();
			iter != instances.end();
			iter++){
                Dhcp3ServiceInstance* instance = (*iter);
		if(!instance->enabled){
			continue;
		}

		NetDevice* device = interfaceManager->get(instance->interface);	
		if(device){
			ss << "log-facility local0;" << endl;
			ss << "subnet " << device->getNetworkAddress() << " netmask " << device->getNetMask() << endl;
			ss << "{" << endl;
			ss << "range " << instance->startIpAddress << " " << instance->finishIpAddress << ";" << endl;

			if(instance->defaultRouter.size() > 0){
				ss << "option routers " << instance->defaultRouter << ";" << endl;
			}

			if(instance->domainNameService.size() > 0){
				ss << "option domain-name-servers " << instance->domainNameService << ";" << endl;
			}

			if(instance->domainName.size() > 0){
				ss << "option domain-name " << instance->domainName << ";" << endl;
			}

			list<StaticLease*>::iterator iter;
			for(iter = instance->staticLeases.begin();
					iter != instance->staticLeases.end();
					iter++){

				StaticLease* lease = (*iter);
				ss << "host " << lease->hostname << "{\n";
				ss << "hardware ethernet " << lease->hw << ";\n";
				ss << "fixed-address " << lease->ip << ";\n";
				ss << "}\n";
			}
			ss << "} " << endl;
		}
	}
	return ss.str();
}

void Dhcp3ConfigurationManager::publishConfigurationToServer(){
	string config = generateConfigurationString();
	string filename = "/etc/dhcp/dhcpd.conf";
	if(FileUtils::checkExists(filename)){
		ofstream configFile;
		configFile.open(filename.c_str());
		configFile << config;
	}

	stop();
	start();
}

void Dhcp3ConfigurationManager::start(){
	std::string interface;
	for(list<Dhcp3ServiceInstance*>::iterator iter = instances.begin();
			iter != instances.end();
			iter++){
		Dhcp3ServiceInstance* instance = (*iter);
		if(!instance->enabled){
			continue;
		}
		
		interface += " " + instance->interface;
	}	

	active = true;
	stringstream ss; ss << "/bin/bash -c 'start-stop-daemon --start --quiet --pidfile /var/run/dhcpd.pid --exec /usr/sbin/dhcpd -q " << interface << "'";
	System::execWithNoFds(ss.str());
	sleep(3);
	save();
}

void Dhcp3ConfigurationManager::stop(){
	active = false;
	stringstream ss;
	ss << "/bin/bash -c 'start-stop-daemon --stop --quiet --pidfile /var/run/dhcpd.pid'";
	System::execWithNoFds(ss.str());
	sleep(3);
	system("rm -Rf /var/run/dhcpd.pid");	
	save();
}

bool Dhcp3ConfigurationManager::running() {
	return FileUtils::checkExists("/var/run/dhcpd.pid");
}

void Dhcp3ServiceInstance::addStaticLease(std::string hostname, std::string ipaddress, std::string mac){
	StaticLease* lease = new StaticLease();
	lease->hostname = hostname;
	lease->ip = ipaddress;
	lease->hw = mac;

	staticLeases.push_back(lease);
}

void Dhcp3ServiceInstance::deleteStaticLease(std::string hostname, std::string ipaddress, std::string mac){
	list<StaticLease*>::iterator iter;
	for(iter = staticLeases.begin();
			iter != staticLeases.end();
			iter++){

		StaticLease* lease = (*iter);
		if(lease->hostname.compare(hostname) == 0){
			staticLeases.remove(lease);
			delete lease;
			return;
		}
	}
}
