/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream> 
#include <sstream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <stdarg.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <ctype.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <linux/icmp.h>
#include <boost/regex.hpp>

using namespace std;

#include "Utils/IP4Addr.h"
#include "SFwallCore/ConnTracker.h"
#include "SFwallCore/Packet.h"
#include "Utils/Utils.h"
#include "Auth/Session.h"
#include "Core/System.h"
#include "SFwallCore/Utils.h"
#include "Utils/NetDevice.h"
#include "SFwallCore/Firewall.h"

#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#define SERVER_NAME_LEN 256
#define TLS_HEADER_LEN 5
#define TLS_HANDSHAKE_CONTENT_TYPE 0x16
#define TLS_HANDSHAKE_TYPE_CLIENT_HELLO 0x01

static int HASH_MAX = 200000;
void SFwallCore::Packet::setData(std::string d) {
	data = d;
	fakeData = true;
	payloadlen = d.size();	
}

std::string SFwallCore::Packet::getData() {
	if(!fakeData){
		return (const char*) getCData(); 
	}

	return data;
}

unsigned char* SFwallCore::Packet::getCData(){
	if(fakeData){
		return (unsigned char*) data.c_str();
	}

	if(getProtocol() == TCP){
		unsigned char* packet = sqp->raw_packet;	

		struct iphdr* packet_header;
		packet_header = (struct iphdr*) packet;

		struct tcphdr* packet_tcp_header;
		packet_tcp_header = (struct tcphdr *) (packet + (packet_header->ihl << 2));

		int position = (packet_header->ihl << 2) + packet_tcp_header->doff * 4;
		return packet + position;
	}
	return NULL;
}

std::string SFwallCore::Packet::toString() const {
	return "unknown packet";
}

in_addr_t SFwallCore::Packet::getSrcIp() const {
	struct ip* ip = (struct ip*) sqp->raw_packet;
	return ntohl(ip->ip_src.s_addr);
}

in_addr_t SFwallCore::Packet::getDstIp() const {
	struct ip* ip = (struct ip*) sqp->raw_packet;
	return ntohl(ip->ip_dst.s_addr);
}

int SFwallCore::Packet::getLen() const {
	return len;
}

int SFwallCore::Packet::getHeaderLen() const {
	return headerlen;
}

int SFwallCore::Packet::getPayloadLen() const {
	return payloadlen;
}

int SFwallCore::Packet::getCheckSum() const {
	struct iphdr* packet_header = (struct iphdr*) sqp->raw_packet;
	return packet_header->check;
}

NetDevice* SFwallCore::Packet::getSourceNetDevice() {
	return System::getInstance()->getInterfaces()->get(getSourceDev());
}

NetDevice* SFwallCore::Packet::getDestNetDevice() {
	return System::getInstance()->getInterfaces()->get(getDestDev());
}


int SFwallCore::Packet::getSourceDev() {
	return sqp->indev;
}

int SFwallCore::Packet::getDestDev() {
	return sqp->outdev;
}

User* SFwallCore::Packet::getUser(){
	if (sqp->hwlen == 6) {
		if(userPtr == NULL){
			Session* session = System::getInstance()->getSessionDb()->get(getHw(), IP4Addr::ip4AddrToString(getSrcIp()));
			if (session != NULL) {
				userPtr = session->getUser();
			} else {
				userPtr = NULL;
			}
		}
	}

	return userPtr;
}

std::string SFwallCore::Packet::getUsername() const {
	if (userPtr != NULL) {
		return userPtr->getUserName();
	}
	return "unknown";
}

std::string SFwallCore::Packet::getHw(){
	if (sqp->hwlen == 6) {
		hw[0] = sqp->hw_addr[0];
		hw[1] = sqp->hw_addr[1];
		hw[2] = sqp->hw_addr[2];
		hw[3] = sqp->hw_addr[3];
		hw[4] = sqp->hw_addr[4];
		hw[5] = sqp->hw_addr[5];

		return IP4Addr::convertHw(hw);
	}

	return "";
}

long SFwallCore::Packet::hash() const {
	if(getProtocol() == ICMP){
		return 0;
	}else if(getProtocol() == UDP || getProtocol() == TCP){
		long hash = 0;
		hash += getSrcPort();
		hash += getDstPort();
		hash += getSrcIp();
		hash += getDstIp();

		return hash % HASH_MAX;
	}

	return -1;
}

SFwallCore::proto SFwallCore::Packet::getProtocol() const {
	unsigned char* packet = sqp->raw_packet;
	struct iphdr* packet_header= (struct iphdr*) packet;

	if (packet_header->protocol == IPPROTO_TCP) {
		return TCP;
	} else if (packet_header->protocol == IPPROTO_UDP) {
		return UDP;
	} else if (packet_header->protocol == IPPROTO_ICMP) {
		return ICMP;
	}

	return NDEF;
}

int SFwallCore::Packet::getSrcPort() const {
	if(getProtocol() == TCP){
		unsigned char* packet = sqp->raw_packet;
		struct iphdr* packet_header= (struct iphdr*) packet;
		struct tcphdr* packet_tcp_header= (struct tcphdr *) (packet+ (packet_header->ihl << 2));

		return ntohs(packet_tcp_header->source);
	}else if(getProtocol() == UDP){
		unsigned char* packet = sqp->raw_packet;

		struct iphdr* packet_header= (struct iphdr*) packet;
		struct udphdr* packet_udp_header;
		packet_udp_header = (struct udphdr *) (packet + (packet_header->ihl << 2));
		return ntohs(packet_udp_header->source);
	}else if(getProtocol() == ICMP){
		unsigned char* packet = sqp->raw_packet;
		struct iphdr* packet_header= (struct iphdr*) packet;
		struct icmphdr* icmp = (struct icmphdr *) (packet + (packet_header->ihl << 2));
		return packet_header->saddr; /* WHAT IS THIS?????? */
	}

	return -1;
}

int SFwallCore::Packet::getDstPort() const {
	if(getProtocol() == TCP){
		unsigned char* packet = sqp->raw_packet;
		struct iphdr* packet_header= (struct iphdr*) packet;
		struct tcphdr* packet_tcp_header= (struct tcphdr *) (packet+ (packet_header->ihl << 2)); 

		return ntohs(packet_tcp_header->dest);
	}else if(getProtocol() == UDP){
		unsigned char* packet = sqp->raw_packet;

		struct iphdr* packet_header= (struct iphdr*) packet;
		struct udphdr* packet_udp_header;
		packet_udp_header = (struct udphdr *) (packet + (packet_header->ihl << 2));
		return ntohs(packet_udp_header->dest);
	}

	return -1;
}

bool SFwallCore::TcpPacket::finSet() const {
	unsigned char* packet = sqp->raw_packet;
	struct iphdr* packet_header= (struct iphdr*) packet;
	struct tcphdr* packet_tcp_header= (struct tcphdr *) (packet+ (packet_header->ihl << 2));
	return packet_tcp_header->fin == 1;
}

bool SFwallCore::TcpPacket::ackSet() const {
	unsigned char* packet = sqp->raw_packet;
	struct iphdr* packet_header= (struct iphdr*) packet;
	struct tcphdr* packet_tcp_header= (struct tcphdr *) (packet+ (packet_header->ihl << 2));
	return packet_tcp_header->ack == 1;
}

bool SFwallCore::TcpPacket::synSet() const {
	unsigned char* packet = sqp->raw_packet;
	struct iphdr* packet_header= (struct iphdr*) packet;
	struct tcphdr* packet_tcp_header= (struct tcphdr *) (packet+ (packet_header->ihl << 2));
	return packet_tcp_header->syn == 1;
}

bool SFwallCore::TcpPacket::rstSet() const {
	unsigned char* packet = sqp->raw_packet;
	struct iphdr* packet_header= (struct iphdr*) packet;
	struct tcphdr* packet_tcp_header= (struct tcphdr *) (packet+ (packet_header->ihl << 2));
	return packet_tcp_header->rst == 1;
}

unsigned long SFwallCore::TcpPacket::getAckSeq() const {
	unsigned char* packet = sqp->raw_packet;
	struct iphdr* packet_header= (struct iphdr*) packet;
	struct tcphdr* packet_tcp_header= (struct tcphdr *) (packet+ (packet_header->ihl << 2));

	return ntohl(packet_tcp_header->ack_seq);
}

unsigned long SFwallCore::TcpPacket::getSeq() const {
	unsigned char* packet = sqp->raw_packet;
	struct iphdr* packet_header= (struct iphdr*) packet;
	struct tcphdr* packet_tcp_header= (struct tcphdr *) (packet+ (packet_header->ihl << 2));

	return ntohl(packet_tcp_header->seq);
}

unsigned short SFwallCore::Packet::getIcmpId() const {
	return icmpId;
}

std::string SFwallCore::Packet::getDevice() {
	InterfaceManager* manager = System::getInstance()->getInterfaces();
	NetDevice* device = manager->get(getSourceDev());

	std::string ret;
	if (device != NULL) {
		ret = device->getInterface();
		delete device;
	} else {
		ret = "any";
	}

	return ret;
}

SFwallCore::Packet* SFwallCore::Packet::parsePacket(const struct sq_packet* sqp) {
	struct iphdr* packet_header;
	packet_header = (struct iphdr*) sqp->raw_packet;

	if (packet_header->protocol == IPPROTO_TCP) {
		TcpPacket* tcp = new TcpPacket(sqp);
		return tcp;
	} else if (packet_header->protocol == IPPROTO_UDP) {
		UdpPacket* udp = new UdpPacket(sqp);
		return udp;

	} else if (packet_header->protocol == IPPROTO_ICMP) {
		IcmpPacket* icmp = new IcmpPacket(sqp);
		return icmp;
	}

	return new Packet(sqp);
}

SFwallCore::Packet::Packet(const struct sq_packet* sqp) {
	fakeData = false;
	connection = NULL;
	hasHw = false;
	userPtr = NULL;
	unsigned char* packet = sqp->raw_packet;

	this->sqp = (struct sq_packet*) sqp;
	struct iphdr* packet_header = (struct iphdr*) packet;
	len = ntohs(packet_header->tot_len);
	headerlen = ntohs(packet_header->ihl * 4);
	payloadlen = len - headerlen;
}

SFwallCore::Packet::~Packet() {
}

bool SFwallCore::Packet::isBroadcast(){
	if(IP4Addr::isBroadCast(getSrcIp()) || IP4Addr::isBroadCast(getDstIp())){
		return true;
	}

	return false;
}

bool SFwallCore::Packet::isMulticast(){
	if(IP4Addr::isMultiCast(getSrcIp()) || IP4Addr::isMultiCast(getDstIp())){
		return true;
	}

	return false;
}

SFwallCore::TcpPacket::TcpPacket(const struct sq_packet* sqp) : TcpUdpPacket(sqp) {
	const char* packet = (const char*) sqp->raw_packet;
	struct iphdr* packet_header = (struct iphdr*) packet;

	struct tcphdr* packet_tcp_header;
	packet_tcp_header = (struct tcphdr *) (packet + (packet_header->ihl << 2));

	int position = (packet_header->ihl << 2) + packet_tcp_header->doff * 4;
	headerlen = (packet_header->ihl << 2) + packet_tcp_header->doff * 4;
	payloadlen = len - headerlen;
}

std::string SFwallCore::TcpPacket::toString() const {
	std::stringstream ret;
	ret << "Tcp ";
	ret << IP4Addr::ip4AddrToString(getSrcIp()) << ":" << getSrcPort();
	ret << " -> ";
	ret << IP4Addr::ip4AddrToString(getDstIp()) << ":" << getDstPort();

	ret << " [";
	if (rstSet())
		ret << " RST ";
	if (ackSet())
		ret << " ACK ";
	if (synSet())
		ret << " SYN ";
	if (finSet())
		ret << " FIN ";
	ret << "]";
	ret << " seq: " << getSeq();
	ret << " ackSeq:" << getAckSeq();
	ret << " size:" << getLen();
	return ret.str();
}

std::string SFwallCore::UdpPacket::toString() const {
	std::stringstream ret;
	ret << "Udp ";
	ret << IP4Addr::ip4AddrToString(getSrcIp()) << ":" << getSrcPort();
	ret << " -> ";
	ret << IP4Addr::ip4AddrToString(getDstIp()) << ":" << getDstPort();
	ret << " Size:" << getLen();

	return ret.str();
}

std::string SFwallCore::IcmpPacket::toString() const {
	std::stringstream ret;
	ret << "Icmp ";
	ret << IP4Addr::ip4AddrToString(getSrcIp());
	ret << " -> ";
	ret << IP4Addr::ip4AddrToString(getDstIp());
	ret << " Size:" << getLen();

	return ret.str();
}

SFwallCore::UdpPacket::UdpPacket(const struct sq_packet* sqp) : TcpUdpPacket(sqp) {
}

SFwallCore::IcmpPacket::IcmpPacket(const struct sq_packet* sqp) : SFwallCore::Packet(sqp) {
	unsigned char* packet = sqp->raw_packet;

	struct iphdr* packet_header;
	packet_header = (struct iphdr*) packet;
	check = packet_header->check;

	if (packet_header->protocol == IPPROTO_ICMP) {
		struct icmphdr* icmp = (struct icmphdr *) (packet + (packet_header->ihl << 2));
		icmpId = ntohs(icmp->un.echo.id);
	}
}

string SFwallCore::stringProtocol(proto p) {
	string s;
	switch (p) {
		case NDEF:
			s = "any";
			break;
		case TCP:
			s = "TCP";
			break;
		case UDP:
			s = "UDP";
			break;
		case ICMP:
			s = "ICMP";
			break;
		case IGMP:
			s = "IGMP";
			break;
		default:
			s = "UKNOWN";
			break;
	}
	return s;
}

string SFwallCore::TcpPacket::echo() const {
	stringstream ss;
	ss << "TCP" << ' '
		<< IP4Addr::ip4AddrToString(getSrcIp()) << ':' <<getSrcPort() 
		<< " --> "
		<< IP4Addr::ip4AddrToString(getDstIp()) << ':' << getDstPort() 
		<< endl;
	return ss.str();
}

string SFwallCore::UdpPacket::echo() const {
	stringstream ss;
	ss << "UDP" << IP4Addr::ip4AddrToString(getSrcIp()) << ':' <<getSrcPort() 
		<< " --> "
		<< IP4Addr::ip4AddrToString(getDstIp()) << ':' <<getDstPort() 
		<< endl;
	return ss.str();
}

bool safematch(const char* match, unsigned char* buffer, int position, int len, int matchsize){
	int diff = len - position;
	if(matchsize > diff){
		return false;
	}

	for(int x =0; x < matchsize; x++){
		if(match[x] != buffer[x + position]){
			return false;
		}
	}	

	return true;
}
const char *parse_tls_header(const char* data, int data_len);
string SFwallCore::HttpProxyPacket::getHost() {
	static boost::regex getRequest("^GET http://(.*?)/.*? HTTP/1\\.[01]$");
	std::string packetData = packet->getData();
	std::string packetUrl = "";

	// This searches a large region. Is it nessesary? NOPE, lets optimise it at some point
	boost::smatch what;
	if (regex_search(packetData, what, getRequest)) {
		return what[1];
	}

	static boost::regex connectRequest("^CONNECT ([a-z,A-Z,\\-,0-9,\\.]+)");
	boost::smatch what2;

	while(boost::regex_search(packet->getData(), what2, connectRequest)){
		return what2[1];      
	}

        const char* title = parse_tls_header((const char*)packet->getCData(), packet->getPayloadLen()) ;
        if(title){
                return string(title);
        }
	
	return "";	
}

#define HTTP_HOST_LEN 5
#define HTTP_CR '\r'
#define HTTP_SPACE ' '
string SFwallCore::HttpTcpPacket::getHost() {
	unsigned char* blob = packet->getCData();	
	for(int x= 0; x < packet->getPayloadLen(); x++){
		if(safematch("Host:", blob, x, packet->getPayloadLen(), HTTP_HOST_LEN)){
			x += HTTP_HOST_LEN + 1; 
			string ret;	
			while(x < packet->getPayloadLen() && blob[x] != HTTP_CR){
				if(blob[x] != HTTP_SPACE)
					ret += blob[x];
				x++;

			}
			return ret;
		}
	}

	return "";
}

//SNI Retrieval
/*
 * Copyright (c) 2011 and 2012, Dustin Lundquist <dustin@null-ptr.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/*
 * This is a minimal TLS implementation indented only to parse the server name extension.
 * This was created based primarily on Wireshark dissection of a TLS handshake and RFC4366.
 */
static const char *parse_server_name_extension(const char* buf, int buf_len) {
	static char server_name[SERVER_NAME_LEN];
	const char* p = buf;
	char name_type;
	int name_len;

	if (p - buf + 1 > buf_len) {
		return NULL;
	}

	p += 2;
	while(1) {
		if (p - buf >= buf_len) {
			return NULL;
		}
		name_type = *p;
		p ++;
		switch(name_type) {
			case(0x00):
				if (p - buf + 1 > buf_len) {
					return NULL;
				}
				name_len = ((unsigned char)p[0] << 8) + (unsigned char)p[1];
				p += 2;
				if (p - buf + name_len > buf_len) {
					return NULL;
				}
				if (name_len >= SERVER_NAME_LEN - 1) {
					return NULL;
				}
				strncpy (server_name, p, name_len);
				server_name[name_len] = '\0';
				return server_name;
			default:
				break;
		}
	}
}

const char *parse_tls_header(const char* data, int data_len) {
	char tls_content_type;
	char tls_version_major;
	char tls_version_minor;
	int tls_length;
	const char* p = data;
	int len;

	/* Check that our TCP payload is at least large enough for a TLS header */
	if (data_len < TLS_HEADER_LEN){
		return NULL;
	}

	tls_content_type = p[0];
	if (tls_content_type != TLS_HANDSHAKE_CONTENT_TYPE) {
		return NULL;
	}

	tls_version_major = p[1];
	tls_version_minor = p[2];
	if (tls_version_major < 3) {
		return NULL;
	}

	if (tls_version_major == 3 && tls_version_minor < 1) {
		return NULL;
	}

	tls_length = ((unsigned char)p[3] << 8) + (unsigned char)p[4];
	if (data_len < tls_length + TLS_HEADER_LEN) {
		return NULL;
	}

	/* Advance to first TLS payload */
	p += TLS_HEADER_LEN;

	if (p - data >= data_len) {
		return NULL;
	}

	if (*p != TLS_HANDSHAKE_TYPE_CLIENT_HELLO) {
		return NULL;
	}

	/* Skip past:
	   1    Handshake Type
	   3    Length
	   2    Version (again)
	   32   Random
	   to   Session ID Length
	 */
	p += 38;
	if (p - data >= data_len) {
		return NULL;
	}

	len = (unsigned char)*p; /* Session ID Length */
	p += 1 + len; /* Skip session ID block */
	if (p - data >= data_len) {
		return NULL;
	}

	len = (unsigned char)*p << 8; /* Cipher Suites length high byte */
	p ++;
	if (p - data >= data_len) {
		return NULL;
	}
	len += (unsigned char)*p; /* Cipher Suites length low byte */

	p += 1 + len;

	if (p - data >= data_len) {
		return NULL;
	}
	len = (unsigned char)*p; /* Compression Methods length */

	p += 1 + len;


	if (p - data >= data_len) {
		return NULL;
	}


	len = (unsigned char)*p << 8; /* Extensions length high byte */
	p++;
	if (p - data >= data_len) {
		return NULL;
	}
	len += (unsigned char)*p; /* Extensions length low byte */
	p++;
	while (1) {
		if (p - data + 4 >= data_len) { /* 4 bytes for the extension header */
			return NULL;
		}

		/* Parse our extension header */
		len = ((unsigned char)p[2] << 8) + (unsigned char)p[3]; /* Extension length */
		if (p[0] == 0x00 && p[1] == 0x00) { /* Check if it's a server name extension */
			/* There can be only one extension of each type, so we break
			   our state and move p to beinnging of the extension here */
			p += 4;
			if (p - data + len > data_len) {
				return NULL;
			}
			return parse_server_name_extension(p, len);
		}
		p += 4 + len; /* Advance to the next extension header */
	}
	return NULL;
}

string SFwallCore::HttpsTcpPacket::getHost(){
	const char* title = parse_tls_header((const char*)packet->getCData(), packet->getPayloadLen()) ;
	if(title){
		return string(title);
	}
	return "";
}

bool SFwallCore::HttpsTcpPacket::isTls(){
        const char* p = (const char*) packet->getCData();

        /* Check that our TCP payload is at least large enough for a TLS header */
        if (packet->getPayloadLen() < TLS_HEADER_LEN){
                return true;
        }

        if (p[0]!= TLS_HANDSHAKE_CONTENT_TYPE) {
                return false;
        }
	
	return true;
}

