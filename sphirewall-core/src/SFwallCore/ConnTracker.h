/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_TSHASHSET_H_INCLUDED
#define SPHIREWALL_TSHASHSET_H_INCLUDED

#include <list>
#include <vector>

#include "Conn.h"
#include "NatConn.h"
#include "Packet.h"
#include "Acl.h"
#include "Utils/Lock.h"
#include "Auth/User.h"
#include "TrafficShaper/TokenBucket.h"
#include "Utils/Utils.h"
#include "Core/SysMonitor.h"

#include "Firewall.h"
#define MAX_HANDLERS 10

namespace SFwallCore {

	class DeleteConnectionHook {
		public:
			virtual void hook(Connection* conn) = 0;
	};

	class ConnectionCriteria {
	public:

		ConnectionCriteria() {
			session = NULL;
			this->dest = -1;
			this->destPort = -1;
			this->source = -1;
			this->sourcePort = -1;
			this->type = NDEF;
		}

		void setSession(Session* s) {
			session = s;
		}

		void setSource(unsigned int source) {
			this->source = source;
		}

		void setDest(unsigned int dest) {
			this->dest = dest;
		}

		void setSourcePort(int sourcePort) {
			this->sourcePort = sourcePort;
		}

		void setDestPort(int destPort) {
			this->destPort = destPort;
		}

		void setProtocol(proto type) {
			this->type = type;
		}

		bool match(Connection* connection);

	private:
		unsigned int source, dest;
		int sourcePort, destPort;
		proto type;

		Session* session;
	};

	class TrackerMetrics{
		public:
			long firewall_conntracker_tcp_synsynack;
			long firewall_conntracker_tcp_synackack;
			long firewall_conntracker_tcp_tcpup;
			long firewall_conntracker_tcp_finwait;
			long firewall_conntracker_tcp_closewait;
			long firewall_conntracker_tcp_lastack;
			long firewall_conntracker_tcp_timewait;
			long firewall_conntracker_tcp_finfinack;
			long firewall_conntracker_tcp_closed;
			long firewall_conntracker_tcp_reset;
			
			long firewall_conntracker_tcp_size;
			long firewall_conntracker_tcp_valid;
			long firewall_conntracker_udp_size;
			long firewall_conntracker_udp_valid;
			long firewall_conntracker_icmp_size;
			long firewall_conntracker_icmp_valid;

			TrackerMetrics(){
				firewall_conntracker_tcp_synsynack = 0;
				firewall_conntracker_tcp_synackack = 0;
				firewall_conntracker_tcp_tcpup = 0;
				firewall_conntracker_tcp_finwait = 0;
				firewall_conntracker_tcp_closewait = 0;
				firewall_conntracker_tcp_lastack = 0;
				firewall_conntracker_tcp_timewait = 0;
				firewall_conntracker_tcp_finfinack = 0; 
				firewall_conntracker_tcp_closed = 0;
				firewall_conntracker_tcp_reset = 0;

				firewall_conntracker_tcp_size =0;
				firewall_conntracker_tcp_valid = 0;
				firewall_conntracker_udp_size = 0;
				firewall_conntracker_udp_valid = 0;
				firewall_conntracker_icmp_size = 0;
				firewall_conntracker_icmp_valid = 0;
			}

	};

	class ProtocolHandler {
		public:
			ProtocolHandler(SLogger::LogContext* logger, Firewall* firewall) : 
				logger(logger), firewall(firewall) {

				failsPerSecond = new FlexibleSecondIntervalSampler();
				successPerSecond = new FlexibleSecondIntervalSampler();

				newConns = new FlexibleSecondIntervalSampler();
				offerRequests = new FlexibleSecondIntervalSampler();
				avgIterations = new AverageSampler(60);
				gbTime = new AverageSampler(60);
			}

			virtual ~ProtocolHandler(){}
			Connection* create(Packet* packet);
			virtual Connection* offer(Packet* packet);
			void add(Connection* conn);

			int size();
			std::vector<Connection*> listConnections();
			std::list<Connection*> listConnections(ConnectionCriteria criteria);

			virtual int cleanup();
			virtual Connection* findConn(Packet* packet, int hash);	
			std::list<Connection*> conn_map[Firewall::PLAIN_CONN_HASH_MAX];
			void erase(Connection* conn);	

			void deleteHook(Connection* connection);
			void registerDeleteHook(DeleteConnectionHook* hook);

			//Metrics
			Sampler* offerRequests;
			Sampler* avgIterations;
			Sampler* newConns;
			Sampler* gbTime;
			Sampler* failsPerSecond;
			Sampler* successPerSecond;

			virtual void sample(map<string, long>& items) = 0;
		protected:
			SLogger::LogContext* logger;
			Firewall* firewall;

                        vector<DeleteConnectionHook*> deleteHooks;
	};

	class TcpProtocolHandler : public ProtocolHandler {
		public:
			TcpProtocolHandler(SLogger::LogContext* logger, Firewall* firewall) :
				ProtocolHandler(logger, firewall){
				}			

			~TcpProtocolHandler(){}
			Connection* offer(Packet* packet);
			int cleanup();
			Connection* findConn(Packet* packet, int hash);
			void sample(map<string, long>& items);
			TrackerMetrics* metrics();
	};

	class UdpProtocolHandler : public ProtocolHandler {
		public:
			UdpProtocolHandler(SLogger::LogContext* logger, Firewall* firewall) : 
				ProtocolHandler(logger, firewall){}

			~UdpProtocolHandler(){}
			void sample(map<string, long>& items);
	};


	class IcmpProtocolHandler : public ProtocolHandler {
		public:
			IcmpProtocolHandler(SLogger::LogContext* logger, Firewall* firewall) : 
				ProtocolHandler(logger, firewall){}

			~IcmpProtocolHandler(){};
			void sample(map<string, long>& items);
	};

	class PlainConnTracker : public MetricSampler{
		public:

			PlainConnTracker(SLogger::LogContext* logger, Firewall* firewall) 
				: logger(logger), firewall(firewall) {
					for(int x= 0; x < MAX_HANDLERS; x++){
						handlers[x] = NULL;
					}
					
					handlers[TCP] = new TcpProtocolHandler(logger, firewall);
					handlers[UDP] = new UdpProtocolHandler(logger, firewall);
					handlers[ICMP] = new IcmpProtocolHandler(logger, firewall);
				}

			~PlainConnTracker();

			void sample(map<string, long>& items){
				lock.lock();
				for(int x= 0; x < MAX_HANDLERS; x++){
					if(handlers[x]){
						handlers[x]->sample(items);
					}
				}
				lock.unlock();
			}

			Lock lock;
			void deleteTokenBucket(TokenBucket*);

			Connection* create(Packet* packet);
			Connection* offer(Packet* packet);
			void add(Connection* conn);
			int size();
			std::vector<Connection*> listConnections();
			std::list<Connection*> listConnections(ConnectionCriteria criteria);
			int cleanup();

			Sampler* cacheIterations;

			void terminate(Connection* connection);
			void registerDeleteHook(DeleteConnectionHook* hook){
				for(int x =0; x < MAX_HANDLERS; x++){
					if(handlers[x]){
						handlers[x]->registerDeleteHook(hook);
					}
				}
			}
		protected:
			Connection* findConn(Packet* packet, int hash);

		private:
			void erase(Connection* conn);

			SLogger::LogContext* logger;
			Firewall* firewall;
			ProtocolHandler* handlers[MAX_HANDLERS];
	};
}

#endif
