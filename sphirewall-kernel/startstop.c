/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <linux/module.h>
#include <net/sock.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <asm/types.h>
#include <linux/skbuff.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/in.h>
#include <linux/netfilter_ipv4.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/netfilter.h>
#include <linux/kfifo.h>
#include <net/netfilter/nf_queue.h>
#include <linux/ipc.h>
#include <linux/types.h>
#include <linux/msg.h>
#include <linux/kallsyms.h>
#include <linux/types.h>    // uint64_t //
#include <linux/kthread.h>  // kthread_run, kthread_stop //
#include <linux/delay.h>    // msleep_interruptible //
#include <linux/syscalls.h> // sys_msgget //

#include "queue.h"

static struct task_struct* send_thread;
static struct task_struct* recv_thread;
wait_queue_head_t recv_wait_lock;

static int __init sphirewall_queue_init(void)
{
	PMESSAGE("GPLv2 The Sphirewall Team\n");
	PMESSAGE("Loading Module Threads\n");
	
	qmgr_send = kmalloc(sizeof(struct qmgr), GFP_KERNEL);
	qmgr_recv = kmalloc(sizeof(struct qmgr), GFP_KERNEL);
	qmgr_send->state = STATE_OFFLINE;
	qmgr_recv->state = STATE_OFFLINE;

	init_waitqueue_head(&recv_wait_lock);
	send_queue_init();
	send_thread = kthread_run(&squeue_outgoing_worker, NULL, "sphirewall_send_thread");
	recv_thread = kthread_run(&squeue_incoming_worker, NULL, "sphirewall_recv_thread");

	register_hooks();
	if(open_shm() < 0){
		PMESSAGE("Fatal error loading module, debugfs enabled in the kernel\n");
		return -1;
	}
	PMESSAGE( "Finished Loading Threads: Ready for Connections\n");

	return 0;
}

static void __exit sphirewall_queue_exit(void)
{
	PMESSAGE("Module Unloading\n");
	unregister_hooks();

	kthread_stop(send_thread);
	kthread_stop(recv_thread);
        wake_up(&send_wait_lock);

        close_shm();		
	PMESSAGE("Unloaded\n");
	return;
}

module_init(sphirewall_queue_init);
module_exit(sphirewall_queue_exit);

MODULE_DESCRIPTION("Sphirewall Queue");
MODULE_LICENSE("GPL");

