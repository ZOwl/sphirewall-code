/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DHCP3CONFIGURATION_H
#define	DHCP3CONFIGURATION_H

#include <list>
#include <map>
#include <string>

#include "Core/Process.h"

class ConfigurationManager;
class InterfaceManager;

class StaticLease{
public:
	std::string hostname;
	std::string ip;
	std::string hw;
};

class Dhcp3ServiceInstance {
	public:
		std::string startIpAddress;
		std::string finishIpAddress;
		std::string defaultRouter;
		std::string domainNameService;
		std::string domainName;
		std::string interface;

                std::list<StaticLease*> staticLeases;

                void addStaticLease(std::string hostname,std::string ipaddress, std::string mac);
                void deleteStaticLease(std::string hostname, std::string ipaddress, std::string mac);
		bool enabled;
};

class Dhcp3ConfigurationManager : public Process {
	public:
		Dhcp3ConfigurationManager(ConfigurationManager* config, InterfaceManager* interfaceManager) :
			config(config), interfaceManager(interfaceManager){
				this->controlScript = "/etc/init.d/isc-dhcp-server";			
				this->active = false;
			}

		std::string controlScript;		

		void save();
		void load();
		void publishConfigurationToServer();

		//Startup and Shutdown
		void initalise();
		void destroy();

		void start();
		void stop();
		void restart();
		bool running();

		std::string generateConfigurationString();	
		Dhcp3ServiceInstance* getOrCreate(std::string interface);
	private:
		std::list<Dhcp3ServiceInstance*> instances;
		ConfigurationManager* config;
		InterfaceManager* interfaceManager;
		bool active;
};

#endif

