/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PORTSCANSENSOR_H
#define PORTSCANSENSOR_H

#include <set>
#include "Ids.h"

class PortScanSensor : public IdsSensor {
	static const int TIME_THRESHOLD = 2;
	static const int HASH_MAX = 1024;
	static const int HIT_THRESHOLD = 20;

public:

	PortScanSensor(IDS* ids) : IdsSensor(ids) {
	}
	void handle(SFwallCore::Packet* packet);

	std::string description() {
		return "Port scan";
	}

	std::string type(); 

private:

	int hash(unsigned int source, unsigned int dest);

	class Hit {
	public:

		Hit(unsigned int source, unsigned int dest, int dport) : source(source), dest(dest) {
			startTime = time(NULL);
			reported = false;
			ports.insert(dport);
		}

		unsigned int source;
		unsigned int dest;

		int hits() {
			return ports.size();
		}
		time_t startTime;
		bool reported;

		bool hasExpired() {
			if ((time(NULL) - startTime) > TIME_THRESHOLD) {
				return true;
			}
			return false;
		}

		void insert(int i) {
			ports.insert(i);
		}
	private:
		std::set<int> ports;
	};

	map<int, list<Hit*> >hits;
	Hit* findMatch(unsigned int source, unsigned int dest);
};

#endif
